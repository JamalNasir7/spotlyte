//
//  StaticClass.m
//  Spotlyte
//
//  Created by Tops on 8/9/17.
//  Copyright © 2017 Hemant. All rights reserved.
//

#import "StaticClass.h"

@implementation StaticClass
#pragma mark - Remove Null
+(NSString *)removeNull:(NSString *)str {
    str = [NSString stringWithFormat:@"%@",str];
    if (!str) {
        return @"";
    }
    else if([str isEqualToString:@"<null>"]){
        return @"";
    }
    else if([str isEqualToString:@"(null)"]){
        return @"";
    }
    else if([str isEqualToString:@"null"]){
        return @"";
    }
    else if([str isEqualToString:@""]){
        return @"";
    }
    else if([str isEqualToString:@"N/A"]){
        return @"N/A";
    }
    else if([str isEqualToString:@"n/a"]){
        return @"N/A";
    }
    else{
        return str;
    }
}
@end
