//
//  StaticClass.h
//  Spotlyte
//
//  Created by Tops on 8/9/17.
//  Copyright © 2017 Hemant. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StaticClass : NSObject
+(NSString *)removeNull:(NSString *)str;

@end
