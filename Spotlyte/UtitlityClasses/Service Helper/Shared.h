//
//  Shared.h
//  Spotlyte
//
//  Created by CIPL137-MOBILITY on 09/05/16.
//  Copyright © 2016 Naveen. All rights reserved.
//

#ifndef Shared_h
#define Shared_h


#endif /* Shared_h */
typedef NS_ENUM(NSInteger, categoryType)
{
    DiveBars = 1,
    StreetFestivals=7,
    BYOB=3,
    BeerGardensRoofTops=73,
    WineBars=4,
    trendywhiskeybars=5,
    Breweries=5,
    GameBars=6,
    FoodDrinkChallenges=11,
    KaraokeBars=8,
    TriviaBars=9,
    WhiskeyBars=10,
} ;

typedef enum
{
    e_loginType_Email  = 0,
    e_LoginType_Facebook,
    e_loginType_Twitter,
    e_loginType_Gmail
    
}LoginType;

typedef enum
{
    e_EmonjiType_Cover  = 1,
    e_EmonjiType_Wall,
    e_EmonjiType_Shirt,
    e_EmonjiType_Music

}EmonjiType;

typedef enum
{
    e_Restaurants  = 1,
    e_Bars = 2,
    e_museums = 3,
    
}PlaceType;


