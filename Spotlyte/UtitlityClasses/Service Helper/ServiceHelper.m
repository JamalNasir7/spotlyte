//
//  ServiceHelper.m
//  Spotlyte
//
//  Created by Admin on 28/03/16.
//  Copyright © 2016 Naveen. All rights reserved.

#define SERVERIP @"http://demo.mycipl.in/spotlyte/webservice/api/mobile/"
#import "Reachability.h"
#import "ServiceHelper.h"

static Reachability *reachability;

//Spotlyte New Server
//NSString *baseURL = @"http://spotlytespecials.com/spotlyte/webservice/Api/mobile/";
//NSString *customURL = @"http://spotlytespecials.com/spotlyte/webservice/Api/";

//Spotlyte Local Server
//NSString *baseURL = @"http://192.168.0.56/web_services/spotlyte/webservice/api/mobile1/";
//NSString *customURL = @"http://192.168.0.56/web_services/spotlyte/webservice/api/";

//Spotlyte Tops Demo Server
//NSString *baseURL = @"http://topsdemo.co.in/client/spotlyte/v.1.3/webservice/api/mobile1/";
//NSString *baseURL = @"https://spotlytespecials.com/beta/webservice/api/mobile1/";
//change
//NSString *baseURL = @"https://spotlytespecials.com/webservice/api/mobile1/";

///// FINAL BASE URL
//NSString *baseURL = @"https://www.spotlytespecials.com/webservice/api/mobile/";
///// BETA BASE URL
NSString *baseURL = @"https://dev.spotlytespecials.com/webservice/api/mobile/";

//NSString *baseURL = @"http://192.168.0.57/webservices/Spotlyte/webservice/api/mobile1/";
//NSString *customURL = @"http://topsdemo.co.in/client/spotlyte/v.1.3/webservice/api/";
NSString *customURL = @"https://www.spotlytespecials.com/webservice/api/";
@implementation ServiceHelper
+(void)loginWithEmailId:(NSString *)emailId andPassword:(NSString *)password andDeviceToken:(NSString *)deviceToken andDeviceType:(NSString*)DeviceType callback:(void(^)(id, NSError*, BOOL))callback{
   // NSString *post = @"{\"user_name\":\"hiren\",\"password\":\"tops?123\",\"device_token\":\"123456\",\"device_type\":\"1\"}";
    
    NSDictionary *dict = @{@"user_name":emailId,@"password":password,@"device_token":deviceToken,@"device_type":DeviceType};
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];

  // NSString *post=[NSString stringWithFormat:@"user_name=%@&password=%@&device_token=%@&device_type=%@",emailId,password,deviceToken,DeviceType];
   // post = [post stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
   // NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%d",(int)[jsonData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@loginUser.json",baseURL ]] cachePolicy:0 timeoutInterval:30];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Accept-Encoding"];
   // [request setValue:jsonString forHTTPHeaderField:@"Accept-Encoding"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"]; //application/x-www-form-urlencoded;charset=UTF-8
   // [request setHTTPBody:postData];
    [request setHTTPBody:jsonData];
    [request setAllowsCellularAccess:YES];
    [request setValue:@"no-cache" forHTTPHeaderField:@"cache-control"];
    [self callService:request responseCallback:callback];
}

+ (void)sendRequestToServerWithParameters:(NSString *)parameters forhttpMethod:(NSString *)httpMethod withrequestBody:(id)body sync:(BOOL)isSync completion:(void (^)(id obj))completionBlock
{
    NSURL *requestUrl=[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",baseURL,parameters]];
    
    NSLog(@"request URL - %@",requestUrl);
    NSLog(@"request body - %@",body);
    
    NSString *contentType = [NSString stringWithFormat:@"application/json"];
    //Create thhe session with custom configuration
    NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfiguration.HTTPAdditionalHeaders = @{@"Content-Type": contentType};

    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfiguration];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:requestUrl];
    request.HTTPMethod = httpMethod;
    
    NSMutableData *jsonData = [NSMutableData data];
    if (body) {
        [jsonData appendData:(id)[body dataUsingEncoding:NSUTF8StringEncoding]];
        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[jsonData length]];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        [request setValue:contentType forHTTPHeaderField:@"Content-type"];
        [request setHTTPBody:jsonData];
        [request setAllowsCellularAccess:YES];
        [request setValue:[[self class] getLocalTimeZone] forHTTPHeaderField:@"X-Signature"];
    }
    
    NSError *error = nil;
    if (!error) {
        // 4
        NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
        {
            NSLog(@"Server_Header1 == %@",response);
        
            if (data!=nil) {
            NSDictionary *contentType = [(NSHTTPURLResponse *)response allHeaderFields];
            if ([(NSHTTPURLResponse *)response statusCode] ==200) { //Success
                if ([[contentType valueForKey:@"Content-Type"] rangeOfString:@"application/json"].length) {
  
                NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                NSLog(@"JSON == %@",JSON);
                    if (completionBlock) {
                            completionBlock(JSON);
                        }
                        else
                        {
                            completionBlock(JSON);
                        }
                }
                else{
                    completionBlock(nil);
                }
            }
            else{
                ///Error
                if (completionBlock) {
                    completionBlock(@"Error");
                }
            }
            }else
                completionBlock(nil);
        }];
        [postDataTask resume];
        [session finishTasksAndInvalidate];
    }
}

+ (BOOL)getInternetStatus:(BOOL)shouldShowMessage
{
    if (!reachability) {
        reachability =[Reachability reachabilityForInternetConnection];
        [reachability startNotifier];
    }
    NetworkStatus status=[reachability currentReachabilityStatus];
    
    if(status==NotReachable)
    {
        if (shouldShowMessage)
        {
            ERRORMESSAGE_ALERT(@"Your device is not connected to the internet");
        }
        
    }
    return status!=NotReachable;
}



//----------------------
#pragma mark Get Request
//----------------------
//https://www.googleapis.com/oauth2/v1/userinfo?alt=json

+(void)customGetRequest:(NSString *)request callback:(void(^)(id, NSError*, BOOL))callback{
    [self GetURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",customURL,request]] callback:callback];
}
+(void)customGetRequest_item:(NSString *)request callback:(void(^)(id, NSError*, BOOL))callback{
    [self GetURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",baseURL,request]] callback:callback];
}



+(void)getGoogleRequest:(NSString *)request callback:(void(^)(id, NSError*, BOOL))callback{
    [self GetURL:[NSURL URLWithString:request] callback:callback];
}

+(void)getRequest:(NSString *)request callback:(void(^)(id, NSError*, BOOL))callback{
    [self GetURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",baseURL,request]] callback:callback];
}


+(void)GetURL:(NSURL *)getURl callback:(void(^)(id result, NSError *error, BOOL isNetwork))callback{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:getURl];
    [request setTimeoutInterval:30];
    [request setHTTPMethod:@"GET"];
    [request setAllowsCellularAccess:YES];
    [request setValue:@"no-cache" forHTTPHeaderField:@"cache-control"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"]; //application/x-www-form-urlencoded
    [request setValue:[[self class] getLocalTimeZone] forHTTPHeaderField:@"X-Signature"];
    [self callService:request responseCallback:callback];
}


+(void)callService:(NSMutableURLRequest *)request responseCallback:(void(^)(id result, NSError *error, BOOL isNetwork))callback{
    
    [UIView setAnimationsEnabled:YES];
    if(APP_DELEGATE.checkReachability){
        
        NSURLSessionConfiguration *sessionConfig =[NSURLSessionConfiguration defaultSessionConfiguration];
        sessionConfig.timeoutIntervalForRequest =30;
        sessionConfig.timeoutIntervalForResource =60;
        sessionConfig.allowsCellularAccess = YES;
        
        NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig];
        
        NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){
            
            NSLog(@"ServerHeader2 == %@",response);
            
           if (error==nil){
                if(data){
                    NSString *str = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                    NSLog(@"str == %@",str);
                    str = [str stringByReplacingOccurrencesOfString:@"(null)" withString:@""];
                    str = [str stringByReplacingOccurrencesOfString:@"&quot;" withString:@"\""];
                    str = [str stringByReplacingOccurrencesOfString:@"message" withString:@"Message"];
                    if(str != nil){
                        NSError *error=nil;
                        id result = [NSJSONSerialization JSONObjectWithData: [str dataUsingEncoding:NSUTF8StringEncoding] options: 0 error: &error];
                        if(error==nil){
                            if(callback)
                                callback(result,nil,true);
                        }else{
                            if(callback)
                                callback(nil,error,true);
                        }
                        str=nil;
                        result=nil;
                    }else{
                        if(callback)
                            callback(nil,error,true);
                    }
                }else{
                    if(callback)
                        callback(nil,error,true);
                }
            }else{
                if(callback){
                    if(error.code==(-1009)||error.code==(-1001))
                        callback(nil,error,false);
                    else
                        callback(nil,error,true);
                }
            }
        }];
        [task resume];
        [session finishTasksAndInvalidate];
    }
    else{
        if(callback){
            callback(nil,nil,false);
        }
    }
}


+ (NSString*)getLocalTimeZone{
    NSTimeZone *timeZone = [NSTimeZone localTimeZone];
    NSString *tzName = [timeZone name];
    return tzName;
}
@end
