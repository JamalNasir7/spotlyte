//
//  ServiceHelper.h
//  Spotlyte
//
//  Created by Admin on 28/03/16.
//  Copyright © 2016 Naveen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SharedMethods.h"

@interface ServiceHelper : NSObject


+ (void)sendRequestToServerWithParameters:(NSString *)urlString forhttpMethod:(NSString *)httpMethod withrequestBody:(id)body sync:(BOOL)isSync completion:(void (^)(id obj))completionBlock;
+ (BOOL)getInternetStatus:(BOOL)shouldShowMessage;

+(void)getGoogleRequest:(NSString *)request callback:(void(^)(id, NSError*, BOOL))callback;
+(void)customGetRequest_item:(NSString *)request callback:(void(^)(id, NSError*, BOOL))callback;
+(void)customGetRequest:(NSString *)request callback:(void(^)(id, NSError*, BOOL))callback;
+(void)getRequest:(NSString *)request callback:(void(^)(id, NSError*, BOOL))callback;

+(void)loginWithEmailId:(NSString *)emailId andPassword:(NSString *)password andDeviceToken:(NSString *)deviceToken andDeviceType:(NSString*)DeviceType callback:(void(^)(id, NSError*, BOOL))callback;

@end
