//
//  CALayer+CustomBorder.h
//  Spotlyte
//
//  Created by Admin on 18/04/16.
//  Copyright © 2016 Naveen. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import <UIKit/UIKit.h>

@interface CALayer (CustomBorder)

@property (nonatomic, assign) UIColor* borderUIColor;
@end
