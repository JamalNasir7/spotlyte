//
//  CALayer+CustomBorder.m
//  Spotlyte
//
//  Created by Admin on 18/04/16.
//  Copyright © 2016 Naveen. All rights reserved.
//

#import "CALayer+CustomBorder.h"

@implementation CALayer (CustomBorder)

- (void)setBorderUIColor:(UIColor *)color {
    self.borderColor = color.CGColor;
}

- (UIColor *)borderUIColor {
    return [UIColor colorWithCGColor:self.borderColor];
}

@end
