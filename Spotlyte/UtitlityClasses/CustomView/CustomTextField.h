//
//  CustomTextField.h
//  GopherAssist
//
//  Created by Arunkumar Gnanasekar on 12/01/16.
//  Copyright © 2016 Colan Infotech Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomTextField : UITextField

@end
