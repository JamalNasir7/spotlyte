//
//  CustomTextField.m
//  GopherAssist
//
//  Created by Arunkumar Gnanasekar on 12/01/16.
//  Copyright © 2016 Colan Infotech Private Limited. All rights reserved.
//

#import "CustomTextField.h"

@implementation CustomTextField

- (CGRect)textRectForBounds:(CGRect)bounds              // placeholder position
{
    return CGRectInset(bounds, 6, 6);
}

- (CGRect)editingRectForBounds:(CGRect)bounds           // text position
{
    return CGRectInset(bounds, 6, 6);
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end