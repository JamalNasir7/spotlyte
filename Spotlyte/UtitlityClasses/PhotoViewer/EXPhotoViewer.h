//
//  EXPhotoViewer.h
//  EXPhotoViewerDemo
//
//  Created by Julio Carrettoni on 3/20/14.
//  Copyright (c) 2014 Julio Carrettoni. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PhotosCollectionViewCell.h"
@interface EXPhotoViewer : UIViewController <UIScrollViewDelegate,UIGestureRecognizerDelegate>

+ (void) showImageFrom:(PhotosCollectionViewCell*)cell withIndex:(NSInteger)index response:(void (^)(BOOL))alertCallback;
- (void) onBackgroundTap;
@end
