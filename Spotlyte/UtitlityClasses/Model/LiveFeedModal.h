//
//  LiveFeedModal.h
//  Spotlyte
//
//  Created by CIPL227-MOBILITY on 24/03/16.
//  Copyright © 2016 Naveen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LiveFeedModal : NSObject

@property (strong, nonatomic) NSString *strTweetname;
@property (strong, nonatomic) NSString * strTweetId;
@property (strong, nonatomic) NSString *strTweetMessaged;
@property (strong, nonatomic) NSString *strTweetTime;
@property (strong, nonatomic) NSString *strTweetImage;
@property (strong, nonatomic) NSArray *arrayOfMessages;
@property (strong, nonatomic) NSArray *arrayOfMessagesTiming;

@end
