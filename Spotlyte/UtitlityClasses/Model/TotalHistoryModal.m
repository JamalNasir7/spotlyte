//
//  TotalHistoryModal.m
//  Spotlyte
//
//  Created by CIPL227-MOBILITY on 20/04/16.
//  Copyright © 2016 Naveen. All rights reserved.
//

#import "TotalHistoryModal.h"

@implementation TotalHistoryModal
@synthesize resturantImage,resturantName,recentActivityTime,recentActivityType,resturantReviewDescription;
@synthesize resturantReviewRating,userID_History;
@synthesize reason,restDescription;
@synthesize message,result;
@synthesize resturantImageStr,postComment;
@synthesize arrayNegative,arrayPositive;
+ (TotalHistoryModal *)sharedUserProfile
{
    @synchronized([TotalHistoryModal class])
    {
        static TotalHistoryModal *_sharedUsers;
        if (!_sharedUsers)
        {
            _sharedUsers=[[TotalHistoryModal alloc] init];
        }
        return _sharedUsers;
    }
    return nil;
}

#pragma mark -- Total History ISL

- (void)totalHistoryValuesToServer:(TotalHistoryModal *)userdetails  withCompletion:(void(^)(id obj1))completionBlock
{
    
    NSDictionary *dict = @{@"user_id":userdetails.userID_History};
    
   // NSDictionary *dict = @{@"user_id":@"1557"};

    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    
    [ServiceHelper sendRequestToServerWithParameters:@"totalHistory.json" forhttpMethod:@"POST" withrequestBody:jsonString sync:YES completion:^(id obj) {
        
        NSLog(@"result == %@",obj);
        if([obj isKindOfClass:[NSArray class]] || [obj isKindOfClass:[NSMutableArray class]])
        {
            NSArray *arr = obj;
            if(arr.count>0)
            {
                NSArray *objArray = nil;
                
                objArray = [self setAllValues:arr];
                if (completionBlock) {
                    completionBlock(objArray);
                }
                
            }
            else
            {
                if (completionBlock) {
                    completionBlock(0);
                }
            }
            
        }
        else if([obj isKindOfClass:[NSDictionary class]])
        {
            TotalHistoryModal *modal = [[TotalHistoryModal alloc]init];
            [modal setResult:[obj valueForKey:@"result"]];
            [modal setMessage:[obj valueForKey:@"message"]];
            completionBlock(modal);
        }
        else
        {
            completionBlock(obj);
        }
        
    }];
}


#pragma mark -- Recent Activity ISL
- (void)recentActivityToServer:(TotalHistoryModal *)userdetails  withCompletion:(void(^)(id obj1))completionBlock
{
    NSDictionary *dict = @{@"user_id":userdetails.userID_History};
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    
    [ServiceHelper sendRequestToServerWithParameters:@"RecentActivity.json" forhttpMethod:@"POST" withrequestBody:jsonString sync:YES completion:^(id obj) {
        
        NSLog(@"server response == %@",obj);
        
        if([obj isKindOfClass:[NSArray class]] || [obj isKindOfClass:[NSMutableArray class]])
        {
            NSArray *arr = obj;
            if(arr.count>0)
            {
                NSArray *objArray = nil;
                
                objArray = [self setAllValues:arr];
                if (completionBlock) {
                    completionBlock(objArray);
                }
            }
            else
            {
                if (completionBlock) {
                    completionBlock(0);
                }
            }
            
        }
        else if([obj isKindOfClass:[NSDictionary class]])
        {
            TotalHistoryModal *modal = [[TotalHistoryModal alloc]init];
            [modal setResult:[obj valueForKey:@"result"]];
            [modal setMessage:[obj valueForKey:@"message"]];
            completionBlock(modal);
        }
        else
        {
            completionBlock(obj);
        }
    }];
}

-(NSMutableArray *)setAllValues :(id)objVal
{
    TotalHistoryModal *modal;
    NSMutableArray *modalArray = [[NSMutableArray alloc]init];
    
    for(NSDictionary *dict in objVal)
    {
        modal = [[TotalHistoryModal alloc]init];
        NSString *activity_type = [dict valueForKey:@"activity_type"];
        if(activity_type != (id)[NSNull null])
            [modal setRecentActivityType:activity_type];
        else
            [modal setRecentActivityType:@""];
        
        
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy"];
        
        [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"..."]];
        
        
        
        
        NSString *created_date = [NSString stringWithFormat:@"%@",[dict valueForKey:@"created_date"]];
        if(created_date != (id)[NSNull null])
            [modal setRecentActivityTime:created_date];
        else
            [modal setRecentActivityTime:@""];
        
        NSString *commends = removeNull([dict valueForKey:@"comments"]);
        if(commends != (id)[NSNull null])
            [modal setResturantReviewDescription:commends];
        else
            [modal setResturantReviewDescription:@""];
        
        NSString *place = removeNull([dict valueForKey:@"Place"]);
        if(place != (id)[NSNull null])
            [modal setResturantName:place];
        else
            [modal setResturantName:@""];
        
        NSString *rating = removeNull([dict valueForKey:@"Rating"]);
        if(commends != (id)[NSNull null])
            [modal setResturantReviewRating:rating];
        else
            [modal setResturantReviewRating:@""];
        
        NSString *post_Commend = removeNull([dict valueForKey:@"post_Comment"]);
        if(post_Commend != (id)[NSNull null])
            [modal setPostComment:post_Commend];
        else
            [modal setPostComment:@""];
        
        NSString *image_url = removeNull([dict valueForKey:@"image_url"]);
        if(image_url != (id)[NSNull null])
            [modal setResturantImageStr:image_url];
        else
            [modal setResturantImageStr:@""];
    
        
        NSString *condition = removeNull([dict valueForKey:@"condition"]);
        if(condition != (id)[NSNull null])
            [modal setCondition:condition];
        else
            [modal setCondition:@""];
        
        [modalArray addObject:modal];
        
        
    }
    
    return modalArray;
}

#pragma mark --- Positive and Negative Reviews ISL


- (void)positiveNegativeToServer:(TotalHistoryModal *)userdetails  withCompletion:(void(^)(id obj1))completionBlock
{
    
    NSDictionary *dict = @{@"user_id":userdetails.userID_History};
    
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    
    [ServiceHelper sendRequestToServerWithParameters:@"RetrievePositiveNegativeReviews.json" forhttpMethod:@"POST" withrequestBody:jsonString sync:YES completion:^(id obj) {
        TotalHistoryModal *modal;
        
        if ([obj isKindOfClass:[NSDictionary class]])
        {
            NSArray *arr = obj;
            if(arr.count>0)
            {
                modal = [TotalHistoryModal new];
                NSMutableArray *positiveArray = [obj valueForKey:@"Positive"];
                [modal setArrayPositive:[self setAllPositiveNegativeValues:positiveArray]];
                
                NSMutableArray *NegativeArray = [obj valueForKey:@"Negative"];
                [modal setArrayNegative:[self setAllPositiveNegativeValues:NegativeArray]];
                
                if (completionBlock) {
                    completionBlock(modal);
                }
            }
        }
        else
        {
            completionBlock(obj);
        }
    }];
}


-(NSMutableArray *)setAllPositiveNegativeValues :(NSMutableArray *)objVal
{
    TotalHistoryModal *modal;
    NSMutableArray *modalArray = [[NSMutableArray alloc]init];
    
    for(NSDictionary *dict in objVal)
    {
        modal = [[TotalHistoryModal alloc]init];
        
        
        NSString *created_date = [NSString stringWithFormat:@"%@",[dict valueForKey:@"created_date"]];
        if(created_date != (id)[NSNull null])
            [modal setRecentActivityTime:created_date];
        else
            [modal setRecentActivityTime:@""];
        
        NSString *commends = [dict valueForKey:@"commend"];
        if(commends != (id)[NSNull null])
            [modal setResturantReviewDescription:commends];
        else
            [modal setResturantReviewDescription:@""];
        
        NSString *place = [dict valueForKey:@"place"];
        if(place != (id)[NSNull null])
            [modal setResturantName:place];
        else
            [modal setResturantName:@""];
        
        NSString *rating = [dict valueForKey:@"rating"];
        if(commends != (id)[NSNull null])
            [modal setResturantReviewRating:rating];
        else
            [modal setResturantReviewRating:@""];
        
        NSString *post_Commend = [dict valueForKey:@"post_Comment"];
        if(post_Commend != (id)[NSNull null])
            [modal setPostComment:post_Commend];
        else
            [modal setPostComment:@""];
        
        NSString *image_url = [dict valueForKey:@"image_url"];
        if(image_url != (id)[NSNull null])
            [modal setResturantImageStr:image_url];
        else
            [modal setResturantImageStr:@""];
        
        [modalArray addObject:modal];
        
    }
    
    return modalArray;
}



#pragma mark --- Report ISL

- (void)reportToServer:(TotalHistoryModal *)userdetails  withCompletion:(void(^)(id obj1))completionBlock
{
    NSDictionary *dict = @{@"user_id":userdetails.userID_History,@"reason":userdetails.reason,@"message":userdetails.restDescription};
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    
    [ServiceHelper sendRequestToServerWithParameters:@"SendFeedback.json" forhttpMethod:@"POST" withrequestBody:jsonString sync:YES completion:^(id obj) {
        if ([obj isKindOfClass:[NSDictionary class]]) {
            TotalHistoryModal *modal = [[TotalHistoryModal alloc]init];
            [modal setResult:[obj valueForKey:@"result"]];
            [modal setMessage:[obj valueForKey:@"message"]];
            if (completionBlock) {
                completionBlock(modal);
            }
        }
        else
        {
            completionBlock(obj);
        }
    }];
}
@end
