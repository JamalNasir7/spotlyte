//
//  PromtionsModal.m
//  Spotlyte
//
//  Created by Admin on 12/04/16.
//  Copyright © 2016 Naveen. All rights reserved.
//

#import "PromtionsModal.h"

@implementation PromtionsModal
@synthesize promotionCreatedDate,promotionDesc,promotionExpiredDate,promotionID,promotionImage,placeID,promotionStartDate,promotionTitle,Special_count,Promotion_Type;
@synthesize promotionFavStatus,user_ID;
@synthesize message,result,promotionLongtitude,promotionLatitude,promotionPlaceType;
+ (PromtionsModal *)sharedUserProfile
{
    @synchronized([PromtionsModal class])
    {
        static PromtionsModal *_sharedUsers;
        if (!_sharedUsers)
        {
            _sharedUsers=[[PromtionsModal alloc] init];
        }
        return _sharedUsers;
    }
    return nil;
}

#pragma mark -- HotelPromtions List


- (void)hotelPromotionsListValuesToServer:(ResturantModal *)userdetails  withCompletion:(void(^)(id obj1))completionBlock
{
    
    NSString* latitude=[NSString stringWithFormat:@"%f",APP_DELEGATE.userCurrentLocation.coordinate.latitude];
    NSString* longitude=[NSString stringWithFormat:@"%f",APP_DELEGATE.userCurrentLocation.coordinate.longitude];
 //   NSString *latitude = @"23.0225";
    //NSString *longitude = @"72.5714";
    NSDictionary *dict = @{@"place_id":@"0",@"user_id":userdetails.userID,@"current_latitude":latitude,@"current_longitude":longitude};
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    [ServiceHelper sendRequestToServerWithParameters:@"RetrieveHotelPromotions.json" forhttpMethod:@"POST" withrequestBody:jsonString sync:YES completion:^(id obj) {
        if ([obj isKindOfClass:[NSDictionary class]]) {
            PromtionsModal *modal = [[PromtionsModal alloc]init];
            [modal setResult:[obj valueForKey:@"result"]];
            [modal setMessage:[obj valueForKey:@"message"]];
            if([[obj valueForKey:@"result"] isEqualToString:@"success"])
            {
                modal.promotionArray = [self setListAllHotelPromotionsValue:obj];
                modal.placesArray = [self setListAllValue:obj];
            }
            if (completionBlock) {
                completionBlock(modal);
                modal=nil;
                
                if (!modal) {
                    NSLog(@"modal is nil");
                }
            }
        }
        else{
            completionBlock(obj);
            obj=nil;
        }
    }];
}


-(NSMutableArray *)setListAllValue :(id)arg
{
    ResturantModal *modal;
    NSMutableArray *modalArray = [[NSMutableArray alloc]init];
    NSArray *dictArray;
    dictArray = [arg valueForKeyPath:@"promotion_lists.place_details"];
    
    
    for(NSDictionary *dict in dictArray)
    {
        modal = [[ResturantModal alloc]init];
        
        if(dict != nil)
        {
            
            NSString *place_idStr = [dict valueForKey:@"place_id"];
            if(place_idStr != (id)[NSNull null])
                [modal setPlaceID:place_idStr];
            else
                [modal setPlaceID:@""];
            
            NSString *titleStr = [dict valueForKey:@"title"];
            if(titleStr != (id)[NSNull null])
                [modal setResturantName:titleStr];
            else
                [modal setResturantName:@""];
            
            NSString *subtitleStr = [dict valueForKey:@"subtitle"];
            if(subtitleStr != (id)[NSNull null])
                [modal setSubTitle:subtitleStr];
            else
                [modal setSubTitle:@""];
            
            NSString *descriptionStr = [dict valueForKey:@"description"];
            if(descriptionStr != (id)[NSNull null])
                [modal setResturantDescription:descriptionStr];
            else
                [modal setResturantDescription:@""];
            
            NSString *place_typeStr = [dict valueForKey:@"place_type"];
            if(place_typeStr != (id)[NSNull null])
                [modal setPlaceType:place_typeStr];
            else
                [modal setPlaceType:@""];
            
            
            
            NSString *email_idStr = [dict valueForKey:@"website"];
            if(email_idStr != (id)[NSNull null])
                [modal setResturantEMail:email_idStr];
            else
                [modal setResturantEMail:@""];
            
            NSString *cityStr = [dict valueForKey:@"city"];
            if(cityStr != (id)[NSNull null])
                [modal setResturantCity:cityStr];
            else
                [modal setResturantCity:@""];
            
            NSString *address1Str = [dict valueForKey:@"address"];
            if(address1Str != (id)[NSNull null])
                [modal setResturantAddress:address1Str];
            else
                [modal setResturantAddress:@""];
            
            NSString *address2Str = [dict valueForKey:@"address2"];
            if(address2Str != (id)[NSNull null])
                [modal setResturantAddress2:address2Str];
            else
                [modal setResturantAddress2:@""];
            
            BOOL checkin = [[dict valueForKey:@"checkin"]boolValue];
            [modal setIsCheckedCheckIn:checkin];
            
            
            NSString *zipStr = [dict valueForKey:@"zip"];
            if(zipStr != (id)[NSNull null])
                [modal setResturantPostalCode:zipStr];
            else
                [modal setResturantPostalCode:@""];
            
            NSString *mobileStr = [dict valueForKey:@"international_phone_number"];
            if(mobileStr != (id)[NSNull null])
                [modal setResturantmobileNo:mobileStr];
            else
                [modal setResturantmobileNo:@""];
            NSString *Place_Count = [dict valueForKey:@"place_specials_count"];
            if(Place_Count != (id)[NSNull null])
                [modal setResturantState:Place_Count];
            else
                [modal setResturantState:@""];

            NSString *stateStr = [dict valueForKey:@"state"];
            if(stateStr != (id)[NSNull null])
                [modal setResturantState:stateStr];
            else
                [modal setResturantState:@""];
            
            NSString *countryStr = [dict valueForKey:@"country"];
            if(countryStr != (id)[NSNull null])
                [modal setResturantCountry:countryStr];
            else
                [modal setResturantCountry:@""];
            
            NSString *phone_numberStr = [dict valueForKey:@"phone_number"];
            if(phone_numberStr != (id)[NSNull null])
                [modal setResturantPhoneNo:phone_numberStr];
            else
                [modal setResturantPhoneNo:@""];
            
            NSString *place_imageStr = [dict valueForKey:@"place_image"];
            if(place_imageStr != (id)[NSNull null])
                [modal setPlaceImage:place_imageStr];
            else
                [modal setPlaceImage:@""];
            
            NSString *DetailPlace_imageStr = [dict valueForKey:@"place_imagebig"];
            if(DetailPlace_imageStr != (id)[NSNull null])
                [modal setDetailViewPlaceImage:DetailPlace_imageStr];
            else
                [modal setDetailViewPlaceImage:@""];
            
            NSString *open_fromStr = [dict valueForKey:@"open_from"];
            if(open_fromStr != (id)[NSNull null])
                [modal setOpenFrom:open_fromStr];
            else
                [modal setOpenFrom:@""];
            
            NSString *open_toStr = [dict valueForKey:@"open_to"];
            if(open_toStr != (id)[NSNull null])
                [modal setOpenTo:open_toStr];
            else
                [modal setOpenTo:@""];
            
            NSString *longitudeStr = [dict valueForKey:@"longitude"];
            if(longitudeStr != (id)[NSNull null])
                [modal setResturantLongtitude:longitudeStr];
            else
                [modal setResturantLongtitude:@""];
            
            NSString *latitudeStr = [dict valueForKey:@"latitude"];
            if(latitudeStr != (id)[NSNull null])
                [modal setResturantLatitude:latitudeStr];
            else
                [modal setResturantLatitude:@""];
            
            NSString *is_likeStr = [dict valueForKey:@"is_like"];
            if(is_likeStr != (id)[NSNull null])
                [modal setLikeORDislike:is_likeStr];
            else
                [modal setLikeORDislike:@""];
            
            NSString *like_countStr = [dict valueForKey:@"like_count"];
            if(like_countStr != (id)[NSNull null])
                [modal setLikecount:like_countStr];
            else
                [modal setLikecount:@""];
            
            NSString *dislike_countStr = [dict valueForKey:@"dislike_count"];
            if(dislike_countStr != (id)[NSNull null])
                [modal setDislikeCount:dislike_countStr];
            else
                [modal setDislikeCount:@""];
            
            BOOL is_favoriteBool = [[dict valueForKey:@"is_favorite"]boolValue];
            [modal setIsCheckedFavourite:is_favoriteBool];
            
            
            NSString *user_ratingsStr = [dict valueForKey:@"user_ratings"];
            if(user_ratingsStr != (id)[NSNull null])
                [modal setUserRatings:user_ratingsStr];
            else
                [modal setUserRatings:@""];
            
            NSString *place_ratingsStr = [dict valueForKey:@"place_ratings"]?[dict valueForKey:@"place_ratings"]:@"0";
            if(place_ratingsStr != (id)[NSNull null])
                [modal setPlaceRatings:[NSString stringWithFormat:@"%@",place_ratingsStr ]];
            else
                [modal setPlaceRatings:@""];
            
            NSString *emoji = [dict valueForKey:@"emoji1"];
            if(emoji != (id)[NSNull null])
                [modal setEmonjiID:emoji];
            else
                [modal setEmonjiID:@""];
            
            NSString *emoji2 = [dict valueForKey:@"emoji2"];
            if(emoji2 != (id)[NSNull null])
                [modal setEmonjiID2:emoji2];
            else
                [modal setEmonjiID2:@""];
            
            NSString *emoji3 = [dict valueForKey:@"emoji3"];
            if(emoji3 != (id)[NSNull null])
                [modal setEmonjiID3:emoji3];
            else
                [modal setEmonjiID3:@""];
            
            NSString *emoji4 = [dict valueForKey:@"emoji4"];
            if(emoji4 != (id)[NSNull null])
                [modal setEmonjiID4:emoji4];
            else
                [modal setEmonjiID4:@""];
            
            NSString *latestemonji = [dict valueForKey:@"latest_emoji"];
            if(latestemonji != (id)[NSNull null])
                [modal setLatestEmonji:latestemonji];
            else
                [modal setLatestEmonji:@""];
            
            NSString *emoji_time = [dict valueForKey:@"emoji_time"];
            if(emoji_time != (id)[NSNull null])
                [modal setEmonjiTime:emoji_time];
            else
                [modal setEmonjiTime:@""];
            
            NSString *total_reviews = [dict valueForKey:@"review_count"]?[dict valueForKey:@"review_count"]:@"0";
            if(total_reviews != (id)[NSNull null])
                [modal setTotalReview:total_reviews];
            else
                [modal setTotalReview:@""];
            
            
            NSString *happyHours_Count = [dict valueForKey:@"happy_hour_count"];
            if(happyHours_Count != (id)[NSNull null])
                [modal setHappy_hour_count:happyHours_Count];
            else
                [modal setHappy_hour_count:@"0"];
            
            
            NSString *placeSpecials_count = [dict valueForKey:@"place_specials_count"];
            if(placeSpecials_count != (id)[NSNull null])
                [modal setPlace_specials_count:placeSpecials_count];
            else
                [modal setPlace_specials_count:@"0"];
            
            NSString *distanceStr = [dict valueForKey:@"miles"];
            if(distanceStr != (id)[NSNull null])
                [modal setMile:distanceStr];
            else
                [modal setMile:@"0"];
            
            
            NSString *yelpStr = [dict valueForKey:@"yelp"];
            if(yelpStr != (id)[NSNull null])
                [modal setYelp_url:yelpStr];
            else
                [modal setYelp_url:@""];
            
            NSString *bearStr = [dict valueForKey:@"beer_menus"];
            if(bearStr != (id)[NSNull null])
                [modal setBeer_menus:bearStr];
            else
                [modal setBeer_menus:@""];
            
            NSString *openTable = [dict valueForKey:@"open_table"];
            if(openTable != (id)[NSNull null])
                [modal setOpen_table:openTable];
            else
                [modal setOpen_table:@""];
            
            NSString *grubhub = [dict valueForKey:@"grubhub"];
            if(grubhub != (id)[NSNull null])
                [modal setGrub_hub:grubhub];
            else
                [modal setGrub_hub:@""];
            
            
            NSString *placeDistance = [dict valueForKey:@"miles"];
            if(grubhub != (id)[NSNull null])
                [modal setDistance:placeDistance];
            else
                [modal setDistance:@""];
            
        }
        [modalArray addObject:modal];
    }
    
    
    return modalArray;
}




-(NSMutableArray *)setListAllHotelPromotionsValue :(id)arg
{
    PromtionsModal *modal;
    NSMutableArray *modalArray = [[NSMutableArray alloc]init];
    
    NSArray *dictArray = [arg valueForKey:@"promotion_lists"];
    
    for(NSDictionary *dict in dictArray)
    {
        modal = [[PromtionsModal alloc]init];
        if(dict != nil)
        {
            
            NSString *promotion_idStr = [dict valueForKey:@"promotion_id"];
            if(promotion_idStr != (id)[NSNull null])
                [modal setPromotionID:promotion_idStr];
            else
                [modal setPromotionID:@""];
            
            NSString *promotion_titleStr = [dict valueForKey:@"promotion_title"];
            if(promotion_titleStr != (id)[NSNull null])
                [modal setPromotionTitle:promotion_titleStr];
            else
                [modal setPromotionTitle:@""];
            
            
            NSString *promotion_typeStr = [dict valueForKey:@"promotion_type"];
            if(promotion_typeStr != (id)[NSNull null])
                [modal setPromotion_Type:promotion_typeStr];
            else
                [modal setPromotion_Type:@""];

            
            
            NSString *place_idStr = [dict valueForKey:@"place_id"];
            if(place_idStr != (id)[NSNull null])
                [modal setPlaceID:place_idStr];
            else
                [modal setPlaceID:@""];
            NSString *Place_Count = [dict valueForKey:@"place_specials_count"];
            if(Place_Count != (id)[NSNull null])
                [modal setSpecial_count:Place_Count];
            else
                [modal setSpecial_count:@""];

            
            NSString *descriptionStr = [dict valueForKey:@"description"];
            if(descriptionStr != (id)[NSNull null])
                [modal setPromotionDesc:descriptionStr];
            else
                [modal setPromotionDesc:@""];
            
            NSString *promotion_imageStr = [dict valueForKey:@"promotion_image"];
            if(promotion_imageStr != (id)[NSNull null])
                [modal setPromotionImage:promotion_imageStr];
            else
                [modal setPromotionImage:@""];
            
            
            NSString *created_dateStr = [dict valueForKey:@"created_date"];
            if(created_dateStr != (id)[NSNull null])
                [modal setPromotionCreatedDate:created_dateStr];
            else
                [modal setPromotionCreatedDate:@""];
            
            NSString *start_dateStr = [dict valueForKey:@"start_date"];
            if(start_dateStr != (id)[NSNull null])
                [modal setPromotionStartDate:start_dateStr];
            else
                [modal setPromotionStartDate:@""];
            
            NSString *expired_dateStr = [dict valueForKey:@"expired_date"];
            if(expired_dateStr != (id)[NSNull null])
                [modal setPromotionExpiredDate:expired_dateStr];
            else
                [modal setPromotionExpiredDate:@""];
            
            NSString *favorite_satusStr = [dict valueForKey:@"favorite_satus"];
            if(favorite_satusStr != (id)[NSNull null])
                [modal setPromotionFavStatus:favorite_satusStr];
            else
                [modal setPromotionFavStatus:@""];
            
            
            NSString *longitudeStr = [dict valueForKey:@"longitude"];
            if(longitudeStr != (id)[NSNull null])
                [modal setPromotionLongtitude:longitudeStr];
            else
                [modal setPromotionLongtitude:@""];
            
            NSString *latitudeStr = [dict valueForKey:@"latitude"];
            if(latitudeStr != (id)[NSNull null])
                [modal setPromotionLatitude:latitudeStr];
            else
                [modal setPromotionLatitude:@""];
            
            NSString *place_typeStr = [dict valueForKey:@"place_type"];
            if(place_typeStr != (id)[NSNull null])
                [modal setPromotionPlaceType:place_typeStr];
            else
                [modal setPromotionPlaceType:@""];
            
            
            NSString *email_idStr = [dict valueForKey:@"website"];
            if(email_idStr != (id)[NSNull null])
                [modal setPromotionPlaceEmail:email_idStr];
            else
                [modal setPromotionPlaceEmail:@""];
            
            NSString *cityStr = [dict valueForKey:@"city"];
            if(cityStr != (id)[NSNull null])
                [modal setPromotionPlaceCity:cityStr];
            else
                [modal setPromotionPlaceCity:@""];
            
            NSString *address1Str = [dict valueForKey:@"address"];
            if(address1Str != (id)[NSNull null])
                [modal setPromotionPlaceAddress1:address1Str];
            else
                [modal setPromotionPlaceAddress1:@""];
            
            NSString *address2Str = [dict valueForKey:@"address2"];
            if(address2Str != (id)[NSNull null])
                [modal setPromotionPlaceAddress2:address2Str];
            else
                [modal setPromotionPlaceAddress2:@""];
            
            NSString *zipStr = [dict valueForKey:@"zip"];
            if(zipStr != (id)[NSNull null])
                [modal setPromotionPlaceZip:zipStr];
            else
                [modal setPromotionPlaceZip:@""];
            
            NSString *mobileStr = [dict valueForKey:@"mobile"];
            if(mobileStr != (id)[NSNull null])
                [modal setPromotionPlaceMobileNo:mobileStr];
            else
                [modal setPromotionPlaceMobileNo:@""];
            
            NSString *stateStr = [dict valueForKey:@"state"];
            if(stateStr != (id)[NSNull null])
                [modal setPromotionPlaceState:stateStr];
            else
                [modal setPromotionPlaceState:@""];
            
            NSString *countryStr = [dict valueForKey:@"country"];
            if(countryStr != (id)[NSNull null])
                [modal setPromotionPlacecountry:countryStr];
            else
                [modal setPromotionPlacecountry:@""];
            
            NSString *phone_numberStr = [dict valueForKey:@"phone_number"];
            if(phone_numberStr != (id)[NSNull null])
                [modal setPromotionPlacePhoneNo:phone_numberStr];
            else
                [modal setPromotionPlacePhoneNo:@""];
            
            NSString *open_fromStr = [dict valueForKey:@"open_from"];
            if(open_fromStr != (id)[NSNull null])
                [modal setPromotionPlaceOpenFrom:open_fromStr];
            else
                [modal setPromotionPlaceOpenFrom:@""];
            
            NSString *open_toStr = [dict valueForKey:@"open_to"];
            if(open_toStr != (id)[NSNull null])
                [modal setPromotionPlaceOpenTo:open_toStr];
            else
                [modal setPromotionPlaceOpenTo:@""];
            
            NSString *titleStr = [dict valueForKey:@"title"];
            if(titleStr != (id)[NSNull null])
                [modal setPromotionPlaceTitle:titleStr];
            else
                [modal setPromotionPlaceTitle:@""];
            
            NSString *place_imageStr = [dict valueForKey:@"place_image"];
            if(place_imageStr != (id)[NSNull null])
                [modal setPromotionPlaceImage:place_imageStr];
            else
                [modal setPromotionPlaceImage:@""];
            
            
            NSString *place_Distance = [dict valueForKey:@"miles"];
            if(place_Distance != (id)[NSNull null])
                [modal setPromotionDistance:place_Distance];
            else
                [modal setPromotionDistance:@""];
            
            
            [modal setPromotionPlaceRating:[dict valueForKey:@"rating"]?[dict valueForKey:@"rating"]:@"0"];
            
        }
        [modalArray addObject:modal];
    }
    return modalArray;
}

#pragma mark --- Favourite Place ISL

- (void)addFavouriteSpotlyteToServer:(PromtionsModal *)userdetails  withCompletion:(void(^)(id obj1))completionBlock
{
    NSDictionary *dict = @{@"promotion_id":userdetails.promotionID,@"user_id":userdetails.user_ID};
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    
    [ServiceHelper sendRequestToServerWithParameters:@"AddSpotyteFavouites.json" forhttpMethod:@"POST" withrequestBody:jsonString sync:YES completion:^(id obj) {
        if ([obj isKindOfClass:[NSDictionary class]]) {
            ResturantModal *modal = [[ResturantModal alloc]init];
            [modal setResult:[obj valueForKey:@"result"]];
            [modal setMessage:[obj valueForKey:@"message"]];
            if (completionBlock) {
                completionBlock(modal);
            }
        }
        else{
            completionBlock(obj);
        }
        
    }];
}

#pragma mark --- UnFavourite Place ISL

- (void)unFavouriteSpotlyteToServer:(PromtionsModal *)userdetails  withCompletion:(void(^)(id obj1))completionBlock
{
    NSDictionary *dict = @{@"promotion_id":userdetails.promotionID,@"user_id":userdetails.user_ID};
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    
    [ServiceHelper sendRequestToServerWithParameters:@"AddSpotyteUnFavouites.json" forhttpMethod:@"POST" withrequestBody:jsonString sync:YES completion:^(id obj) {
        if ([obj isKindOfClass:[NSDictionary class]]) {
            ResturantModal *modal = [[ResturantModal alloc]init];
            [modal setResult:[obj valueForKey:@"result"]];
            [modal setMessage:[obj valueForKey:@"message"]];
            if (completionBlock) {
                completionBlock(modal);
            }
        }
        else{
            completionBlock(obj);
        }
    }];
}


@end
