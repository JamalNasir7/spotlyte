//
//  ResturantModal.h
//  Spotlyte
//
//  Created by Admin on 24/03/16.
//  Copyright © 2016 Naveen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "ServiceHelper.h"

#define RestaurantMacro [ResturantModal sharedUserProfile]

@interface ResturantModal : NSObject



@property (nonatomic, retain) UIImage *appIcon;


//// for promotions
@property (strong,nonatomic) NSString *Tabs;
@property (strong, nonatomic) NSString *Span;
@property (strong, nonatomic) NSString *promotionID;
@property (strong, nonatomic) NSString *promotionTitle;
@property (strong, nonatomic) NSString *promotionImage;
@property (strong, nonatomic) NSString *isCheckedPromotionFav;
/////////////////////


@property (nonatomic, retain) NSString *resturantName;
@property (nonatomic, retain) NSString *resturantAddress;
@property (nonatomic, retain) NSString *resturantAddress2;

@property (nonatomic, retain) NSString *resturantCity;
@property (nonatomic, retain) NSString *resturantState;
@property (nonatomic, retain) NSString *resturantCountry;
@property (nonatomic, retain) NSString *resturantPostalCode;
@property (nonatomic, retain) NSString *resturantPhoneNo;
@property (nonatomic, retain) NSString *resturantmobileNo;

@property (nonatomic, retain) NSString *resturantHours;
@property (nonatomic, retain) NSString *resturantEMail;
@property (nonatomic, retain) NSString *resturantDescription;
@property (nonatomic ,retain) NSString *strWebsite;

@property (nonatomic, retain) NSString *resturantLatitude;
@property (nonatomic, retain) NSString *resturantLongtitude;
@property (nonatomic, retain) NSString *resturantRadius;

@property (nonatomic, retain) NSString *placeType;
@property (nonatomic, retain) NSString *ratingType;
@property (strong, nonatomic) NSString *message;
@property (strong, nonatomic) NSString *result;


@property (strong, nonatomic) NSString *placeID;
@property (strong, nonatomic) NSString *emonjiID;
@property (strong, nonatomic) NSString *emonjiID2;
@property (strong, nonatomic) NSString *emonjiID3;
@property (strong, nonatomic) NSString *emonjiID4;
@property (strong, nonatomic) NSString *latestEmonji;
@property (strong, nonatomic) NSString *UserRating_comment;
@property (strong,nonatomic) NSString *userGivenRating;
@property (strong, nonatomic) NSString *minDistance;


@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *subTitle;
@property (strong, nonatomic) NSString *openFrom;
@property (strong, nonatomic) NSString *openTo;
@property (strong, nonatomic) NSString *placeImage;
@property (strong, nonatomic) NSString *DetailViewPlaceImage;
@property (strong, nonatomic) NSString *userID;
@property (strong, nonatomic) NSString *likeORDislike;
@property (strong, nonatomic) NSString *strTotalRecordsFound;
@property (strong, nonatomic) NSString *strOffset;
@property (strong, nonatomic) NSString *likecount;
@property (strong, nonatomic) NSString *dislikeCount;
@property BOOL isCheckedFavourite;
@property BOOL isCheckedCheckIn;
@property (strong, nonatomic) NSString *checkInStr;
@property (strong,nonatomic) NSString *comment_for_checkin;
// SW NW

@property (strong ,nonatomic) NSString *strSW_Lat;
@property (strong ,nonatomic) NSString *strSW_Long;
@property (strong ,nonatomic) NSString *strNE_Lat;
@property (strong ,nonatomic) NSString *strNE_Long;





@property (strong, nonatomic) NSString *placeRatings;
@property (strong, nonatomic) NSString *userRatings;
@property (strong, nonatomic) NSString *reviewID;
@property (strong, nonatomic) NSString *comments;
@property (strong, nonatomic) NSString *commentsImage;
@property (strong, nonatomic) NSString *is_Emoji;
@property (strong, nonatomic) NSString *createdOn;
@property (strong, nonatomic) NSString *totalReview;
@property (strong, nonatomic) NSString *systemMessage;
@property (strong, nonatomic) NSString *cmdNumber;
@property (strong, nonatomic) NSString *UserRatingTime;


@property (strong, nonatomic) NSString *firstName;
@property (strong, nonatomic) NSString *lastName;
@property (strong, nonatomic) NSString *userName;
@property (strong, nonatomic) NSString *profileImage;
@property (nonatomic, retain) UIImage *resturantImage;
@property (nonatomic, retain) NSString *resturantImage1;

@property (strong, nonatomic) NSString *specials_ID;
@property (strong, nonatomic) NSString *specials_title;
@property (strong, nonatomic) NSString *specials_weekDay;
@property (strong, nonatomic) NSString *ratingValueINNumber;
@property (strong, nonatomic) NSString *emonjiTime;
@property (strong, nonatomic) NSString *startTime;
@property (strong, nonatomic) NSString *endTime;

@property (strong, nonatomic) NSString *is_LiveMap;
@property (strong, nonatomic) NSString *is_DailySpecials;
@property (strong, nonatomic) NSString *is_HappyHours;
@property (strong, nonatomic) NSString *happHoursDay;
@property (strong, nonatomic) NSString *dailySpecialsDay;
@property (strong, nonatomic) NSString *top_spot_id;
@property (strong, nonatomic) NSString *isHighlights;

@property (strong, nonatomic) NSString *mile;
@property (strong, nonatomic) NSString *happyHours;
@property (strong, nonatomic) NSString *time;
@property (strong , nonatomic) NSString *dateTime;
@property (strong ,nonatomic) NSString *CityOrArea;
@property (strong, nonatomic) NSString *statusCityOrAreas;
@property (strong, nonatomic) NSString *statusCity;
@property (strong, nonatomic) NSString *webSite_Specials;
@property (strong, nonatomic) NSString *Day_name;
@property BOOL isCheckedFavSpecial;
//new
@property(strong,nonatomic) NSString *isCommentAdded;
@property(strong,nonatomic) NSString *isRated;


///// extra added parametre
@property (strong, nonatomic) NSString *happy_hour_count;
@property (strong, nonatomic) NSString *place_specials_count;

@property (strong, nonatomic) NSString *yelp_url;
@property (strong, nonatomic) NSString *open_table;
@property (strong, nonatomic) NSString *beer_menus;
@property (strong, nonatomic) NSString *grub_hub;


@property (strong, nonatomic) NSMutableArray *arrayPlaceFavourites;
@property (strong, nonatomic) NSMutableArray *arrayHotelFavourites;

@property (strong, nonatomic) NSArray *arrayOthers;
@property (strong, nonatomic) NSArray *arrayBars;
@property (strong, nonatomic) NSArray *arrayResturants;

@property (strong, nonatomic) NSArray *arraySpecials;
@property (strong, nonatomic) NSArray *arrayHappyHours;

@property (strong, nonatomic) NSArray *arrayHappyHours_Monday;
@property (strong, nonatomic) NSArray *arrayHappyHours_Tuesday;
@property (strong, nonatomic) NSArray *arrayHappyHours_Wednesday;
@property (strong, nonatomic) NSArray *arrayHappyHours_Thursday;
@property (strong, nonatomic) NSArray *arrayHappyHours_Friday;
@property (strong, nonatomic) NSArray *arrayHappyHours_Saturday;
@property (strong, nonatomic) NSArray *arrayHappyHours_Sunday;
@property (strong, nonatomic) NSString *PlaceName;

@property (strong, nonatomic) NSString *price;
@property (strong, nonatomic) NSString *rating;
@property (strong, nonatomic) NSString *distance;
@property (strong, nonatomic) NSString *homeVCStatus;
@property (strong, nonatomic) NSString *cityId;

@property BOOL isCheckedModalPrice;
@property BOOL isCheckedModalMile;
@property BOOL isCheckedModalTime;
@property BOOL isCheckedModalCity;
@property BOOL isCheckedModalDay;


@property NSInteger intMile;
@property NSInteger intPrice;
@property NSInteger intTime;
@property NSInteger intCity;
@property NSInteger intDay;


//HighLight

@property (strong, nonatomic) NSArray *arrHighlight;
@property (strong, nonatomic) NSString *main_id;
@property (strong, nonatomic) NSString *HighlightID;
@property (strong, nonatomic) NSString *Highlight_Status;
@property (strong, nonatomic) NSString *HighlightName;

- (void)listAllMapValuesToServer:(ResturantModal *)userdetails  withCompletion:(void(^)(id obj1))completionBlock;
- (void)likeOrDislikeToServer:(ResturantModal *)userdetails  withCompletion:(void(^)(id obj1))completionBlock;
- (void)favouriteToServer:(ResturantModal *)userdetails  withCompletion:(void(^)(id obj1))completionBlock;
- (void)unFavouriteToServer:(ResturantModal *)userdetails  withCompletion:(void(^)(id obj1))completionBlock;
- (void)ratingGivenToServer:(ResturantModal *)userdetails  withCompletion:(void(^)(id obj1))completionBlock;
- (void)listAllCommentsValuesToServer:(ResturantModal *)userdetails  withCompletion:(void(^)(id obj1))completionBlock;
- (void)postCommentsToServer:(ResturantModal *)userdetails  withCompletion:(void(^)(id obj1))completionBlock;
- (void)postImagesToServer:(ResturantModal *)userdetails  withCompletion:(void(^)(id obj1))completionBlock;
- (void)listAllDailySpecialsFromServer:(ResturantModal*)userdetails withCompletion:(void (^)(id obj1))completionBlock;
- (void)listAllFavouriteToServer:(ResturantModal *)userdetails  withCompletion:(void(^)(id obj1))completionBlock;
- (void)listTopRatedToServer:(ResturantModal *)userdetails  withCompletion:(void(^)(id obj1))completionBlock;

- (void)listAllcommentsImageToServer:(ResturantModal *)userdetails  withCompletion:(void(^)(id obj1))completionBlock;
- (void)listAllHotelcommentsImageToServer:(ResturantModal *)userdetails  withCompletion:(void(^)(id obj1))completionBlock;
- (void)checkInToServer:(ResturantModal *)userdetails  withCompletion:(void(^)(id obj1))completionBlock;
- (void)retriveUserLevels:(ResturantModal *)userdetails  withCompletion:(void(^)(id obj1))completionBlock;
- (void)setEmonjiToServer:(ResturantModal *)userdetails  withCompletion:(void(^)(id obj1))completionBlock;
+ (ResturantModal *)sharedUserProfile;
- (void)getEmonjiTimeToServer:(ResturantModal *)userdetails  withCompletion:(void(^)(id obj1))completionBlock;
- (void)listAllSearchData:(ResturantModal *)userdetails  withCompletion:(void(^)(id obj1))completionBlock;
- (void)listAllAdvancefilterData:(ResturantModal *)userdetails  withCompletion:(void(^)(id obj1))completionBlock;
- (void)getSpecialsHappyHoursToServer:(ResturantModal *)userdetails  withCompletion:(void(^)(id obj1))completionBlock;
- (void)getSpecialsHappyHoursToServer_Special:(ResturantModal *)userdetails  withCompletion:(void(^)(id obj1))completionBlock;
- (void)listAllLikesDislikeToServer:(ResturantModal *)userdetails  withCompletion:(void(^)(id obj1))completionBlock;
- (void)checkInsToServer:(ResturantModal *)userdetails  withCompletion:(void(^)(id obj1))completionBlock;
- (void)commentsToServer:(ResturantModal *)userdetails  withCompletion:(void(^)(id obj1))completionBlock;
- (void)ratingsToServer:(ResturantModal *)userdetails  withCompletion:(void(^)(id obj1))completionBlock;
- (void)likeAndDislikeSpecialFavoriteToServer:(ResturantModal *)userdetails  withCompletion:(void(^)(id obj1))completionBlock;
- (void)favUnfavSpecialToServer:(ResturantModal *)userdetails :(NSString *)strFav :(NSString *)strType  withCompletion:(void(^)(id obj1))completionBlock;
- (void)GetHighlightsOfPlace:(ResturantModal *)userdetails  withCompletion:(void(^)(id obj1))completionBlock;
    
@end
