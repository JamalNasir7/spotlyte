//
//  RegisterModal.h
//  Spotlyte
//
//  Created by Admin on 28/03/16.
//  Copyright © 2016 Naveen. All rights reserved.
//

#import <Foundation/Foundation.h>
#define REGISTERMACRO [RegisterModal sharedUserProfile]
#define REGISTERINFO [REGISTERMACRO userInfo]
@interface RegisterModal : NSObject

@property (strong, nonatomic) NSString *emailID;
@property (strong, nonatomic) NSString * password;
@property (strong, nonatomic) NSString *userName;
@property (strong, nonatomic) NSString *deviceID;
@property (strong, nonatomic) NSString *deviceType;
@property (strong, nonatomic) NSString *deviceToken;
@property (strong, nonatomic) NSString *firstName;
@property (strong, nonatomic) NSString *lastName;
@property (strong, nonatomic) NSString * address1;
@property (strong, nonatomic) NSString *address2;
@property (strong, nonatomic) NSString *city;
@property (strong, nonatomic) NSString *CityorArea;
@property (strong, nonatomic) NSString *state;
@property (strong, nonatomic) NSString *stateID;
@property (strong, nonatomic) NSString *cityID;

@property (strong, nonatomic) NSString *gender;
@property (strong, nonatomic) NSString *dateOfBirth;

@property (strong, nonatomic) NSString *login_status;


@property (strong, nonatomic) NSString *country;
@property (strong, nonatomic) NSString *countryID;

@property (strong, nonatomic) NSString *zipCode;
@property (strong, nonatomic) NSString *mobileNo;
@property (strong, nonatomic) NSString *phoneNo;
@property (strong, nonatomic) NSString *message;
@property (strong, nonatomic) NSString *result;
@property (strong, nonatomic) NSString *userID;
@property (strong, nonatomic) NSString *friendsUserId;

@property (strong, nonatomic) NSString *profileImage;

@property (strong, nonatomic) NSString *placeID;
@property (strong, nonatomic) NSString *placeName;
@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *subTitle;
@property (strong, nonatomic) NSString *placeType;
@property (strong, nonatomic) NSString *openFrom;
@property (strong, nonatomic) NSString *openTo;
@property (strong, nonatomic) NSString *latitude;
@property (strong, nonatomic) NSString *longitude;
@property (strong, nonatomic) NSString *placeImage;
@property (strong, nonatomic) NSString *descr;

@property (strong, nonatomic) NSString *categoryName;
@property (strong, nonatomic) NSArray *profileCommentsArray;
@property (strong, nonatomic) NSArray *UsersCommentsArray;

@property (strong, nonatomic) NSArray *prefferedCategoryArray;
@property (strong, nonatomic) NSString *reviewID;
@property (strong, nonatomic) NSString *comments;
@property (strong, nonatomic) NSString *commentsImage;
@property (strong, nonatomic) NSString *is_Emoji;
@property (strong, nonatomic) NSString *createdOn;
@property (strong, nonatomic) NSString *followerID;
@property (strong, nonatomic) NSString *followerCount;
@property (strong, nonatomic) NSString *followingCount;
@property (strong, nonatomic) NSString *statusCity;
@property (strong, nonatomic) NSString *CommentedUser_name;
@property (strong, nonatomic) NSString *CommentedUser_Image;

@property (strong, nonatomic) NSString *socialName;
@property (strong, nonatomic) NSString *socialID;
@property (strong, nonatomic) NSArray *followersArray;
@property (strong, nonatomic) NSArray *followingsArray;
@property (strong, nonatomic) NSString *cityId;


@property (strong, nonatomic) NSString *checkin_count;
@property (strong, nonatomic) NSString *comment_count;
@property (strong, nonatomic) NSString *dislike_count;
@property (strong, nonatomic) NSString *favorites_count;
@property (strong, nonatomic) NSString *special_count;
@property (strong, nonatomic) NSString *like_count;
@property (strong, nonatomic) NSString *photo_count;
@property (strong, nonatomic) NSString *rating;



@property BOOL isCheckedFollower;


//@property (strong, nonatomic) NSArray *profileImage;


- (void)registerToServer:(RegisterModal *)userdetails  withCompletion:(void(^)(id obj1))completionBlock;
- (void)forgotPasswordToServer:(NSString *)emailStr  withCompletion:(void(^)(id obj1))completionBlock;
- (void)loginToServer:(RegisterModal *)userdetails  withCompletion:(void(^)(id obj1))completionBlock;
- (void)editAccountToServer:(RegisterModal *)userdetails  withCompletion:(void(^)(id obj1))completionBlock;
- (void)viewAccountToServer:(NSString *)user_IDArg  withCompletion:(void(^)(id obj1))completionBlock;
+ (RegisterModal *)sharedUserProfile;
-(void)encodeWithCoder:(NSCoder *)encoder;
-(id)initWithCoder:(NSCoder *)decoder;
- (void)addFollowerToServer:(RegisterModal *)userdetails  withCompletion:(void(^)(id obj1))completionBlock;
- (void)addUnFollowerToServer:(RegisterModal *)userdetails  withCompletion:(void(^)(id obj1))completionBlock;
- (void)listallCountryServer:(RegisterModal *)userdetails  withCompletion:(void(^)(id obj1))completionBlock;

- (void)faceBookLoginToServer:(RegisterModal *)userdetails  withCompletion:(void(^)(id obj1))completionBlock;
- (void)listallCategoryServer:(RegisterModal *)userdetails  withCompletion:(void(^)(id obj1))completionBlock;
- (void)listallFollowersAndFollowingsServer:(NSString *)userIDStr  withCompletion:(void(^)(id obj1))completionBlock;
- (void)listallCityServer:(RegisterModal *)userdetails  withCompletion:(void(^)(id obj1))completionBlock;
- (void)getDashboardCount:(NSString *)userIDStr  withCompletion:(void(^)(id obj1))completionBlock;
@end
