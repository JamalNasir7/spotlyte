//
//  UserLevelProgressModal.h
//  Spotlyte
//
//  Created by Admin on 10/05/16.
//  Copyright © 2016 Naveen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserLevelProgressModal : NSObject

@property (strong, nonatomic) NSString *userLevel;
@property (strong, nonatomic) NSString *userLevelName;
@property (strong, nonatomic) NSString *userLevelAchievement;

@property (strong, nonatomic) NSString *achievementsName;
@property (strong, nonatomic) NSString *achievementsURL;


@property (strong, nonatomic) NSMutableArray *arrayAchievements;

@end
