
//
//  EventsModel.m
//  Spotlyte
//
//  Created by CIPL137-MOBILITY on 18/04/16.
//  Copyright © 2016 Naveen. All rights reserved.
//

#import "EventsModel.h"

@implementation EventsModel
@synthesize eventName, eventImage, eventTime,eventToTime;
@synthesize venue;
@synthesize typeOFPlaces;
@synthesize eventCreatedDate,eventEndDate,eventImageStr,eventMessage,eventResult;
@synthesize eventStartDate,categoryID,categoryName,eventPlace,eventDescription;
@synthesize eventCity,eventCountry,eventState;
@synthesize userID,eventAddress,specialsArray;
@synthesize specials_weekDay,dayArray;
@synthesize sundayStr,mondayStr,wednesdayStr,thursdayStr;
@synthesize fridayStr,saturdayStr,phoneNo,EventWebsite;
//

+ (EventsModel *)sharedUserProfile
{
    @synchronized([EventsModel class])
    {
        static EventsModel *_sharedUsers;
        if (!_sharedUsers)
        {
            _sharedUsers=[[EventsModel alloc] init];
        }
        return _sharedUsers;
    }
    return nil;
}




#pragma mark -- Events List Service Helper
- (void)eventsListToServer:(EventsModel *)userdetails  withCompletion:(void(^)(id obj1))completionBlock
{
    NSString* latitude=[NSString stringWithFormat:@"%f",APP_DELEGATE.userCurrentLocation.coordinate.latitude];
    NSString* longitude=[NSString stringWithFormat:@"%f",APP_DELEGATE.userCurrentLocation.coordinate.longitude];
    
    NSDictionary *dict = @{@"category_id":userdetails.categoryID,@"date":userdetails.eventTime,@"user_id":userdetails.userID,@"current_latitude":latitude,@"current_longitude":longitude,@"city":userdetails.searchByCity,@"end_date":userdetails.eventToTime};
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    [ServiceHelper sendRequestToServerWithParameters:@"RetrieveAllEvents.json" forhttpMethod:@"POST" withrequestBody:jsonString sync:YES completion:^(id obj) {
        NSLog(@" objectImage == %@",obj);
        if ([obj isKindOfClass:[NSDictionary class]]) {
            EventsModel *modal = [[EventsModel alloc]init];
            [modal setEventResult:[obj valueForKey:@"result"]];
            [modal setEventMessage:[obj valueForKey:@"message"]];
            
            if([[obj valueForKey:@"result"] isEqualToString:@"success"])
            {
                modal.eventArray = [self setcommentsImageValue:obj];
                modal.placesArray =[self setListAllValue:obj];
            }
            if (completionBlock) {
                completionBlock(modal);
                modal=nil;
            }
        }
        else{
            completionBlock(obj);
            obj=nil;
        }
    }];
}




#pragma mark -- Events List Service Helper
- (void)cityListToServer:(EventsModel *)userdetails  withCompletion:(void(^)(id obj1))completionBlock
{
    NSDictionary *dict;
    if (userdetails.eventTime) {
        dict = @{@"category_id":userdetails.categoryID,@"date":userdetails.eventTime,@"end_date":userdetails.eventToTime};
    }else
        dict = @{@"category_id":userdetails.categoryID};
    
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    
    [ServiceHelper sendRequestToServerWithParameters:@"searchEventByCity.json" forhttpMethod:@"POST" withrequestBody:jsonString sync:YES completion:^(id obj) {
        if ([obj isKindOfClass:[NSArray class]]) {
         
            
            NSArray *cityArray=obj;
            
            if (cityArray.count>0) {
                NSMutableArray *cities=[NSMutableArray new];
                for (NSDictionary *cityDict in cityArray) {
                    [cities addObject:cityDict[@"city"]];
                }
                completionBlock(cities);
                cities=nil;
            }
            else{
                completionBlock(obj);
                obj=nil;
            }
          
        }
        else{
            completionBlock(obj);
            obj=nil;
        }
        
    }];
}

-(NSMutableArray *)setListAllValue :(id)arg{
    ResturantModal *modal;
    NSMutableArray *modalArray = [[NSMutableArray alloc]init];
    NSArray *dictArray;
    dictArray = [arg valueForKey:@"place_details"];
    
    for(NSDictionary *dict in dictArray)
    {
        modal = [[ResturantModal alloc]init];
        
        if(dict != nil)
        {
            NSString *place_idStr = [dict valueForKey:@"place_id"];
            if(place_idStr != (id)[NSNull null])
                [modal setPlaceID:place_idStr];
            else
                [modal setPlaceID:@""];
            
            NSString *titleStr = [dict valueForKey:@"title"];
            if(titleStr != (id)[NSNull null])
                [modal setResturantName:titleStr];
            else
                [modal setResturantName:@""];
            
            NSString *subtitleStr = [dict valueForKey:@"subtitle"];
            if(subtitleStr != (id)[NSNull null])
                [modal setSubTitle:subtitleStr];
            else
                [modal setSubTitle:@""];
            
            NSString *descriptionStr = [dict valueForKey:@"description"];
            if(descriptionStr != (id)[NSNull null])
                [modal setResturantDescription:descriptionStr];
            else
                [modal setResturantDescription:@""];
            
            NSString *place_typeStr = [dict valueForKey:@"place_type"];
            if(place_typeStr != (id)[NSNull null])
                [modal setPlaceType:place_typeStr];
            else
                [modal setPlaceType:@""];
            
            NSString *email_idStr = [dict valueForKey:@"website"];
            if(email_idStr != (id)[NSNull null])
                [modal setResturantEMail:email_idStr];
            else
                [modal setResturantEMail:@""];
            
            NSString *cityStr = [dict valueForKey:@"city"];
            if(cityStr != (id)[NSNull null])
                [modal setResturantCity:cityStr];
            else
                [modal setResturantCity:@""];
            
            NSString *address = [dict valueForKey:@"address"];
            NSString *address1Str=[[address componentsSeparatedByString:@","] objectAtIndex:0];
            
            if(address1Str != (id)[NSNull null])
                [modal setResturantAddress:address1Str];
            else
                [modal setResturantAddress:@""];
            
            NSString *address2Str = [dict valueForKey:@"address2"];
            if(address2Str != (id)[NSNull null])
                [modal setResturantAddress2:address2Str];
            else
                [modal setResturantAddress2:@""];
            
            NSString *zipStr = [dict valueForKey:@"zip"];
            if(zipStr != (id)[NSNull null])
                [modal setResturantPostalCode:zipStr];
            else
                [modal setResturantPostalCode:@""];
            
            NSString *mobileStr = [dict valueForKey:@"international_phone_number"];
            if(mobileStr != (id)[NSNull null])
                [modal setResturantmobileNo:mobileStr];
            else
                [modal setResturantmobileNo:@""];
            
            NSString *stateStr = [dict valueForKey:@"state"];
            if(stateStr != (id)[NSNull null])
                [modal setResturantState:stateStr];
            else
                [modal setResturantState:@""];
            
            NSString *countryStr = [dict valueForKey:@"country"];
            if(countryStr != (id)[NSNull null])
                [modal setResturantCountry:countryStr];
            else
                [modal setResturantCountry:@""];
            
            NSString *phone_numberStr = [dict valueForKey:@"phone_number"];
            if(phone_numberStr != (id)[NSNull null])
                [modal setResturantPhoneNo:phone_numberStr];
            else
                [modal setResturantPhoneNo:@""];
            
            NSString *place_imageStr = [dict valueForKey:@"place_image"];
            if(place_imageStr != (id)[NSNull null])
                [modal setPlaceImage:place_imageStr];
            else
                [modal setPlaceImage:@""];
            
            NSString *DetailPlace_imageStr = [dict valueForKey:@"place_imagebig"];
            if(DetailPlace_imageStr != (id)[NSNull null])
                [modal setDetailViewPlaceImage:DetailPlace_imageStr];
            else
                [modal setDetailViewPlaceImage:@""];
            
            NSString *open_fromStr = [dict valueForKey:@"open_from"];
            if(open_fromStr != (id)[NSNull null])
                [modal setOpenFrom:open_fromStr];
            else
                [modal setOpenFrom:@""];
            
            NSString *open_toStr = [dict valueForKey:@"open_to"];
            if(open_toStr != (id)[NSNull null])
                [modal setOpenTo:open_toStr];
            else
                [modal setOpenTo:@""];
            
            NSString *longitudeStr = [dict valueForKey:@"longitude"];
            if(longitudeStr != (id)[NSNull null])
                [modal setResturantLongtitude:longitudeStr];
            else
                [modal setResturantLongtitude:@""];
            
            NSString *latitudeStr = [dict valueForKey:@"latitude"];
            if(latitudeStr != (id)[NSNull null])
                [modal setResturantLatitude:latitudeStr];
            else
                [modal setResturantLatitude:@""];
            
            NSString *is_likeStr = [dict valueForKey:@"is_like"];
            if(is_likeStr != (id)[NSNull null])
                [modal setLikeORDislike:is_likeStr];
            else
                [modal setLikeORDislike:@""];
            
            NSString *like_countStr = [dict valueForKey:@"like_count"];
            if(like_countStr != (id)[NSNull null])
                [modal setLikecount:like_countStr];
            else
                [modal setLikecount:@""];
            
            NSString *dislike_countStr = [dict valueForKey:@"dislike_count"];
            if(dislike_countStr != (id)[NSNull null])
                [modal setDislikeCount:dislike_countStr];
            else
                [modal setDislikeCount:@""];
            
            BOOL is_favoriteBool = [[dict valueForKey:@"is_favorite"]boolValue];
            [modal setIsCheckedFavourite:is_favoriteBool];
            
            
            BOOL checkin = [[dict valueForKey:@"checkin"]boolValue];
            [modal setIsCheckedCheckIn:checkin];
            
            NSString *user_ratingsStr = [dict valueForKey:@"user_ratings"];
            if(user_ratingsStr != (id)[NSNull null])
                [modal setUserRatings:user_ratingsStr];
            else
                [modal setUserRatings:@""];
            
            NSString *place_ratingsStr = [dict valueForKey:@"place_ratings"];
            if(place_ratingsStr != (id)[NSNull null])
                [modal setPlaceRatings:[NSString stringWithFormat:@"%@",place_ratingsStr ]];
            else
                [modal setPlaceRatings:@""];
            
            NSString *emoji = [dict valueForKey:@"emoji1"];
            if(emoji != (id)[NSNull null])
                [modal setEmonjiID:emoji];
            else
                [modal setEmonjiID:@""];
            
            NSString *emoji2 = [dict valueForKey:@"emoji2"];
            if(emoji2 != (id)[NSNull null])
                [modal setEmonjiID2:emoji2];
            else
                [modal setEmonjiID2:@""];
            
            NSString *emoji3 = [dict valueForKey:@"emoji3"];
            if(emoji3 != (id)[NSNull null])
                [modal setEmonjiID3:emoji3];
            else
                [modal setEmonjiID3:@""];
            
            NSString *emoji4 = [dict valueForKey:@"emoji4"];
            if(emoji4 != (id)[NSNull null])
                [modal setEmonjiID4:emoji4];
            else
                [modal setEmonjiID4:@""];
            
            NSString *latestemonji = [dict valueForKey:@"latest_emoji"];
            if(latestemonji != (id)[NSNull null])
                [modal setLatestEmonji:latestemonji];
            else
                [modal setLatestEmonji:@""];
            
            NSString *emoji_time = [dict valueForKey:@"emoji_time"];
            if(emoji_time != (id)[NSNull null])
                [modal setEmonjiTime:emoji_time];
            else
                [modal setEmonjiTime:@""];
            
            NSString *total_reviews = [dict valueForKey:@"total_reviews"];
            if(total_reviews != (id)[NSNull null])
                [modal setTotalReview:total_reviews];
            else
                [modal setTotalReview:@""];
            
            
            NSString *happyHours_Count = [dict valueForKey:@"happy_hour_count"];
            if(happyHours_Count != (id)[NSNull null])
                [modal setHappy_hour_count:happyHours_Count];
            else
                [modal setHappy_hour_count:@"0"];
            
            
            NSString *placeSpecials_count = [dict valueForKey:@"place_specials_count"];
            if(placeSpecials_count != (id)[NSNull null])
                [modal setPlace_specials_count:placeSpecials_count];
            else
                [modal setPlace_specials_count:@"0"];
            
            NSString *distanceStr = [dict valueForKey:@"miles"];
            if(distanceStr != (id)[NSNull null])
                [modal setMile:distanceStr];
            else
                [modal setMile:@"0"];
            
            NSString *yelpStr = [dict valueForKey:@"yelp"];
            if(yelpStr != (id)[NSNull null])
                [modal setYelp_url:yelpStr];
            else
                [modal setYelp_url:@""];
            
            NSString *bearStr = [dict valueForKey:@"beer_menus"];
            if(bearStr != (id)[NSNull null])
                [modal setBeer_menus:bearStr];
            else
                [modal setBeer_menus:@""];
            
            NSString *openTable = [dict valueForKey:@"open_table"];
            if(openTable != (id)[NSNull null])
                [modal setOpen_table:openTable];
            else
                [modal setOpen_table:@""];
            
            NSString *grubhub = [dict valueForKey:@"grubhub"];
            if(grubhub != (id)[NSNull null])
                [modal setGrub_hub:grubhub];
            else
                [modal setGrub_hub:@""];
            
            
            NSString *placeDistance = [dict valueForKey:@"miles"];
            if(grubhub != (id)[NSNull null])
                [modal setDistance:placeDistance];
            else
                [modal setDistance:@""];
            
        }
        [modalArray addObject:modal];
         modal=nil;
    }
    
    
    return modalArray;
}



-(NSMutableArray *)setcommentsImageValue :(id)arg
{
    EventsModel *modal;
    NSMutableArray *modalArray = [[NSMutableArray alloc]init];
    NSArray *dictArray = [arg valueForKey:@"data"];
    
    for(NSDictionary *dict in dictArray)
    {
        modal = [[EventsModel alloc]init];
        if(dict != nil)
        {
            
            NSString *event_title = [dict valueForKey:@"event_title"];
            if(event_title != (id)[NSNull null])
                [modal setEventName:event_title];
            else
                [modal setEventName:@""];
            
            NSString *category_name = [dict valueForKey:@"category_name"];
            if(category_name != (id)[NSNull null])
                [modal setCategoryName:category_name];
            else
                [modal setCategoryName:@""];
            
            NSString *place = [dict valueForKey:@"place"];
            if(place != (id)[NSNull null])
                [modal setEventPlace:place];
            else
                [modal setEventPlace:@""];
            
            NSString *commend = [dict valueForKey:@"description"];
            if(place != (id)[NSNull null])
                [modal setEventDescription:commend];
            else
                [modal setEventDescription:@""];
            
            
            NSString *events_image = [dict valueForKey:@"events_image"];
            if(events_image != (id)[NSNull null])
            {
                events_image = [events_image stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
                [modal setEventImageStr:events_image];
            }
            else
                [modal setEventImageStr:@""];
            
            
            NSString *start_date = [dict valueForKey:@"start_date"];
            if(start_date != (id)[NSNull null])
                [modal setEventStartDate:start_date];
            else
                [modal setEventStartDate:@""];
            
            NSString *end_date = [dict valueForKey:@"end_date"];
            if(end_date != (id)[NSNull null])
                [modal setEventEndDate:end_date];
            else
                [modal setEventEndDate:@""];
            
            NSString *created_date = [dict valueForKey:@"created_date"];
            if(created_date != (id)[NSNull null])
                [modal setEventCreatedDate:created_date];
            else
                [modal setEventCreatedDate:@""];
            
            
            NSString *address = [dict valueForKey:@"location"];
            if(address != (id)[NSNull null])
                [modal setEventAddress:address];
            else
                [modal setEventAddress:@""];
            
            NSString *web = [dict valueForKey:@"website"];
            if(web != (id)[NSNull null])
                [modal setEventWebsite:web];
            else
                [modal setEventAddress:@""];
            
            NSString *city = [dict valueForKey:@"city"];
            if(city != (id)[NSNull null])
                [modal setEventCity:city];
            else
                [modal setEventCity:@""];
            
            NSString *state = [dict valueForKey:@"state"];
            if(state != (id)[NSNull null])
                [modal setEventState:state];
            else
                [modal setEventState:@""];
            
            NSString *country = [dict valueForKey:@"country"];
            if(country != (id)[NSNull null])
                [modal setEventCountry:country];
            else
                [modal setEventCountry:@""];
            
            NSString *phone = [dict valueForKey:@"phone"];
            if(phone != (id)[NSNull null])
                [modal setPhoneNo:phone];
            else
                [modal setPhoneNo:@""];
            
            NSMutableArray *arrSpecials = [[NSMutableArray alloc]init];
            
            NSString *sunday = [dict valueForKey:@"sunday"];
            if(sunday != (id)[NSNull null] && [sunday length] > 0)
            {
                NSArray *arrSunday = [sunday componentsSeparatedByString:@","];
                NSDictionary *dictSunday = @{@"Sunday":arrSunday};
                [arrSpecials addObject:dictSunday];
            }
            
            
            NSString *monday = [dict valueForKey:@"monday"];
            if(monday != (id)[NSNull null] && [monday length] > 0)
            {
                NSArray *arrMonday = [monday componentsSeparatedByString:@","];
                NSDictionary *dictMonday = @{@"Monday":arrMonday};
                [arrSpecials addObject:dictMonday];
            }
            
            NSString *tuesday = [dict valueForKey:@"tuesday"];
            if(tuesday != (id)[NSNull null] && [tuesday length] > 0)
            {
                NSArray *arrTuesday = [tuesday componentsSeparatedByString:@","];
                NSDictionary *dictTuesday = @{@"Tuesday":arrTuesday};
                [arrSpecials addObject:dictTuesday];
            }
            
            NSString *wednesday = [dict valueForKey:@"wednesday"];
            if(wednesday != (id)[NSNull null] && [wednesday length] > 0)
            {
                NSArray *arrWed = [wednesday componentsSeparatedByString:@","];
                NSDictionary *dictWed = @{@"Wednesday":arrWed};
                [arrSpecials addObject:dictWed];
            }
            
            NSString *thursday = [dict valueForKey:@"thursday"];
            if(thursday != (id)[NSNull null] && [thursday length] > 0)
            {
                NSArray *arrThursday = [thursday componentsSeparatedByString:@","];
                NSDictionary *dictThursday = @{@"Thursday":arrThursday};
                [arrSpecials addObject:dictThursday];
            }
            
            NSString *friday = [dict valueForKey:@"friday"];
            if(friday != (id)[NSNull null] && [friday length] > 0)
            {
                NSArray *arrFriday = [friday componentsSeparatedByString:@","];
                NSDictionary *dictFriday = @{@"Friday":arrFriday};
                [arrSpecials addObject:dictFriday];
            }
            
            NSString *saturday = [dict valueForKey:@"saturday"];
            if(saturday != (id)[NSNull null] && [saturday length] > 0)
            {
                NSArray *arrSaturday = [saturday componentsSeparatedByString:@","];
                NSDictionary *dictSaturday = @{@"Saturday":arrSaturday};
                [arrSpecials addObject:dictSaturday];
            }
            
            
            [modal setSpecialsArray:arrSpecials];
            
        }
        
        [modalArray addObject:modal];
        modal=nil;
    }
    
    
    return modalArray;
}


@end
