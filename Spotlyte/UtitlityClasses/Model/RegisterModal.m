//
//  RegisterModal.m
//  Spotlyte
//
//  Created by Admin on 28/03/16.
//  Copyright © 2016 Naveen. All rights reserved.
//

#import "RegisterModal.h"
#import "ServiceHelper.h"

@implementation RegisterModal
@synthesize emailID;
@synthesize password;
@synthesize deviceID;
@synthesize deviceType;
@synthesize deviceToken;
@synthesize firstName;
@synthesize lastName;
@synthesize address1;
@synthesize address2;
@synthesize city,CityorArea;
@synthesize state;
@synthesize country;
@synthesize zipCode;
@synthesize mobileNo;
@synthesize phoneNo;
@synthesize profileImage;
@synthesize message;
@synthesize result;
@synthesize userID;
@synthesize placeID;
@synthesize placeName;
@synthesize placeImage;
@synthesize placeType;
@synthesize title;
@synthesize subTitle;
@synthesize latitude;
@synthesize longitude;
@synthesize openFrom;
@synthesize openTo;
@synthesize descr;
@synthesize followerID;
@synthesize isCheckedFollower;
@synthesize countryID,stateID;
@synthesize socialName,socialID,gender;
@synthesize dateOfBirth;
@synthesize categoryName;
@synthesize CommentedUser_name,CommentedUser_Image;
+ (RegisterModal *)sharedUserProfile
{
    @synchronized([RegisterModal class])
    {
        static RegisterModal *_sharedUsers;
        if (!_sharedUsers)
        {
            _sharedUsers=[[RegisterModal alloc] init];
        }
        return _sharedUsers;
    }
    return nil;
}

-(void)encodeWithCoder:(NSCoder *)encoder
{
    //Encode the properties of the object
    [encoder encodeObject:self.userID forKey:@"userID"];
    [encoder encodeObject:self.userName forKey:@"userName"];
    [encoder encodeObject:self.emailID forKey:@"emailID"];
    [encoder encodeObject:self.profileImage forKey:@"profileImage"];
}

-(id)initWithCoder:(NSCoder *)decoder
{
    self = [super init];
    if ( self != nil )
    {
        //decode the properties
        self.userID = [decoder decodeObjectForKey:@"userID"];
        self.userName = [decoder decodeObjectForKey:@"userName"];
        self.emailID = [decoder decodeObjectForKey:@"emailID"];
        self.profileImage = [decoder decodeObjectForKey:@"profileImage"];
    }
    return self;
}


#pragma mark --- FaceBook Login

- (void)faceBookLoginToServer:(RegisterModal *)userdetails  withCompletion:(void(^)(id obj1))completionBlock
{
    
     NSDictionary *dict = @{@"user_name":userdetails.userName,@"social_type":userdetails.socialName,@"social_id":userdetails.socialID,@"device_id":userdetails.deviceID,@"device_token":userdetails.deviceToken,@"device_type":userdetails.deviceType,@"user_email":userdetails.emailID,@"user_gender":userdetails.gender,@"last_name":userdetails.lastName,@"first_name":userdetails.firstName};
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    [ServiceHelper sendRequestToServerWithParameters:@"addUserSocial.json" forhttpMethod:@"POST" withrequestBody:jsonString sync:YES completion:^(id obj) {
        if ([obj isKindOfClass:[NSDictionary class]])
        {
            
            RegisterModal *modal = [[RegisterModal alloc]init];
            [modal setResult:[obj valueForKey:@"result"]];
            [modal setMessage:[obj valueForKey:@"message"]];
            [modal setUserName:[obj valueForKey:@"user_name"]];
            [modal setFirstName:[obj valueForKey:@"first_name"]];
            [modal setLastName:[obj valueForKey:@"last_name"]];
            [modal setUserID:[obj valueForKey:@"user_id"]];
            [modal setEmailID:[obj valueForKey:@"email_id"]];
            [modal setProfileImage:[obj valueForKey:@"profile_image"]];
            [modal setLogin_status:[obj valueForKey:@"login_status"]];
            
            [Default setObject:[obj valueForKey:@"first_name"] forKey:@"first_name"];
            [Default setObject:[obj valueForKey:@"last_name"] forKey:@"last_name"];
            
            if (completionBlock) {
                completionBlock(modal);
                modal=nil;
            }
        }
        else
        {
            completionBlock(obj);
            obj=nil;
        }
    }];
}

#pragma  mark Register ISL

- (void)registerToServer:(RegisterModal *)userdetails  withCompletion:(void(^)(id obj1))completionBlock
{

    NSDictionary *dict = @{@"email_id":userdetails.emailID,@"password":userdetails.password,@"user_name":userdetails.userName,@"first_name":userdetails.firstName,@"last_name":userdetails.lastName,@"gender":userdetails.gender,@"birthday":userdetails.dateOfBirth,@"device_id":userdetails.deviceID,@"device_token":userdetails.deviceToken,@"device_type":userdetails.deviceType};
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    NSLog(@"dictSignup == %@",dict);
    
    [ServiceHelper sendRequestToServerWithParameters:@"addUser.json" forhttpMethod:@"POST" withrequestBody:jsonString sync:YES completion:^(id obj) {
        NSLog(@"ResultSignup == %@",obj);
        
        if ([obj isKindOfClass:[NSDictionary class]]) {
            RegisterModal *modal = [[RegisterModal alloc]init];
            [modal setResult:[obj valueForKey:@"result"]];
            [modal setMessage:[obj valueForKey:@"message"]];
            [modal setUserID:[obj valueForKey:@"user_id"]];
            if (completionBlock) {
                completionBlock(modal);
                modal=nil;
            }
        }
        else
        {
            completionBlock(obj);
            obj=nil;
        }
    }];
}

#pragma mark ForGot Password ISL

- (void)forgotPasswordToServer:(NSString *)emailStr  withCompletion:(void(^)(id obj1))completionBlock
{
    
    NSDictionary *dict = @{@"email_id":emailStr};
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    
    [ServiceHelper sendRequestToServerWithParameters:@"forgotPassword.json" forhttpMethod:@"POST" withrequestBody:jsonString sync:YES completion:^(id obj) {
        if ([obj isKindOfClass:[NSDictionary class]]) {
            RegisterModal *modal = [[RegisterModal alloc]init];
            [modal setResult:[obj valueForKey:@"result"]];
            [modal setMessage:[obj valueForKey:@"message"]];
            if (completionBlock) {
                completionBlock(modal);
                modal=nil;
            }
        }
        else
        {
            completionBlock(obj);
            obj=nil;
        }
    }];
}

#pragma mark Login ISL

- (void)loginToServer:(RegisterModal *)userdetails  withCompletion:(void(^)(id obj1))completionBlock
{

    NSLog(@"login credientials == %@, %@",userdetails.userName,userdetails.password);
    [ServiceHelper loginWithEmailId:userdetails.userName andPassword:userdetails.password andDeviceToken:userdetails.deviceToken andDeviceType:userdetails.deviceType callback:^(id obj,NSError* error, BOOL success) {
          NSLog(@"response == %@",obj);
        
        if ([obj isKindOfClass:[NSDictionary class]]) {
            RegisterModal *modal = [[RegisterModal alloc]init];
            [modal setResult:[obj valueForKey:@"result"]];
            [modal setMessage:[obj valueForKey:@"Message"]];
            [modal setUserName:[obj valueForKey:@"user_name"]];
            [modal setFirstName:[obj valueForKey:@"first_name"]];
            [modal setLastName:[obj valueForKey:@"last_name"]];
            [modal setUserID:[obj valueForKey:@"user_id"]];
            [modal setEmailID:[obj valueForKey:@"email_id"]];
            [modal setProfileImage:[obj valueForKey:@"profile_image"]];
            [modal setLogin_status:[obj valueForKey:@"login_status"]];
            
            [Default setObject:[obj valueForKey:@"first_name"] forKey:@"first_name"];
            [Default setObject:[obj valueForKey:@"last_name"] forKey:@"last_name"];
            if (completionBlock) {
                completionBlock(modal);
                modal=nil;
            }
        }
        else
        {
            completionBlock(obj);
            obj=nil;
        }
    }];
}

#pragma mark -- Edit Account ISL

- (void)editAccountToServer:(RegisterModal *)userdetails  withCompletion:(void(^)(id obj1))completionBlock
{
    
    NSDictionary *dict = @{@"user_id":userdetails.userID,@"first_name":userdetails.firstName,@"last_name":userdetails.lastName,@"user_name":userdetails.userName,@"address":userdetails.address2,@"city":userdetails.city,@"state":userdetails.state,@"country":@"230",@"zip":userdetails.zipCode,@"mobile":userdetails.mobileNo,@"profile_image":userdetails.profileImage,@"email":userdetails.emailID};
    
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    
    [ServiceHelper sendRequestToServerWithParameters:@"editUserProfile.json" forhttpMethod:@"POST" withrequestBody:jsonString sync:YES completion:^(id obj) {
        if ([obj isKindOfClass:[NSDictionary class]]) {
            
            RegisterModal *modal = [[RegisterModal alloc]init];
            [modal setResult:[obj valueForKey:@"result"]];
            [modal setMessage:[obj valueForKey:@"message"]];
            [modal setUserName:[obj valueForKey:@"user_name"]];
            [modal setUserID:[obj valueForKey:@"user_id"]];
            [modal setEmailID:[obj valueForKey:@"email_id"]];
            [modal setProfileImage:[obj valueForKey:@"profile_image"]];
            [Default setObject:[obj valueForKey:@"first_name"] forKey:@"first_name"];
            [Default setObject:[obj valueForKey:@"last_name"] forKey:@"last_name"];
            if (completionBlock) {
                completionBlock(modal);
                modal=nil;
            }
        }
        else
        {
            completionBlock(obj);
            obj=nil;
        }
    }];
}

#pragma mark -- View Account ISL

- (void)viewAccountToServer:(NSString *)user_IDArg  withCompletion:(void(^)(id obj1))completionBlock
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [defaults objectForKey:@"logindetails"];
    RegisterModal *registerModalResponeValue = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    NSString *user_IDRegister = registerModalResponeValue.userID;
    NSDictionary *dict = @{@"user_id":user_IDRegister,@"follower_id":user_IDArg};
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    [ServiceHelper sendRequestToServerWithParameters:@"getUserProfile.json" forhttpMethod:@"POST" withrequestBody:jsonString sync:YES completion:^(id obj) {
        NSLog(@"%@",obj);
        if ([obj isKindOfClass:[NSDictionary class]]) {
            RegisterModal *modal = [[RegisterModal alloc]init];
            [modal setResult:[obj valueForKey:@"result"]];
            [modal setMessage:[obj valueForKey:@"message"]];
            if([[obj valueForKey:@"result"] isEqualToString:@"success"])
            {
                modal = [self setProfileValue:obj];
            }
            if (completionBlock) {
                completionBlock(modal);
                modal=nil;
            }
        }
        else
        {
            completionBlock(obj);
            obj=nil;
        }
    }];
}

-(RegisterModal *)setProfileValue :(id)arg
{
    RegisterModal *modal = [[RegisterModal alloc]init];
    [modal setResult:[arg valueForKey:@"result"]];
    [modal setMessage:[arg valueForKey:@"message"]];
    NSDictionary *dict = [arg valueForKey:@"user_details"];
    if(dict != nil)
    {
        /*if (![[arg valueForKey:@"message"] isEqualToString:@"User Found"]) {
            [Default setObject:[dict valueForKey:@"first_name"] forKey:@"first_name"];
            [Default setObject:[dict valueForKey:@"last_name"] forKey:@"last_name"];
        }*/

        NSString *userIDStr = [dict valueForKey:@"user_id"];
        if(userIDStr != (id)[NSNull null])
            [modal setUserID:userID];
        else
            [modal setUserID:@""];
        
        NSString *firstNameStr = [dict valueForKey:@"first_name"];
        if(firstNameStr != (id)[NSNull null])
            [modal setFirstName:firstNameStr];
        else
            [modal setFirstName:@""];
        
        NSString *lastNameStr = [dict valueForKey:@"last_name"];
        if(lastNameStr != (id)[NSNull null])
            [modal setLastName:lastNameStr];
        else
            [modal setLastName:@""];
        
        
        NSString *user_nameStr = [dict valueForKey:@"user_name"];
        if(user_nameStr != (id)[NSNull null])
            [modal setUserName:user_nameStr];
        else
            [modal setUserName:@""];
        
        NSString *email_idStr = [dict valueForKey:@"email_id"];
        if(email_idStr != (id)[NSNull null])
            [modal setEmailID:email_idStr];
        else
            [modal setEmailID:@""];
        
        NSString *cityStr = [dict valueForKey:@"city"];
        if(cityStr != (id)[NSNull null])
            [modal setCity:cityStr];
        else
            [modal setCity:@""];
        
        NSString *address1Str = [dict valueForKey:@"address1"];
        if(address1Str != (id)[NSNull null])
            [modal setAddress1:address1Str];
        else
            [modal setAddress1:@""];
        
        NSString *address2Str = [dict valueForKey:@"address2"];
        if(address2Str != (id)[NSNull null])
            [modal setAddress2:address2Str];
        else
            [modal setAddress2:@""];
        
        NSString *zipStr = [dict valueForKey:@"zip"];
        if(zipStr != (id)[NSNull null])
            [modal setZipCode:zipStr];
        else
            [modal setZipCode:@""];
        
        NSString *mobileStr = [dict valueForKey:@"mobile"];
        if(mobileStr != (id)[NSNull null])
            [modal setMobileNo:mobileStr];
        else
            [modal setMobileNo:@""];
        
        NSString *stateStr = [dict valueForKey:@"state"];
        if(stateStr != (id)[NSNull null])
            [modal setState:stateStr];
        else
            [modal setState:@""];
        
        NSString *state_id = [dict valueForKey:@"state_id"];
        if(state_id != (id)[NSNull null])
            [modal setStateID:state_id];
        else
            [modal setStateID:@""];
        
        NSString *countryStr = [dict valueForKey:@"country"];
        if(countryStr != (id)[NSNull null])
            [modal setCountry:countryStr];
        else
            [modal setCountry:@""];
        
        NSString *phone_numberStr = [dict valueForKey:@"phone_number"];
        if(phone_numberStr != (id)[NSNull null])
            [modal setPhoneNo:phone_numberStr];
        else
            [modal setPhoneNo:@""];
        
        NSString *profile_imageStr = [dict valueForKey:@"profile_image"];
        if(profile_imageStr != (id)[NSNull null])
            [modal setProfileImage:profile_imageStr];
        else
            [modal setProfileImage:@""];
        
        NSString *total_followers = [dict valueForKey:@"total_followers"];
        if(total_followers != (id)[NSNull null])
            [modal setFollowerCount:total_followers];
        else
            [modal setFollowerCount:@""];
        
        NSString *total_followings = [dict valueForKey:@"total_followings"];
        if(total_followings != (id)[NSNull null])
            [modal setFollowingCount:total_followings];
        else
            [modal setFollowingCount:@""];
        
        NSString *PlaceImage = [dict valueForKey:@"place_image"];
        if(PlaceImage != (id)[NSNull null])
            [modal setPlaceImage:PlaceImage];
        else
            [modal setPlaceImage:@""];

        NSArray *sortedArray = nil;[NSMutableArray new];
        sortedArray = [self setUserProfileComments:dict];
        [modal setProfileCommentsArray:sortedArray];
        
        NSArray *arrSorted = nil;[NSMutableArray new];
        //arrSorted = [self setDashboardProfileComments:dict];
        arrSorted = [self DashboardProfileComments:dict];
        [modal setUsersCommentsArray:arrSorted];
        
        NSArray *sortedArraynew = nil;[NSMutableArray new];
        sortedArraynew = [self setUserPrefferedCategory:dict];
        [modal setPrefferedCategoryArray:sortedArraynew];
        
        BOOL is_FollowerBool = [[dict valueForKey:@"is_follower"]boolValue];
        [modal setIsCheckedFollower:is_FollowerBool];
    }
    return modal;
}

-(NSMutableArray *)setUserPrefferedCategory :(id)commentsObj
{
    NSMutableArray *commentsarray1 = [[NSMutableArray alloc]init];
    NSArray *userCommentsarray = [commentsObj valueForKey:@"selected_category"];
    RegisterModal *modal;
    for(NSDictionary *dict in userCommentsarray)
    {
        if(dict != nil)
        {
            modal = [[RegisterModal alloc]init];
            NSString *place_idStr = [dict valueForKey:@"category_name"];
            if(place_idStr != (id)[NSNull null])
                [modal setCategoryName:place_idStr];
            else
                [modal setCategoryName:@""];
        }
        [commentsarray1 addObject:modal];
        modal=nil;
    }
    return commentsarray1;
}

-(NSMutableArray *)DashboardProfileComments :(id)commentsObj
{
    NSMutableArray *commentsarray1 = [[NSMutableArray alloc]init];
    NSArray *userCommentsarray = [commentsObj valueForKey:@"user_comments"];
    RegisterModal *modal;
    for(NSDictionary *dict in userCommentsarray)
    {
        if(dict != nil)
        {
            modal = [[RegisterModal alloc]init];
            NSString *place_idStr = [dict valueForKey:@"place_id"];
            if(place_idStr != (id)[NSNull null])
                [modal setPlaceID:place_idStr];
            else
                [modal setPlaceID:@""];
            
            
            NSString *place_name = [dict valueForKey:@"place_name"];
            if(place_name != (id)[NSNull null])
                [modal setPlaceName:place_name];
            else
                [modal setPlaceName:@""];
            NSString *place_image = [dict valueForKey:@"place_image"];
            if(place_name != (id)[NSNull null])
                [modal setPlaceImage:place_image];
            else
                [modal setPlaceName:@""];
            
            
            
            NSString *review_idStr = [dict valueForKey:@"review_id"];
            if(review_idStr != (id)[NSNull null])
                [modal setReviewID:review_idStr];
            else
                [modal setReviewID:@""];
            
            NSString *commentsStr = [dict valueForKey:@"comments"];
            if(commentsStr != (id)[NSNull null])
                [modal setComments:commentsStr];
            else
                [modal setComments:@""];
            
            
            NSString *comments_imageStr = [dict valueForKey:@"comments_image"];
            if(comments_imageStr != (id)[NSNull null])
                [modal setCommentsImage:comments_imageStr];
            else
                [modal setCommentsImage:@""];
            
            NSString *Ratings = [dict valueForKey:@"ratings"];
            if (Ratings != (id)[NSNull null]) {
                [modal setRating:Ratings];
            }
            else
                [modal setRating:@""];
            
            NSString *is_emojiStr = [dict valueForKey:@"is_emoji"];
            if(is_emojiStr != (id)[NSNull null])
                [modal setIs_Emoji:is_emojiStr];
            else
                [modal setIs_Emoji:@""];
            
            NSString *created_dateStr = [dict valueForKey:@"created_date"];
            if(created_dateStr != (id)[NSNull null])
                [modal setCreatedOn:created_dateStr];
            else
                [modal setCreatedOn:@""];
            
            NSString *firstNameStr = [dict valueForKey:@"first_name"];
            if(firstNameStr != (id)[NSNull null])
                [modal setFirstName:firstNameStr];
            else
                [modal setFirstName:@""];
            
        }
        [commentsarray1 addObject:modal];
        modal=nil;
        
    }
    
    return commentsarray1;
}
-(NSMutableArray *)setUserProfileComments :(id)commentsObj
{
    NSMutableArray *commentsarray1 = [[NSMutableArray alloc]init];
    NSArray *userCommentsarray = [commentsObj valueForKey:@"following_comments"];
    RegisterModal *modal;
    if (userCommentsarray.count>0)
    {
        for(NSDictionary *dict in userCommentsarray)
        {
            if(dict != nil)
            {
                modal = [[RegisterModal alloc]init];
                NSString *place_idStr = [dict valueForKey:@"place_id"];
                if(place_idStr != (id)[NSNull null])
                    [modal setPlaceID:place_idStr];
                else
                    [modal setPlaceID:@""];
                
                
                NSString *place_name = [dict valueForKey:@"place_name"];
                if(place_name != (id)[NSNull null])
                    [modal setPlaceName:place_name];
                else
                    [modal setPlaceName:@""];
                NSString *place_image = [dict valueForKey:@"place_image"];
                if(place_name != (id)[NSNull null])
                    [modal setPlaceImage:place_image];
                else
                    [modal setPlaceName:@""];
                
                
                NSString *review_idStr = [dict valueForKey:@"review_id"];
                if(review_idStr != (id)[NSNull null])
                    [modal setReviewID:review_idStr];
                else
                    [modal setReviewID:@""];
                
                NSString *commentsStr = [dict valueForKey:@"comments"];
                if(commentsStr != (id)[NSNull null])
                    [modal setComments:commentsStr];
                else
                    [modal setComments:@""];
                
                
                NSString *comments_imageStr = [dict valueForKey:@"comments_image"];
                if(comments_imageStr != (id)[NSNull null])
                    [modal setCommentsImage:comments_imageStr];
                else
                    [modal setCommentsImage:@""];
                
                
                NSString *CommentedUsername = [dict valueForKey:@"user_name"];
                if(CommentedUsername != (id)[NSNull null])
                    [modal setCommentedUser_name:CommentedUsername];
                else
                    [modal setCommentedUser_name:@""];

                NSString *CommentedUserimage = [dict valueForKey:@"user_image"];
                if(CommentedUserimage != (id)[NSNull null])
                    [modal setCommentedUser_Image:CommentedUserimage];
                else
                    [modal setCommentedUser_Image:@""];

                
                NSString *Ratings = [dict valueForKey:@"ratings"];
                if (Ratings != (id)[NSNull null]) {
                    [modal setRating:Ratings];
                }
                else
                    [modal setRating:@""];
                
                NSString *is_emojiStr = [dict valueForKey:@"is_emoji"];
                if(is_emojiStr != (id)[NSNull null])
                    [modal setIs_Emoji:is_emojiStr];
                else
                    [modal setIs_Emoji:@""];
                
                NSString *created_dateStr = [dict valueForKey:@"created_date"];
                if(created_dateStr != (id)[NSNull null])
                    [modal setCreatedOn:created_dateStr];
                else
                    [modal setCreatedOn:@""];
                
                NSString *firstNameStr = [dict valueForKey:@"first_name"];
                if(firstNameStr != (id)[NSNull null])
                    [modal setFirstName:firstNameStr];
                else
                    [modal setFirstName:@""];
            }
            [commentsarray1 addObject:modal];
            modal=nil;
        }
    }
    return commentsarray1;
}

#pragma mark -- Add Follower Service Helper

- (void)addFollowerToServer:(RegisterModal *)userdetails  withCompletion:(void(^)(id obj1))completionBlock
{
    NSDictionary *dict = @{@"user_id":userdetails.userID,@"follower_id":userdetails.followerID};
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    [ServiceHelper sendRequestToServerWithParameters:@"AddFollowers.json" forhttpMethod:@"POST" withrequestBody:jsonString sync:YES completion:^(id obj) {
        if ([obj isKindOfClass:[NSDictionary class]]) {
            NSDictionary *dict = [obj valueForKey:@"data"];
            RegisterModal *modal = [[RegisterModal alloc]init];
            [modal setResult:[dict valueForKey:@"result"]];
            [modal setMessage:[dict valueForKey:@"message"]];
            if(modal.result == nil || modal.result.length == 0)
            {
                [modal setResult:[obj valueForKey:@"result"]];
                [modal setMessage:[obj valueForKey:@"message"]];
            }
            if (completionBlock) {
                completionBlock(modal);
                modal=nil;
            }
        }
        else
        {
            completionBlock(obj);
        }
    }];
}


#pragma mark -- Add Follower Service Helper


- (void)addUnFollowerToServer:(RegisterModal *)userdetails  withCompletion:(void(^)(id obj1))completionBlock
{
    
    NSDictionary *dict = @{@"user_id":userdetails.userID,@"follower_id":userdetails.followerID};
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    [ServiceHelper sendRequestToServerWithParameters:@"Unfollows.json" forhttpMethod:@"POST" withrequestBody:jsonString sync:YES completion:^(id obj) {
        if ([obj isKindOfClass:[NSDictionary class]]) {
            NSDictionary *dict = [obj valueForKey:@"data"];
            RegisterModal *modal = [[RegisterModal alloc]init];
            [modal setResult:[dict valueForKey:@"result"]];
            [modal setMessage:[dict valueForKey:@"message"]];
            if(modal.result == nil || modal.result.length == 0)
            {
                [modal setResult:[obj valueForKey:@"result"]];
                [modal setMessage:[obj valueForKey:@"message"]];
            }
            if (completionBlock) {
                completionBlock(modal);
                
                modal=nil;
            }
        }
        else
        {
            completionBlock(obj);
            obj=nil;
        }
    }];
}


#pragma mark -- List All Country ISL

- (void)listallCountryServer:(RegisterModal *)userdetails  withCompletion:(void(^)(id obj1))completionBlock
{
    [ServiceHelper sendRequestToServerWithParameters:@"stateLists.json" forhttpMethod:@"GET" withrequestBody:@"" sync:YES completion:^(id obj) {
        NSMutableArray *arrayValue;
        if ([obj isKindOfClass:[NSDictionary class]])
        {
            NSArray *responseArray = [obj valueForKey:@"data"];
            
            RegisterModal *modal;
            if(responseArray.count>0)
            {
                arrayValue = [[NSMutableArray alloc]init];
                for(NSDictionary *dict in responseArray)
                {
                    modal = [[RegisterModal alloc]init];
                    NSString *country_id = [dict valueForKey:@"state_id"];
                    if(country_id != (id)[NSNull null])
                        [modal setStateID:country_id];
                    else
                        [modal setStateID:@""];
                    
                    NSString *country_name = [dict valueForKey:@"state_name"];
                    if(country_name != (id)[NSNull null])
                        [modal setState:country_name];
                    else
                        [modal setState:@""];
                    [arrayValue addObject:modal];
                     modal=nil;
                }
            }
            
            
            if (completionBlock) {
                completionBlock(arrayValue);
            }
            
            arrayValue=nil;
        }
        else
        {
            completionBlock(obj);
            obj=nil;
        }
    }];
}

#pragma mark -- List All City ISL

- (void)listallCityServer:(RegisterModal *)userdetails  withCompletion:(void(^)(id obj1))completionBlock
{
   
    [ServiceHelper customGetRequest:@"cityLists" callback:^(id obj,NSError *error, BOOL success){
         if (obj!=nil) {
             NSMutableArray *arrayValue;
             if ([obj isKindOfClass:[NSDictionary class]])
             {
                 NSArray *responseArray = [obj valueForKey:@"data"];
                 
                 RegisterModal *modal;
                 if(responseArray.count>0)
                 {
                     arrayValue = [[NSMutableArray alloc]init];
                     for(NSDictionary *dict in responseArray)
                     {
                         modal = [[RegisterModal alloc]init];
                         NSString *country_id = [dict valueForKey:@"state_id"];
                         if(country_id != (id)[NSNull null])
                             [modal setStateID:country_id];
                         else
                             [modal setStateID:@""];
                         
                         NSString *country_name = [dict valueForKey:@"state_name"];
                         if(country_name != (id)[NSNull null])
                             [modal setState:country_name];
                         else
                             [modal setState:@""];
                         
                         NSString *cityIDStr = [dict valueForKey:@"city_id"];
                         cityIDStr = (cityIDStr != (id)[NSNull null]) ? cityIDStr : @"";
                         [modal setCityID:cityIDStr];
                         
                         NSString *status = [dict valueForKey:@"status"];
                         status = (status != (id)[NSNull null]) ? status : @"";
                         [modal setStatusCity:status];
                          [modal setCityorArea:status];
                         
                         if([status isEqualToString:@"City"])
                         {
                             NSString *city_name = [NSString stringWithFormat:@"%@, IL",[dict valueForKey:@"city_name"]];
                             city_name = (city_name != (id)[NSNull null]) ? city_name : @"";
                             [modal setPlaceType:city_name];
                         }
                         else
                         {
                             NSString *city_name = [NSString stringWithFormat:@"%@, Chicago",[dict valueForKey:@"city_name"]];
                             city_name = (city_name != (id)[NSNull null]) ? city_name : @"";
                             [modal setPlaceType:city_name];
                         }
                         
                         
                         
                         NSString *city_nameStr = [dict valueForKey:@"city_name"];
                         city_nameStr = (city_nameStr != (id)[NSNull null]) ? city_nameStr : @"";
                         [modal setCity:city_nameStr];
                         
                         NSString *address=[NSString stringWithFormat:@"%@",[dict valueForKey:@"state_name"]];
                            [modal setAddress1:address];
                         
                         [arrayValue addObject:modal];
                         
                         modal=nil;
                     }
                 }
              }
             
             if (completionBlock)
                 completionBlock(arrayValue);
             
             arrayValue=nil;
         }
         
        else
        {
            completionBlock(obj);
            obj=nil;
        }
    }];
}


#pragma mark --- List all Category

- (void)listallCategoryServer:(RegisterModal *)userdetails  withCompletion:(void(^)(id obj1))completionBlock
{
    [ServiceHelper customGetRequest:@"RetrieveAllPlaces" callback:^(id obj,NSError *error, BOOL success){
     if (obj!=nil) {
        NSMutableArray *arrayValue;
        if ([obj isKindOfClass:[NSDictionary class]])
        {
            NSArray *responseArray = [obj valueForKey:@"data"];
            
            RegisterModal *modal;
            if(responseArray.count>0)
            {
                arrayValue = [[NSMutableArray alloc]init];
                for(NSDictionary *dict in responseArray)
                {
                    modal = [[RegisterModal alloc]init];
                    NSString *place_id = [dict valueForKey:@"place_id"];
                    if(place_id != (id)[NSNull null])
                        [modal setPlaceID:place_id];
                    else
                        [modal setPlaceID:@""];
                    
                    NSString *place_name = [dict valueForKey:@"place_name"];
                    if(place_name != (id)[NSNull null])
                        [modal setPlaceType:place_name];
                    else
                        [modal setPlaceType:@""];
                    
                    NSString *citystr= [dict valueForKey:@"city"];
                    if(citystr != (id)[NSNull null])
                        [modal setCity:citystr];
                    else
                        [modal setCity:@""];
                    
                    NSString *placeAddress= [dict valueForKey:@"address"];
                    if(placeAddress != (id)[NSNull null])
                        [modal setAddress1:placeAddress];
                    else
                        [modal setAddress1:@""];
                    
                    NSString *Address2= [dict valueForKey:@"city"];
                    if(Address2 != (id)[NSNull null])
                        [modal setAddress2:[NSString stringWithFormat:@"%@,IL",address2]];
                    else
                        [modal setAddress2:@""];

                    
                    [modal setCityorArea:@"Place"];
                    
                    
                    [arrayValue addObject:modal];
                    modal=nil;
                }
            }
            
            
            if (completionBlock)
                completionBlock(arrayValue);
            
            arrayValue=nil;
        }
        else
        {
            completionBlock(obj);
            obj=nil;
        }
      }
    }];
}


#pragma mark -- List All Folowers and Followings ISL

- (void)listallFollowersAndFollowingsServer:(NSString *)userIDStr  withCompletion:(void(^)(id obj1))completionBlock
{
    NSDictionary *dict = @{@"user_id":userIDStr};
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
     
    [ServiceHelper sendRequestToServerWithParameters:@"userfollow.json" forhttpMethod:@"POST" withrequestBody:jsonString sync:YES completion:^(id obj) {
        if ([obj isKindOfClass:[NSDictionary class]])
        {
            NSDictionary *mainDict  = [obj valueForKey:@"user_details"];
            NSArray *responseFollowersArray = [mainDict valueForKey:@"followers"];
            
            RegisterModal *modal = [[RegisterModal alloc]init];
            if(responseFollowersArray.count>0)
            {
                [modal setFollowersArray:[self setFolowersAndFollowingsValue:responseFollowersArray]];
            }
            
            NSArray *responseFolowingsArray = [mainDict valueForKey:@"followings"];
            if(responseFolowingsArray.count>0)
            {
                [modal setFollowingsArray:[self setFolowersAndFollowingsValue:responseFolowingsArray]];
            }
            
            if (completionBlock) {
                completionBlock(modal);
                modal=nil;
            }
        }
        else
        {
            completionBlock(obj);
            obj=nil;
        }
    }];
}

-(NSMutableArray *)setFolowersAndFollowingsValue :(NSArray *)dictArray
{
    RegisterModal *modal;
    NSMutableArray *modalArray = [[NSMutableArray alloc]init];
    
    for(NSDictionary *dict in dictArray)
    {
        modal = [[RegisterModal alloc]init];
        if(dict != nil)
        {
            NSString *user_id = [dict valueForKey:@"user_id"];
            if(user_id != (id)[NSNull null])
                [modal setFriendsUserId:user_id];
            else
                [modal setFriendsUserId:@""];
            
            NSString *first_name = [dict valueForKey:@"first_name"];
            if(first_name != (id)[NSNull null])
                [modal setFirstName:first_name];
            else
                [modal setFirstName:@""];
            
            NSString *last_name = [dict valueForKey:@"last_name"];
            if(last_name != (id)[NSNull null])
                [modal setLastName:last_name];
            else
                [modal setLastName:@""];
            
            
            NSString *user_name = [dict valueForKey:@"user_name"];
            if(user_name != (id)[NSNull null])
                [modal setUserName:user_name];
            else
                [modal setUserName:@""];
            
            NSString *genderStr = [dict valueForKey:@"gender"];
            if(genderStr != (id)[NSNull null])
                [modal setGender:genderStr];
            else
                [modal setGender:@""];
            
            NSString *email_id = [dict valueForKey:@"email_id"];
            if(email_id != (id)[NSNull null])
                [modal setEmailID:email_id];
            else
                [modal setEmailID:@""];
            
            NSString *cityStr = [dict valueForKey:@"city"];
            if(cityStr != (id)[NSNull null])
                [modal setCity:cityStr];
            else
                [modal setCity:@""];
            
            NSString *profile_image = [dict valueForKey:@"profile_image"];
            if(profile_image != (id)[NSNull null])
                [modal setProfileImage:profile_image];
            else
                [modal setProfileImage:@""];
            
            BOOL is_FollowerBool = [[dict valueForKey:@"is_follower"]boolValue];
            [modal setIsCheckedFollower:is_FollowerBool];
        }
        [modalArray addObject:modal];
        
        modal=nil;
    }
    
    
    return modalArray;
}

- (void)getDashboardCount:(NSString *)userIDStr  withCompletion:(void(^)(id obj1))completionBlock
{
    NSDictionary *dict = @{@"user_id":userIDStr};
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    [ServiceHelper sendRequestToServerWithParameters:@"countUserView.json" forhttpMethod:@"POST" withrequestBody:jsonString sync:YES completion:^(id obj) {
        if ([obj isKindOfClass:[NSDictionary class]])
        {
            RegisterModal *modal = [[RegisterModal alloc]init];
            [modal setCheckin_count:[obj valueForKey:@"checkin_count"]];
            [modal setComment_count:[obj valueForKey:@"comment_count"]];
            [modal setDislike_count:[obj valueForKey:@"dislike_count"]];
            [modal setFavorites_count:[obj valueForKey:@"favorites_count"]];
            [modal setSpecial_count:[obj valueForKey:@"special_count"]];
            [modal setLike_count:[obj valueForKey:@"like_count"]];
            [modal setPhoto_count:[obj valueForKey:@"photo_count"]];
            [modal setRating:[obj valueForKey:@"rating"]];
            
            if (completionBlock) {
                completionBlock(modal);
                
                modal=nil;
            }
        }
        else
        {
            completionBlock(obj);
            obj=nil;
        }
    }];
}




@end
