//
//  TotalHistoryModal.h
//  Spotlyte
//
//  Created by CIPL227-MOBILITY on 20/04/16.
//  Copyright © 2016 Naveen. All rights reserved.
//

#import <Foundation/Foundation.h>
#define HistoryMacro [TotalHistoryModal sharedUserProfile]

@interface TotalHistoryModal : NSObject

@property (nonatomic, retain) NSString *resturantName;
@property (nonatomic, retain) UIImage *resturantImage;
@property (nonatomic, retain) NSString *resturantReviewRating;
@property (nonatomic, retain) NSString *resturantReviewDescription;
@property (nonatomic, retain) NSString *recentActivityType;
@property (nonatomic, retain) NSString *recentActivityTime;
@property (nonatomic, retain) NSString *userID_History;
@property (nonatomic, retain) NSString *resturantImageStr;
@property (nonatomic, retain) NSString *postComment;
@property (nonatomic, retain) NSString *condition;

@property (nonatomic, retain) NSString *message;
@property (nonatomic, retain) NSString *result;

@property (nonatomic, retain) NSString *reason;
@property (nonatomic, retain) NSString *restDescription;

@property (nonatomic, retain) NSMutableArray *arrayPositive;
@property (nonatomic, retain) NSMutableArray *arrayNegative;


- (void)totalHistoryValuesToServer:(TotalHistoryModal *)userdetails  withCompletion:(void(^)(id obj1))completionBlock;
- (void)recentActivityToServer:(TotalHistoryModal *)userdetails  withCompletion:(void(^)(id obj1))completionBlock;
- (void)reportToServer:(TotalHistoryModal *)userdetails  withCompletion:(void(^)(id obj1))completionBlock;
+ (TotalHistoryModal *)sharedUserProfile;
- (void)positiveNegativeToServer:(TotalHistoryModal *)userdetails  withCompletion:(void(^)(id obj1))completionBlock;

@end
