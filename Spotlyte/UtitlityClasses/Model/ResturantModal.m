//
//  ResturantModal.m
//  Spotlyte
//
//  Created by Admin on 24/03/16.
//  Copyright © 2016 Naveen. All rights reserved.
//

#import "ResturantModal.h"

@implementation ResturantModal
@synthesize CityOrArea;
@synthesize resturantAddress;
@synthesize resturantCity;
@synthesize resturantCountry;
@synthesize resturantDescription;
@synthesize resturantEMail,strWebsite;
@synthesize resturantHours;
@synthesize resturantImage;
@synthesize resturantName;
@synthesize resturantPhoneNo;
@synthesize resturantPostalCode;
@synthesize resturantState;
@synthesize resturantLatitude;
@synthesize resturantLongtitude;
@synthesize resturantRadius;
@synthesize resturantAddress2;
@synthesize ratingType;
@synthesize placeID;
@synthesize placeImage;
@synthesize PlaceName;
@synthesize DetailViewPlaceImage;
@synthesize placeType;
@synthesize title;
@synthesize subTitle;
@synthesize openFrom;
@synthesize openTo;
@synthesize resturantmobileNo;
@synthesize result;
@synthesize message;
@synthesize userID;
@synthesize likeORDislike;
@synthesize likecount;
@synthesize dislikeCount;
@synthesize isCheckedFavourite;
@synthesize isCheckedFavSpecial;
@synthesize placeRatings;
@synthesize isHighlights;
@synthesize userRatings,UserRatingTime;
@synthesize reviewID;
@synthesize comments;
@synthesize commentsImage;
@synthesize is_Emoji,price,rating,distance;
@synthesize createdOn,systemMessage,latestEmonji,comment_for_checkin;
@synthesize firstName,lastName,userName,profileImage,resturantImage1;
@synthesize cmdNumber;
@synthesize dateTime;
@synthesize specials_title;
@synthesize Tabs;
@synthesize specials_weekDay;
@synthesize ratingValueINNumber;
@synthesize arrayHotelFavourites,arrayPlaceFavourites;
@synthesize arrayBars,arrayOthers,arrayResturants;
@synthesize isCheckedCheckIn,checkInStr;
@synthesize emonjiID,emonjiTime,totalReview;
@synthesize emonjiID2,emonjiID3,emonjiID4,arraySpecials;
@synthesize isCommentAdded,isRated;
@synthesize userGivenRating;
+ (ResturantModal *)sharedUserProfile
{
    @synchronized([ResturantModal class])
    {
        static ResturantModal *_sharedUsers;
        if (!_sharedUsers)
        {
            _sharedUsers=[[ResturantModal alloc] init];
        }
        return _sharedUsers;
    }
    return nil;
}

#pragma mark -- List All Values In Advance Filter ISL

- (void)listAllAdvancefilterData:(ResturantModal *)userdetails  withCompletion:(void(^)(id obj1))completionBlock
{
    NSString* latitude=[NSString stringWithFormat:@"%f",APP_DELEGATE.userCurrentLocation.coordinate.latitude];
    NSString* longitude=[NSString stringWithFormat:@"%f",APP_DELEGATE.userCurrentLocation.coordinate.longitude];
    
    //NSDictionary *dict = @{@"latitude":userdetails.resturantLatitude,@"longitude":userdetails.resturantLongtitude,@"user_id":userdetails.userID,@"distance":userdetails.mile,@"happy_hours_time":userdetails.time,@"happy_hours_cost":userdetails.happyHours,@"searchby":userdetails.systemMessage,@"city":userdetails.resturantCity,@"happy_hours_day":userdetails.Day_name,@"current_latitude":latitude,@"current_longitude":longitude};
    if (userdetails.CityOrArea == nil)
    {
        userdetails.CityOrArea = @"";
    }
    
    //NSDictionary *dict = @{@"latitude":userdetails.resturantLatitude,@"longitude":userdetails.resturantLongtitude,@"user_id":userdetails.userID,@"happy_hour":@"",@"daily_special":@"",@"txtsearch":@"",@"radius":userdetails.resturantRadius,@"current_latitude":latitude,@"current_longitude":longitude,@"topSpot":userdetails.top_spot_id,@"item_term":@"City",@"tab_name":@"firsttab",@"count_check":@"false",@"promotion_place_id":@"",@"location":@"",@"offset":@"1",@"price":@"",@"timepicker_range":@"",@"timerange":userdetails.time,@"date":@""};
    NSLog(@"%@",userdetails.systemMessage);
    NSString *itemTerm = [[NSString alloc]init];
    NSString *tab = [[NSString alloc]init];
    NSLog(@"%@",userdetails.mile);
    NSLog(@"%@",userdetails.resturantCity);
    NSLog(@"%@",userdetails.price);
    NSLog(@"%@",userdetails.time);
    NSLog(@"%@",userdetails.Day_name);
    NSArray *arrDate;
    if (![userdetails.Day_name isEqualToString:@""]) {
       arrDate = [[NSArray alloc]initWithObjects:userdetails.Day_name, nil];
    }
    else{
        arrDate = [[NSArray alloc]init];
    }
    NSString *strSystemMsg;
    if ([userdetails.systemMessage isEqualToString:@""])
    {
        NSString *str = userdetails.resturantCity;
        strSystemMsg=[str stringByReplacingOccurrencesOfString:@"'s" withString: @"%20"];
        itemTerm = userdetails.CityOrArea;
        tab = @"firsttab";
    }
    else{
        strSystemMsg = userdetails.systemMessage;
        NSString *str = strSystemMsg;
        NSData *data = [str dataUsingEncoding:NSUTF8StringEncoding];
        
        NSLog(@"%@",data);
        NSLog(@"%@",strSystemMsg);
        itemTerm = @"";
        tab = @"seconstab";
    }
    NSDictionary *dict = @{@"latitude":userdetails.resturantLatitude,@"longitude":userdetails.resturantLongtitude,@"user_id":userdetails.userID,@"happy_hour":@"",@"daily_special":@"",@"txtsearch":strSystemMsg,@"radius":userdetails.mile,@"current_latitude":latitude,@"current_longitude":longitude,@"topSpot":@"",@"item_term":itemTerm,@"tab_name":tab,@"count_check":@"0",@"promotion_place_id":@"",@"location":@"",@"offset":userdetails.strOffset,@"price":userdetails.happyHours,@"timepicker_range":@"",@"timerange":userdetails.time,@"date":arrDate};

    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
//AdvancedSearch.json
    [ServiceHelper sendRequestToServerWithParameters:@"AdvancedSearchNew.json" forhttpMethod:@"POST" withrequestBody:jsonString sync:YES completion:^(id obj) {
        if ([obj isKindOfClass:[NSDictionary class]]) {
            ResturantModal *modal = [[ResturantModal alloc]init];
            NSMutableArray *objArray = [[NSMutableArray alloc]init];
            [modal setResult:[obj valueForKey:@"result"]];
            [modal setMessage:[obj valueForKey:@"message"]];
            if([[obj valueForKey:@"result"] isEqualToString:@"success"])
            {
                [objArray addObject:[obj valueForKey:@"found_records"]];
                [objArray addObject:[self setListAllValue:obj check:@"place" ]];

            }
            if (completionBlock)
            {
                completionBlock(objArray);
            }
        }
        else
        {
            completionBlock(obj);
        }
    }];
}

#pragma mark -- List All Values In Map Page ISL

- (void)listAllSearchData:(ResturantModal *)userdetails  withCompletion:(void(^)(id obj1))completionBlock
{
    
    NSDictionary *dict = @{@"searchby":userdetails.resturantName,@"user_id":userdetails.userID};
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    
    [ServiceHelper sendRequestToServerWithParameters:@"RetrieveAllLocationsBySearch.json" forhttpMethod:@"POST" withrequestBody:jsonString sync:YES completion:^(id obj) {
        if ([obj isKindOfClass:[NSDictionary class]]) {
            
            ResturantModal *modal = [[ResturantModal alloc]init];
            NSMutableArray *objArray = [[NSMutableArray alloc]init];
            [modal setResult:[obj valueForKey:@"result"]];
            [modal setMessage:[obj valueForKey:@"message"]];
            if([[obj valueForKey:@"result"] isEqualToString:@"success"])
            {
                objArray = [self setListAllValue:obj check:@"place" ];
            }
            if (completionBlock) {
                completionBlock(objArray);
            }
        }
        else
        {
            completionBlock(obj);
        }
    }];
}


#pragma mark -- List All Values In Map Page ISL

- (void)listAllMapValuesToServer:(ResturantModal *)userdetails  withCompletion:(void(^)(id obj1))completionBlock
{
    NSString* latitude=[NSString stringWithFormat:@"%f",APP_DELEGATE.userCurrentLocation.coordinate.latitude];
    NSString* longitude=[NSString stringWithFormat:@"%f",APP_DELEGATE.userCurrentLocation.coordinate.longitude];
    if (userdetails.CityOrArea == nil)
    {
        userdetails.CityOrArea = @"";
    }
    if ((userdetails.Tabs == nil) || ([userdetails.Tabs isEqualToString:@""]))
    {
        userdetails.Tabs = @"firsttab";
    }
    NSDictionary *dict;
    if (userdetails.resturantRadius!=nil){
        dict = @{@"latitude":userdetails.resturantLatitude,@"longitude":userdetails.resturantLongtitude,@"user_id":userdetails.userID,@"happy_hour":userdetails.happHoursDay.lowercaseString,@"daily_special":userdetails.dailySpecialsDay.lowercaseString,@"txtsearch":userdetails.systemMessage,@"radius":userdetails.resturantRadius,@"current_latitude":latitude,@"current_longitude":longitude,@"topSpot":userdetails.top_spot_id,@"item_term":userdetails.CityOrArea,@"tab_name":userdetails.Tabs,@"count_check":@"0",@"promotion_place_id":@"",@"location":@"",@"offset":userdetails.strOffset,@"price":@"",@"timepicker_range":@"",@"timerange":@"",@"date":@"",@"bounds_sw_lat":userdetails.strSW_Lat,@"bounds_sw_lng":userdetails.strSW_Long,@"bounds_ne_lng":userdetails.strNE_Long,@"bounds_ne_lat":userdetails.strNE_Lat};
    }else
    {
        dict = @{@"latitude":userdetails.resturantLatitude,@"longitude":userdetails.resturantLongtitude,@"user_id":userdetails.userID,@"happy_hour":userdetails.happHoursDay.lowercaseString,@"daily_special":userdetails.dailySpecialsDay.lowercaseString,@"txtsearch":userdetails.systemMessage,@"current_latitude":latitude,@"current_longitude":longitude,@"topSpot":userdetails.top_spot_id,@"item_term":userdetails.CityOrArea,@"tab_name":userdetails.Tabs,@"count_check":@"0",@"promotion_place_id":@"",@"location":@"",@"offset":userdetails.strOffset,@"price":@"",@"timepicker_range":@"",@"timerange":@"",@"date":@"",@"bounds_sw_lat":userdetails.strSW_Lat,@"bounds_sw_lng":userdetails.strSW_Long,@"bounds_ne_lng":userdetails.strNE_Long,@"bounds_ne_lat":userdetails.strNE_Lat};
        //,@"top_spot_id":userdetails.top_spot_id
    }
    
//    if (userdetails.resturantRadius!=nil) {
//        dict = @{@"latitude":userdetails.resturantLatitude,@"longitude":userdetails.resturantLongtitude,@"user_id":userdetails.userID,@"is_livemap":userdetails.is_LiveMap,@"is_happy_hours":userdetails.is_HappyHours,@"is_daily_specials":userdetails.is_DailySpecials,@"happy_hours_day":userdetails.happHoursDay.lowercaseString,@"daily_specials_day":userdetails.dailySpecialsDay.lowercaseString,@"searchby":userdetails.systemMessage,@"distance":userdetails.resturantRadius,@"current_latitude":latitude,@"current_longitude":longitude,@"top_spot_id":userdetails.top_spot_id};
//        //,@"top_spot_id":userdetails.top_spot_id
//    }else
//    {
//        dict = @{@"latitude":userdetails.resturantLatitude,@"longitude":userdetails.resturantLongtitude,@"user_id":userdetails.userID,@"is_livemap":userdetails.is_LiveMap,@"is_happy_hours":userdetails.is_HappyHours,@"is_daily_specials":userdetails.is_DailySpecials,@"happy_hours_day":userdetails.happHoursDay.lowercaseString,@"daily_specials_day":userdetails.dailySpecialsDay.lowercaseString,@"searchby":userdetails.systemMessage,@"distance":@"0",@"current_latitude":latitude,@"current_longitude":longitude,@"top_spot_id":userdetails.top_spot_id};
//        //,@"top_spot_id":userdetails.top_spot_id
//    }
    NSLog(@"dict ================== %@",dict);
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
  //  AdvancedSearchNew.json
    [ServiceHelper sendRequestToServerWithParameters:@"AdvancedSearchNew.json" forhttpMethod:@"POST" withrequestBody:jsonString sync:YES completion:^(id obj) {
        
       // NSLog(@"result====================== %@",obj);
        
        if ([obj isKindOfClass:[NSDictionary class]])
        {
            ResturantModal *modal = [[ResturantModal alloc]init];
            NSMutableArray *objArray = [[NSMutableArray alloc]init];
            [modal setSpan:[obj valueForKeyPath:@"max_distance"]];
            [modal setResult:[obj valueForKey:@"result"]];
            [modal setMessage:[obj valueForKey:@"message"]];
            [modal setStrTotalRecordsFound:[obj valueForKey:@"found_records"]];
            
//            int specialCount = [[obj valueForKey:@"place_specials_count"] intValue]+[[obj valueForKey:@"happy_hour_count"] intValue];
//            [modal setPlace_specials_count:[NSString stringWithFormat:@"%d",specialCount]];
            
            
            if([[obj valueForKey:@"result"] isEqualToString:@"success"])
            {
                [objArray addObject:[obj valueForKeyPath:@"max_distance"]];
                [objArray addObject: [self setListAllValue:obj check:@"place" ]];
                [objArray addObject:[obj valueForKey:@"found_records"]];
                
            }
            if (completionBlock)
            {
                completionBlock(objArray);
                objArray=nil;
            }
        }
        else
        {
            completionBlock(obj);
            obj=nil;
        }
    }];
}

-(NSMutableArray *)setListAllValue :(id)arg check :(NSString *)value
{
    ResturantModal *modal;
    NSMutableArray *modalArray = [[NSMutableArray alloc]init];
    NSArray *dictArray;
    
    if([value isEqualToString:@"place"])
    {
        dictArray = [arg valueForKey:@"place_details"];
    }
    else if ([value isEqualToString:@"toprated"])
    {
        dictArray = arg;
    }
    else if([value isEqualToString:@"place_favorites"])
    {
        dictArray = [arg valueForKey:@"place_favorites"];
    }
    else if([value isEqualToString:@"data"])
    {
        if ([[arg valueForKey:@"data"] isKindOfClass:[NSArray class]]) {
            dictArray = [arg valueForKey:@"data"];
        }
    }
    else{
        dictArray = [arg valueForKey:@"promotion_favorites"];
    }
    
    for(NSDictionary *dict in dictArray)
    {
        modal = [[ResturantModal alloc]init];
        if(dict != nil)
        {
            NSString *place_idStr = removeNull([dict valueForKey:@"place_id"]);
            if(place_idStr != (id)[NSNull null])
                [modal setPlaceID:place_idStr];
            else
                [modal setPlaceID:@""];
            
            NSString *special_id = removeNull([dict valueForKey:@"sepcial_id"]);
            if(special_id != (id)[NSNull null])
                [modal setSpecials_ID:special_id];
            else
                [modal setSpecials_ID:@""];

            NSString *titleStr = removeNull([dict valueForKey:@"title"]);
            if(titleStr != (id)[NSNull null])
                [modal setResturantName:titleStr];
            else
                [modal setResturantName:@""];
            
            if ([modal.resturantName isEqualToString:@""]) {
                
                NSString *placename = removeNull([dict valueForKey:@"place_name"]);
                if(placename != (id)[NSNull null])
                    [modal setResturantName:placename];
                else
                    [modal setResturantName:@""];
            }
            
            NSString *date_Time = removeNull([dict valueForKey:@"created_date"]);
            if(date_Time != (id)[NSNull null])
                [modal setDateTime:date_Time];
            else
                [modal setDateTime:@""];
            
            

            NSString *min_distance =removeNull([dict valueForKey:@"max_miles"]) ;
            if(min_distance != (id)[NSNull null])
                [modal setMinDistance:min_distance];
            else
                [modal setMinDistance:@"0"];
            
            
            NSString *subtitleStr = removeNull([dict valueForKey:@"subtitle"]);
            if(subtitleStr != (id)[NSNull null])
                [modal setSubTitle:subtitleStr];
            else
                [modal setSubTitle:@""];
            
            NSString *descriptionStr = removeNull([dict valueForKey:@"description"]);
            if(descriptionStr != (id)[NSNull null])
                [modal setResturantDescription:descriptionStr];
            else
                [modal setResturantDescription:@""];
            
            NSString *place_typeStr = removeNull([dict valueForKey:@"place_type"]);
            if(place_typeStr != (id)[NSNull null])
                [modal setPlaceType:place_typeStr];
            else
                [modal setPlaceType:@""];
            
            NSString *placeName = removeNull([dict valueForKey:@"place_name"]);
            if(placeName != (id)[NSNull null])
                [modal setPlaceName:placeName];
            else
                [modal setPlaceName:@""];
            
            
            NSString *email_idStr = removeNull([dict valueForKey:@"happy_hour_link"]);
            if(email_idStr != (id)[NSNull null])
                [modal setResturantEMail:email_idStr];
            else
                [modal setResturantEMail:@""];
            
            NSString *Website = removeNull([dict valueForKey:@"website"]);
            if(email_idStr != (id)[NSNull null])
                [modal setStrWebsite:Website];
            else
                [modal setResturantEMail:@""];

            
            NSString *cityStr = removeNull([dict valueForKey:@"city"]);
            if(cityStr != (id)[NSNull null])
                [modal setResturantCity:cityStr];
            else
                [modal setResturantCity:@""];
            
            NSString *address1Str = removeNull([dict valueForKey:@"address"]);
            if(address1Str != (id)[NSNull null])
                [modal setResturantAddress:address1Str];
            else
                [modal setResturantAddress:@""];
            
            NSString *address2Str = removeNull([dict valueForKey:@"address2"]);
            if(address2Str != (id)[NSNull null])
                [modal setResturantAddress2:address2Str];
            else
                [modal setResturantAddress2:@""];
            
            NSString *zipStr = removeNull([dict valueForKey:@"zip"]);
            if(zipStr != (id)[NSNull null])
                [modal setResturantPostalCode:zipStr];
            else
                [modal setResturantPostalCode:@""];
            
            NSString *mobileStr = removeNull([dict valueForKey:@"mobile"]);
            if(mobileStr != (id)[NSNull null])
                [modal setResturantmobileNo:mobileStr];
            else
                [modal setResturantmobileNo:@""];
            
            NSString *stateStr = removeNull([dict valueForKey:@"state"]);
            if(stateStr != (id)[NSNull null])
                [modal setResturantState:stateStr];
            else
                [modal setResturantState:@""];
            
            NSString *countryStr = removeNull([dict valueForKey:@"country"]);
            if(countryStr != (id)[NSNull null])
                [modal setResturantCountry:countryStr];
            else
                [modal setResturantCountry:@""];
//            NSString *phone_numberStr = removeNull([dict valueForKey:@"phone_number"]);
//            if(phone_numberStr != (id)[NSNull null])
//                [modal setResturantPhoneNo:phone_numberStr];
//            else
//                [modal setResturantPhoneNo:@""];

            NSString *phone_numberStr = removeNull([dict valueForKey:@"phone"]);
            if(phone_numberStr != (id)[NSNull null])
                [modal setResturantPhoneNo:phone_numberStr];
            else
                [modal setResturantPhoneNo:@""];
            
            NSString *place_imageStr = removeNull([dict valueForKey:@"place_image"]);
            if(place_imageStr != (id)[NSNull null])
                [modal setPlaceImage:place_imageStr];
            else
                [modal setPlaceImage:@""];
            
            NSString *DetailPlace_imageStr = removeNull([dict valueForKey:@"place_imagebig"]);
            if(DetailPlace_imageStr != (id)[NSNull null])
                [modal setDetailViewPlaceImage:DetailPlace_imageStr];
            else
                [modal setDetailViewPlaceImage:@""];
            
            NSString *open_fromStr = removeNull([dict valueForKey:@"open_from"]);
            if(open_fromStr != (id)[NSNull null])
                [modal setOpenFrom:open_fromStr];
            else
                [modal setOpenFrom:@""];
            
            NSString *open_toStr = removeNull([dict valueForKey:@"open_to"]);
            if(open_toStr != (id)[NSNull null])
                [modal setOpenTo:open_toStr];
            else
                [modal setOpenTo:@""];
            
            NSString *longitudeStr = removeNull([dict valueForKey:@"lng"]);
            if(longitudeStr != (id)[NSNull null])
                [modal setResturantLongtitude:longitudeStr];
            else
                [modal setResturantLongtitude:@""];
            
            NSString *latitudeStr = removeNull([dict valueForKey:@"lat"]);
            if(latitudeStr != (id)[NSNull null])
                [modal setResturantLatitude:latitudeStr];
            else
                [modal setResturantLatitude:@""];
            
            NSString *is_likeStr = removeNull([dict valueForKey:@"is_like"]);
            if(is_likeStr != (id)[NSNull null])
                [modal setLikeORDislike:is_likeStr];
            else
                [modal setLikeORDislike:@""];
            
            NSString *like_countStr = removeNull([dict valueForKey:@"like_count"]);
            if(like_countStr != (id)[NSNull null])
                [modal setLikecount:like_countStr];
            else
                [modal setLikecount:@""];
            
            NSString *dislike_countStr = removeNull([dict valueForKey:@"dislike_count"]);
            if(dislike_countStr != (id)[NSNull null])
                [modal setDislikeCount:dislike_countStr];
            else
                [modal setDislikeCount:@""];
            
            BOOL is_favoriteBool = [[dict valueForKey:@"is_favorite"]boolValue];
            [modal setIsCheckedFavourite:is_favoriteBool];
            
            
            BOOL checkin = [[dict valueForKey:@"checkin"]boolValue];
            [modal setIsCheckedCheckIn:checkin];
            
            NSString *user_ratingsStr = removeNull([dict valueForKey:@"rating"]);
            if(user_ratingsStr != (id)[NSNull null])
                [modal setUserRatings:user_ratingsStr];
            else
                [modal setUserRatings:@""];
            
            NSString *place_ratingsStr = removeNull([dict valueForKey:@"place_ratings"]);
            if(place_ratingsStr != (id)[NSNull null])
                [modal setPlaceRatings:place_ratingsStr];
            else
                [modal setPlaceRatings:@""];
            
            NSString *is_Highlights = removeNull([dict valueForKey:@"is_highlight"]);
            if(is_Highlights != (id)[NSNull null])
                [modal setIsHighlights:is_Highlights];
            else
                [modal setIsHighlights:@""];
            
            NSString *emoji = removeNull([dict valueForKey:@"emoji1"]);
            if(emoji != (id)[NSNull null])
                [modal setEmonjiID:emoji];
            else
                [modal setEmonjiID:@""];
            
            NSString *emoji2 = removeNull([dict valueForKey:@"emoji2"]);
            if(emoji2 != (id)[NSNull null])
                [modal setEmonjiID2:emoji2];
            else
                [modal setEmonjiID2:@""];
            
            NSString *emoji3 = removeNull([dict valueForKey:@"emoji3"]);
            if(emoji3 != (id)[NSNull null])
                [modal setEmonjiID3:emoji3];
            else
                [modal setEmonjiID3:@""];
            
            NSString *emoji4 = removeNull([dict valueForKey:@"emoji4"]);
            if(emoji4 != (id)[NSNull null])
                [modal setEmonjiID4:emoji4];
            else
                [modal setEmonjiID4:@""];
            
            NSString *latestemonji = removeNull([dict valueForKey:@"latest_emoji"]);
            if(latestemonji != (id)[NSNull null])
                [modal setLatestEmonji:latestemonji];
            else
                [modal setLatestEmonji:@""];
            
            NSString *emoji_time = removeNull([dict valueForKey:@"emoji_time"]);
            if(emoji_time != (id)[NSNull null])
                [modal setEmonjiTime:emoji_time];
            else
                [modal setEmonjiTime:@""];
            
            NSString *total_reviews = removeNull([dict valueForKey:@"total_reviews"]);
            if(total_reviews != (id)[NSNull null])
                [modal setTotalReview:total_reviews];
            else
                [modal setTotalReview:@""];
            
            
            NSString *happyHours_Count = removeNull([dict valueForKey:@"happy_hour_count"]);
            if(happyHours_Count != (id)[NSNull null])
                [modal setHappy_hour_count:happyHours_Count];
            else
                [modal setHappy_hour_count:@"0"];
            
            
            NSString *placeSpecials_count = removeNull([dict valueForKey:@"place_specials_count"]);
            if(placeSpecials_count != (id)[NSNull null])
                [modal setPlace_specials_count:placeSpecials_count];
            else
                [modal setPlace_specials_count:@"0"];
            
            NSString *distanceStr = removeNull([dict valueForKey:@"distance"]);
            if(distanceStr != (id)[NSNull null])
                [modal setMile:distanceStr];
            else
                [modal setMile:@"0"];
            
            
            NSString *yelpStr = removeNull([dict valueForKey:@"yelp"]);
            if(yelpStr != (id)[NSNull null])
                [modal setYelp_url:yelpStr];
            else
                [modal setYelp_url:@""];
            
            NSString *bearStr = removeNull([dict valueForKey:@"beer_menus"]);
            if(bearStr != (id)[NSNull null])
                [modal setBeer_menus:bearStr];
            else
                [modal setBeer_menus:@""];
            
            NSString *openTable = removeNull([dict valueForKey:@"open_table"]);
            if(openTable != (id)[NSNull null])
                [modal setOpen_table:openTable];
            else
                [modal setOpen_table:@""];
            
            NSString *grubhub = [dict valueForKey:@"grubhub"];
            if(grubhub != (id)[NSNull null])
                [modal setGrub_hub:grubhub];
            else
                [modal setGrub_hub:@""];
            
            // logic for count display
         //   int specialCount = [[dict valueForKey:@"place_specials_count"] intValue]+[[dict valueForKey:@"happy_hour_count"] intValue];
           // [modal setPlace_specials_count:[NSString stringWithFormat:@"%d",specialCount]];
            //NSLog(@"Count : %d",specialCount);
        }
        
        [modalArray addObject:modal];
         modal=nil;
    }
    
    return modalArray;
}

#pragma mark --- Like And DisLike ISL

- (void)likeOrDislikeToServer:(ResturantModal *)userdetails  withCompletion:(void(^)(id obj1))completionBlock
{
    NSDictionary *dict = @{@"place_id":userdetails.placeID,@"user_id":userdetails.userID,@"is_like":userdetails.likeORDislike};
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    [ServiceHelper sendRequestToServerWithParameters:@"likeAndDislike.json" forhttpMethod:@"POST" withrequestBody:jsonString sync:YES completion:^(id obj) {
        if ([obj isKindOfClass:[NSDictionary class]]) {
            ResturantModal *modal = [[ResturantModal alloc]init];
            [modal setResult:[obj valueForKey:@"result"]];
            [modal setMessage:[obj valueForKey:@"message"]];
            if (completionBlock) {
                completionBlock(modal);
                modal=nil;
            }
        }
        else
        {
            completionBlock(obj);
            obj=nil;
        }
    }];
}

#pragma mark --- Favourite Place ISL

- (void)favouriteToServer:(ResturantModal *)userdetails  withCompletion:(void(^)(id obj1))completionBlock
{
    NSDictionary *dict = @{@"place_id":userdetails.placeID,@"user_id":userdetails.userID};
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    
    [ServiceHelper sendRequestToServerWithParameters:@"placeFavorites.json" forhttpMethod:@"POST" withrequestBody:jsonString sync:YES completion:^(id obj) {
        if ([obj isKindOfClass:[NSDictionary class]]) {
            ResturantModal *modal = [[ResturantModal alloc]init];
            [modal setResult:[obj valueForKey:@"result"]];
            [modal setMessage:[obj valueForKey:@"message"]];
            if (completionBlock) {
                completionBlock(modal);
            }
        }
        else
        {
            completionBlock(obj);
        }
    }];
}

#pragma mark --- UnFavourite Place ISL

- (void)unFavouriteToServer:(ResturantModal *)userdetails  withCompletion:(void(^)(id obj1))completionBlock
{
    NSDictionary *dict = @{@"place_id":userdetails.placeID,@"user_id":userdetails.userID};
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    
    [ServiceHelper sendRequestToServerWithParameters:@"placeUnFavorites.json" forhttpMethod:@"POST" withrequestBody:jsonString sync:YES completion:^(id obj) {
        if ([obj isKindOfClass:[NSDictionary class]]) {
            ResturantModal *modal = [[ResturantModal alloc]init];
            [modal setResult:[obj valueForKey:@"result"]];
            [modal setMessage:[obj valueForKey:@"message"]];
            if (completionBlock) {
                completionBlock(modal);
            }
        }
        else
        {
            completionBlock(obj);
        }
    }];
}

- (void)favUnfavSpecialToServer:(ResturantModal *)userdetails :(NSString *)strFav :(NSString *)strType  withCompletion:(void(^)(id obj1))completionBlock{
    NSDictionary *dict = @{@"special_id":userdetails.specials_ID,@"user_id":userdetails.userID,@"is_favorite":strFav,@"type":strType};
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
 
    [ServiceHelper sendRequestToServerWithParameters:@"likeAndDislikeSpecialFavorite.json" forhttpMethod:@"POST" withrequestBody:jsonString sync:YES completion:^(id obj) {
        if ([obj isKindOfClass:[NSDictionary class]]) {
            ResturantModal *modal = [[ResturantModal alloc]init];
            [modal setResult:[obj valueForKey:@"result"]];
            [modal setMessage:[obj valueForKey:@"message"]];
            if (completionBlock) {
                completionBlock(modal);
            }
        }
        else
        {
            completionBlock(obj);
        }
    }];
}

#pragma mark ---- GivenRating - ISL

- (void)ratingGivenToServer:(ResturantModal *)userdetails  withCompletion:(void(^)(id obj1))completionBlock
{
    NSDictionary *dict = @{@"place_id":userdetails.placeID,@"user_id":userdetails.userID,@"rating":userdetails.userRatings};
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    [ServiceHelper sendRequestToServerWithParameters:@"Ratings.json" forhttpMethod:@"POST" withrequestBody:jsonString sync:YES completion:^(id obj) {
        if ([obj isKindOfClass:[NSDictionary class]]) {
            ResturantModal *modal = [[ResturantModal alloc]init];
            [modal setResult:[obj valueForKey:@"result"]];
            [modal setMessage:[obj valueForKey:@"message"]];
            
            NSString *place_ratingsStr = [obj valueForKey:@"place_ratings"];
            if(place_ratingsStr != (id)[NSNull null])
                [modal setPlaceRatings:place_ratingsStr];
            else
                [modal setPlaceRatings:@""];
            
            
            NSString *user_ratingsStr = [obj valueForKey:@"rating"];
            if(user_ratingsStr != (id)[NSNull null])
                [modal setUserRatings:user_ratingsStr];
            else
                [modal setUserRatings:@""];
            
            
            if (completionBlock) {
                completionBlock(modal);
            }
        }
        else{
            completionBlock(obj);
        }
    }];
}
#pragma mark -- Post Image to Sever
- (void)postImagesToServer:(ResturantModal *)userdetails  withCompletion:(void(^)(id obj1))completionBlock
{
    NSDictionary *dict = @{@"place_id":userdetails.placeID,@"user_id":userdetails.userID,@"picture":userdetails.commentsImage};
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    [ServiceHelper sendRequestToServerWithParameters:@"PostPicture.json" forhttpMethod:@"POST" withrequestBody:jsonString sync:YES completion:^(id obj) {
        if ([obj isKindOfClass:[NSDictionary class]]) {
            ResturantModal *modal = [[ResturantModal alloc]init];
            [modal setResult:[obj valueForKey:@"result"]];
            [modal setMessage:[obj valueForKey:@"message"]];
            if (completionBlock) {
                completionBlock(modal);
            }
        }
        else
        {
            completionBlock(obj);
        }
        
    }];
}

#pragma mark --- Post Comments ISL

- (void)postCommentsToServer:(ResturantModal *)userdetails  withCompletion:(void(^)(id obj1))completionBlock
{
    if (userdetails.comment_for_checkin == nil)
    {
        userdetails.comment_for_checkin = @"0";
    }
    NSDictionary *dict = @{@"place_id":userdetails.placeID,@"user_id":userdetails.userID,@"comments":userdetails.comments,@"is_emoji":userdetails.is_Emoji,@"comments_image":userdetails.commentsImage,@"system_message":userdetails.systemMessage,@"cmd":userdetails.cmdNumber,@"comment_for_checkin":userdetails.comment_for_checkin,@"rating":userdetails.UserRating_comment};

    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    [ServiceHelper sendRequestToServerWithParameters:@"PostComments.json" forhttpMethod:@"POST" withrequestBody:jsonString sync:YES completion:^(id obj) {
        if ([obj isKindOfClass:[NSDictionary class]]) {
            ResturantModal *modal = [[ResturantModal alloc]init];
            [modal setResult:[obj valueForKey:@"result"]];
            [modal setMessage:[obj valueForKey:@"message"]];
            if (completionBlock) {
                completionBlock(modal);
            }
        }
        else
        {
            completionBlock(obj);
        }
        
    }];
}


#pragma mark -- List All Comments ISL

- (void)listAllCommentsValuesToServer:(ResturantModal *)userdetails  withCompletion:(void(^)(id obj1))completionBlock
{
    
    NSDictionary *dict = @{@"place_id":userdetails.placeID,@"user_id":userdetails.userID};
    
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    
    [ServiceHelper sendRequestToServerWithParameters:@"RetrieveLiveFeedByHotel.json" forhttpMethod:@"POST" withrequestBody:jsonString sync:YES completion:^(id obj) {
        if ([obj isKindOfClass:[NSDictionary class]]) {
            ResturantModal *modal = [[ResturantModal alloc]init];
            NSMutableArray *objArray = [[NSMutableArray alloc]init];
            [modal setResult:[obj valueForKey:@"result"]];
            [modal setMessage:[obj valueForKey:@"message"]];

            if([[obj valueForKey:@"result"] isEqualToString:@"success"])
            {
                objArray = [self setListAllCommentsValue:obj];
            }
            if (completionBlock) {
                completionBlock(objArray);
            }
           }
        else
        {
            completionBlock(obj);
        }
    }];
}

-(NSMutableArray *)setListAllCommentsValue :(id)arg
{
    ResturantModal *modal;
    NSMutableArray *modalArray = [[NSMutableArray alloc]init];
    
    NSArray *dictArray = [arg valueForKey:@"users_comments"];
    
    for(NSDictionary *dict in dictArray)
    {
        if(dict != nil)
        {
            modal = [[ResturantModal alloc]init];
            NSString *isComment = [arg valueForKey:@"is_commented"];
            if(isComment != (id)[NSNull null])
                [modal setIsCommentAdded:isComment];
            else
                [modal setIsCommentAdded:@""];
            // New Added
            

            NSString *isRate = [arg valueForKey:@"is_rated"];
            if(isRate != (id)[NSNull null])
                [modal setIsRated:isRate];
            else
                [modal setIsRated:@""];
            NSString *place_idStr = [dict valueForKey:@"place_id"];
            if(place_idStr != (id)[NSNull null])
                [modal setPlaceID:place_idStr];
            else
                [modal setPlaceID:@""];
            
            NSString *review_idStr = [dict valueForKey:@"review_id"];
            if(review_idStr != (id)[NSNull null])
                [modal setReviewID:review_idStr];
            else
                [modal setReviewID:@""];
            
            NSNumber *userrating = [dict valueForKey:@"rating"];
            NSString *UserRating = [NSString stringWithFormat:@"%@",userrating];
            if(UserRating != (id)[NSNull null])
                [modal setUserGivenRating:UserRating];
            else
                [modal setUserGivenRating:@"0"];

            NSString *commentsStr = [dict valueForKey:@"comments"];
            if(commentsStr != (id)[NSNull null])
                [modal setComments:commentsStr];
            else
                [modal setComments:@""];
            
            NSString *user_idStr = [dict valueForKey:@"user_id"];
            if(user_idStr != (id)[NSNull null])
                [modal setUserID:user_idStr];
            else
                [modal setUserID:@""];
            
            NSString *comments_imageStr = [dict valueForKey:@"comments_image"];
            if(comments_imageStr != (id)[NSNull null])
                [modal setCommentsImage:comments_imageStr];
            else
                [modal setCommentsImage:@""];
            
            
            
            NSString *is_emojiStr = [dict valueForKey:@"is_emoji"];
            if(is_emojiStr != (id)[NSNull null])
                [modal setIs_Emoji:is_emojiStr];
            else
                [modal setIs_Emoji:@""];
            
            NSString *created_dateStr = [dict valueForKey:@"created_date"];
            if(created_dateStr != (id)[NSNull null])
                [modal setCreatedOn:created_dateStr];
            else
                [modal setCreatedOn:@""];
            
            NSString *firstNameStr = [dict valueForKey:@"first_name"];
            if(firstNameStr != (id)[NSNull null])
                [modal setFirstName:firstNameStr];
            else
                [modal setFirstName:@""];
            
            NSString *lastNameStr = [dict valueForKey:@"last_name"];
            if(lastNameStr != (id)[NSNull null])
                [modal setLastName:lastNameStr];
            else
                [modal setLastName:@""];
            
            NSString *profile_imageStr = [dict valueForKey:@"profile_image"];
            if(profile_imageStr != (id)[NSNull null])
                [modal setProfileImage:profile_imageStr];
            else
                [modal setProfileImage:@""];
            
            NSString *user_nameStr = [dict valueForKey:@"user_name"];
            if(user_nameStr != (id)[NSNull null])
                [modal setUserName:user_nameStr];
            else
                [modal setUserName:@""];
        }
        [modalArray addObject:modal];
    }
    return modalArray;
}

#pragma mark -- List All Spotlyte Promotions for the Resturant

- (void)listAllDailySpecialsFromServer:(ResturantModal*)userdetails withCompletion:(void (^)(id obj1))completionBlock;
{
    NSDictionary *dict = @{@"place_id":userdetails.placeID};
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    
    [ServiceHelper sendRequestToServerWithParameters:@"ClickhereForSpecial.json" forhttpMethod:@"POST" withrequestBody:jsonString sync:YES completion:^(id obj) {
        if ([obj isKindOfClass:[NSDictionary class]]) {
            
            ResturantModal *modal = [[ResturantModal alloc]init];
            NSMutableArray *objArray = [[NSMutableArray alloc]init];
            [modal setResult:[obj valueForKey:@"result"]];
            [modal setMessage:[obj valueForKey:@"message"]];
            if([[obj valueForKey:@"result"] isEqualToString:@"success"])
            {
                objArray = [self setListAllSpecialsValues:obj];
            }
            if (completionBlock) {
                completionBlock(objArray);
            }
        }
        else
        {
            completionBlock(obj);
        }
    }];
}
#pragma mark - Data For Daily Specials Of Resturant

-(NSMutableDictionary *)testSpecialsData
{
    NSMutableDictionary *outerDict = [[NSMutableDictionary alloc] init];
    
    [outerDict setObject:@"found" forKey:@"message"];
    [outerDict setObject:@"success" forKey:@"result"];
    
    NSMutableArray *arrSpl = [[NSMutableArray alloc] init];
    NSArray *mondyVal = [NSArray arrayWithObjects:@"Beer-$3 Miller Domestics", @"Test 2",@"Test 3", nil];
    NSDictionary *mondayDict = [NSDictionary dictionaryWithObject:mondyVal forKey:@"Monday"];
    
    NSArray *tuesVal = [NSArray arrayWithObjects:@"Beer-$3 Miller Domestics", @"Test 2",@"Test 3", nil];
    NSDictionary *tuesDict = [NSDictionary dictionaryWithObject:tuesVal forKey:@"Tuesday"];
    
    NSArray *wedVal = [NSArray arrayWithObjects:@"Beer-$3 Miller Domestics", @"Test 2",@"Test 3", nil];
    NSDictionary *wedDict = [NSDictionary dictionaryWithObject:wedVal forKey:@"Wednesday"];
    
    [arrSpl addObject:mondayDict];
    [arrSpl addObject:tuesDict];
    [arrSpl addObject:wedDict];
    
    
    [outerDict setObject:arrSpl forKey:@"specials"];
    
    return outerDict;
}


-(NSMutableArray *)setListAllSpecialsValues :(id)arg
{
    ResturantModal *modal1;
    NSMutableArray *modalArray = [[NSMutableArray alloc]init];
    
    NSArray *dictArray = [arg valueForKey:@"specials"];
    
    @try {
        
        for(NSDictionary *dictSpecial in dictArray)
        {
            if(dictSpecial != nil)
            {
                NSArray *allKeys    = [dictSpecial allKeys];
                NSString *keyName   = [allKeys objectAtIndex:0];
                NSArray *arrDailySpecials = [dictSpecial objectForKey:keyName];
                
                
                modal1= [[ResturantModal alloc]init];
                
                
                if(arrDailySpecials.count>0)
                {
                    [modal1 setArraySpecials:arrDailySpecials];
                }
                
                
                
                if(keyName != (id)[NSNull null])
                    [modal1 setSpecials_weekDay:keyName];
                else
                    [modal1 setSpecials_weekDay:@""];
                
            }
            [modalArray addObject:modal1];
        }
        
        
        
    }
    @catch (NSException *exception) {
        
        
        
    }
    @finally {
        
        
        
    }
    
    
    
    
    return modalArray;
}

#pragma mark --- Retrieve All Favourites  ISL

- (void)listAllFavouriteToServer:(ResturantModal *)userdetails  withCompletion:(void(^)(id obj1))completionBlock
{
    
    NSString* latitude=[NSString stringWithFormat:@"%f",APP_DELEGATE.userCurrentLocation.coordinate.latitude];
    NSString* longitude=[NSString stringWithFormat:@"%f",APP_DELEGATE.userCurrentLocation.coordinate.longitude];
    NSDictionary *dict = @{@"user_id":userdetails.userID,@"current_latitude":latitude,@"current_longitude":longitude};
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    [ServiceHelper sendRequestToServerWithParameters:@"RetrieveFavouritesByHotelAndPromotions.json" forhttpMethod:@"POST" withrequestBody:jsonString sync:YES completion:^(id obj) {
        if ([obj isKindOfClass:[NSDictionary class]]) {
            ResturantModal *modal = [[ResturantModal alloc]init];
            
            
            NSMutableArray *objArray = [self setListAllValue:obj check:@"place_favorites"];
            NSMutableArray *obj1Array = [self setPromotionsValue:obj];
            [modal setArrayPlaceFavourites:objArray];
            [modal setArrayHotelFavourites:obj1Array];
            if (completionBlock) {
                completionBlock(modal);
            }
        }
        else
        {
            completionBlock(obj);
        }
    }];
}

- (void)listAllLikesDislikeToServer:(ResturantModal *)userdetails  withCompletion:(void(^)(id obj1))completionBlock
{
    NSDictionary *dict = @{@"user_id":userdetails.userID,@"is_like":userdetails.likeORDislike};
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    [ServiceHelper sendRequestToServerWithParameters:@"likeOrDislike.json" forhttpMethod:@"POST" withrequestBody:jsonString sync:YES completion:^(id obj) {
        if ([obj isKindOfClass:[NSDictionary class]]) {
            ResturantModal *modal = [[ResturantModal alloc]init];
            
            NSMutableArray *objArray = [self setListAllValue:obj check:@"data"];
            [modal setArrayPlaceFavourites:objArray];
    
            if (completionBlock) {
                completionBlock(modal);
            }
        }
        else
        {
            completionBlock(obj);
        }
    }];
}

- (void)checkInsToServer:(ResturantModal *)userdetails  withCompletion:(void(^)(id obj1))completionBlock
{
    NSDictionary *dict = @{@"user_id":userdetails.userID};
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    [ServiceHelper sendRequestToServerWithParameters:@"userCheckins.json" forhttpMethod:@"POST" withrequestBody:jsonString sync:YES completion:^(id obj) {
        if ([obj isKindOfClass:[NSDictionary class]]) {
            ResturantModal *modal = [[ResturantModal alloc]init];
            
            NSMutableArray *objArray = [self setListAllValue:obj check:@"data"];
            [modal setArrayPlaceFavourites:objArray];
            
            if (completionBlock) {
                completionBlock(modal);
            }
        }
        else
        {
            completionBlock(obj);
        }
    }];
}

- (void)commentsToServer:(ResturantModal *)userdetails  withCompletion:(void(^)(id obj1))completionBlock
{
    NSDictionary *dict = @{@"user_id":userdetails.userID};
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    [ServiceHelper sendRequestToServerWithParameters:@"userComments.json" forhttpMethod:@"POST" withrequestBody:jsonString sync:YES completion:^(id obj) {
        if ([obj isKindOfClass:[NSDictionary class]]) {
            ResturantModal *modal = [[ResturantModal alloc]init];
            
            NSMutableArray *objArray = [self setListAllValue:obj check:@"data"];
            [modal setArrayPlaceFavourites:objArray];
            
            if (completionBlock) {
                completionBlock(modal);
            }
        }
        else
        {
            completionBlock(obj);
        }
    }];
}

- (void)ratingsToServer:(ResturantModal *)userdetails  withCompletion:(void(^)(id obj1))completionBlock
{
    NSDictionary *dict = @{@"user_id":userdetails.userID};
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    [ServiceHelper sendRequestToServerWithParameters:@"userRatings.json" forhttpMethod:@"POST" withrequestBody:jsonString sync:YES completion:^(id obj) {
        if ([obj isKindOfClass:[NSDictionary class]]) {
            ResturantModal *modal = [[ResturantModal alloc]init];
            
            NSMutableArray *objArray = [self setListAllValue:obj check:@"data"];
            [modal setArrayPlaceFavourites:objArray];
            
            if (completionBlock) {
                completionBlock(modal);
            }
        }
        else
        {
            completionBlock(obj);
        }
    }];
}

- (void)likeAndDislikeSpecialFavoriteToServer:(ResturantModal *)userdetails  withCompletion:(void(^)(id obj1))completionBlock
{
    NSDictionary *dict = @{@"user_id":userdetails.userID};
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    [ServiceHelper sendRequestToServerWithParameters:@"userFavoriteItem.json" forhttpMethod:@"POST" withrequestBody:jsonString sync:YES completion:^(id obj) {
        if ([obj isKindOfClass:[NSDictionary class]]) {
            ResturantModal *modal = [[ResturantModal alloc]init];
            
            NSMutableArray *objArray = [self setListAllValue:obj check:@"data"];
            [modal setArrayPlaceFavourites:objArray];
            
            if (completionBlock) {
                completionBlock(modal);
            }
        }
        else
        {
            completionBlock(obj);
        }
    }];
}

-(NSMutableArray *)setPromotionsValue :(id)arg
{
    ResturantModal *modal;
    NSMutableArray *modalArray = [[NSMutableArray alloc]init];
    NSArray *dictArray = [arg valueForKey:@"promotion_favorites"];
    
    
    for(NSDictionary *dict in dictArray)
    {
        modal = [[ResturantModal alloc]init];
        if(dict != nil)
        {
            /// promotions details
            NSString *promotions_idStr = [dict valueForKey:@"promotion_id"];
            if(promotions_idStr != (id)[NSNull null])
                [modal setPromotionID:promotions_idStr];
            else
                [modal setPromotionID:@""];
            
            NSString *promotionTitleStr = [dict valueForKey:@"promotion_title"];
            if(promotionTitleStr != (id)[NSNull null])
                [modal setPromotionTitle:promotionTitleStr];
            else
                [modal setPromotionTitle:@""];
            
            
            NSString *promotions_imageStr = [dict valueForKey:@"promotion_image"];
            if(promotions_imageStr != (id)[NSNull null])
                [modal setPromotionImage:promotions_imageStr];
            else
                [modal setPromotionImage:@""];
            
            NSString* is_Promofavorite = [dict valueForKey:@"is_favPromotion"];
            [modal setIsCheckedPromotionFav:is_Promofavorite];
            
            
            /// Place details
            NSString *place_idStr = [dict valueForKey:@"place_id"];
            if(place_idStr != (id)[NSNull null])
                [modal setPlaceID:place_idStr];
            else
                [modal setPlaceID:@""];
            
            NSString *titleStr = [dict valueForKey:@"title"];
            if(titleStr != (id)[NSNull null])
                [modal setResturantName:titleStr];
            else
                [modal setResturantName:@""];
            
            NSString *subtitleStr = [dict valueForKey:@"subtitle"];
            if(subtitleStr != (id)[NSNull null])
                [modal setSubTitle:subtitleStr];
            else
                [modal setSubTitle:@""];
            
            NSString *descriptionStr = [dict valueForKey:@"description"];
            if(descriptionStr != (id)[NSNull null])
                [modal setResturantDescription:descriptionStr];
            else
                [modal setResturantDescription:@""];
            
            NSString *place_typeStr = [dict valueForKey:@"place_type"];
            if(place_typeStr != (id)[NSNull null])
                [modal setPlaceType:place_typeStr];
            else
                [modal setPlaceType:@""];
            
            
            
            NSString *email_idStr = [dict valueForKey:@"website"];
            if(email_idStr != (id)[NSNull null])
                [modal setResturantEMail:email_idStr];
            else
                [modal setResturantEMail:@""];
            
            NSString *cityStr = [dict valueForKey:@"city"];
            if(cityStr != (id)[NSNull null])
                [modal setResturantCity:cityStr];
            else
                [modal setResturantCity:@""];
            
            NSString *address1Str = [dict valueForKey:@"address"];
            if(address1Str != (id)[NSNull null])
                [modal setResturantAddress:address1Str];
            else
                [modal setResturantAddress:@""];
            
            NSString *address2Str = [dict valueForKey:@"address2"];
            if(address2Str != (id)[NSNull null])
                [modal setResturantAddress2:address2Str];
            else
                [modal setResturantAddress2:@""];
            
            NSString *zipStr = [dict valueForKey:@"zip"];
            if(zipStr != (id)[NSNull null])
                [modal setResturantPostalCode:zipStr];
            else
                [modal setResturantPostalCode:@""];
            
            NSString *mobileStr = [dict valueForKey:@"mobile"];
            if(mobileStr != (id)[NSNull null])
                [modal setResturantmobileNo:mobileStr];
            else
                [modal setResturantmobileNo:@""];
            
            NSString *stateStr = [dict valueForKey:@"state"];
            if(stateStr != (id)[NSNull null])
                [modal setResturantState:stateStr];
            else
                [modal setResturantState:@""];
            
            NSString *countryStr = [dict valueForKey:@"country"];
            if(countryStr != (id)[NSNull null])
                [modal setResturantCountry:countryStr];
            else
                [modal setResturantCountry:@""];
            
            NSString *phone_numberStr = [dict valueForKey:@"phone_number"];
            if(phone_numberStr != (id)[NSNull null])
                [modal setResturantPhoneNo:phone_numberStr];
            else
                [modal setResturantPhoneNo:@""];
            
            NSString *place_imageStr = [dict valueForKey:@"place_image"];
            if(place_imageStr != (id)[NSNull null])
                [modal setPlaceImage:place_imageStr];
            else
                [modal setPlaceImage:@""];
            
            NSString *DetailPlace_imageStr = [dict valueForKey:@"place_imagebig"];
            if(DetailPlace_imageStr != (id)[NSNull null])
                [modal setDetailViewPlaceImage:DetailPlace_imageStr];
            else
                [modal setDetailViewPlaceImage:@""];
            
            NSString *open_fromStr = [dict valueForKey:@"open_from"];
            if(open_fromStr != (id)[NSNull null])
                [modal setOpenFrom:open_fromStr];
            else
                [modal setOpenFrom:@""];
            
            NSString *open_toStr = [dict valueForKey:@"open_to"];
            if(open_toStr != (id)[NSNull null])
                [modal setOpenTo:open_toStr];
            else
                [modal setOpenTo:@""];
            
            NSString *longitudeStr = [dict valueForKey:@"longitude"];
            if(longitudeStr != (id)[NSNull null])
                [modal setResturantLongtitude:longitudeStr];
            else
                [modal setResturantLongtitude:@""];
            
            NSString *latitudeStr = [dict valueForKey:@"latitude"];
            if(latitudeStr != (id)[NSNull null])
                [modal setResturantLatitude:latitudeStr];
            else
                [modal setResturantLatitude:@""];
            
            NSString *is_likeStr = [dict valueForKey:@"is_like"];
            if(is_likeStr != (id)[NSNull null])
                [modal setLikeORDislike:is_likeStr];
            else
                [modal setLikeORDislike:@""];
            
            NSString *like_countStr = [dict valueForKey:@"like_count"];
            if(like_countStr != (id)[NSNull null])
                [modal setLikecount:like_countStr];
            else
                [modal setLikecount:@""];
            
            NSString *dislike_countStr = [dict valueForKey:@"dislike_count"];
            if(dislike_countStr != (id)[NSNull null])
                [modal setDislikeCount:dislike_countStr];
            else
                [modal setDislikeCount:@""];
            
            BOOL is_favoriteBool = [[dict valueForKey:@"is_favorite"]boolValue];
            [modal setIsCheckedFavourite:is_favoriteBool];
            
            
            BOOL checkin = [[dict valueForKey:@"checkin"]boolValue];
            [modal setIsCheckedCheckIn:checkin];
            
            NSString *user_ratingsStr = [dict valueForKey:@"user_ratings"];
            if(user_ratingsStr != (id)[NSNull null])
                [modal setUserRatings:user_ratingsStr];
            else
                [modal setUserRatings:@""];
            
            NSString *place_ratingsStr = [dict valueForKey:@"place_ratings"];
            if(place_ratingsStr != (id)[NSNull null])
                [modal setPlaceRatings:place_ratingsStr];
            else
                [modal setPlaceRatings:@""];
            
            NSString *emoji = [dict valueForKey:@"emoji1"];
            if(emoji != (id)[NSNull null])
                [modal setEmonjiID:emoji];
            else
                [modal setEmonjiID:@""];
            
            NSString *emoji2 = [dict valueForKey:@"emoji2"];
            if(emoji2 != (id)[NSNull null])
                [modal setEmonjiID2:emoji2];
            else
                [modal setEmonjiID2:@""];
            
            NSString *emoji3 = [dict valueForKey:@"emoji3"];
            if(emoji3 != (id)[NSNull null])
                [modal setEmonjiID3:emoji3];
            else
                [modal setEmonjiID3:@""];
            
            NSString *emoji4 = [dict valueForKey:@"emoji4"];
            if(emoji4 != (id)[NSNull null])
                [modal setEmonjiID4:emoji4];
            else
                [modal setEmonjiID4:@""];
            
            NSString *latestemonji = [dict valueForKey:@"latest_emoji"];
            if(latestemonji != (id)[NSNull null])
                [modal setLatestEmonji:latestemonji];
            else
                [modal setLatestEmonji:@""];
            
            NSString *emoji_time = [dict valueForKey:@"emoji_time"];
            if(emoji_time != (id)[NSNull null])
                [modal setEmonjiTime:emoji_time];
            else
                [modal setEmonjiTime:@""];
            
            NSString *total_reviews = [dict valueForKey:@"total_reviews"];
            if(total_reviews != (id)[NSNull null])
                [modal setTotalReview:total_reviews];
            else
                [modal setTotalReview:@""];
            
            
            NSString *happyHours_Count = [dict valueForKey:@"happy_hour_count"];
            if(happyHours_Count != (id)[NSNull null])
                [modal setHappy_hour_count:happyHours_Count];
            else
                [modal setHappy_hour_count:@"0"];
            
            
            NSString *placeSpecials_count = [dict valueForKey:@"place_specials_count"];
            if(placeSpecials_count != (id)[NSNull null])
                [modal setPlace_specials_count:placeSpecials_count];
            else
                [modal setPlace_specials_count:@"0"];
            
            NSString *distanceStr = [dict valueForKey:@"miles"];
            if(distanceStr != (id)[NSNull null])
                [modal setMile:distanceStr];
            else
                [modal setMile:@"0"];
            
            
            NSString *yelpStr = [dict valueForKey:@"yelp"];
            if(yelpStr != (id)[NSNull null])
                [modal setYelp_url:yelpStr];
            else
                [modal setYelp_url:@""];
            
            NSString *bearStr = [dict valueForKey:@"beer_menus"];
            if(bearStr != (id)[NSNull null])
                [modal setBeer_menus:bearStr];
            else
                [modal setBeer_menus:@""];
            
            NSString *openTable = [dict valueForKey:@"open_table"];
            if(openTable != (id)[NSNull null])
                [modal setOpen_table:openTable];
            else
                [modal setOpen_table:@""];
            
            NSString *grubhub = [dict valueForKey:@"grubhub"];
            if(grubhub != (id)[NSNull null])
                [modal setGrub_hub:grubhub];
            else
                [modal setGrub_hub:@""];
        }
        [modalArray addObject:modal];
    }
    return modalArray;
}


#pragma mark -- List Top Rated ISL

- (void)listTopRatedToServer:(ResturantModal *)userdetails  withCompletion:(void(^)(id obj1))completionBlock
{
    
    NSDictionary *dict = @{@"user_id":userdetails.userID};
    
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    
    [ServiceHelper sendRequestToServerWithParameters:@"TopRates.json" forhttpMethod:@"POST" withrequestBody:jsonString sync:YES completion:^(id obj) {
        if ([obj isKindOfClass:[NSDictionary class]]) {
            
            ResturantModal *modal = [[ResturantModal alloc]init];
            NSArray *objArrayOthers = nil;
            NSArray *objArrayBars = nil;
            NSArray *objArrayResturants = nil;
            
            [modal setResult:[obj valueForKey:@"result"]];
            [modal setMessage:[obj valueForKey:@"message"]];
            if([[obj valueForKey:@"result"] isEqualToString:@"success"])
            {
                NSArray *barsArray = [obj valueForKey:@"bars"];
                NSArray *resturantsArray = [obj valueForKey:@"restaurants"];
                NSArray *othersArray = [obj valueForKey:@"others"];
                
                
                if(barsArray.count>0)
                {
                    objArrayBars = [self setListAllValue:barsArray check:@"toprated"];
                    [modal setArrayBars:objArrayBars];
                }
                if(resturantsArray.count>0)
                {
                    objArrayResturants = [self setListAllValue:resturantsArray check:@"toprated"];
                    [modal setArrayResturants:objArrayResturants];
                }
                if(othersArray.count>0)
                {
                    objArrayOthers = [self setListAllValue:othersArray check:@"toprated"];
                    [modal setArrayOthers:objArrayOthers];
                }
                
                
            }
            if (completionBlock) {
                completionBlock(modal);
            }
        }
        else
        {
            completionBlock(obj);
        }
    }];
}

-(NSMutableArray *)setListTopRateAllValue :(NSArray *)arg
{
    ResturantModal *modal;
    NSMutableArray *modalArray = [[NSMutableArray alloc]init];
    
    for(NSDictionary *dict in arg)
    {
        modal = [[ResturantModal alloc]init];
        if(dict != nil)
        {
            
            NSString *rating11 = [dict valueForKey:@"rating"];
            if(rating11 != (id)[NSNull null])
                [modal setRatingValueINNumber:rating11];
            else
                [modal setRatingValueINNumber:@""];
            
            NSString *titleStr = [dict valueForKey:@"place"];
            if(titleStr != (id)[NSNull null])
                [modal setResturantName:titleStr];
            else
                [modal setResturantName:@""];
            
            NSString *place_typeStr = [dict valueForKey:@"place_type"];
            if(place_typeStr != (id)[NSNull null])
                [modal setPlaceType:place_typeStr];
            else
                [modal setPlaceType:@"0"];
            
            
            NSString *descriptionStr = [dict valueForKey:@"Description"];
            if(descriptionStr != (id)[NSNull null])
                [modal setResturantDescription:descriptionStr];
            else
                [modal setResturantDescription:@""];
            
            
            NSString *email_idStr = [dict valueForKey:@"website"];
            if(email_idStr != (id)[NSNull null])
                [modal setResturantEMail:email_idStr];
            else
                [modal setResturantEMail:@""];
            
            NSString *cityStr = [dict valueForKey:@"city"];
            if(cityStr != (id)[NSNull null])
                [modal setResturantCity:cityStr];
            else
                [modal setResturantCity:@""];
            
            NSString *address1Str = [dict valueForKey:@"address1"];
            if(address1Str != (id)[NSNull null])
                [modal setResturantAddress:address1Str];
            else
                [modal setResturantAddress:@""];
            
            NSString *address2Str = [dict valueForKey:@"address2"];
            if(address2Str != (id)[NSNull null])
                [modal setResturantAddress2:address2Str];
            else
                [modal setResturantAddress2:@""];
            
            NSString *zipStr = [dict valueForKey:@"zip"];
            if(zipStr != (id)[NSNull null])
                [modal setResturantPostalCode:zipStr];
            else
                [modal setResturantPostalCode:@""];
            
            
            NSString *stateStr = [dict valueForKey:@"state"];
            if(stateStr != (id)[NSNull null])
                [modal setResturantState:stateStr];
            else
                [modal setResturantState:@""];
            
            NSString *countryStr = [dict valueForKey:@"country"];
            if(countryStr != (id)[NSNull null])
                [modal setResturantCountry:countryStr];
            else
                [modal setResturantCountry:@""];
            
            NSString *phone_numberStr = [dict valueForKey:@"phone"];
            if(phone_numberStr != (id)[NSNull null])
                [modal setResturantPhoneNo:phone_numberStr];
            else
                [modal setResturantPhoneNo:@""];
            
            NSString *place_imageStr = [dict valueForKey:@"image_url"];
            if(place_imageStr != (id)[NSNull null])
                [modal setPlaceImage:place_imageStr];
            else
                [modal setPlaceImage:@""];
            
            
            
            
        }
        [modalArray addObject:modal];
    }
    
    
    return modalArray;
}


#pragma mark --- Retrieve Comments Image  ISL

- (void)listAllcommentsImageToServer:(ResturantModal *)userdetails  withCompletion:(void(^)(id obj1))completionBlock
{
    NSDictionary *dict = @{@"user_id":userdetails.userID};
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    
    [ServiceHelper sendRequestToServerWithParameters:@"pictureList.json" forhttpMethod:@"POST" withrequestBody:jsonString sync:YES completion:^(id obj) {
        if ([obj isKindOfClass:[NSDictionary class]]) {
            ResturantModal *modal = [[ResturantModal alloc]init];
            NSMutableArray *objArray = [[NSMutableArray alloc]init];
            [modal setResult:[obj valueForKey:@"result"]];
            [modal setMessage:[obj valueForKey:@"message"]];
            if([[obj valueForKey:@"result"] isEqualToString:@"success"])
            {
                objArray = [self setUsersCommentImageValue:obj];
            }
            if (completionBlock) {
                completionBlock(objArray);
            }
        }
        else
        {
            completionBlock(obj);
        }
    }];
}

-(NSMutableArray *)setUsersCommentImageValue :(id)arg
{
    ResturantModal *modal;
    NSMutableArray *modalArray = [[NSMutableArray alloc]init];
    NSArray *dictArray = [arg valueForKey:@"data"];
    
    
    for(NSDictionary *dict in dictArray)
    {
        modal = [[ResturantModal alloc]init];
        if(dict != nil)
        {
            
            
            NSString *created_date = [dict valueForKey:@"created_date"];
            if(created_date != (id)[NSNull null])
                [modal setCreatedOn:created_date];
            else
                [modal setCreatedOn:@""];
            
            
            NSString *place = [dict valueForKey:@"place"];
            if(place != (id)[NSNull null])
                [modal setResturantName:place];
            else
                [modal setResturantName:@""];
            
            NSString *commend = [dict valueForKey:@"commend"];
            if(place != (id)[NSNull null])
                [modal setResturantDescription:commend];
            else
                [modal setResturantDescription:@""];
            
            NSString *reviews_url = [dict valueForKey:@"image_name"];
            if(place != (id)[NSNull null])
                [modal setCommentsImage:reviews_url];
            else
                [modal setCommentsImage:@""];
            
            
        }
        [modalArray addObject:modal];
    }
    
    
    return modalArray;
}
-(NSMutableArray *)setcommentsImageValue :(id)arg
{
    ResturantModal *modal;
    NSMutableArray *modalArray = [[NSMutableArray alloc]init];
    NSArray *dictArray = [arg valueForKey:@"data"];
    
    
    for(NSDictionary *dict in dictArray)
    {
        modal = [[ResturantModal alloc]init];
        if(dict != nil)
        {
            
            
            NSString *created_date = [dict valueForKey:@"created_date"];
            if(created_date != (id)[NSNull null])
                [modal setCreatedOn:created_date];
            else
                [modal setCreatedOn:@""];
            
            
            NSString *place = [dict valueForKey:@"place"];
            if(place != (id)[NSNull null])
                [modal setResturantName:place];
            else
                [modal setResturantName:@""];
            
            NSString *commend = [dict valueForKey:@"commend"];
            if(place != (id)[NSNull null])
                [modal setResturantDescription:commend];
            else
                [modal setResturantDescription:@""];
            
            NSString *reviews_url = [dict valueForKey:@"reviews_url"];
            if(place != (id)[NSNull null])
                [modal setCommentsImage:reviews_url];
            else
                [modal setCommentsImage:@""];
            
            
        }
        [modalArray addObject:modal];
    }
    
    
    return modalArray;
}

#pragma mark --- Retrieve Hotel Based All Comments Image  ISL

- (void)listAllHotelcommentsImageToServer:(ResturantModal *)userdetails  withCompletion:(void(^)(id obj1))completionBlock
{
    NSDictionary *dict = @{@"place_id":userdetails.placeID};
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    
    [ServiceHelper sendRequestToServerWithParameters:@"placePictureList.json" forhttpMethod:@"POST" withrequestBody:jsonString sync:YES completion:^(id obj) {
        if ([obj isKindOfClass:[NSDictionary class]]) {
            ResturantModal *modal = [[ResturantModal alloc]init];
            NSMutableArray *objArray = [[NSMutableArray alloc]init];
            [modal setResult:[obj valueForKey:@"result"]];
            [modal setMessage:[obj valueForKey:@"message"]];
            if([[obj valueForKey:@"result"] isEqualToString:@"success"])
            {
                objArray = [self setHotelImageValue:obj];
            }
            if (completionBlock) {
                completionBlock(objArray);
            }
        }
        else
        {
            completionBlock(obj);
        }
    }];
}

-(NSMutableArray *)setHotelImageValue :(id)arg
{
    ResturantModal *modal;
    NSMutableArray *modalArray = [[NSMutableArray alloc]init];
    NSArray *dictArray = [arg valueForKey:@"data"];
    
    for(NSDictionary *dict in dictArray)
    {
        modal = [[ResturantModal alloc]init];
        if(dict != nil)
        {
            
            NSString *user_name = [dict valueForKey:@"user_name"];
            if(user_name != (id)[NSNull null])
                [modal setUserName:user_name];
            else
                [modal setUserName:@""];
            
            NSString *reviews_url = [dict valueForKey:@"image_name"];
            if(reviews_url != (id)[NSNull null])
                [modal setCommentsImage:reviews_url];
            else
                [modal setCommentsImage:@""];
            
            
        }
        [modalArray addObject:modal];
    }
    
    
    return modalArray;
}

-(NSMutableArray *)setHotelcommentsImageValue :(id)arg
{
    ResturantModal *modal;
    NSMutableArray *modalArray = [[NSMutableArray alloc]init];
    NSArray *dictArray = [arg valueForKey:@"review_images"];
    
    
    for(NSDictionary *dict in dictArray)
    {
        modal = [[ResturantModal alloc]init];
        if(dict != nil)
        {
            
            NSString *user_name = [dict valueForKey:@"user_name"];
            if(user_name != (id)[NSNull null])
                [modal setUserName:user_name];
            else
                [modal setUserName:@""];
            
            NSString *reviews_url = [dict valueForKey:@"review_image"];
            if(reviews_url != (id)[NSNull null])
                [modal setCommentsImage:reviews_url];
            else
                [modal setCommentsImage:@""];
            
            
        }
        [modalArray addObject:modal];
    }
    
    
    return modalArray;
}


#pragma mark --- Check In ISL

- (void)checkInToServer:(ResturantModal *)userdetails  withCompletion:(void(^)(id obj1))completionBlock
{
    NSDictionary *dict = @{@"user_id":userdetails.userID,@"place_id":userdetails.placeID,@"checkin":userdetails.checkInStr};
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    
    [ServiceHelper sendRequestToServerWithParameters:@"SetHotelCheckIn.json" forhttpMethod:@"POST" withrequestBody:jsonString sync:YES completion:^(id obj) {
        if ([obj isKindOfClass:[NSDictionary class]]) {
            ResturantModal *modal = [[ResturantModal alloc]init];
            
            [modal setResult:@"success"];
            [modal setMessage:@"successfully checkin"];
            
            
            if (completionBlock) {
                completionBlock(modal);
            }
        }
        else
        {
            completionBlock(obj);
        }
    }];
}

#pragma mark --- Retrieve Userlevels and Achievements ISL

- (void)retriveUserLevels:(ResturantModal *)userdetails  withCompletion:(void(^)(id obj1))completionBlock
{
    
    NSDictionary *dict = @{@"user_id":userdetails.userID};
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    
    [ServiceHelper sendRequestToServerWithParameters:@"RetrieveUserLevelAndAchivements.json" forhttpMethod:@"POST" withrequestBody:jsonString sync:YES completion:^(id obj) {
        if ([obj isKindOfClass:[NSDictionary class]]) {
            ResturantModal *modal = [[ResturantModal alloc]init];
            [modal setResult:[obj valueForKey:@"result"]];
            [modal setMessage:[obj valueForKey:@"message"]];
            UserLevelProgressModal*model = [[UserLevelProgressModal alloc]init];
            if([[obj valueForKey:@"result"] isEqualToString:@"success"])
            {
                model =[self setUserLevelsAndAchievements:obj];
            }
            if (completionBlock) {
                completionBlock(model);
            }
        }
        else
        {
            completionBlock(obj);
        }
    }];
}

#pragma mark --- Retrive All UserLevels

-(UserLevelProgressModal *)setUserLevelsAndAchievements :(id)arg
{
    UserLevelProgressModal *modal;
    NSDictionary *dict = [arg valueForKey:@"level"];
    NSMutableArray *achArr;
    modal = [[UserLevelProgressModal alloc]init];
    if(dict != nil)
    {
        NSString *userLevel = [dict valueForKey:@"level"];
        if(userLevel != (id)[NSNull null])
            [modal setUserLevel:userLevel];
        else
            [modal setUserLevel:@""];
        
        
        NSString *userlevel_name = [dict valueForKey:@"level_name"];
        if(userlevel_name != (id)[NSNull null])
            [modal setUserLevelName:userlevel_name];
        else
            [modal setUserLevelName:@""];
        
        NSString *achivements = [dict valueForKey:@"percentage"];
        if(achivements != (id)[NSNull null])
            [modal setUserLevelAchievement:achivements];
        else
            [modal setUserLevelAchievement:@""];
        
        
        //Achievements
        NSArray *arrayDict = [arg valueForKey:@"achivements"];
        achArr = [[NSMutableArray alloc]init];
        UserLevelProgressModal *subModal;
        for(NSDictionary *dict1 in arrayDict)
        {
            subModal = [[UserLevelProgressModal alloc]init];
            
            NSString *achivements_name = [dict1 valueForKey:@"achivements_name"];
            if(achivements_name != (id)[NSNull null])
                [subModal setAchievementsName:achivements_name];
            else
                [subModal setAchievementsName:@""];
            
            
            
            NSString *achivements_url = [dict1 valueForKey:@"achivements_url"];
            
            if(achivements_url != (id)[NSNull null])
            {
                achivements_url = [achivements_url stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
                [subModal setAchievementsURL:achivements_url];
            }
            else
                [subModal setAchievementsURL:@""];
            [achArr addObject:subModal];
        }
        [modal setArrayAchievements:achArr];
        
    }
    return modal;
}

#pragma mark --- Set Emonji's ISL

- (void)setEmonjiToServer:(ResturantModal *)userdetails  withCompletion:(void(^)(id obj1))completionBlock
{
    NSDictionary *dict = @{@"user_id":userdetails.userID,@"place_id":userdetails.placeID,@"emoji_id":userdetails.emonjiID};
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    
    [ServiceHelper sendRequestToServerWithParameters:@"AddEmoji.json" forhttpMethod:@"POST" withrequestBody:jsonString sync:YES completion:^(id obj) {
        if ([obj isKindOfClass:[NSDictionary class]]) {
            ResturantModal *modal = [[ResturantModal alloc]init];
            [modal setResult:@"success"];
            [modal setMessage:@"successfully checkin"];
            
            
            if (completionBlock) {
                completionBlock(modal);
            }
        }
        else
        {
            completionBlock(obj);
        }
    }];
}


#pragma mark --- Get Emonji's  Time ISL

- (void)getEmonjiTimeToServer:(ResturantModal *)userdetails  withCompletion:(void(^)(id obj1))completionBlock
{
    NSDictionary *dict = @{@"user_id":userdetails.userID,@"place_id":userdetails.placeID,@"emoji_id":userdetails.emonjiID};
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    
    [ServiceHelper sendRequestToServerWithParameters:@"Emojitime.json" forhttpMethod:@"POST" withrequestBody:jsonString sync:YES completion:^(id obj) {
        if ([obj isKindOfClass:[NSDictionary class]]) {
            ResturantModal *modal = [[ResturantModal alloc]init];
            [modal setResult:[obj valueForKey:@"result"]];
            [modal setMessage:[obj valueForKey:@"message"]];
            
            
            NSString *emoji_time = [obj valueForKey:@"emoji_time"];
            if(emoji_time != (id)[NSNull null])
                [modal setEmonjiTime:emoji_time];
            else
                [modal setEmonjiTime:@""];
            
            
            if (completionBlock) {
                completionBlock(modal);
            }
        }
        else
        {
            completionBlock(obj);
        }
    }];
}

#pragma mark -- Happy Hours and Daily Specials ISL

- (void)getSpecialsHappyHoursToServer_Special:(ResturantModal *)userdetails  withCompletion:(void(^)(id obj1))completionBlock
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [defaults objectForKey:@"logindetails"];
    RegisterModal * registerModalResponeValue = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    NSString * user_IDRegister = registerModalResponeValue.userID;
    //NSDictionary *dict = @{@"user_id":user_IDRegister,@"place_id":userdetails.placeID};
    NSDictionary *dict = @{@"user_id":user_IDRegister};
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    //retrieveSpecialsHappyHours.json
    //https://spotlytespecials.com/beta/webservice/api/mobile1/userFavoritesList.json
    [ServiceHelper sendRequestToServerWithParameters:@"retrieveUserSpecialsHappyHours.json" forhttpMethod:@"POST" withrequestBody:jsonString sync:YES completion:^(id obj) {
        if ([obj isKindOfClass:[NSDictionary class]]) {
            ResturantModal *modal = [[ResturantModal alloc]init];
            [modal setResult:[obj valueForKey:@"result"]];
            [modal setMessage:[obj valueForKey:@"message"]];
            
            id data = [obj valueForKey:@"data"];
            NSArray *specialArray = [data valueForKey:@"place_specials_list"];
            [modal setArraySpecials:[self setSpecial_List:specialArray]];
            
            NSArray *shappyHoursArray = [data valueForKey:@"happy_hours_list"];
            [modal setArrayHappyHours:[self setSpecial_List:shappyHoursArray]];
            
            if (completionBlock)
            {
                completionBlock(modal);
            }
        }
        else
        {
            completionBlock(obj);
        }
    }];
}

- (void)getSpecialsHappyHoursToServer:(ResturantModal *)userdetails  withCompletion:(void(^)(id obj1))completionBlock
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [defaults objectForKey:@"logindetails"];
    RegisterModal * registerModalResponeValue = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    NSString * user_IDRegister = registerModalResponeValue.userID;
    NSDictionary *dict = @{@"user_id":user_IDRegister,@"place_id":userdetails.placeID};
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    //retrieveSpecialsHappyHours.json
    //https://spotlytespecials.com/beta/webservice/api/mobile1/userFavoritesList.json
    [ServiceHelper sendRequestToServerWithParameters:@"retrieveSpecialsHappyHours.json" forhttpMethod:@"POST" withrequestBody:jsonString sync:YES completion:^(id obj) {
        if ([obj isKindOfClass:[NSDictionary class]]) {
            ResturantModal *modal = [[ResturantModal alloc]init];
            [modal setResult:[obj valueForKey:@"result"]];
            [modal setMessage:[obj valueForKey:@"message"]];
            //daily_special
            id data = [obj valueForKey:@"data"];
            NSArray *specialArray = [data valueForKey:@"place_specials"];
            [modal setArraySpecials:[self setListAllSpecialsHappyHoursValues:specialArray]];
            
            NSArray *shappyHoursArray = [data valueForKey:@"happy_hours"];
            [modal setArrayHappyHours:[self setListAllSpecialsHappyHoursValues:shappyHoursArray]];
            
            if (completionBlock)
            {
                completionBlock(modal);
            }
        }
        else
        {
            completionBlock(obj);
        }
    }];
}
- (void)GetHighlightsOfPlace:(ResturantModal *)userdetails  withCompletion:(void(^)(id obj1))completionBlock;
{
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    NSData *data = [defaults objectForKey:@"logindetails"];
//    RegisterModal * registerModalResponeValue = [NSKeyedUnarchiver unarchiveObjectWithData:data];
//    NSString * user_IDRegister = registerModalResponeValue.userID;
    NSDictionary *dict = @{@"place_id":userdetails.placeID};
  //  NSDictionary *dict = @{@"place_id":@"5569"};
   // NSDictionary *dict = @{@"place_id":@"1"};
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    //retrieveSpecialsHappyHours.json
    //https://spotlytespecials.com/beta/webservice/api/mobile1/userFavoritesList.json
    [ServiceHelper sendRequestToServerWithParameters:@"placeHighlightesList.json" forhttpMethod:@"POST" withrequestBody:jsonString sync:YES completion:^(id obj) {
        if ([obj isKindOfClass:[NSDictionary class]]) {
            ResturantModal *modal = [[ResturantModal alloc]init];
            [modal setResult:[obj valueForKey:@"result"]];
            [modal setMessage:[obj valueForKey:@"message"]];
            //daily_special
            id data = [obj valueForKey:@"data"];
            
            NSArray *ArrHighlights = [obj valueForKey:@"data"];
            if (ArrHighlights.count > 1)
            {
                [modal setArrHighlight:[self setHighLightData:ArrHighlights]];
            }
            
           //NSArray *specialArray = [data valueForKey:@"place_specials"];
            //[modal setArraySpecials:[self setListAllSpecialsHappyHoursValues:specialArray]];
            
           // NSArray *shappyHoursArray = [data valueForKey:@"happy_hours"];
            //[modal setArrayHappyHours:[self setListAllSpecialsHappyHoursValues:shappyHoursArray]];
            
            if (completionBlock)
            {
                completionBlock(modal);
            }
        }
        else
        {
            completionBlock(obj);
        }
    }];

}
-(NSArray *)setListAllSpecialsHappyHoursValues :(NSArray *)dictArray
{
    ResturantModal *modal1;
    NSMutableArray *modalArray = [[NSMutableArray alloc]init];
    NSMutableArray *modalDaysArray = [[NSMutableArray alloc]init];
    for(NSDictionary *dictSpecial in dictArray)
    {
        if(dictSpecial != nil)
        {
            NSString *keyName   = [dictSpecial valueForKey:@"day_name"];
            NSArray *arrDailySpecials = [dictSpecial objectForKey:keyName];
            
            modal1= [[ResturantModal alloc]init];
            
            if(arrDailySpecials.count>0)
            {
                for(NSDictionary *detailDict in arrDailySpecials)
                {
                    ResturantModal *subModal= [[ResturantModal alloc]init];
                    NSString *place_idStr = [detailDict valueForKey:@"ID"];
                    if(place_idStr != (id)[NSNull null])
                        [subModal setSpecials_ID:place_idStr];
                    else
                        [subModal setSpecials_ID:@""];
                    
                    
                    
                    NSString *priceStr = [detailDict valueForKey:@"price"];
                    if(priceStr != (id)[NSNull null])
                        [subModal setPrice:priceStr];
                    else
                        [subModal setPrice:@""];
                    
                    NSString *titleStr = [detailDict valueForKey:@"title"];
                    if(titleStr != (id)[NSNull null])
                        [subModal setTitle:titleStr];
                    else
                        [subModal setTitle:@""];
                    
                    
                    NSString *start_timeStr = [detailDict valueForKey:@"start_time"];
                    if(start_timeStr != (id)[NSNull null])
                        [subModal setStartTime:start_timeStr];
                    else
                        [subModal setStartTime:@""];
                    
                    
                    NSString *end_timeStr = [detailDict valueForKey:@"end_time"];
                    if(end_timeStr != (id)[NSNull null])
                        [subModal setEndTime:end_timeStr];
                    else
                        [subModal setEndTime:@""];
                    
                    NSString *weekday = [detailDict valueForKey:@"weekday"];
                    if(weekday != (id)[NSNull null])
                        [subModal setSpecials_weekDay:weekday];
                    else
                        [subModal setSpecials_weekDay:@""];
                    
                    NSString *website = [detailDict valueForKey:@"website"];
                    if(website != (id)[NSNull null])
                        [subModal setWebSite_Specials:website];
                    else
                        [subModal setWebSite_Specials:@""];
                    
                    NSString *startTime = [detailDict valueForKey:@"start_time"];
                    if(startTime != (id)[NSNull null])
                        [subModal setStartTime:startTime];
                    else
                        [subModal setStartTime:@""];
                    
                    
                    NSString *EndTime = [detailDict valueForKey:@"end_time"];
                    if(EndTime != (id)[NSNull null])
                        [subModal setEndTime:EndTime];
                    else
                        [subModal setEndTime:@""];

                    
                    BOOL is_favoriteBool = [[detailDict valueForKey:@"if_favorite"]boolValue];
                    [subModal setIsCheckedFavSpecial:is_favoriteBool];
                    
                    [modalDaysArray addObject:subModal];
                    
                }
                [modal1 setArraySpecials:modalDaysArray];
                if(keyName != (id)[NSNull null])
                    [modal1 setSpecials_weekDay:keyName];
                else
                    [modal1 setSpecials_weekDay:@""];
                [modalArray addObject:modal1];
            }
        }
    }
    
    return modalDaysArray;

}
-(NSArray *)setHighLightData : (NSArray *)dictArray
{
    ResturantModal *modal_Highlight;
    modal_Highlight = [[ResturantModal alloc]init];
    NSMutableArray *modalArr = [[NSMutableArray alloc]init];
    if (dictArray.count > 0)
    {
        for (NSDictionary *highlightDict in dictArray) {
            ResturantModal *submodal = [[ResturantModal alloc]init];
            NSString *Main_id = [highlightDict valueForKey:@"id"];
            if(Main_id != (id)[NSNull null])
                [submodal setMain_id:Main_id];
            else
                [submodal setMain_id:@""];
            
            NSString *Highlight_id = [highlightDict valueForKey:@"highlight_id"];
            if(Highlight_id != (id)[NSNull null])
                [submodal setHighlightID:Highlight_id];
            else
                [submodal setHighlightID:@""];
            
            NSString *place_id = [highlightDict valueForKey:@"place_id"];
            if(place_id != (id)[NSNull null])
                [submodal setPlaceID:place_id];
            else
                [submodal setPlaceID:@""];

            
            NSString *status = [highlightDict valueForKey:@"status"];
            if(status != (id)[NSNull null])
                [submodal setHighlight_Status:status];
            else
                [submodal setHighlight_Status:@""];

            
            NSString *name = [highlightDict valueForKey:@"name"];
            if(name != (id)[NSNull null])
                [submodal setHighlightName:name];
            else
                [submodal setHighlightName:@""];
            
            [modalArr addObject:submodal];
        }
        [modal_Highlight setArrHighlight:modalArr];
    }
    return modalArr;
}

-(NSArray *)setSpecial_List :(NSArray *)dictArray
{
    ResturantModal *modal1;
    NSMutableArray *modalArray = [[NSMutableArray alloc]init];
    NSMutableArray *modalDaysArray = [[NSMutableArray alloc]init];
    NSMutableArray *mainArr = [[NSMutableArray alloc]init];
            modal1= [[ResturantModal alloc]init];
            
            if(dictArray.count>0)
            {
                for(NSDictionary *detailDict in dictArray)
                {
                    ResturantModal *subModal= [[ResturantModal alloc]init];

                    NSString *PlaceID = [detailDict valueForKey:@"place_id"];
                    if(PlaceID != (id)[NSNull null])
                        [subModal setStartTime:PlaceID];
                    else
                        [subModal setStartTime:@""];
                    
                    NSString *Place_name = [detailDict valueForKey:@"place_name"];
                    if(Place_name != (id)[NSNull null])
                        [subModal setPlaceName:Place_name];
                    else
                        [subModal setStartTime:@""];
                    
                    NSString *Place_image = [detailDict valueForKey:@"place_image"];
                    if(Place_image != (id)[NSNull null])
                        [subModal setPlaceName:Place_image];
                    else
                        [subModal setPlaceName:@""];
                    [modalArray addObject:subModal];
                    [mainArr addObject:modalArray];
                    NSArray *arrHappyHours = [detailDict valueForKey:@"happy_hours"];
                    
                    if(arrHappyHours.count>0)
                    {
                        for(NSDictionary *dictHappyHours in arrHappyHours)
                        {
                            NSString *place_idStr = [dictHappyHours valueForKey:@"place_id"];
                            if(place_idStr != (id)[NSNull null])
                                [subModal setPlaceID:place_idStr];
                            else
                                [subModal setPlaceID:@""];
                            
                            NSString *Special_id = [dictHappyHours valueForKey:@"id"];
                            if(Special_id != (id)[NSNull null])
                                [subModal setSpecials_ID:Special_id];
                            else
                                [subModal setSpecials_ID:@""];
                            
                            NSString *priceStr = [dictHappyHours valueForKey:@"price"];
                            if(priceStr != (id)[NSNull null])
                                [subModal setPrice:priceStr];
                            else
                                [subModal setPrice:@""];
                            
                            NSString *titleStr = [dictHappyHours valueForKey:@"title"];
                            if(titleStr != (id)[NSNull null])
                                [subModal setTitle:titleStr];
                            else
                                [subModal setTitle:@""];
                            
                            
                            NSString *start_timeStr = [dictHappyHours valueForKey:@"start_time"];
                            if(start_timeStr != (id)[NSNull null])
                                [subModal setStartTime:start_timeStr];
                            else
                                [subModal setStartTime:@""];
                            
                            
                            NSString *end_timeStr = [dictHappyHours valueForKey:@"end_time"];
                            if(end_timeStr != (id)[NSNull null])
                                [subModal setEndTime:end_timeStr];
                            else
                                [subModal setEndTime:@""];
                            
                            NSString *weekday = [dictHappyHours valueForKey:@"weekday"];
                            if(weekday != (id)[NSNull null])
                                [subModal setSpecials_weekDay:weekday];
                            else
                                [subModal setSpecials_weekDay:@""];
                            
                            NSString *website = [dictHappyHours valueForKey:@"website"];
                            if(website != (id)[NSNull null])
                                [subModal setWebSite_Specials:website];
                            else
                                [subModal setWebSite_Specials:@""];
                            NSString *Place_Name = [dictHappyHours valueForKey:@"place_name"];
                            if(Place_Name != (id)[NSNull null])
                                [subModal setPlaceName:Place_Name];
                            else
                                [subModal setWebSite_Specials:@"--"];
                            [modalDaysArray addObject:subModal];
                        }
                        [mainArr addObject:modalDaysArray];
                    }
                }
                
                [modal1 setArraySpecials:mainArr];
            }
            
     return modalDaysArray;

}



@end
