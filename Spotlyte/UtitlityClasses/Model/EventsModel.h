//
//  EventsModel.h
//  Spotlyte
//
//  Created by CIPL137-MOBILITY on 18/04/16.
//  Copyright © 2016 Naveen. All rights reserved.
//

#import <Foundation/Foundation.h>
#define EventsModalmacro [EventsModel sharedUserProfile]

@interface EventsModel : NSObject
{
    
}

@property (nonatomic, strong) NSString *eventName;
@property (nonatomic, strong) NSString *eventTime;
@property (nonatomic, strong) NSString *eventToTime;

@property (nonatomic, strong) UIImage *eventImage;
@property (nonatomic, strong) NSString *venue;
@property (nonatomic, strong) NSString *typeOFPlaces;

@property (nonatomic, strong) NSString *eventMessage;
@property (nonatomic, strong) NSString *eventResult;
@property (nonatomic, strong) NSString *categoryID;
@property (nonatomic, strong) NSString *categoryName;
@property (nonatomic, strong) NSString *categoryImage;
@property (nonatomic, strong) NSString *eventImageStr;
@property (nonatomic, strong) NSString *eventStartDate;
@property (nonatomic, strong) NSString *eventEndDate;
@property (nonatomic, strong) NSString *eventCreatedDate;
@property (nonatomic, strong) NSString *eventPlace;
@property (nonatomic, strong) NSString *eventDescription;
@property (nonatomic, strong) NSString *eventCity;
@property (nonatomic, strong) NSString *eventState;
@property (nonatomic, strong) NSString *eventCountry;
@property (nonatomic, strong) NSString *eventAddress;
@property (nonatomic, strong) NSString *phoneNo;
@property (nonatomic, strong) NSString *searchByCity;
@property (nonatomic, strong) NSString *EventWebsite;

@property (nonatomic, strong) NSArray *specialsArray;
@property (nonatomic, strong) NSArray *dayArray;

@property (strong, nonatomic) NSString *specials_weekDay;


@property (nonatomic, strong) NSString *userID;
@property (nonatomic, strong) NSString *sundayStr;
@property (nonatomic, strong) NSString *mondayStr;
@property (nonatomic, strong) NSString *tuesdayStr;
@property (nonatomic, strong) NSString *wednesdayStr;
@property (nonatomic, strong) NSString *thursdayStr;
@property (nonatomic, strong) NSString *fridayStr;
@property (nonatomic, strong) NSString *saturdayStr;

@property (nonatomic, strong) NSArray *placesArray;
@property (nonatomic, strong) NSArray *eventArray;






- (void)eventsListToServer:(EventsModel *)userdetails  withCompletion:(void(^)(id obj1))completionBlock;
- (void)cityListToServer:(EventsModel *)userdetails  withCompletion:(void(^)(id obj1))completionBlock;
+ (EventsModel *)sharedUserProfile;




@end
