//
//  PromtionsModal.h
//  Spotlyte
//
//  Created by Admin on 12/04/16.
//  Copyright © 2016 Naveen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "ServiceHelper.h"
#import "ResturantModal.h"

#define PromotionMacro [PromtionsModal sharedUserProfile]

@interface PromtionsModal : NSObject

@property (nonatomic, retain) NSArray *placesArray;
@property (nonatomic, retain) NSArray *promotionArray;
@property (nonatomic,retain) NSString *Promotion_Type;

@property (nonatomic, retain) NSString *promotionID;
@property (nonatomic, retain) NSString *promotionTitle;
@property (nonatomic, retain) NSString *promotionDesc;
@property (nonatomic, retain) NSString *promotionImage;
@property (nonatomic, retain) NSString *promotionCreatedDate;
@property (nonatomic, retain) NSString *promotionStartDate;
@property (nonatomic, retain) NSString *promotionExpiredDate;
@property (nonatomic, retain) NSString *placeID;
@property (nonatomic, retain) NSString *promotionFavStatus;
@property (nonatomic, retain) NSString *user_ID;
@property (nonatomic, retain) NSString *promotionLatitude;
@property (nonatomic, retain) NSString *promotionLongtitude;
@property (nonatomic, retain) NSString *promotionPlaceType;
@property (nonatomic, retain) NSString *promotionDistance;

@property (nonatomic, retain) NSString *promotionPlaceEmail;
@property (nonatomic, retain) NSString *promotionPlaceCity;
@property (nonatomic, retain) NSString *promotionPlaceState;
@property (nonatomic, retain) NSString *promotionPlaceAddress1;
@property (nonatomic, retain) NSString *promotionPlaceAddress2;
@property (nonatomic, retain) NSString *promotionPlaceZip;
@property (nonatomic, retain) NSString *promotionPlaceMobileNo;
@property (nonatomic, retain) NSString *promotionPlacePhoneNo;
@property (nonatomic, retain) NSString *promotionPlacecountry;
@property (nonatomic, retain) NSString *promotionPlaceOpenFrom;
@property (nonatomic, retain) NSString *promotionPlaceOpenTo;
@property (nonatomic, retain) NSString *promotionPlaceTitle;
@property (nonatomic, retain) NSString *promotionPlaceImage;
@property (nonatomic, retain) NSString *promotionPlaceRating;
@property (nonatomic, retain) NSString *Special_count;
@property (strong, nonatomic) NSString *message;
@property (strong, nonatomic) NSString *result;




+ (PromtionsModal *)sharedUserProfile;
- (void)hotelPromotionsListValuesToServer:(ResturantModal *)userdetails  withCompletion:(void(^)(id obj1))completionBlock;
- (void)addFavouriteSpotlyteToServer:(PromtionsModal *)userdetails  withCompletion:(void(^)(id obj1))completionBlock;
- (void)unFavouriteSpotlyteToServer:(PromtionsModal *)userdetails  withCompletion:(void(^)(id obj1))completionBlock;
@end
