//
//  UITabBarCustom.m
//  iOSCodeStructure
//
//  Created by Nishant
//  Copyright (c) 2013 Nishant. All rights reserved.
//

#import "UITabBarCustom.h"

@interface UITabBarCustom ()
{
    UINavigationController * navController;
}
@end

@implementation UITabBarCustom
@synthesize btnTab1, btnTab2, btnTab3, btnTab4,btnTab5,btnTab6;
@synthesize imgTabBg;
#pragma mark - This is the main Tabbar in the Application
#pragma mark - View Life Cycle
-(void)viewDidLoad
{
    [super viewDidLoad];
    HeightTabbar = 50;
    noOfTab = 6;
    [self addCustomElements];
    
    CALayer *topBorder = [CALayer layer];
    topBorder.frame = CGRectMake(0.0f, 0.0f, self.view.frame.size.width, 2.0f);
    topBorder.backgroundColor = [[UIColor colorWithRed:0.0f/255.0f green:204.0f/255.0f blue:255.0f/255.0f alpha:1] CGColor];
    [self.tabBar.layer addSublayer:topBorder];
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self hideOriginalTabBar];
}
#pragma mark - Hide Original TabBar - Add Custom TabBar
- (void)hideOriginalTabBar
{
    for(UIView *view in self.view.subviews)
    {
        if([view isKindOfClass:[UITabBar class]])
        {
            view.hidden = YES;
            break;
        }
    }
}
-(void)addCustomElements
{
    //Add Bg Image
    if(imgTabBg != nil)
    {
        [imgTabBg removeFromSuperview];
    }
    float y = [UIScreen mainScreen].bounds.size.height-HeightTabbar;
    float w = [UIScreen mainScreen].bounds.size.width;
    bottom_view=[[UIView alloc] initWithFrame:CGRectMake(0,y,w,HeightTabbar)];
    imgTabBg = [[UIImageView alloc]initWithFrame:CGRectMake(0,0,w,HeightTabbar)];
    imgTabBg.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:bottom_view];
    [bottom_view addSubview:imgTabBg];
    y_position=0;
    btn_y_position=13;
    btn_x_position=0;
    btn_width=w/noOfTab;
    btn_height=HeightTabbar-13;
    [self addAllElements];
    
    UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, bottom_view.frame.size.width, 2)];
    lblTitle.backgroundColor = [UIColor colorWithRed:0.0/255.0 green:158.0/255.0 blue:224.0/255.0 alpha:1.0];
    [bottom_view addSubview:lblTitle];
    
}
-(void)addAllElements
{
    //Add Tab Buttons
    if(btnTab1 != nil)
        [btnTab1 removeFromSuperview];
    if (btnTab2 != nil)
        [btnTab2 removeFromSuperview];
    if(btnTab3 != nil)
        [btnTab3 removeFromSuperview];
    if (btnTab4 != nil)
        [btnTab4 removeFromSuperview];
    if (btnTab5 != nil)
        [btnTab5 removeFromSuperview];
    if (btnTab6 != nil)
        [btnTab6 removeFromSuperview];
    
    btnTab1 = [self getGeneralTabButton:0 isSelected:true];
    btnTab2 = [self getGeneralTabButton:1 isSelected:false];
    btnTab3 = [self getGeneralTabButton:2 isSelected:false];
    btnTab4 = [self getGeneralTabButton:3 isSelected:false];
    btnTab5 = [self getGeneralTabButton:4 isSelected:false];
    btnTab6 = [self getGeneralTabButton:5 isSelected:false];
    
    [bottom_view addSubview:btnTab1];
    [bottom_view addSubview:btnTab2];
    [bottom_view addSubview:btnTab3];
    [bottom_view addSubview:btnTab4];
    [bottom_view addSubview:btnTab5];
    [bottom_view addSubview:btnTab6];
    
    // Setup event handlers so that the buttonClicked method will respond to the touch up inside event.
    [btnTab1 addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchDown];
    [btnTab2 addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchDown];
    [btnTab3 addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchDown];
    [btnTab4 addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchDown];
    [btnTab5 addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchDown];
    [btnTab6 addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchDown];
}

-(UIButton *)getGeneralTabButton:(int)pintTag isSelected:(BOOL)pbolIsSelected
{
    UIImage *btnImage;
    UIImage *btnImageSelected;
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setTitleColor:[UIColor colorWithRed:0.0/255.0 green:158.0/255.0 blue:224.0/255.0 alpha:1.0] forState:UIControlStateSelected];
    [btn setTitleColor:[UIColor colorWithRed:46.0/255.0 green:46.0/255.0 blue:46.0/255.0 alpha:1.0] forState:UIControlStateNormal];
    if (IS_IPHONE_5) {
        //[btn.titleLabel setFont:[UIFont efontWithName:@"Roboto-Bold" size:4.0]];
        [btn.titleLabel setFont:[UIFont systemFontOfSize:8.0]];
    }
    else {
        //[btn.titleLabel setFont:[UIFont fontWithName:@"Roboto-Bold" size:4.0]];
        [btn.titleLabel setFont:[UIFont systemFontOfSize:9.0]];
    }
    [btn setTag:pintTag];
    [btn setSelected:pbolIsSelected];
    switch (pintTag) {
        case 0:	//Tab-1
            btnImage = [UIImage imageNamed:@"menu"];
            btnImageSelected = [UIImage imageNamed:@"menuSelected"];
            btn.frame = CGRectMake(btn_x_position,btn_y_position, btn_width, btn_height);
            [btn setTitle:@"Menu" forState:UIControlStateNormal];
            break;
        case 1:	//Tab-2
            btnImage = [UIImage imageNamed:@"Map"];
            btnImageSelected = [UIImage imageNamed:@"MapSelected"];
            btn.frame = CGRectMake(btn_x_position+btn_width, btn_y_position, btn_width, btn_height);
            [btn setTitle:@"Map" forState:UIControlStateNormal];
            break;
        case 2:	//Tab-3
            btnImage = [UIImage imageNamed:@"Mug"];
            btnImageSelected = [UIImage imageNamed:@"MugSelected"];
            btn.frame = CGRectMake(btn_x_position+(btn_width*2), btn_y_position, btn_width, btn_height);
            [btn setTitle:@"Top Spots" forState:UIControlStateNormal];
            break;
        case 3:	//Tab-4
            btnImage = [UIImage imageNamed:@"Tag"];
            btnImageSelected = [UIImage imageNamed:@"TagSelected"];
            btn.frame = CGRectMake(btn_x_position+(btn_width*3), btn_y_position, btn_width, btn_height);
            [btn setTitle:@"Promotions" forState:UIControlStateNormal];

            break;
        case 4:	//Tab-4
            btnImage = [UIImage imageNamed:@"Dashboard_menu"];
            btnImageSelected = [UIImage imageNamed:@"Dashboard_menuSelected"];
            btn.frame = CGRectMake(btn_x_position+(btn_width*4), btn_y_position, btn_width, btn_height);
            [btn setTitle:@"Dashboard" forState:UIControlStateNormal];
            
            break;
        case 5:	//Tab-4
            btnImage = [UIImage imageNamed:@"Profile"];
            btnImageSelected = [UIImage imageNamed:@"ProfileSelected"];
            btn.frame = CGRectMake(btn_x_position+(btn_width*5), btn_y_position, btn_width, btn_height);
            [btn setTitle:@"Profile" forState:UIControlStateNormal];
            
            break;
        default://Tab-1
            btnImage = [UIImage imageNamed:@"menu"];
            btnImageSelected = [UIImage imageNamed:@"menuSelected"];
            btn.frame = CGRectMake(btn_x_position+(btn_width*5), btn_y_position, btn_width, btn_height);
            break;
    }
    
    [btn setImage:btnImage forState:UIControlStateNormal];
    [btn setImage:btnImageSelected forState:UIControlStateSelected];
//    btn.imageEdgeInsets = UIEdgeInsetsMake(-btn.imageView.frame.size.height,btn.frame.size.width/2-(btn.imageView.frame.size.width/2),0,0);
    //btn.titleEdgeInsets = UIEdgeInsetsMake(-30,-20,-btn.titleLabel.frame.size.height*2,-15);
    btn.titleLabel.textAlignment=NSTextAlignmentCenter;
    
     // the space between the image and text
     CGFloat spacing = 6.0;
     
     // lower the text and push it left so it appears centered
     //  below the image
     CGSize imageSize = btn.imageView.image.size;
     btn.titleEdgeInsets = UIEdgeInsetsMake(0.0, - imageSize.width, - (imageSize.height + spacing) + 4, 0.0);
     
     // raise the image and push it right so it appears centered
     //  above the text
     CGSize titleSize = [btn.titleLabel.text sizeWithAttributes:@{NSFontAttributeName: btn.titleLabel.font}];
     btn.imageEdgeInsets = UIEdgeInsetsMake(- (titleSize.height + spacing), 0.0, 0.0, - titleSize.width);
     
     // increase the content height to avoid clipping
     CGFloat edgeOffset = fabsf(titleSize.height - imageSize.height) / 2.0;
     btn.contentEdgeInsets = UIEdgeInsetsMake(edgeOffset, 0.0, edgeOffset, 0.0);
    
    return btn;
}

-(UILabel *)getGeneralTabLabel:(int)pintTag isSelected:(BOOL)pbolIsSelected
{
    UILabel *lblTitle = [[UILabel alloc] init];
    
    switch (pintTag) {
        case 0:	//Tab-1
            lblTitle.frame = CGRectMake(btn_x_position,btn_y_position, btn_width, btn_height);
            lblTitle.text = @"Menu";
            break;
        case 1:	//Tab-2
            lblTitle.frame = CGRectMake(btn_x_position+btn_width, btn_y_position, btn_width, btn_height);
            lblTitle.text = @"Map";
            break;
        case 2:	//Tab-3
            lblTitle.frame = CGRectMake(btn_x_position+(btn_width*2), btn_y_position, btn_width, btn_height);
            lblTitle.text = @"Top Spots";
            break;
        case 3:	//Tab-4
            lblTitle.frame = CGRectMake(btn_x_position+(btn_width*3), btn_y_position, btn_width, btn_height);
            lblTitle.text = @"Promotions";
            break;
        case 4:	//Tab-4
            lblTitle.frame = CGRectMake(btn_x_position+(btn_width*4), btn_y_position, btn_width, btn_height);
            lblTitle.text = @"Dashboard";
            
            break;
        case 5:	//Tab-4
            lblTitle.frame = CGRectMake(btn_x_position+(btn_width*5), btn_y_position, btn_width, btn_height);
            lblTitle.text = @"Profile";
            
            break;
    }
    lblTitle.textAlignment = NSTextAlignmentCenter;
    return lblTitle;
}

#pragma mark - Select Tab
//-(void)setSelectedIndex:(NSUInteger)selectedIndex{
//
//}

#pragma mark - Select Tab
- (void)buttonClicked:(id)sender
{
    int tagNum =(int)[sender tag];
    if (tagNum >= 1){
        //appDelegate.indexpathSliderSelected = [NSIndexPath indexPathForRow:tagNum+1 inSection:0];
    }else{
        //appDelegate.indexpathSliderSelected = [NSIndexPath indexPathForRow:tagNum inSection:0];
    }
    
    [self selectTab:tagNum];
    
    if (tagNum==0) {
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"MenuButtonEnable"
         object:self userInfo:nil];
        [self.revealViewController revealToggle:nil];
    }
}

- (void)selectTab:(int)tabID
{
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"tabSelected"];
    switch(tabID)
    {
        case 0:
            [btnTab1 setSelected:true];
            [btnTab2 setSelected:false];
            [btnTab3 setSelected:false];
            [btnTab4 setSelected:false];
            [btnTab5 setSelected:false];
            [btnTab6 setSelected:false];
            appDelegate.IsFromNewHome = NO;
            break;
        case 1:
            [btnTab1 setSelected:false];
            [btnTab2 setSelected:true];
            [btnTab3 setSelected:false];
            [btnTab4 setSelected:false];
            [btnTab5 setSelected:false];
            [btnTab6 setSelected:false];
            break;
        case 2:
            [btnTab1 setSelected:false];
            [btnTab2 setSelected:false];
            [btnTab3 setSelected:true];
            [btnTab4 setSelected:false];
            [btnTab5 setSelected:false];
            [btnTab6 setSelected:false];
            appDelegate.IsFromNewHome = NO;
            break;
        case 3:
            [btnTab1 setSelected:false];
            [btnTab2 setSelected:false];
            [btnTab3 setSelected:false];
            [btnTab4 setSelected:true];
            [btnTab5 setSelected:false];
            [btnTab6 setSelected:false];
            appDelegate.IsFromNewHome = NO;
            break;
        case 4:
            [btnTab1 setSelected:false];
            [btnTab2 setSelected:false];
            [btnTab3 setSelected:false];
            [btnTab4 setSelected:false];
            [btnTab5 setSelected:true];
            [btnTab6 setSelected:false];
            appDelegate.IsFromNewHome = NO;
            break;
        case 5:
            [btnTab1 setSelected:false];
            [btnTab2 setSelected:false];
            [btnTab3 setSelected:false];
            [btnTab4 setSelected:false];
            [btnTab5 setSelected:false];
            [btnTab6 setSelected:true];
            appDelegate.IsFromNewHome = NO;
            break;
    }
    self.selectedIndex = tabID;
    if (self.selectedIndex == tabID)
    {
        navController = (UINavigationController *)[self selectedViewController];
        [navController popToRootViewControllerAnimated:YES];
    }else{
        self.selectedIndex = tabID;
    }
}


- (void)showTabSelect:(int)tabID
{
    switch(tabID)
    {
        case 0:
            [btnTab1 setSelected:true];
            [btnTab2 setSelected:false];
            [btnTab3 setSelected:false];
            [btnTab4 setSelected:false];
            [btnTab5 setSelected:false];
            [btnTab6 setSelected:false];
            break;
        case 1:
            [btnTab1 setSelected:false];
            [btnTab2 setSelected:true];
            [btnTab3 setSelected:false];
            [btnTab4 setSelected:false];
            [btnTab5 setSelected:false];
            [btnTab6 setSelected:false];
            break;
        case 2:
            [btnTab1 setSelected:false];
            [btnTab2 setSelected:false];
            [btnTab3 setSelected:true];
            [btnTab4 setSelected:false];
            [btnTab5 setSelected:false];
            [btnTab6 setSelected:false];
            break;
        case 3:
            [btnTab1 setSelected:false];
            [btnTab2 setSelected:false];
            [btnTab3 setSelected:false];
            [btnTab4 setSelected:true];
            [btnTab5 setSelected:false];
            [btnTab6 setSelected:false];
            break;
        case 4:
            [btnTab1 setSelected:false];
            [btnTab2 setSelected:false];
            [btnTab3 setSelected:false];
            [btnTab4 setSelected:false];
            [btnTab5 setSelected:true];
            [btnTab6 setSelected:false];
            break;
        case 5:
            [btnTab1 setSelected:false];
            [btnTab2 setSelected:false];
            [btnTab3 setSelected:false];
            [btnTab4 setSelected:false];
            [btnTab5 setSelected:false];
            [btnTab6 setSelected:true];
            break;
    }
}

#pragma mark - Show/Hide TabBar
- (void)showTabBar
{
    [UIView transitionWithView:bottom_view
                      duration:0.2
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        bottom_view.hidden = NO;
                        self.imgTabBg.hidden = NO;
                        //bottom_view.hidden = NO;
                        self.btnTab1.hidden = NO;
                        self.btnTab2.hidden = NO;
                        self.btnTab3.hidden = NO;
                        self.btnTab4.hidden = NO;
                        self.btnTab5.hidden = NO;
                        self.btnTab6.hidden = NO;
                        self.btnTab1.userInteractionEnabled=YES;
                        self.btnTab2.userInteractionEnabled=YES;
                        self.btnTab3.userInteractionEnabled=YES;
                        self.btnTab4.userInteractionEnabled=YES;
                        self.btnTab5.userInteractionEnabled=YES;
                        self.btnTab6.userInteractionEnabled=YES;
                    }
                    completion:NULL];
}
- (void)hideTabBar
{
    [UIView transitionWithView:bottom_view
                      duration:0.4
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        bottom_view.hidden = YES;
                        self.imgTabBg.hidden = YES;
                        //bottom_view.hidden = YES;
                        self.btnTab1.hidden = YES;
                        self.btnTab2.hidden = YES;
                        self.btnTab3.hidden = YES;
                        self.btnTab4.hidden = YES;
                        self.btnTab5.hidden = YES;
                        self.btnTab6.hidden = YES;
                        self.btnTab1.userInteractionEnabled=NO;
                        self.btnTab2.userInteractionEnabled=NO;
                        self.btnTab3.userInteractionEnabled=NO;
                        self.btnTab4.userInteractionEnabled=NO;
                        self.btnTab5.userInteractionEnabled=NO;
                        self.btnTab6.userInteractionEnabled=NO;
                    }
                    completion:NULL];
}
#pragma mark -Load Nib
- (id)loadViewNib:(NSString *)nibName {
    NSArray *nibs = [[NSBundle mainBundle] loadNibNamed:nibName owner:nil options:nil];
    if([nibs count] > 0) {
        return [nibs objectAtIndex:0];
    }
    return nil;
}
#pragma mark- player delegate method
-(void)pushSettingView
{
    //    if (![navController.visibleViewController isKindOfClass:[SettingVC class]]) {
    //        navController = (UINavigationController *)[self selectedViewController];
    //        [navController popToRootViewControllerAnimated:NO];
    //        [navController pushViewController:viewObj animated:YES];
    //    }
}
-(void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    //    if ([keyPath isEqual:@"outputVolume"]) {
    //        NSLog(@"volume changed!");
    //        [viewTop setVolumeButtonImage];
    //    }
    
}
@end
