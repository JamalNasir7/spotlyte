//
//  SharedMethods.m
//  Spotlyte
//
//  Created by Admin on 28/03/16.
//  Copyright © 2016 Naveen. All rights reserved.
//

#import "SharedMethods.h"
#import <Foundation/Foundation.h>

@implementation SharedMethods

+ (void)showErrorAlertMessage:(NSString *)message
{
    UIAlertView *errorAlert = [[UIAlertView alloc]initWithTitle:@"Message" message:message delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    [errorAlert show];
    
}
+ (BOOL) validateEmail: (NSString *) candidate {
    
    BOOL isvalid = NO;
    
    if (candidate.length != 0)
    {
        NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
        NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
        isvalid =  [emailTest evaluateWithObject:candidate];
    }
    else
    {
        isvalid = YES;
    }
    return isvalid;
}

UIFont *Calibri(CGFloat size){
    return [UIFont fontWithName:@"Calibri" size:size];
}

UIFont *CalibriBold(CGFloat size){
    return [UIFont fontWithName:@"Calibri-Bold" size:size];
}



UILabel *createLabelWithFrame(CGRect frame,NSString *title, UIColor *textColor, UIColor *bgColor, UIFont *font, NSTextAlignment intAlignment, int intLine, CGFloat borderWidth, UIColor *borderColor){
    
    UILabel *label=[[UILabel alloc] initWithFrame:frame];
    //[label setAutoresizesSubviews:YES];
    
    if (intLine!=-1)
        label.numberOfLines=intLine;
    
    if (bgColor)
        [label setBackgroundColor:bgColor];
    
    if (textColor)
        [label setTextColor:textColor];
    
    if (font)
        [label setFont:font];
    
    if (title)
        [label setText:title];
    
    if (intAlignment!=(NSTextAlignment)(-1))
        [label setTextAlignment:intAlignment];
    
    if (borderColor!=nil) {
        label.layer.borderColor = borderColor.CGColor;
    }
    
    label.layer.borderWidth = borderWidth;
    
    return label;
}

UIImageView *createImageViewWithFrameAndString(CGRect frame, NSString *imgName){
    UIImageView *imgView =[[UIImageView alloc] initWithFrame:frame];
    [imgView setImage:[UIImage imageNamed:imgName]];
    return imgView;
}


UIButton *createButtonWithSize(CGRect frame, NSString *title, UIImage *imageName, int tag, UIColor *bgColor, UIColor *textColor, UIFont *textFont, SEL selector, id target ,CGFloat borderWidth, UIColor *borderColor, int cornerRedius){
    
    UIButton *button = [[UIButton alloc]initWithFrame:frame];
    [button setBackgroundColor:bgColor];
    [button setTitleColor:textColor forState:UIControlStateNormal];
    [button setTag:tag];
    [button setTitle:title forState:UIControlStateNormal];
    [button.titleLabel setFont:textFont];
    [button setBackgroundImage:imageName forState:UIControlStateNormal];
    [button addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
    button.layer.borderColor = borderColor.CGColor;
    button.layer.borderWidth = borderWidth;
    button.layer.cornerRadius = cornerRedius;
    button.clipsToBounds=YES;
    
    return button;
}

void showAlertController(NSString *title, NSString * messsage, UIViewController *vc,NSString *cancleButton,NSString *okButton,AlertHandler okHandler) {
    dispatch_async(dispatch_get_main_queue(), ^{
        
        UIAlertController * alert=   [UIAlertController alertControllerWithTitle:title
                                                                         message:messsage preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* cancel = [UIAlertAction actionWithTitle:cancleButton style:UIAlertActionStyleCancel handler:^(UIAlertAction * action){
        
            okHandler(YES,NO);
        }];
        
        [alert addAction:cancel];
        
        
        if (okButton) {
            UIAlertAction* ok = [UIAlertAction actionWithTitle:okButton style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
                okHandler(NO,YES);
            }];
            [alert addAction:ok];
            ok=nil;
        }
        
        [vc presentViewController:alert animated:YES completion:nil];
        alert=nil;
        
    });
}

void showAlert(NSString *title, NSString * messsage, UIViewController *vc,NSString *cancleButton,NSString *okButton){

    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:title message:messsage delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    
    [alert show];
}


@end
