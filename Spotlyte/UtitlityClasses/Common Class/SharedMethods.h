//
//  SharedMethods.h
//  Spotlyte
//
//  Created by Admin on 28/03/16.
//  Copyright © 2016 Naveen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "NSJSONSerialization+RemovingNulls.h"
#import "Reachability.h"

typedef void (^AlertHandler)(BOOL cancel,BOOL ok);

#define ERRORMESSAGE_ALERT(message) [SharedMethods showErrorAlertMessage:message]
#define PLACENOTFOUND_ALERT @"Whoops! We couldn’t find what you’re looking for."

@interface SharedMethods : NSObject

+ (void)showErrorAlertMessage:(NSString *)message;
+ (BOOL) validateEmail: (NSString *) candidate;

// font
UIFont *Calibri(CGFloat size);
UIFont *CalibriBold(CGFloat size);

// User Interface
UILabel *createLabelWithFrame(CGRect frame,NSString *title, UIColor *textColor, UIColor *bgColor, UIFont *font, NSTextAlignment intAlignment, int intLine, CGFloat borderWidth, UIColor *borderColor);

UIButton *createButtonWithSize(CGRect frame, NSString *title, UIImage *imageName, int tag, UIColor *bgColor, UIColor *textColor, UIFont *textFont, SEL selector, id target ,CGFloat borderWidth, UIColor *borderColor, int cornerRedius);

UIImageView *createImageViewWithFrameAndString(CGRect frame, NSString *imgName);

void showAlertController(NSString *title, NSString * messsage, UIViewController *vc,NSString *cancleButton,NSString *okButton,AlertHandler okHandler);

void showAlert(NSString *title, NSString * messsage, UIViewController *vc,NSString *cancleButton,NSString *okButton);
@end
