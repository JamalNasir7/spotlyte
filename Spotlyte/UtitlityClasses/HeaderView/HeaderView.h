//
//  HeaderView.h
//  Spotlyte
//
//  Created by Hemant on 13/02/17.
//  Copyright © 2017 Hemant. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HeaderView : UIView



-(id)initWithTitle:(NSString *)strTitle controller:(UIViewController*)view;
-(id)initWithTitleAndWhiteBack:(NSString *)strTitle controller:(UIViewController*)view;

@property(strong ,nonatomic)UIViewController *VC;
@property(strong ,nonatomic)UIView *lblSaprator;
@property(strong ,nonatomic)UILabel *lblViewName;
@property(strong,nonatomic)UIButton *btnback;
@property(strong, nonatomic)UIImageView *imgViewBack;
@property(strong, nonatomic)UIImageView *imgViewBack2;
@property(strong, nonatomic)NSString *title;
@property(nonatomic, copy) void (^BackButtonHandler)(BOOL sucess);
@property(strong,nonatomic)UIButton *btnAddPhotos;
@property(strong,nonatomic)UIButton *btnShare;

@end
