//
//  HeaderView.m
//  Spotlyte
//
//  Created by Hemant on 13/02/17.
//  Copyright © 2017 Hemant. All rights reserved.
//

#import "HeaderView.h"
#import "SharedMethods.h"
#import "AppDelegate.h"
#import "spotlytePromotionViewController.h"
#import "EventCategoryViewController.h"
#import "PromotionsViewController.h"
#import "RestListViewController.h"
#import "PromtionDetailsViewController.h"
#import "EventDescriptionVC.h"
#import "spotlytePromotionViewController.h"
#import "EventCategoryViewController.h"
#import "DashBoardViewController.h"
@implementation HeaderView


-(id)initWithTitle:(NSString *)strTitle controller:(UIViewController*)view{
   
        _VC=view;
        _title=strTitle;
    
      self = [super initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 60)];
    if (self) {
        [self setBackgroundColor:[UIColor whiteColor]];
        
        
        if ([[UIScreen mainScreen] bounds].size.height == 568) {
            self.lblViewName = createLabelWithFrame(CGRectMake((self.frame.size.width/2)-125, 18, 250, 40), strTitle, [UIColor colorWithRed:54.0f/255.0f green:69.0f/255.0f blue:79.0f/255.0f alpha:1], nil,CalibriBold(20.0f), NSTextAlignmentCenter, 2, 0, nil);
        }else
        {
            
            self.lblViewName = createLabelWithFrame(CGRectMake((self.frame.size.width/2)-150, 18, 300, 40), strTitle, [UIColor colorWithRed:54.0f/255.0f green:69.0f/255.0f blue:79.0f/255.0f alpha:1], nil,CalibriBold(20.0f), NSTextAlignmentCenter, 2, 0, nil);
        }
        self.imgViewBack = createImageViewWithFrameAndString(CGRectMake(15, 30, 10, 15), @"Back");
        self.btnback = createButtonWithSize(CGRectMake(0, 0, 75, 60), @"",nil, 0 , [UIColor clearColor], nil, nil, @selector(btnMethodForTransistions:), self,0.0, nil, 0);
        
        self.lblSaprator= [[UIView alloc]initWithFrame:CGRectMake(0, 58, [UIScreen mainScreen].bounds.size.width, 2)];
        [self.lblSaprator setBackgroundColor:[UIColor colorWithRed:0.0f/255.0f green:204.0f/255.0f blue:255.0f/255.0f alpha:1]];
        
        [self addSubview:self.imgViewBack];
        [self addSubview:self.lblSaprator];
        [self addSubview:self.lblViewName];
        [self addSubview:self.btnback];
        
    }
    return self;
}

-(id)initWithTitleAndWhiteBack:(NSString *)strTitle controller:(UIViewController*)view{
    
    _VC=view;
    _title=strTitle;
    
    self = [super initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 70)];
    if (self) {
        [self setBackgroundColor:[UIColor whiteColor]];
        
        
        if ([[UIScreen mainScreen] bounds].size.height == 568) {
            self.lblViewName = createLabelWithFrame(CGRectMake((self.frame.size.width/2)-125, 18, 250, 40), strTitle, [UIColor  redColor], nil,CalibriBold(16.0f), NSTextAlignmentCenter, 2, 0, nil);
        }else
        {
            
            self.lblViewName = createLabelWithFrame(CGRectMake((self.frame.size.width/2)-150, 18, 300, 40), strTitle, [UIColor  redColor], nil,CalibriBold(16.0f), NSTextAlignmentCenter, 2, 0, nil);
        }
        self.imgViewBack = createImageViewWithFrameAndString(CGRectMake(15, 30, 30, 30), @"ic_keyboard_arrow_left_white_48pt");
        self.imgViewBack2 = createImageViewWithFrameAndString(CGRectMake(15, 30, 30, 30), @"ic_keyboard_arrow_left_white_48pt_red");
        self.btnback = createButtonWithSize(CGRectMake(0, 0, 75, 60), @"",nil, 0 , [UIColor clearColor], nil, nil, @selector(btnMethodForTransistions:), self,0.0, nil, 0);
       
       
        self.btnAddPhotos =  [UIButton buttonWithType:UIButtonTypeCustom];
        [self.btnAddPhotos setBackgroundColor:[UIColor clearColor]];
        [self.btnAddPhotos setTitle:@"Add Photos" forState:UIControlStateNormal];
        [self.btnAddPhotos setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
        self.btnAddPhotos.titleLabel.font = Calibri(14.0f);
        self.btnAddPhotos.frame = CGRectMake(self.frame.size.width-100, 18, 100.0, 40.0);
        
        self.lblSaprator= [[UIView alloc]initWithFrame:CGRectMake(0, 68, [UIScreen mainScreen].bounds.size.width, 2)];
        [self.lblSaprator setBackgroundColor:[UIColor clearColor]];
        
        
        
        //[self addSubview: self.btnAddPhotos];
        [self addSubview:self.imgViewBack2];
        [self addSubview:self.imgViewBack];
        [self addSubview:self.lblViewName];
        [self addSubview:self.btnback];
        [self addSubview:self.lblSaprator];
        
    }
    return self;
}

-(void)btnMethodForTransistions:(UIButton*)sender{
 
    if ([_VC isMemberOfClass:[EventCategoryViewController class]] ||
        [_VC isMemberOfClass:[spotlytePromotionViewController class]] ||
        [_VC isMemberOfClass:[DashBoardViewController class]] ||
        [_VC isMemberOfClass:[PromotionsViewController class]]||
        [_VC isMemberOfClass:[RestListViewController class]])
    {
        _BackButtonHandler(YES);
    }
    else
        [_VC.navigationController popViewControllerAnimated:YES];
}


@end
