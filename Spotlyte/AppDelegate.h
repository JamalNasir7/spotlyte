//
//  AppDelegate.h
//  Spotlyte
//
//  Created by Tamilarasan on 3/3/16.
//  Copyright © 2016 Naveen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <Google/SignIn.h>
#import "SWRevealViewController.h"
//#import "TabBarController.h"
#import "UITabBarCustom.h"
#import "MenuViewController.h"
#import "HomeViewController.h"
#import "NewHomeViewController.h"
#import "EventCategoryViewController.h"
#import "spotlytePromotionViewController.h"
#import "DashBoardViewController.h"
#import "ProfileViewController.h"
#import "MyProfileViewController.h"

#define Default ([NSUserDefaults standardUserDefaults])
#define APP_DELEGATE ((AppDelegate*)[UIApplication sharedApplication].delegate)

@interface AppDelegate : UIResponder <UIApplicationDelegate,SWRevealViewControllerDelegate>
{
    EventCategoryViewController *topSpotVC;
    spotlytePromotionViewController *promotionVC;
    NewHomeViewController *homeVC;
    MenuViewController *sideMenuVC;
    DashBoardViewController *dashboardVC;
    MyProfileViewController *profileVC;
}

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) NSMutableArray * liveFeedArray;
@property (strong, nonatomic) UINavigationController *navigationController;
@property (strong, nonatomic) SWRevealViewController *swRevealController;
@property (strong, nonatomic) CLLocation *userCurrentLocation;
@property (strong, nonatomic) NSArray *categoryarray;
@property (strong, nonatomic) NSArray *cityArray;
@property (strong, nonatomic) NSString *Modal_PlaceType;
@property (strong, nonatomic) NSString *Modal_CityOrArea;


//@property (strong, nonatomic) TabBarController *tabView;
@property (nonatomic) BOOL settingEnable;
@property (nonatomic) BOOL IsFromNewHome;

@property (nonatomic, retain) UITabBarCustom *objCustomTabBar;
@property (strong, nonatomic) UINavigationController *navObj;


-(BOOL)checkReachability;
-(void) setCustomeTabbar;
-(UINavigationController *)getNavigationController;
+(AppDelegate *)shareAppdelegateInstance;
-(UIStoryboard *)getStoryBoardObj;
+ (UIImage *)scaleAndRotateImage:(UIImage *)image;


@end

