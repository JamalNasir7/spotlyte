//
//  EditPreferedCell.h
//  Spotlyte
//
//  Created by Tops on 8/9/17.
//  Copyright © 2017 Hemant. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EditPreferedCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UICollectionView *clctnView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cnst_ClctnViewHeight;

@end
