//
//  EditPreferedVC.m
//  Spotlyte
//
//  Created by Tops on 8/9/17.
//  Copyright © 2017 Hemant. All rights reserved.
//

#import "EditPreferedVC.h"
#import "HeaderView.h"
#import "EditPreferedCell.h"
#import "PreferedCollectionCell.h"
#import "EditProfileSObj.h"
#import <SDWebImage/UIImageView+WebCache.h>
@interface EditPreferedVC ()
{
    NSMutableArray *ArrParentPreferedList;
    NSMutableArray *ArrSelectedValue;
    BOOL IsSearching;
    NSMutableArray *ArrSearchFromParent;
    NSString *searchString;
    NSMutableArray *FinalResult;
}
@end

@implementation EditPreferedVC

#pragma mark - View LifeCycle
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupView];
    FinalResult = [[NSMutableArray alloc]init];
}
#pragma mark - setup View

-(void)setupView {
    //HeaderView *header=[[HeaderView alloc]initWithTitle:@"Favourite Item Selection" controller:self];
    //[header addSubview:header.btnback];
    //[self.view addSubview:header];
    _lbltitle.text = @"Choose your favorite daily special or happy hour items to get updates when location offer them.";
    
   //  _tblPreferedCateory.rowHeight = 120;
   // _tblPreferedCateory.estimatedRowHeight = UITableViewAutomaticDimension;

    //Array Declaration
    ArrParentPreferedList = [[NSMutableArray alloc]init];
    ArrSelectedValue = [[NSMutableArray alloc]init];
    ArrSearchFromParent = [[NSMutableArray alloc]init];
    searchString = [[NSString alloc]init];
    //API Call
    [self GetPreferedCategoryList];
}
#pragma mark - SomeOhter Methods
-(void)SetSkip_SubmitButton
{
    if (ArrSelectedValue.count > 0)
    {
        _btnSkip.hidden = true;
        _btnDone.hidden = false;
    }
    else{
        _btnSkip.hidden = false;
        _btnDone.hidden = true;
    }
}
-(void)showAlertTitle:(NSString *)title Message:(NSString *)message
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [alertController dismissViewControllerAnimated:YES completion:nil];
        
        //[self.navigationController popViewControllerAnimated:true];
        [self dismissViewControllerAnimated:true completion:nil];
    }];
    
    [alertController addAction:okBtn];
    
    [self presentViewController:alertController animated:YES completion:nil];
}
#pragma mark - TextviewDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField;
{
    
    [_txtSearch resignFirstResponder];
    return true;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string;
{
    
    [ArrSearchFromParent removeAllObjects];
    [FinalResult removeAllObjects];
    if([string isEqualToString:@""])
    {
        searchString = [searchString substringToIndex:[searchString length]-1];
    }
    else{
        searchString = [_txtSearch.text stringByAppendingString:string];
    }
    NSString *stringToSearch = searchString;
    if ([stringToSearch isEqualToString:@""])
    {
        IsSearching = false;
    }
    else{
        IsSearching = true;
    }
    for (EditProfileSObj *mainObj in ArrParentPreferedList)
    {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"strSubCategoryName BEGINSWITH[c] %@",stringToSearch];
            NSArray *results = [mainObj.arrSubcategory  filteredArrayUsingPredicate:predicate];
            NSMutableArray *arrSubobj = [[NSMutableArray alloc]init];
            if (results.count > 0)
            {
                [arrSubobj addObjectsFromArray:results];
                EditProfileSObj *Obj = [[EditProfileSObj alloc]init];
                Obj.strCategoryName = mainObj.strCategoryName;
                Obj.arrSubcategory = arrSubobj;
                [FinalResult addObject:Obj];
            }
    }
    if (FinalResult.count > 0)
    {
        [ArrSearchFromParent addObjectsFromArray:FinalResult];
    }
    else
    {
        [ArrSearchFromParent removeAllObjects];
    }
    [_tblPreferedCateory reloadData];
    return true;
}


#pragma mark - Tableview Delegate and datasource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    if (IsSearching == true)
    {
        return ArrSearchFromParent.count;
    }
    return ArrParentPreferedList.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    static NSString *cellIdentifier = @"EditPreferedCell";
    EditPreferedCell *cell = (EditPreferedCell *)[_tblPreferedCateory dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if(cell == nil)
    {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"EditPreferedCell" owner:self options:nil]objectAtIndex:0];
    }
    if (ArrParentPreferedList.count > 0)
    {
        EditProfileSObj *shareobj = [[EditProfileSObj alloc]init];
        if (IsSearching == true)
        {
            shareobj = [ArrSearchFromParent objectAtIndex:indexPath.row];
        }
        else{
            shareobj = [ArrParentPreferedList objectAtIndex:indexPath.row];
        }
        if (shareobj.arrSubcategory.count > 0)
        {
            cell.lblTitle.text = shareobj.strCategoryName;
        }
        else
        {
            cell.lblTitle.hidden = YES;
        }
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.clctnView.delegate = self;
        cell.clctnView.dataSource = self;
        cell.clctnView.scrollEnabled = false;
        [cell.clctnView registerNib:[UINib nibWithNibName:@"PreferedCollectionCell" bundle:nil]
        forCellWithReuseIdentifier:@"PreferedCollectionCell"];
        cell.clctnView.tag = indexPath.row;
        [cell.clctnView reloadData];
    }

    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"%f",self.view.frame.size.width);
    
    EditProfileSObj *shareObj = [[EditProfileSObj alloc]init];
    if (IsSearching == true)
    {
        shareObj = [ArrSearchFromParent objectAtIndex:indexPath.row];
    }
    else
    {
        shareObj = [ArrParentPreferedList objectAtIndex:indexPath.row];
    }

    if (shareObj.arrSubcategory.count <= 3)
    {
        return (self.view.frame.size.width/3) + 20;
    }
    
    double numberToRound = ((shareObj.arrSubcategory.count + 1) / 3.0);
    float min = ([ [[NSString alloc]initWithFormat:@"%.0f",numberToRound] doubleValue]);
    float max = min + 1;
    if (shareObj.arrSubcategory.count % 3 == 0)
    {
        return ((min) * (((tableView.frame.size.width/3.3)+10.0)));
    }
    return ((min) * (((tableView.frame.size.width/3.2)+10.0)));
}
#pragma mark - collectionview datasource and delegate
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section;
{
    EditProfileSObj *shareObj = [[EditProfileSObj alloc]init];
    if(IsSearching == true)
    {
        shareObj = [ArrSearchFromParent objectAtIndex:collectionView.tag];
    }
    else
    {
        shareObj = [ArrParentPreferedList objectAtIndex:collectionView.tag];
    }
    
    return [shareObj.arrSubcategory count];
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath;
{
    PreferedCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"PreferedCollectionCell" forIndexPath:indexPath];
    EditProfileSObj *shareObj = [[EditProfileSObj alloc]init];
    if (IsSearching == true)
    {
        shareObj = [ArrSearchFromParent objectAtIndex:collectionView.tag];
    }
    else
    {
        shareObj = [ArrParentPreferedList objectAtIndex:collectionView.tag];
    }
    NSLog(@"%lu",(unsigned long)shareObj.arrSubcategory.count);

    EditProfileSubObj *shareSubObj = [shareObj.arrSubcategory objectAtIndex:indexPath.row];
    cell.lblCategoryname.text = shareSubObj.strSubCategoryName;
    
    if([shareSubObj.strIsSelected isEqualToString: @"0"])
    {
        cell.imgSelected.hidden = true;
    }
    else
    {
        cell.imgSelected.hidden = false;
    }
    [cell SetView];
     NSString *strImageUrl = shareSubObj.strImagename;
    [cell.imgChildCategory sd_setImageWithURL:[NSURL URLWithString:strImageUrl] placeholderImage:[UIImage imageNamed:@""] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL)
    {
        if (!error)
        {
            [cell.imgChildCategory setImage:image];
        }
        else
        {
            
        }
    }];
    
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath;
{
    PreferedCollectionCell *collectionCell =(PreferedCollectionCell *) [collectionView cellForItemAtIndexPath:indexPath];
    EditProfileSObj *shareObj = [[EditProfileSObj alloc]init];
    if(IsSearching == true)
    {
        shareObj = [ArrSearchFromParent objectAtIndex:collectionView.tag];

    }
    else
    {
        shareObj = [ArrParentPreferedList objectAtIndex:collectionView.tag];

    }
    EditProfileSubObj *shareSubObj = [shareObj.arrSubcategory objectAtIndex:indexPath.row];
    NSMutableArray * tempArray = [ArrSelectedValue mutableCopy];

    if([shareSubObj.strIsSelected isEqualToString:@"0"])
    {
        shareSubObj.strIsSelected = @"1";
        [ArrSelectedValue addObject:shareSubObj.strCategoryID];
        collectionCell.imgSelected.hidden = false;
    }
    else{
        for(NSString *strCatID in ArrSelectedValue)
        {
            if ([strCatID isEqualToString:shareSubObj.strCategoryID])
            {
                [tempArray removeObject:strCatID];
            }
        }
        ArrSelectedValue = tempArray;
        shareSubObj.strIsSelected = @"0";
        collectionCell.imgSelected.hidden = true;
    }
    [self SetSkip_SubmitButton];
 //   NSLog(@"%@",ArrSelectedValue);
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(collectionView.frame.size.width/3.0, collectionView.frame.size.width/3.0);
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Set Array Data
-(NSMutableArray *)setArrayData:(NSArray*)input
{
    NSMutableArray *arrOut = [[NSMutableArray alloc]init];
    
    for(NSDictionary *dict in input)
    {
        EditProfileSObj *shareObj = [[EditProfileSObj alloc]init];
        shareObj = [[EditProfileSObj alloc]init];
        shareObj.arrSubcategory = [[NSMutableArray alloc]init];
        shareObj.strCategoryName = removeNull(dict[@"category_name"]);
        NSArray *arrsubCategory = [dict objectForKey:@"sub_cat"];

        for(NSDictionary *dict in arrsubCategory)
        {
            EditProfileSubObj *shareSubObj = [[EditProfileSubObj alloc]init];
            shareSubObj.strSubCategoryName = removeNull(dict[@"category_name"]);
            shareSubObj.strImagename = removeNull(dict[@"category_image"]);
            shareSubObj.strParentID = removeNull(dict[@"parentid"]);
            shareSubObj.strCategoryID = removeNull(dict[@"category_id"]);
            shareSubObj.strIsSelected = removeNull(dict[@"is_selected"]);
            if ([shareSubObj.strIsSelected isEqualToString:@"1"])
            {
                [ArrSelectedValue addObject:shareSubObj.strCategoryID];
            }
            [shareObj.arrSubcategory addObject:shareSubObj];
        }
        [arrOut addObject:shareObj];
    }
    [self SetSkip_SubmitButton];
    return arrOut;
}
#pragma mark - Button Events
- (IBAction)btnBackClick:(id)sender {
    [self dismissViewControllerAnimated:true completion:nil];
}

- (IBAction)btnSkipClick:(id)sender {
    [self dismissViewControllerAnimated:true completion:nil];
}

- (IBAction)btnDone:(id)sender {
    [self PostSelectedCategory];
}

- (IBAction)btnSearchClick:(id)sender {
    
}

#pragma mark - API Call
-(void)GetPreferedCategoryList
{
    NSDictionary *dict = @{@"user_id":self.strUserID};
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    [CUSTOMINDICATORMACRO startLoadAnimation:self];
    [ServiceHelper sendRequestToServerWithParameters:@"preferredCategory.json" forhttpMethod:@"POST" withrequestBody:jsonString sync:YES completion:^(id obj) {

        if ([obj isKindOfClass:[NSDictionary class]])
        {
            [ArrParentPreferedList removeAllObjects];
            NSArray *arrData = [obj objectForKey:@"data"];
            ArrParentPreferedList = [self setArrayData:arrData];
            if(ArrParentPreferedList.count>0)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [CUSTOMINDICATORMACRO stopLoadAnimation:self];
                    [_tblPreferedCateory reloadData];
                });
            }
        }
    }];
}

-(void)PostSelectedCategory
{
    NSMutableString *strPostSelectedValue = [[NSMutableString alloc]init];
    for(NSString *obj in ArrSelectedValue)
    {
     [strPostSelectedValue appendString:[NSString stringWithFormat:@"%@,",obj]];
    }
    
    NSDictionary *dict = @{@"user_id":self.strUserID,@"category_id":strPostSelectedValue};
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    [CUSTOMINDICATORMACRO startLoadAnimation:self];

    [ServiceHelper sendRequestToServerWithParameters:@"savePreferredCategory.json" forhttpMethod:@"POST" withrequestBody:jsonString sync:YES completion:^(id obj) {
        if ([obj isKindOfClass:[NSDictionary class]])
        {
            NSArray *arrData = [obj objectForKey:@"data"];
            NSLog(@"%@",[arrData valueForKey:@"message"]);
            dispatch_async(dispatch_get_main_queue(), ^{
                [CUSTOMINDICATORMACRO stopLoadAnimation:self];
                [self showAlertTitle:@"Spotlyte" Message:[arrData valueForKey:@"message"]];
            });
        }
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
