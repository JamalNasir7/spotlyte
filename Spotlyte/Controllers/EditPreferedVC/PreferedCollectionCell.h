//
//  PreferedCollectionCell.h
//  Spotlyte
//
//  Created by Tops on 8/9/17.
//  Copyright © 2017 Hemant. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PreferedCollectionCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIView *View_Content;
@property (weak, nonatomic) IBOutlet UIImageView *imgChildCategory;
@property (weak, nonatomic) IBOutlet UILabel *lblCategoryname;
@property (weak, nonatomic) IBOutlet UIImageView *imgSelected;
-(void)SetView;
@end
