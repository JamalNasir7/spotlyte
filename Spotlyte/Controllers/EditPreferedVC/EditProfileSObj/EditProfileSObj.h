//
//  EditProfileSObj.h
//  Spotlyte
//
//  Created by Tops on 8/9/17.
//  Copyright © 2017 Hemant. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EditProfileSObj : NSObject

@property (strong,nonatomic) NSString *strCategoryName;
@property (strong,nonatomic) NSMutableArray *arrSubcategory;
@end

@interface EditProfileSubObj : NSObject

@property (strong,nonatomic) NSString *strSubCategoryName;
@property (strong,nonatomic) NSString *strImagename;
@property (strong,nonatomic) NSString *strCategoryID;
@property (strong,nonatomic) NSString *strParentID;
@property (strong,nonatomic) NSString *strIsSelected;

@end
