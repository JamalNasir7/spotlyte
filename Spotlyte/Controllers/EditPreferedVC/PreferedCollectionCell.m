//
//  PreferedCollectionCell.m
//  Spotlyte
//
//  Created by Tops on 8/9/17.
//  Copyright © 2017 Hemant. All rights reserved.
//

#import "PreferedCollectionCell.h"

@implementation PreferedCollectionCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(void)SetView
{
    _View_Content.layer.cornerRadius = 5;
    _View_Content.layer.masksToBounds = true;
   // _View_Content.layer.borderWidth = 1.0;
    _imgChildCategory.layer.cornerRadius = 5;
    _imgChildCategory.layer.masksToBounds = true;
   // _imgChildCategory.layer.borderWidth = 1.0;
}
@end
