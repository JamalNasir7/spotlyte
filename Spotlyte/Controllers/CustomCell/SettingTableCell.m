//
//  SettingTableCell.m
//  Spotlyte
//
//  Created by Hemant on 13/02/17.
//  Copyright © 2017 Hemant. All rights reserved.
//

#import "SettingTableCell.h"

@implementation SettingTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
