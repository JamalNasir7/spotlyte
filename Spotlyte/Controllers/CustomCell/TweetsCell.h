//
//  TweetsCell.h
//  SpotLyte
//
//  Created by Senthil Kumar on 16/03/16.
//  Copyright © 2016 Colan. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface TweetsCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *userNameLabel;
@property (nonatomic, weak) IBOutlet UIImageView *imageProfile;
@property (nonatomic, weak) IBOutlet UILabel *lblTweetMessage;
@property (nonatomic, weak) IBOutlet UILabel *lblTweetTiming;
@property (nonatomic, weak) IBOutlet UIImageView *asynch_ProfileImage;
@property (nonatomic, weak) IBOutlet UILabel *lblDate;
@property (nonatomic, weak) IBOutlet UILabel *lblRating;
@property (nonatomic, weak) IBOutlet UILabel *user_name;

@end
