//
//  FollowersCell.m
//  Spotlyte
//
//  Created by Admin on 20/09/16.
//  Copyright © 2016 Naveen. All rights reserved.
//

#import "FollowersCell.h"

@implementation FollowersCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)layoutSubviews{
    self.image_Profile.layer.cornerRadius = self.image_Profile.frame.size.width / 2;
    self.image_Profile.layer.masksToBounds = true;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
