//
//  DailySpecialCell.h
//  Spotlyte
//
//  Created by Admin on 04/04/16.
//  Copyright © 2016 Naveen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DailySpecialCell : UITableViewCell


@property (strong, nonatomic) IBOutlet UILabel *lblDays;

@property (strong, nonatomic) IBOutlet UILabel *lblDescription;
@property (strong, nonatomic) IBOutlet UIView *viewSeperator;

@property (strong, nonatomic) IBOutlet UIButton *btnFavSpecial;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btnFavWidthCons;
@end
