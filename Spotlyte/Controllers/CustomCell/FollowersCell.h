//
//  FollowersCell.h
//  Spotlyte
//
//  Created by Admin on 20/09/16.
//  Copyright © 2016 Naveen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FollowersCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *image_Profile;
@property (strong, nonatomic) IBOutlet UILabel *lbl_ProfileName;
@property (strong, nonatomic) IBOutlet UILabel *lbl_FirstLastName;

@property (strong, nonatomic) IBOutlet UIButton *btn_Follow;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btnFollowWidthConst;

@end
