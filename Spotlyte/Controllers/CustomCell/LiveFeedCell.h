//
//  LiveFeedCell.h
//  Spotlyte
//
//  Created by Admin on 18/03/16.
//  Copyright © 2016 Naveen. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface LiveFeedCell : UITableViewCell
@property (strong, nonatomic) IBOutlet EDStarRating *starRating_LiveFeed;

@property (nonatomic, weak) IBOutlet UIImageView *imageProfile;
@property (nonatomic, weak) IBOutlet UILabel *lbltweetName;
@property (nonatomic, weak) IBOutlet UILabel *lbltweetMessage;
@property (nonatomic, weak) IBOutlet UILabel *lbltweetTime;
@property (nonatomic, weak) IBOutlet UIImageView *asynchProfileImage;
@property (weak, nonatomic) IBOutlet UILabel *lblUsername;

-(void)liveFeedstarRating :(NSString *)ratingVal;


@end
