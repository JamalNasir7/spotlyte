//
//  PhotosCollectionViewCell.h
//  Spotlyte
//
//  Created by Admin on 02/05/16.
//  Copyright © 2016 Naveen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhotosCollectionViewCell : UICollectionViewCell
@property (nonatomic, retain) IBOutlet UIImageView *asynchImage;

@end
