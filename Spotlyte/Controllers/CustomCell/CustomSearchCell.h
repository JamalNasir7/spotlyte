//
//  CustomSearchCell.h
//  Spotlyte
//
//  Created by CIPL242-MOBILITY on 12/09/16.
//  Copyright © 2016 Naveen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomSearchCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lbl_Name;
@property (strong, nonatomic) IBOutlet UILabel *lbl_Select;
@property (strong, nonatomic) IBOutlet UIImageView *imgView;

@end
