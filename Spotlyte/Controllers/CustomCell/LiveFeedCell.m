//
//  LiveFeedCell.m
//  Spotlyte
//
//  Created by Admin on 18/03/16.
//  Copyright © 2016 Naveen. All rights reserved.
//

#import "LiveFeedCell.h"

@implementation LiveFeedCell
@synthesize starRating_LiveFeed;
- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)liveFeedstarRating :(NSString *)ratingVal
{
    
    if(ratingVal.floatValue <= 2.0)
    {
        starRating_LiveFeed.starHighlightedImage = [UIImage imageNamed:@"star-1_R.png"];
    }
    else if(ratingVal.floatValue <= 3.9)
    {
        starRating_LiveFeed.starHighlightedImage = [UIImage imageNamed:@"star-1_Y.png"];
    }
    else
    {
        starRating_LiveFeed.starHighlightedImage = [UIImage imageNamed:@"star-1_G.png"];
    }
    starRating_LiveFeed.starImage = [UIImage imageNamed:@"unstartemp.png"];
    
    starRating_LiveFeed.maxRating = 5.0;
    starRating_LiveFeed.horizontalMargin = 4;
    starRating_LiveFeed.editable = NO;
    starRating_LiveFeed.rating = ratingVal.floatValue;
    starRating_LiveFeed.displayMode = EDStarRatingDisplayAccurate;
}
@end
