//
//  BreweriesCollectionCell.h
//  Spotlyte
//
//  Created by Hemant on 01/03/17.
//  Copyright © 2017 Hemant. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BreweriesCollectionCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIButton *btnLike;
@property (weak, nonatomic) IBOutlet UILabel *lblLike;
@property (weak, nonatomic) IBOutlet UIButton *btnSpecials;
@property (weak, nonatomic) IBOutlet UIButton *btnPressReport;

@property (strong, nonatomic) IBOutlet UIImageView *imgView;
@property (strong, nonatomic) IBOutlet UILabel *lblResName;
@property (strong, nonatomic) IBOutlet UILabel *lblAddress;
@property (strong, nonatomic) IBOutlet UILabel *lblOverallRating;
@property (strong, nonatomic) IBOutlet EDStarRating *EDStar_userRating;
@property (strong, nonatomic) IBOutlet UILabel *lblReviews;
@property (strong, nonatomic) IBOutlet UILabel *lblMiles;
@property (strong, nonatomic) IBOutlet UIButton *btnFavourite;
@property (strong, nonatomic) IBOutlet UILabel *lblPlaceName;

@end
