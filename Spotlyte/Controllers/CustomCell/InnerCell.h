//
//  InnerCell.h
//  Spotlyte
//
//  Created by Admin on 08/09/16.
//  Copyright © 2016 Naveen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InnerCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *textLbl;
@property (strong, nonatomic) IBOutlet UIButton *btnRadio;
@property (strong, nonatomic) IBOutlet UIButton *btnFullScreen;

@end
