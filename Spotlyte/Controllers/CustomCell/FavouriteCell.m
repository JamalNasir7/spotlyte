//
//  FavouriteCell.m
//  Spotlyte
//
//  Created by Hemant on 25/02/17.
//  Copyright © 2017 Hemant. All rights reserved.
//

#import "FavouriteCell.h"

@implementation FavouriteCell

- (void)awakeFromNib {
    [super awakeFromNib];
  }

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
