//
//  ReviewCollectionCell.h
//  Spotlyte
//
//  Created by Admin on 27/04/16.
//  Copyright © 2016 Naveen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReviewCollectionCell : UICollectionViewCell

@property (nonatomic, retain) IBOutlet UIImageView *asynchImage;
@property (nonatomic, retain) IBOutlet UIImageView *image1;

@end
