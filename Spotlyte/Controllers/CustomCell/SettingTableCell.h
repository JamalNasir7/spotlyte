//
//  SettingTableCell.h
//  Spotlyte
//
//  Created by Hemant on 13/02/17.
//  Copyright © 2017 Hemant. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingTableCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *settingImgView;

@property (strong, nonatomic) IBOutlet UILabel *settingLabel;


@end
