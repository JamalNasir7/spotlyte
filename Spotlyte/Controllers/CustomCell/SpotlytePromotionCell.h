//
//  SpotlytePromotionCell.h
//  Spotlyte
//
//  Created by Hemant on 27/02/17.
//  Copyright © 2017 Hemant. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SpotlytePromotionCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UILabel *lblResName;
@property (strong, nonatomic) IBOutlet UILabel *lblDistance;
@property (strong, nonatomic) IBOutlet UILabel *lblAddress;
@property (strong, nonatomic) IBOutlet UIButton *btnFavourite;
@property (strong, nonatomic) IBOutlet UILabel *lblOverallRating;
@property (strong, nonatomic) IBOutlet EDStarRating *lblStar;
@property (strong, nonatomic) IBOutlet UILabel *lblReviews;

@property (strong, nonatomic) IBOutlet UIImageView *imgView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *SpecialBtn_Y;
@property (strong, nonatomic) IBOutlet UIButton *btnSpecials;

@end
