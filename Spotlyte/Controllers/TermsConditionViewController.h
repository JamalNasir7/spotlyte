//
//  TermsConditionViewController.h
//  Spotlyte
//
//  Created by Hemant on 27/02/17.
//  Copyright © 2017 Hemant. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TermsConditionViewController : UIViewController

@property (nonatomic,strong) NSString *headerName;

@property (strong, nonatomic) IBOutlet UITextView *lblTect;
@end
