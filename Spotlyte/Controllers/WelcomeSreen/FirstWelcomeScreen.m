//
//  FirstWelcomeScreen.m
//  Spotlyte
//
//  Created by Hemant on 10/02/17.
//  Copyright © 2017 Naveen. All rights reserved.
//

#import "FirstWelcomeScreen.h"
#import "SecondWelcomeSreen.h"
@interface FirstWelcomeScreen ()

@end

@implementation FirstWelcomeScreen

- (void)viewDidLoad {
    [super viewDidLoad];
}

-(void)viewWillAppear:(BOOL)animated{

    if (APP_DELEGATE.settingEnable==NO) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)btnMethod:(id)sender {
    SecondWelcomeSreen *secondCon=[self.storyboard instantiateViewControllerWithIdentifier:@"secondCon"];
    [self.navigationController pushViewController:secondCon animated:YES];
}


@end
