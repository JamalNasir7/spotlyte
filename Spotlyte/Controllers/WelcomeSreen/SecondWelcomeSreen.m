//
//  SecondWelcomeSreen.m
//  Spotlyte
//
//  Created by Hemant on 10/02/17.
//  Copyright © 2017 Naveen. All rights reserved.
//

#import "SecondWelcomeSreen.h"
#import "TutorialScreenController.h"
@interface SecondWelcomeSreen ()

@end

@implementation SecondWelcomeSreen

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)btnMethod:(id)sender {
    APP_DELEGATE.settingEnable=NO;
    [self.navigationController popToRootViewControllerAnimated:NO];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"dismissNotification" object:nil];
}

@end
