//
//  TutorialScreenController.m
//  Spotlyte
//
//  Created by Hemant on 10/02/17.
//  Copyright © 2017 Naveen. All rights reserved.
//

#import "TutorialScreenController.h"
#import "FirstWelcomeScreen.h"


@interface TutorialScreenController ()
{
    NSArray *arrPageImages;
}
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@end

@implementation TutorialScreenController

- (void)viewDidLoad {
    [super viewDidLoad];
    _btnLetsGO_S1.hidden = NO;
    _btnLetsGO_S1.adjustsImageWhenHighlighted = NO;
    _btnLetsGO_S4.adjustsImageWhenHighlighted = NO;
    
     arrPageImages =@[@"tutorial1",@"tutorial2",@"tutorial3",@"tutorial4"];
    _pageControl.hidden = YES;
    self.automaticallyAdjustsScrollViewInsets = false;
    _scrollView.contentInset = UIEdgeInsetsZero;
    _scrollView.scrollIndicatorInsets = UIEdgeInsetsZero;
    _scrollView.contentOffset = CGPointMake(0.0, 0.0);
    
    UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTap:)];
    singleTapGestureRecognizer.numberOfTapsRequired = 1;
    singleTapGestureRecognizer.enabled = YES;
    singleTapGestureRecognizer.cancelsTouchesInView = NO;
    [_scrollView addGestureRecognizer:singleTapGestureRecognizer];
}

- (void)singleTap:(UITapGestureRecognizer *)gesture {
    if (_pageControl.currentPage == 3)
        {
            APP_DELEGATE.settingEnable=NO;
            [self.navigationController popToRootViewControllerAnimated:NO];
            [self dismissViewControllerAnimated:true completion:nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"dismissNotification" object:nil];
        }
}
- (IBAction)btnMethod:(UIButton*)sender
{
    if (sender.tag == 4)
    {
        APP_DELEGATE.settingEnable=NO;
        [self.navigationController popToRootViewControllerAnimated:NO];
        [self dismissViewControllerAnimated:true completion:nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"dismissNotification" object:nil];
    }
    else
    {
        CGRect frame = _scrollView.frame;
        frame.origin.x = frame.size.width * 1;
        frame.origin.y = 0;
        [_scrollView scrollRectToVisible:frame animated:YES];
    }
//    APP_DELEGATE.settingEnable=NO;
//    
//    [self.navigationController popToRootViewControllerAnimated:NO];
//    [self dismissViewControllerAnimated:true completion:nil];
//
//    [[NSNotificationCenter defaultCenter] postNotificationName:@"dismissNotification" object:nil];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];}

-(void)viewDidLayoutSubviews{
    [self setupScrollView];}

//<----------------------------
#pragma mark scrollview methods
//<----------------------------

-(void)setupScrollView {
    _pageControl.numberOfPages=arrPageImages.count;
    [_scrollView setContentSize:CGSizeMake(self.view.frame.size.width * [arrPageImages count], _scrollView.frame.size.height)];
    
    for (int i = 0; i < arrPageImages.count; i++){
        CGFloat xOrigin = i * self.view.frame.size.width;
        
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(xOrigin, 0, self.view.frame.size.width, _scrollView.frame.size.height)];
        [imageView setImage:[UIImage imageNamed:arrPageImages[i]]];
        [_scrollView addSubview:imageView];
    }
}


- (void)scrollViewDidScroll:(UIScrollView *)sender{
    CGFloat width = self.view.frame.size.width;
    NSInteger page = (_scrollView.contentOffset.x + (0.5f * width)) / width;
    _pageControl.currentPage = page;
    
    if (page == 0)
    {
        _btnLetsGO_S1.hidden = NO;
        _btnLetsGO_S4.hidden = YES;
    }
    else if (page == 3)
    {
        _btnLetsGO_S4.hidden = YES;
        _btnLetsGO_S1.hidden = YES;
    }
    else
    {
        _btnLetsGO_S4.hidden = YES;
        _btnLetsGO_S1.hidden = YES;
    }
}


@end
