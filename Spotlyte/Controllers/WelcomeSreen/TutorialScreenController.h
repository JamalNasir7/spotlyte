//
//  TutorialScreenController.h
//  Spotlyte
//
//  Created by Hemant on 10/02/17.
//  Copyright © 2017 Naveen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TutorialScreenController : UIViewController<UIGestureRecognizerDelegate>
@property (strong,nonatomic) IBOutlet UIPageControl *pageControl;
@property (strong,nonatomic) IBOutlet UIButton *btnLetsGO_S4;

@property (strong,nonatomic) IBOutlet UIButton *btnLetsGO_S1;

@end
