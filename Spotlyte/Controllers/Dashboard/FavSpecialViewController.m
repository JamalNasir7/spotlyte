//
//  DashboardDetailViewController.m
//  Spotlyte
//
//  Created by Admin on 25/03/16.
//  Copyright © 2016 Naveen. All rights reserved.
//

#import "FavSpecialViewController.h"
#import "HeaderView.h"
#import "FavouriteCell.h"
#import "DailySpecialCell.h"
#import "PromotionsViewController.h"
#import "HappyHoursCell.h"
#import "SpecialCell.h"
#import "DashboardSpecialCell.h"
#import "SpecialObj.h"

@interface FavSpecialViewController ()
{
    int btnNo;
    NSInteger selectedRow;
    NSArray *arrySpecials;
    
    BOOL tapped;
    BOOL locationBool;
    NSMutableArray *ArrHappyHours;
    NSMutableArray *ArrDailySpecials;
    NSMutableArray *arrSubObj;
    
    SpecialSubObj *specialSubobj;
    SpecialObj *specialObj;
    
    
    RegisterModal *registerModalResponeValue1;
    ResturantModal *restModal_QuickView;
    ResturantModal *selectedResturant;
}
@end

@implementation FavSpecialViewController

@synthesize placeArray;

- (void)viewDidLoad {
    [super viewDidLoad];

    arrSubObj = [[NSMutableArray alloc]init];
    ArrHappyHours = [[NSMutableArray alloc]init];
    arrySpecials = [[NSMutableArray alloc]init];
    _tblPopup.rowHeight = UITableViewAutomaticDimension;
    _tblPopup.estimatedRowHeight = 44.0;
    tableView_Favourite.rowHeight = UITableViewAutomaticDimension;
    tableView_Favourite.estimatedRowHeight = 120;
    [_btnDailySpecial setTitle:@"Favorite Happy Hours" forState:UIControlStateNormal];
    [_btnHappyHours setTitle:@"Favorite Daily Specials" forState:UIControlStateNormal];
    // Register Cell
        [self colorChangeOnPressTab:@[_btnHappyHours,_btnDailySpecial]];
    UINib *nib = [UINib nibWithNibName:@"SpecialCell" bundle:nil];
    [tableView_Favourite registerNib:nib forCellReuseIdentifier:@"SpecialCell"];
    
    UINib *nib_2 = [UINib nibWithNibName:@"DashboardSpecialCell" bundle:nil];
    [tableView_Favourite registerNib:nib_2 forCellReuseIdentifier:@"DashboardSpecialCell"];
    //Hiren
    self.viewTool.sectionTitles = @[@"Favorite Happy Hours",@"Favorite Daily Specials"];
    self.viewTool.selectedSegmentIndex = 0;
    self.viewTool.backgroundColor = [UIColor whiteColor];
    self.viewTool.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor colorWithRed:153.0/255.0 green:153.0/255.0 blue:153.0/255.0 alpha:1.0],NSFontAttributeName: [UIFont systemFontOfSize:14.0]};
    self.viewTool.selectedTitleTextAttributes = @{NSForegroundColorAttributeName : [UIColor colorWithRed:0.0/255.0 green:158.0/255.0 blue:224.0/255.0 alpha:1.0],NSFontAttributeName: [UIFont systemFontOfSize:14.0]};
    self.viewTool.selectionIndicatorColor = [UIColor colorWithRed:0.0/255.0 green:158.0/255.0 blue:224.0/255.0 alpha:1.0];
    self.viewTool.selectionIndicatorBoxColor = [UIColor blueColor];
    self.viewTool.selectionStyle = HMSegmentedControlSelectionStyleFullWidthStripe;
    self.viewTool.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    self.viewTool.selectionIndicatorHeight = 2.0;
    
    [self.viewTool setIndexChangeBlock:^(NSInteger index) {
        if (index == 0){
            if (ArrHappyHours.count>0)
            {
                _View_Empty.hidden = YES;
                tableView_Favourite.hidden = NO;
                [tableView_Favourite reloadData];
            }
            else{
                tableView_Favourite.hidden = YES;
                _View_Empty.hidden = NO;
            }
           // [self showHidePopup:arrySpecials.count];
        }else{
            //arrySpecials = restModal_QuickView.arraySpecials;
            if (ArrDailySpecials.count>0)
            {
                _View_Empty.hidden = YES;
                tableView_Favourite.hidden = NO;
                [tableView_Favourite reloadData];
            }
            else{
                tableView_Favourite.hidden = YES;
                _View_Empty.hidden = NO;
            }
           // [self showHidePopup:arrySpecials.count];
        }
    }];

    
    
    tableView_Favourite.rowHeight = UITableViewAutomaticDimension;
    tableView_Favourite.estimatedRowHeight = 30.0;

    // Do any additional setup after loading the view.
    placeArray = [[NSMutableArray alloc]init];
    
    HeaderView *header = [[HeaderView alloc]initWithTitle:@"FAVORITE SPECIALS" controller:self];
    [header addSubview:header.btnback];
    [self.view addSubview:header];
    
    tableView_Favourite.tableFooterView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, tableView_Favourite.frame.size.width, 0.1f)];
    
    _popupViewBG.hidden = YES;
    _popupView.hidden=YES;
    _btnCross.hidden=YES;
    _emptyView.hidden=YES;
    
    _popupView.layer.cornerRadius = 5;
    _popupView.layer.borderWidth = 1;
    _popupView.clipsToBounds = YES;
    _popupView.layer.borderColor = [[UIColor colorWithRed:153.0f/255.0f green:153.0f/255.0f blue:153.0f/255.0f alpha:1]CGColor];
    
    _btnExpand.layer.cornerRadius = 5;
    _btnExpand.layer.borderWidth = 1;
    _btnExpand.clipsToBounds = YES;
    _btnExpand.layer.borderColor = [[UIColor colorWithRed:18.0f/255.0f green:166.0f/255.0f blue:233.0f/255.0f alpha:1]CGColor];
    
    isCheckedTab = FALSE;
    tableView_Favourite.hidden = YES;
    [self initializeLocationManager];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self Favorite_specialsHappyHoursServiceHelper];
    locationBool=YES;
    [tableView_Favourite reloadData];
}


- (void)initializeLocationManager {
    if(![CLLocationManager locationServicesEnabled]) {
        showAlertController(@"Enable Location Service", @"You have to enable the Location Service to use this App. To enable, please go to Settings->Privacy->Location Services", self, @"OK", nil,^(BOOL cancel, BOOL ok){
        });
        return;
    }
    
    if ([CLLocationManager authorizationStatus]==kCLAuthorizationStatusDenied){
        showAlertController(@"App Permission Denied", @"To re-enable, please go to Settings and turn on Location Service for this app.", self, @"OK", nil,^(BOOL cancel, BOOL ok){
        });
        return;
    }
    
    if (!_locationManager) {
        _locationManager = [[CLLocationManager alloc] init];
        
    }
    
    _locationManager.delegate = self;
    [_locationManager requestAlwaysAuthorization];
    _locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
    _locationManager.distanceFilter = kCLDistanceFilterNone;
    _locationManager.activityType = CLActivityTypeAutomotiveNavigation;
    [_locationManager startUpdatingLocation];
}


- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    APP_DELEGATE.userCurrentLocation=newLocation;
    
    if (APP_DELEGATE.userCurrentLocation.coordinate.latitude && APP_DELEGATE.userCurrentLocation.coordinate.longitude) {
        if (locationBool) {
            [self Favorite_specialsHappyHoursServiceHelper];

          // [self likeAndDislikeSpecialFavoriteServiceHelper];
            locationBool=NO;}
    }
    
    [_locationManager stopUpdatingLocation];
    _locationManager=nil;
}
#pragma mark -- SetArrayData
-(NSMutableArray *)setArrayData_DailySpecial:(NSArray*)input {
    NSMutableArray *arrOut = [[NSMutableArray alloc]init];
    
    for(NSDictionary *dict in input) {
        specialObj = [[SpecialObj alloc]init];
        specialObj.Arr_happyHour = [[NSMutableArray alloc]init];
        specialObj.strPlaceID = removeNull(dict[@"place_id"]);
        specialObj.strPlaceImage = removeNull(dict[@"place_image"]);
        specialObj.strPlaceName = removeNull(dict[@"place_name"]);
        specialObj.strRating = removeNull(dict[@"ratings"]);
        NSArray *arrsubCategory = [dict objectForKey:@"place_specials"];
        
        for(NSDictionary *dict in arrsubCategory) {
            specialSubobj = [[SpecialSubObj alloc]init];
            specialSubobj.strHappyHoursID = removeNull(dict[@"id"]);
            specialSubobj.strTitle = removeNull(dict[@"title"]);
            specialSubobj.strLocation = removeNull(dict[@"location"]);
            specialSubobj.strPrice = removeNull(dict[@"price"]);
            specialSubobj.strStartTime = removeNull(dict[@"start_time"]);
            specialSubobj.strEndTime = removeNull(dict[@"end_time"]);
            specialSubobj.strWeekDay = removeNull(dict[@"weekday"]);
            
            [specialObj.Arr_happyHour addObject:specialSubobj];
        }
        [arrOut addObject:specialObj];
    }
    //  [self SetSkip_SubmitButton];
    return arrOut;
}
-(NSMutableArray *)setArrayData_happyHours:(NSArray*)input {
    NSMutableArray *arrOut = [[NSMutableArray alloc]init];
    
    for(NSDictionary *dict in input) {
        SpecialObj *shareObj = [[SpecialObj alloc]init];
        shareObj = [[SpecialObj alloc]init];
        shareObj.Arr_happyHour = [[NSMutableArray alloc]init];
        shareObj.strPlaceID = removeNull(dict[@"place_id"]);
        shareObj.strPlaceImage = removeNull(dict[@"place_image"]);
        shareObj.strPlaceName = removeNull(dict[@"place_name"]);
        shareObj.strRating = removeNull(dict[@"ratings"]);
        NSArray *arrsubCategory = [dict objectForKey:@"happy_hours"];
        
        for(NSDictionary *dict in arrsubCategory) {
            SpecialSubObj *shareSubObj = [[SpecialSubObj alloc]init];
            shareSubObj.strHappyHoursID = removeNull(dict[@"id"]);
            shareSubObj.strTitle = removeNull(dict[@"title"]);
            shareSubObj.strLocation = removeNull(dict[@"location"]);
            shareSubObj.strPrice = removeNull(dict[@"price"]);
            shareSubObj.strStartTime = removeNull(dict[@"start_time"]);
            shareSubObj.strEndTime = removeNull(dict[@"end_time"]);
            shareSubObj.strWeekDay = removeNull(dict[@"weekday"]);

            [shareObj.Arr_happyHour addObject:shareSubObj];
        }
        [arrOut addObject:shareObj];
    }
  //  [self SetSkip_SubmitButton];
    return arrOut;
}
#pragma mark -- Service Helper
-(void)likeAndDislikeSpecialFavoriteServiceHelper
{
    ResturantModal *modal       =   [[ResturantModal alloc]init];
    NSUserDefaults *defaults    =   [NSUserDefaults standardUserDefaults];
    NSData *data                =   [defaults objectForKey:@"logindetails"];
    
    RegisterModal * registerModalResponeValue   =   [NSKeyedUnarchiver unarchiveObjectWithData:data];
    NSString * user_IDRegister                  =   registerModalResponeValue.userID;
    
    BOOL check=[[AppDelegate shareAppdelegateInstance] checkReachability];
    if(check)
    {
        [CUSTOMINDICATORMACRO startLoadAnimation:self];
        [modal setUserID:user_IDRegister];
        
        [RestaurantMacro likeAndDislikeSpecialFavoriteToServer:modal withCompletion:^(id obj1) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if (obj1!=nil && ![obj1 isKindOfClass:[NSNull class]]) {
                    if([obj1 isKindOfClass:[ResturantModal class]])
                    {
                        ResturantModal *response = obj1;
                        placeArray = response.arrayPlaceFavourites;
                        
                        if(placeArray.count == 0)
                        {
                            view_NoValues.hidden = NO;
                            _View_Empty.hidden = NO;

                            tableView_Favourite.hidden=YES;
                        }
                        else
                        {
                            view_NoValues.hidden = YES;
                            _View_Empty.hidden = YES;

                            tableView_Favourite.hidden = NO;
                            [tableView_Favourite reloadData];
                        }
                    }
                    else
                    {
                        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Please check your connection" preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction *okBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                            [self dismissViewControllerAnimated:YES completion:nil];
                        }];
                        
                        [alertController addAction:okBtn];
                        [self presentViewController:alertController animated:YES completion:nil];
                    }
                }
                else
                view_NoValues.hidden = NO;
                [CUSTOMINDICATORMACRO stopLoadAnimation:self];
            });
        }];
    }
}

-(void)Favorite_specialsHappyHoursServiceHelper
{
        //NSDictionary *dict = @{@"user_id":self.strUserID};
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [defaults objectForKey:@"logindetails"];
    registerModalResponeValue1 = [NSKeyedUnarchiver unarchiveObjectWithData:data];

        NSDictionary *dict = @{@"user_id":registerModalResponeValue1.userID};
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:nil];
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        [CUSTOMINDICATORMACRO startLoadAnimation:self];
        [ServiceHelper sendRequestToServerWithParameters:@"retrieveUserSpecialsHappyHours.json" forhttpMethod:@"POST" withrequestBody:jsonString sync:YES completion:^(id obj) {
            [CUSTOMINDICATORMACRO stopLoadAnimation:self];

            if ([obj isKindOfClass:[NSDictionary class]])
            {
                [ArrDailySpecials removeAllObjects];
                [ArrHappyHours removeAllObjects];
                NSDictionary *dictData = [obj valueForKey:@"data"];
                NSArray *happyhrs = [dictData valueForKey:@"happy_hours_list"];
                if (happyhrs.count>0) {
                    ArrHappyHours = [self setArrayData_happyHours:happyhrs];
                }
                NSArray *dailySpl = [dictData valueForKey:@"place_specials_list"];
                if (dailySpl.count>0)
                {
                    ArrDailySpecials = [self setArrayData_DailySpecial:dailySpl];
                }

                dispatch_async(dispatch_get_main_queue(), ^{
                    tableView_Favourite.hidden = false;
                    if (ArrHappyHours.count>0)
                    {
                        _View_Empty.hidden = YES;
                        tableView_Favourite.hidden = NO;
                        [tableView_Favourite reloadData];
                    }
                    else{
                        tableView_Favourite.hidden = YES;
                        _View_Empty.hidden = NO;
                    }
                    [tableView_Favourite reloadData];
                });

                //[ArrParentPreferedList removeAllObjects];
                //NSArray *arrData = [obj objectForKey:@"data"];
               // ArrParentPreferedList = [self setArrayData:arrData];
               // if(ArrParentPreferedList.count>0)
               // {
                 //   dispatch_async(dispatch_get_main_queue(), ^{
                //        [CUSTOMINDICATORMACRO stopLoadAnimation:self];
                  //      [_tblPreferedCateory reloadData];
                //});
               // }
            }
        }];
    }
//-(void)Favorite_specialsHappyHoursServiceHelper
//{
//    BOOL check=[[AppDelegate shareAppdelegateInstance] checkReachability];
//    if(check){
//        [CUSTOMINDICATORMACRO startLoadAnimation:self];
//        [RestaurantMacro getSpecialsHappyHoursToServer_Special:selectedResturant withCompletion:^(id obj1) {
//            dispatch_async(dispatch_get_main_queue(), ^{
//                if([obj1 isKindOfClass:[ResturantModal class]]){
//                    restModal_QuickView  = obj1;
//                    if (restModal_QuickView.arrayHappyHours.count>0 || restModal_QuickView.arraySpecials.count>0) {
//
//                        [_tblPopup setContentOffset:CGPointZero animated:NO];
//                        [self tapOnHappyHours:nil];
////                        _popupViewBG.hidden = NO;
////                        _popupView.hidden=NO;
////                        _btnCross.hidden=NO;
////                        alertVw.hidden=NO;
//                        view_NoValues.hidden = YES;
//                        _View_Empty.hidden = YES;
//                        tableView_Favourite.hidden = NO;
//                        [tableView_Favourite reloadData];
//                    }
//                    else{
//                        tableView_Favourite.hidden = YES;
//                        view_NoValues.hidden = NO;
//                        _View_Empty.hidden = NO;
//                    }
//                }
//                else{
//                    tableView_Favourite.hidden = YES;
//                    view_NoValues.hidden = NO;
//                    _View_Empty.hidden = NO;
//                    //[tableView_Favourite reloadData];
////                    _popupViewBG.hidden = NO;
////                    _popupView.hidden =NO;
////                    _btnCross.hidden  =NO;
//                    [self serverError];
//                }
//                [CUSTOMINDICATORMACRO stopLoadAnimation:self];
//            });
//        }];
//    }
//}

#pragma mark -- TableView Delegate Method

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([tableView isEqual:_tblPopup ]){
        return arrySpecials.count;
    }
    else if (tableView == tableView_Favourite){
        if(self.viewTool.selectedSegmentIndex == 0)
        {
            return ArrHappyHours.count;
        }
        else{
            return ArrDailySpecials.count;
        }
    }
    else{
        if(self.viewTool.selectedSegmentIndex == 0)
        {
            SpecialObj *Obj = [ArrHappyHours objectAtIndex:tableView.tag];
            arrSubObj = Obj.Arr_happyHour;
            return arrSubObj.count;
        }
        else{
            SpecialObj *Obj = [ArrDailySpecials objectAtIndex:tableView.tag];
            arrSubObj = Obj.Arr_happyHour;
            return arrSubObj.count;
        }
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([tableView isEqual:_tblPopup]) {
        if (tableView == _tblPopup)
        {
            static NSString *cellIdentifier = @"sampleidentifier";
            DailySpecialCell *cell = (DailySpecialCell *)[_tblPopup dequeueReusableCellWithIdentifier:cellIdentifier];
            
            if(cell == nil)
            {
                cell = [[[NSBundle mainBundle]loadNibNamed:@"DailySpecialCell" owner:self options:nil]objectAtIndex:0];
            }
            if (arrySpecials.count>0) {
                ResturantModal *modelDailySpecialDetails = [arrySpecials objectAtIndex:indexPath.row];
                
                
                _btnExpand.tag=indexPath.row;
                _btnReportHere.tag=indexPath.row;
                
                NSString *mystr;
                
                if([modelDailySpecialDetails.specials_weekDay isEqualToString:@"monday"])
                {
                    mystr=@"Monday";
                }
                else if ([modelDailySpecialDetails.specials_weekDay isEqualToString:@"tuesday"])
                {
                    mystr=@"Tuesday";
                }
                else if ([modelDailySpecialDetails.specials_weekDay isEqualToString:@"wednesday"])
                {
                    mystr=@"Wednesday";
                }
                else if ([modelDailySpecialDetails.specials_weekDay isEqualToString:@"thursday"])
                {
                    mystr=@"Thursday";
                }
                else if ([modelDailySpecialDetails.specials_weekDay isEqualToString:@"friday"])
                {
                    mystr=@"Friday";
                }
                else if ([modelDailySpecialDetails.specials_weekDay isEqualToString:@"saturday"])
                {
                    mystr=@"Saturday";
                }
                else
                {
                    mystr=@"Sunday";
                }
                
                
                
                NSString *trimmedString = [modelDailySpecialDetails.title stringByTrimmingCharactersInSet:
                                           [NSCharacterSet whitespaceCharacterSet]];
                
                NSString *stringStartTime;
                
                NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
                NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
                [dateFormatter1 setLocale:locale];

                dateFormatter1.dateFormat = @"HH:mm:ss";
                
                NSDate *date1 = [dateFormatter1 dateFromString:modelDailySpecialDetails.startTime];
                
                
                
                dateFormatter1.dateFormat = @"hh:mm a";
                
                stringStartTime = [dateFormatter1 stringFromDate:date1];
                
                stringStartTime = [stringStartTime stringByReplacingOccurrencesOfString:@":00" withString:@""];
                
                
                int stringStartTimeInt=[stringStartTime intValue];
                
                if (stringStartTimeInt<10)
                {
                    NSRange range = NSMakeRange(0,1);
                    
                    stringStartTime = [stringStartTime stringByReplacingCharactersInRange:range withString:@""];
                }
                else
                {
                    stringStartTime = [stringStartTime stringByReplacingOccurrencesOfString:@":00" withString:@""];
                }
                
                
                NSString *stringCloseTime;
                
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                NSLocale *locale2 = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
                [dateFormatter setLocale:locale2];

                dateFormatter.dateFormat = @"HH:mm:ss";
                
                NSDate *date = [dateFormatter dateFromString:modelDailySpecialDetails.endTime];
                
                
                
                dateFormatter.dateFormat = @"hh:mm a";
                
                stringCloseTime = [dateFormatter stringFromDate:date];
                
                stringCloseTime = [stringCloseTime stringByReplacingOccurrencesOfString:@":00" withString:@""];
                
                
                
                int stringCloseTimeInt=[stringCloseTime intValue];
                
                if (stringCloseTimeInt<10)
                {
                    NSRange range = NSMakeRange(0,1);
                    
                    stringCloseTime = [stringCloseTime stringByReplacingCharactersInRange:range withString:@""];
                }
                else
                {
                    stringCloseTime = [stringCloseTime stringByReplacingOccurrencesOfString:@":00" withString:@""];
                }
                
                
                
                if ([modelDailySpecialDetails.price isEqualToString:@"N/A"] || [modelDailySpecialDetails.price isEqualToString:@"1/2 Off"] || [modelDailySpecialDetails.price isEqualToString:@"Other"] || [modelDailySpecialDetails.price isEqualToString:@"FREE"])
                {
                    
                    cell.lblDays.text=mystr;
                    NSString *completeString = [NSString stringWithFormat:@"%@ %@ (%@-%@)",modelDailySpecialDetails.price,trimmedString,stringStartTime,stringCloseTime];
                    NSMutableAttributedString *AttributedString = [[NSMutableAttributedString alloc] initWithString:completeString];
                    NSString *boldString =modelDailySpecialDetails.price;
                    NSRange boldRange = [completeString rangeOfString:boldString];
                    [AttributedString addAttribute: NSFontAttributeName value:CalibriBold(11.0f) range:boldRange];
                    [cell.lblDescription setAttributedText: AttributedString];
                    
                }
                else
                {
                    cell.lblDays.text=mystr;
                    NSString *price;
                    if ([modelDailySpecialDetails.price rangeOfString:@"$"].location == NSNotFound) {
                        price = [NSString stringWithFormat:@"$%@",modelDailySpecialDetails.price];
                    }else
                    {
                        price = [NSString stringWithFormat:@"%@",modelDailySpecialDetails.price];
                    }
                    
                    cell.lblDescription.text=[NSString stringWithFormat:@"%@ %@ (%@-%@)",price, trimmedString,stringStartTime,stringCloseTime];

                    
                }
                
                if(modelDailySpecialDetails.isCheckedFavSpecial == NO)
                {
                    cell.btnFavSpecial.selected = false;
                }
                else{
                    cell.btnFavSpecial.selected = true;
                }
                
                [cell.btnFavSpecial addTarget:self action:@selector(btnFavSpecialClickk:) forControlEvents:UIControlEventTouchUpInside];
                cell.btnFavSpecial.tag = indexPath.row;
                
                cell.textLabel.numberOfLines = 2;
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                cell.viewSeperator.backgroundColor = [UIColor colorWithRed:153.0f/255.0f green:153.0f/255.0f blue:153.0f/255.0f alpha:1];
                
            }
            
            
        return cell;
            
        }

    }
    else if (tableView == tableView_Favourite){
        
        static NSString *cellIdentifier = @"DashboardSpecialCell";
        DashboardSpecialCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
        if(cell == nil)
        {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"DashboardSpecialCell" owner:self options:nil]objectAtIndex:0];
        }
        SpecialObj *modalObj;
        
        if (self.viewTool.selectedSegmentIndex ==0)
        {
            modalObj = [ArrHappyHours objectAtIndex:indexPath.row];
        }
        else{
            modalObj = [ArrDailySpecials objectAtIndex:indexPath.row];
        }
        
        cell.lblPlaceName.text = modalObj.strPlaceName;
        cell.lblRating.hidden = NO;
        if ([modalObj.strRating isEqualToString:@"0"]){
            cell.lblRating.text = @"N/A";
        }else{
            cell.lblRating.text = modalObj.strRating;
        }
        
        NSString *strImageUrl = modalObj.strPlaceImage;
        [cell.imgPlace sd_setImageWithURL:[NSURL URLWithString:strImageUrl] placeholderImage:[UIImage imageNamed:@""] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            if (!error) {
                [cell.imgPlace setImage:image];
            }
            else {
            }
        }];
        //arrSubObj = [modalObj.Arr_happyHour objectAtIndex:indexPath.section];
        [cell.tblHappyHours reloadData];
        cell.tblHappyHours.tag = indexPath.row;
        cell.tblHappyHours.delegate = self;
        cell.tblHappyHours.dataSource = self;
        cell.tblHappyHours.rowHeight = UITableViewAutomaticDimension;
        cell.tblHappyHours.estimatedRowHeight = 30.0;

        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    else{
        static NSString *cellIdentifier = @"sampleidentifier";
        DailySpecialCell *cell = (DailySpecialCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if(cell == nil)
        {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"DailySpecialCell" owner:self options:nil]objectAtIndex:0];
        }
        if (arrSubObj.count>0)
        {
            
            if(self.viewTool.selectedSegmentIndex == 0)
            {
                SpecialObj *Obj = [ArrHappyHours objectAtIndex:tableView.tag];
                arrSubObj = Obj.Arr_happyHour;
            }
            else{
                SpecialObj *Obj = [ArrDailySpecials objectAtIndex:tableView.tag];
                arrSubObj = Obj.Arr_happyHour;
            }

            SpecialSubObj *subobj = [arrSubObj objectAtIndex:indexPath.row];
            NSString *mystr;
            cell.viewSeperator.hidden = NO;
            cell.btnFavSpecial.userInteractionEnabled = true;
            if([subobj.strWeekDay isEqualToString:@"monday"])
            {
                mystr= @"Monday"; //[modelDailySpecialDetails.specials_weekDay substringToIndex:1];
            }
            else if ([subobj.strWeekDay isEqualToString:@"tuesday"])
            {
                mystr= @"Tuesday"; //[modelDailySpecialDetails.specials_weekDay substringToIndex:2];
            }
            else if ([subobj.strWeekDay isEqualToString:@"wednesday"])
            {
                mystr= @"Wednesday"; //[modelDailySpecialDetails.specials_weekDay substringToIndex:1];
            }
            else if ([subobj.strWeekDay isEqualToString:@"thursday"])
            {
                mystr= @"Thursday"; //[modelDailySpecialDetails.specials_weekDay substringToIndex:2];
            }
            else if ([subobj.strWeekDay isEqualToString:@"friday"])
            {
                mystr= @"Friday"; //[modelDailySpecialDetails.specials_weekDay substringToIndex:1];
            }
            else if ([subobj.strWeekDay isEqualToString:@"saturday"])
            {
                mystr= @"Saturday"; //[modelDailySpecialDetails.specials_weekDay substringToIndex:2];
            }
            else
            {
                mystr= @"Sunday"; //[modelDailySpecialDetails.specials_weekDay substringToIndex:2];
            }
            cell.lblDays.text = mystr;
            NSString *strPrice;
            if ([subobj.strPrice isEqualToString:@"N/A"] || [subobj.strPrice isEqualToString:@"1/2 Off"] || [subobj.strPrice isEqualToString:@"Other"] || [subobj.strPrice isEqualToString:@"FREE"])
            {
                strPrice = subobj.strPrice;
                
            }
            else
            {
                if ([subobj.strPrice rangeOfString:@"$"].location == NSNotFound) {
                    strPrice = [NSString stringWithFormat:@"$%@",subobj.strPrice];
                }else
                {
                    strPrice = [NSString stringWithFormat:@"%@",subobj.strPrice];
                }
            }
            NSString *stringStartTime;
            
            NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
            [dateFormatter1 setLocale:locale];

            dateFormatter1.dateFormat = @"HH:mm:ss";
            
            NSDate *date1 = [dateFormatter1 dateFromString:subobj.strStartTime];
            
            
            
            dateFormatter1.dateFormat = @"hh:mm a";
            
            stringStartTime = [dateFormatter1 stringFromDate:date1];
            
            stringStartTime = [stringStartTime stringByReplacingOccurrencesOfString:@":00" withString:@""];
            
            
            int stringStartTimeInt=[stringStartTime intValue];
            
            if (stringStartTimeInt<10)
            {
                NSRange range = NSMakeRange(0,1);
                
                stringStartTime = [stringStartTime stringByReplacingCharactersInRange:range withString:@""];
            }
            else
            {
                stringStartTime = [stringStartTime stringByReplacingOccurrencesOfString:@":00" withString:@""];
            }
            
            
            NSString *stringCloseTime;
            
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            NSLocale *locale3 = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
            [dateFormatter setLocale:locale3];

            dateFormatter.dateFormat = @"HH:mm:ss";
            
            NSDate *date = [dateFormatter dateFromString:subobj.strEndTime];
            
            
            
            dateFormatter.dateFormat = @"hh:mm a";
            
            stringCloseTime = [dateFormatter stringFromDate:date];
            
            stringCloseTime = [stringCloseTime stringByReplacingOccurrencesOfString:@":00" withString:@""];
            
            
            
            int stringCloseTimeInt=[stringCloseTime intValue];
            
            if (stringCloseTimeInt<10)
            {
                NSRange range = NSMakeRange(0,1);
                
                stringCloseTime = [stringCloseTime stringByReplacingCharactersInRange:range withString:@""];
            }
            else
            {
                stringCloseTime = [stringCloseTime stringByReplacingOccurrencesOfString:@":00" withString:@""];
            }
            NSString *strTime = [NSString stringWithFormat:@"(%@ - %@)",stringStartTime,stringCloseTime];
            NSString *trimmedString = [subobj.strTitle stringByTrimmingCharactersInSet:
                                       [NSCharacterSet whitespaceCharacterSet]];
            
            cell.lblDescription.text = [NSString stringWithFormat:@"%@ %@ %@",strPrice,trimmedString,strTime];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
//            [cell.btnFavSpecial setSelected:YES];
//            cell.btnFavSpecial.tag = indexPath.row;
//            [cell.btnFavSpecial addTarget:self action:@selector(btnFavSpecialClick:) forControlEvents:UIControlEventTouchUpInside];

        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        [cell.btnFavSpecial setSelected:YES];
        cell.btnFavSpecial.tag = indexPath.row;
        [cell.btnFavSpecial addTarget:self action:@selector(btnFavSpecialClickk:) forControlEvents:UIControlEventTouchUpInside];
        return cell;
    }
    return nil;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([tableView isEqual:_tblPopup])
        return UITableViewAutomaticDimension;
    else
       return UITableViewAutomaticDimension;
}
-(BOOL) tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView == _tblPopup)
    {
        return NO;
    }
    else if(tableView == tableView_Favourite)
    {
        return NO;
    }
    else{
        return YES;
    }

    return NO;
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        if (tableView == _tblPopup)
        {
            
        }
        else if(tableView == tableView_Favourite)
        {
            
        }
        else{
            
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Are you sure want to delete?" preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *YesBtn = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                                     {
                                         
                                         NSLog(@"indexPath = %ld", (long)tableView.tag);
                                         NSLog(@"tag = %ld",indexPath.row);
                                         
                                         
                                         SpecialObj *modalObj;
                                         
                                         if (self.viewTool.selectedSegmentIndex ==0)
                                         {
                                             modalObj = [ArrHappyHours objectAtIndex:tableView.tag];
                                         }
                                         else{
                                             modalObj = [ArrDailySpecials objectAtIndex:tableView.tag];
                                         }
                                         
                                         arrSubObj = modalObj.Arr_happyHour;
                                
                                         SpecialSubObj *modelDailySpecialDetails = [arrSubObj objectAtIndex:indexPath.row];
                                         
                                         NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:@"logindetails"];
                                         RegisterModal *registerModalResponeValue = [NSKeyedUnarchiver unarchiveObjectWithData:data];
                                         
                                         NSString *strType;
                                         if(self.viewTool.selectedSegmentIndex == 0){
                                             strType = @"h";
                                         }
                                         else{
                                             strType = @"s";
                                         }
                                         
                                         NSString *strFav;
                                         strFav = @"2";
                                         
                                         BOOL check=[[AppDelegate shareAppdelegateInstance] checkReachability];
                                         
                                         if(check)
                                         {
                                             [CUSTOMINDICATORMACRO startLoadAnimation:self];
                                             
                                             NSDictionary *dict = @{@"special_id":modelDailySpecialDetails.strHappyHoursID,@"user_id":registerModalResponeValue.userID,@"is_favorite":strFav,@"type":strType};
                                             
                                             NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:nil];
                                             NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
                                             
                                             [ServiceHelper sendRequestToServerWithParameters:@"likeAndDislikeSpecialFavorite.json" forhttpMethod:@"POST" withrequestBody:jsonString sync:YES completion:^(id obj) {
                                                 if ([obj isKindOfClass:[NSDictionary class]]) {
                                                     
                                                     if(self.viewTool.selectedSegmentIndex == 0)
                                                     {
                                                         [arrSubObj removeObjectAtIndex:indexPath.row];
                                                         
                                                         if (arrSubObj.count == 0) {
                                                             [ArrHappyHours removeObjectAtIndex:tableView.tag];
                                                         }
                                                         if (ArrHappyHours.count == 0) {
                                                             _View_Empty.hidden = NO;
                                                             tableView_Favourite.hidden = YES;
                                                         }
                                                         else {
                                                             _View_Empty.hidden = YES;
                                                             tableView_Favourite.hidden = NO;
                                                         }
                                                     }
                                                     else
                                                     {
                                                         [arrSubObj removeObjectAtIndex:indexPath.row];
                                                         if (arrSubObj.count == 0) {
                                                             [ArrDailySpecials removeObjectAtIndex:tableView.tag];
                                                         }
                                                         if (ArrDailySpecials.count == 0) {
                                                             _View_Empty.hidden = NO;
                                                             tableView_Favourite.hidden = YES;
                                                         }
                                                         else {
                                                             _View_Empty.hidden = YES;
                                                             tableView_Favourite.hidden = NO;
                                                         }
                                                     }
                                                     [CUSTOMINDICATORMACRO stopLoadAnimation:self];
                                                     dispatch_async(dispatch_get_main_queue(), ^{
                                                         [tableView_Favourite reloadData];
                                                     });
                                                 }
                                             }];
                                         }
                                         
                                         
                                     }];
            UIAlertAction *NoBtn = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                                    {
                                        [self dismissViewControllerAnimated:YES completion:nil];
                                    }];
            
            [alertController addAction:YesBtn];
            [alertController addAction:NoBtn];
            
            [self presentViewController:alertController animated:YES completion:nil];

        }

        
    }
}
- (void)btnFavSpecialClickk:(id)sender {
    /*
    UIButton *btn = (UIButton*)sender;
    
    UIView *parentCell = btn.superview;
    while (![parentCell isKindOfClass:[UITableViewCell class]]) {   // iOS 7 onwards the table cell hierachy has changed.
        parentCell = parentCell.superview;
    }
    
    UIView *parentView = parentCell.superview;
    
    while (![parentView isKindOfClass:[UITableView class]]) {   // iOS 7 onwards the table cell hierachy has changed.
        parentView = parentView.superview;
    }
    
    UITableView *tableView = (UITableView *)parentView;
    NSIndexPath *indexpath = [NSIndexPath indexPathForRow:tableView.tag inSection:0];

//    NSIndexPath *indexPath = [tableView indexPathForCell:(UITableViewCell *)parentCell];
    
    NSLog(@"indexPath = %ld", (long)indexpath.row);
    NSLog(@"tag = %ld", (long)btn.tag);
    
    SpecialSubObj *modelDailySpecialDetails = [arrSubObj objectAtIndex:btn.tag];
    
    NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:@"logindetails"];
    RegisterModal *registerModalResponeValue = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    NSString *strType;
    if(self.viewTool.selectedSegmentIndex == 0){
        strType = @"h";
    }
    else{
        strType = @"s";
    }
    
    NSString *strFav;
        strFav = @"2";
    
    BOOL check=[[AppDelegate shareAppdelegateInstance] checkReachability];
    
    if(check)
    {
        [CUSTOMINDICATORMACRO startLoadAnimation:self];

        NSDictionary *dict = @{@"special_id":modelDailySpecialDetails.strHappyHoursID,@"user_id":registerModalResponeValue.userID,@"is_favorite":strFav,@"type":strType};
        
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:nil];
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        
        [ServiceHelper sendRequestToServerWithParameters:@"likeAndDislikeSpecialFavorite.json" forhttpMethod:@"POST" withrequestBody:jsonString sync:YES completion:^(id obj) {
            if ([obj isKindOfClass:[NSDictionary class]]) {
                
                if(self.viewTool.selectedSegmentIndex == 0)
                {
                    [arrSubObj removeObjectAtIndex:btn.tag];
                    
                    if (arrSubObj.count == 0) {
                        [ArrHappyHours removeObjectAtIndex:indexpath.row];
                    }
                    if (ArrHappyHours.count == 0) {
                        _View_Empty.hidden = NO;
                        tableView_Favourite.hidden = YES;
                    }
                    else {
                        _View_Empty.hidden = YES;
                        tableView_Favourite.hidden = NO;
                    }
                }
                else
                {
                    [arrSubObj removeObjectAtIndex:btn.tag];
                    if (arrSubObj.count == 0) {
                        [ArrDailySpecials removeObjectAtIndex:indexpath.row];
                    }
                    if (ArrDailySpecials.count == 0) {
                        _View_Empty.hidden = NO;
                        tableView_Favourite.hidden = YES;
                    }
                    else {
                        _View_Empty.hidden = YES;
                        tableView_Favourite.hidden = NO;
                    }
                }
                [CUSTOMINDICATORMACRO stopLoadAnimation:self];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [tableView_Favourite reloadData];
                });
            }
        }];
    }*/
}

- (void)displayStar :(NSString *)ratingVal
{
    if(ratingVal.floatValue <= 2.0)
    {
        starRatingView.starHighlightedImage = [UIImage imageNamed:@"RedStarIcon"];
    }
    else if(ratingVal.floatValue <= 3.9)
    {
        starRatingView.starHighlightedImage = [UIImage imageNamed:@"YellowStarIcon"];
    }
    else
    {
        starRatingView.starHighlightedImage = [UIImage imageNamed:@"GreenStarIcon"];
    }
    
    starRatingView.starImage = [UIImage imageNamed:@"UnStarIcon"];
    starRatingView.maxRating = 5.0;
    starRatingView.delegate = self;
    starRatingView.horizontalMargin = 10;
    starRatingView.editable = NO;
    starRatingView.rating = ratingVal.floatValue;
    starRatingView.displayMode = EDStarRatingDisplayAccurate;
}

#pragma mark --- Action Methods
- (IBAction)btnHappyHourClick:(id)sender {
    [self colorChangeOnPressTab:@[_btnHappyHours,_btnDailySpecial]];
//    [_btnHppyHours setSelected:NO];
//    [_btnDailySpecial setSelected:YES];
    arrySpecials = restModal_QuickView.arrayHappyHours;
    [tableView_Favourite reloadData];
}
- (IBAction)btnDailSpecialClick:(id)sender {
    
//    [_btnHppyHours setSelected:YES];
//    [_btnDailySpecial setSelected:NO];
    [self colorChangeOnPressTab:@[_btnDailySpecial,_btnHappyHours]];
    arrySpecials = restModal_QuickView.arraySpecials;
    [tableView_Favourite reloadData];
}
- (IBAction)tapOnCrossBtn:(id)sender {
    _popupViewBG.hidden = YES;
    _popupView.hidden=YES;
    _btnCross.hidden=YES;
       alertVw.hidden=YES;
}
-(IBAction)pressFavouriteBtn:(UIButton*)sender
{
    [self placeFavorite:sender];
}

-(void)placeFavorite:(UIButton*)sender{

    UIButton *btnFavourite=sender;
    
    ResturantModal *modal;
    NSArray *filteredArray;
    
    modal = [placeArray objectAtIndex:sender.tag];
    filteredArray  = [placeArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"placeID ==%@",modal.placeID]];
    
    if(filteredArray>0)
    {
        modal = [filteredArray firstObject];
    }
    
    NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:@"logindetails"];
    RegisterModal *registerModalResponeValue = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    if(modal.isCheckedFavourite == NO)
    {
        BOOL check=[[AppDelegate shareAppdelegateInstance] checkReachability];
        if(check)
        {
            [modal setUserID:registerModalResponeValue.userID];
            [CUSTOMINDICATORMACRO startLoadAnimation:self];
            [RestaurantMacro favouriteToServer:modal withCompletion:^(id obj1) {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    ResturantModal *responseModal = nil;
                    if([obj1 isKindOfClass:[ResturantModal class]])
                    {
                        responseModal = obj1;
                        if([responseModal.result isEqualToString:@"success"]){
                            [btnFavourite setImage:[UIImage imageNamed:@"FavSelLIst" ] forState:UIControlStateNormal];
                            
                            [modal setIsCheckedFavourite:YES];
                        }
                    }
                    else
                        [self serverError];
                    
                    [CUSTOMINDICATORMACRO stopLoadAnimation:self];
                });
            }];
        }
    }
    else
    {
        [modal setUserID:registerModalResponeValue.userID];
        
        BOOL check=[[AppDelegate shareAppdelegateInstance] checkReachability];
        if(check)
        {
            [CUSTOMINDICATORMACRO startLoadAnimation:self];
            [RestaurantMacro unFavouriteToServer:modal withCompletion:^(id obj1) {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [CUSTOMINDICATORMACRO stopLoadAnimation:self];
                    ResturantModal *responseModal =  nil;
                    if([obj1 isKindOfClass:[ResturantModal class]])
                    {
                        responseModal = obj1;
                        if([responseModal.result isEqualToString:@"success"])
                        {
                            [btnFavourite setImage:[UIImage imageNamed:@"FavUnSelLIst" ] forState:UIControlStateNormal];
                            [modal setIsCheckedFavourite:NO];
                        }
                    }
                    else
                        [self serverError];
                });
            }];
        }
        
    }


}

-(void)favouriteServiceHelper :(PromtionsModal *)modalObj andwith:(UIButton*)sender
{
    BOOL check=[[AppDelegate shareAppdelegateInstance] checkReachability];
    if(check)
    {
        [CUSTOMINDICATORMACRO startLoadAnimation:self];
        [PromotionMacro addFavouriteSpotlyteToServer:modalObj withCompletion:^(id obj1) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [CUSTOMINDICATORMACRO stopLoadAnimation:self];
                if([obj1 isKindOfClass:[ResturantModal class]])
                {
                    ResturantModal *responseModal = nil;
                    responseModal = obj1;
                    if([responseModal.result isEqualToString:@"success"])
                    {
                        [sender setImage:[UIImage imageNamed:@"FavSelLIst"] forState:UIControlStateNormal];
                        [modalObj setPromotionFavStatus:@"Yes"];
                    }
                }
                else
                    [self serverError];
            });
        }];
    }
}

-(void)unFavouriteServiceHelper :(PromtionsModal *)modalObj andwith:(UIButton*)sender
{
    BOOL check=[[AppDelegate shareAppdelegateInstance] checkReachability];
    if(check)
    {
        [CUSTOMINDICATORMACRO startLoadAnimation:self];
        [PromotionMacro unFavouriteSpotlyteToServer:modalObj withCompletion:^(id obj1) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [CUSTOMINDICATORMACRO stopLoadAnimation:self];
                if([obj1 isKindOfClass:[ResturantModal class]])
                {
                    ResturantModal *responseModal;
                    responseModal = obj1;
                    if([responseModal.result isEqualToString:@"success"])
                    {
                        [sender setImage:[UIImage imageNamed:@"FavUnSelLIst"] forState:UIControlStateNormal];
                        [modalObj setPromotionFavStatus:@"No"];
                    }
                }
                else
                    [self serverError];
            });
        }];
    }
    
}

- (IBAction)pressNavigateSpecialHours:(UIButton*)sender
{
        
    PromotionsViewController *promtionVC = [self.storyboard instantiateViewControllerWithIdentifier:@"promtions"];
    promtionVC.selectedValue = sender.tag;
    promtionVC.placeIDString = selectedResturant.placeID;
    promtionVC.promtionsArray = placeArray;
    promtionVC.selectedWhichController = @"specialshappyhours";
    
    BOOL isCheckedRandomSearch = [[NSUserDefaults standardUserDefaults]boolForKey:@"randomsearch"];
    if(isCheckedRandomSearch == TRUE)
    {
        promtionVC.checkTextOrSearch = @"customsearch";
    }
    else
    {
        promtionVC.checkTextOrSearch = @"nosearch";
        [[NSUserDefaults standardUserDefaults]setBool:FALSE forKey:@"randomsearch"];
        
    }
    [self.navigationController pushViewController:promtionVC animated:YES];
    
    /*promtionVC.selectHomeVC=^(ResturantModal *restModel){
     
     promotionModalOBJ = restModel;
     };*/
}

-(IBAction)pressReportHereBtn : (UIButton*)sender
{
   UIAlertController *alertController = [UIAlertController alertControllerWithTitle:selectedResturant.resturantName message:@"Specials and Hours are subject to change. To ensure the most up-to-date specials you can view this locations website specials here" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *YesBtn = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                             {
                                 
                                
                                     
                                     //[selectedResturant setWebSite_Specials:modal.webSite_Specials];
                                     if(selectedResturant.webSite_Specials.length>0)
                                     {
                                         TermsViewController *termsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"termsvc"];
                                         termsVC.checkVC = @"promotions";
                                         termsVC.checkBack = @"homevc";
                                         termsVC.CheckUrlType=@"SpecialLink";
                                         termsVC.restModal_TermsVC = selectedResturant;
                                         [self.navigationController pushViewController:termsVC animated:YES];
                                     }
                                     else
                                     {
                                         
                                         TermsViewController *termsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"termsvc"];
                                         termsVC.checkVC = @"promotions";
                                         termsVC.checkBack = @"homevc";
                                         termsVC.CheckUrlType=@"ResturantEMail";
                                         termsVC.restModal_TermsVC = selectedResturant;
                                         [self.navigationController pushViewController:termsVC animated:YES];
                                     }
                                     
                                 
                                 
                             }];
    
    UIAlertAction *NoBtn = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                            {
                                [self dismissViewControllerAnimated:YES completion:nil];
                            }];
    
    [alertController addAction:YesBtn];
    [alertController addAction:NoBtn];
    
    [self presentViewController:alertController animated:YES completion:nil];
    
}




- (IBAction)reportAction:(id)sender {
    NSString *emailTitle = selectedResturant.resturantName.uppercaseString;
    // Email Content
    NSString *messageBody = @"";
    //NSString *messageBody = selectedResturant.resturantDescription;
    // To address
    NSArray *toRecipents = [NSArray arrayWithObject:@"contact@spotlytespecials.com"];
    
    MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
    if ([MFMailComposeViewController canSendMail]) {
        mc.mailComposeDelegate = self;
        [mc setSubject:emailTitle];
        [mc setMessageBody:messageBody isHTML:NO];
        [mc setToRecipients:toRecipents];
        
        // Present mail view controller on screen
        [self presentViewController:mc animated:YES completion:NULL];
    }
    
}


- (void)mailComposeController:(MFMailComposeViewController*)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError*)error;
{
 
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            [self dismissViewControllerAnimated:YES completion:NULL];
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            [self dismissViewControllerAnimated:YES completion:NULL];
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            [self dismissViewControllerAnimated:YES completion:NULL];
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            [self dismissViewControllerAnimated:YES completion:NULL];
            break;
        default:
            break;
    }
    // Close the Mail Interface
}


-(IBAction)pressBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)tapOnLike:(UIButton*)sender{
    
    ResturantModal *modal = [[ResturantModal alloc]init];
    
    modal = [placeArray objectAtIndex:sender.tag];
    NSInteger  strLike = [modal.likecount integerValue];
    if ([modal.likeORDislike isEqualToString:@"2"] || [modal.likeORDislike isEqualToString:@""]) {
        [modal.likeORDislike isEqualToString:@"1"];
        [modal setLikeORDislike:@"1"];
        modal.likecount = [NSString stringWithFormat:@"%ld",(long)strLike+1];
    }
    else{
        [modal.likeORDislike isEqualToString:@"2"];
        [modal setLikeORDislike:@"2"];
        if (strLike > 0)
        {
            modal.likecount = [NSString stringWithFormat:@"%ld",(long)strLike-1];
        }
    }
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [defaults objectForKey:@"logindetails"];
    RegisterModal *registerModalResponeValue = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    NSString * user_IDRegister = registerModalResponeValue.userID;
    [modal setUserID:user_IDRegister];
    NSString *checkcount  = modal.likeORDislike;
    BOOL check=[[AppDelegate shareAppdelegateInstance] checkReachability];
    if(check)
    {
        [CUSTOMINDICATORMACRO startLoadAnimation:self];
        [RestaurantMacro likeOrDislikeToServer:modal withCompletion:^(id obj1) {
            dispatch_async(dispatch_get_main_queue(), ^{
                ResturantModal *responseModal = nil;
                
                if([obj1 isKindOfClass:[ResturantModal class]])
                {
                    responseModal = obj1;
                    if([responseModal.result isEqualToString:@"success"])
                    {
                        
                        [tableView_Favourite reloadData];
                    }
                    if([checkcount isEqualToString:@"2"])
                    {
                    }
                }
                else
                {
                    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Please check your Internet Connection." preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction *okBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        [self dismissViewControllerAnimated:YES completion:nil];
                    }];
                    
                    [alertController addAction:okBtn];
                    [self presentViewController:alertController animated:YES completion:nil];
                }
                [CUSTOMINDICATORMACRO stopLoadAnimation:self];
            });
        }];
    }
}
-(void)tapOnSpecial:(UIButton*)sender{

    NSIndexPath *indexpath=[NSIndexPath indexPathForRow:sender.tag inSection:0];
    CGRect myRect = [tableView_Favourite rectForRowAtIndexPath:indexpath];
    CGRect rectInSuperview = [tableView_Favourite convertRect:myRect toView:[tableView_Favourite superview]];
    self.viewTool.selectedSegmentIndex = 0;
    [self changePositionOfPopupView:rectInSuperview];
    
    selectedResturant = [placeArray objectAtIndex:sender.tag];
    _lblpopupName.text=selectedResturant.resturantName;
    
    if ([selectedResturant.happy_hour_count intValue]+[selectedResturant.place_specials_count intValue]!=0) {
       // [self specialsHappyHoursServiceHelper];
        selectedRow=sender.tag;
    }
    

}

-(void)changePositionOfPopupView:(CGRect)rect{
    int x_position=rect.origin.y+rect.size.height-90;
    
    int popup_Y=self.view.frame.size.height-(self.tabBarController.tabBar.bounds.size.height+_popupView.frame.size.height+60);
    
    if (x_position>popup_Y)
        X_axis.constant=self.view.frame.size.height-(self.tabBarController.tabBar.bounds.size.height+_popupView.frame.size.height+60);
    else
        X_axis.constant=x_position;
}

- (IBAction)tapOnHappyHours:(id)sender {
    //[self colorChangeOnPressTab:@[_btnHppyHours,_btnDailySpecials]];
    arrySpecials = restModal_QuickView.arrayHappyHours;
    [self showHidePopup:arrySpecials.count];
}

- (IBAction)tapOnDailySpecials:(id)sender {
    //[self colorChangeOnPressTab:@[_btnDailySpecials,_btnHppyHours]];
     arrySpecials = restModal_QuickView.arraySpecials;
    [self showHidePopup:arrySpecials.count];
}


-(void)showHidePopup:(NSInteger)count{

    if (count==0) {
        _emptyView.hidden=NO;
        _tblPopup.hidden=YES;
        _btnExpand.hidden=YES;
        _btnReportHere.hidden=YES;
        _View_Empty.hidden = NO;
        return;
    }else{
        _View_Empty.hidden = YES;
        _tblPopup.hidden=NO;
        _emptyView.hidden=YES;
        _btnExpand.hidden=NO;
        _btnReportHere.hidden=NO;
    }
    [_tblPopup reloadData];
    
}

-(void)colorChangeOnPressTab:(NSArray*)arry{
    
    dispatch_async(dispatch_get_main_queue(), ^{
    
    for (btnNo=0; btnNo<arry.count; btnNo++) {
        if (btnNo==0) {
            
            UIButton *button =(UIButton*)arry[0];
            button.backgroundColor=[UIColor whiteColor];
            [button setTitleColor:[UIColor colorWithRed:54.0f/255.0f green:69.0f/255.0f blue:79.0f/255.0f alpha:1] forState:UIControlStateNormal];
        }else
        {
            UIButton *button =(UIButton*)arry[btnNo];
            
            button.backgroundColor=[UIColor colorWithRed:0.0f/255.0f green:204.0f/255.0f blue:255.0f/255.0f alpha:1];
            [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        }
    }
  });
}

-(void)serverError{
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Please check your Internet Connection." preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
    [alertController addAction:okBtn];
    [self presentViewController:alertController animated:YES completion:nil];
   
}

@end
