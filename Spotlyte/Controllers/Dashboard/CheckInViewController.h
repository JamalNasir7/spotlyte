//
//  DashboardDetailViewController.h
//  Spotlyte
//
//  Created by Admin on 02/08/17.
//  Copyright © 2016 Jigar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HMSegmentedControl.h"

@interface CheckInViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,EDStarRatingProtocol,MFMailComposeViewControllerDelegate,CLLocationManagerDelegate>
{
    IBOutlet UITableView *tableView_Favourite;
    IBOutlet UIButton *btnPlace,*btnPrmotions;
    IBOutlet UIView *view_NoValues;
    BOOL isCheckedTab;
    IBOutlet UIImageView *imageView_Indicator;
    EDStarRating    *starRatingView;
    IBOutlet UIView *alertVw;
    
    CLLocationManager *_locationManager;
    IBOutlet NSLayoutConstraint *X_axis;
}


@property (nonatomic, retain) NSMutableArray *placeArray;

//// Popup
@property (strong, nonatomic) IBOutlet UIView *popupViewBG;
    
@property (strong, nonatomic) IBOutlet UIView *popupView;
@property (strong, nonatomic) IBOutlet UILabel *lblpopupName;
@property (strong, nonatomic) IBOutlet UIButton *btnHppyHours;
@property (strong, nonatomic) IBOutlet UIButton *lblDailySpecial;

@property (strong, nonatomic) IBOutlet UITableView *tblPopup;
@property (strong, nonatomic) IBOutlet UIButton *btnCross;

@property (strong, nonatomic) IBOutlet UIButton *btnHappyHours;
@property (strong, nonatomic) IBOutlet UIButton *btnDailySpecials;
@property (strong, nonatomic) IBOutlet UIView *emptyView;
@property (strong, nonatomic) IBOutlet UIButton *btnReportHere;
@property (strong, nonatomic) IBOutlet UIButton *btnExpand;
@property (weak, nonatomic) IBOutlet UIView *View_Empty;

@property (weak, nonatomic) IBOutlet HMSegmentedControl *viewTool;
    
@end
