//
//  DashboardDetailViewController.m
//  Spotlyte
//
//  Created by Admin on 25/03/16.
//  Copyright © 2016 Naveen. All rights reserved.
//

#import "CommentViewController.h"
#import "HeaderView.h"
#import "FavouriteCell.h"
#import "DailySpecialCell.h"
#import "PromotionsViewController.h"
#import "TweetsCell.h"

@interface CommentViewController ()
{
    int btnNo;
    NSInteger selectedRow;
    NSArray *arrySpecials;
    
    BOOL tapped;
    BOOL locationBool;
    RegisterModal *registerModalResponeValue1;
    ResturantModal *restModal_QuickView;
    ResturantModal *selectedResturant;
}
@end

@implementation CommentViewController

@synthesize placeArray,personalTweetArray,registerModalOBJ;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    placeArray = [[NSMutableArray alloc]init];
    
    personalTweetArray = [[NSMutableArray alloc]init];
    HeaderView *header = [[HeaderView alloc]initWithTitle:@"REVIEWS" controller:self];
    [header addSubview:header.btnback];
    [self.view addSubview:header];
    
    tableView_Favourite.tableFooterView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, tableView_Favourite.frame.size.width, 0.1f)];
    
    _popupViewBG.hidden = YES;
    _popupView.hidden=YES;
    _btnCross.hidden=YES;
    _emptyView.hidden=YES;
    
    _popupView.layer.cornerRadius = 5;
    _popupView.layer.borderWidth = 1;
    _popupView.clipsToBounds = YES;
    _popupView.layer.borderColor = [[UIColor colorWithRed:153.0f/255.0f green:153.0f/255.0f blue:153.0f/255.0f alpha:1]CGColor];
    
    _btnExpand.layer.cornerRadius = 5;
    _btnExpand.layer.borderWidth = 1;
    _btnExpand.clipsToBounds = YES;
    _btnExpand.layer.borderColor = [[UIColor colorWithRed:18.0f/255.0f green:166.0f/255.0f blue:233.0f/255.0f alpha:1]CGColor];
    
    isCheckedTab = FALSE;
    tableView_Favourite.hidden = YES;
    [self initializeLocationManager];
    
    self.viewTool.sectionTitles = @[@"Happy Hours",@"Daily Specials"];
    self.viewTool.selectedSegmentIndex = 0;
    self.viewTool.backgroundColor = [UIColor whiteColor];
    self.viewTool.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor colorWithRed:153.0/255.0 green:153.0/255.0 blue:153.0/255.0 alpha:1.0],NSFontAttributeName: [UIFont systemFontOfSize:14.0]};
    self.viewTool.selectedTitleTextAttributes = @{NSForegroundColorAttributeName : [UIColor colorWithRed:0.0/255.0 green:158.0/255.0 blue:224.0/255.0 alpha:1.0],NSFontAttributeName: [UIFont systemFontOfSize:14.0]};
    self.viewTool.selectionIndicatorColor = [UIColor colorWithRed:0.0/255.0 green:158.0/255.0 blue:224.0/255.0 alpha:1.0];
    self.viewTool.selectionStyle = HMSegmentedControlSelectionStyleFullWidthStripe;
    self.viewTool.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    self.viewTool.selectionIndicatorHeight = 2.0;
    
    [self.viewTool setIndexChangeBlock:^(NSInteger index) {
        if (index == 0){
            arrySpecials = restModal_QuickView.arrayHappyHours;
            [self showHidePopup:arrySpecials.count];
        }else{
            arrySpecials = restModal_QuickView.arraySpecials;
            [self showHidePopup:arrySpecials.count];
        }
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    tableView_Favourite.rowHeight = UITableViewAutomaticDimension;
    tableView_Favourite.estimatedRowHeight = 30;

    registerModalOBJ = [[RegisterModal alloc]init];
    locationBool=YES;
    [self loadServiceGetProfile];
    [tableView_Favourite reloadData];
}

- (void)initializeLocationManager {
    if(![CLLocationManager locationServicesEnabled]) {
        showAlertController(@"Enable Location Service", @"You have to enable the Location Servic    e to use this App. To enable, please go to Settings->Privacy->Location Services", self, @"OK", nil,^(BOOL cancel, BOOL ok){
        });
        return;
    }
    
    if ([CLLocationManager authorizationStatus]==kCLAuthorizationStatusDenied){
        showAlertController(@"App Permission Denied", @"To re-enable, please go to Settings and turn on Location Service for this app.", self, @"OK", nil,^(BOOL cancel, BOOL ok){
        });
        return;
    }
    
    if (!_locationManager) {
        _locationManager = [[CLLocationManager alloc] init];
        
    }
    
    _locationManager.delegate = self;
    [_locationManager requestAlwaysAuthorization];
    _locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
    _locationManager.distanceFilter = kCLDistanceFilterNone;
    _locationManager.activityType = CLActivityTypeAutomotiveNavigation;
    [_locationManager startUpdatingLocation];
}


- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    APP_DELEGATE.userCurrentLocation=newLocation;
    
    if (APP_DELEGATE.userCurrentLocation.coordinate.latitude && APP_DELEGATE.userCurrentLocation.coordinate.longitude) {
        if (locationBool) {
          // [self commentsServiceHelper];
            [self loadServiceGetProfile];
            locationBool=NO;}
    }
    
    [_locationManager stopUpdatingLocation];
    _locationManager=nil;
}


#pragma mark -- Service Helper
-(void)commentsServiceHelper
{
    ResturantModal *modal       =   [[ResturantModal alloc]init];
    NSUserDefaults *defaults    =   [NSUserDefaults standardUserDefaults];
    NSData *data                =   [defaults objectForKey:@"logindetails"];
    
    RegisterModal * registerModalResponeValue   =   [NSKeyedUnarchiver unarchiveObjectWithData:data];
    NSString * user_IDRegister                  =   registerModalResponeValue.userID;
    
    BOOL check=[[AppDelegate shareAppdelegateInstance] checkReachability];
    if(check)
    {
        [CUSTOMINDICATORMACRO startLoadAnimation:self];
        [modal setUserID:user_IDRegister];
        
        [RestaurantMacro commentsToServer:modal withCompletion:^(id obj1) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if (obj1!=nil && ![obj1 isKindOfClass:[NSNull class]]) {
                    if([obj1 isKindOfClass:[ResturantModal class]])
                    {
                        ResturantModal *response = obj1;
                        placeArray = response.arrayPlaceFavourites;
                        
                        if(placeArray.count == 0)
                        {
                            view_NoValues.hidden = NO;
                            _View_Empty.hidden = NO;

                            tableView_Favourite.hidden=YES;
                        }
                        else
                        {
                            view_NoValues.hidden = YES;
                            _View_Empty.hidden = YES;
                            tableView_Favourite.hidden = NO;
                            [tableView_Favourite reloadData];
                        }
                        
                    }
                    else
                    {
                        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Please check your connection" preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction *okBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                            [self dismissViewControllerAnimated:YES completion:nil];
                        }];
                        
                        [alertController addAction:okBtn];
                        [self presentViewController:alertController animated:YES completion:nil];
                    }
                    
                }else
                view_NoValues.hidden = NO;
                [CUSTOMINDICATORMACRO stopLoadAnimation:self];
            });
        }];
    }
    
}
-(void)loadServiceGetProfile
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [defaults objectForKey:@"logindetails"];
    registerModalResponeValue1 = [NSKeyedUnarchiver unarchiveObjectWithData:data];

    BOOL check=[[AppDelegate shareAppdelegateInstance] checkReachability];
    if(check)
    {
        [CUSTOMINDICATORMACRO startLoadAnimation:self];
        [REGISTERMACRO viewAccountToServer:registerModalResponeValue1.userID withCompletion:^(id obj1) {
            NSLog(@"View Account ===  %@",obj1);
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [CUSTOMINDICATORMACRO stopLoadAnimation:self];
                
                if([obj1 isKindOfClass:[RegisterModal class]])
                {
                    registerModalOBJ = obj1;
                    if([registerModalOBJ.result isEqualToString:@"success"])
                    {
                        personalTweetArray = [registerModalOBJ.UsersCommentsArray mutableCopy];
                        
                        if (personalTweetArray.count > 0)
                        {
                            tableView_Favourite.hidden = NO;
                            [tableView_Favourite reloadData];
                            view_NoValues.hidden = YES;
                            _View_Empty.hidden = YES;
                        }
                        else
                        {
                            view_NoValues.hidden = NO;
                            _View_Empty.hidden = NO;
                        }

                    }
                    
                }
                else
                {
                    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Please check your Internet Connection." preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction *okBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        [self dismissViewControllerAnimated:YES completion:nil];
                    }];
                    [alertController addAction:okBtn];
                    [self presentViewController:alertController animated:YES completion:nil];
                }
            });
        }];
        
    }
    
}
-(void)specialsHappyHoursServiceHelper
{
    BOOL check=[[AppDelegate shareAppdelegateInstance] checkReachability];
    if(check){
        [CUSTOMINDICATORMACRO startLoadAnimation:self];
        [RestaurantMacro getSpecialsHappyHoursToServer:selectedResturant withCompletion:^(id obj1) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if([obj1 isKindOfClass:[ResturantModal class]]){
                    restModal_QuickView  = obj1;
                    if (restModal_QuickView.arrayHappyHours.count>0 || restModal_QuickView.arraySpecials.count>0) {
                        
                        [_tblPopup setContentOffset:CGPointZero animated:NO];
                        [self tapOnHappyHours:nil];
                        _popupViewBG.hidden = NO;
                        _popupView.hidden=NO;
                        _btnCross.hidden=NO;
                        alertVw.hidden=NO;
                    }
                }
                else{
                    _popupViewBG.hidden = NO;
                    _popupView.hidden =NO;
                    _btnCross.hidden  =NO;
                    [self serverError];
                }
                [CUSTOMINDICATORMACRO stopLoadAnimation:self];
            });
        }];
    }
}

#pragma mark -- TableView Delegate Method

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([tableView isEqual:_tblPopup ]){
        return arrySpecials.count;
    }
    else{
        return personalTweetArray.count;
    }

}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if ([tableView isEqual:_tblPopup]) {
        if (tableView == _tblPopup)
        {
            
            static NSString *cellIdentifier = @"sampleidentifier";
            DailySpecialCell *cell = (DailySpecialCell *)[_tblPopup dequeueReusableCellWithIdentifier:cellIdentifier];
            
            if(cell == nil)
            {
                cell = [[[NSBundle mainBundle]loadNibNamed:@"DailySpecialCell" owner:self options:nil]objectAtIndex:0];
            }
            
            if (arrySpecials.count>0) {
                ResturantModal *modelDailySpecialDetails = [arrySpecials objectAtIndex:indexPath.row];
                _btnExpand.tag=indexPath.row;
                _btnReportHere.tag=indexPath.row;
                
                NSString *mystr;
                
                if([modelDailySpecialDetails.specials_weekDay isEqualToString:@"monday"])
                {
                    mystr=@"Monday";
                }
                else if ([modelDailySpecialDetails.specials_weekDay isEqualToString:@"tuesday"])
                {
                    mystr=@"Tuesday";
                }
                else if ([modelDailySpecialDetails.specials_weekDay isEqualToString:@"wednesday"])
                {
                    mystr=@"Wednesday";
                }
                else if ([modelDailySpecialDetails.specials_weekDay isEqualToString:@"thursday"])
                {
                    mystr=@"Thursday";
                }
                else if ([modelDailySpecialDetails.specials_weekDay isEqualToString:@"friday"])
                {
                    mystr=@"Friday";
                }
                else if ([modelDailySpecialDetails.specials_weekDay isEqualToString:@"saturday"])
                {
                    mystr=@"Saturday";
                }
                else
                {
                    mystr=@"Sunday";
                }
                
                
                
                NSString *trimmedString = [modelDailySpecialDetails.title stringByTrimmingCharactersInSet:
                                           [NSCharacterSet whitespaceCharacterSet]];
                
                NSString *stringStartTime;
                
                NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
                NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
                [dateFormatter1 setLocale:locale];

                dateFormatter1.dateFormat = @"HH:mm:ss";
                
                NSDate *date1 = [dateFormatter1 dateFromString:modelDailySpecialDetails.startTime];
                
                
                
                dateFormatter1.dateFormat = @"hh:mm a";
                
                stringStartTime = [dateFormatter1 stringFromDate:date1];
                
                stringStartTime = [stringStartTime stringByReplacingOccurrencesOfString:@":00" withString:@""];
                
                
                int stringStartTimeInt=[stringStartTime intValue];
                
                if (stringStartTimeInt<10)
                {
                    NSRange range = NSMakeRange(0,1);
                    
                    stringStartTime = [stringStartTime stringByReplacingCharactersInRange:range withString:@""];
                }
                else
                {
                    stringStartTime = [stringStartTime stringByReplacingOccurrencesOfString:@":00" withString:@""];
                }
                
                
                NSString *stringCloseTime;
                
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                NSLocale *locale2 = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
                [dateFormatter setLocale:locale2];

                dateFormatter.dateFormat = @"HH:mm:ss";
                
                NSDate *date = [dateFormatter dateFromString:modelDailySpecialDetails.endTime];
                
                
                
                dateFormatter.dateFormat = @"hh:mm a";
                
                stringCloseTime = [dateFormatter stringFromDate:date];
                
                stringCloseTime = [stringCloseTime stringByReplacingOccurrencesOfString:@":00" withString:@""];
                
                
                
                int stringCloseTimeInt=[stringCloseTime intValue];
                
                if (stringCloseTimeInt<10)
                {
                    NSRange range = NSMakeRange(0,1);
                    
                    stringCloseTime = [stringCloseTime stringByReplacingCharactersInRange:range withString:@""];
                }
                else
                {
                    stringCloseTime = [stringCloseTime stringByReplacingOccurrencesOfString:@":00" withString:@""];
                }
                
                
                
                if ([modelDailySpecialDetails.price isEqualToString:@"N/A"] || [modelDailySpecialDetails.price isEqualToString:@"1/2 Off"] || [modelDailySpecialDetails.price isEqualToString:@"Other"] || [modelDailySpecialDetails.price isEqualToString:@"FREE"])
                {
                    
                    cell.lblDays.text=mystr;
                    NSString *completeString = [NSString stringWithFormat:@"%@ %@ (%@-%@)",modelDailySpecialDetails.price,trimmedString,stringStartTime,stringCloseTime];
                    NSMutableAttributedString *AttributedString = [[NSMutableAttributedString alloc] initWithString:completeString];
                    NSString *boldString =modelDailySpecialDetails.price;
                    NSRange boldRange = [completeString rangeOfString:boldString];
                    [AttributedString addAttribute: NSFontAttributeName value:CalibriBold(11.0f) range:boldRange];
                    [cell.lblDescription setAttributedText: AttributedString];
                    
                }
                else
                {
                    cell.lblDays.text=mystr;
                    NSString *price;
                    if ([modelDailySpecialDetails.price rangeOfString:@"$"].location == NSNotFound) {
                        price = [NSString stringWithFormat:@"$%@",modelDailySpecialDetails.price];
                    }else
                    {
                        price = [NSString stringWithFormat:@"%@",modelDailySpecialDetails.price];
                    }
                    
                    cell.lblDescription.text=[NSString stringWithFormat:@"%@ %@ (%@-%@)",price, trimmedString,stringStartTime,stringCloseTime];

                    
                }
                
                if(modelDailySpecialDetails.isCheckedFavSpecial == NO)
                {
                    cell.btnFavSpecial.selected = false;
                }
                else{
                    cell.btnFavSpecial.selected = true;
                }
                
                [cell.btnFavSpecial addTarget:self action:@selector(btnFavSpecialClick:) forControlEvents:UIControlEventTouchUpInside];
                cell.btnFavSpecial.tag = indexPath.row;
                
                cell.textLabel.numberOfLines = 2;
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                cell.viewSeperator.backgroundColor = [UIColor colorWithRed:153.0f/255.0f green:153.0f/255.0f blue:153.0f/255.0f alpha:1];
                
            }
            
            
        return cell;
            
        }

    }
    else{
        
        static NSString *cellIdentifier=@"tweetCell";
        TweetsCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (cell == nil) {
            cell = [[TweetsCell alloc]
                    initWithStyle:UITableViewCellStyleDefault
                    reuseIdentifier:cellIdentifier];
        }
        if(personalTweetArray.count>0)
        {
            RegisterModal *modal = [personalTweetArray objectAtIndex:indexPath.row];
            /* if(registerModalOBJ.userName != nil && registerModalOBJ.userName.length != 0 )
             {
             NSString *combinedStr = [NSString stringWithFormat:@"%@ %@ @%@",registerModalOBJ.firstName,registerModalOBJ.lastName,registerModalOBJ.userName];
             [cell.userNameLabel setAttributedText:[self getAttrStrfromTheStr:combinedStr]];
             }*/
            cell.userNameLabel.text=modal.placeName;
            
            cell.asynch_ProfileImage.layer.cornerRadius = 3.0;
            [cell.asynch_ProfileImage setBackgroundColor:[UIColor blackColor]];
            cell.asynch_ProfileImage.layer.masksToBounds = YES;
            dispatch_async(dispatch_get_main_queue(), ^{
                NSURL *url = [[NSBundle mainBundle] URLForResource:@"loading-1" withExtension:@"gif"];
                NSString *newUrl = [modal.placeImage stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
                [cell.asynch_ProfileImage sd_setImageWithURL:[NSURL URLWithString:newUrl]
                                            placeholderImage:[UIImage imageNamed:@"noimage.png"]];
            });
            cell.lblTweetMessage.text = modal.comments;
            //cell.lblTweetTiming.text = modal.createdOn;
            cell.lblRating.hidden = NO;
            if ([modal.rating isEqualToString:@"0"])
            {
                cell.lblRating.text = @"N/A";
            }
            else{
                cell.lblRating.text = modal.rating;
            }
            
            NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
            [dateFormat setLocale:locale];

            [dateFormat setDateFormat:@"MM-dd-yyyy hh:mm a"];
            
            NSString *strCreatedDate = modal.createdOn;
            NSDate *dateCreate = [dateFormat dateFromString:strCreatedDate];
            
            [dateFormat setDateFormat:@"MM-dd-yyyy"];
            NSString *strStartDate = [dateFormat stringFromDate:dateCreate];
            cell.lblDate.text = strStartDate;
            
            [dateFormat setDateFormat:@"hh:mm a"];
            NSString *strTime = [dateFormat stringFromDate:dateCreate];
            cell.lblTweetTiming.text = strTime;
        }
        return cell;
    }
    return nil;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    HomeViewController *homeVC = [self.storyboard instantiateViewControllerWithIdentifier:@"homescreen"];
//    
//    NSMutableArray *array = [[NSMutableArray alloc]init];
//    ResturantModal *modal = [placeArray objectAtIndex:indexPath.row];
//    
//    [array addObject:modal];
//    homeVC.restListArray = array;
//    homeVC.selectedRestValue = indexPath.row;
//    homeVC.checkVC = @"restlist";
//    [self.navigationController pushViewController:homeVC animated:YES];
    
//    selectedResturant = [personalTweetArray objectAtIndex:indexPath.row];
//    PromotionsViewController *promtionVC = [self.storyboard instantiateViewControllerWithIdentifier:@"promtions"];
//    promtionVC.selectedValue = indexPath.row;
//    promtionVC.placeIDString = selectedResturant.placeID;
//    promtionVC.promtionsArray = personalTweetArray;
//    promtionVC.selectedWhichController = @"1";
//    
//    BOOL isCheckedRandomSearch = [[NSUserDefaults standardUserDefaults]boolForKey:@"randomsearch"];
//    if(isCheckedRandomSearch == TRUE)
//    {
//        promtionVC.checkTextOrSearch = @"customsearch";
//    }
//    else
//    {
//        promtionVC.checkTextOrSearch = @"nosearch";
//        [[NSUserDefaults standardUserDefaults]setBool:FALSE forKey:@"randomsearch"];
//        
//    }
//    promtionVC.selectHomeVC=^(ResturantModal *restModel){
//        HomeViewController *homeVC = [self.storyboard instantiateViewControllerWithIdentifier:@"homescreen"];
//        homeVC.promotionModalOBJ = restModel;
//    };
//    [self.navigationController pushViewController:promtionVC animated:YES];

}



-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([tableView isEqual:_tblPopup])
        return 44;
    else
       return UITableViewAutomaticDimension;
}

- (void)btnFavSpecialClick:(id)sender {
    UIButton *btn = (UIButton*)sender;
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:btn.tag inSection:0];
    
    DailySpecialCell *cell = (DailySpecialCell *)[_tblPopup cellForRowAtIndexPath:indexPath];
    
    ResturantModal *modelDailySpecialDetails = [arrySpecials objectAtIndex:btn.tag];
    
    NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:@"logindetails"];
    RegisterModal *registerModalResponeValue = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    NSString *strType;
    if(self.viewTool.selectedSegmentIndex == 0){
        strType = @"h";
    }
    else{
        strType = @"s";
    }
    
    NSString *strFav;
    if(btn.selected == true){
        strFav = @"2";
    }
    else{
        strFav = @"1";
    }
    
    BOOL check=[[AppDelegate shareAppdelegateInstance] checkReachability];
    if(check)
    {
        [modelDailySpecialDetails setUserID:registerModalResponeValue.userID];
        [CUSTOMINDICATORMACRO startLoadAnimation:self];
        [RestaurantMacro favUnfavSpecialToServer:modelDailySpecialDetails :strFav :strType withCompletion:^(id obj1) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                ResturantModal *responseModal = nil;
                if([obj1 isKindOfClass:[ResturantModal class]])
                {
                    responseModal = obj1;
                    if([responseModal.result isEqualToString:@"success"]){
                        if(btn.selected == true){
                            [modelDailySpecialDetails setIsCheckedFavSpecial:NO];
                        }
                        else{
                            [modelDailySpecialDetails setIsCheckedFavSpecial:YES];
                        }
                        
                        //                        [_tblPopup beginUpdates];
                        //                        [_tblPopup reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:false];
                        //                        [_tblPopup endUpdates];
                        if(btn.selected == true){
                            [cell.btnFavSpecial setSelected:false];
                        }
                        else{
                            [cell.btnFavSpecial setSelected:true];
                        }
                    }
                }
                else
                [self serverError];
                
                [CUSTOMINDICATORMACRO stopLoadAnimation:self];
            });
        }];
    }
}
    
- (void)displayStar :(NSString *)ratingVal
{
    if(ratingVal.floatValue <= 2.0)
    {
        starRatingView.starHighlightedImage = [UIImage imageNamed:@"RedStarIcon"];
    }
    else if(ratingVal.floatValue <= 3.9)
    {
        starRatingView.starHighlightedImage = [UIImage imageNamed:@"YellowStarIcon"];
    }
    else
    {
        starRatingView.starHighlightedImage = [UIImage imageNamed:@"GreenStarIcon"];
    }
    
    starRatingView.starImage = [UIImage imageNamed:@"UnStarIcon"];
    starRatingView.maxRating = 5.0;
    starRatingView.delegate = self;
    starRatingView.horizontalMargin = 10;
    starRatingView.editable = NO;
    starRatingView.rating = ratingVal.floatValue;
    starRatingView.displayMode = EDStarRatingDisplayAccurate;
}


#pragma mark --- Action Methods

- (IBAction)tapOnCrossBtn:(id)sender {
    _popupViewBG.hidden = YES;
    _popupView.hidden=YES;
    _btnCross.hidden=YES;
       alertVw.hidden=YES;
}



-(IBAction)pressFavouriteBtn:(UIButton*)sender
{
    [self placeFavorite:sender];
}

-(void)placeFavorite:(UIButton*)sender{

    UIButton *btnFavourite=sender;
    
    ResturantModal *modal;
    NSArray *filteredArray;
    
    modal = [placeArray objectAtIndex:sender.tag];
    filteredArray  = [placeArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"placeID ==%@",modal.placeID]];
    
    if(filteredArray>0)
    {
        modal = [filteredArray firstObject];
    }
    
    NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:@"logindetails"];
    RegisterModal *registerModalResponeValue = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    if(modal.isCheckedFavourite == NO)
    {
        BOOL check=[[AppDelegate shareAppdelegateInstance] checkReachability];
        if(check)
        {
            [modal setUserID:registerModalResponeValue.userID];
            [CUSTOMINDICATORMACRO startLoadAnimation:self];
            [RestaurantMacro favouriteToServer:modal withCompletion:^(id obj1) {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    ResturantModal *responseModal = nil;
                    if([obj1 isKindOfClass:[ResturantModal class]])
                    {
                        responseModal = obj1;
                        if([responseModal.result isEqualToString:@"success"]){
                            [btnFavourite setImage:[UIImage imageNamed:@"FavSelLIst" ] forState:UIControlStateNormal];
                            
                            [modal setIsCheckedFavourite:YES];
                        }
                    }
                    else
                        [self serverError];
                    
                    [CUSTOMINDICATORMACRO stopLoadAnimation:self];
                });
            }];
        }
    }
    else
    {
        [modal setUserID:registerModalResponeValue.userID];
        
        BOOL check=[[AppDelegate shareAppdelegateInstance] checkReachability];
        if(check)
        {
            [CUSTOMINDICATORMACRO startLoadAnimation:self];
            [RestaurantMacro unFavouriteToServer:modal withCompletion:^(id obj1) {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [CUSTOMINDICATORMACRO stopLoadAnimation:self];
                    ResturantModal *responseModal =  nil;
                    if([obj1 isKindOfClass:[ResturantModal class]])
                    {
                        responseModal = obj1;
                        if([responseModal.result isEqualToString:@"success"])
                        {
                            [btnFavourite setImage:[UIImage imageNamed:@"FavUnSelLIst" ] forState:UIControlStateNormal];
                            [modal setIsCheckedFavourite:NO];
                        }
                    }
                    else
                        [self serverError];
                });
            }];
        }
        
    }


}

-(void)favouriteServiceHelper :(PromtionsModal *)modalObj andwith:(UIButton*)sender
{
    BOOL check=[[AppDelegate shareAppdelegateInstance] checkReachability];
    if(check)
    {
        [CUSTOMINDICATORMACRO startLoadAnimation:self];
        [PromotionMacro addFavouriteSpotlyteToServer:modalObj withCompletion:^(id obj1) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [CUSTOMINDICATORMACRO stopLoadAnimation:self];
                if([obj1 isKindOfClass:[ResturantModal class]])
                {
                    ResturantModal *responseModal = nil;
                    responseModal = obj1;
                    if([responseModal.result isEqualToString:@"success"])
                    {
                        [sender setImage:[UIImage imageNamed:@"FavSelLIst"] forState:UIControlStateNormal];
                        [modalObj setPromotionFavStatus:@"Yes"];
                    }
                }
                else
                    [self serverError];
            });
        }];
    }
}

-(void)unFavouriteServiceHelper :(PromtionsModal *)modalObj andwith:(UIButton*)sender
{
    BOOL check=[[AppDelegate shareAppdelegateInstance] checkReachability];
    if(check)
    {
        [CUSTOMINDICATORMACRO startLoadAnimation:self];
        [PromotionMacro unFavouriteSpotlyteToServer:modalObj withCompletion:^(id obj1) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [CUSTOMINDICATORMACRO stopLoadAnimation:self];
                if([obj1 isKindOfClass:[ResturantModal class]])
                {
                    ResturantModal *responseModal;
                    responseModal = obj1;
                    if([responseModal.result isEqualToString:@"success"])
                    {
                        [sender setImage:[UIImage imageNamed:@"FavUnSelLIst"] forState:UIControlStateNormal];
                        [modalObj setPromotionFavStatus:@"No"];
                    }
                }
                else
                    [self serverError];
            });
        }];
    }
    
}

- (IBAction)pressNavigateSpecialHours:(UIButton*)sender
{
        
    PromotionsViewController *promtionVC = [self.storyboard instantiateViewControllerWithIdentifier:@"promtions"];
    promtionVC.selectedValue = sender.tag;
    promtionVC.placeIDString = selectedResturant.placeID;
    promtionVC.promtionsArray = placeArray;
    promtionVC.selectedWhichController = @"specialshappyhours";
    
    BOOL isCheckedRandomSearch = [[NSUserDefaults standardUserDefaults]boolForKey:@"randomsearch"];
    if(isCheckedRandomSearch == TRUE)
    {
        promtionVC.checkTextOrSearch = @"customsearch";
    }
    else
    {
        promtionVC.checkTextOrSearch = @"nosearch";
        [[NSUserDefaults standardUserDefaults]setBool:FALSE forKey:@"randomsearch"];
        
    }
    [self.navigationController pushViewController:promtionVC animated:YES];
    
    /*promtionVC.selectHomeVC=^(ResturantModal *restModel){
     
     promotionModalOBJ = restModel;
     };*/
}

-(IBAction)pressReportHereBtn : (UIButton*)sender
{
   UIAlertController *alertController = [UIAlertController alertControllerWithTitle:selectedResturant.resturantName message:@"Specials and Hours are subject to change. To ensure the most up-to-date specials you can view this locations website specials here" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *YesBtn = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                             {
                                 
                                 
                                     
                                     //[selectedResturant setWebSite_Specials:modal.webSite_Specials];
                                     if(selectedResturant.webSite_Specials.length>0)
                                     {
                                         TermsViewController *termsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"termsvc"];
                                         termsVC.checkVC = @"promotions";
                                         termsVC.checkBack = @"homevc";
                                         termsVC.CheckUrlType=@"SpecialLink";
                                         termsVC.restModal_TermsVC = selectedResturant;
                                         [self.navigationController pushViewController:termsVC animated:YES];
                                     }
                                     else
                                     {
                                         
                                         TermsViewController *termsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"termsvc"];
                                         termsVC.checkVC = @"promotions";
                                         termsVC.checkBack = @"homevc";
                                         termsVC.CheckUrlType=@"ResturantEMail";
                                         termsVC.restModal_TermsVC = selectedResturant;
                                         [self.navigationController pushViewController:termsVC animated:YES];
                                     }
                                     
                                 
                                 
                             }];
    
    UIAlertAction *NoBtn = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                            {
                                [self dismissViewControllerAnimated:YES completion:nil];
                            }];
    
    [alertController addAction:YesBtn];
    [alertController addAction:NoBtn];
    
    [self presentViewController:alertController animated:YES completion:nil];
    
}




- (IBAction)reportAction:(id)sender {
    NSString *emailTitle = selectedResturant.resturantName.uppercaseString;
    // Email Content
    NSString *messageBody = selectedResturant.resturantDescription;
    // To address
    NSArray *toRecipents = [NSArray arrayWithObject:@"contact@spotlytespecials.com"];
    
    MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
    if ([MFMailComposeViewController canSendMail]) {
        mc.mailComposeDelegate = self;
        [mc setSubject:emailTitle];
        [mc setMessageBody:messageBody isHTML:NO];
        [mc setToRecipients:toRecipents];
        
        // Present mail view controller on screen
        [self presentViewController:mc animated:YES completion:NULL];
    }
    
}


- (void)mailComposeController:(MFMailComposeViewController*)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError*)error;
{
 
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            [self dismissViewControllerAnimated:YES completion:NULL];
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            [self dismissViewControllerAnimated:YES completion:NULL];
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            [self dismissViewControllerAnimated:YES completion:NULL];
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            [self dismissViewControllerAnimated:YES completion:NULL];
            break;
        default:
            break;
    }
    // Close the Mail Interface
}


-(IBAction)pressBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)tapOnLike:(UIButton*)sender{
    
    ResturantModal *modal = [[ResturantModal alloc]init];
    
    modal = [placeArray objectAtIndex:sender.tag];
    NSInteger  strLike = [modal.likecount integerValue];
    if ([modal.likeORDislike isEqualToString:@"2"] || [modal.likeORDislike isEqualToString:@""]) {
        [modal.likeORDislike isEqualToString:@"1"];
        [modal setLikeORDislike:@"1"];
        modal.likecount = [NSString stringWithFormat:@"%ld",(long)strLike+1];
    }
    else{
        [modal.likeORDislike isEqualToString:@"2"];
        [modal setLikeORDislike:@"2"];
        if (strLike > 0)
        {
            modal.likecount = [NSString stringWithFormat:@"%ld",(long)strLike-1];
        }
    }
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [defaults objectForKey:@"logindetails"];
    RegisterModal *registerModalResponeValue = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    NSString * user_IDRegister = registerModalResponeValue.userID;
    [modal setUserID:user_IDRegister];
    NSString *checkcount  = modal.likeORDislike;
    BOOL check=[[AppDelegate shareAppdelegateInstance] checkReachability];
    if(check)
    {
        [CUSTOMINDICATORMACRO startLoadAnimation:self];
        [RestaurantMacro likeOrDislikeToServer:modal withCompletion:^(id obj1) {
            dispatch_async(dispatch_get_main_queue(), ^{
                ResturantModal *responseModal = nil;
                
                if([obj1 isKindOfClass:[ResturantModal class]])
                {
                    responseModal = obj1;
                    if([responseModal.result isEqualToString:@"success"])
                    {
                        
                        [tableView_Favourite reloadData];
                    }
                    if([checkcount isEqualToString:@"2"])
                    {
                    }
                }
                else
                {
                    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Please check your Internet Connection." preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction *okBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        [self dismissViewControllerAnimated:YES completion:nil];
                    }];
                    
                    [alertController addAction:okBtn];
                    [self presentViewController:alertController animated:YES completion:nil];
                }
                [CUSTOMINDICATORMACRO stopLoadAnimation:self];
            });
        }];
        
    }
}
-(void)tapOnSpecial:(UIButton*)sender{

    NSIndexPath *indexpath=[NSIndexPath indexPathForRow:sender.tag inSection:0];
    CGRect myRect = [tableView_Favourite rectForRowAtIndexPath:indexpath];
    CGRect rectInSuperview = [tableView_Favourite convertRect:myRect toView:[tableView_Favourite superview]];
    self.viewTool.selectedSegmentIndex = 0;
    [self changePositionOfPopupView:rectInSuperview];
    
    selectedResturant = [placeArray objectAtIndex:sender.tag];
    _lblpopupName.text=selectedResturant.resturantName;
    
    if ([selectedResturant.happy_hour_count intValue]+[selectedResturant.place_specials_count intValue]!=0) {
        [self specialsHappyHoursServiceHelper];
        selectedRow=sender.tag;
    }
    

}

-(void)changePositionOfPopupView:(CGRect)rect{
    int x_position=rect.origin.y+rect.size.height-90;
    
    int popup_Y=self.view.frame.size.height-(self.tabBarController.tabBar.bounds.size.height+_popupView.frame.size.height+60);
    
    if (x_position>popup_Y)
        X_axis.constant=self.view.frame.size.height-(self.tabBarController.tabBar.bounds.size.height+_popupView.frame.size.height+60);
    else
        X_axis.constant=x_position;
}

- (IBAction)tapOnHappyHours:(id)sender {
    //[self colorChangeOnPressTab:@[_btnHppyHours,_btnDailySpecials]];
    arrySpecials = restModal_QuickView.arrayHappyHours;
    [self showHidePopup:arrySpecials.count];
}

- (IBAction)tapOnDailySpecials:(id)sender {
    //[self colorChangeOnPressTab:@[_btnDailySpecials,_btnHppyHours]];
     arrySpecials = restModal_QuickView.arraySpecials;
    [self showHidePopup:arrySpecials.count];
}


-(void)showHidePopup:(NSInteger)count{

    if (count==0) {
        _emptyView.hidden=NO;
        _tblPopup.hidden=YES;
        _btnExpand.hidden=YES;
        _btnReportHere.hidden=YES;
        
        return;
    }else{
        _tblPopup.hidden=NO;
        _emptyView.hidden=YES;
        _btnExpand.hidden=NO;
        _btnReportHere.hidden=NO;
    }
    [_tblPopup reloadData];
    
}


-(void)colorChangeOnPressTab:(NSArray*)arry{
    
    dispatch_async(dispatch_get_main_queue(), ^{
    
    for (btnNo=0; btnNo<arry.count; btnNo++) {
        if (btnNo==0) {
            
            UIButton *button =(UIButton*)arry[0];
            button.backgroundColor=[UIColor colorWithRed:0.0f/255.0f green:204.0f/255.0f blue:255.0f/255.0f alpha:1];
            [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        }else
        {
            UIButton *button =(UIButton*)arry[btnNo];
            button.backgroundColor=[UIColor whiteColor];
            [button setTitleColor:[UIColor colorWithRed:54.0f/255.0f green:69.0f/255.0f blue:79.0f/255.0f alpha:1] forState:UIControlStateNormal];
        }
    }
  });
}


-(void)serverError{
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Please check your Internet Connection." preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
    [alertController addAction:okBtn];
    [self presentViewController:alertController animated:YES completion:nil];
   
}

@end

