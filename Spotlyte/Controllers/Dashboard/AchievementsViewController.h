//
//  AchievementsViewController.h
//  Spotlyte
//
//  Created by Admin on 19/04/16.
//  Copyright © 2016 Naveen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AchievementsViewController : UIViewController<UICollectionViewDataSource,UICollectionViewDelegate>
{
    IBOutlet UICollectionView *collectionView_Progress;
    IBOutlet UIView *view_Empty;
}

@property (nonatomic, retain) NSMutableArray *responseArray;
@end
