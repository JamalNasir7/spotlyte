//
//  ReviewViewController.h
//  Spotlyte
//
//  Created by Admin on 25/03/16.
//  Copyright © 2016 Naveen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReviewViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,EDStarRatingProtocol>
{
    IBOutlet UILabel *lbl_TopBar;
    IBOutlet UITableView  *tableView_Review;
     EDStarRating    *starRatingView;
    IBOutlet UIView *view_noValues;
}
@property (nonatomic, retain) NSString *checkWhichReview;
@property (strong, nonatomic) NSArray *hotelNameArray;
@property (strong, nonatomic) NSArray *hotelImageArray;
@property (strong, nonatomic) NSMutableArray *reviewResponseArray;

@end
