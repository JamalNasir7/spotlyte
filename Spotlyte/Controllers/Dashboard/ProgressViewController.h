//
//  ProgressViewController.h
//  Spotlyte
//
//  Created by Admin on 16/03/16.
//  Copyright © 2016 Naveen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBCircularProgressBarView.h"

@interface ProgressViewController : UIViewController<UICollectionViewDataSource,UICollectionViewDelegate>
{
    IBOutlet UICollectionView *collectionView_Progress;
    IBOutlet UILabel *userProgressLevel;
    IBOutlet UILabel *userProgressLevelName;
    IBOutlet MBCircularProgressBarView *userLevelProgressBar;
    IBOutlet UIImageView *levelImage;
    IBOutlet NSLayoutConstraint *image_X;

}

@end
