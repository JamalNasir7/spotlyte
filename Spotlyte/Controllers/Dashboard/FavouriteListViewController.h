//
//  FavouriteListViewController.h
//  Spotlyte
//
//  Created by Admin on 25/03/16.
//  Copyright © 2016 Naveen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HMSegmentedControl.h"

@interface FavouriteListViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,EDStarRatingProtocol,MFMailComposeViewControllerDelegate,CLLocationManagerDelegate>
{
    IBOutlet UITableView *tableView_Favourite;
    IBOutlet UIButton *btnPlace,*btnPrmotions;
    IBOutlet UIView *view_NoValues;
    BOOL isCheckedTab;
    IBOutlet UIImageView *imageView_Indicator;
    EDStarRating    *starRatingView;
    IBOutlet UIView *alertVw;
      CLLocationManager *_locationManager;
    IBOutlet NSLayoutConstraint *X_axis;

}
@property (nonatomic, retain) NSMutableArray *favListArray;

@property (nonatomic, retain) NSMutableArray *promotionsArray;
@property (nonatomic, retain) NSMutableArray *placeArray;
@property (weak, nonatomic) IBOutlet UIView *View_Empty;

//// Popup

@property (strong, nonatomic) IBOutlet UIView *popupViewBG;
    
@property (strong, nonatomic) IBOutlet UIView *popupView;
@property (strong, nonatomic) IBOutlet UILabel *lblpopupName;
@property (strong, nonatomic) IBOutlet UIButton *btnHppyHours;
@property (strong, nonatomic) IBOutlet UIButton *lblDailySpecial;

@property (strong, nonatomic) IBOutlet UITableView *tblPopup;
@property (strong, nonatomic) IBOutlet UIButton *btnCross;

@property (strong, nonatomic) IBOutlet UIButton *btnHappyHours;
@property (strong, nonatomic) IBOutlet UIButton *btnDailySpecials;
@property (strong, nonatomic) IBOutlet UIView *emptyView;
@property (strong, nonatomic) IBOutlet UIButton *btnReportHere;
@property (strong, nonatomic) IBOutlet UIButton *btnExpand;


-(IBAction)pressTabBtn:(id)sender;
    
@property (weak, nonatomic) IBOutlet HMSegmentedControl *viewTool;
@property (weak, nonatomic) IBOutlet HMSegmentedControl *view_Tool_Popup;

@end
