//
//  AchievementsViewController.m
//  Spotlyte
//
//  Created by Admin on 19/04/16.
//  Copyright © 2016 Naveen. All rights reserved.
//

#import "AchievementsViewController.h"
#import "ReviewCollectionCell.h"

@implementation AchievementsViewController
@synthesize responseArray;
- (void)viewDidLoad {
    [super viewDidLoad];
    responseArray = [[NSMutableArray alloc]init];
    [collectionView_Progress registerNib:[UINib nibWithNibName:@"ReviewCollectionCell" bundle:nil]
              forCellWithReuseIdentifier:@"cell123"];
    [self userProgressAndLevelsMethod];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark -- Service Helper

-(void)userProgressAndLevelsMethod
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [defaults objectForKey:@"logindetails"];
    RegisterModal * registerModalResponeValue = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    NSString *user_IDRegister = registerModalResponeValue.userID;
    ResturantModal *modal = [[ResturantModal alloc]init];
    [modal setUserID:user_IDRegister];
    
    BOOL check=[[AppDelegate shareAppdelegateInstance] checkReachability];
    
    if(check)
    {
        [CUSTOMINDICATORMACRO startLoadAnimation:self];
        
        [RestaurantMacro retriveUserLevels:modal withCompletion:^(id obj1) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [CUSTOMINDICATORMACRO stopLoadAnimation:self];
                
                if([obj1 isKindOfClass:[UserLevelProgressModal class]])
                {
                    UserLevelProgressModal *userModel = obj1;
                    responseArray = userModel.arrayAchievements;
                    if(userModel.arrayAchievements.count == 0)
                    {
                        view_Empty.hidden = NO;
                    }
                    else
                    {
                        view_Empty.hidden = YES;
                    }
                    [collectionView_Progress reloadData];
                }
                else
                {
                    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Please check your connection" preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction *okBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        [self dismissViewControllerAnimated:YES completion:nil];
                    }];
                    
                    [alertController addAction:okBtn];
                    [self presentViewController:alertController animated:YES completion:nil];
                }
                
            });
        }];
        
    }
}


#pragma mark -- Collection View Delegate Method

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
    
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return responseArray.count;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    ReviewCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell123" forIndexPath:indexPath];
    
    if(responseArray.count>0)
    {
        UserLevelProgressModal *modal = [responseArray objectAtIndex:indexPath.row];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            NSURL *url = [[NSBundle mainBundle] URLForResource:@"loading-1" withExtension:@"gif"];
            
            
            NSString *newUrl = [modal.achievementsURL stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
            
            [cell.asynchImage sd_setImageWithURL:[NSURL URLWithString:newUrl]
                                placeholderImage:[UIImage imageNamed:@"noimage.png"]];
            
        });
    }
    
    return cell;
}
-(IBAction)pressBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
@end
