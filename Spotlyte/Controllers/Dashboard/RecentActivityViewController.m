//
//  RecentActivityViewController.m
//  Spotlyte
//
//  Created by Admin on 25/03/16.
//  Copyright © 2016 Naveen. All rights reserved.
//

#import "RecentActivityViewController.h"
#import "HeaderView.h"
@interface RecentActivityViewController ()

@end

@implementation RecentActivityViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    HeaderView *header=[[HeaderView alloc]initWithTitle:@"Dashboard" controller:self];
    [header addSubview:header.btnback];
    [self.view addSubview:header];}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 10;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"activitycell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    if(cell == nil)
    {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    }
    UIView *mainView = (UIView *)[cell.contentView viewWithTag:100];
    [mainView.layer setCornerRadius:10.0f];
    
    // border
    [mainView.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [mainView.layer setBorderWidth:1.5f];
    
    UIImageView *commentImageView = (UIImageView *)[cell.contentView viewWithTag:101];
    
    if(i == 0)
    {
        i = i+1;
        commentImageView.image = [UIImage imageNamed:@"Comment_Icon"];
    }
    else if (i == 1)
    {
        commentImageView.image = [UIImage imageNamed:@"Favorite"];
        i = i+1;
    }
    else
    {
        commentImageView.image = [UIImage imageNamed:@"unfavourite"];
        i = 0;
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

-(IBAction)pressBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
