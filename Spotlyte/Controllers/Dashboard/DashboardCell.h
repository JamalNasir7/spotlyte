//
//  DashboardCell.h
//  Spotlyte
//
//  Created by Tops on 8/12/17.
//  Copyright © 2017 Hemant. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DashboardCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgDashboard;
@property (weak, nonatomic) IBOutlet UILabel *lblTitles;
@property (weak, nonatomic) IBOutlet UILabel *lblCount;
@property (weak, nonatomic) IBOutlet UIView *view_Bottom;
@property (weak, nonatomic) IBOutlet UIView *view_right;
@property (weak, nonatomic) IBOutlet UIView *viewtop;

@end
