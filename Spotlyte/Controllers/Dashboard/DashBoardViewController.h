//
//  DashBoardViewController.h
//  Spotlyte
//
//  Created by Admin on 16/03/16.
//  Copyright © 2016 Naveen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MFMailComposeViewController.h>

@interface DashBoardViewController : UIViewController<MFMailComposeViewControllerDelegate,UICollectionViewDelegate,UICollectionViewDataSource>

@property (strong, nonatomic) IBOutlet UILabel *lblCheckIn;
@property (strong, nonatomic) IBOutlet UILabel *lblComments;
@property (strong, nonatomic) IBOutlet UILabel *lblPictures;
@property (strong, nonatomic) IBOutlet UILabel *lblNew;

@property (strong, nonatomic) IBOutlet UILabel *lblLikes;
@property (strong, nonatomic) IBOutlet UILabel *lblDislike;
@property (strong, nonatomic) IBOutlet UILabel *lblStarRating;
@property (strong, nonatomic) IBOutlet UILabel *lblFavorite;
@property (strong, nonatomic) IBOutlet UILabel *lblSpecial;
@property (weak, nonatomic) IBOutlet UICollectionView *Dash_collectionView;

@property (strong, nonatomic) IBOutlet UIButton *btnHistory;
@property (strong, nonatomic) IBOutlet UIButton *btnRecent;

@property (strong,nonatomic)  NSMutableArray *arrImages;
@property (strong,nonatomic)  NSMutableArray *arrTitle;
@property (strong,nonatomic)  NSMutableArray *arrCount;




-(IBAction)pressSelectedButton:(id)sender;

@end
