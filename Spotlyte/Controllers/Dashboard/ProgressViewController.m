//
//  ProgressViewController.m
//  Spotlyte
//
//  Created by Admin on 16/03/16.
//  Copyright © 2016 Naveen. All rights reserved.
//

#import "ProgressViewController.h"

@interface ProgressViewController ()

@end

@implementation ProgressViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if(IS_IPHONE_5)
    {
        image_X.constant = 2;
    }
    else
    {
        image_X.constant = 60;
    }
    
    [self userProgressAndLevelsMethod];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)userProgressAndLevelsMethod
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [defaults objectForKey:@"logindetails"];
    RegisterModal * registerModalResponeValue = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    NSString *user_IDRegister = registerModalResponeValue.userID;
    ResturantModal *modal = [[ResturantModal alloc]init];
    [modal setUserID:user_IDRegister];
    
    BOOL check=[[AppDelegate shareAppdelegateInstance] checkReachability];
    
    if(check)
    {
        [CUSTOMINDICATORMACRO startLoadAnimation:self];
        
        [RestaurantMacro retriveUserLevels:modal withCompletion:^(id obj1) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [CUSTOMINDICATORMACRO stopLoadAnimation:self];
                
                if([obj1 isKindOfClass:[UserLevelProgressModal class]])
                {
                    UserLevelProgressModal *userModel = obj1;
                    userProgressLevel.text = userModel.userLevel;
                    userProgressLevelName.text = userModel.userLevelName;
                    [userLevelProgressBar setValue:[userModel.userLevelAchievement floatValue]
                               animateWithDuration:1];
                    [self selectedLevelImage:userModel.userLevel];
                }
                else
                {
                    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Please check your connection" preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction *okBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        [self dismissViewControllerAnimated:YES completion:nil];
                    }];
                    
                    [alertController addAction:okBtn];
                    [self presentViewController:alertController animated:YES completion:nil];
                }
                
            });
        }];
        
    }
}


-(void)selectedLevelImage :(NSString *)levelString
{
    NSString *imageString;
    if([levelString isEqualToString:@"level1"])
    {
        imageString = @"Level1.jpg";
    }
    else if([levelString isEqualToString:@"level2"])
    {
        imageString = @"Level2.jpg";
    }
    else if([levelString isEqualToString:@"level3"])
    {
        imageString = @"Level3.jpg";
    }
    else if([levelString isEqualToString:@"level4"])
    {
        imageString = @"Level4.jpg";
    }
    else if([levelString isEqualToString:@"level5"])
    {
        imageString = @"Level5.jpg";
    }
    else
    {
        imageString = @"Level6.jpg";
    }
    levelImage.image = [UIImage imageNamed:imageString];
}



- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
    
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 20;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier = @"collectionprogress";
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    
    return cell;
}
-(IBAction)pressBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
