//
//  DashboardSpecialCell.h
//  Spotlyte
//
//  Created by Tops on 9/1/17.
//  Copyright © 2017 Hemant. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DashboardSpecialCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblRating;

@property (weak, nonatomic) IBOutlet UIImageView *imgPlace;
@property (weak, nonatomic) IBOutlet UILabel *lblPlaceName;
@property (weak, nonatomic) IBOutlet UITableView *tblHappyHours;

@end
