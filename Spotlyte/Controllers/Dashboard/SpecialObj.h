//
//  SpecialObj.h
//  Spotlyte
//
//  Created by Tops on 9/1/17.
//  Copyright © 2017 Hemant. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SpecialObj : NSObject


@property (strong,nonatomic) NSString *strPlaceID;
@property (strong,nonatomic) NSString *strPlaceImage;
@property (strong,nonatomic) NSString *strPlaceName;
@property (strong,nonatomic) NSString *strRating;

@property (strong,nonatomic) NSMutableArray *Arr_happyHour;
@end

@interface SpecialSubObj : NSObject
@property(strong,nonatomic) NSString *strUserId;
@property (strong,nonatomic) NSString *strHappyHoursID;
@property (strong,nonatomic) NSString *strTitle;
@property (strong,nonatomic) NSString *strWebsite;
@property (strong,nonatomic) NSString *strLocation;
@property (strong,nonatomic) NSString *strEmail;
@property (strong,nonatomic) NSString *strPrice;
@property (strong,nonatomic) NSString *strWeekDay;
@property (strong,nonatomic) NSString *strStartTime;
@property (strong,nonatomic) NSString *strEndTime;
@end
