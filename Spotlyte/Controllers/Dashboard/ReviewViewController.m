//
//  ReviewViewController.m
//  Spotlyte
//
//  Created by Admin on 25/03/16.
//  Copyright © 2016 Naveen. All rights reserved.
//

#import "ReviewViewController.h"

@interface ReviewViewController ()

@end

@implementation ReviewViewController
@synthesize checkWhichReview;
@synthesize hotelImageArray,hotelNameArray;
@synthesize reviewResponseArray;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    reviewResponseArray = [NSMutableArray new];
    tableView_Review.tableFooterView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, tableView_Review.frame.size.width, 0.1f)];
    tableView_Review.hidden = YES;
    
    [self positiveNegativeServiceHelper];
    if([checkWhichReview isEqualToString:@"positive"])
    {
        lbl_TopBar.text = @"POSITIVE REVIEWS";
    }
    else
    {
        lbl_TopBar.text = @"NEGATIVE REVIEWS";
    }
    
    hotelNameArray = [[NSArray alloc]initWithObjects:@"Intourist ",@"Promenade ",@"Cardose ",@"The View Point Inn ",@"Royal Hotel ",@"Ostro ",@"BarCode ",@"Liquor Shop ",@"Bar One ",@"Safari Bar ", nil];
    hotelImageArray = [[NSArray alloc]initWithObjects:@"promo1.jpeg",@"promo2.jpeg",@"promo3.jpeg",@"promo4.jpeg",@"promo5.jpeg",@"promo6.jpeg",@"promo7.jpeg",@"promo8.jpeg",@"promo9.jpeg",@"promo10.jpeg", nil];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark --- positive Negative ServiceHelper

-(void)positiveNegativeServiceHelper
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [defaults objectForKey:@"logindetails"];
    RegisterModal * registerModalResponeValue = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    NSString *user_IDRegister = registerModalResponeValue.userID;
    
    TotalHistoryModal *modal = [[TotalHistoryModal alloc]init];
    [modal setUserID_History:user_IDRegister];
    
    BOOL check=[[AppDelegate shareAppdelegateInstance] checkReachability];
    if(check)
    {
        [CUSTOMINDICATORMACRO startLoadAnimation:self];
        
        [HistoryMacro positiveNegativeToServer:modal withCompletion:^(id obj1) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [CUSTOMINDICATORMACRO stopLoadAnimation:self];
                if([obj1 isKindOfClass:[TotalHistoryModal class]])
                {
                    TotalHistoryModal *modal = obj1;
                    if([checkWhichReview isEqualToString:@"positive"])
                    {
                        reviewResponseArray = modal.arrayPositive;
                    }
                    else
                    {
                        reviewResponseArray = modal.arrayNegative;
                    }
                    if(reviewResponseArray.count == 0)
                    {
                        view_noValues.hidden = NO;
                    }
                    else
                    {
                        view_noValues.hidden = YES;
                    }
                    tableView_Review.hidden = NO;
                    
                    [tableView_Review reloadData];
                }
                else
                {
                    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Please check your Internet Connection." preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction *okBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        [self dismissViewControllerAnimated:YES completion:nil];
                    }];
                    [alertController addAction:okBtn];
                    [self presentViewController:alertController animated:YES completion:nil];
                }
                
                
            });
        }];
        
    }
}

- (void)displayStar :(NSString *)ratingVal
{
    if(ratingVal.floatValue <= 2.0)
    {
        starRatingView.starHighlightedImage = [UIImage imageNamed:@"RedStarIcon"];
    }
    else if(ratingVal.floatValue <= 3.9)
    {
        starRatingView.starHighlightedImage = [UIImage imageNamed:@"YellowStarIcon"];
    }
    else
    {
        starRatingView.starHighlightedImage = [UIImage imageNamed:@"GreenStarIcon"];
    }
    starRatingView.starImage = [UIImage imageNamed:@"UnStarIcon"];
    starRatingView.maxRating = 5.0;
    starRatingView.delegate = self;
    starRatingView.horizontalMargin = 12;
    starRatingView.editable = YES;
    starRatingView.rating = ratingVal.intValue;
    starRatingView.displayMode = EDStarRatingDisplayAccurate;
}

#pragma mark --- TableView Delegate Methods


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return reviewResponseArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TotalHistoryModal *modal1 = [reviewResponseArray objectAtIndex:indexPath.row];
    
    static NSString *cellIdentifier = @"reviewcell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    if(cell == nil)
    {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    }
    UIView *mainView = (UIView *)[cell.contentView viewWithTag:100];
    [mainView.layer setCornerRadius:10.0f];
    
    // border
    [mainView.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [mainView.layer setBorderWidth:1.5f];
    
    UIImageView *restImage = (UIImageView *)[cell.contentView viewWithTag:101];
    restImage.layer.cornerRadius = 30.0;
    [restImage setBackgroundColor:[UIColor clearColor]];
    restImage.layer.masksToBounds = YES;
    dispatch_async(dispatch_get_main_queue(), ^{
        NSURL *url = [[NSBundle mainBundle] URLForResource:@"loading-1" withExtension:@"gif"];
        NSString *newUrl = [modal1.resturantImageStr stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
        [restImage sd_setImageWithURL:[NSURL URLWithString:newUrl]
                     placeholderImage:[UIImage imageNamed:@"noimage.png"]];
        
    });
    
    UILabel *hotelName = (UILabel *)[cell.contentView viewWithTag:102];
    hotelName.text = modal1.resturantName;
    UILabel *commentDesc = (UILabel *)[cell.contentView viewWithTag:103];
    commentDesc.text = modal1.resturantReviewDescription;
    
    starRatingView = (EDStarRating *)[cell.contentView viewWithTag:200];
    [self displayStar:modal1.resturantReviewRating];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

-(IBAction)pressBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


@end
