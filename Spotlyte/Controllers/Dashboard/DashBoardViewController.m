//
//  DashBoardViewController.m
//  Spotlyte
//
//  Created by Admin on 16/03/16.
//  Copyright © 2016 Naveen. All rights reserved.
//

#import "DashBoardViewController.h"
#import "ProgressViewController.h"
#import "FavouriteListViewController.h"
#import "RecentActivityViewController.h"
#import "ReviewViewController.h"
#import "HeaderView.h"
#import "LikeViewController.h"
#import "CheckInViewController.h"
#import "CommentViewController.h"
#import "RatingsViewController.h"
#import "FavSpecialViewController.h"
#import "DashboardCell.h"

@interface DashBoardViewController ()
{
}

@end

@implementation DashBoardViewController
@synthesize arrImages,arrCount,arrTitle;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    HeaderView *header=[[HeaderView alloc]initWithTitle:@"Dashboard" controller:self];
    [header addSubview:header.btnback];
    [header.imgViewBack setHidden:YES];
    header.BackButtonHandler=^(BOOL success){
        if (success) {
           // [self.tabBarController setSelectedIndex:0];
        }
    };
    [self.view addSubview:header];
    [_Dash_collectionView registerNib:[UINib nibWithNibName:@"DashboardCell" bundle:nil] forCellWithReuseIdentifier:@"DashboardCell"];
    self.navigationController.navigationBar.hidden = true;
    
    arrImages = [[NSMutableArray alloc]initWithObjects:@"Dash_CheckIns",@"Dash_Comments",@"Dash_Pictures",@"Dash_Specials",@"Dash_likes",@"Dash_Dislikes",@"Dash_Ratings",@"Dash_Favorites", nil];
    arrTitle = [[NSMutableArray alloc]initWithObjects:@"Check Ins",@"Reviews",@"Pictures",@"Favorite Specials",@"Likes",@"Dislikes",@"Avg Star Ratings",@"Favorites",nil];
    [self getDashBoardCount];

    _Dash_collectionView.dataSource = self;
    _Dash_collectionView.delegate = self;
    [_Dash_collectionView reloadData];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [self getDashBoardCount];
    
    //Disable Swipe
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
    
   // _btnHistory.layer.cornerRadius = 5.0;
   // _btnRecent.layer.cornerRadius = 5.0;
}

-(void)goBackToHomePage{
    //[self.tabBarController setSelectedIndex:0];
}

-(void)getDashBoardCount{
 BOOL check=[[AppDelegate shareAppdelegateInstance] checkReachability];
    if(check){
        [CUSTOMINDICATORMACRO startLoadAnimation:self];
        
        NSUserDefaults *defaults    =   [NSUserDefaults standardUserDefaults];
        NSData *data                =   [defaults objectForKey:@"logindetails"];
        
        RegisterModal * registerModalResponeValue   =   [NSKeyedUnarchiver unarchiveObjectWithData:data];
        NSString * user_IDRegister                  =   registerModalResponeValue.userID;
        
        
        [REGISTERMACRO getDashboardCount:user_IDRegister withCompletion:^(id obj1){
        
            dispatch_async(dispatch_get_main_queue(), ^{
                
            if (obj1!=nil && ![obj1 isKindOfClass:[NSNull class]]) {
                if([obj1 isKindOfClass:[RegisterModal class]])
                {
                    RegisterModal *response = obj1;
                    
                    arrCount = [[NSMutableArray alloc]initWithObjects:response.checkin_count,response.comment_count,response.photo_count,response.special_count,response.like_count,response.dislike_count,response.rating,response.favorites_count, nil];
//                    _lblCheckIn.text=[NSString stringWithFormat:@"%d",[response.checkin_count intValue]];
//                    _lblComments.text=[NSString stringWithFormat:@"%d",[response.comment_count intValue]];
//                    _lblDislike.text=[NSString stringWithFormat:@"%d",[response.dislike_count intValue]];
//                    _lblFavorite.text=[NSString stringWithFormat:@"%d",[response.favorites_count intValue]];
//                    _lblSpecial.text=[NSString stringWithFormat:@"%d",[response.special_count intValue]];
//                    _lblLikes.text=[NSString stringWithFormat:@"%d",[response.like_count intValue]];
//                    _lblPictures.text=[NSString stringWithFormat:@"%d",[response.photo_count intValue]];
//                    _lblStarRating.text=[NSString stringWithFormat:@"%.01f",[response.rating floatValue]];
                    [_Dash_collectionView reloadData];
                     [CUSTOMINDICATORMACRO stopLoadAnimation:self];
                  }
                else
                {
                    [self serverError];
                }
            }else
                [self PleaseCheckInternet];
                
                [CUSTOMINDICATORMACRO stopLoadAnimation:self];
            });
        }];
    }
}



-(void)PleaseCheckInternet{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Please check your connection" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [alertController addAction:okBtn];
    [self presentViewController:alertController animated:YES completion:nil];
}


-(void)serverError{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Server Error" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [alertController addAction:okBtn];
    [self presentViewController:alertController animated:YES completion:nil];
}

-(IBAction)pressSelectedButton:(id)sender
{
    UIButton *button = (UIButton *)sender;
    
    if(button.tag == 0)
    {
        TotalHistoryViewController *totalHVC  = [self.storyboard instantiateViewControllerWithIdentifier:@"totalHistoryVC"];
        totalHVC.whichControllerHistory = @"history";
        [self.navigationController pushViewController:totalHVC animated:YES];
    }
    else if (button.tag == 1)
    {
        TotalHistoryViewController *totalHVC  = [self.storyboard instantiateViewControllerWithIdentifier:@"totalHistoryVC"];
        totalHVC.whichControllerHistory = @"recent";
        [self.navigationController pushViewController:totalHVC animated:YES];
    }
    else if (button.tag == 2) // Like
    {
        LikeViewController *likeObj  = [self.storyboard instantiateViewControllerWithIdentifier:@"likelist"];
        likeObj.checkWhichType = @"Like";
        [self.navigationController pushViewController:likeObj animated:YES];
    }
    else if (button.tag == 3) // DisLike
    {
        LikeViewController *dislikeObj  = [self.storyboard instantiateViewControllerWithIdentifier:@"likelist"];
        dislikeObj.checkWhichType = @"Dislike";
        [self.navigationController pushViewController:dislikeObj animated:YES];
    }
    else if (button.tag == 4) // Ratings
    {
        RatingsViewController *ratingsObj  = [self.storyboard instantiateViewControllerWithIdentifier:@"ratinglist"];
        [self.navigationController pushViewController:ratingsObj animated:YES];
    }
    else if (button.tag == 5) // Check Ins
    {
        CheckInViewController *checkInObj  = [self.storyboard instantiateViewControllerWithIdentifier:@"checkinlist"];
        [self.navigationController pushViewController:checkInObj animated:YES];
    }
    else if (button.tag == 6) // Comments
    {
        CommentViewController *commentsObj  = [self.storyboard instantiateViewControllerWithIdentifier:@"commentlist"];
        [self.navigationController pushViewController:commentsObj animated:YES];
    }
    else if (button.tag == 7) // Favourite
    {
        FavouriteListViewController *favouriteVC  = [self.storyboard instantiateViewControllerWithIdentifier:@"favouritelist"];
        [self.navigationController pushViewController:favouriteVC animated:YES];
    }
    else if (button.tag == 8) // Pictures
    {
        AverageReviewViewController *averageVC  = [self.storyboard instantiateViewControllerWithIdentifier:@"averagereviewvc"];
        [self.navigationController pushViewController:averageVC animated:YES];
    }
    else // Special
    {
        FavSpecialViewController *favouriteVC  = [self.storyboard instantiateViewControllerWithIdentifier:@"favspeciallist"];
        [self.navigationController pushViewController:favouriteVC animated:YES];
    }
}

-(void)reportAction
{
    NSString *emailTitle = @"Reporting Email";
    // Email Content
    NSString *messageBody = @"test mail";
    // To address
    NSArray *toRecipents = [NSArray arrayWithObject:@"report@spotlyte.com"];
    
    MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
    mc.mailComposeDelegate = self;
    [mc setSubject:emailTitle];
    [mc setMessageBody:messageBody isHTML:NO];
    [mc setToRecipients:toRecipents];
    
    // Present mail view controller on screen
    [self presentViewController:mc animated:YES completion:NULL];
}

- (void)mailComposeController:(MFMailComposeViewController*)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError*)error;
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            [self dismissViewControllerAnimated:YES completion:NULL];
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            [self dismissViewControllerAnimated:YES completion:NULL];
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            [self dismissViewControllerAnimated:YES completion:NULL];
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            [self dismissViewControllerAnimated:YES completion:NULL];
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    
}
#pragma mark - CollectionView Delegate
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section;
{
    return 8;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath;
{
    static NSString *cellIdentifier = @"DashboardCell";
    DashboardCell *cell = (DashboardCell*)[_Dash_collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    if(cell == nil)
    {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"DashboardCell" owner:self options:nil]objectAtIndex:0];
    }
    cell.lblTitles.text = [NSString stringWithFormat:@"%@",[arrTitle objectAtIndex:indexPath.item]];
    cell.imgDashboard.image = [UIImage imageNamed:[arrImages objectAtIndex:indexPath.item]];
    if (arrCount.count>0)
    {
        cell.lblCount.text = [NSString stringWithFormat:@"%@",[arrCount objectAtIndex:indexPath.item]];
    }
    else{
        cell.lblCount.text =@"0";
    }
    
    if ((indexPath.item == 0) || (indexPath.item == 1) || (indexPath.item == 3) ||(indexPath.item == 4)){
        cell.viewtop.hidden = true;
        cell.view_right.hidden = false;
        cell.view_Bottom.hidden = false;
    }
    else if ((indexPath.item == 2)||(indexPath.item == 5))
    {
        cell.viewtop.hidden = true;
        cell.view_right.hidden = true;
        cell.view_Bottom.hidden = false;
    }
    else if ((indexPath.item == 6)||(indexPath.item == 7))
    {
        cell.viewtop.hidden = true;
        cell.view_right.hidden = false;
        cell.view_Bottom.hidden = true;
    }
    return cell;
}
- (CGSize)collectionView:(UICollectionView *)collectionView
                   layout:(UICollectionViewLayout *)collectionViewLayout
   sizeForItemAtIndexPath:(NSIndexPath *)indexPath;
{
    return CGSizeMake(self.view.frame.size.width/3, (self.view.frame.size.width/3)+15);
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath;
{
    if (indexPath.item == 0) //Check-ins
    {
        CheckInViewController *checkInObj  = [self.storyboard instantiateViewControllerWithIdentifier:@"checkinlist"];
        [self.navigationController pushViewController:checkInObj animated:YES];

    }else if (indexPath.item == 1)//Comments
    {
        CommentViewController *commentsObj  = [self.storyboard instantiateViewControllerWithIdentifier:@"commentlist"];
        [self.navigationController pushViewController:commentsObj animated:YES];

    }else if (indexPath.item == 2)//Pictures
    {
        AverageReviewViewController *averageVC  = [self.storyboard instantiateViewControllerWithIdentifier:@"averagereviewvc"];
        [self.navigationController pushViewController:averageVC animated:YES];

    }else if (indexPath.item == 3)//Specialist
    {
        
        FavSpecialViewController *favouriteVC  = [self.storyboard instantiateViewControllerWithIdentifier:@"favspeciallist"];
        [self.navigationController pushViewController:favouriteVC animated:YES];

    }else if (indexPath.item == 4)//likes
    {
        LikeViewController *likeObj  = [self.storyboard instantiateViewControllerWithIdentifier:@"likelist"];
        likeObj.checkWhichType = @"Like";
        [self.navigationController pushViewController:likeObj animated:YES];

    }else if (indexPath.item == 5)//Dislikes
    {
        LikeViewController *dislikeObj  = [self.storyboard instantiateViewControllerWithIdentifier:@"likelist"];
        dislikeObj.checkWhichType = @"Dislike";
        [self.navigationController pushViewController:dislikeObj animated:YES];
   
    }else if (indexPath.item == 6)//Star Rating
    {
        RatingsViewController *ratingsObj  = [self.storyboard instantiateViewControllerWithIdentifier:@"ratinglist"];
        [self.navigationController pushViewController:ratingsObj animated:YES];
    }else if (indexPath.item == 7)//Favourites
    {
        FavouriteListViewController *favouriteVC  = [self.storyboard instantiateViewControllerWithIdentifier:@"favouritelist"];
        [self.navigationController pushViewController:favouriteVC animated:YES];
    }

}
-(IBAction)pressBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
