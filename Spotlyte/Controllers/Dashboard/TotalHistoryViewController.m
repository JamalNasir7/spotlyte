//
//  TotalHistoryViewController.m
//  Spotlyte
//
//  Created by CIPL227-MOBILITY on 20/04/16.
//  Copyright © 2016 Naveen. All rights reserved.
//

#import "TotalHistoryViewController.h"
#import "HeaderView.h"
#import "TotalHistoryCell.h"

@interface TotalHistoryViewController ()

@end

@implementation TotalHistoryViewController
@synthesize arrayTotalHistory;
@synthesize whichControllerHistory;
- (void)viewDidLoad {
    [super viewDidLoad];
    tableView_History.rowHeight = UITableViewAutomaticDimension;
    tableView_History.estimatedRowHeight = 80.0;
    arrayTotalHistory = [[NSMutableArray alloc]init];
    tableView_History.tableFooterView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, tableView_History.frame.size.width, 0.1f)];
    tableView_History.hidden = YES;
   
        HeaderView *header=[[HeaderView alloc]initWithTitle:[whichControllerHistory isEqualToString:@"history"]?@"TOTAL HISTORY":@"MOST RECENT ACTIVITY" controller:self];
        [header addSubview:header.btnback];
        [self.view addSubview:header];
    
    [whichControllerHistory isEqualToString:@"history"]?[self totalHistoryServiceHelper]:[self recentActivityServiceHelper];
}

-(void)addHeaderInView{
    HeaderView *header=[[HeaderView alloc]initWithTitle:lblHeader.text controller:self];
    [header addSubview:header.btnback];
    [self.view addSubview:header];

}

#pragma mark --- Total History ServiceHelper

-(void)totalHistoryServiceHelper
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [defaults objectForKey:@"logindetails"];
    RegisterModal * registerModalResponeValue = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    NSString *user_IDRegister = registerModalResponeValue.userID;
    profileImageStr = registerModalResponeValue.profileImage;
    
    TotalHistoryModal *modal = [[TotalHistoryModal alloc]init];
    [modal setUserID_History:user_IDRegister];
    
    BOOL check=[[AppDelegate shareAppdelegateInstance] checkReachability];
    if(check)
    {
        [CUSTOMINDICATORMACRO startLoadAnimation:self];
        
        [HistoryMacro totalHistoryValuesToServer:modal withCompletion:^(id obj1) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [CUSTOMINDICATORMACRO stopLoadAnimation:self];
                NSString *title;
                NSString *message;
                BOOL isCheckedAlert = FALSE;
                if([obj1 isKindOfClass:[NSArray class]] || [obj1 isKindOfClass:[NSMutableArray class]])
                {
                    arrayTotalHistory = obj1;
                    
                    if (arrayTotalHistory.count>0) {
                        tableView_History.hidden = NO;
                        [tableView_History reloadData];
                    }
                    
                }
                else if([obj1 isKindOfClass:[TotalHistoryModal class]])
                {
                    TotalHistoryModal *modal = obj1;
                    title = modal.result;
                    view_NoValues.hidden = NO;
                }
                else
                {
                    title = @"";
                    message = @"Please check your Internet Connection.";
                    isCheckedAlert = TRUE;
                    
                }
                if(isCheckedAlert == TRUE)
                {
                    [self showAlertTitle:@"" Message:message];
                }
                
            });
        }];
        
    }
}

#pragma mark --- Recent Activity Service Helper

-(void)recentActivityServiceHelper
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [defaults objectForKey:@"logindetails"];
    RegisterModal * registerModalResponeValue = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    NSString *user_IDRegister = registerModalResponeValue.userID;
    profileImageStr = registerModalResponeValue.profileImage;
    
    TotalHistoryModal *modal = [[TotalHistoryModal alloc]init];
    [modal setUserID_History:user_IDRegister];
    
    BOOL check=[[AppDelegate shareAppdelegateInstance] checkReachability];
    if(check)
    {
        [CUSTOMINDICATORMACRO startLoadAnimation:self];
        
        if (arrayTotalHistory.count>0)
            [arrayTotalHistory removeAllObjects];
        
      
        [HistoryMacro recentActivityToServer:modal withCompletion:^(id obj1) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [CUSTOMINDICATORMACRO stopLoadAnimation:self];
                NSString *title;
                NSString *message;
                BOOL isCheckedAlert = FALSE;
                if([obj1 isKindOfClass:[NSArray class]] || [obj1 isKindOfClass:[NSMutableArray class]])
                {
                    arrayTotalHistory = obj1;
                    
                    if (arrayTotalHistory.count>0) {
                        tableView_History.hidden = NO;
                        [tableView_History reloadData];
                    }
                }
                else if([obj1 isKindOfClass:[TotalHistoryModal class]])
                {
                    TotalHistoryModal *modal = obj1;
                    title = modal.result;
                    view_NoValues.hidden = NO;
                }
                else
                {
                    title = @"";
                    message = @"Please check your Internet Connection.";
                    isCheckedAlert = TRUE;
                    
                }
                if(isCheckedAlert == TRUE)
                {
                    [self showAlertTitle:@"" Message:message];
                }
                
            });
        }];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arrayTotalHistory count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
   
    TotalHistoryModal *modal1 = [arrayTotalHistory objectAtIndex:indexPath.row];
    
    NSLog(@"indexPath ======= %ld",(long)indexPath.row);
    
    NSString *str=[NSString stringWithFormat:@"%@",modal1.recentActivityType];
    NSString *condition =[NSString stringWithFormat:@"%@",modal1.condition];
    
//    if([str isEqualToString:@"POSTCOMMENTS"])
//    {
    //recentReviews
    
    
    static NSString *cellIdentifier = @"sampleidentifier";
    TotalHistoryCell *cell = (TotalHistoryCell *)[tableView_History dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if(cell == nil)
    {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"TotalHistoryCell" owner:self options:nil]objectAtIndex:0];
    }
//        static NSString *cellIdentifier = @"Comments";
//        TotalHistoryCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
//        if(cell == nil)
//        {
//            cell = [[TotalHistoryCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
//        }

    if (arrayTotalHistory.count>0) {
        cell.imgPlaceName.layer.masksToBounds = YES;
        cell.imgPlaceName.layer.cornerRadius =
            3.0;
            
        dispatch_async(dispatch_get_main_queue(), ^{
            NSURL *url = [[NSBundle mainBundle] URLForResource:@"loading-1" withExtension:@"gif"];
            NSString *newUrl = [modal1.resturantImageStr stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
        [cell.imgPlace sd_setImageWithURL:[NSURL URLWithString:newUrl]
                         placeholderImage:[UIImage imageNamed:@"noimage.png"]];
         });
             EDStarRating *starRatingView = (EDStarRating *)[cell.contentView viewWithTag:203];
            if ([condition isEqualToString:@"Image"]) {
               
                starRatingView.hidden=NO;
                 cell.EDRating_X.constant=61;
                starRatingView.starImage = [UIImage imageNamed:@"UnStarIcon"];
                if(modal1.resturantReviewRating.floatValue <= 2.0)
                {
                    starRatingView.starHighlightedImage = [UIImage imageNamed:@"RedStarIcon"];
                }
                else if(modal1.resturantReviewRating.floatValue <= 3.9)
                {
                    starRatingView.starHighlightedImage = [UIImage imageNamed:@"YellowStarIcon"];
                }
                else
                {
                    starRatingView.starHighlightedImage = [UIImage imageNamed:@"GreenStarIcon"];
                }
                starRatingView.maxRating = 5.0;
                starRatingView.delegate = self;
                starRatingView.horizontalMargin = 12;
                starRatingView.editable = NO;
                starRatingView.rating = [modal1.resturantReviewRating floatValue];
                starRatingView.displayMode = EDStarRatingDisplayAccurate;
                
            }else{
                starRatingView.hidden=YES;
                 cell.EDRating_X.constant=0;
            }
            
            
           
            
            cell.imgPlaceName.text = modal1.resturantName;
            
            
            
            NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
        [dateFormat setLocale:locale];

            [dateFormat setDateFormat:@"MM-dd-yyyy hh:mm a"];
            
            NSString *strCreatedDate = modal1.recentActivityTime;
            NSDate *dateCreate = [dateFormat dateFromString:strCreatedDate];
            
            [dateFormat setDateFormat:@"MM-dd-yyyy"];
            NSString *strStartDate = [dateFormat stringFromDate:dateCreate];
            cell.lblDate.text = strStartDate;
            
            [dateFormat setDateFormat:@"hh:mm a"];
            NSString *strTime = [dateFormat stringFromDate:dateCreate];
            cell.lblTime.text = strTime;

            
            cell.lblComment.text =@"";
            
            if ([condition isEqualToString:@"Comment"]) {
                cell.lblComment.text =modal1.resturantReviewDescription;
                cell.lblDescription.text = modal1.postComment;
            }
            else
            {
                if (modal1.postComment != nil)
                {
                   // cell.lblComment.text = @"";
                   // cell.lblComment.text = modal1.postComment;
                }
                cell.lblDescription.text =modal1.resturantReviewDescription;
            }
            
        /* UILabel *commentDesc = (UILabel *)[cell.contentView viewWithTag:204];
         
         commentDesc.text = modal1.postComment;*/
            
            
            /*if ([modal1.condition isEqualToString:@"Image"]) {
                UILabel *hotelName = (UILabel *)[cell.contentView viewWithTag:202];
                // hotelName.text = modal1.resturantName;
                hotelName.text = modal1.postComment;
                
                 UILabel *dateLbl = (UILabel *)[cell.contentView viewWithTag:210];
                dateLbl.text = modal1.recentActivityTime;
                
                UILabel *comment = (UILabel *)[cell.contentView viewWithTag:204];
                comment.text = @"";

                
            }else
            {
            
                UILabel *hotelName = (UILabel *)[cell.contentView viewWithTag:202];
                // hotelName.text = modal1.resturantName;
                hotelName.text = modal1.resturantReviewDescription;
                hotelName.numberOfLines=2;
                
                UILabel *dateLbl = (UILabel *)[cell.contentView viewWithTag:210];
                dateLbl.text = modal1.recentActivityTime;
                
                UILabel *comment = (UILabel *)[cell.contentView viewWithTag:204];
                comment.text = modal1.postComment;
            
            }*/
         cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
   
  //  else
//    {
//        static NSString *cell_identifier = @"recentActivity";
//        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cell_identifier forIndexPath:indexPath];
//        if(cell == nil)
//        {
//            cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
//        }
//        
//        if (arrayTotalHistory.count>0) {
//            
//            if (![str isEqualToString:@"(\n)"]) {
//                
//                UIView *mainView = (UIView *)[cell.contentView viewWithTag:100];
//                [mainView.layer setCornerRadius:10.0f];
//                [mainView.layer setBorderColor:[UIColor lightGrayColor].CGColor];
//                [mainView.layer setBorderWidth:1.5f];
//                
//                UIImageView *restImage = (UIImageView *)[cell.contentView viewWithTag:101];
//                restImage.layer.cornerRadius = 25.0;
//                [restImage setBackgroundColor:[UIColor clearColor]];
//                restImage.layer.masksToBounds = YES;
//                dispatch_async(dispatch_get_main_queue(), ^{
//                    NSURL *url = [[NSBundle mainBundle] URLForResource:@"loading-1" withExtension:@"gif"];
//                    NSString *newUrl = [modal1.resturantImageStr stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
//                 [restImage sd_setImageWithURL:[NSURL URLWithString:newUrl]
//                                 placeholderImage:[UIImage animatedImageWithAnimatedGIFURL:url]];
//                    
//                });
//                
//                
//                UILabel *commentDesc = (UILabel *)[cell.contentView viewWithTag:102];
//                 if ([modal1.resturantReviewDescription rangeOfString:@"Rated"].location ==NSNotFound)
//                {
//                     commentDesc.text = modal1.resturantReviewDescription;
//                   
//                } else
//                {
//                   commentDesc.text =[whichControllerHistory isEqualToString:@"history"]? [ NSString stringWithFormat:@"%@ %ld/5",modal1.resturantReviewDescription,(long)[modal1.resturantReviewRating integerValue] ]:modal1.resturantReviewDescription;
//                }
//
//                UILabel *dateLbl = (UILabel *)[cell.contentView viewWithTag:105];
//                dateLbl.text = modal1.recentActivityTime;
//                cell.selectionStyle = UITableViewCellSelectionStyleNone;
//            }
//          }
//            
//        return cell;
// 
//
    return cell;

//    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;

}
-(IBAction)pressBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)loadModalArrayData
{
    TotalHistoryModal *modal;
    //1
    modal = [[TotalHistoryModal alloc]init];
    [modal setResturantReviewDescription:@"Whether you are going to have a business lunch, romantic candlelight dinner or just a drink after a busy day, the five-star Grand Hotel Bohemia will always meet your needs."];
    [modal setResturantImage:[UIImage imageNamed:@"Rest1.jpeg"]];
    [modal setResturantName:@"Beekman Bar"];
    [modal setResturantReviewRating:@"0"];
    [modal setRecentActivityType:@"2"];
    
    [arrayTotalHistory addObject:modal];
    
    //2
    modal = [[TotalHistoryModal alloc]init];
    
    [modal setResturantReviewDescription:@"Uploaded three images of Grand Hotel Bohemia."];
    [modal setResturantImage:[UIImage imageNamed:@"img_06.jpeg"]];
    [modal setResturantName:@" "];
    [modal setResturantReviewRating:@"0"];
    [modal setRecentActivityType:@"0"];
    
    
    
    [arrayTotalHistory addObject:modal];
    
    //3
    modal = [[TotalHistoryModal alloc]init];
    [modal setResturantReviewDescription:@"Whether you are going to have a business lunch, romantic candlelight dinner or just a drink after a busy day, the five-star Grand Hotel Bohemia will always meet your needs."];
    [modal setResturantImage:[UIImage imageNamed:@"Rest3.jpeg"]];
    [modal setResturantName:@"Frankfort Restaurant"];
    [modal setResturantReviewRating:@"2"];
    [modal setRecentActivityType:@"1"];
    
    
    [arrayTotalHistory addObject:modal];
    
    //4
    
    
    modal = [[TotalHistoryModal alloc]init];
    [modal setResturantReviewDescription:@"Whether you are going to have a business lunch, romantic candlelight dinner or just a drink after a busy day, the five-star Vessey Museum will always meet your needs."];
    [modal setResturantImage:[UIImage imageNamed:@"Rest3.jpeg"]];
    [modal setResturantReviewRating:@"3"];
    [modal setRecentActivityType:@"1"];
    [modal setResturantImage:[UIImage imageNamed:@"Rest4.jpeg"]];
    [modal setResturantName:@"Vessey Museum"];
    
    
    [arrayTotalHistory addObject:modal];
    
    //5
    
    modal = [[TotalHistoryModal alloc]init];
    
    [modal setResturantReviewDescription:@"Uploaded four images of Nassua Museum."];
    [modal setResturantImage:[UIImage imageNamed:@"img_06.jpeg"]];
    [modal setResturantName:@" "];
    [modal setResturantReviewRating:@"0"];
    [modal setRecentActivityType:@"0"];
    
    
    [arrayTotalHistory addObject:modal];
    
    //6
    modal = [[TotalHistoryModal alloc]init];
    
    [modal setResturantReviewDescription:@"Uploaded one image of Fulton Bar."];
    [modal setResturantImage:[UIImage imageNamed:@"img_06.jpeg"]];
    [modal setResturantName:@" "];
    [modal setResturantReviewRating:@"0"];
    [modal setRecentActivityType:@"0"];
    
    
    
    [arrayTotalHistory addObject:modal];
}

-(void)showAlertTitle:(NSString *)title Message:(NSString *)message
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [alertController addAction:okBtn];
    
    [self presentViewController:alertController animated:YES completion:nil];
}


@end
