//
//  TotalHistoryViewController.h
//  Spotlyte
//
//  Created by CIPL227-MOBILITY on 20/04/16.
//  Copyright © 2016 Naveen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EDStarRating.h"


@interface TotalHistoryViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,EDStarRatingProtocol >
{
    IBOutlet UITableView *tableView_History;
    IBOutlet UILabel *lblHeader;
    IBOutlet UIView  *view_NoValues;
    NSString *profileImageStr;

    //
}

@property (nonatomic, retain) NSMutableArray *arrayTotalHistory;
@property (nonatomic, retain) NSString *whichControllerHistory;




@end
