//
//  AverageReviewViewController.h
//  Spotlyte
//
//  Created by Admin on 19/04/16.
//  Copyright © 2016 Naveen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AverageReviewViewController : UIViewController<UICollectionViewDataSource,UICollectionViewDelegate>
{
    IBOutlet UICollectionView *collectionViewReview;
    IBOutlet UICollectionViewFlowLayout *collectionViewReviewFlow;

    IBOutlet UIView   *view_NoValues;

}
@property (nonatomic, retain)NSMutableArray *responseArray;
@end
