//
//  AverageReviewViewController.m
//  Spotlyte
//
//  Created by Admin on 19/04/16.
//  Copyright © 2016 Naveen. All rights reserved.
//

#import "AverageReviewViewController.h"
#import "ReviewCollectionCell.h"
#import "HeaderView.h"

#import "FSBasicImage.h"
#import "FSBasicImageSource.h"
#import "FSImageViewerViewController.h"

@interface AverageReviewViewController ()
{
    NSMutableArray *images;
    UIButton *btnGalleryClose;
    FSImageViewerViewController *imageViewController;
}
@end

@implementation AverageReviewViewController
@synthesize responseArray;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    HeaderView *header=[[HeaderView alloc]initWithTitle:@"PHOTOS" controller:self];
    [header addSubview:header.btnback];
    [self.view addSubview:header];

    
    [collectionViewReview registerNib:[UINib nibWithNibName:@"ReviewCollectionCell" bundle:nil]
           forCellWithReuseIdentifier:@"cell123"];
    responseArray = [[NSMutableArray alloc]init];
    collectionViewReview.hidden = YES;
    [self commentsImageServiceHelper];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark --- Service Helper

-(void)commentsImageServiceHelper
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [defaults objectForKey:@"logindetails"];
    RegisterModal * registerModalResponeValue = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    NSString *user_IDRegister = registerModalResponeValue.userID;
    ResturantModal *modal = [[ResturantModal alloc]init];
    [modal setUserID:user_IDRegister];
    
    BOOL check=[[AppDelegate shareAppdelegateInstance] checkReachability];
    
    if(check)
    {
        [CUSTOMINDICATORMACRO startLoadAnimation:self];
        
        [RestaurantMacro listAllcommentsImageToServer:modal withCompletion:^(id obj1) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
               
           if (obj1!=nil && ![obj1 isKindOfClass:[NSNull class]]) {
                if([obj1 isKindOfClass:[NSArray class]] || [obj1 isKindOfClass:[NSMutableArray class]])
                {
                    responseArray = obj1;
                    collectionViewReview.hidden = NO;
                    if(responseArray.count == 0)
                    {
                        view_NoValues.hidden = NO;
                    }
                    else
                    {
                        view_NoValues.hidden = YES;
                        [collectionViewReview reloadData];
                    }
                }
                else
                {
                    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Please check your connection" preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction *okBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        [self dismissViewControllerAnimated:YES completion:nil];
                    }];
                    
                    [alertController addAction:okBtn];
                    [self presentViewController:alertController animated:YES completion:nil];
                }
            }else
                 view_NoValues.hidden = NO;
            });
        }];
        
    }
}

#pragma mark -- Collection View Delegate Method

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
    
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return responseArray.count;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    ReviewCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell123" forIndexPath:indexPath];
    
    if(responseArray.count>0)
    {
        ResturantModal *modal = [responseArray objectAtIndex:indexPath.row];
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            NSURL *url = [[NSBundle mainBundle] URLForResource:@"loading-1" withExtension:@"gif"];
            
            NSString *newUrl = [modal.commentsImage stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
            [cell.asynchImage sd_setImageWithURL:[NSURL URLWithString:newUrl]
                                placeholderImage:[UIImage imageNamed:@"noimage.png"]];
            [CUSTOMINDICATORMACRO stopLoadAnimation:self];
        });
        
        if (!images) {
            images=[NSMutableArray new];
        }
        FSBasicImage *image = [[FSBasicImage alloc] initWithImageURL:[NSURL URLWithString:modal.commentsImage] name:[NSString stringWithFormat:@"Photo %ld",(long)indexPath.row]];
        [images addObject:image];
    }
    return cell;
}


-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    FSBasicImageSource *photoSource = [[FSBasicImageSource alloc] initWithImages:images];
    imageViewController = [[FSImageViewerViewController alloc] initWithImageSource:photoSource imageIndex:indexPath.item];
    
    imageViewController.view.frame =CGRectMake(0, 60, [UIScreen mainScreen].bounds.size.width,  [UIScreen mainScreen].bounds.size.height-100);
    imageViewController.view.backgroundColor=[UIColor grayColor];
    [self.view addSubview:imageViewController.view];
    
    
    btnGalleryClose=[[UIButton alloc]initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width-35, 5, 25,25)];
    [btnGalleryClose setImage:[UIImage imageNamed:@"close.png"] forState:UIControlStateNormal];
    [btnGalleryClose addTarget:self action:@selector(closeGallery) forControlEvents:UIControlEventTouchUpInside];
    [imageViewController.view addSubview:btnGalleryClose];
}


-(void)closeGallery{
    [imageViewController.view removeFromSuperview];
}


- (void)downloadImageWithURL:(NSURL *)url completionBlock:(void (^)(BOOL succeeded, NSData *data))completionBlock
{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        if (!error) {
            completionBlock(YES, data);
        } else {
            completionBlock(NO, nil);
        }
    }];
}

-(IBAction)pressBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
