//
//  RegisterViewController.m
//  Spotlyte
//
//  Created by Admin on 17/03/16.
//  Copyright © 2016 Naveen. All rights reserved.
//

#import "RegisterViewController.h"
#import "EditPreferedVC.h"

@interface RegisterViewController ()

@end

@implementation RegisterViewController

#pragma mark - Loading Methods

- (void)viewDidLoad {
    [super viewDidLoad];

    isSelectedCheckbox = FALSE;
    
    genderPickerView.hidden=YES;
    datePickerView.hidden=YES;
    
    genderArray = [[NSMutableArray alloc]init];
    [genderArray addObject:@"Male"];
    [genderArray addObject:@"Female"];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardWillHideNotification object:nil];
    
    datePickerView.datePickerMode = UIDatePickerModeDate;
    [datePickerView setMaximumDate:[NSDate date]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
   
}

#pragma mark - PickerView Delegate Methods


-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}


- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return genderArray.count;
}


- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
   return [genderArray objectAtIndex:row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow: (NSInteger)row inComponent:(NSInteger)component
{
        genderString = [genderArray objectAtIndex:row];
}

-(IBAction)pressGender:(id)sender;
{
    NSLog(@"click state");
    [self resignKeyBoard];
    viewPicker.hidden = NO;
    genderPickerView.hidden=NO;
    viewPicker.tag=0;
}

- (IBAction)pressBirthday:(id)sender {
    NSLog(@"click state");
    [self resignKeyBoard];
    viewPicker.hidden = NO;
    datePickerView.hidden=NO;
    viewPicker.tag=1;

}


-(IBAction)pressSelect:(id)sender
{
    
    if (viewPicker.tag==0) {
        viewPicker.hidden = YES;
        genderPickerView.hidden=YES;
        if([genderString isEqualToString:@""] || genderString.length == 0)
        {
            genderString = [NSString stringWithFormat:@"%@",[genderArray objectAtIndex:0]];
        }
        txt_gender.text = genderString;
    }
    else
    {
        viewPicker.hidden = YES;
        datePickerView.hidden=YES;
        
        NSDate *myDate = datePickerView.date;
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
        [dateFormat setLocale:locale];

        [dateFormat setDateFormat:@"dd/MM/yyyy"];
        dateString = [dateFormat stringFromDate:myDate];
        txt_birthday.text = dateString;
    }
}

-(IBAction)pressCancel:(id)sender
{
    if (viewPicker.tag==0) {
        genderPickerView.hidden=YES;
    }else
        datePickerView.hidden=YES;
    
    viewPicker.hidden = YES;
}


-(IBAction)pressCheckBox:(id)sender
{
    
    UIButton *button = (UIButton *)sender;
    if(isSelectedCheckbox == FALSE)
    {
        isSelectedCheckbox = TRUE;
        [button setBackgroundImage:[UIImage imageNamed:@"check.png"] forState:UIControlStateNormal];
    }
    else
    {
        isSelectedCheckbox = FALSE;
        [button setBackgroundImage:[UIImage imageNamed:@"check-box-empty"] forState:UIControlStateNormal];

    }
}

-(IBAction)pressTermsAndConditions:(id)sender
{
    TermsViewController *termsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"termsvc"];
    [self.navigationController pushViewController:termsVC animated:YES];
}
#pragma mark - TextField Methods

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    if (textField != txt_Email)
    {
        textField.autocapitalizationType = UITextAutocapitalizationTypeWords;
    }

    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == txt_FirstName) {
        [txt_FirstName resignFirstResponder];
        [txt_LastName becomeFirstResponder];
        
    } else if (textField == txt_LastName) {
        [txt_LastName resignFirstResponder];
        [txt_Email becomeFirstResponder];
    }else if (textField == txt_Email) {
        [txt_Email resignFirstResponder];
        [txt_UserName becomeFirstResponder];
    }else if (textField == txt_UserName) {
        [txt_UserName resignFirstResponder];
        [txt_Password becomeFirstResponder];
    }else if (textField == txt_Password) {
        [txt_Password resignFirstResponder];
    }

    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    
    return YES;
}

#pragma mark - Keyboard Methods

- (void)keyboardDidShow:(NSNotification *)notification
{
    [UIView beginAnimations:@"" context:@""];
    [UIView animateWithDuration:0.1 animations:^{
        [self.view setFrame:CGRectMake(0,-110,self.view.frame.size.width,self.view.frame.size.height)];
        
    }];
    [UIView commitAnimations];
    
}

-(void)keyboardDidHide:(NSNotification *)notification
{
    [UIView beginAnimations:@"" context:@""];
    [UIView animateWithDuration:0.1 animations:^{
        [self.view setFrame:CGRectMake(0,0,self.view.frame.size.width,self.view.frame.size.height)];
    }];
    
    [UIView commitAnimations];
}

-(void)resignKeyBoard
{
    
    [txt_Email resignFirstResponder];
    [txt_FirstName resignFirstResponder];
    [txt_LastName resignFirstResponder];
    [txt_Password resignFirstResponder];
    [txt_UserName resignFirstResponder];
    
    [self keyboardDidHide:nil];
}
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}
#pragma mark - Button Action Methods

-(IBAction)pressBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)pressCreateBtn:(id)sender
{
    [self resignKeyBoard];
    
   
    
     if (APP_DELEGATE.checkReachability) {
    
    BOOL bIsSuccess          = FALSE;
    NSString *errorMessage   = nil;
    
    if([txt_FirstName.text length] == 0)
    {
        errorMessage =  @"Please enter first name.";
    }
    else if ([txt_LastName.text length] == 0)
    {
        errorMessage =  @"Please enter last name.";
    }
    else if ([txt_Email.text length] == 0)
    {
        errorMessage =  @"Please enter E-Mail ID.";
    }
    else if([SharedMethods validateEmail:txt_Email.text] == FALSE)
    {
        errorMessage = @"Please enter a valid e-mail";
    }
    else if ([txt_UserName.text length] == 0)
    {
        errorMessage =  @"Please enter user name.";
    }
    else if ([txt_Password.text length]< 6)
    {
        errorMessage = @"Password must contain at least 6 characters.";
    }
//    else if ([txt_gender.text length] == 0)
//    {
//        errorMessage = @"Please select gender.";
//    }
    else if (isSelectedCheckbox == FALSE)
    {
        errorMessage = @"Please Accept terms and conditions.";
    }

    else
    {
        bIsSuccess = TRUE;
        
        RegisterModal *userDetails = [[RegisterModal alloc]init];
        [userDetails setUserName:txt_UserName.text];
        [userDetails setFirstName:txt_FirstName.text];
        [userDetails setLastName:txt_LastName.text];
        [userDetails setPassword:txt_Password.text];
        [userDetails setEmailID:txt_Email.text];
        [userDetails setGender:txt_gender.text];
        [userDetails setDateOfBirth:txt_birthday.text];
        [userDetails setDeviceID:@"1234348348374837"];
        [userDetails setDeviceToken:@"121y7281212218281281"];
        [userDetails setDeviceType:@"iOS"];
        
        BOOL bIsNetworkAvailable = [[AppDelegate shareAppdelegateInstance] checkReachability];
        if(bIsNetworkAvailable)
        {
            [self requestRegisterProcess:userDetails];
        }
        else
            [self showAlertTitle:@"" Message:@"Please check your Internet Connection."];
    }
    
    if(!bIsSuccess)
        [self showAlertTitle:@"" Message:errorMessage];
     }
}

-(void)showAlertTitle:(NSString *)title Message:(NSString *)message
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [alertController dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [alertController addAction:okBtn];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

#pragma mark - ISL Mehtods

-(void)requestRegisterProcess:(RegisterModal *)registerObj
{
    [CUSTOMINDICATORMACRO startLoadAnimation:self];
    
    [REGISTERMACRO registerToServer:registerObj withCompletion:^(id obj1)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [CUSTOMINDICATORMACRO stopLoadAnimation:self];
            
            if([obj1 isKindOfClass:[RegisterModal class]])
            {
                RegisterModal *responseModal = obj1;
                if([responseModal.result isEqualToString:@"success"])
                {
                    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:responseModal.result message:responseModal.message preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction *okBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        [self.navigationController popViewControllerAnimated:YES];
                        [self performSelector:@selector(presentCaegoryVC:) withObject:responseModal.userID afterDelay:1.0];
                        
//                        EditPreferedVC *PreferedVC=[[EditPreferedVC alloc]initWithNibName:@"EditPreferedVC" bundle:nil];
//                        PreferedVC.strUserID = responseModal.userID;
//                        [appDelegate.window.rootViewController presentViewController:PreferedVC animated:true completion:nil];
                    }];
                    
                    [alertController addAction:okBtn];
                    [self presentViewController:alertController animated:YES completion:nil];
                }
                else
                {
                    [self showAlertTitle:@"" Message:responseModal.message];
                }
            }
            else
            {
                 [self showAlertTitle:@"" Message:@"Please check your Internet Connection."];
            }
        });
    }];
}

-(void)presentCaegoryVC:(NSString *)strUserID{
    //if ([[NSUserDefaults standardUserDefaults] boolForKey:@"isSelectPreferredCatego"] == NO)
    //{    
    EditPreferedVC *PreferedVC=[[EditPreferedVC alloc]initWithNibName:@"EditPreferedVC" bundle:nil];
    PreferedVC.strUserID = strUserID;
    [appDelegate.window.rootViewController presentViewController:PreferedVC animated:true completion:nil];
    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isSelectPreferredCatego"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    //}
}

@end
