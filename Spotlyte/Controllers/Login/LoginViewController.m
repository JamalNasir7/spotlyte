//
//  LoginViewController.m
//  GopherAssist
//
//  Created by Arunkumar Gnanasekar on 22/12/15.
//  Copyright © 2015 Colan Infotech Private Limited. All rights reserved.
//

#import "LoginViewController.h"
#import "HomeViewController.h"
#import "SWRevealViewController.h"
#import "RegisterViewController.h"
#import "ForgotPasswordViewController.h"
#import "FirstWelcomeScreen.h"
#import <TwitterKit/TwitterKit.h>
//#import "TabBarController.h"
#import "TutorialScreenController.h"
#import "EditPreferedVC.h"

@interface LoginViewController ()
@end

@implementation LoginViewController

#pragma mark - Loading Methods

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    userName_view.layer.borderWidth = 1;
    userName_view.layer.cornerRadius=5;
    userName_view.layer.borderColor = [[UIColor colorWithRed:0.0f/255.0f green:204.0f/255.0f blue:255.0f/255.0f alpha:1] CGColor];
    
    password_view.layer.borderWidth = 1;
    password_view.layer.cornerRadius=5;
    password_view.layer.borderColor = [[UIColor colorWithRed:0.0f/255.0f green:204.0f/255.0f blue:255.0f/255.0f alpha:1] CGColor];
    
    
    textField_EmailId.text=@"hemant123";
    textField_Passwords.text=@"123456";
    
    NSError* configureError;
    [[GGLContext sharedInstance] configureWithError: &configureError];
    NSAssert(!configureError, @"Error configuring Google services: %@", configureError);
    
    [GIDSignIn sharedInstance].delegate = self;
    [GIDSignIn sharedInstance].uiDelegate = self;
   
    [GIDSignIn sharedInstance].scopes=@[@"https://www.googleapis.com/auth/plus.login",
                                        @"https://www.googleapis.com/auth/userinfo.profile"];
    
    tapToDismiss = [[UITapGestureRecognizer alloc]initWithTarget: self action: @selector (dismissKeyboard:)];
    [self.view addGestureRecognizer:tapToDismiss];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dismissControllerNotification:) name:@"dismissNotification" object:nil];
}



-(void)dismissControllerNotification:(NSNotification*)notification{
    if ( [[NSUserDefaults standardUserDefaults] boolForKey:@"firstTimeLogin"]) {
        
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"firstTimeLogin"];
        /*SWRevealViewController *obj_RevealVC = [[UIStoryboard storyboardWithName:@"Main" bundle: nil] instantiateViewControllerWithIdentifier:@"SWRevealViewStoryboard"];
        [self.navigationController pushViewController:obj_RevealVC animated:YES];*/
        [appDelegate setCustomeTabbar];
        
    }else
    {
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    }
}



- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    textField_EmailId.text      = @"";
    textField_Passwords.text    = @"";
    
    [btnMail setImage:[UIImage imageNamed:@"Active_MailIcon"] forState:UIControlStateNormal];
    [btnFacebook setImage:[UIImage imageNamed:@"FbIcon"] forState:UIControlStateNormal];
    [btnTwitter setImage:[UIImage imageNamed:@"TwitterIcon"] forState:UIControlStateNormal];
    [btnGmail setImage:[UIImage imageNamed:@"Google"] forState:UIControlStateNormal];
    
    CGRect emailFrame=textField_EmailId.leftView.frame;
    emailFrame.origin.x=-40;
    textField_EmailId.leftView.frame=emailFrame;
}

- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
}


#pragma mark - Keyboard Methods

- (void)dismissKeyboard:(id)sender{
    [self.view endEditing:YES];
    tapToDismiss.cancelsTouchesInView = NO;
}


#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 216.0;
     CGFloat distNeededFromTFtoKB = 75.0;
    CGRect textFieldRect        = [self.view.window convertRect:textField.bounds fromView:textField];
    CGFloat textFieldYPosition  = textFieldRect.origin.y;
    CGFloat textFieldHeight     = textField.frame.size.height;
    CGFloat selfViewHeight      = self.view.frame.size.height;
    CGFloat selfViewWidth       = self.view.frame.size.width;
    
    [scrolView_Login setContentSize:CGSizeMake(selfViewWidth, selfViewHeight + (PORTRAIT_KEYBOARD_HEIGHT / 2))];
    
    if ((textFieldYPosition + distNeededFromTFtoKB) > (selfViewHeight - PORTRAIT_KEYBOARD_HEIGHT))
    {
        [scrolView_Login setContentOffset:CGPointMake(0, ((textFieldYPosition + textFieldHeight + distNeededFromTFtoKB) - (selfViewHeight - PORTRAIT_KEYBOARD_HEIGHT))) animated:YES];
    }
}


- (BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    return YES;
}




- (void)textFieldDidEndEditing:(UITextField *)textField{
    [scrolView_Login setContentSize:CGSizeMake(self.view.window.frame.size.width, self.view.window.frame.size.height)];
    [scrolView_Login setContentOffset:CGPointMake(0, 0) animated:YES];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    return YES;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField{
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    [scrolView_Login setContentSize:CGSizeMake(self.view.window.frame.size.width, self.view.window.frame.size.height)];
    [scrolView_Login setContentOffset:CGPointMake(0, 0) animated:YES];
    return YES;
}

#pragma mark - Button Methods

- (IBAction)LoginButtonClicked:(id)sender
{
    [self.view endEditing:YES];
    
    NSString *userName1 = textField_EmailId.text;
    NSString *password = textField_Passwords.text;
    BOOL isSuccess = FALSE;
    NSString *errorMessage = nil;
    
    if([userName1 length] == 0)
        errorMessage = @"Enter the Username";
    else if([password length] == 0)
        errorMessage = @"Enter the Password";
    else
    {
        isSuccess = TRUE;
    }
    if(!isSuccess)
    {
        [self showAlertTitle:@"" Message:errorMessage];
    }
    else
    {
        if (APP_DELEGATE.checkReachability) {
            RegisterModal *modalOBJ  =   [[RegisterModal alloc]init];
            [modalOBJ setUserName:textField_EmailId.text];
            [modalOBJ setPassword:textField_Passwords.text];
            [modalOBJ setDeviceType:@"iOS"];
            [modalOBJ setDeviceToken:@"121y7281212218281281"];
            
            BOOL bIsNetworkAvailable =[[AppDelegate shareAppdelegateInstance] checkReachability];
            if(bIsNetworkAvailable)
            {
                [self requestEmailLogin:modalOBJ];
            }
            else
                [self showAlertTitle:@"" Message:@"Please check your Internet Connection."];
        }
    }
   /* [ServiceHelper customGetRequest:@"testRequest" callback:^(id obj,NSError *error, BOOL success){
        if (obj!=nil) {
            NSLog(@"Response test == %@",obj);
        }
    }];*/
}

-(void)saveLoggedInDetails:(RegisterModal *)loginObj
{
    NSUserDefaults *userPref  = [NSUserDefaults standardUserDefaults];
    [userPref setBool:TRUE forKey:@"login"];
    
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:loginObj];
    [userPref setObject:data forKey:@"logindetails"];
    [userPref synchronize];
}

-(void)showAlertTitle:(NSString *)title Message:(NSString *)message
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [alertController dismissViewControllerAnimated:YES completion:nil];
    }];
    [alertController addAction:okBtn];
    [self presentViewController:alertController animated:YES completion:nil];
}
- (void)pushHomeView:(NSString*)LoginStatus
{
    if ([LoginStatus isEqualToString:@"0"]){
        TutorialScreenController *firstCon=[self.storyboard instantiateViewControllerWithIdentifier:@"tutorialCon"];
        APP_DELEGATE.settingEnable=YES;
        [self.navigationController pushViewController:firstCon animated:YES];
    }
    else{
        if ( [[NSUserDefaults standardUserDefaults] boolForKey:@"firstTimeLogin"]) {
           
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"firstTimeLogin"];
            /*SWRevealViewController *obj_RevealVC = [[UIStoryboard storyboardWithName:@"Main" bundle: nil] instantiateViewControllerWithIdentifier:@"SWRevealViewStoryboard"];
                 [self.navigationController pushViewController:obj_RevealVC animated:YES];*/
            
            [appDelegate setCustomeTabbar];
        }else
        {
            [self.navigationController dismissViewControllerAnimated:YES completion:nil];
        }
    }
}

-(IBAction)pressForgotPassword:(id)sender
{
    ForgotPasswordViewController *forGotVC = [self.storyboard instantiateViewControllerWithIdentifier:@"forgot"];
    [self.navigationController pushViewController:forGotVC animated:YES];
}

-(IBAction)pressRegister:(id)sender
{
    RegisterViewController *registerVC = [self.storyboard instantiateViewControllerWithIdentifier:@"register"];
    [self.navigationController pushViewController:registerVC animated:YES];
}



-(IBAction)pressSelectionBtn:(id)sender
{
    UIButton *button = (UIButton *)sender;
    int buttonTag =  (int)button.tag;
    
    switch (buttonTag) {
        case e_loginType_Email:
        {
            [btnMail setImage:[UIImage imageNamed:@"Active_MailIcon"] forState:UIControlStateNormal];
            [btnFacebook setImage:[UIImage imageNamed:@"FbIcon"] forState:UIControlStateNormal];
            [btnTwitter setImage:[UIImage imageNamed:@"TwitterIcon"] forState:UIControlStateNormal];
            [btnGmail setImage:[UIImage imageNamed:@"Google"] forState:UIControlStateNormal];
            
            break;
        }
        case e_LoginType_Facebook:
        {
            [btnMail setImage:[UIImage imageNamed:@"Mail"] forState:UIControlStateNormal];
            [btnFacebook setImage:[UIImage imageNamed:@"Active_FbIcon"] forState:UIControlStateNormal];
            [btnTwitter setImage:[UIImage imageNamed:@"TwitterIcon"] forState:UIControlStateNormal];
            [btnGmail setImage:[UIImage imageNamed:@"Google"] forState:UIControlStateNormal];
            
            [self requestFacebookLogin];
            
            break;
        }
        case e_loginType_Twitter:
        {
            [btnMail setImage:[UIImage imageNamed:@"Mail"] forState:UIControlStateNormal];
            [btnFacebook setImage:[UIImage imageNamed:@"FbIcon"] forState:UIControlStateNormal];
            [btnGmail setImage:[UIImage imageNamed:@"Google"] forState:UIControlStateNormal];
            [btnTwitter setImage:[UIImage imageNamed:@"Active_TwitterIcon"] forState:UIControlStateNormal];
            
            
            [self requestTwitterLogin];
            
            break;
        }
        
        case e_loginType_Gmail:
        {
            [btnMail setImage:[UIImage imageNamed:@"Mail"] forState:UIControlStateNormal];
            [btnFacebook setImage:[UIImage imageNamed:@"FbIcon"] forState:UIControlStateNormal];
            [btnTwitter setImage:[UIImage imageNamed:@"TwitterIcon"] forState:UIControlStateNormal];
            [btnGmail setImage:[UIImage imageNamed:@"Active_Google"] forState:UIControlStateNormal];
            //
            [self requestGmailLogin];
            
            break;
        }
        default:
            break;
    }
}

#pragma mark - Social Login Methods


-(void)requestGmailLogin{
     [[GIDSignIn sharedInstance] signIn];
}

    //====================================
#pragma mark - Gmail Delegate Methods
    //====================================
    // Stop the UIActivityIndicatorView animation that was started when the user
    // pressed the Sign In button
- (void)signInWillDispatch:(GIDSignIn *)signIn error:(NSError *)error {
    //[myActivityIndicator stopAnimating];
}
    
    // Present a view that prompts the user to sign in with Google
- (void)signIn:(GIDSignIn *)signIn
presentViewController:(UIViewController *)viewController {
    
    [self.navigationController presentViewController:viewController animated:YES completion:nil];
}
    
- (void)signIn:(GIDSignIn *)signIn
didSignInForUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
    
    NSString *request=[NSString stringWithFormat:@"https://www.googleapis.com/oauth2/v3/userinfo?access_token=%@",user.authentication.accessToken];
    [ServiceHelper getGoogleRequest:request callback:^(id data,NSError* error, BOOL success){
    
        
        if ([data isKindOfClass:[NSDictionary class]]) {
            if (!error){
                
                if ([data[@"error"] isEqualToString:@"invalid_request"]) {
                    return ;
                }
                
                
                RegisterModal *modalOBJ  =   [[RegisterModal alloc]init];
                
                //User's full name
                if(data[@"name"] != nil)
                    [modalOBJ setUserName:data[@"name"]];
                else
                    [modalOBJ setUserName:@""];
                
                //Social ID
                if(data[@"sub"] != nil)
                    [modalOBJ setSocialID:data[@"sub"]];
                else
                    [modalOBJ setSocialID:@""];
                
                //first name
                if(data[@"given_name"] != nil)
                    [modalOBJ setFirstName:data[@"given_name"]];
                else
                    [modalOBJ setFirstName:@""];
                
                //last name
                if(data[@"family_name"] != nil)
                    [modalOBJ setLastName:data[@"family_name"]];
                else
                    [modalOBJ setLastName:@""];
                
                // Email
                if(data[@"email"] != nil)
                    [modalOBJ setEmailID:data[@"email"]];
                else
                    [modalOBJ setEmailID:@""];
                
                //Gender
                if(data[@"gender"] != nil)
                    [modalOBJ setGender:data[@"gender"]];
                else
                    [modalOBJ setGender:@""];
                
                [modalOBJ setSocialName:@"Gmail"];
                [modalOBJ setDeviceID:@"3232323"];
                [modalOBJ setDeviceType:@"iOS"];
                [modalOBJ setDeviceToken:@"121y7281212218281281"];
                
                BOOL bIsNetworkAvailable =[[AppDelegate shareAppdelegateInstance] checkReachability];
                if(bIsNetworkAvailable)
                {
                    [self requestFaceBookLoginServiceHelper:modalOBJ];
                }
                else
                    [self showAlertTitle:@"" Message:@"Please check your Internet Connection."];
            }
        }
    }];
}


- (void)signIn:(GIDSignIn *)signIn
didDisconnectWithUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
    // Perform any operations when the user disconnects from app here.
    // ...
}



    // Dismiss the "Sign in with Google" view
- (void)signIn:(GIDSignIn *)signIn
dismissViewController:(UIViewController *)viewController {
    [self dismissViewControllerAnimated:YES completion:nil];
}


-(void)requestEmailLogin:(RegisterModal *)modalOBJ
{
    [CUSTOMINDICATORMACRO startLoadAnimation:self];
    [REGISTERMACRO loginToServer:modalOBJ withCompletion:^(id obj1) {
        NSLog(@"LoginToServer response === %@",obj1);
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [CUSTOMINDICATORMACRO stopLoadAnimation:self];
            
            if([obj1 isKindOfClass:[RegisterModal class]]){
                RegisterModal *responseModal = obj1;
                
                if([responseModal.result isEqualToString:@"success"]){
                    [self saveLoggedInDetails:responseModal];
                    [self pushHomeView:responseModal.login_status];
                }
                else{
                    [self showAlertTitle:@"" Message:responseModal.message]; }}
            else{
                [self showAlertTitle:@"" Message:@"Please try again."]; }
        });
    }];
}

-(void)requestFacebookLogin
{
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logInWithReadPermissions: @[@"public_profile"]
                 fromViewController:self
                            handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
                                if (error) {
                                    NSLog(@"Process error");
                                } else if (result.isCancelled) {
                                    NSLog(@"Cancelled");
                                } else {
                                    NSLog(@"Logged in");
                                    
                                    NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
                                    [parameters setValue:@"id,name,email,first_name,last_name,gender,locale,link,picture" forKey:@"fields"];
                                    
                                    [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:parameters]
                                     startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection,
                                                                  id result, NSError *error) {
                                         NSLog(@"results == %@",result);
                                         
                                         NSLog(@"email is %@", [result objectForKey:@"email"]);
                                         
                                         faceBookID      =   [result objectForKey:@"id"];
                                         firstName       =   [result objectForKey:@"first_name"];
                                         lastName        =   [result objectForKey:@"last_name"];
                                         firstLastName   =   [NSString stringWithFormat:@"%@ %@",firstName,lastName];
                                         gender          =   [result objectForKey:@"gender"];
                                         email           =   [result objectForKey:@"email"];
                                         userName        =   [result objectForKey:@"name"];
                                         
                                         RegisterModal *modalOBJ  =   [[RegisterModal alloc]init];
                                         
                                         if(userName != nil)
                                             [modalOBJ setUserName:userName];
                                         else
                                             [modalOBJ setUserName:@""];
                                         
                                         if(faceBookID != nil)
                                             [modalOBJ setSocialID:faceBookID];
                                         else
                                             [modalOBJ setSocialID:@""];
                                         
                                         if(firstName != nil)
                                             [modalOBJ setFirstName:firstName];
                                         else
                                             [modalOBJ setFirstName:@""];
                                         
                                         if(lastName != nil)
                                             [modalOBJ setLastName:lastName];
                                         else
                                             [modalOBJ setLastName:@""];
                                         
                                         if(email != nil)
                                             [modalOBJ setEmailID:email];
                                         else
                                             [modalOBJ setEmailID:@""];
                                         
                                         if(gender != nil)
                                             [modalOBJ setGender:gender];
                                         else
                                             [modalOBJ setGender:@""];
                                         
                                         [modalOBJ setSocialName:@"facebook"];
                                         
                                         [modalOBJ setDeviceID:@"3232323"];
                                         [modalOBJ setDeviceType:@"iOS"];
                                         [modalOBJ setDeviceToken:@"121y7281212218281281"];
                                         
                                         BOOL bIsNetworkAvailable =[[AppDelegate shareAppdelegateInstance] checkReachability];
                                         if(bIsNetworkAvailable)
                                         {
                                             [self requestFaceBookLoginServiceHelper:modalOBJ];
                                         }
                                         else
                                             [self showAlertTitle:@"" Message:@"Please check your Internet Connection."];
                                         
                                         
                                     }];
                                }
                            }];
}

-(void)requestFaceBookLoginServiceHelper:(RegisterModal *)modalOBJ
    {
        [CUSTOMINDICATORMACRO startLoadAnimation:self];
        
        [REGISTERMACRO faceBookLoginToServer:modalOBJ withCompletion:^(id obj1) {
            NSLog(@"LoginToServer response === %@",obj1);
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [CUSTOMINDICATORMACRO stopLoadAnimation:self];
                if([obj1 isKindOfClass:[RegisterModal class]]){
                    RegisterModal *responseModal = obj1;
                    
                    if([responseModal.result isEqualToString:@"success"]){
                        [self saveLoggedInDetails:responseModal];
                        [self pushHomeView:responseModal.login_status];
                    }
                    else {
                        [self showAlertTitle:@"" Message:responseModal.message];}
                }
                else{
                    [self showAlertTitle:@"" Message:@"Please check your Internet Connection."];}
            });
        }];
}
    
    
    
    
-(void)requestTwitterLogin
{
    
    [[Twitter sharedInstance] logInWithCompletion:^
     (TWTRSession *session, NSError *error) {
         if (session)
         {
             
             NSLog(@"signed in as %@", [session userName]);
             [[NSUserDefaults standardUserDefaults]setValue:session.userID forKey:@"twitteruserid"];
             RegisterModal *modalOBJ  =   [[RegisterModal alloc]init];
             if(session.userName != nil)
                 [modalOBJ setUserName:session.userName];
             else
                 [modalOBJ setUserName:@""];
             
             if(session.userID != nil)
                 [modalOBJ setSocialID:session.userID];
             else
                 [modalOBJ setSocialID:@""];
             
             [modalOBJ setSocialName:@"twitter"];
             [modalOBJ setDeviceID:@"3232323"];
             [modalOBJ setDeviceType:@"iOS"];
             [modalOBJ setDeviceToken:@"121y7281212218281281"];
             
             
             //// remaining
             [modalOBJ setEmailID:@""];
             [modalOBJ setGender:@""];
             [modalOBJ setLastName:@""];
             [modalOBJ setFirstName:@""];
             
             
             
             BOOL bIsNetworkAvailable =[[AppDelegate shareAppdelegateInstance] checkReachability];
             if(bIsNetworkAvailable){
                 [self requestFaceBookLoginServiceHelper:modalOBJ];}
             else
                 [self showAlertTitle:@"" Message:@"Please check your Internet Connection."];
          }
         else {
             NSLog(@"error: %@", [error localizedDescription]);
         }
     }];
}

@end
