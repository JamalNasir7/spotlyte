//
//  RegisterViewController.h
//  Spotlyte
//
//  Created by Admin on 17/03/16.
//  Copyright © 2016 Naveen. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface RegisterViewController : UIViewController<UITextFieldDelegate>
{
    IBOutlet UITextField    *txt_FirstName;
    IBOutlet UITextField    *txt_LastName;
    IBOutlet UITextField    *txt_Email;
    IBOutlet UITextField    *txt_UserName;
    IBOutlet UITextField    *txt_Password;
    IBOutlet UITextField    *txt_gender;
    IBOutlet UITextField    *txt_birthday;
    
    IBOutlet UIPickerView   *genderPickerView;
    IBOutlet UIDatePicker   *datePickerView;
    
    IBOutlet UIView         *viewPicker;

    
    NSMutableArray          *genderArray;
    NSString                *genderString;
    NSString                *dateString;
    
    BOOL                    isSelectedCheckbox;
    

}

-(IBAction)pressCreateBtn:(id)sender;
@end
