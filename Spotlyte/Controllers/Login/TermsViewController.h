//
//  TermsViewController.h
//  Spotlyte
//
//  Created by Admin on 27/05/16.
//  Copyright © 2016 Naveen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ResturantModal.h"

@interface TermsViewController : UIViewController<UIWebViewDelegate>
{
    IBOutlet UIWebView *webViewTC;
    IBOutlet UILabel *lbl_title;
}

-(IBAction)pressBack:(id)sender;

@property (nonatomic, retain) NSString *checkVC;
@property (nonatomic, retain) NSString *checkBack;
@property (nonatomic,retain) NSString *StreetFestivalURL;
@property (nonatomic ,retain)NSString *StreetFestivalTitle;
@property (nonatomic, retain) NSString *CheckUrlType;
@property (nonatomic , retain) NSString *isFromDetail;


@property (nonatomic, retain) ResturantModal *restModal_TermsVC;
@end
