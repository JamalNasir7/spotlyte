//
//  TermsViewController.m
//  Spotlyte
//
//  Created by Admin on 27/05/16.
//  Copyright © 2016 Naveen. All rights reserved.
//

#import "TermsViewController.h"
#import "HeaderView.h"
@interface TermsViewController ()
{
    HeaderView *header;
    UIActivityIndicatorView *activityIndicator;
}
@end

@implementation TermsViewController
@synthesize restModal_TermsVC,checkVC,checkBack,StreetFestivalURL,StreetFestivalTitle,isFromDetail;

///<----------------------------
#pragma mark - Initially Methods
///<----------------------------
- (void)viewDidLoad{
    [super viewDidLoad];
    header=[[HeaderView alloc]initWithTitle:@"" controller:self];
    [header addSubview:header.btnback];
    [self.view addSubview:header];
    
    webViewTC.hidden = YES;
    
    if (!activityIndicator) {
        activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        activityIndicator.frame = CGRectMake([UIScreen mainScreen].bounds.size.width/2-15, [UIScreen mainScreen].bounds.size.height/2-15, 30, 30);
    }
    [activityIndicator startAnimating];
    [self.view addSubview:activityIndicator];
    
    [self loadWebView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

///<-------------------
#pragma mark - Methods
///<-------------------
-(void)loadWebView{
    if([checkVC isEqualToString:@"promotions"]){
        if ([_CheckUrlType isEqualToString:@"SpecialLink"]){
             header.lblViewName.text = restModal_TermsVC.resturantName.uppercaseString;
            NSURL *targetURL = [NSURL URLWithString:restModal_TermsVC.webSite_Specials];
            NSURLRequest *request = [NSURLRequest requestWithURL:targetURL];
            [webViewTC loadRequest:request];
        }
        else{
            
            NSString *url;
            if ([_CheckUrlType isEqualToString:@"yelp"]){
                url=restModal_TermsVC.yelp_url;
            }
            if ([_CheckUrlType isEqualToString:@"openTable"]){
                url=restModal_TermsVC.open_table;}
            
            if ([_CheckUrlType isEqualToString:@"beerMenus"]){
                url=restModal_TermsVC.beer_menus;}
            
            if ([_CheckUrlType isEqualToString:@"GrubHub"]){
                url=restModal_TermsVC.grub_hub;}
            
            if ([_CheckUrlType isEqualToString:@"ResturantEMail"]){
                url=restModal_TermsVC.resturantEMail;}
            
            if ([url hasPrefix:@"http://"] || [url hasPrefix:@"https://"]){
                header.lblViewName.text = restModal_TermsVC.resturantName.uppercaseString;
                NSURL *targetURL = [NSURL URLWithString:url];
                NSURLRequest *request = [NSURLRequest requestWithURL:targetURL];
                [webViewTC loadRequest:request];
            }
            else{
                header.lblViewName.text = restModal_TermsVC.resturantName.uppercaseString;
                NSURL *targetURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@",url]];
                NSURLRequest *request = [NSURLRequest requestWithURL:targetURL];
                [webViewTC loadRequest:request];
            }
        }
    }else if([checkVC isEqualToString:@"streetfestival"])
    {
       NSString *url = StreetFestivalURL;
        
        if ([url hasPrefix:@"http://"] || [url hasPrefix:@"https://"]){
            header.lblViewName.text = StreetFestivalTitle;
            NSURL *targetURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@",url]];
            NSURLRequest *request = [NSURLRequest requestWithURL:targetURL];
            [webViewTC loadRequest:request];

        }else{
            header.lblViewName.text = @"Street Festival";
            NSURL *targetURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@",url]];
            NSURLRequest *request = [NSURLRequest requestWithURL:targetURL];
            [webViewTC loadRequest:request];
        }

    }
    else if ([isFromDetail isEqualToString:@"true"])
    {
        header.lblViewName.text = restModal_TermsVC.resturantName.uppercaseString;
        NSURL *targetURL = [NSURL URLWithString:restModal_TermsVC.strWebsite];
        NSURLRequest *request = [NSURLRequest requestWithURL:targetURL];
        [webViewTC loadRequest:request];
    }
    else{
        header.lblViewName.text = @"TERMS AND CONDITIONS";
        NSString *path = nil;
        path = [[NSBundle mainBundle] pathForResource:@"SpotLyte" ofType:@"pdf"];
        NSURL *targetURL = [NSURL fileURLWithPath:path];
        NSURLRequest *request = [NSURLRequest requestWithURL:targetURL];
        [webViewTC loadRequest:request];
    }
}


-(void)webViewDidStartLoad:(UIWebView *)webView{
   
    
   
}

-(void)webViewDidFinishLoad:(UIWebView *)webView{
    [self stopActivityIndicator];
       webViewTC.hidden = NO;
}

-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    [self stopActivityIndicator];
}


#pragma mark - Action Methods
-(IBAction)pressBack:(id)sender{
    if([checkBack isEqualToString:@"homevc"]){
        [[NSUserDefaults standardUserDefaults]setBool:FALSE forKey:@"ischeckedshowquickview"];}
    [self.navigationController popViewControllerAnimated:YES];
}


-(void)stopActivityIndicator{
    [activityIndicator stopAnimating];
    [activityIndicator removeFromSuperview];
    activityIndicator=nil;
}


-(void)dealloc{
    [self stopActivityIndicator];
}


@end
