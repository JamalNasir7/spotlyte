//
//  ForgotPasswordViewController.h
//  Spotlyte
//
//  Created by Admin on 17/03/16.
//  Copyright © 2016 Naveen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SharedMethods.h"
#import "RegisterModal.h"

@interface ForgotPasswordViewController : UIViewController<UITextFieldDelegate>
{
    IBOutlet UITextField    *txt_EMail;
}
-(IBAction)pressContinueBtn:(id)sender;

@end
