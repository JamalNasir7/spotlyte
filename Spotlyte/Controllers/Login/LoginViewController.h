//
//  LoginViewController.h
//  GopherAssist
//
//  Created by Arunkumar Gnanasekar on 22/12/15.
//  Copyright © 2015 Colan Infotech Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "SharedMethods.h"
#import "RegisterModal.h"
#import <Google/SignIn.h>

@interface LoginViewController : UIViewController<GIDSignInDelegate,GIDSignInUIDelegate,UITextFieldDelegate>
{
    __weak IBOutlet UIScrollView    *scrolView_Login;
    __weak IBOutlet UITextField     *textField_EmailId;
    __weak IBOutlet UITextField     *textField_Passwords;
    
    IBOutlet UIButton       *btnFacebook;
    IBOutlet UIButton       *btnTwitter;
    IBOutlet UIButton       *btnMail;
    IBOutlet UIButton       *btnGmail;
    UITapGestureRecognizer  *tapToDismiss;
    
    NSString                *firstName;
    NSString                *lastName;
    NSString                *firstLastName;
    NSString                *email;
    NSString                *faceBookID;
    NSString                *userName;
    NSString                *gender;
    
    IBOutlet UIView *password_view;
    
    IBOutlet UIView *userName_view;

}

- (IBAction)LoginButtonClicked:(id)sender;
- (IBAction)pressForgotPassword:(id)sender;
- (IBAction)pressRegister:(id)sender;
- (IBAction)pressSelectionBtn:(id)sender;


@end
