//
//  ForgotPasswordViewController.m
//  Spotlyte
//
//  Created by Admin on 17/03/16.
//  Copyright © 2016 Naveen. All rights reserved.
//

#import "ForgotPasswordViewController.h"

@interface ForgotPasswordViewController ()

@end

@implementation ForgotPasswordViewController

#pragma mark - Loading Methods

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - TextField Delegate Method

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    
    [self.view endEditing:YES];
    return YES;
}

#pragma mark - Keyboard Methods

- (void)keyboardDidShow:(NSNotification *)notification
{
    [UIView beginAnimations:@"" context:@""];
    [UIView animateWithDuration:0.1 animations:^{
        [self.view setFrame:CGRectMake(0,-110,self.view.frame.size.width,self.view.frame.size.height)];
        
    }];
    [UIView commitAnimations];
}

-(void)keyboardDidHide:(NSNotification *)notification
{
    [UIView beginAnimations:@"" context:@""];
    [UIView animateWithDuration:0.1 animations:^{
        [self.view setFrame:CGRectMake(0,0,self.view.frame.size.width,self.view.frame.size.height)];
    }];
    
    [UIView commitAnimations];
}

#pragma mark - Button Action Methods

-(IBAction)pressContinueBtn:(id)sender
{
    
     if (APP_DELEGATE.checkReachability) {
    
    [txt_EMail resignFirstResponder];
    
    BOOL isSuccess      = FALSE;
    NSString *errorMsg  = nil;
    
    if([txt_EMail.text length] == 0)
    {
        errorMsg = @"Please enter E-Mail ID.";
    }
    else if([SharedMethods validateEmail:txt_EMail.text] == FALSE)
    {
        errorMsg = @"Please enter a valid e-mail";
    }
    else
    {
        isSuccess = TRUE;
        
        BOOL bIsNetworkAvailable    = [[AppDelegate shareAppdelegateInstance] checkReachability];
        if(bIsNetworkAvailable)
        {
            [self requestForgotPasswordProcess:txt_EMail.text];
        }
        else
            [self showAlertTitle:@"" Message:@"Please check your connection"];
    }
    
    if(!isSuccess)
        [self showAlertTitle:@"" Message:errorMsg];
     }
}

-(IBAction)pressBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)showAlertTitle:(NSString *)title Message:(NSString *)message
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [alertController dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [alertController addAction:okBtn];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

#pragma mark - ISL Methods

-(void)requestForgotPasswordProcess:(NSString *)email
{
    [CUSTOMINDICATORMACRO startLoadAnimation:self];

    [REGISTERMACRO forgotPasswordToServer:email withCompletion:^(id obj1) {
        
        NSLog(@"requestForgotPasswordProcess response === %@",obj1);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [CUSTOMINDICATORMACRO stopLoadAnimation:self];
            
            if([obj1 isKindOfClass:[RegisterModal class]])
            {
                RegisterModal *responseModal = obj1;
                if([responseModal.result isEqualToString:@"success"])
                {
                    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:responseModal.result message:responseModal.message preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction *okBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        [alertController dismissViewControllerAnimated:YES completion:nil];
                        [self.navigationController popViewControllerAnimated:YES];
                    }];
                    
                    [alertController addAction:okBtn];
                    [self presentViewController:alertController animated:YES completion:nil];
                }
                else
                {
                    [self showAlertTitle:@"" Message:responseModal.message];
                }
            }
            else
            {
                [self showAlertTitle:@"" Message:@"Please check your connection"];
            }
        });
        
    } ];
}


@end
