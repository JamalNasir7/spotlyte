//
//  EventDetailsViewController.h
//  Spotlyte
//
//  Created by Admin on 18/03/16.
//  Copyright © 2016 Naveen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EventsModel.h"
#import <CoreText/CoreText.h>

@interface EventDetailsViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    IBOutlet UIImageView *asynchImage;
    
    IBOutlet UIView  *view_Special;
    IBOutlet UITableView *tableView_Special;

}

@property (nonatomic, weak) IBOutlet UILabel *lblEventName;
@property (nonatomic, weak) IBOutlet UILabel *lblEventTime;
@property (nonatomic, weak) IBOutlet UILabel *lblVenue;
@property (nonatomic, weak) IBOutlet UIImageView *imgViewEvent;
@property (nonatomic, weak) IBOutlet UITextView *txtEventDesc;
@property (nonatomic, weak) IBOutlet NSMutableArray  *daysArray;


@property (nonatomic, weak) IBOutlet NSString  *categoryString;


@property (nonatomic, strong) EventsModel *eventObj;

-(void)configEventDetails;


@end
