//
//  EventDetailsViewController.m
//  Spotlyte
//
//  Created by Admin on 18/03/16.
//  Copyright © 2016 Naveen. All rights reserved.
//

#import "EventDetailsViewController.h"
#import "DailySpecialCell.h"

@interface EventDetailsViewController ()

@end

@implementation EventDetailsViewController
@synthesize eventObj,daysArray;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self configEventDetails];
    
    tableView_Special.allowsSelectionDuringEditing=YES;
    tableView_Special.tableFooterView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, tableView_Special.frame.size.width, 0.1f)];
}


-(void)configEventDetails
{
    if(self.eventObj)
    {
        self.lblEventName.text  = self.eventObj.eventName;
        
        if([eventObj.categoryName isEqualToString:@"Happy_hour"])
        {
            self.lblEventTime.text  = eventObj.eventAddress;
            
            self.lblVenue.text      = eventObj.phoneNo;
            view_Special.hidden = NO;
        }
        else
        {
            NSString *time = [NSString stringWithFormat:@"%@ - %@",eventObj.eventStartDate,eventObj.eventEndDate];
            
            self.lblEventTime.text  = time;
            
            self.lblVenue.text      = eventObj.eventAddress;
            view_Special.hidden = YES;
        }
        
        
        asynchImage.layer.cornerRadius = 3.0;
        [asynchImage setBackgroundColor:[UIColor clearColor]];
        asynchImage.layer.masksToBounds = YES;
        dispatch_async(dispatch_get_main_queue(), ^{
            NSString *newUrl = [eventObj.eventImageStr stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
            [asynchImage sd_setImageWithURL:[NSURL URLWithString:newUrl]
                           placeholderImage:[UIImage imageNamed:@"noimage.png"]];
            
        });
        
        self.txtEventDesc.text  = eventObj.eventDescription;
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma  mark TableView Datasource and Delegate Method

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return eventObj.specialsArray.count;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSDictionary *dict = [eventObj.specialsArray objectAtIndex:section];
    NSArray *allKeys    = [dict allKeys];
    NSString *keyName   = [allKeys objectAtIndex:0];
    return keyName;
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIImageView *logo = [[UIImageView  alloc]initWithFrame:CGRectMake(20, 10, 11, 11)];
    logo.image = [UIImage imageNamed:@"Dots"];
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 30.0f)];
    [view setBackgroundColor:[UIColor clearColor]];
    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(40, 5, 150, 20)];
    NSDictionary *dict = [eventObj.specialsArray objectAtIndex:section];
    NSArray *allKeys    = [dict allKeys];
    NSString *keyName   = [allKeys objectAtIndex:0];
    NSString *sectionTitle =keyName;
    
    NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:sectionTitle];
    [attString addAttribute:(NSString*)kCTUnderlineStyleAttributeName
                      value:[NSNumber numberWithInt:kCTUnderlineStyleSingle]
                      range:(NSRange){0,[attString length]}];
    
    [lbl setTextColor:[UIColor colorWithRed:0/255.0 green:113.0/255.0 blue:201.0/255.0 alpha:1.0]];
    [view addSubview:logo];
    [view addSubview:lbl];
    lbl.attributedText = attString;
    
    return view;
    
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //  return 2;
    NSDictionary *dict = [eventObj.specialsArray objectAtIndex:section];
    NSArray *allKeys    = [dict allKeys];
    NSString *keyName   = [allKeys objectAtIndex:0];
    NSArray *array = [dict objectForKey:keyName];
    return array.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellIdentifier = @"sampleidentifier";
    DailySpecialCell *cell = (DailySpecialCell *)[tableView_Special dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if(cell == nil)
    {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"DailySpecialCell" owner:self options:nil]objectAtIndex:0];
    }
    
    
    NSDictionary *dict = [eventObj.specialsArray objectAtIndex:indexPath.section];
    NSArray *allKeys    = [dict allKeys];
    NSString *keyName   = [allKeys objectAtIndex:0];
    NSArray *array = [dict objectForKey:keyName];
    
    NSString *strSplDish = [array objectAtIndex:indexPath.row];
    cell.textLabel.text = [NSString stringWithFormat:@"     %@", strSplDish];
    [cell.textLabel setTextColor:[UIColor colorWithRed:95/255.0 green:95.0/255.0 blue:95.0/255.0 alpha:1.0]];
    cell.textLabel.numberOfLines = 0;
    
    
    cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
    [cell.textLabel setFont:[UIFont boldSystemFontOfSize:12]];
    return cell;
    
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 30;
}


#pragma mark - Button Methods

-(IBAction)pressBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
@end
