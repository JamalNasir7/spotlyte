//
//  EventViewTableCell.h
//  Spotlyte
//
//  Created by Hemant on 02/03/17.
//  Copyright © 2017 Hemant. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventViewTableCell : UITableViewCell
////////////////////////
@property (strong, nonatomic) IBOutlet UILabel *lblEventName;
@property (strong, nonatomic) IBOutlet UILabel *lblEventAddress;
@property (strong, nonatomic) IBOutlet UIButton *btnViewInfo;
@property (strong, nonatomic) IBOutlet UILabel *lblEventDesc;
@property (strong, nonatomic) IBOutlet UILabel *lblEventTime;

@property (strong, nonatomic) IBOutlet UIImageView *imgView;
@property (strong, nonatomic) IBOutlet UIView *mainView;

@end
