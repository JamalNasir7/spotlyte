//
//  EventsViewController.h
//  Spotlyte
//
//  Created by CIPL227-MOBILITY on 18/03/16.
//  Copyright © 2016 Naveen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EventsModel.h"
#import "Shared.h"

@interface EventsViewController : UIViewController <UITableViewDataSource, UITableViewDelegate,UIGestureRecognizerDelegate,EDStarRatingProtocol,CLLocationManagerDelegate,UITextFieldDelegate>
{
    IBOutlet UILabel *lblPlacesCount;
    IBOutlet UITableView * tblViewEvents;
    IBOutlet UILabel    *lblTitle;
    IBOutlet UIView     *viewPicker;
    IBOutlet UIDatePicker *datePickerView;
    IBOutlet UITextField *txtDate;
    IBOutlet UITextField *txtToDate;
    NSString *dateString;
    BOOL isFromDate;
    UITapGestureRecognizer *tap;
    IBOutlet UIView *viewEmpty;
    NSString *realDateStr;
    NSString *ToDateStr;
    NSDate *date1;
    NSDate *date2;
    IBOutlet NSLayoutConstraint *imageview_XConstraint;
    BOOL tableviewEnable;
    BOOL streetFestivalEnable;
    EDStarRating    *starRatingView;
    CLLocationManager *_locationManager;
    
    IBOutlet UIView *dateMainView;
    IBOutlet UIView *dateToView;

    
    IBOutlet UITableView *cityTable;
    IBOutlet UITextField *txtSearch;
    IBOutlet UIView *cityMainView;
}


@property (nonatomic) categoryType selectedCategory;
@property (strong, nonatomic) IBOutlet UICollectionView *collectionVw;
@property (strong, nonatomic) NSMutableArray *arrayEvents;
@property (strong, nonatomic) NSMutableArray *arrayPlaces;
@property (strong, nonatomic) NSMutableArray *placeArray;

@property (nonatomic, retain) NSString *titleString;
@property (nonatomic, retain) EventsModel *eventModalOBJ;


   /// for constrains
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *dateView_Height;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *dateMainView_Height;
//Popup
@property (strong, nonatomic) IBOutlet UIView *popupViewBG;
@property (strong, nonatomic) IBOutlet UILabel *lblTo;

@property (strong, nonatomic) IBOutlet UIView *popupView;
@property (strong, nonatomic) IBOutlet UILabel *lblpopupName;
@property (strong, nonatomic) IBOutlet UIButton *btnHppyHours;
@property (strong, nonatomic) IBOutlet UIButton *lblDailySpecial;

@property (strong, nonatomic) IBOutlet UITableView *tblPopup;
@property (strong, nonatomic) IBOutlet UIButton *btnCross;

@property (strong, nonatomic) IBOutlet UIButton *btnHappyHours;
@property (strong, nonatomic) IBOutlet UIButton *btnDailySpecials;
@property (strong, nonatomic) IBOutlet UIView *emptyView;
@property (strong, nonatomic) IBOutlet UIButton *btnReportHere;
@property (strong, nonatomic) IBOutlet UIButton *btnExpand;

@property (weak, nonatomic) IBOutlet HMSegmentedControl *viewTool;




-(IBAction)pickerValueChanged:(id)sender;
-(IBAction)pressOK:(id)sender;
-(IBAction)pressCancel:(id)sender;
-(IBAction)pressDateBtn:(id)sender;

@end
