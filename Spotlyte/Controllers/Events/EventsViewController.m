//
//  EventsViewController.m
//  Spotlyte
//
//  Created by CIPL227-MOBILITY on 18/03/16.
//  Copyright © 2016 Naveen. All rights reserved.
//

#import "EventsViewController.h"
#import "EventDetailsViewController.h"
#import "EventsModel.h"
#import "EventLocationViewController.h"
#import "BreweriesCollectionCell.h"
#import "HeaderView.h"
#import "EventDescriptionVC.h"
#import "BeerGardenCell.h"
#import "EventViewTableCell.h"
#import "PromotionsViewController.h"
#import "JTSScrollIndicator.h"
#import "DailySpecialCell.h"
#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]


@interface EventsViewController ()<CKCalendarDelegate>
{
    CKCalendarView *calendar;
    BOOL locationBool;
    NSMutableArray *tempArray;
    NSArray *arrySpecials;
    NSArray *cities;
    BOOL searchEnable;
    ResturantModal *restModal_QuickView;
    ResturantModal *selectedResturant;
}
@property (strong, nonatomic) JTSScrollIndicator *indicator;
@property(nonatomic, strong) CKCalendarView *calendar;
@property(nonatomic, strong) NSDateFormatter *dateFormatter;
@property(nonatomic, strong) NSDate *minimumDate;
@property(nonatomic, strong) NSArray *disabledDates;
@end

@implementation EventsViewController
@synthesize titleString;
@synthesize eventModalOBJ;
@synthesize calendar;
@synthesize placeArray;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _tblPopup.rowHeight = UITableViewAutomaticDimension;
    _tblPopup.estimatedRowHeight = 44.0;
    tblViewEvents.rowHeight = UITableViewAutomaticDimension;
    tblViewEvents.estimatedRowHeight = 110;
     placeArray = [[NSMutableArray alloc]init];
    self.viewTool.sectionTitles = @[@"Happy Hours",@"Daily Specials"];
    self.viewTool.selectedSegmentIndex = 0;
    self.viewTool.backgroundColor = [UIColor whiteColor];
    self.viewTool.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor colorWithRed:153.0/255.0 green:153.0/255.0 blue:153.0/255.0 alpha:1.0],NSFontAttributeName: [UIFont systemFontOfSize:14.0]};
    self.viewTool.selectedTitleTextAttributes = @{NSForegroundColorAttributeName : [UIColor colorWithRed:0.0/255.0 green:158.0/255.0 blue:224.0/255.0 alpha:1.0],NSFontAttributeName: [UIFont systemFontOfSize:14.0]};
    self.viewTool.selectionIndicatorColor = [UIColor colorWithRed:0.0/255.0 green:158.0/255.0 blue:224.0/255.0 alpha:1.0];
    self.viewTool.selectionStyle = HMSegmentedControlSelectionStyleFullWidthStripe;
    self.viewTool.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    self.viewTool.selectionIndicatorHeight = 2.0;
    [self.viewTool setIndexChangeBlock:^(NSInteger index) {
        if (index == 0){
            arrySpecials = restModal_QuickView.arrayHappyHours;
            [self showHidePopup:arrySpecials.count];
        }else{
            arrySpecials = restModal_QuickView.arraySpecials;
            [self showHidePopup:arrySpecials.count];
        }
    }];
    _popupViewBG.hidden = YES;
    _popupView.hidden=YES;
    _btnCross.hidden=YES;
    _emptyView.hidden=YES;
    
    _popupView.layer.cornerRadius = 5;
    _popupView.layer.borderWidth = 1;
    _popupView.clipsToBounds = YES;
    _popupView.layer.borderColor = [[UIColor colorWithRed:153.0f/255.0f green:153.0f/255.0f blue:153.0f/255.0f alpha:1]CGColor];
    
    
    _btnExpand.layer.cornerRadius = 5;
    _btnExpand.layer.borderWidth = 1;
    _btnExpand.clipsToBounds = YES;
    _btnExpand.layer.borderColor = [[UIColor colorWithRed:18.0f/255.0f green:166.0f/255.0f blue:233.0f/255.0f alpha:1]CGColor];

    tempArray=[NSMutableArray new];
    self.arrayEvents = [[NSMutableArray alloc] init];
    lblTitle.text = titleString;
    
   
    [self designOfTableView];
    
    NSString *newString = [titleString stringByReplacingOccurrencesOfString:@"\n" withString:@" "];
    HeaderView *header=[[HeaderView alloc]initWithTitle:newString controller:self];
    [header addSubview:header.btnback];
    [self.view addSubview:header];
    
   
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    [_collectionVw registerNib:[UINib nibWithNibName:@"BreweriesCollectionCell" bundle: nil] forCellWithReuseIdentifier:@"BreweriesCell"];
    
   // NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
   // [dateFormatter setDateFormat:@"MM-dd-yyyy"];
   // NSDate *dateVal = [NSDate date];
    //NSString *str = [dateFormatter stringFromDate:dateVal];
    txtDate.text = @"Select Date";
    //date1 = [NSDate date];
    txtToDate.text = @"Select Date";
   
    tap = [[UITapGestureRecognizer alloc]
           initWithTarget:self
           action:@selector(dismissView)];
    
    [viewPicker addGestureRecognizer:tap];
    tap.delegate = self;
    if(IS_IPHONE_6PLUS)
    {
        imageview_XConstraint.constant = 130;
    }
    else if (IS_IPHONE_5)
    {
        imageview_XConstraint.constant = 90;
    }
    
    _lblTo.hidden = YES;
    _dateView_Height.constant=0;
    dateToView.hidden = YES;
    _dateMainView_Height.constant=50;
    
    _selectedCategory =[eventModalOBJ.categoryID integerValue];
//|( _selectedCategory ==GameBars)
    //|( _selectedCategory ==StreetFestivals)
    if ((_selectedCategory ==BeerGardensRoofTops)|(_selectedCategory==StreetFestivals)){
        tableviewEnable=YES;
        if (_selectedCategory==StreetFestivals) {
            tableviewEnable=YES;
             streetFestivalEnable=YES;
             dateMainView.hidden=NO;
            dateToView.hidden = NO;
            _lblTo.hidden = NO;
            _dateView_Height.constant=30;
            _dateMainView_Height.constant=80;
        }
    }
    
    [self eventsListServiceHelper];
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    cityTable.hidden=YES;
    [txtSearch resignFirstResponder];
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    
    [tblViewEvents setContentOffset:CGPointZero animated:NO];
    [_collectionVw setContentOffset:CGPointZero animated:NO];
    
    locationBool=YES;
    BOOL tabSelected=[[NSUserDefaults standardUserDefaults] boolForKey:@"tabSelected"];
    if (tabSelected) {
       [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"tabSelected"];
       [self initializeLocationManager];
    }
    else
    {
        if (tableviewEnable)
            [tblViewEvents reloadData];
        else
            [_collectionVw reloadData];
    }
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    if (!_indicator) {
        self.indicator = [[JTSScrollIndicator alloc] initWithScrollView:cityTable];
        self.indicator.backgroundColor = [UIColor lightGrayColor];
        cityTable.contentOffset = CGPointMake(0.0, 1.0);
        [_indicator show:YES];
    }
}


- (void)initializeLocationManager {
    if(![CLLocationManager locationServicesEnabled]) {
        showAlertController(@"Enable Location Service", @"You have to enable the Location Service to use this App. To enable, please go to Settings->Privacy->Location Services", self, @"OK", nil,^(BOOL cancel, BOOL ok){
        });
        return;
    }
    
    if ([CLLocationManager authorizationStatus]==kCLAuthorizationStatusDenied){
        showAlertController(@"App Permission Denied", @"To re-enable, please go to Settings and turn on Location Service for this app.", self, @"OK", nil,^(BOOL cancel, BOOL ok){
        });
        return;
    }
    
    if (!_locationManager) {
        _locationManager = [[CLLocationManager alloc] init];
    }
    _locationManager.delegate = self;
   [_locationManager requestAlwaysAuthorization];
    _locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
    _locationManager.distanceFilter = kCLDistanceFilterNone;
    _locationManager.activityType = CLActivityTypeAutomotiveNavigation;
   [_locationManager startUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    APP_DELEGATE.userCurrentLocation=newLocation;
    
    if (APP_DELEGATE.userCurrentLocation.coordinate.latitude && APP_DELEGATE.userCurrentLocation.coordinate.longitude){
        if (locationBool){
            [self eventsListServiceHelper];
            locationBool=NO;
        }
    }
    [_locationManager stopUpdatingLocation];
    _locationManager=nil;
}
#pragma mark --- Total History ServiceHelper


-(void)getCityListServiceHelper{
    EventsModel *modal = [[EventsModel alloc]init];
    [modal setCategoryID:eventModalOBJ.categoryID];
   
    if (streetFestivalEnable) {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
        [dateFormatter setLocale:locale];

        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        realDateStr = [dateFormatter stringFromDate:date1];
        if (realDateStr == nil){
            [modal setEventTime:@""];
        }else{
            [modal setEventTime:@""];
        }
        ToDateStr = [dateFormatter stringFromDate:date2];
        if (ToDateStr == nil){
            [modal setEventToTime:@""];
        }else{
            [modal setEventToTime:@""];
        }
    }
   


    BOOL check=[[AppDelegate shareAppdelegateInstance] checkReachability];
    if(check)
    {
        [CUSTOMINDICATORMACRO startLoadAnimation:self];
        [EventsModalmacro cityListToServer:modal withCompletion:^(id obj1) {
            if ([obj1 isKindOfClass:[NSArray class]]) {
                [CUSTOMINDICATORMACRO stopLoadAnimation:self];
                cities=[obj1 mutableCopy];
                NSLog(@"getCities");
            }
        }];
    }
}

-(void)eventsListServiceHelper
{
    [tempArray removeAllObjects];
    searchEnable=NO;
    [txtSearch resignFirstResponder];
    cityTable.hidden=YES;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [defaults objectForKey:@"logindetails"];
    RegisterModal *registerModalResponeValue = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    EventsModel *modal = [[EventsModel alloc]init];
    [modal setCategoryID:eventModalOBJ.categoryID];
    [modal setUserID:registerModalResponeValue.userID];
    [modal setSearchByCity:txtSearch.text];
    
      
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [dateFormatter setLocale:locale];

    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    realDateStr = [dateFormatter stringFromDate:date1];
    
    if (realDateStr == nil){
        [modal setEventTime:@""];
        
    }else{
        [modal setEventTime:realDateStr];
    }

    ToDateStr = [dateFormatter stringFromDate:date2];
    if (ToDateStr == nil){
        [modal setEventToTime:@""];

    }else{
        [modal setEventToTime:ToDateStr];
    }
    
    BOOL check=[[AppDelegate shareAppdelegateInstance] checkReachability];
    if(check)
    {
        [CUSTOMINDICATORMACRO startLoadAnimation:self];
        
        [EventsModalmacro eventsListToServer:modal withCompletion:^(id obj1) {
            dispatch_async(dispatch_get_main_queue(), ^{
         if (obj1!=nil && ![obj1 isKindOfClass:[NSNull class]]) {
                if ([obj1 isKindOfClass:[EventsModel class]]) {
                    
                    EventsModel *event=obj1;
                    if (_arrayPlaces.count>0) {
                        [_arrayPlaces removeAllObjects];
                    }
                    
                    if (_arrayEvents.count>0) {
                        [_arrayEvents removeAllObjects];
                    }
                    
                    self.arrayEvents = [event.eventArray mutableCopy];
                    self.arrayPlaces = [event.placesArray mutableCopy];
                    
                    if(self.arrayEvents.count > 0)
                    {
                        [self getCityListServiceHelper];
                        lblPlacesCount.text=[NSString stringWithFormat:@"%lu Places Available",(unsigned long)_arrayEvents.count];
                        viewEmpty.hidden = YES;
                        if (tableviewEnable){
                            [CUSTOMINDICATORMACRO stopLoadAnimation:self];

                            dispatch_async(dispatch_get_main_queue(), ^{

                                [tblViewEvents reloadData];
                                tblViewEvents.hidden = NO;
                                
                            });

                        }
                        else{
                            [CUSTOMINDICATORMACRO stopLoadAnimation:self];
                            dispatch_async(dispatch_get_main_queue(), ^{

                                [_collectionVw reloadData];
                                [_collectionVw setHidden:NO];
                            });
                        }
                      }
                    else
                    {
                        [CUSTOMINDICATORMACRO stopLoadAnimation:self];

                        lblPlacesCount.text=[NSString stringWithFormat:@"0 Places Available"];
                        tblViewEvents.hidden = YES;
                        viewEmpty.hidden = NO;
                    }
                }
                else
                {
                    [CUSTOMINDICATORMACRO stopLoadAnimation:self];
                    [self getCityListServiceHelper];
                    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Please check your Internet Connection." preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction *okBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        [self dismissViewControllerAnimated:YES completion:nil];
                    }];
                    
                    [alertController addAction:okBtn];
                    [self presentViewController:alertController animated:YES completion:nil];
                }
                NSLog(@"man");
         }else
         {
             [self getCityListServiceHelper];
             tblViewEvents.hidden = YES;
             viewEmpty.hidden = NO;
         }
            });
        }];
    }
    
    
}

-(void)loadEventData
{
    EventsModel *eventsObj = nil;
    
    for(int index = 1; index <= 5; index++)
    {
        eventsObj = [[EventsModel alloc] init];
        
        switch (index) {
                
            case 1:
            {
                eventsObj.eventName = @"Intourist Event";
                eventsObj.eventImage = [UIImage imageNamed:@"event1.jpeg"];
                eventsObj.eventTime = @"6pm - 11pm";
                eventsObj.venue = @"Hilton Hotel, Chicago IL";
            }
                break;
            case 2:
            {
                eventsObj.eventName = @"Promenade Event";
                eventsObj.eventImage = [UIImage imageNamed:@"event2.jpeg"];
                eventsObj.eventTime = @"6pm - 11pm";
                eventsObj.venue = @"Hilton Hotel, Chicago IL";
            }
                break;
            case 3:
            {
                eventsObj.eventName = @"Cardose Event";
                eventsObj.eventImage = [UIImage imageNamed:@"event3.jpeg"];
                eventsObj.eventTime = @"6pm - 11pm";
                eventsObj.venue = @"Hilton Hotel, Chicago IL";
            }
                break;
            case 4:
            {
                eventsObj.eventName = @"Royal Hotel Event";
                eventsObj.eventImage = [UIImage imageNamed:@"event4.jpeg"];
                eventsObj.eventTime = @"6pm - 11pm";
                eventsObj.venue = @"Hilton Hotel, Chicago IL";
            }
                break;
            case 5:
            {
                eventsObj.eventName = @"Bar One Event";
                eventsObj.eventImage = [UIImage imageNamed:@"event5.jpeg"];
                eventsObj.eventTime = @"6pm - 11pm";
                eventsObj.venue = @"Hilton Hotel, Chicago IL";
            }
                break;
                
            default:
                break;
        }
        
        [self.arrayEvents addObject:eventsObj];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)designOfTableView{
    cityTable.layer.borderWidth = 1;
    cityTable.layer.cornerRadius = 5;
    cityTable.clipsToBounds = YES;
    cityTable.layer.borderColor = [[UIColor colorWithRed:234.0/255.0 green:234.0/255.0 blue:234.0/255.0 alpha:1.0]CGColor];
    
    cityTable.allowsSelectionDuringEditing=YES;
    cityTable.tableFooterView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, cityTable.frame.size.width, 0.1f)];
    cityTable.hidden = YES;
}

///<---------------------------------------
#pragma mark --- TextField Delegate Method
///<---------------------------------------

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    searchEnable=YES;
    if(cities.count>0){
        cityTable.hidden = NO;
        [cityTable reloadData];
    
        dispatch_async(dispatch_get_main_queue(), ^{
            [cityTable setContentOffset:CGPointZero animated:YES];
        });
    }
    else{
        cityTable.hidden = YES;
        searchEnable=NO;
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"No city available for this Category." preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self dismissViewControllerAnimated:YES completion:nil];
        }];
        [alertController addAction:okBtn];
        [self presentViewController:alertController animated:YES completion:nil];
           [txtSearch resignFirstResponder];
    }
}


-(BOOL)textFieldShouldClear:(UITextField *)textField{
    textField.text = @"";
    [textField resignFirstResponder];
    searchEnable=YES;
    cityTable.hidden = YES;
    [self eventsListServiceHelper];
    return NO;
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (textField.text.length>0) {
        [self eventsListServiceHelper];
    }
    [textField resignFirstResponder];
       return YES;
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(txtSearch.text.length == 1 && [string isEqualToString:@""]){
        cityTable.hidden = YES;
        txtSearch.text = @"";
        [self eventsListServiceHelper];
        [txtSearch resignFirstResponder];
    }else{
        cityTable.hidden = NO;
        NSString *substring = [NSString stringWithString:textField.text];
        substring = [substring stringByReplacingCharactersInRange:range withString:string];
        
        [self searchByEvents:substring];
    }
    return YES;
}


- (void)searchByEvents:(NSString *)searchStr{
    
    if([searchStr length] > 0){
        [tempArray removeAllObjects];
        
        if (cities.count>0) {
            for (NSString *city in cities){
                NSRange range=[city rangeOfString:searchStr options:(NSCaseInsensitiveSearch)];
                
                if(range.location != NSNotFound)
                    [tempArray addObject:city];
            }
        }
    }
    else
        [tempArray removeAllObjects];
    
    if(tempArray.count>0){
        cityTable.hidden = NO;
        [cityTable reloadData];}
    else{
        cityTable.hidden = YES;
        searchEnable=NO;
    }
    [self scrollViewDidScroll:cityTable];
}

#pragma mark - TableView Delegates
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == _tblPopup)
    {
        return arrySpecials.count;
    }
        
    return searchEnable ?[tempArray count]>0?[tempArray count]:cities.count>0?cities.count:0:[self.arrayEvents count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if ([tableView isEqual:_tblPopup]) {
        if (tableView == _tblPopup)
        {
            
            static NSString *cellIdentifier = @"sampleidentifier";
            DailySpecialCell *cell = (DailySpecialCell *)[_tblPopup dequeueReusableCellWithIdentifier:cellIdentifier];
            
            if(cell == nil)
            {
                cell = [[[NSBundle mainBundle]loadNibNamed:@"DailySpecialCell" owner:self options:nil]objectAtIndex:0];
            }
            
            
            if (arrySpecials.count>0) {
                ResturantModal *modelDailySpecialDetails = [arrySpecials objectAtIndex:indexPath.row];
                
                
                _btnExpand.tag=indexPath.row;
                _btnReportHere.tag=indexPath.row;
                
                NSString *mystr;
                
                if([modelDailySpecialDetails.specials_weekDay isEqualToString:@"monday"])
                {
                    mystr=@"Monday";
                }
                else if ([modelDailySpecialDetails.specials_weekDay isEqualToString:@"tuesday"])
                {
                    mystr=@"Tuesday";
                }
                else if ([modelDailySpecialDetails.specials_weekDay isEqualToString:@"wednesday"])
                {
                    mystr=@"Wednesday";
                }
                else if ([modelDailySpecialDetails.specials_weekDay isEqualToString:@"thursday"])
                {
                    mystr=@"Thursday";
                }
                else if ([modelDailySpecialDetails.specials_weekDay isEqualToString:@"friday"])
                {
                    mystr=@"Friday";
                }
                else if ([modelDailySpecialDetails.specials_weekDay isEqualToString:@"saturday"])
                {
                    mystr=@"Saturday";
                }
                else
                {
                    mystr=@"Sunday";
                }
                
                
                
                NSString *trimmedString = [modelDailySpecialDetails.title stringByTrimmingCharactersInSet:
                                           [NSCharacterSet whitespaceCharacterSet]];
                
                NSString *stringStartTime;
                
                NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
                NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
                [dateFormatter1 setLocale:locale];

                dateFormatter1.dateFormat = @"HH:mm:ss";
                
                NSDate *date1 = [dateFormatter1 dateFromString:modelDailySpecialDetails.startTime];
                
                
                
                dateFormatter1.dateFormat = @"hh:mm a";
                
                stringStartTime = [dateFormatter1 stringFromDate:date1];
                
                stringStartTime = [stringStartTime stringByReplacingOccurrencesOfString:@":00" withString:@""];
                
                
                int stringStartTimeInt=[stringStartTime intValue];
                
                if (stringStartTimeInt<10)
                {
                    NSRange range = NSMakeRange(0,1);
                    
                    stringStartTime = [stringStartTime stringByReplacingCharactersInRange:range withString:@""];
                }
                else
                {
                    stringStartTime = [stringStartTime stringByReplacingOccurrencesOfString:@":00" withString:@""];
                }
                
                
                NSString *stringCloseTime;
                
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                NSLocale *locale2 = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
                [dateFormatter setLocale:locale2];

                dateFormatter.dateFormat = @"HH:mm:ss";
                
                NSDate *date = [dateFormatter dateFromString:modelDailySpecialDetails.endTime];
                
                
                
                dateFormatter.dateFormat = @"hh:mm a";
                
                stringCloseTime = [dateFormatter stringFromDate:date];
                
                stringCloseTime = [stringCloseTime stringByReplacingOccurrencesOfString:@":00" withString:@""];
                
                
                
                int stringCloseTimeInt=[stringCloseTime intValue];
                
                if (stringCloseTimeInt<10)
                {
                    NSRange range = NSMakeRange(0,1);
                    
                    stringCloseTime = [stringCloseTime stringByReplacingCharactersInRange:range withString:@""];
                }
                else
                {
                    stringCloseTime = [stringCloseTime stringByReplacingOccurrencesOfString:@":00" withString:@""];
                }
                
                
                
                if ([modelDailySpecialDetails.price isEqualToString:@"N/A"] || [modelDailySpecialDetails.price isEqualToString:@"1/2 Off"] || [modelDailySpecialDetails.price isEqualToString:@"Other"] || [modelDailySpecialDetails.price isEqualToString:@"FREE"])
                {
                    
                    cell.lblDays.text=mystr;
                    NSString *completeString = [NSString stringWithFormat:@"%@ %@ (%@-%@)",modelDailySpecialDetails.price,trimmedString,stringStartTime,stringCloseTime];
                    NSMutableAttributedString *AttributedString = [[NSMutableAttributedString alloc] initWithString:completeString];
                    NSString *boldString =modelDailySpecialDetails.price;
                    NSRange boldRange = [completeString rangeOfString:boldString];
                    [AttributedString addAttribute: NSFontAttributeName value:CalibriBold(11.0f) range:boldRange];
                    [cell.lblDescription setAttributedText: AttributedString];
                    
                }
                else
                {
                    cell.lblDays.text=mystr;
                    NSString *price;
                    if ([modelDailySpecialDetails.price rangeOfString:@"$"].location == NSNotFound) {
                        price = [NSString stringWithFormat:@"$%@",modelDailySpecialDetails.price];
                    }else
                    {
                        price = [NSString stringWithFormat:@"%@",modelDailySpecialDetails.price];
                    }
                    
                    cell.lblDescription.text=[NSString stringWithFormat:@"%@ %@ (%@-%@)",price, trimmedString,stringStartTime,stringCloseTime];
                    
                    
                }
                
                if(modelDailySpecialDetails.isCheckedFavSpecial == NO)
                {
                    cell.btnFavSpecial.selected = false;
                }
                else{
                    cell.btnFavSpecial.selected = true;
                }
                
                [cell.btnFavSpecial addTarget:self action:@selector(btnFavSpecialClick:) forControlEvents:UIControlEventTouchUpInside];
                cell.btnFavSpecial.tag = indexPath.row;
                
                cell.textLabel.numberOfLines = 2;
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                cell.viewSeperator.backgroundColor = [UIColor colorWithRed:153.0f/255.0f green:153.0f/255.0f blue:153.0f/255.0f alpha:1];
                
            }
            
            
            return cell;
            
        }
        
    }
    if (searchEnable) {
        static NSString *cellIdentifier = @"identifier";
        UITableViewCell *cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
        if(cell == nil)
        {
            cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        }
        if(tempArray.count>0)
        {
            NSString *modal = [tempArray objectAtIndex:indexPath.row];
            cell.textLabel.text = modal;
        }else
            if (cities.count>0) {
                NSString *modal = [cities objectAtIndex:indexPath.row];
                cell.textLabel.text = modal;
            }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    else if (_selectedCategory==StreetFestivals){
     static NSString *simpleTableIdentifier  = @"streetFestivalCell";
    
        EventViewTableCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        
        if (cell == nil) {
            cell = [[EventViewTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
        }

        if([self.arrayEvents count] > 0){
            EventsModel *eventObj = [self.arrayEvents objectAtIndex:indexPath.row];
          //  ResturantModal *modalRes = [self.arrayPlaces objectAtIndex:indexPath.row];
            
            
            cell.imgView.layer.cornerRadius = 3.0;
            [cell.imgView setBackgroundColor:[UIColor clearColor]];
            cell.imgView.layer.masksToBounds = YES;
            dispatch_async(dispatch_get_main_queue(), ^{
                NSURL *url = [[NSBundle mainBundle] URLForResource:@"loading-1" withExtension:@"gif"];
                NSString *newUrl = [eventObj.eventImageStr stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
                [cell.imgView sd_setImageWithURL:[NSURL URLWithString:newUrl]
                                placeholderImage:[UIImage imageNamed:@"noimage.png"]];
            });
            
            cell.clipsToBounds=YES;
            cell.mainView.layer.borderWidth = 0.5;
            cell.mainView.layer.borderColor = [[UIColor darkGrayColor] CGColor];
            
            //start_date" = "03-23-2017  12:00 am"
            
            NSDateFormatter *dateFormatter3 = [[NSDateFormatter alloc] init];
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
            [dateFormatter3 setLocale:locale];

            [dateFormatter3 setDateStyle:NSDateFormatterMediumStyle];
            [dateFormatter3 setDateFormat:@"MM-dd-yyyy  HH:mm a"];
            NSDate *date = [dateFormatter3 dateFromString:eventObj.eventStartDate];
            NSDate *date2 = [dateFormatter3 dateFromString:eventObj.eventEndDate];

            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            [formatter setDateFormat:@"EEEE, MMM dd"];
            NSString *changedTime =[formatter stringFromDate:date];
            NSString *EndTime = [formatter stringFromDate:date2];
            
            if (![changedTime isEqualToString:EndTime])
            {
                cell.lblEventAddress.text=[NSString stringWithFormat:@"%@ - %@",changedTime,EndTime];
            }else{
                cell.lblEventAddress.text=[NSString stringWithFormat:@"%@",changedTime];
            }
          //  NSString *trimmedString = [modalRes.resturantCity stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
         //   NSString *city=[NSString stringWithFormat:@"%@,",trimmedString];
          //  NSString *state=modalRes.resturantState;
            cell.lblEventDesc.numberOfLines = 3;
            cell.lblEventDesc.text = [NSString stringWithFormat:@"%@",eventObj.eventDescription];
            
            cell.lblEventName.text=eventObj.eventName?eventObj.eventName:@"";
            cell.lblEventTime.text = [NSString stringWithFormat:@"%@",eventObj.eventAddress];
            cell.btnViewInfo.tag = indexPath.row;
            [cell.btnViewInfo setTitle:@"View Website" forState:UIControlStateNormal];
            [cell.btnViewInfo addTarget:self action:@selector(tapOnViewInfo:) forControlEvents:UIControlEventTouchUpInside];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    else{
        static NSString *simpleTableIdentifier = @"beerCell";
        BeerGardenCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        
        if (cell == nil) {
            cell = [[BeerGardenCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
        }
        
        if([self.arrayEvents count] > 0){
            EventsModel *eventObj  = [self.arrayEvents objectAtIndex:indexPath.row];
            ResturantModal *modalRes  =  [self.arrayPlaces objectAtIndex:indexPath.row];
            
            
             cell.imgView.layer.cornerRadius = 3.0;
             [cell.imgView setBackgroundColor:[UIColor clearColor]];
             cell.imgView.layer.masksToBounds = YES;
           
            
            dispatch_async(dispatch_get_main_queue(), ^{
                NSURL *url = [[NSBundle mainBundle] URLForResource:@"loading-1" withExtension:@"gif"];
                NSString *newUrl;
                if ([eventObj.eventImageStr isEqualToString:@""]){
                    newUrl = [modalRes.placeImage stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
                }
                else{
                    newUrl = [eventObj.eventImageStr stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
                }
                
                [cell.imgView sd_setImageWithURL:[NSURL URLWithString:newUrl]
                             placeholderImage:[UIImage imageNamed:@"noimage.png"]];
                
                
                cell.lblName.text=modalRes.resturantName;
                
                
                NSString *trimmedString = [modalRes.resturantCity stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                NSString *city=[NSString stringWithFormat:@"%@,",trimmedString];
                NSString *state=modalRes.resturantState;
                NSString *zipcode=modalRes.resturantPostalCode;
                
                cell.lblAddress.text=[NSString stringWithFormat:@"%@\n%@ %@ %@",modalRes.resturantAddress,[modalRes.resturantCity isEqualToString:@""]?@"":city,[state isEqualToString:@""]?@"":state,[zipcode isEqualToString:@""]?@"":zipcode];
                
                if([city isEqualToString:@"Chicago,"])
                    cell.lblAreaName.text=@"";
                else
                    cell.lblAreaName.text=@"";
                
                starRatingView = cell.starRating;
                [self displayStar:modalRes.placeRatings];
                
                cell.lblMiles.text=[NSString stringWithFormat:@"%.1f miles",[modalRes.mile floatValue]];
                
                cell.lblReviews.text=[modalRes.totalReview isEqualToString:@"1"]?@"1 review":[modalRes.totalReview isEqualToString:@""]?@"0 reviews":[NSString stringWithFormat:@"%@ reviews",modalRes.totalReview];;
                cell.lblRating.text=modalRes.placeRatings?modalRes.placeRatings:@"N/A";
                if ([modalRes.placeRatings isEqualToString:@"0"])
                {
                    cell.lblRating.text=@"N/A";

                }else{
                    cell.lblRating.text=modalRes.placeRatings;

                }
                cell.btnFavourite.tag=indexPath.row;
                [cell.btnFavourite setImage:[UIImage imageNamed:modalRes.isCheckedFavourite?@"FavSelLIst":@"FavUnSelLIst"] forState:UIControlStateNormal];
                
                cell.borderView.layer.borderColor=[[UIColor lightGrayColor] CGColor];
                cell.borderView.layer.borderWidth=1.0f;
             });
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
         return cell;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (cityTable.hidden==NO) {
        NSArray *arry=tempArray.count>0?tempArray:cities;
        NSString *city = [arry objectAtIndex:indexPath.row];
        txtSearch.text=city;
        [self eventsListServiceHelper];
    }
    else
    if (_selectedCategory == GameBars | _selectedCategory == BeerGardensRoofTops) {
        ResturantModal *modal=_arrayPlaces[indexPath.row];
        EventsModel *eventObj  = [self.arrayEvents objectAtIndex:indexPath.row];
        
        PromotionsViewController *promtionVC = [self.storyboard instantiateViewControllerWithIdentifier:@"promtions"];
        promtionVC.selectedValue = indexPath.row;
        promtionVC.placeIDString = [NSString stringWithFormat:@"%@",modal.placeID ];
       // promtionVC.strEventImage = eventObj.eventImageStr;
        promtionVC.promtionsArray = _arrayPlaces ;
        promtionVC.selectedWhichController = @"eventSection";
        promtionVC.checkTextOrSearch = @"customsearch";
        [self.navigationController pushViewController:promtionVC animated:YES];}
    
    else
        if (_selectedCategory == StreetFestivals){
            NSLog(@"Street Festival");
        }
    else{
       EventLocationViewController *eventVC = [self.storyboard instantiateViewControllerWithIdentifier:@"eventlocationvc"];
       eventVC.eventObj = [self.arrayEvents objectAtIndex:indexPath.row];
      [self.navigationController pushViewController:eventVC animated:YES];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == _tblPopup)
    {
        return UITableViewAutomaticDimension;
    }
    if (searchEnable)
        return  UITableViewAutomaticDimension;
    else{
        if (streetFestivalEnable)
            return UITableViewAutomaticDimension;
        else
            return 195;
    }
}

///<------------------------------
#pragma mark Scroll View Methods
///<------------------------------
- (void)scrollViewDidScroll:(UITableView *)scrollView {
  
    if (searchEnable && [scrollView isEqual: tblViewEvents]) {
        cityTable.hidden=YES;
        searchEnable=NO;
        [txtSearch resignFirstResponder];
    }
    [self.indicator scrollViewDidScroll:cityTable];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    [self.indicator scrollViewDidEndDragging:cityTable willDecelerate:decelerate];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    [self.indicator scrollViewDidEndDecelerating:cityTable];
}

- (void)scrollViewWillScrollToTop:(UIScrollView *)scrollView {
    [self.indicator scrollViewWillScrollToTop:cityTable];
}

- (void)scrollViewDidScrollToTop:(UIScrollView *)scrollView {
    [self.indicator scrollViewDidScrollToTop:cityTable];
}

///<---------------------------
#pragma mark CollectionView methods
///<---------------------------

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;}


-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _arrayPlaces.count;
}


-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellIdentifier=@"BreweriesCell";
    BreweriesCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];

    
    if (_arrayEvents.count>0) {
        EventsModel *modal = [_arrayEvents objectAtIndex:indexPath.row];
        ResturantModal *modalRes =[_arrayPlaces objectAtIndex:indexPath.row];

        cell.imgView.layer.cornerRadius = 3.0;
        [cell.imgView setBackgroundColor:[UIColor blackColor]];
        cell.imgView.layer.masksToBounds = YES;
        
        dispatch_async(dispatch_get_main_queue(), ^{

            //NSString *newUrl;
//            if ([modal.eventImageStr rangeOfString:@".jpg"].location==NSNotFound) {
//                
//                NSString *newstring = [modal.eventImageStr stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
//                
//                NSString *changeUrl =[newstring stringByReplacingOccurrencesOfString:@"http://103.231.46.90:8433/" withString:@"http://192.168.88.2:8433/"];
//                
//                newUrl=[NSString stringWithFormat:@"%@.jpg",changeUrl];
//                
//            }else
//            {
//                NSString *changeUrl =[modal.eventImageStr stringByReplacingOccurrencesOfString:@"http://103.231.46.90:8433/" withString:@"http://192.168.88.2:8433/"];
//              newUrl = [changeUrl stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
//            }
//            
            
            NSString *newUrl = [modalRes.placeImage stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
            
            NSURL *url = [[NSBundle mainBundle] URLForResource:@"loading-1" withExtension:@"gif"];
            [cell.imgView sd_setImageWithURL:[NSURL URLWithString:newUrl]
                            placeholderImage:[UIImage imageNamed:@"noimage.png"]];
            
            cell.btnFavourite.tag=indexPath.item;
             [cell.btnFavourite addTarget:self action:@selector(pressFavouriteBtn:) forControlEvents:UIControlEventTouchUpInside];
             [cell.btnFavourite setImage:[UIImage imageNamed:modalRes.isCheckedFavourite?@"FavSelLIst":@"FavUnSelLIst"] forState:UIControlStateNormal];
         });
        
        cell.lblPlaceName.text=modalRes.resturantName;
        cell.lblResName.text = modal.eventName;
    
        [cell.btnSpecials setTitle:[NSString stringWithFormat:@"%@ specials",modalRes.place_specials_count] forState:UIControlStateNormal];

        NSString *trimmedString = [modalRes.resturantCity stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        NSString *city=[NSString stringWithFormat:@"%@,",trimmedString];
        NSString *state=modalRes.resturantState;
        NSString *zipcode=modalRes.resturantPostalCode;
        
        cell.lblAddress.text=[NSString stringWithFormat:@"%@\n%@ %@ %@",modalRes.resturantAddress,[modalRes.resturantCity isEqualToString:@""]?@"":city,[state isEqualToString:@""]?@"":state,[zipcode isEqualToString:@""]?@"":zipcode];
        cell.lblLike.text=[modalRes.likecount isEqualToString:@"1"]?@"1 Like":[modalRes.likecount isEqualToString:@""]?@"0 Likes":[NSString stringWithFormat:@"%@ Likes",modalRes.likecount];
        cell.btnLike.tag = indexPath.row;
        if([modalRes.likeORDislike isEqualToString:@"2"] || [modalRes.likeORDislike isEqualToString:@""]){
            
            [cell.btnLike setTitle:@"Like" forState:UIControlStateNormal];
            [cell.btnLike setImage:[UIImage imageNamed:@"like"] forState:UIControlStateNormal];
        }
        else{
            [cell.btnLike setTitle:@"Dislike" forState:UIControlStateNormal];
            [cell.btnLike setImage:[UIImage imageNamed:@"disLike"] forState:UIControlStateNormal];
        }
        [cell.btnLike addTarget:self action:@selector(tapOnLike:) forControlEvents:UIControlEventTouchUpInside];
    
        cell.lblMiles.text=[NSString stringWithFormat:@"%.1f miles",[modalRes.mile floatValue]];
        
        cell.lblReviews.text=[modalRes.totalReview isEqualToString:@"1"]?@"1 review":[modalRes.totalReview isEqualToString:@""]?@"0 reviews":[NSString stringWithFormat:@"%@ reviews",modalRes.totalReview];
        int specialCount=[modalRes.happy_hour_count intValue]+[modalRes.place_specials_count intValue];

        
        if ([modalRes.placeRatings isEqualToString:@"0"])
        {
            cell.lblOverallRating.text=@"N/A";
        }
        else{
            cell.lblOverallRating.text=modalRes.placeRatings?modalRes.placeRatings:@"N/A";

        }
        [cell.btnPressReport addTarget:self action:@selector(pressReportHereBtn_cell:) forControlEvents:UIControlEventTouchUpInside];
        cell.btnPressReport.tag = indexPath.item;
        
        [cell.btnSpecials setTitle:[NSString stringWithFormat:@"%d specials",specialCount] forState:UIControlStateNormal];
        dispatch_async(dispatch_get_main_queue(), ^{
            cell.btnSpecials.userInteractionEnabled=specialCount==0?NO:YES;
        });
        [cell.btnSpecials addTarget:self action:@selector(tapOnSpecial:) forControlEvents:UIControlEventTouchUpInside];
        cell.btnSpecials.tag=indexPath.item;
         starRatingView = cell.EDStar_userRating;
        [self displayStar:modalRes.placeRatings];
        
        modal=nil;
        modalRes=nil;
    }
     return cell;
}


-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if ((_selectedCategory ==Breweries)|( _selectedCategory ==FoodDrinkChallenges)) {
        EventDescriptionVC *eventVC = [self.storyboard instantiateViewControllerWithIdentifier:@"eventDescription"];
        eventVC.eventObj = [self.arrayEvents objectAtIndex:indexPath.row];
        eventVC.restObj=_arrayPlaces[indexPath.row];
        eventVC.placeInfo=@{@"selectedTag":[NSString stringWithFormat:@"%ld",(long)indexPath.row],
                            @"placeArray":_arrayPlaces};
        [self.navigationController pushViewController:eventVC animated:YES];
    }else
    {
        ResturantModal *modal=_arrayPlaces[indexPath.row];
        PromotionsViewController *promtionVC = [self.storyboard instantiateViewControllerWithIdentifier:@"promtions"];
        promtionVC.selectedValue = indexPath.row;
        promtionVC.placeIDString = [NSString stringWithFormat:@"%@",modal.placeID ];
        promtionVC.promtionsArray = _arrayPlaces ;
        promtionVC.selectedWhichController = @"eventSection";
        promtionVC.checkTextOrSearch = @"customsearch";
        [self.navigationController pushViewController:promtionVC animated:YES];

    }
}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    float cellWidth =screenRect.size.width;
   // float cellHeight = screenRect.size.height/3.5-10;
    float cellHeight=115;
    CGSize size = CGSizeMake(cellWidth,cellHeight);
    return size;
}


-(IBAction)tapOnViewInfo:(UIButton*)sender{
    
    EventsModel *eventObj = [self.arrayEvents objectAtIndex:sender.tag];

    TermsViewController *termsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"termsvc"];
    termsVC.checkVC = @"streetfestival";
    termsVC.StreetFestivalURL = eventObj.EventWebsite;
    termsVC.StreetFestivalTitle = eventObj.eventName;
    NSLog(@"%@",eventObj.EventWebsite);
   // termsVC.checkBack = @"homevc";
   // termsVC.CheckUrlType=@"SpecialLink";
   // termsVC.restModal_TermsVC = restModal_DidSelectMap;
    [self.navigationController pushViewController:termsVC animated:YES];
}
-(IBAction)pressFavouriteBtn:(UIButton*)sender
{
    UIButton *btnFavourite=sender;
    
    ResturantModal *modal = [_arrayPlaces objectAtIndex:sender.tag];;
    NSArray *filteredArray = [_arrayPlaces filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"placeID ==%@",modal.placeID]];
    if(filteredArray>0)
        modal = [filteredArray firstObject];
    
    NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:@"logindetails"];
    RegisterModal *registerModalResponeValue = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    if(modal.isCheckedFavourite == NO)
    {
        BOOL check=[[AppDelegate shareAppdelegateInstance] checkReachability];
        if(check)
        {
            [modal setUserID:registerModalResponeValue.userID];
            [CUSTOMINDICATORMACRO startLoadAnimation:self];
            [RestaurantMacro favouriteToServer:modal withCompletion:^(id obj1) {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    ResturantModal *responseModal = nil;
                    if([obj1 isKindOfClass:[ResturantModal class]]){
                        responseModal = obj1;
                        if([responseModal.result isEqualToString:@"success"]){
                            [sender setImage:[UIImage imageNamed:@"FavSelLIst" ] forState:UIControlStateNormal];
                            [modal setIsCheckedFavourite:YES];}
                    }
                    else
                        [self serverError];
                    
                    [CUSTOMINDICATORMACRO stopLoadAnimation:self];
                });
            }];
        }
    }
    else
    {
        [modal setUserID:registerModalResponeValue.userID];
        
        BOOL check=[[AppDelegate shareAppdelegateInstance] checkReachability];
        if(check)
        {
            [CUSTOMINDICATORMACRO startLoadAnimation:self];
            [RestaurantMacro unFavouriteToServer:modal withCompletion:^(id obj1) {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [CUSTOMINDICATORMACRO stopLoadAnimation:self];
                    ResturantModal *responseModal =  nil;
                    if([obj1 isKindOfClass:[ResturantModal class]])
                    {
                        responseModal = obj1;
                        if([responseModal.result isEqualToString:@"success"])
                        {
                            [btnFavourite setImage:[UIImage imageNamed:@"FavUnSelLIst" ] forState:UIControlStateNormal];
                            [modal setIsCheckedFavourite:NO];
                        }
                    }
                    else
                        [self serverError];
                });
            }];
        }
    }
}

- (void)displayStar :(NSString *)ratingVal
{
    if(ratingVal.floatValue <= 2.0)
    {
        starRatingView.starHighlightedImage = [UIImage imageNamed:@"RedStarIcon"];
    }
    else if(ratingVal.floatValue <= 3.9)
    {
        starRatingView.starHighlightedImage = [UIImage imageNamed:@"YellowStarIcon"];
    }
    else
    {
        starRatingView.starHighlightedImage = [UIImage imageNamed:@"GreenStarIcon"];
    }
    
    starRatingView.starImage = [UIImage imageNamed:@"UnStarIcon"];
    starRatingView.maxRating = 5.0;
    starRatingView.delegate = self;
    starRatingView.horizontalMargin = 10;
    starRatingView.editable = NO;
    starRatingView.rating = ratingVal.floatValue;
    starRatingView.displayMode = EDStarRatingDisplayAccurate;
}



#pragma mark - Button Action Methods


- (IBAction)pressNavigateSpecialHours:(UIButton*)sender
{
    PromotionsViewController *promtionVC = [self.storyboard instantiateViewControllerWithIdentifier:@"promtions"];
    promtionVC.selectedValue = sender.tag;
    promtionVC.placeIDString = selectedResturant.placeID;
    promtionVC.promtionsArray = _arrayPlaces;
    promtionVC.selectedWhichController = @"specialshappyhours";
    
    BOOL isCheckedRandomSearch = [[NSUserDefaults standardUserDefaults]boolForKey:@"randomsearch"];
    if(isCheckedRandomSearch == TRUE)
    {
        promtionVC.checkTextOrSearch = @"customsearch";
    }
    else
    {
        promtionVC.checkTextOrSearch = @"nosearch";
        [[NSUserDefaults standardUserDefaults]setBool:FALSE forKey:@"randomsearch"];
    }
    [self.navigationController pushViewController:promtionVC animated:YES];
    
    /*promtionVC.selectHomeVC=^(ResturantModal *restModel){
     
     promotionModalOBJ = restModel;
     };*/
}

- (void)btnFavSpecialClick:(id)sender {
    UIButton *btn = (UIButton*)sender;
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:btn.tag inSection:0];
    
    DailySpecialCell *cell = (DailySpecialCell *)[_tblPopup cellForRowAtIndexPath:indexPath];
    
    ResturantModal *modelDailySpecialDetails = [arrySpecials objectAtIndex:btn.tag];
    
    NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:@"logindetails"];
    RegisterModal *registerModalResponeValue = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    NSString *strType;
    if(self.viewTool.selectedSegmentIndex == 0){
        strType = @"h";
    }
    else{
        strType = @"s";
    }
    
    NSString *strFav;
    if(btn.selected == true){
        strFav = @"2";
    }
    else{
        strFav = @"1";
    }
    
    BOOL check=[[AppDelegate shareAppdelegateInstance] checkReachability];
    if(check)
    {
        [modelDailySpecialDetails setUserID:registerModalResponeValue.userID];
        [CUSTOMINDICATORMACRO startLoadAnimation:self];
        [RestaurantMacro favUnfavSpecialToServer:modelDailySpecialDetails :strFav :strType withCompletion:^(id obj1) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                ResturantModal *responseModal = nil;
                if([obj1 isKindOfClass:[ResturantModal class]])
                {
                    responseModal = obj1;
                    if([responseModal.result isEqualToString:@"success"]){
                        if(btn.selected == true){
                            [modelDailySpecialDetails setIsCheckedFavSpecial:NO];
                        }
                        else{
                            [modelDailySpecialDetails setIsCheckedFavSpecial:YES];
                        }
                        //                        [_tblPopup beginUpdates];
                        //                        [_tblPopup reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:false];
                        //                        [_tblPopup endUpdates];
                        if(btn.selected == true){
                            [cell.btnFavSpecial setSelected:false];
                        }
                        else{
                            [cell.btnFavSpecial setSelected:true];
                        }
                    }
                }
                else
                    [self serverError];
                
                [CUSTOMINDICATORMACRO stopLoadAnimation:self];
            });
        }];
    }
}
-(IBAction)pressReportHereBtn_cell : (UIButton*)sender
{
    //ResturantModal *modal;
    // modal = [arr objectAtIndex:sender.tag];
    
        selectedResturant = [_arrayPlaces objectAtIndex:sender.tag];
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:selectedResturant.resturantName message:@"Specials and Hours are subject to change. To ensure the most up-to-date specials you can view this locations website specials here" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *YesBtn = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                             {
                                 // [selectedResturant setWebSite_Specials:modal.webSite_Specials];
                                 if(selectedResturant.webSite_Specials.length>0)
                                 {
                                     TermsViewController *termsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"termsvc"];
                                     termsVC.checkVC = @"promotions";
                                     termsVC.checkBack = @"homevc";
                                     termsVC.CheckUrlType=@"SpecialLink";
                                     termsVC.restModal_TermsVC = selectedResturant;
                                     [self.navigationController pushViewController:termsVC animated:YES];
                                 }
                                 else
                                 {
                                     
                                     TermsViewController *termsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"termsvc"];
                                     termsVC.checkVC = @"promotions";
                                     termsVC.checkBack = @"homevc";
                                     termsVC.CheckUrlType=@"ResturantEMail";
                                     termsVC.restModal_TermsVC =selectedResturant;
                                     [self.navigationController pushViewController:termsVC animated:YES];
                                 }
                                 
                                 
                                 
                             }];
    
    UIAlertAction *NoBtn = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                            {
                                [self dismissViewControllerAnimated:YES completion:nil];
                            }];
    
    [alertController addAction:YesBtn];
    [alertController addAction:NoBtn];
    
    [self presentViewController:alertController animated:YES completion:nil];
    
}
-(IBAction)pressReportHereBtn : (UIButton*)sender
{
    //ResturantModal *modal;
    // modal = [arr objectAtIndex:sender.tag];
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:selectedResturant.resturantName message:@"Specials and Hours are subject to change. To ensure the most up-to-date specials you can view this locations website specials here" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *YesBtn = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                             {
                                     if(selectedResturant.webSite_Specials.length>0)
                                     {
                                         TermsViewController *termsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"termsvc"];
                                         termsVC.checkVC = @"promotions";
                                         termsVC.checkBack = @"homevc";
                                         termsVC.CheckUrlType=@"SpecialLink";
                                         termsVC.restModal_TermsVC = selectedResturant;
                                         [self.navigationController pushViewController:termsVC animated:YES];
                                     }
                                     else
                                     {
                                         TermsViewController *termsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"termsvc"];
                                         termsVC.checkVC = @"promotions";
                                         termsVC.checkBack = @"homevc";
                                        termsVC.CheckUrlType=@"ResturantEMail";
                                         termsVC.restModal_TermsVC =selectedResturant;
                                         [self.navigationController pushViewController:termsVC animated:YES];
                                     }
                                     
                                 
                                 
                             }];
    
    UIAlertAction *NoBtn = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                            {
                                [self dismissViewControllerAnimated:YES completion:nil];
                            }];
    
    [alertController addAction:YesBtn];
    [alertController addAction:NoBtn];
    
    [self presentViewController:alertController animated:YES completion:nil];
    
}

-(IBAction)pickerValueChanged:(id)sender
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [dateFormatter setLocale:locale];

    [dateFormatter setDateFormat:@"dd-MM-yyyy"];
    dateString = [dateFormatter stringFromDate:datePickerView.date];
    
}
-(void)tapOnLike:(UIButton*)sender{
    NSLog(@"%ld",(long)sender.tag);
   ResturantModal *modal = [_arrayPlaces objectAtIndex:sender.tag];
    NSInteger  strLike = [modal.likecount integerValue];
    if ([modal.likeORDislike isEqualToString:@"2"] || [modal.likeORDislike isEqualToString:@""]) {
        [modal.likeORDislike isEqualToString:@"1"];
        [modal setLikeORDislike:@"1"];
        modal.likecount = [NSString stringWithFormat:@"%ld",(long)strLike+1];
    }
    else{
        [modal.likeORDislike isEqualToString:@"2"];
        [modal setLikeORDislike:@"2"];
        if (strLike > 0)
        {
            modal.likecount = [NSString stringWithFormat:@"%ld",(long)strLike-1];
        }
    }
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [defaults objectForKey:@"logindetails"];
    RegisterModal *registerModalResponeValue = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    NSString * user_IDRegister = registerModalResponeValue.userID;
    [modal setUserID:user_IDRegister];
    NSString *checkcount  = modal.likeORDislike;
    BOOL check=[[AppDelegate shareAppdelegateInstance] checkReachability];
    if(check)
    {
        [CUSTOMINDICATORMACRO startLoadAnimation:self];
        [RestaurantMacro likeOrDislikeToServer:modal withCompletion:^(id obj1) {
            dispatch_async(dispatch_get_main_queue(), ^{
                ResturantModal *responseModal = nil;
                
                if([obj1 isKindOfClass:[ResturantModal class]])
                {
                    responseModal = obj1;
                    if([responseModal.result isEqualToString:@"success"])
                    {
                        [_collectionVw reloadData];
                    }
                    if([checkcount isEqualToString:@"2"])
                    {
                    }
                }
                else
                {
                    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Please check your Internet Connection." preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction *okBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        [self dismissViewControllerAnimated:YES completion:nil];
                    }];
                    
                    [alertController addAction:okBtn];
                    [self presentViewController:alertController animated:YES completion:nil];
                }
                [CUSTOMINDICATORMACRO stopLoadAnimation:self];
            });
        }];
        
    }
}

-(IBAction)pressCancel:(id)sender
{
    [viewPicker setHidden:YES];
}

-(IBAction)pressOK:(id)sender
{
    if(dateString != nil)
    {
        txtDate.text = dateString;
    }
    else
    {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
        [dateFormatter setLocale:locale];

        [dateFormatter setDateFormat:@"dd-MM-yyyy"];
        NSDate *dateVal = [NSDate date];
        NSString *str = [dateFormatter stringFromDate:dateVal];
        txtDate.text = str;
    }
    [viewPicker setHidden:YES];
    
}

-(IBAction)pressDateBtn:(UIButton*)sender
{
 
    if (sender.tag == 1)
    {
        isFromDate = true;
    }
    else
    {
        isFromDate = false;
    }
    
    [viewPicker setHidden:NO];
    [self aaa];
}

-(IBAction)pressBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark ---- Custom Calendar

-(void)aaa
{
    calendar = [[CKCalendarView alloc] initWithStartDay:startMonday];
    self.calendar = calendar;
    calendar.delegate = self;
    self.dateFormatter = [[NSDateFormatter alloc] init];
    [self.dateFormatter setDateFormat:@"dd/MM/yyyy"];
    self.minimumDate = [NSDate date];
    
    self.disabledDates = @[
                           [self.dateFormatter dateFromString:@"05/01/2013"],
                           [self.dateFormatter dateFromString:@"06/01/2013"],
                           [self.dateFormatter dateFromString:@"07/01/2013"]
                           ];
    
    calendar.onlyShowCurrentMonth = NO;
    calendar.adaptHeightToNumberOfWeeksInMonth = YES;
    
    calendar.frame = CGRectMake(10, txtDate.frame.origin.y + txtDate.frame.size.height, self.view.frame.size.width-20, 320);
    calendar.backgroundColor = UIColorFromRGB(0x393B40);
    [viewPicker addSubview:calendar];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(localeDidChange) name:NSCurrentLocaleDidChangeNotification object:nil];
}




- (void)localeDidChange {
    [self.calendar setLocale:[NSLocale currentLocale]];
}

- (BOOL)dateIsDisabled:(NSDate *)date {
    for (NSDate *disabledDate in self.disabledDates) {
        if ([disabledDate isEqualToDate:date]) {
            return YES;
        }
    }
    return NO;
}

#pragma mark - Popup View

- (IBAction)tapOnCrossBtn:(id)sender {
    _popupViewBG.hidden = YES;
    _popupView.hidden=YES;
    _btnCross.hidden=YES;
  //  alertVw.hidden=YES;
}

-(void)tapOnSpecial:(UIButton*)sender{
    
   // NSIndexPath *indexpath=[NSIndexPath indexPathForRow:sender.tag inSection:0];
  //  CGRect myRect = [tableView_Favourite rectForRowAtIndexPath:indexpath];
   // CGRect rectInSuperview = [tableView_Favourite convertRect:myRect toView:[tableView_Favourite superview]];
    
   // [self changePositionOfPopupView:rectInSuperview];
    self.viewTool.selectedSegmentIndex = 0;
    selectedResturant = [_arrayPlaces objectAtIndex:sender.tag];
    _lblpopupName.text=selectedResturant.resturantName;
    
    if ([selectedResturant.happy_hour_count intValue]+[selectedResturant.place_specials_count intValue]!=0) {
        [self specialsHappyHoursServiceHelper];
       // selectedRow=sender.tag;
    }
}
-(void)specialsHappyHoursServiceHelper
{
    BOOL check=[[AppDelegate shareAppdelegateInstance] checkReachability];
    if(check){
        [CUSTOMINDICATORMACRO startLoadAnimation:self];
        [RestaurantMacro getSpecialsHappyHoursToServer:selectedResturant withCompletion:^(id obj1) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if([obj1 isKindOfClass:[ResturantModal class]]){
                    restModal_QuickView  = obj1;
                    if (restModal_QuickView.arrayHappyHours.count>0 || restModal_QuickView.arraySpecials.count>0) {
                        
                        [_tblPopup setContentOffset:CGPointZero animated:NO];
                        [self tapOnHappyHours:nil];
                        _popupViewBG.hidden = NO;
                        _popupView.hidden=NO;
                        _btnCross.hidden=NO;
                      //  alertVw.hidden=NO;
                    }
                }
                else{
                    _popupViewBG.hidden = NO;
                    _popupView.hidden =NO;
                    _btnCross.hidden  =NO;
                    [self serverError];
                }
                [CUSTOMINDICATORMACRO stopLoadAnimation:self];
            });
        }];
    }
}
-(void)showHidePopup:(NSInteger)count{
    
    if (count==0) {
        _emptyView.hidden=NO;
        _tblPopup.hidden=YES;
        _btnExpand.hidden=YES;
        _btnReportHere.hidden=YES;
        
        return;
    }else{
        _tblPopup.hidden=NO;
        _emptyView.hidden=YES;
        _btnExpand.hidden=NO;
        _btnReportHere.hidden=NO;
    }
    [_tblPopup reloadData];
    
}
- (IBAction)tapOnHappyHours:(id)sender {
    //[self colorChangeOnPressTab:@[_btnHppyHours,_btnDailySpecials]];
    arrySpecials = restModal_QuickView.arrayHappyHours;
    [self showHidePopup:arrySpecials.count];
}

#pragma mark - CKCalendarDelegate

- (void)calendar:(CKCalendarView *)calendar configureDateItem:(CKDateItem *)dateItem forDate:(NSDate *)date {
    if ([self dateIsDisabled:date]) {
        dateItem.backgroundColor = [UIColor redColor];
        dateItem.textColor = [UIColor whiteColor];
    }
}

- (BOOL)calendar:(CKCalendarView *)calendar willSelectDate:(NSDate *)date {
    return ![self dateIsDisabled:date];
}

- (void)calendar:(CKCalendarView *)calendar1 didSelectDate:(NSDate *)date {
    
    
    NSDate *currentdate = [NSDate date];
    
    
    NSComparisonResult result;
    
    
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [dateFormat setLocale:locale];

    [dateFormat setDateFormat:@"MM"];
    
    
    NSCalendar* calendar2 = [NSCalendar currentCalendar];
    NSDateComponents* components = [calendar2 components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:currentdate]; // Get necessary date components
    NSInteger month1 = [components month];
    NSInteger day1 = [components day];
    
    NSCalendar* calendar3 = [NSCalendar currentCalendar];
    NSDateComponents* components3 = [calendar3 components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:date]; // Get necessary date components
    NSInteger month2 = [components3 month];
    NSInteger day2 = [components3 day];
    
    
    
    result = [currentdate compare:date]; // comparing two dates
    BOOL isCheckedDate = FALSE;
    if(result==NSOrderedAscending)
    {
        NSLog(@"today is less");
        isCheckedDate = TRUE;
    }
    else if(result==NSOrderedDescending)
    {
        if(month1 <= month2)
        {
            if(day1<=day2)
            {
                isCheckedDate = TRUE;
                
            }
            else
            {
                isCheckedDate = FALSE;
                
            }
        }
        else
        {
            isCheckedDate = FALSE;
            
        }
        NSLog(@"newDate is less");
    }
    else
    {
        NSLog(@"Both dates are same");
    }
    
    if(isCheckedDate == TRUE)
    {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
        [dateFormatter setLocale:locale];

        [dateFormatter setDateFormat:@"dd-MM-yyyy"];
        dateString = [dateFormatter stringFromDate:date];
        
        NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
        NSLocale *locale2 = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
        [dateFormatter1 setLocale:locale2];

        [dateFormatter1 setDateFormat:@"MM-dd-yyyy"];
        NSString *str =[dateFormatter1 stringFromDate:date];
        
        if (isFromDate == true){
            txtDate.text = str;
            txtToDate.text = @"Select Date";
            date1 = date;

        }else{
            txtToDate.text = str;
            date2 = date;
            if (![txtDate.text isEqualToString:@"Select Date"])
            {
                [self eventsListServiceHelper];
            }
        }
        
        viewPicker.hidden = YES;
        [calendar1 removeFromSuperview];
        
    }
    else{
        ERRORMESSAGE_ALERT(@"Select Valid date");
    }
}

-(void)dismissView
{
    viewPicker.hidden = YES;
    if (isFromDate == true){
        date1 = nil;
        txtDate.text = @"Select Date";
    }else{
        date2 = nil;
        txtToDate.text = @"Select Date";
    }
    [calendar removeFromSuperview];
}
- (BOOL)calendar:(CKCalendarView *)calendar willChangeToMonth:(NSDate *)date {
    
    NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
    [dateComponents setMonth:1];
    NSCalendar *calendar1 = [NSCalendar currentCalendar];
    NSDate *newDate = [calendar1 dateByAddingComponents:dateComponents toDate:date options:0];
    
    if ([date laterDate:self.minimumDate] == date) {
        self.calendar.backgroundColor = UIColorFromRGB(0x393B40);
        return YES;
    } else {
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
        [dateFormat setLocale:locale];

        [dateFormat setDateFormat:@"MM"];
        
        
        NSCalendar* calendar2 = [NSCalendar currentCalendar];
        NSDateComponents* components = [calendar2 components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:self.minimumDate]; // Get necessary date components
        NSInteger a = [components month];
        
        NSCalendar* calendar1 = [NSCalendar currentCalendar];
        NSDateComponents* components1 = [calendar1 components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:newDate]; // Get necessary date components
        NSInteger a1 = [components1 month];
        
        if(a < a1)
        {
            self.calendar.backgroundColor = UIColorFromRGB(0x393B40);
            return YES;
        }
        self.calendar.backgroundColor = UIColorFromRGB(0x393B40);
        return NO;
    }
}

- (void)calendar:(CKCalendarView *)calendar didLayoutInRect:(CGRect)frame {
    NSLog(@"calendar layout: %@", NSStringFromCGRect(frame));
}



-(void)serverError{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Please Check Your Connection." preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
    [alertController addAction:okBtn];
    [self presentViewController:alertController animated:YES completion:nil];
    
    
}



-(void)dealloc{

    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    if (_locationManager) {
        [_locationManager stopUpdatingLocation];
        _locationManager=nil;
    }
    
    _collectionVw =nil;
    if (_arrayEvents.count>0) {
        [_arrayEvents removeAllObjects];
    }
    
    if (_arrayPlaces.count>0) {
        [_arrayPlaces removeAllObjects];
    }
    
    eventModalOBJ=nil;
}


@end
