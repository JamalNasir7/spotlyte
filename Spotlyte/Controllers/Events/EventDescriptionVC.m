//
//  EventDescriptionVC.m
//  Spotlyte
//
//  Created by Hemant on 01/03/17.
//  Copyright © 2017 Hemant. All rights reserved.
//

#import "EventDescriptionVC.h"
#import "HeaderView.h"
#import "PromotionsViewController.h"

@interface EventDescriptionVC ()
@end

@implementation EventDescriptionVC
- (void)viewDidLoad {
    [super viewDidLoad];
    HeaderView *header=[[HeaderView alloc]initWithTitle:_eventObj.eventName controller:self];
    [header addSubview:header.btnback];
    [self.view addSubview:header];
    
    _btnViewLocation.layer.cornerRadius = 10;
    _btnViewLocation.layer.borderWidth = 1;
    _btnViewLocation.clipsToBounds = YES;
    _btnViewLocation.layer.borderColor = [[UIColor darkGrayColor]CGColor];
    
    _lblNoDescription.hidden=[_eventObj.eventDescription isEqualToString:@""]?NO:YES;
    
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
     
    });
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        NSURL *url = [[NSBundle mainBundle] URLForResource:@"loading-1" withExtension:@"gif"];
        NSString *newUrl = [_eventObj.eventImageStr stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
        [_imgView sd_setImageWithURL:[NSURL URLWithString:newUrl]
                    placeholderImage:[UIImage imageNamed:@"noimage.png"]];
         _lblDes.text=_eventObj.eventDescription;
    });
}

- (IBAction)tapOnViewLocations:(id)sender {
    PromotionsViewController *promtionVC = [self.storyboard instantiateViewControllerWithIdentifier:@"promtions"];
    promtionVC.selectedValue = [_placeInfo[@"selectedTag"] integerValue];
    promtionVC.placeIDString = [NSString stringWithFormat:@"%@",_restObj.placeID];
    promtionVC.promtionsArray = [_placeInfo[@"placeArray"] copy];
    promtionVC.selectedWhichController = @"eventSection";
    promtionVC.checkTextOrSearch = @"customsearch";
    [self.navigationController pushViewController:promtionVC animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end
