//
//  EventDescriptionVC.h
//  Spotlyte
//
//  Created by Hemant on 01/03/17.
//  Copyright © 2017 Hemant. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventDescriptionVC : UIViewController

@property (nonatomic,strong) EventsModel *eventObj;
@property (nonatomic,strong) ResturantModal *restObj;
@property (strong, nonatomic) IBOutlet UIImageView *imgView;
@property (strong, nonatomic) IBOutlet UILabel *lblDes;
@property (strong, nonatomic) IBOutlet UIButton *btnViewLocation;
@property (strong, nonatomic) IBOutlet UILabel *lblNoDescription;
@property (strong, nonatomic) NSDictionary *placeInfo;

@end
