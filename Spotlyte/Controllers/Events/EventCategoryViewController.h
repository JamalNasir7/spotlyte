//
//  EventCategoryViewController.h
//  Spotlyte
//
//  Created by Admin on 20/04/16.
//  Copyright © 2016 Naveen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventCategoryViewController : UIViewController<UICollectionViewDelegate,UICollectionViewDataSource>
{
    IBOutlet UITableView *tableViewCategory;
}
@property (nonatomic, retain) NSMutableArray *arrayCategory;
@property (nonatomic, retain) NSMutableArray *nameArray;
@property (nonatomic, retain) NSMutableArray *idArray;
@property (strong, nonatomic) IBOutlet UICollectionView *eventCollectionVw;
@property (strong, nonatomic) IBOutlet UIView *emptyVIew;


@end
