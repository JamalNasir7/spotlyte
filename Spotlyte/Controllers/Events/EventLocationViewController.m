//
//  EventLocationViewController.m
//  Spotlyte
//
//  Created by Admin on 18/08/16.
//  Copyright © 2016 Naveen. All rights reserved.
//

#import "EventLocationViewController.h"
#import "HeaderView.h"
@interface EventLocationViewController ()

@end

@implementation EventLocationViewController
@synthesize eventObj;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    HeaderView *header=[[HeaderView alloc]initWithTitle:@"Event Location" controller:self];
    [header addSubview:header.btnback];
    [self.view addSubview:header];
    
    if(eventObj.eventAddress.length>0)
    {
        NSString *removeString = [eventObj.eventAddress stringByReplacingOccurrencesOfString:@"-" withString:@""];
        [self getAddressLatLong:removeString];
        
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}



-(void)getAddressLatLong:(NSString *)string
{
    if(string.length>0)
    {
        CLGeocoder *geocoder = [[CLGeocoder alloc] init];
        
        [geocoder geocodeAddressString:string completionHandler:^(NSArray *placemarks, NSError *error) {
            if([placemarks count]) {
                CLPlacemark *placemark = [placemarks objectAtIndex:0];
                CLLocation *location = placemark.location;
                CLLocationCoordinate2D coordinate = location.coordinate;
                NSLog(@"coordinate = (%f, %f)", coordinate.latitude, coordinate.longitude);
                
                MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(coordinate, 800, 800);
                [mapViewSelectLoc setRegion:[mapViewSelectLoc regionThatFits:region] animated:YES];
                
                
                MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
                point.coordinate = coordinate;
                point.title = eventObj.categoryName;
                point.subtitle = eventObj.eventName;
                
                [mapViewSelectLoc addAnnotation:point];
            }
        }];
    }
}


- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(userLocation.coordinate, 800, 800);
    [mapViewSelectLoc setRegion:[mapViewSelectLoc regionThatFits:region] animated:YES];
    
    
    CLLocationCoordinate2D userCoordinate1= CLLocationCoordinate2DMake(13.080453, 80.256240);
    
    MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
    point.coordinate = userCoordinate1;
    point.title = @"Where am I?";
    point.subtitle = @"I'm here!!!";
    
    [mapViewSelectLoc addAnnotation:point];
}

-(IBAction)pressBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
