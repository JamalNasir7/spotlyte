//
//  EventCategoryViewController.m
//  Spotlyte
//
//  Created by Admin on 20/04/16.
//  Copyright © 2016 Naveen. All rights reserved.
//

#import "EventCategoryViewController.h"
#import "HeaderView.h"
#import "EventCell.h"

@interface EventCategoryViewController ()
{
    NSDictionary *contents;
}
@end

@implementation EventCategoryViewController
@synthesize arrayCategory;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    HeaderView *header=[[HeaderView alloc]initWithTitle:@"Top Spots" controller:self];
    [header addSubview:header.btnback];
    [header.imgViewBack setHidden:YES];
    header.BackButtonHandler=^(BOOL success){
        if (success) {
           // [self.tabBarController setSelectedIndex:0];
        }
    };
    [self.view addSubview:header];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc] init];
    flow.minimumInteritemSpacing = 0;
    flow.minimumLineSpacing = 2;
    _eventCollectionVw.collectionViewLayout = flow;
    arrayCategory=[NSMutableArray new];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [_eventCollectionVw setContentOffset:CGPointZero animated:NO];

    BOOL tabSelected=[[NSUserDefaults standardUserDefaults] boolForKey:@"tabSelected"];
    if (tabSelected) {
        [self getEventsCategory];
    }
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"tabSelected"];
    
    self.navigationController.navigationBar.hidden = true;
    
}

-(void)getEventsCategory{
    [CUSTOMINDICATORMACRO startLoadAnimation:self];
    
    [ServiceHelper getRequest:@"getEventCategories.json" callback:^(id data, NSError* error, BOOL success){
         dispatch_async(dispatch_get_main_queue(), ^{

        if (success) {
            if (data!=nil && ![data isKindOfClass:[NSNull class]]) {
                if ([data isKindOfClass:[NSArray class]]) {
                    
                    
                    if (arrayCategory.count>0) {
                        [arrayCategory  removeAllObjects];
                    }
                    
                    EventsModel *model;
                    for (NSDictionary *dict in data) {
                         model = [EventsModel new];
                        [model setCategoryID:dict[@"Category_id"]];
                        [model setCategoryName:dict[@"Category_name"]];
                        [model setCategoryImage:dict[@"Category_image"]];
                        [arrayCategory addObject:model];
                        model=nil;
                    }
                    [CUSTOMINDICATORMACRO stopLoadAnimation:self];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [_eventCollectionVw reloadData];
                        _emptyVIew.hidden=YES;
                        _eventCollectionVw.hidden=NO;
                    });
                }
                else
                {
                    _emptyVIew.hidden=NO;
                    _eventCollectionVw.hidden=YES;
                 [CUSTOMINDICATORMACRO stopLoadAnimation:self];
                }
            }else
            {
                _emptyVIew.hidden=NO;
                _eventCollectionVw.hidden=YES;
                [CUSTOMINDICATORMACRO stopLoadAnimation:self];
                
            }
        }else
         {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Please check your Internet Connection." preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self dismissViewControllerAnimated:YES completion:nil];
            }];
            [alertController addAction:okBtn];
            [self presentViewController:alertController animated:YES completion:nil];
            [CUSTOMINDICATORMACRO stopLoadAnimation:self];
          }
        });
    }];
}


///<---------------------------
#pragma mark CollectionView methods
///<---------------------------

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;}


-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return [arrayCategory count];}


-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifier=@"EventSectionCell";
    EventCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    if (arrayCategory.count>0) {
       
        EventsModel *model=[arrayCategory objectAtIndex:indexPath.item];
        cell.lblName.text = model.categoryName;
        dispatch_async(dispatch_get_main_queue(), ^{
            NSURL *url = [[NSBundle mainBundle] URLForResource:@"loading-1" withExtension:@"gif"];
            NSString *newUrl = [model.categoryImage stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
            [cell.imgView sd_setImageWithURL:[NSURL URLWithString:newUrl]
                            placeholderImage:[UIImage imageNamed:@"noimage.png"]];
        });
        model=nil;
    }
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    EventsViewController *eventsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"eventlist"];
    EventsModel *modal = [arrayCategory objectAtIndex:indexPath.row];
    eventsVC.titleString = modal.categoryName;
    eventsVC.eventModalOBJ = modal;
    [self.navigationController pushViewController:eventsVC animated:YES];
}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    float cellWidth =screenRect.size.width/2-1;
    float cellHeight = screenRect.size.height/4-2;
    CGSize size = CGSizeMake(cellWidth,cellHeight);
    return size;
}


-(UIColor*)getColorFromRGB:(NSArray*)RGBarray{
    UIColor *color=[UIColor colorWithRed:[RGBarray[0] floatValue]/255.0f
                                   green:[RGBarray[1] floatValue]/255.0f
                                    blue:[RGBarray[2] floatValue]/255.0f
                                   alpha:[RGBarray[3] floatValue]];
    return color;
}
@end
