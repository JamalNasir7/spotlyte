//
//  EventLocationViewController.h
//  Spotlyte
//
//  Created by Admin on 18/08/16.
//  Copyright © 2016 Naveen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventLocationViewController : UIViewController<MKMapViewDelegate,CLLocationManagerDelegate>
{
    IBOutlet MKMapView *mapViewSelectLoc;
    AnnotationView *annotationView;
    CLLocationManager *locationManager;
    CLLocation *currentLocation;
    CLLocationCoordinate2D userCoordinate;
}
@property (nonatomic, strong) EventsModel *eventObj;

-(IBAction)pressBack:(id)sender;
@end
