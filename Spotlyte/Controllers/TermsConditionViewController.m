//
//  TermsConditionViewController.m
//  Spotlyte
//
//  Created by Hemant on 27/02/17.
//  Copyright © 2017 Hemant. All rights reserved.
//

#import "TermsConditionViewController.h"
#import "HeaderView.h"
@interface TermsConditionViewController ()

@end

@implementation TermsConditionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    HeaderView *header=[[HeaderView alloc]initWithTitle:_headerName controller:self];
    [header addSubview:header.btnback];
    [self.view addSubview:header];
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [_lblTect setContentOffset:CGPointZero animated:YES];
    });
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end
