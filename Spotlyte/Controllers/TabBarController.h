//
//  TabBarController.h
//  Spotlyte
//
//  Created by Hemant on 03/03/17.
//  Copyright © 2017 Hemant. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TabBarController : UITabBarController<UITabBarControllerDelegate>

@end
