//
//  EditProfileViewController.m
//  Spotlyte
//
//  Created by Admin on 18/04/16.
//  Copyright © 2016 Naveen. All rights reserved.
//

#import "EditProfileViewController.h"
#import "HeaderView.h"
#import "TagCell.h"
#import "FlowLayout.h"
#import "EditPreferedVC.h"

@interface EditProfileViewController ()

@property (strong, nonatomic) FlowLayout *layout;

@end

@implementation EditProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    stateArray = [[NSMutableArray alloc]init];
    preferredCategoryArray = [[NSMutableArray alloc]init];
    btn_PreferrdItem.layer.borderColor = [UIColor whiteColor].CGColor;
    HeaderView *header=[[HeaderView alloc]initWithTitle:@"Edit Account" controller:self];
    [header addSubview:header.btnback];
    [self.view addSubview:header];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:self.view.window];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:self.view.window];
    
    [self createNumberToolBar:txt_Mobile tagArg:1];
    [self createNumberToolBar:txt_Zipcode tagArg:0];
    
    
    txt_Address1.userInteractionEnabled = NO;
    txt_Email.userInteractionEnabled = NO;
    
    self.layout = [[FlowLayout alloc] init];
    self.layout.alignment = FlowAlignmentCenter;
    self.layout.estimatedItemSize = CGSizeMake(80, 30);
    self.layout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
    self.layout.headerReferenceSize = CGSizeMake(0, 0);
    self.layout.footerReferenceSize = CGSizeMake(0, 0);
    collectionView_PreferredCategory.collectionViewLayout = self.layout;

    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [preferredCategoryArray removeAllObjects];
    collectionView_PreferredCategory.delegate = self;
    collectionView_PreferredCategory.dataSource = self;
    [self getProfileServiceHelper];
    
    btn_PreferrdItem.layer.cornerRadius = 5.0;
    
    txt_FirstName.layer.cornerRadius = 8.0;
    txt_LastName.layer.cornerRadius = 8.0;
    txt_Email.layer.cornerRadius = 8.0;
    txt_Address1.layer.cornerRadius = 8.0;
    txt_Address2.layer.cornerRadius = 8.0;
    txt_City.layer.cornerRadius = 8.0;
    txt_State.layer.cornerRadius = 8.0;
    txt_Country.layer.cornerRadius = 8.0;
    txt_Zipcode.layer.cornerRadius = 8.0;
    txt_Mobile.layer.cornerRadius = 8.0;
    
    btnUpdate.layer.cornerRadius = 8.0;
}

-(void)viewDidLayoutSubviews{
    profileImage.layer.cornerRadius = profileImage.frame.size.width / 2;
    profileImage.layer.masksToBounds = true;
}

-(void)createNumberToolBar :(UITextField *)textfield tagArg:(int)tagVal
{
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
    
    UIBarButtonItem *cancel =[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelNumberPad:)];
    cancel.tag = tagVal;
    
    UIBarButtonItem *done = [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad:)];
    done.tag = tagVal;
    
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar.items = [NSArray arrayWithObjects:cancel,
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],done,
                           nil];
    [numberToolbar sizeToFit];
    textfield.inputAccessoryView = numberToolbar;
}

-(void)cancelNumberPad :(id)sender{
    UIButton *btnClicked = (UIButton *)sender;
    
    if(btnClicked.tag == 0)
    {
        [txt_Zipcode resignFirstResponder];
        txt_Zipcode.text = @"";
    }
    else
    {
        [txt_Mobile resignFirstResponder];
        txt_Mobile.text = @"";
    }
}

-(void)doneWithNumberPad :(id)sender{
    UIButton *btnClicked = (UIButton *)sender;
    
    if(btnClicked.tag == 0)
    {
        [txt_Zipcode resignFirstResponder];
    }
    else
    {
        [txt_Mobile resignFirstResponder];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - PickerView Delegate Methods


-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return stateArray.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    RegisterModal *modal = [stateArray objectAtIndex:row];
    return modal.state;
    
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow: (NSInteger)row inComponent:(NSInteger)component {
    modalObj = [RegisterModal new];
    NSLog(@"%@",[stateArray objectAtIndex:row]);
    modalObj = [stateArray objectAtIndex:row];
    stateID = modalObj.stateID;
    stateString = [NSString stringWithFormat:@"%@",modalObj.state];
}


#pragma mark --- Service Helper

-(void)getProfileServiceHelper
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [defaults objectForKey:@"logindetails"];
    RegisterModal * registerModalResponeValue = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    BOOL check=[[AppDelegate shareAppdelegateInstance] checkReachability];
    if(check)
    {
        [CUSTOMINDICATORMACRO startLoadAnimation:self];
        
        [REGISTERMACRO viewAccountToServer:registerModalResponeValue.userID withCompletion:^(id obj1) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [preferredCategoryArray removeAllObjects];
                
                if([obj1 isKindOfClass:[RegisterModal class]])
                {
                    RegisterModal *modal = obj1;
                    [self setValuesINTextField:modal];
                    preferredCategoryArray = modal.prefferedCategoryArray;
                    [self getCountryListServiceHelper];
                    
                    if(preferredCategoryArray.count <= 4){
                        CollCatHeightConst.constant = 0;//40;
                    }
                    else if(preferredCategoryArray.count >= 4 && preferredCategoryArray.count <= 8){
                        CollCatHeightConst.constant = 0;//85;
                    }
                    else{
                        CollCatHeightConst.constant = 0;//113;
                    }
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self ReloadCollectionView];
                    });
                }
                else
                {
                    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Please check your connection" preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction *okBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        [self dismissViewControllerAnimated:YES completion:nil];
                    }];
                    
                    [alertController addAction:okBtn];
                    [self presentViewController:alertController animated:YES completion:nil];
                    [CUSTOMINDICATORMACRO stopLoadAnimation:self];
                }
                
            });
        }];
    }
}


#pragma mark --- Country Service Helper

-(void)getCountryListServiceHelper
{
    BOOL check=[[AppDelegate shareAppdelegateInstance] checkReachability];
    if(check)
    {
        [CUSTOMINDICATORMACRO startLoadAnimation:self];
        
        [REGISTERMACRO listallCountryServer:nil withCompletion:^(id obj1) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [CUSTOMINDICATORMACRO stopLoadAnimation:self];
                if([obj1 isKindOfClass:[NSArray class]] || [obj1 isKindOfClass:[NSMutableArray class]])
                {
                    stateArray = obj1;
                    [statePickerView reloadAllComponents];
                    [CUSTOMINDICATORMACRO stopLoadAnimation:self];
                }
                else
                {
                    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Please check your connection" preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction *okBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        [self dismissViewControllerAnimated:YES completion:nil];
                    }];
                    
                    [alertController addAction:okBtn];
                    [self presentViewController:alertController animated:YES completion:nil];
                     [CUSTOMINDICATORMACRO stopLoadAnimation:self];
                }
            });
        }];
    }
}




-(void)setValuesINTextField :(RegisterModal *)modalOBJ
{
    dispatch_async(dispatch_get_main_queue(), ^{
        
        txt_Address1.text   =   modalOBJ.userName;
        txt_Address2.text   =   modalOBJ.address1;
        // txt_Address2.text   =   modalOBJ.address2;
        txt_City.text       =   modalOBJ.city;
        txt_Country.text    =   modalOBJ.country;
        txt_FirstName.text  =   modalOBJ.firstName;
        txt_LastName.text   =   modalOBJ.lastName;
        txt_Mobile.text     =   modalOBJ.mobileNo;
        txt_Zipcode.text    =   modalOBJ.zipCode;
        txt_State.text      =   modalOBJ.state;
        stateID             =   modalOBJ.stateID;
        txt_Email.text      =   modalOBJ.emailID;
        [profileImage setBackgroundColor:[UIColor clearColor]];
        
        if(!isImageEdited){
            NSURL *url = [[NSBundle mainBundle] URLForResource:@"loading-1" withExtension:@"gif"];
        
            NSString *newUrl = [modalOBJ.profileImage stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
            [profileImage sd_setImageWithURL:[NSURL URLWithString:newUrl]
                        placeholderImage:[UIImage imageNamed:@"imgplaceHolder"]];
        }
    });
   // [CUSTOMINDICATORMACRO stopLoadAnimation:self];
}

-(NSString *)checkNilValue :(NSString *)string
{
    if(string != nil && string.length>0)
    {
        return string;
    }
    else
    {
        return @"";
    }
}

-(void)editProfileServiceHelper
{
    
    if ([txt_FirstName.text isEqualToString:@""]) {
            showAlert(@"", @"Please Enter Your First Name.", self, @"Ok", @"");
        return;
    }
    
    if ([txt_LastName.text isEqualToString:@""]) {
            showAlert(@"", @"Please Enter Your Last Name.", self, @"Ok", @"");
        return;
        }
    
    if ([txt_Address1.text isEqualToString:@""]) {
            showAlert(@"", @"Please Enter User Name.", self, @"Ok", @"");
        return;
        }
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSData *data = [defaults objectForKey:@"logindetails"];
    RegisterModal * registerModalResponeValue = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    RegisterModal *modal = [[RegisterModal alloc]init];
    [modal setUserID:registerModalResponeValue.userID];
    [modal setFirstName:[self checkNilValue:txt_FirstName.text]];
    [modal setLastName:[self checkNilValue:txt_LastName.text]];
    [modal setUserName:[self checkNilValue:txt_Address1.text]];
    [modal setAddress2:[self checkNilValue:txt_Address2.text]];
    [modal setCity:[self checkNilValue:txt_City.text]];
    [modal setState:[self checkNilValue:stateID]];
    [modal setCountry:[self checkNilValue:txt_Country.text]];
    [modal setMobileNo:[self checkNilValue:txt_Mobile.text]];
    [modal setZipCode:[self checkNilValue:txt_Zipcode.text]];
    [modal setEmailID:[self checkNilValue:txt_Email.text]];


    NSLog(@"profile image encoding with base64%@", [self encodeToBase64String:profileImage.image]);
    
    
    UIImage *resizeImage = [AppDelegate scaleAndRotateImage:profileImage.image];
    
    if(resizeImage == nil)
    {
        [modal setProfileImage:@""];
        
    }
    else
    {
        [modal setProfileImage:[self encodeToBase64String:resizeImage]];
    }
    BOOL check=[[AppDelegate shareAppdelegateInstance] checkReachability];
    if(check)
    {
        [CUSTOMINDICATORMACRO startLoadAnimation:self];
        [REGISTERMACRO editAccountToServer:modal withCompletion:^(id obj1)
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                
                 
                 if([obj1 isKindOfClass:[RegisterModal class]])
                 {
                     RegisterModal *responseModal = obj1;
                     if ([responseModal.result isEqualToString:@"success"])
                     {
                         
                         [self saveLoggedInDetails:responseModal];
                         
                         [CUSTOMINDICATORMACRO stopLoadAnimation:self];
                         UIAlertController *alertController = [UIAlertController alertControllerWithTitle:responseModal.result message:responseModal.message preferredStyle:UIAlertControllerStyleAlert];
                         
                         UIAlertAction *okBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                             [self dismissViewControllerAnimated:YES completion:nil];
                             [self.navigationController popViewControllerAnimated:YES];
                             
                             NSLog(@"new values after upload%@",responseModal.profileImage);
                         }];
                         
                         [alertController addAction:okBtn];
                         [self presentViewController:alertController animated:YES completion:nil];
                         
                     }else
                     {
                         [CUSTOMINDICATORMACRO stopLoadAnimation:self];
                         UIAlertController *alertController = [UIAlertController alertControllerWithTitle:responseModal.result message:responseModal.message preferredStyle:UIAlertControllerStyleAlert];
                         
                         UIAlertAction *okBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                             [self dismissViewControllerAnimated:YES completion:nil];
                             [self.navigationController popViewControllerAnimated:YES];
                             
                             NSLog(@"new values after upload%@",responseModal.profileImage);
                         }];
                         
                         [alertController addAction:okBtn];
                         [self presentViewController:alertController animated:YES completion:nil];
                     }
                 }
                 else
                 {
                     [CUSTOMINDICATORMACRO stopLoadAnimation:self];
                     UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Please check your connection" preferredStyle:UIAlertControllerStyleAlert];
                     
                     UIAlertAction *okBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                         [self dismissViewControllerAnimated:YES completion:nil];
                     }];
                     
                     [alertController addAction:okBtn];
                     [self presentViewController:alertController animated:YES completion:nil];
                 }
                 
             });
         }];
    }
    
}

-(void)saveLoggedInDetails:(RegisterModal *)loginObj
{
    NSUserDefaults *userPref  = [NSUserDefaults standardUserDefaults];
    [userPref setBool:TRUE forKey:@"login"];
    
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:loginObj];
    [userPref setObject:data forKey:@"logindetails"];
    [userPref synchronize];
}

-(IBAction)pressSubmitBtn:(id)sender
{
    [self editProfileServiceHelper];
}

-(IBAction)btnPreferredCatEgoryClick:(id)sender
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [defaults objectForKey:@"logindetails"];
    RegisterModal * registerModalResponeValue = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    NSString *user_IDRegister = registerModalResponeValue.userID;
    
    EditPreferedVC *PreferedVC=[[EditPreferedVC alloc]initWithNibName:@"EditPreferedVC" bundle:nil];
    PreferedVC.strUserID = user_IDRegister;
    [self.navigationController presentViewController:PreferedVC animated:true completion:nil];
}
-(IBAction)pressBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)ReloadCollectionView
{
    [collectionView_PreferredCategory reloadData];
    [collectionView_PreferredCategory.collectionViewLayout invalidateLayout];
}
-(IBAction)pressProfileImageToChooseFromGallery:(id)sender
{
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Choose Your Profile Picture"
                                                             delegate:self
                                                    cancelButtonTitle:@"Cancel"
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:@"Camera", @"Gallery", nil];
    actionSheet.tag = 1;
    [actionSheet showInView:self.view];
}

# pragma mark - ActionSheet Delegate Method

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    UIImagePickerController *picker;
    switch (actionSheet.tag) {
        case 1: {
            switch (buttonIndex) {
                case 0:
                    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
                        picker = [[UIImagePickerController alloc] init];
                        picker.delegate = self;
                        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                        [self presentViewController:picker animated:YES completion:NULL];
                    }
                    break;
                case 1:
                    
                    picker = [[UIImagePickerController alloc] init];
                    picker.delegate = self;
                    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                    [self presentViewController:picker animated:YES completion:NULL];
                    
                    break;
                case 2:
                    
                    break;
                default:
                    break;
            }
            break;
        }
        default:
            break;
    }
}

#pragma  mark - Method to handle Base64 Image Encoding

- (NSString *)encodeToBase64String:(UIImage *)image {
    return [UIImagePNGRepresentation(image) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
}

#pragma  mark - Custom Method to KeyBoard Resign

- (void)keyboardWillHide:(NSNotification *)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    scroll_EditProfile.contentInset = contentInsets;
    scroll_EditProfile.scrollIndicatorInsets = contentInsets;
    scroll_EditProfile.contentOffset = CGPointMake(0, 0);
    
}

- (void)keyboardWillShow:(NSNotification *)aNotification
{
    
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    scroll_EditProfile.contentInset = contentInsets;
    scroll_EditProfile.scrollIndicatorInsets = contentInsets;
    
    CGRect aRect = self.view.frame;
    aRect.size.height -= kbSize.height;
    
    if (!CGRectContainsPoint(aRect, currentTextField.frame.origin) ) {
        CGPoint scrollPoint = CGPointMake(0.0, currentTextField.frame.origin.y-kbSize.height);
        [scroll_EditProfile setContentOffset:scrollPoint animated:YES];
    }
}

#pragma mark - UIImagePicker Delegate Method

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    isImageEdited = true;
    pickProfileImage = [AppDelegate scaleAndRotateImage:info[UIImagePickerControllerOriginalImage]];
    profileImage.image = pickProfileImage;
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    isImageEdited = false;
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}




#pragma mark - Text Field Delegate
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == txt_FirstName) {
        [txt_FirstName resignFirstResponder];
        [txt_LastName becomeFirstResponder];
        
    } else if (textField == txt_LastName) {
        [txt_LastName resignFirstResponder];
        [txt_Address1 becomeFirstResponder];
    }else if (textField == txt_Address1) {
        [txt_Address1 resignFirstResponder];
        [txt_Address2 becomeFirstResponder];
    }else if (textField == txt_Address2) {
        [txt_Address2 resignFirstResponder];
        [txt_City becomeFirstResponder];
    }else if (textField == txt_City) {
        [txt_City resignFirstResponder];
        [txt_Country becomeFirstResponder];
    }else if (textField == txt_Country) {
        [txt_Country resignFirstResponder];
        [txt_Zipcode becomeFirstResponder];
    }else if (textField == txt_Zipcode) {
        [txt_Zipcode resignFirstResponder];
        [txt_Mobile becomeFirstResponder];
    }
    else if (textField == txt_Mobile) {
        [txt_Mobile resignFirstResponder];
    }
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField != txt_Email)
    {
        textField.autocapitalizationType = UITextAutocapitalizationTypeWords;
    }
    

    currentTextField = textField;
}

-(void)resignKeyBoard
{
    [txt_Address1 resignFirstResponder];
    [txt_Address2 resignFirstResponder];
    [txt_City resignFirstResponder];
    [txt_Country resignFirstResponder];
    [txt_FirstName resignFirstResponder];
    [txt_LastName resignFirstResponder];
    [txt_Mobile resignFirstResponder];
    [txt_State resignFirstResponder];
    [txt_Zipcode resignFirstResponder];
    [txt_Email resignFirstResponder];
}

-(IBAction)pressState:(id)sender;
{
    NSLog(@"click state");
    [self resignKeyBoard];
    viewPicker.hidden = NO;
}

-(IBAction)pressSelect:(id)sender
{
    viewPicker.hidden = YES;
    if([stateString isEqualToString:@""] || stateString.length == 0)
    {
        modalObj = [RegisterModal new];
        modalObj = [stateArray objectAtIndex:0];
        stateString = [NSString stringWithFormat:@"%@",modalObj.state];
    }
    txt_State.text = stateString;
}

-(IBAction)pressCancel:(id)sender
{
    viewPicker.hidden = YES;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return preferredCategoryArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    TagCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"TagCell" forIndexPath:indexPath];
    cell.maxWidthConstraint.constant = CGRectGetWidth(collectionView.bounds) - self.layout.sectionInset.left - self.layout.sectionInset.right - cell.layoutMargins.left - cell.layoutMargins.right - 10;
    cell.textLabel.numberOfLines = 1;
    
    RegisterModal *modal = [preferredCategoryArray objectAtIndex:indexPath.row];
    NSLog(@"MODEL = %@",modal.categoryName);
    cell.textLabel.text = modal.categoryName;
    
    return cell;
}


@end
