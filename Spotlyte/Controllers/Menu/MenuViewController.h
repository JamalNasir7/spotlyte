//
//  MenuViewController.h
//  GopherAssist
//
//  Created by Arunkumar Gnanasekar on 04/01/16.
//  Copyright © 2016 Colan Infotech Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import <MessageUI/MFMailComposeViewController.h>


@interface MenuViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,MFMailComposeViewControllerDelegate>
{
    NSArray *menuArray;

    IBOutlet UITableView *tblMenu;
    IBOutlet UIImageView *imageView_Profile;
    IBOutlet UILabel *label_Name;
    UITabBarController *tabBarController;
}

@end
