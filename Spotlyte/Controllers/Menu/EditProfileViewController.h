//
//  EditProfileViewController.h
//  Spotlyte
//
//  Created by Admin on 18/04/16.
//  Copyright © 2016 Naveen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EditProfileViewController : UIViewController<UIActionSheetDelegate,  UITextFieldDelegate,  UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIPickerViewDataSource,UIPickerViewDelegate,UICollectionViewDataSource,UICollectionViewDelegate>
{
    IBOutlet UITextField *txt_FirstName;
    IBOutlet UITextField *txt_LastName;
    IBOutlet UITextField *txt_Address1;
    IBOutlet UITextField *txt_Address2;
    IBOutlet UITextField *txt_City;
    IBOutlet UITextField *txt_State;
    IBOutlet UITextField *txt_Country;
    IBOutlet UITextField *txt_Zipcode;
    IBOutlet UITextField *txt_Mobile;
    
    IBOutlet UITextField *txt_Email;
    IBOutlet UIButton *btnUpdate;
    
    IBOutlet UIScrollView *scroll_EditProfile;
    UITextField *currentTextField;
    IBOutlet UIImageView *profileImage;
    UIImage *pickProfileImage;
    
    
    IBOutlet UIView *viewPicker;
    IBOutlet UIPickerView *statePickerView;
    
    NSMutableArray *stateArray;
    
    NSMutableArray *preferredCategoryArray;
    
    NSString       *stateString;
    NSString        *stateID;
    RegisterModal *modalObj;

    IBOutlet UIButton *btn_back;
    IBOutlet UIButton *btn_PreferrdItem;
    
    IBOutlet UICollectionView *collectionView_PreferredCategory;
    
    BOOL isImageEdited;
    
    __weak IBOutlet NSLayoutConstraint *CollCatHeightConst;
}

-(IBAction)pressBack:(id)sender;
-(IBAction)pressSubmitBtn:(id)sender;
-(IBAction)pressProfileImageToChooseFromGallery:(id)sender;
-(IBAction)pressState:(id)sender;
-(IBAction)pressSelect:(id)sender;
-(IBAction)pressCancel:(id)sender;

@end
