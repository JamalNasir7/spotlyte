//
//  TopRatedViewController.h
//  Spotlyte
//
//  Created by Admin on 19/04/16.
//  Copyright © 2016 Naveen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TopRatedViewController : UIViewController<UITableViewDataSource, UITableViewDelegate,EDStarRatingProtocol>

{
    IBOutlet UITableView * tblViewRated;
    EDStarRating    *starRatingView;
    IBOutlet UIButton *btn_Bars;
    IBOutlet UIButton *btn_Restaurants;
    IBOutlet UIButton *btn_Others;
    IBOutlet UIView   *view_NoValues;
    IBOutlet UILabel *ratingLbl;
    NSInteger typeValue;
    IBOutlet UIImageView *imageView_Indicator;
}

@property (strong, nonatomic) NSMutableArray *arrayRated;
@property (strong, nonatomic) ResturantModal *restModalResponeOBJ;


-(IBAction)pressTabBar:(id)sender;

@end
