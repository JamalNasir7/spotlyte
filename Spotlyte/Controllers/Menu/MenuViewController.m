//
//  MenuViewController.m
//  GopherAssist
//
//  Created by Arunkumar Gnanasekar on 04/01/16.
//  Copyright © 2016 Colan Infotech Private Limited. All rights reserved.
//

#import "MenuViewController.h"
#import "LoginViewController.h"
#import "SWRevealViewController.h"

#import "DashBoardViewController.h"
#import "ProgressViewController.h"
#import "SettingsViewController.h"
#import "spotlytePromotionViewController.h"
#import "EventsViewController.h"
#import "NewHomeViewController.h"
#import <TwitterKit/TwitterKit.h>
#import "customSearchViewController.h"
#import "HomeViewController.h"

@interface MenuViewController ()
@end

@implementation MenuViewController
- (void)viewDidLoad
{
    [super viewDidLoad];
    menuArray = [NSArray arrayWithObjects:@"Home", @"Maps", @"Dashboard", @"Top Deals", @"Events",@"Advanced Search", @"Report Issue", @"Settings",@"logout", nil];
    
    imageView_Profile.layer.cornerRadius = imageView_Profile.frame.size.width/2.0;
    imageView_Profile.layer.masksToBounds = YES;
    imageView_Profile.clipsToBounds = YES;
    imageView_Profile.center = self.view.center;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [defaults objectForKey:@"logindetails"];
    RegisterModal * registerModalResponeValue = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    label_Name.text =  registerModalResponeValue.userName;
    dispatch_async(dispatch_get_main_queue(), ^{
        NSURL *url = [[NSBundle mainBundle] URLForResource:@"loading-1" withExtension:@"gif"];
        NSString *newUrl = [registerModalResponeValue.profileImage stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
        [imageView_Profile sd_setImageWithURL:[NSURL URLWithString:newUrl]
                             placeholderImage:[UIImage imageNamed:@"imgplaceHolder"]];
        
    });
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [menuArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    CellIdentifier = [menuArray objectAtIndex:indexPath.row];
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: CellIdentifier forIndexPath: indexPath];
    
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50.0f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.revealViewController revealToggle:self];
    [self postNotification:indexPath.row];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(void)postNotification:(NSInteger)index{
    NSDictionary *userInfo =
    [NSDictionary dictionaryWithObject:[NSString stringWithFormat:@"%ld",(long)index ] forKey:@"revealIndex"];
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"revealNotification"
     object:self userInfo:userInfo];
}


-(void)reportAction
{
    NSString *emailTitle = @"Reporting Email";
    // Email Content
    NSString *messageBody = @"test mail";
    // To address
    NSArray *toRecipents = [NSArray arrayWithObject:@"report@spotlyte.com"];
    
    MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
    mc.mailComposeDelegate = self;
    [mc setSubject:emailTitle];
    [mc setMessageBody:messageBody isHTML:NO];
    [mc setToRecipients:toRecipents];
    
    // Present mail view controller on screen
    [self presentViewController:mc animated:YES completion:NULL];
}

- (void)mailComposeController:(MFMailComposeViewController*)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError*)error;
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            [self dismissViewControllerAnimated:YES completion:NULL];
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            [self dismissViewControllerAnimated:YES completion:NULL];
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            [self dismissViewControllerAnimated:YES completion:NULL];
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            [self dismissViewControllerAnimated:YES completion:NULL];
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    
}

-(void)pushTabBarController
{
    DashBoardViewController     *dashBoardVC = [self.storyboard instantiateViewControllerWithIdentifier:@"dashboard"];
    SettingsViewController      *settingsVC  = [self.storyboard instantiateViewControllerWithIdentifier:@"settings"];
    
    tabBarController= [[UITabBarController alloc] init];
    UIImage* tabBarBackground = [UIImage imageNamed:@"TabBarIcon"];
    [[UITabBar appearance] setBackgroundImage:tabBarBackground];
    
    tabBarController.viewControllers = @[dashBoardVC,settingsVC];
    
    UITabBar *tabBar = tabBarController.tabBar;
    UITabBarItem *tabBarItem1 = [tabBar.items objectAtIndex:0];
    UITabBarItem *tabBarItem3 = [tabBar.items objectAtIndex:1];
    
    tabBarItem1.title = @"Dashboard";
    tabBarItem3.title = @"settings";
    
    UIImage *dashImage = [UIImage imageNamed:@"dashboard"];
    UIImage *SettingsImage = [UIImage imageNamed:@"Setting_di"];
    
    
    [tabBarItem1 setImage:dashImage];
    [tabBarItem3 setImage:SettingsImage];
    
    [self.navigationController pushViewController:tabBarController animated:YES];
    
}

-(void)pushToLoginView
{
    AppDelegate *appdelref = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    UINavigationController *navigationControllerObj = [appdelref getNavigationController];
    NSLog(@"%@",navigationControllerObj.viewControllers);
    
    NSString *s1 = @"ASd as a";
    __weak NSString *s2 = s1;
    s1=nil;
    NSLog(@"%@",s2);
    
    if(navigationControllerObj)
    {
        [self.revealViewController revealToggleAnimated:YES];
        [self.revealViewController.navigationController popToRootViewControllerAnimated:YES];
    }
}


-(void)toLoginView
{
    [self.revealViewController revealToggleAnimated:YES];
}


@end
