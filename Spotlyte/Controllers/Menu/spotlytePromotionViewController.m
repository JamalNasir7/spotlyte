//
//  spotlytePromotionViewController.m
//  Spotlyte
//
//  Created by CIPL227-MOBILITY on 14/03/16.
//  Copyright © 2016 Naveen. All rights reserved.
//

#import "spotlytePromotionViewController.h"
#import "SWRevealViewController.h"
#import "ProfileViewController.h"
#import "PromtionDetailsViewController.h"
#import "HeaderView.h"
#import "SpotlytePromotionCell.h"
#import "PromotionsViewController.h"
#import "BreweriesCollectionCell.h"
#import "DailySpecialCell.h"

@interface spotlytePromotionTblViewCell ()@end
@implementation spotlytePromotionTblViewCell
@end

@interface spotlytePromotionViewController ()
{
    BOOL locationBool;
    ResturantModal *selectedResturant;
    NSMutableArray *arrySpecials;
    ResturantModal *restModal_QuickView;
}

@property (strong, nonatomic) IBOutlet UILabel *lblPlacesCount;
@property (strong, nonatomic) UICollectionViewFlowLayout *flowLayout;
@property (weak, nonatomic) IBOutlet UIButton *MenuTapBtn_lbl;
- (IBAction)MenuTapBtn:(id)sender;
@end

@implementation spotlytePromotionViewController
@synthesize arrayNonFavHotel,hotelImageArray,hotelNameArray;
@synthesize spotlyteArray;
@synthesize selectedController;
@synthesize placeArray;


- (void)viewDidLoad {
    
    
    [super viewDidLoad];
    // Load CollectionView Cell
    
    
    _tblPopup.rowHeight = UITableViewAutomaticDimension;
    _tblPopup.estimatedRowHeight = 44.0;

    placeArray = [[NSMutableArray alloc]init];
    self.viewTool.sectionTitles = @[@"Happy Hours",@"Daily Specials"];
    self.viewTool.selectedSegmentIndex = 0;
    self.viewTool.backgroundColor = [UIColor whiteColor];
    self.viewTool.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor colorWithRed:153.0/255.0 green:153.0/255.0 blue:153.0/255.0 alpha:1.0],NSFontAttributeName: [UIFont systemFontOfSize:14.0]};
    self.viewTool.selectedTitleTextAttributes = @{NSForegroundColorAttributeName : [UIColor colorWithRed:0.0/255.0 green:158.0/255.0 blue:224.0/255.0 alpha:1.0],NSFontAttributeName: [UIFont systemFontOfSize:14.0]};
    self.viewTool.selectionIndicatorColor = [UIColor colorWithRed:0.0/255.0 green:158.0/255.0 blue:224.0/255.0 alpha:1.0];
    self.viewTool.selectionStyle = HMSegmentedControlSelectionStyleFullWidthStripe;
    self.viewTool.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    self.viewTool.selectionIndicatorHeight = 2.0;
    
    [self.viewTool setIndexChangeBlock:^(NSInteger index) {
        if (index == 0){
            arrySpecials = restModal_QuickView.arrayHappyHours;
            [self showHidePopup:arrySpecials.count];
        }else{
            arrySpecials = restModal_QuickView.arraySpecials;
            [self showHidePopup:arrySpecials.count];
        }
    }];
   
    arrySpecials = [[NSMutableArray alloc]init];
    [collectionVw registerNib:[UINib nibWithNibName:@"BreweriesCollectionCell" bundle: nil] forCellWithReuseIdentifier:@"BreweriesCell"];

    HeaderView *header=[[HeaderView alloc]initWithTitle:@"SPOTLYTE PROMOTIONS" controller:self];
    [header addSubview:header.btnback];
    [header.imgViewBack setHidden:YES];
    header.BackButtonHandler=^(BOOL success){
        if (success) {
            [self.tabBarController setSelectedIndex:0];
        }
    };
    [self.view addSubview:header];
    _popupViewBG.hidden = YES;
    _popupView.hidden=YES;
    _btnCross.hidden=YES;
    _emptyView.hidden=YES;
    
    _popupView.layer.cornerRadius = 5;
    _popupView.layer.borderWidth = 1;
    _popupView.clipsToBounds = YES;
    _popupView.layer.borderColor = [[UIColor colorWithRed:153.0f/255.0f green:153.0f/255.0f blue:153.0f/255.0f alpha:1]CGColor];
    
    _btnExpand.layer.cornerRadius = 5;
    _btnExpand.layer.borderWidth = 1;
    _btnExpand.clipsToBounds = YES;
    _btnExpand.layer.borderColor = [[UIColor colorWithRed:18.0f/255.0f green:166.0f/255.0f blue:233.0f/255.0f alpha:1]CGColor];

    [self colorChangeOnPressTab:@[btnSpotlyteDeal,btnFeatureDeal]];
}

- (void)viewWillAppear:(BOOL)animated
{    [super viewWillAppear:YES];

     [self hotelPromotionsServiceHelper];
    [collectionVw setContentOffset:CGPointZero animated:NO];
     locationBool=YES;
    _MenuTapBtn_lbl.hidden=YES;
    
    if (!APP_DELEGATE.checkReachability){
        view_noPromotions.hidden  =   NO;
        collectionVw.hidden =   YES;
    }
    
    lbl_NoComments.hidden = true;
    
    BOOL tabSelected=[[NSUserDefaults standardUserDefaults] boolForKey:@"tabSelected"];
    if (tabSelected) {
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"tabSelected"];
        [self initializeLocationManager];
    }else
    {
        [collectionVw reloadData];
    }
    
    self.navigationController.navigationBar.hidden = true;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)initializeLocationManager {
    if(![CLLocationManager locationServicesEnabled]) {
        showAlertController(@"Enable Location Service", @"You have to enable the Location Service to use this App. To enable, please go to Settings->Privacy->Location Services", self, @"OK", nil,^(BOOL cancel, BOOL ok){
        });
        return;
    }
    
    if ([CLLocationManager authorizationStatus]==kCLAuthorizationStatusDenied){
        showAlertController(@"App Permission Denied", @"To re-enable, please go to Settings and turn on Location Service for this app.", self, @"OK", nil,^(BOOL cancel, BOOL ok){
        });
        return;
    }
    
    if (!_locationManager)
         _locationManager = [[CLLocationManager alloc] init];
   
    _locationManager.delegate = self;
   [_locationManager requestAlwaysAuthorization];
    _locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
    _locationManager.distanceFilter = kCLDistanceFilterNone;
    _locationManager.activityType = CLActivityTypeAutomotiveNavigation;
    [_locationManager startUpdatingLocation];
}


- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    APP_DELEGATE.userCurrentLocation=newLocation;
    if (APP_DELEGATE.userCurrentLocation.coordinate.latitude && APP_DELEGATE.userCurrentLocation.coordinate.longitude) {
        
        if (locationBool) {
            [self hotelPromotionsServiceHelper];
            locationBool=NO;}
    }
    [_locationManager stopUpdatingLocation];
}

#pragma mark -- Service Helper
-(void)specialsHappyHoursServiceHelper
{
    BOOL check=[[AppDelegate shareAppdelegateInstance] checkReachability];
    if(check){
        [CUSTOMINDICATORMACRO startLoadAnimation:self];
        [RestaurantMacro getSpecialsHappyHoursToServer:selectedResturant withCompletion:^(id obj1) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if([obj1 isKindOfClass:[ResturantModal class]]){
                  //  ResturantModal  = obj1;

                    restModal_QuickView = obj1;
                    if (restModal_QuickView.arrayHappyHours.count>0 || restModal_QuickView.arraySpecials.count>0) {
                        
                        [_tblPopup setContentOffset:CGPointZero animated:NO];
                        [self tapOnHappyHours:nil];
                        _popupViewBG.hidden = NO;
                        _popupView.hidden=NO;
                        _btnCross.hidden=NO;
                       // alertVw.hidden=NO;
                    }
                }
                else{
                    _popupViewBG.hidden = NO;
                    _popupView.hidden =NO;
                    _btnCross.hidden  =NO;
                    //[self serverError];
                }
                arrySpecials = restModal_QuickView.arrayHappyHours;
                [self showHidePopup:arrySpecials.count];

                [CUSTOMINDICATORMACRO stopLoadAnimation:self];
            });
        }];
    }
}
-(void)hotelPromotionsServiceHelper
{
    ResturantModal *modal       =   [[ResturantModal alloc]init];
    NSUserDefaults *defaults    =   [NSUserDefaults standardUserDefaults];
    NSData *data                =   [defaults objectForKey:@"logindetails"];
    
    RegisterModal * registerModalResponeValue   =   [NSKeyedUnarchiver unarchiveObjectWithData:data];
    NSString * user_IDRegister                  =   registerModalResponeValue.userID;
    
    BOOL check=[[AppDelegate shareAppdelegateInstance] checkReachability];
    if(check)
    {
        [CUSTOMINDICATORMACRO startLoadAnimation:self];
        [modal setPlaceID:@"1"];
        [modal setUserID:user_IDRegister];
        
        [PromotionMacro hotelPromotionsListValuesToServer:modal withCompletion:^(id obj1) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [CUSTOMINDICATORMACRO stopLoadAnimation:self];
            if (obj1!=nil && ![obj1 isKindOfClass:[NSNull class]]) {
            if([obj1 isKindOfClass:[PromtionsModal class]])
                {
                    PromtionsModal *modal=obj1;
                    self.spotlyteArray=  [modal.promotionArray copy];
                    self.arrayPlaces=[modal.placesArray copy];
                    if(self.spotlyteArray.count>0)
                    {
                        view_noPromotions.hidden        =   YES;
                        collectionVw.hidden =   NO;
                        dispatch_async(dispatch_get_main_queue(), ^{
                            _lblPlacesCount.text=[NSString stringWithFormat:@"%lu Places Available",(unsigned long)spotlyteArray.count];
                        });
                        [collectionVw reloadData];
                    }
                    else
                    {
                        view_noPromotions.hidden  =   NO;
                        collectionVw.hidden =   YES;
                    }
                }
                else
                {
                    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Can't Connect to Server." preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction *okBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        [self dismissViewControllerAnimated:YES completion:nil];
                    }];
                    [alertController addAction:okBtn];
                    [self presentViewController:alertController animated:YES completion:nil];
                }
             }
        else
        {
            view_noPromotions.hidden  =   NO;
            collectionVw.hidden =   YES;
        }

                
            });
        }];
        
    }
}
#pragma mark - TableView Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([tableView isEqual:_tblPopup ]){
        return arrySpecials.count;
    }
    return 0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if ([tableView isEqual:_tblPopup]) {
        if (tableView == _tblPopup)
        {
            
            static NSString *cellIdentifier = @"sampleidentifier";
            DailySpecialCell *cell = (DailySpecialCell *)[_tblPopup dequeueReusableCellWithIdentifier:cellIdentifier];
            
            if(cell == nil)
            {
                cell = [[[NSBundle mainBundle]loadNibNamed:@"DailySpecialCell" owner:self options:nil]objectAtIndex:0];
            }
            
            
            if (arrySpecials.count>0) {
                ResturantModal *modelDailySpecialDetails = [arrySpecials objectAtIndex:indexPath.row];
                
                
                _btnExpand.tag=indexPath.row;
                _btnReportHere.tag=indexPath.row;
                
                NSString *mystr;
                
                if([modelDailySpecialDetails.specials_weekDay isEqualToString:@"monday"])
                {
                    mystr=@"Monday";
                }
                else if ([modelDailySpecialDetails.specials_weekDay isEqualToString:@"tuesday"])
                {
                    mystr=@"Tuesday";
                }
                else if ([modelDailySpecialDetails.specials_weekDay isEqualToString:@"wednesday"])
                {
                    mystr=@"Wednesday";
                }
                else if ([modelDailySpecialDetails.specials_weekDay isEqualToString:@"thursday"])
                {
                    mystr=@"Thursday";
                }
                else if ([modelDailySpecialDetails.specials_weekDay isEqualToString:@"friday"])
                {
                    mystr=@"Friday";
                }
                else if ([modelDailySpecialDetails.specials_weekDay isEqualToString:@"saturday"])
                {
                    mystr=@"Saturday";
                }
                else
                {
                    mystr=@"Sunday";
                }
                
                
                
                NSString *trimmedString = [modelDailySpecialDetails.title stringByTrimmingCharactersInSet:
                                           [NSCharacterSet whitespaceCharacterSet]];
                
                NSString *stringStartTime;
                
                NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
                NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
                [dateFormatter1 setLocale:locale];

                dateFormatter1.dateFormat = @"HH:mm:ss";
                
                NSDate *date1 = [dateFormatter1 dateFromString:modelDailySpecialDetails.startTime];
                
                
                
                dateFormatter1.dateFormat = @"hh:mm a";
                
                stringStartTime = [dateFormatter1 stringFromDate:date1];
                
                stringStartTime = [stringStartTime stringByReplacingOccurrencesOfString:@":00" withString:@""];
                
                
                int stringStartTimeInt=[stringStartTime intValue];
                
                if (stringStartTimeInt<10)
                {
                    NSRange range = NSMakeRange(0,1);
                    
                    stringStartTime = [stringStartTime stringByReplacingCharactersInRange:range withString:@""];
                }
                else
                {
                    stringStartTime = [stringStartTime stringByReplacingOccurrencesOfString:@":00" withString:@""];
                }
                
                
                NSString *stringCloseTime;
                
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                NSLocale *locale2 = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
                [dateFormatter setLocale:locale2];

                dateFormatter.dateFormat = @"HH:mm:ss";
                
                NSDate *date = [dateFormatter dateFromString:modelDailySpecialDetails.endTime];
                
                
                
                dateFormatter.dateFormat = @"hh:mm a";
                
                stringCloseTime = [dateFormatter stringFromDate:date];
                
                stringCloseTime = [stringCloseTime stringByReplacingOccurrencesOfString:@":00" withString:@""];
                
                
                
                int stringCloseTimeInt=[stringCloseTime intValue];
                
                if (stringCloseTimeInt<10)
                {
                    NSRange range = NSMakeRange(0,1);
                    
                    stringCloseTime = [stringCloseTime stringByReplacingCharactersInRange:range withString:@""];
                }
                else
                {
                    stringCloseTime = [stringCloseTime stringByReplacingOccurrencesOfString:@":00" withString:@""];
                }
                
                
                
                if ([modelDailySpecialDetails.price isEqualToString:@"N/A"] || [modelDailySpecialDetails.price isEqualToString:@"1/2 Off"] || [modelDailySpecialDetails.price isEqualToString:@"Other"] || [modelDailySpecialDetails.price isEqualToString:@"FREE"])
                {
                    
                    cell.lblDays.text=mystr;
                    NSString *completeString = [NSString stringWithFormat:@"%@ %@ (%@-%@)",modelDailySpecialDetails.price,trimmedString,stringStartTime,stringCloseTime];
                    NSMutableAttributedString *AttributedString = [[NSMutableAttributedString alloc] initWithString:completeString];
                    NSString *boldString =modelDailySpecialDetails.price;
                    NSRange boldRange = [completeString rangeOfString:boldString];
                    [AttributedString addAttribute: NSFontAttributeName value:CalibriBold(11.0f) range:boldRange];
                    [cell.lblDescription setAttributedText: AttributedString];
                    
                }
                else
                {
                    cell.lblDays.text=mystr;
                    NSString *price;
                    if ([modelDailySpecialDetails.price rangeOfString:@"$"].location == NSNotFound) {
                        price = [NSString stringWithFormat:@"$%@",modelDailySpecialDetails.price];
                    }else
                    {
                        price = [NSString stringWithFormat:@"%@",modelDailySpecialDetails.price];
                    }
                    
                    cell.lblDescription.text=[NSString stringWithFormat:@"%@ %@ (%@-%@)",price, trimmedString,stringStartTime,stringCloseTime];
                    
                    
                }
                
                if(modelDailySpecialDetails.isCheckedFavSpecial == NO)
                {
                    cell.btnFavSpecial.selected = false;
                }
                else{
                    cell.btnFavSpecial.selected = true;
                }
                
                [cell.btnFavSpecial addTarget:self action:@selector(btnFavSpecialClick:) forControlEvents:UIControlEventTouchUpInside];
                cell.btnFavSpecial.tag = indexPath.row;
                
                cell.textLabel.numberOfLines = 2;
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                cell.viewSeperator.backgroundColor = [UIColor colorWithRed:153.0f/255.0f green:153.0f/255.0f blue:153.0f/255.0f alpha:1];
                
            }
            return cell;
            
        }
        
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    if ([tableView isEqual:_tblPopup]) {
        if (tableView == _tblPopup)
        {
            return  UITableViewAutomaticDimension;
        }
    }
    return 0;
}
#pragma  mark - CollectionView Delegate Methods

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}


-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if(spotlyteArray.count == 0){
        lbl_NoComments.hidden = true;
    }
    else{
        lbl_NoComments.hidden = true;
    }
    return _arrayPlaces.count;
}


-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifier=@"BreweriesCell";
    BreweriesCollectionCell *cell = [collectionVw dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    if(spotlyteArray.count > 0)
    {
        PromtionsModal *modal   =   [spotlyteArray objectAtIndex:indexPath.row];
        ResturantModal *modalRes = [self.arrayPlaces objectAtIndex:indexPath.row];
        
        cell.lblPlaceName.text = modalRes.resturantName;
        cell.btnFavourite.tag = indexPath.row;
       
        [cell.btnFavourite addTarget:self action:@selector(pressFavouriteBtn:) forControlEvents:UIControlEventTouchUpInside];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            NSURL *url = [[NSBundle mainBundle] URLForResource:@"loading-1" withExtension:@"gif"];
            NSString *newUrl = [modal.promotionImage stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
            [cell.imgView sd_setImageWithURL:[NSURL URLWithString:newUrl]
                            placeholderImage:[UIImage imageNamed:@"noimage.png"]];
            
            [cell.btnFavourite setImage:[UIImage imageNamed:[modal.promotionFavStatus isEqualToString:@"No"]?@"FavUnSelLIst":@"FavSelLIst"] forState:UIControlStateNormal];
        });
        
        /// parametre of places
        NSString *trimmedString = [modalRes.resturantCity stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        NSString *city=[NSString stringWithFormat:@"%@,",trimmedString];
        NSString *state=modalRes.resturantState;
        NSString *zipcode=modalRes.resturantPostalCode;
        cell.lblAddress.text=[NSString stringWithFormat:@"%@\n%@ %@ %@",modalRes.resturantAddress,[modalRes.resturantCity isEqualToString:@""]?@"":city,[state isEqualToString:@""]?@"":state,[zipcode isEqualToString:@""]?@"":zipcode];
        if([modalRes.placeRatings isEqualToString:@"0"])
        {
            cell.lblOverallRating.text=@"N/A";
        }
        else{
            cell.lblOverallRating.text=modalRes.placeRatings;
        }
        cell.lblLike.text=[modalRes.likecount isEqualToString:@"1"]?@"1 Like":[modalRes.likecount isEqualToString:@""]?@"0 Likes":[NSString stringWithFormat:@"%@ Likes",modalRes.likecount];
        cell.btnLike.tag = indexPath.row;
        if([modalRes.likeORDislike isEqualToString:@"2"] || [modalRes.likeORDislike isEqualToString:@""]){
            
            [cell.btnLike setTitle:@"Like" forState:UIControlStateNormal];
            [cell.btnLike setImage:[UIImage imageNamed:@"like"] forState:UIControlStateNormal];
        }
        else{
            [cell.btnLike setTitle:@"Dislike" forState:UIControlStateNormal];
            [cell.btnLike setImage:[UIImage imageNamed:@"disLike"] forState:UIControlStateNormal];
        }
        [cell.btnLike addTarget:self action:@selector(tapOnLike:) forControlEvents:UIControlEventTouchUpInside];
        int specialCount=[modalRes.happy_hour_count intValue]+[modalRes.place_specials_count intValue];

        [cell.btnSpecials setTitle:[NSString stringWithFormat:@"%d specials",specialCount] forState:UIControlStateNormal];
        [cell.btnSpecials addTarget:self action:@selector(tapOnSpecial:) forControlEvents:UIControlEventTouchUpInside];
        cell.btnSpecials.tag = indexPath.item;
        [cell.btnPressReport addTarget:self action:@selector(pressReportHereBtn_cell:) forControlEvents:UIControlEventTouchUpInside];
        cell.btnPressReport.tag = indexPath.item;
        //starRatingView = cell.lblStar;
       // [self displayStar:modalRes.placeRatings];
       // cell.SpecialBtn_Y.constant=0;
        cell.lblReviews.text=[modalRes.totalReview isEqualToString:@"1"]?@"1 review":[modalRes.totalReview isEqualToString:@"0"]?@"0 review":[NSString stringWithFormat:@"%@ reviews",modalRes.totalReview];
        
        cell.lblMiles.text=[NSString stringWithFormat:@"%.1f miles",[modalRes.mile floatValue]];
    }
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    PromtionDetailsViewController *promtionDetailsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"promtiondetails"];
    [self.view endEditing:YES];
    promtionDetailsVC.promtionModalObj = [spotlyteArray objectAtIndex:indexPath.row];
    promtionDetailsVC.resturantModalObj= [_arrayPlaces objectAtIndex:indexPath.row];
    promtionDetailsVC.promtions=_arrayPlaces;
    [self.navigationController pushViewController:promtionDetailsVC animated:YES];
}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    float cellWidth =screenRect.size.width;
    //float cellHeight = screenRect.size.height/4-5;
    float cellHeight=115;
    CGSize size = CGSizeMake(cellWidth,cellHeight);
    return size;
}


- (void)displayStar :(NSString *)ratingVal
{
    if(ratingVal.floatValue <= 2.0)
    {
        starRatingView.starHighlightedImage = [UIImage imageNamed:@"RedStarIcon"];
    }
    else if(ratingVal.floatValue <= 3.9)
    {
        starRatingView.starHighlightedImage = [UIImage imageNamed:@"YellowStarIcon"];
    }
    else
    {
        starRatingView.starHighlightedImage = [UIImage imageNamed:@"GreenStarIcon"];
    }
    
    starRatingView.starImage = [UIImage imageNamed:@"UnStarIcon"];
    starRatingView.maxRating = 5.0;
    starRatingView.delegate = self;
    starRatingView.horizontalMargin = 10;
    starRatingView.editable = NO;
    starRatingView.rating = ratingVal.floatValue;
    starRatingView.displayMode = EDStarRatingDisplayAccurate;
}


#pragma mark - Selector Button Action Methods
- (IBAction)pressNavigateSpecialHours:(UIButton*)sender
{
    PromotionsViewController *promtionVC = [self.storyboard instantiateViewControllerWithIdentifier:@"promtions"];
    promtionVC.selectedValue = sender.tag;
    promtionVC.placeIDString = selectedResturant.placeID;
    promtionVC.promtionsArray = placeArray;
    promtionVC.selectedWhichController = @"specialshappyhours";
    
    BOOL isCheckedRandomSearch = [[NSUserDefaults standardUserDefaults]boolForKey:@"randomsearch"];
    if(isCheckedRandomSearch == TRUE)
    {
        promtionVC.checkTextOrSearch = @"customsearch";
    }
    else
    {
        promtionVC.checkTextOrSearch = @"nosearch";
        [[NSUserDefaults standardUserDefaults]setBool:FALSE forKey:@"randomsearch"];
        
    }
    [self.navigationController pushViewController:promtionVC animated:YES];
    
    /*promtionVC.selectHomeVC=^(ResturantModal *restModel){
     
     promotionModalOBJ = restModel;
     };*/
}
-(void)showHidePopup:(NSInteger)count{
    
    if (count==0) {
        _emptyView.hidden=NO;
        _tblPopup.hidden=YES;
        _btnExpand.hidden=YES;
        _btnReportHere.hidden=YES;
        
        return;
    }else{
        _tblPopup.hidden=NO;
        _emptyView.hidden=YES;
        _btnExpand.hidden=NO;
        _btnReportHere.hidden=NO;
    }
    [_tblPopup reloadData];
    
}
- (IBAction)tapOnHappyHours:(id)sender {
    //[self colorChangeOnPressTab:@[_btnHppyHours,_btnDailySpecials]];
    arrySpecials = restModal_QuickView.arrayHappyHours;
    [self showHidePopup:arrySpecials.count];
}

- (IBAction)tapOnDailySpecials:(id)sender {
   // [self colorChangeOnPressTab:@[_btnDailySpecials,_btnHppyHours]];
    arrySpecials = restModal_QuickView.arraySpecials;
    [self showHidePopup:arrySpecials.count];
}
- (void)btnFavSpecialClick:(id)sender {
    UIButton *btn = (UIButton*)sender;
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:btn.tag inSection:0];
    DailySpecialCell *cell = (DailySpecialCell *)[_tblPopup cellForRowAtIndexPath:indexPath];
    
    ResturantModal *modelDailySpecialDetails = [arrySpecials objectAtIndex:btn.tag];
    
    NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:@"logindetails"];
    RegisterModal *registerModalResponeValue = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    NSString *strType;
    if(self.viewTool.selectedSegmentIndex == 0){
        strType = @"h";
    }
    else{
        strType = @"s";
    }
    
    NSString *strFav;
    if(btn.selected == true){
        strFav = @"2";
    }
    else{
        strFav = @"1";
    }
    
    BOOL check=[[AppDelegate shareAppdelegateInstance] checkReachability];
    if(check)
    {
        [modelDailySpecialDetails setUserID:registerModalResponeValue.userID];
        [CUSTOMINDICATORMACRO startLoadAnimation:self];
        [RestaurantMacro favUnfavSpecialToServer:modelDailySpecialDetails :strFav :strType withCompletion:^(id obj1) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                ResturantModal *responseModal = nil;
                if([obj1 isKindOfClass:[ResturantModal class]])
                {
                    responseModal = obj1;
                    if([responseModal.result isEqualToString:@"success"]){
                        if(btn.selected == true){
                            [modelDailySpecialDetails setIsCheckedFavSpecial:NO];
                        }
                        else{
                            [modelDailySpecialDetails setIsCheckedFavSpecial:YES];
                        }
                        
                        //                        [_tblPopup beginUpdates];
                        //                        [_tblPopup reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:false];
                        //                        [_tblPopup endUpdates];
                        if(btn.selected == true){
                            [cell.btnFavSpecial setSelected:false];
                        }
                        else{
                            [cell.btnFavSpecial setSelected:true];
                        }
                    }
                }
                else
                    [self serverError];
                
                [CUSTOMINDICATORMACRO stopLoadAnimation:self];
            });
        }];
    }
}

-(void)tapOnSpecial:(UIButton*)sender{
    
   // NSIndexPath *indexpath=[NSIndexPath indexPathForRow:sender.tag inSection:0];
   // CGRect myRect = [tableView_Favourite rectForRowAtIndexPath:indexpath];
   // CGRect rectInSuperview = [tableView_Favourite convertRect:myRect toView:[tableView_Favourite superview]];
    self.viewTool.selectedSegmentIndex = 0;
   // [self changePositionOfPopupView:rectInSuperview];
   

    selectedResturant = [_arrayPlaces objectAtIndex:sender.tag];
    _lblpopupName.text=selectedResturant.resturantName;
    if ([selectedResturant.happy_hour_count intValue]+[selectedResturant.place_specials_count intValue]!=0){
        [self specialsHappyHoursServiceHelper];
       // selectedRow=sender.tag;
    }
}
- (IBAction)tapOnCrossBtn:(id)sender {
    
    _popupViewBG.hidden = YES;
    _popupView.hidden=YES;
    _btnCross.hidden=YES;
   // alertVw.hidden=YES;
}
-(IBAction)pressReportHereBtn_cell : (UIButton*)sender
{
    /*ResturantModal *modal;
     if(isCheckedTab == FALSE)
     modal = [placeArray objectAtIndex:sender.tag];
     else
     modal = [promotionsArray objectAtIndex:sender.tag];
     */
    
        selectedResturant = [_arrayPlaces objectAtIndex:sender.tag];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:selectedResturant.resturantName message:@"Specials and Hours are subject to change. To ensure the most up-to-date specials you can view this locations website specials here" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *YesBtn = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                             {
                                 
                                 
                                 //[selectedResturant setWebSite_Specials:modal.webSite_Specials];
                                 if(selectedResturant.webSite_Specials.length>0)
                                 {
                                     TermsViewController *termsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"termsvc"];
                                     termsVC.checkVC = @"promotions";
                                     termsVC.checkBack = @"homevc";
                                     termsVC.CheckUrlType=@"SpecialLink";
                                     termsVC.restModal_TermsVC = selectedResturant;
                                     [self.navigationController pushViewController:termsVC animated:YES];
                                 }
                                 else
                                 {
                                     
                                     TermsViewController *termsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"termsvc"];
                                     termsVC.checkVC = @"promotions";
                                     termsVC.checkBack = @"homevc";
                                     termsVC.CheckUrlType=@"ResturantEMail";
                                     termsVC.restModal_TermsVC = selectedResturant;
                                     [self.navigationController pushViewController:termsVC animated:YES];
                                 }
                             }];
    
    UIAlertAction *NoBtn = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                            {
                                [self dismissViewControllerAnimated:YES completion:nil];
                            }];
    
    [alertController addAction:YesBtn];
    [alertController addAction:NoBtn];
    
    [self presentViewController:alertController animated:YES completion:nil];
    
}
-(IBAction)pressReportHereBtn : (UIButton*)sender
{
    /*ResturantModal *modal;
     if(isCheckedTab == FALSE)
     modal = [placeArray objectAtIndex:sender.tag];
     else
     modal = [promotionsArray objectAtIndex:sender.tag];
     */
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:selectedResturant.resturantName message:@"Specials and Hours are subject to change. To ensure the most up-to-date specials you can view this locations website specials here" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *YesBtn = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                             {
                                 
                                 
                                     //[selectedResturant setWebSite_Specials:modal.webSite_Specials];
                                     if(selectedResturant.webSite_Specials.length>0)
                                     {
                                         TermsViewController *termsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"termsvc"];
                                         termsVC.checkVC = @"promotions";
                                         termsVC.checkBack = @"homevc";
                                         termsVC.CheckUrlType=@"SpecialLink";
                                         termsVC.restModal_TermsVC = selectedResturant;
                                         [self.navigationController pushViewController:termsVC animated:YES];
                                     }
                                     else
                                     {
                                         
                                         TermsViewController *termsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"termsvc"];
                                         termsVC.checkVC = @"promotions";
                                         termsVC.checkBack = @"homevc";
                                         termsVC.CheckUrlType=@"ResturantEMail";
                                         termsVC.restModal_TermsVC = selectedResturant;
                                         [self.navigationController pushViewController:termsVC animated:YES];
                                     }
                             }];
    
    UIAlertAction *NoBtn = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                            {
                                [self dismissViewControllerAnimated:YES completion:nil];
                            }];
    
    [alertController addAction:YesBtn];
    [alertController addAction:NoBtn];
    
    [self presentViewController:alertController animated:YES completion:nil];
    
}
-(IBAction)pressFavouriteBtn :(UIButton *)sender
{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [defaults objectForKey:@"logindetails"];
    RegisterModal *registerModalResponeValue = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    NSString * user_IDRegister = registerModalResponeValue.userID;
    
    //UIButton *button = (UIButton *)sender;
    PromtionsModal *modal = [spotlyteArray objectAtIndex:sender.tag];
    [modal setUser_ID:user_IDRegister];
    if([modal.promotionFavStatus isEqualToString:@"No"])
    {
        [self favouriteServiceHelper:modal andwith:sender];
    }
    else
    {
        [self unFavouriteServiceHelper:modal andwith:sender];
    }
}


-(void)favouriteServiceHelper :(PromtionsModal *)modalObj andwith:(UIButton*)sender
{
    BOOL check=[[AppDelegate shareAppdelegateInstance] checkReachability];
    if(check)
    {
        [CUSTOMINDICATORMACRO startLoadAnimation:self];
        [PromotionMacro addFavouriteSpotlyteToServer:modalObj withCompletion:^(id obj1) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [CUSTOMINDICATORMACRO stopLoadAnimation:self];
                if([obj1 isKindOfClass:[ResturantModal class]])
                {
                    ResturantModal *responseModal = nil;
                    responseModal = obj1;
                    if([responseModal.result isEqualToString:@"success"])
                    {
                         [sender setImage:[UIImage imageNamed:@"FavSelLIst"] forState:UIControlStateNormal];
                        [modalObj setPromotionFavStatus:@"Yes"];
                    }
                }
                else
                   [self serverError];
            });
        }];
    }
}

-(void)unFavouriteServiceHelper :(PromtionsModal *)modalObj andwith:(UIButton*)sender
{
    BOOL check=[[AppDelegate shareAppdelegateInstance] checkReachability];
    if(check)
    {
        [CUSTOMINDICATORMACRO startLoadAnimation:self];
        [PromotionMacro unFavouriteSpotlyteToServer:modalObj withCompletion:^(id obj1) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [CUSTOMINDICATORMACRO stopLoadAnimation:self];
                if([obj1 isKindOfClass:[ResturantModal class]])
                {
                    ResturantModal *responseModal;
                    responseModal = obj1;
                    if([responseModal.result isEqualToString:@"success"])
                    {
                      [sender setImage:[UIImage imageNamed:@"FavUnSelLIst"] forState:UIControlStateNormal];
                         [modalObj setPromotionFavStatus:@"No"];
                    }
                }
                else
                [self serverError];
            });
        }];
    }
    
}

-(void)tapOnLike:(UIButton*)sender{
    NSLog(@"%ld",(long)sender.tag);
   // [sender setBackgroundColor:[UIColor blueColor]];
    ResturantModal *modal = [self.arrayPlaces objectAtIndex:sender.tag];
    NSInteger  strLike = [modal.likecount integerValue];
    if ([modal.likeORDislike isEqualToString:@"0"] || [modal.likeORDislike isEqualToString:@"2"]) {
        [modal.likeORDislike isEqualToString:@"1"];
        [modal setLikeORDislike:@"1"];
        modal.likeORDislike = @"1";
        modal.likecount = [NSString stringWithFormat:@"%ld",(long)strLike+1];
    }
    else{
        [modal.likeORDislike isEqualToString:@"2"];
        [modal setLikeORDislike:@"2"];
        modal.likeORDislike = @"2";
        if (strLike > 0)
        {
            modal.likecount = [NSString stringWithFormat:@"%ld",(long)strLike-1];
        }
    }
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [defaults objectForKey:@"logindetails"];
    RegisterModal *registerModalResponeValue = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    NSString * user_IDRegister = registerModalResponeValue.userID;
    [modal setUserID:user_IDRegister];
    NSString *checkcount  = modal.likeORDislike;
    BOOL check=[[AppDelegate shareAppdelegateInstance] checkReachability];
    if(check)
    {
        [CUSTOMINDICATORMACRO startLoadAnimation:self];
        [RestaurantMacro likeOrDislikeToServer:modal withCompletion:^(id obj1) {
            dispatch_async(dispatch_get_main_queue(), ^{
                ResturantModal *responseModal = nil;
                
                if([obj1 isKindOfClass:[ResturantModal class]])
                {
                    responseModal = obj1;
                    if([responseModal.result isEqualToString:@"success"])
                    {
                        [collectionVw reloadData];
                    }
                    if([checkcount isEqualToString:@"2"])
                    {
                        
                    }
                }
                else
                {
                    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Please check your Internet Connection." preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction *okBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        [self dismissViewControllerAnimated:YES completion:nil];
                    }];
                    
                    [alertController addAction:okBtn];
                    [self presentViewController:alertController animated:YES completion:nil];
                }
                [CUSTOMINDICATORMACRO stopLoadAnimation:self];
            });
        }];
        
    }
}


-(IBAction)btnMethods:(UIButton*)sender{
    switch (sender.tag) {
        case 0:{
            [self colorChangeOnPressTab:@[btnSpotlyteDeal,btnFeatureDeal]];
            
        }
            break;
        case 1:{
            [self colorChangeOnPressTab:@[btnFeatureDeal,btnSpotlyteDeal]];
         
        }
            break;
            
        default:
            break;
    }
}

-(void)colorChangeOnPressTab:(NSArray*)arry{
    dispatch_async(dispatch_get_main_queue(), ^{
   
    int btnNo;
    for (btnNo=0; btnNo<arry.count; btnNo++) {
        if (btnNo==0) {
            
            UIButton *button =(UIButton*)arry[0];
            button.backgroundColor=[UIColor colorWithRed:0.0f/255.0f green:204.0f/255.0f blue:255.0f/255.0f alpha:1];
            [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        }else
        {
            UIButton *button =(UIButton*)arry[btnNo];
            button.backgroundColor=[UIColor whiteColor];
            [button setTitleColor:[UIColor colorWithRed:54.0f/255.0f green:69.0f/255.0f blue:79.0f/255.0f alpha:1] forState:UIControlStateNormal];
        }
    }
  });
}


- (IBAction)MenuTapBtn:(id)sender
{
    _MenuTapBtn_lbl.hidden=YES;
    [self.view endEditing:YES];
    [self.revealViewController revealToggle:0];
}


-(void)serverError{

    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Can't Connect to Server." preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
    [alertController addAction:okBtn];
    [self presentViewController:alertController animated:YES completion:nil];
}

-(void)dealloc{

    if (_locationManager) {
        [_locationManager stopUpdatingLocation];
        _locationManager=nil;
    }
  
}

@end
