//
//  ReportsViewController.h
//  SpotlyteTestApp
//
//  Created by CIPL137-MOBILITY on 20/04/16.
//  Copyright © 2016 Muthu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface ReportsViewController : UIViewController <UITableViewDataSource, UITableViewDelegate,UITextViewDelegate,UIGestureRecognizerDelegate>
{
    NSMutableArray *arrayReasons;
    IBOutlet UIView *viewSub;
}

@property (weak, nonatomic) IBOutlet UITextField *txtReportReason;
@property (weak, nonatomic) IBOutlet UITextView *txtviewComments;
@property (weak, nonatomic) IBOutlet UITableView *tblReason;



- (IBAction)showReasonButtonPressed:(id)sender;

@end
