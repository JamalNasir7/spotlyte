//
//  TopRatedViewController.m
//  Spotlyte
//
//  Created by Admin on 19/04/16.
//  Copyright © 2016 Naveen. All rights reserved.
//

#import "TopRatedViewController.h"
#import "EventsModel.h"
@interface TopRatedViewController ()

@end

@implementation TopRatedViewController
@synthesize restModalResponeOBJ;
- (void)viewDidLoad {
    
    self.arrayRated = [[NSMutableArray alloc] init];
    typeValue = 0;
    [self topRatedServiceHelper];
    
    [super viewDidLoad];
}

#pragma mark --- Total History ServiceHelper

-(void)topRatedServiceHelper
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [defaults objectForKey:@"logindetails"];
    RegisterModal * registerModalResponeValue = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    NSString *user_IDRegister = registerModalResponeValue.userID;
    ResturantModal *modal = [[ResturantModal alloc]init];
    [modal setUserID:user_IDRegister];
    
    BOOL check=[[AppDelegate shareAppdelegateInstance] checkReachability];
    if(check)
    {
        [CUSTOMINDICATORMACRO startLoadAnimation:self];
        
        [RestaurantMacro listTopRatedToServer:modal withCompletion:^(id obj1) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [CUSTOMINDICATORMACRO stopLoadAnimation:self];
                if([obj1 isKindOfClass:[ResturantModal class]])
                {
                    restModalResponeOBJ = obj1;
                    self.arrayRated = [[self sortRatingArray:restModalResponeOBJ.arrayBars]mutableCopy];
                    ;
                    if(self.arrayRated.count > 0)
                    {
                        tblViewRated.hidden = NO;
                        view_NoValues.hidden = YES;
                        [tblViewRated reloadData];
                    }
                    else
                    {
                        tblViewRated.hidden = YES;
                        view_NoValues.hidden = NO;
                    }
                    
                }
                else
                {
                    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Please check your Internet Connection." preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction *okBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        [self dismissViewControllerAnimated:YES completion:nil];
                    }];
                    [alertController addAction:okBtn];
                    [self presentViewController:alertController animated:YES completion:nil];
                }
                
            });
        }];
    }
}


-(NSArray *)sortRatingArray :(NSArray *)array
{
    NSSortDescriptor *sortDescriptor;
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"userRatings"
                                                 ascending:NO];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    NSArray *sortedArray = [array sortedArrayUsingDescriptors:sortDescriptors];
    return sortedArray;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - TableView Delegates

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 125;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.arrayRated count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"RatedTableCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier forIndexPath:indexPath];
    
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
        
    }
    
    if([self.arrayRated count] > 0)
    {
        ResturantModal *restObj = [self.arrayRated objectAtIndex:indexPath.row];
        
        UIImageView *imageviewEvent = (UIImageView *)[cell.contentView viewWithTag:100];
        imageviewEvent.layer.cornerRadius = 3.0;
        [imageviewEvent setBackgroundColor:[UIColor clearColor]];
        imageviewEvent.layer.masksToBounds = YES;
        
        dispatch_async(dispatch_get_main_queue(), ^{
            NSURL *url = [[NSBundle mainBundle] URLForResource:@"loading-1" withExtension:@"gif"];
            NSString *newUrl = [restObj.placeImage stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
            [imageviewEvent sd_setImageWithURL:[NSURL URLWithString:newUrl]
                              placeholderImage:[UIImage imageNamed:@"noimage.png"]];
            
        });
        
        
        UILabel *lblRestName            = (UILabel *)[cell.contentView viewWithTag:101];
        lblRestName.text                = restObj.resturantName;
        
        UILabel *lblAddressCity         = (UILabel *)[cell.contentView viewWithTag:102];
        lblAddressCity.text             = [NSString stringWithFormat:@"%@, %@",restObj.resturantAddress,restObj.resturantCity];
        
        UILabel *lblPinCountry          = (UILabel *)[cell.contentView viewWithTag:103];
        lblPinCountry.text              = [NSString stringWithFormat:@"%@, %@",restObj.resturantPostalCode,restObj.resturantCountry];
        
        UILabel *lblRating              = (UILabel *)[cell.contentView viewWithTag:104];
        lblRating.text                  = restObj.placeRatings;
        
        
        
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    HomeViewController *homeVC = [self.storyboard instantiateViewControllerWithIdentifier:@"homescreen"];
    
    NSMutableArray *array = [[NSMutableArray alloc]init];
    
    ResturantModal *modal = [self.arrayRated objectAtIndex:indexPath.row];
    
    [array addObject:modal];
    homeVC.restListArray = array;
    homeVC.selectedRestValue = indexPath.row;
    homeVC.checkVC = @"restlist";
    homeVC.restVCString = @"singlemap";
    [self.navigationController pushViewController:homeVC animated:YES];
}
- (void)displayStar :(NSString *)ratingVal
{
    if(ratingVal.floatValue <= 2.0)
    {
        starRatingView.starHighlightedImage = [UIImage imageNamed:@"RedStarIcon"];
    }
    else if(ratingVal.floatValue <= 3.9)
    {
        starRatingView.starHighlightedImage = [UIImage imageNamed:@"YellowStarIcon"];
    }
    else
    {
        starRatingView.starHighlightedImage = [UIImage imageNamed:@"GreenStarIcon"];
    }
    starRatingView.starImage = [UIImage imageNamed:@"UnStarIcon"];
    starRatingView.maxRating = 5.0;
    starRatingView.delegate = self;
    starRatingView.horizontalMargin = 12;
    starRatingView.editable = YES;
    starRatingView.rating = ratingVal.intValue;
    starRatingView.displayMode = EDStarRatingDisplayAccurate;
}
#pragma mark - Button Action Methods

-(IBAction)favouriteAction:(UIButton *)sender
{
    
}

-(IBAction)pressTabBar:(id)sender
{
    UIButton *button = (UIButton *)sender;
    
    if(button.tag == 0)
    {
        
        [UIView animateWithDuration:0.3
                              delay:0.1
                            options: UIViewAnimationOptionCurveEaseInOut
                         animations:^
         {
             
             CGRect frame = imageView_Indicator.frame;
             frame.origin.x = button.frame.origin.x+20;
             frame.size.width = button.frame.size.width - 40;
             
             imageView_Indicator.frame = frame;
             
             self.arrayRated = [[self sortRatingArray:restModalResponeOBJ.arrayBars]mutableCopy];
             if(self.arrayRated.count > 0)
             {
                 tblViewRated.hidden = NO;
                 view_NoValues.hidden = YES;
                 [tblViewRated reloadData];
             }
             else
             {
                 tblViewRated.hidden = YES;
                 view_NoValues.hidden = NO;
             }
             
             
         }
                         completion:^(BOOL finished)
         {
             
         }];
    }
    else if (button.tag == 1)
    {
        
        [UIView animateWithDuration:0.3
                              delay:0.1
                            options: UIViewAnimationOptionCurveEaseInOut
                         animations:^
         {
             
             CGRect frame = imageView_Indicator.frame;
             frame.origin.x = button.frame.origin.x+20;
             frame.size.width = button.frame.size.width - 40;
             
             imageView_Indicator.frame = frame;
             self.arrayRated = [[self sortRatingArray:restModalResponeOBJ.arrayResturants]mutableCopy];
             if(self.arrayRated.count > 0)
             {
                 tblViewRated.hidden = NO;
                 view_NoValues.hidden = YES;
                 [tblViewRated reloadData];
             }
             else
             {
                 tblViewRated.hidden = YES;
                 view_NoValues.hidden = NO;
             }
             
             
         }
                         completion:^(BOOL finished)
         {
             
         }];
    }
    else
    {
        [UIView animateWithDuration:0.3
                              delay:0.1
                            options: UIViewAnimationOptionCurveEaseInOut
                         animations:^
         {
             
             CGRect frame = imageView_Indicator.frame;
             frame.origin.x = button.frame.origin.x+20;
             frame.size.width = button.frame.size.width - 40;
             
             imageView_Indicator.frame = frame;
             self.arrayRated = [[self sortRatingArray:restModalResponeOBJ.arrayOthers]mutableCopy];
             if(self.arrayRated.count > 0)
             {
                 tblViewRated.hidden = NO;
                 view_NoValues.hidden = YES;
                 [tblViewRated reloadData];
             }
             else
             {
                 tblViewRated.hidden = YES;
                 view_NoValues.hidden = NO;
             }
             
         }
                         completion:^(BOOL finished)
         {
             
         }];
        
    }
    typeValue = button.tag;
    
    
}



-(IBAction)pressBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


@end
