//
//  ReportsViewController.m
//  SpotlyteTestApp
//
//  Created by CIPL137-MOBILITY on 20/04/16.
//  Copyright © 2016 Muthu. All rights reserved.
//

#import "ReportsViewController.h"
#import "HeaderView.h"

@interface ReportsViewController ()

@end

@implementation ReportsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    HeaderView *header=[[HeaderView alloc]initWithTitle:@"CONTACT US / REPORT ISSUE" controller:self];
    [header addSubview:header.btnback];
    [self.view addSubview:header];
    
    arrayReasons = [[NSMutableArray alloc]init];
    [arrayReasons addObject:@"Report an error with a special?"];
    [arrayReasons addObject:@"Report an error with a promotion?"];
    [arrayReasons addObject:@"Report and Error with a Top Spot?"];
    [arrayReasons addObject:@"General suggestions/comments?"];
    [arrayReasons addObject:@"Issue with your account?"];
    //[arrayReasons addObject:@"List a Promotion?"];
    
    
    self.txtviewComments.text = @"";
    self.txtviewComments.delegate = self;
    
    self.tblReason.tableFooterView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.tblReason.frame.size.width, 0.1f)];
    
    UITapGestureRecognizer *letterTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(pressTap:)];
    [self.view addGestureRecognizer:letterTapRecognizer];
    letterTapRecognizer.delegate = self;
    self.txtviewComments.layer.cornerRadius=8.0f;
    self.txtviewComments.layer.masksToBounds=YES;
    self.txtviewComments.layer.borderColor=[[UIColor lightGrayColor]CGColor];
    self.txtviewComments.layer.borderWidth= 1.0f;
    
    self.tblReason.layer.cornerRadius=8.0f;
    self.tblReason.layer.masksToBounds=YES;
    self.tblReason.layer.borderColor=[[UIColor lightGrayColor]CGColor];
    self.tblReason.layer.borderWidth= 1.0f;
    
}
- (void)pressTap:(UITapGestureRecognizer*)sender {
    [self.txtviewComments resignFirstResponder];
    viewSub.hidden = YES;
    self.txtviewComments.hidden = NO;
}
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if ([touch.view isDescendantOfView:self.tblReason]) {
        
        return NO;
    }
    
    return YES;
}

#pragma mark --- Report ServiceHelper

-(void)reportServiceHelper
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [defaults objectForKey:@"logindetails"];
    RegisterModal * registerModalResponeValue = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    NSString *user_IDRegister = registerModalResponeValue.userID;
    
    TotalHistoryModal *modal = [[TotalHistoryModal alloc]init];
    [modal setUserID_History:user_IDRegister];
    [modal setReason:self.txtReportReason.text];
    [modal setRestDescription:self.txtviewComments.text];
    
    BOOL check=[[AppDelegate shareAppdelegateInstance] checkReachability];
    if(check)
    {
        [CUSTOMINDICATORMACRO startLoadAnimation:self];
        
        [HistoryMacro reportToServer:modal withCompletion:^(id obj1) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [CUSTOMINDICATORMACRO stopLoadAnimation:self];
                if([obj1 isKindOfClass:[TotalHistoryModal class]])
                {
                    TotalHistoryModal *responseModal = obj1;
                    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:responseModal.result message:responseModal.message preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction *okBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        [self dismissViewControllerAnimated:YES completion:nil];
                        [self.navigationController popViewControllerAnimated:YES];
                    }];
                    
                    [alertController addAction:okBtn];
                    [self presentViewController:alertController animated:YES completion:nil];
                }
                else
                {
                    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Please check your Internet Connection." preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction *okBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        [self dismissViewControllerAnimated:YES completion:nil];
                    }];
                    [alertController addAction:okBtn];
                    [self presentViewController:alertController animated:YES completion:nil];
                }
                
                
            });
        }];
    }
    
}
-(IBAction)pressSubmitBtn:(id)sender
{
    if(self.txtReportReason.text.length != 0)
    {
        [self reportServiceHelper];
    }
    else
    {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Warning" message:@"Please select the reason" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self dismissViewControllerAnimated:YES completion:nil];
        }];
        
        [alertController addAction:okBtn];
        [self presentViewController:alertController animated:YES completion:nil];
    }
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -- Text View Delegate Method

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if ([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        
        return NO;
    }
    
    return YES;
}


#pragma mark - Button Methods

- (IBAction)showReasonButtonPressed:(id)sender
{
    viewSub.hidden = NO;
    self.txtviewComments.hidden = YES;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    return [arrayReasons count];
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cell_identifier = @"";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cell_identifier];
    if(cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cell_identifier];
    }
    
    cell.textLabel.text = [arrayReasons objectAtIndex:indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
    
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.txtReportReason.text = [arrayReasons objectAtIndex:indexPath.row];
    viewSub.hidden = YES;
    self.txtviewComments.hidden = NO;
}

-(IBAction)pressBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
