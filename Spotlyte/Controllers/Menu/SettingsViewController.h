//
//  SettingsViewController.h
//  Spotlyte
//
//  Created by Admin on 16/03/16.
//  Copyright © 2016 Naveen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    IBOutlet UITableView    *tableView_Settings;
}
-(IBAction)pressEditProfileBtn:(id)sender;
@property (strong, nonatomic) IBOutlet UITableView *tableVw;


@end
