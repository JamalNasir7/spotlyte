//
//  SettingsViewController.m
//  Spotlyte
//
//  Created by Admin on 16/03/16.
//  Copyright © 2016 Naveen. All rights reserved.
//

#import "SettingsViewController.h"
#import "EditProfileViewController.h"
#import "HeaderView.h"
#import "SettingTableCell.h"
#import "FirstWelcomeScreen.h"
#import "TermsConditionViewController.h"
#import "LoginViewController.h"
#import <TwitterKit/TwitterKit.h>
#import "TutorialScreenController.h"

@interface SettingsViewController ()
{
    NSDictionary *contents;
}
@end

@implementation SettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    HeaderView *header=[[HeaderView alloc]initWithTitle:@"Settings" controller:self];
    [header addSubview:header.btnback];
    [self.view addSubview:header];
    
    
    _tableVw.delegate=self;
    _tableVw.dataSource=self;
    
    contents=@{@"heading":@[@"GENERAL",@"SOCIAL",@"OTHER"],
              
               @"content":@[@[@"EDIT ACCOUNT",@"VIEW TUTORIAL"],
                            @[@"SHARE THE APP",@"TELL US HOW WE ARE DOING. RATE US"],
                            @[@"TERMS & CONDITIONS",@"PRIVACY POLICY"]],
             
               @"imageName":@[@[@"Editaccount",@"playbutton"],
                              @[@"Shareapp",@"RateUs"],
                              @[@"Terms&Condition",@"PrivacyPolicy"]],
               
                  @"tag":@[@[@1,@2],
                           @[@3,@4],
                           @[@5,@6]]
               };
    
    _tableVw.contentInset = UIEdgeInsetsMake(-20, 0, 0, 0);

}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 3;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [contents[@"content"][section] count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 40.0f;
}


-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
     UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 40.0f)];
    [view setBackgroundColor:[UIColor clearColor]];
    
    UILabel *lblHeaderTitle=createLabelWithFrame(CGRectMake(10, 0, 320, 40), contents[@"heading"][section], [UIColor blackColor], [UIColor clearColor], [UIFont fontWithName:@"Helvetica-Bold" size:21], NSTextAlignmentLeft, 10, 0, nil);
    
    [view addSubview:lblHeaderTitle];
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
      return 40;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellID= @"settingshomecell";
    SettingTableCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    
    if (!cell) {
        [_tableVw registerNib:[UINib nibWithNibName:@"SettingTableCell" bundle:nil] forCellReuseIdentifier:cellID];
        cell = [_tableVw dequeueReusableCellWithIdentifier:cellID];
    }
    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    cell.settingImgView.image=[UIImage imageNamed:contents[@"imageName"][indexPath.section][indexPath.row]];
    cell.settingLabel.text=contents[@"content"][indexPath.section][indexPath.row];
    cell.tag=(NSInteger)contents[@"tag"][indexPath.section][indexPath.row];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    int cellTag=[contents[@"tag"][indexPath.section][indexPath.row] intValue];

    switch (cellTag) {
        case 1:{
            EditProfileViewController *editProfileVC = [self.storyboard instantiateViewControllerWithIdentifier:@"editProfileVC"];
            [self.navigationController pushViewController:editProfileVC animated:YES];
            break;
        }
            
        case 2:{

            TutorialScreenController *firstCon=[self.storyboard instantiateViewControllerWithIdentifier:@"tutorialCon"];
            UINavigationController *navigationController = [[UINavigationController alloc]initWithRootViewController:firstCon];
            navigationController.navigationBarHidden=YES;
            APP_DELEGATE.settingEnable=YES;
            
            [self presentViewController:navigationController animated:YES completion:nil];
            
            break;
        }
    
        case 3:{
            NSMutableArray *sharingItems = [NSMutableArray new];
            [sharingItems addObject:@"Spotlyte helps you find the best Happy Hour Deals and Daily Specials.\nCheck it out:https://itunes.apple.com/us/app/spotlyte-specials/id1291896238?mt=8"];
            
            UIActivityViewController *activityController = [[UIActivityViewController alloc] initWithActivityItems:sharingItems applicationActivities:nil];
            activityController.popoverPresentationController.sourceView =
            self.view;
            [self presentViewController:activityController animated:YES completion:nil];
            break;
        }
         
        case 4:{
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"https://itunes.apple.com/us/app/spotlyte-specials/id1291896238?mt=8"]];
            break;
        }
            
        case 5:{
            TermsConditionViewController *terms=[[TermsConditionViewController alloc]init];
            terms.headerName=@"Terms and Conditions";
            [self.navigationController pushViewController:terms animated:YES];
            break;
        }
            
        case 6:{
            TermsConditionViewController *terms=[[TermsConditionViewController alloc]init];
            terms.headerName=@"Privacy Policy";
            [self.navigationController pushViewController:terms animated:YES];
            break;
          
        }
        case 7:{
            [[NSUserDefaults standardUserDefaults]setBool:FALSE forKey:@"login"];
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [defaults setObject:nil forKey:@"logindetails"];
            [defaults synchronize];
            [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me/permissions" parameters:nil
                                               HTTPMethod:@"DELETE"] startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
                FBSDKLoginManager *loginManager = [[FBSDKLoginManager alloc] init];
                [loginManager logOut];
                
                [FBSDKAccessToken setCurrentAccessToken:nil];
                // ...
            }];
            NSString *twitterID = [[NSUserDefaults standardUserDefaults]valueForKey:@"twitteruserid"];
            [[[Twitter sharedInstance] sessionStore] logOutUserID:twitterID];
            
            [[GIDSignIn sharedInstance] signOut];
            
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            LoginViewController *rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"loginvc"];
            [[UIApplication sharedApplication].keyWindow setRootViewController:rootViewController];
            
            break;
        }

        default:
            break;
    }
}


-(IBAction)pressEditProfileBtn:(id)sender
{
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
