//
//  spotlytePromotionViewController.h
//  Spotlyte
//
//  Created by CIPL227-MOBILITY on 14/03/16.
//  Copyright © 2016 Naveen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface spotlytePromotionTblViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView    *imgViewHotelIcon;
@property (weak, nonatomic) IBOutlet UILabel        * lblHotelName;
@end

@interface spotlytePromotionViewController : UIViewController<UITextFieldDelegate,EDStarRatingProtocol,UICollectionViewDelegate,UICollectionViewDataSource,CLLocationManagerDelegate>
{
    IBOutlet UIView     *view_noPromotions;
     EDStarRating    *starRatingView;
    IBOutlet UICollectionView *collectionVw;
    IBOutlet UIButton *btnSpotlyteDeal;
    IBOutlet UIButton *btnFeatureDeal;
     CLLocationManager *_locationManager;
    IBOutlet UILabel    *lbl_NoComments;
}

//popup

@property (strong, nonatomic) IBOutlet UIView *popupViewBG;

@property (strong, nonatomic) IBOutlet UIView *popupView;
@property (strong, nonatomic) IBOutlet UILabel *lblpopupName;
@property (strong, nonatomic) IBOutlet UIButton *btnHppyHours;
@property (strong, nonatomic) IBOutlet UIButton *lblDailySpecial;
@property (strong, nonatomic) NSMutableArray        *placeArray;

@property (strong, nonatomic) IBOutlet UITableView *tblPopup;
@property (strong, nonatomic) IBOutlet UIButton *btnCross;

@property (strong, nonatomic) IBOutlet UIButton *btnHappyHours;
@property (strong, nonatomic) IBOutlet UIButton *btnDailySpecials;
@property (strong, nonatomic) IBOutlet UIView *emptyView;
@property (strong, nonatomic) IBOutlet UIButton *btnReportHere;
@property (strong, nonatomic) IBOutlet UIButton *btnExpand;

@property (weak, nonatomic) IBOutlet HMSegmentedControl *viewTool;
//
@property (strong, nonatomic) NSMutableArray        *arrayNonFavHotel;
@property (strong, nonatomic) NSArray               *hotelNameArray;
@property (strong, nonatomic) NSArray               *hotelImageArray;
@property (strong, nonatomic) NSArray        *spotlyteArray;
@property (strong, nonatomic) NSArray        *arrayPlaces;

@property (strong, nonatomic) NSString        *selectedController;

@end
