//
//  NewHomeViewController.m
//  Spotlyte
//
//  Created by Admin on 17/08/16.
//  Copyright © 2016 Naveen. All rights reserved.
//

#import "NewHomeViewController.h"
#import "HomeViewController.h"
#import "ProfileViewController.h"
#import "customSearchViewController.h"
#import "JTSScrollIndicator.h"
//#import "TabBarController.h"
#import "LoginViewController.h"
#import <TwitterKit/TwitterKit.h>
#import "UIAlertView+MKBlockAdditions.h"

@interface NewHomeViewController (){
    BOOL revealEnable;
    NSMutableArray *tempArray;
    BOOL isCheckedCityStatus;
    NSUserDefaults *defaults;
    int completeProcessCount;
    CGRect oldFrame;
    CLLocationManager *locationManager;
    HomeViewController *homeCon;
}

@property (strong, nonatomic) JTSScrollIndicator *indicator;
@end

@implementation NewHomeViewController
@synthesize tblView_Category;

///-------------------------
#pragma mark IntialMethods
///-------------------------

- (void)viewDidLoad {
    [super viewDidLoad];
    homeCon=[self.storyboard instantiateViewControllerWithIdentifier:@"homescreen"];

    tempArray = [NSMutableArray new];
    completeProcessCount=0;
    _scrollVw.scrollEnabled=NO;
     defaults = [NSUserDefaults standardUserDefaults];
    [self designOfTableView];
    [self registerKeyBoardShowHideNotification];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveRevealNotification:)
                                                 name:@"revealNotification"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(MenuButtoneEnable:)
                                                 name:@"MenuButtonEnable"
                                               object:nil];
    
    
    [self CurrentLocationIdentifier];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}


- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if (!_indicator) {
        self.indicator = [[JTSScrollIndicator alloc] initWithScrollView:tblView_Category];
        self.indicator.backgroundColor = [UIColor lightGrayColor];
        tblView_Category.contentOffset = CGPointMake(0.0, 1.0);
        [_indicator show:YES];
    }
    
    [self addCityArray];
    [self viewScrollDown];
}

-(void)CurrentLocationIdentifier
{
    //---- For getting current gps location
    locationManager = [CLLocationManager new];
    locationManager.delegate = self;
    [locationManager requestAlwaysAuthorization];
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [locationManager startUpdatingLocation];
    //------
}

-(void)MenuButtoneEnable:(NSNotification*)notification{
    _menuBtn.hidden=NO;
}
- (void)receiveRevealNotification:(NSNotification *) notification
{  
    NSDictionary *userInfo = notification.userInfo;
    int index = [[userInfo objectForKey:@"revealIndex"] intValue];
    switch (index) {
        case 0:{
            [self.revealViewController rightRevealToggleAnimated:YES];
            [self addCityArray];
        }
            break;
        case 1:
            [self.tabBarController setSelectedIndex:1];
            [appDelegate.objCustomTabBar selectTab:1];
            break;
        case 2:
            [self.tabBarController setSelectedIndex:4];
            [appDelegate.objCustomTabBar selectTab:4];
            break;
        case 3:
            [self.tabBarController setSelectedIndex:3];
            [appDelegate.objCustomTabBar selectTab:3];
            break;
        case 4:
            [self.tabBarController setSelectedIndex:2];
            [appDelegate.objCustomTabBar selectTab:2];
            break;
        case 5:
        {
            customSearchViewController *customVC = [self.storyboard instantiateViewControllerWithIdentifier:@"advancedsearch"];
            [self.navigationController pushViewController:customVC animated:YES];
        }
            break;
        case 6:
        {
            ReportsViewController *reportsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"reportsvc"];
            [self.navigationController pushViewController:reportsVC animated:YES];
        }
            break;
        case 7:
        {
            SettingsViewController *settingsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"settings"];
            [self.navigationController pushViewController:settingsVC animated:YES];
        }
            break;
        case 8:
        {
            [UIAlertView alertViewWithTitle:@"" message:@"Are you sure you want to logout?" cancelButtonTitle:@"Cancel" otherButtonTitles:@[@"Ok"] onDismiss:^(int buttonIndex) {
                if (buttonIndex == 0) {
                    [[NSUserDefaults standardUserDefaults]setBool:FALSE forKey:@"login"];
                    [defaults setObject:nil forKey:@"logindetails"];
                    [defaults synchronize];
                    [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me/permissions" parameters:nil
                                                       HTTPMethod:@"DELETE"] startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
                        FBSDKLoginManager *loginManager = [[FBSDKLoginManager alloc] init];
                        [loginManager logOut];
                        
                        [FBSDKAccessToken setCurrentAccessToken:nil];
                        // ...
                    }];
                    NSString *twitterID = [[NSUserDefaults standardUserDefaults]valueForKey:@"twitteruserid"];
                    [[[Twitter sharedInstance] sessionStore] logOutUserID:twitterID];
                    
                    [[GIDSignIn sharedInstance] signOut];
                    
                    
                    LoginViewController *loginCon= [self.storyboard instantiateViewControllerWithIdentifier:@"loginvc"];
                    UINavigationController *navigationController =
                    [[UINavigationController alloc] initWithRootViewController:loginCon];
                    navigationController.navigationBarHidden=YES;
                    [self.navigationController presentViewController:navigationController animated:YES completion:nil];
                }
            } onCancel:^() {
            }];
            
        }
            break;
            
        default:
            break;
    }
}

-(void)addCityArray{
    
    BOOL check=[[AppDelegate shareAppdelegateInstance] checkReachability];
    
    if (check) {
        if (APP_DELEGATE.cityArray.count==0) {
            [CUSTOMINDICATORMACRO startLoadAnimation:self];
            [self getCityListServiceHelper];
        }
    }
}

- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
}


///<---------------------------------------
#pragma mark --- City Service Helper
///<---------------------------------------
-(void)getCityListServiceHelper{
    BOOL check=[[AppDelegate shareAppdelegateInstance] checkReachability];
    if(check){
        [REGISTERMACRO listallCityServer:nil withCompletion:^(id obj1) {
        
            dispatch_async(dispatch_get_main_queue(), ^{
              if (obj1!=nil) {
                if([obj1 isKindOfClass:[NSArray class]] || [obj1 isKindOfClass:[NSMutableArray class]]){
                        APP_DELEGATE.cityArray = obj1;
                        [self getCategoryListServiceHelper];
                       // [self completeTask];
                        NSLog(@"cityList");
                    }
                    else
                        [self showCheckYourConnection];
                }
              else
                  [self showCheckYourConnection];
            });
        }];
    }
}


-(void)getCategoryListServiceHelper
{
    BOOL check=[[AppDelegate shareAppdelegateInstance] checkReachability];
    if(check)
    {
        [REGISTERMACRO listallCategoryServer:nil withCompletion:^(id obj1) {
          if (obj1!=nil) {
                if([obj1 isKindOfClass:[NSArray class]] || [obj1 isKindOfClass:[NSMutableArray class]] || [obj1 isKindOfClass:[NSDictionary class]]){
                     APP_DELEGATE.categoryarray=obj1;
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [CUSTOMINDICATORMACRO stopLoadAnimation:self];
                                           });
                    //[self completeTask];
                      NSLog(@"categoryList");
                    }
                 else
                    [self showCheckYourConnection];
            }else
                [self showCheckYourConnection];
            
        }];
    }
    
}


-(void)completeTask{
    completeProcessCount=completeProcessCount+1;
    if (completeProcessCount==2) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [CUSTOMINDICATORMACRO stopLoadAnimation:self];
        });
    }
}


///<---------------------------------------
#pragma mark --- TextField Delegate Method
///<---------------------------------------

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    BOOL check=[self checkLocation];
    if (check) {
       [self viewScrollUp];
    }else{
        [textField resignFirstResponder];
    }
}


-(BOOL)textFieldShouldClear:(UITextField *)textField{
        textField.text = @"";
        [textField resignFirstResponder];
        tblView_Category.hidden = YES;
        return NO;
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    BOOL check=[self checkLocation];
    if (check) {
    
    if (textField.text.length!=0) {
      //  HomeViewController *homeCon=[self.storyboard instantiateViewControllerWithIdentifier:@"homescreen"];
//        homeCon.isCheckedCityStatus=YES;
//        homeCon.cityArray=APP_DELEGATE.cityArray;
//        homeCon.categoryarray=[APP_DELEGATE.categoryarray copy];
//        [homeCon pressSearchBtn:textField.text];
//        [self.navigationController pushViewController:homeCon animated:YES];
        _txtField.text=@"";
    }
    [_txtField endEditing:YES];
    [textField resignFirstResponder];
    }
    
    return YES;
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(_txtField.text.length == 1 && [string isEqualToString:@""]){
        tblView_Category.hidden = YES;
        _txtField.text = @"";
        [textField resignFirstResponder];
    }else{
        tblView_Category.hidden = NO;
        NSString *substring = [NSString stringWithString:textField.text];
        substring = [substring stringByReplacingCharactersInRange:range withString:string];
        
        [self searchByEvents:substring];
    }
    return YES;
}

- (void)searchByEvents:(NSString *)searchStr{
    NSArray *TempNSArray = [[NSArray alloc]init];
    [tempArray removeAllObjects];
    if([searchStr length] > 0){
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"placeType beginswith[c] %@",  searchStr];
        TempNSArray = [APP_DELEGATE.cityArray filteredArrayUsingPredicate:predicate];
        if (TempNSArray.count>0)
        {
            [tempArray addObjectsFromArray:TempNSArray];
        }
        TempNSArray = [APP_DELEGATE.categoryarray filteredArrayUsingPredicate:predicate];
        
        if (TempNSArray.count>0)
        {
            [tempArray addObjectsFromArray:TempNSArray];
        }
    }
    else
        [tempArray removeAllObjects];
    
    if(tempArray.count>0){
        tblView_Category.hidden = NO;
        [tblView_Category reloadData];}
    else
       tblView_Category.hidden = YES;
    
    [self scrollViewDidScroll:tblView_Category];
}


///<-----------------------------
#pragma mark Table View Methods
///<-----------------------------

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return  tempArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
        static NSString *cellIdentifier = @"identifier_new";
        UITableViewCell *cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
        if(cell == nil)
        {
            cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        }
        if(tempArray.count>0)
        {
            RegisterModal *modal = [tempArray objectAtIndex:indexPath.row];
            cell.textLabel.text = modal.placeType;
            
            cell.detailTextLabel.numberOfLines = 1;
            if ([modal.CityorArea isEqualToString:@"City"] || [modal.CityorArea isEqualToString:@"Area"])
            {
                // cell.detailTextLabel.text= [NSString stringWithFormat:@"%@",modal.address1];
            }else{
                cell.detailTextLabel.text= [NSString stringWithFormat:@"%@, %@, IL",modal.address1,modal.city];
            }
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    BOOL check=[self checkLocation];
    if (check)
    {
        
//        dispatch_async(dispatch_get_main_queue(), ^{
//            [appDelegate.objCustomTabBar selectTab:1];
//        });
        
            tblView_Category.hidden = YES;
            RegisterModal *modal = [tempArray objectAtIndex:indexPath.row];
            NSRange range=[modal.placeType rangeOfString:@", IL" options:(NSCaseInsensitiveSearch)];
            NSRange range1=[modal.placeType rangeOfString:@", Chicago" options:(NSCaseInsensitiveSearch)];
        
         //   HomeViewController *homeCon=[self.storyboard instantiateViewControllerWithIdentifier:@"homescreen"];

            if(range.location != NSNotFound || range1.location != NSNotFound)
            {
                homeCon.isCheckedCityStatus = TRUE;
            }
            else if([modal.statusCity isEqualToString:@"City"])
            {
                homeCon.isCheckedCityStatus = TRUE;
            }
            else
            {
                homeCon.isCheckedCityStatus = FALSE;
            }
            _txtField.text = modal.placeType;
        
            //homeCon.isCheckedCityStatus=YES;
            homeCon.cityArray=[APP_DELEGATE.cityArray copy];
            homeCon.categoryarray=[APP_DELEGATE.categoryarray copy];
            
        //Hiren
            APP_DELEGATE.Modal_PlaceType = modal.placeType;
        APP_DELEGATE.Modal_CityOrArea = modal.CityorArea;
            //[homeCon pressSearchBtn:modal.placeType];
            [appDelegate.objCustomTabBar showTabSelect:1];
        
             APP_DELEGATE.IsFromNewHome = YES;
        [APP_DELEGATE.objCustomTabBar selectTab:1];
        
           // [self.navigationController pushViewController:homeCon animated:YES];
        
           //
            _txtField.text=@"";
            [_txtField endEditing:YES];
        }
}


///<------------------------------
#pragma mark Scroll View Methods
///<------------------------------
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [self.indicator scrollViewDidScroll:tblView_Category];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    [self.indicator scrollViewDidEndDragging:tblView_Category willDecelerate:decelerate];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    [self.indicator scrollViewDidEndDecelerating:tblView_Category];
}

- (void)scrollViewWillScrollToTop:(UIScrollView *)scrollView {
    [self.indicator scrollViewWillScrollToTop:tblView_Category];
}

- (void)scrollViewDidScrollToTop:(UIScrollView *)scrollView {
    [self.indicator scrollViewDidScrollToTop:tblView_Category];
}

///<-----------------------
#pragma mark button Methods
///<-----------------------
- (IBAction)tapOnView:(id)sender {
    [_txtField resignFirstResponder];
    [self.revealViewController rightRevealToggleAnimated:YES];
}

- (IBAction)BtnSearchCurrentLocation:(id)sender {
    
    BOOL check=[self checkLocation];
    if (check) {
        HomeViewController *homeCon=[self.storyboard instantiateViewControllerWithIdentifier:@"homescreen"];
        homeCon.cityArray=APP_DELEGATE.cityArray;
        homeCon.categoryarray=[APP_DELEGATE.categoryarray copy];
        //homeCon.firstTimeLoad=YES;
        homeCon.currentLocationEnable = true;
        [homeCon pressCurrentLocation:nil];
        [self.navigationController pushViewController:homeCon animated:YES];
        [appDelegate.objCustomTabBar showTabSelect:1];
    }
}

- (IBAction)btnAdvancedSearchFilter:(id)sender {
    customSearchViewController *customVC = [self.storyboard instantiateViewControllerWithIdentifier:@"advancedsearch"];
    customVC.responseArray=APP_DELEGATE.cityArray;
    [self.navigationController pushViewController:customVC animated:YES];
}


- (IBAction)tapOnMenuBtn:(id)sender {
    _menuBtn.hidden=YES;
     [self.revealViewController rightRevealToggleAnimated:YES];
}
///<-----------------------------------
#pragma mark KeyBoard show/hide Methods
///<-----------------------------------
- (void)keyboardDidShow: (NSNotification *) notif{
    [self viewScrollUp];
}

- (void)keyboardDidHide: (NSNotification *) notif{
    [self viewScrollDown];
}


-(void)viewScrollUp{
    _lblFind.hidden=YES;
    _lblNever.hidden=YES;
    _textField_Y.constant = 40;
}


-(void)viewScrollDown{
      tblView_Category.hidden = YES;
     _lblFind.hidden=NO;
     _lblNever.hidden=NO;
     _textField_Y.constant = 79;
}

///<------------------------
#pragma mark Other Methods
///<------------------------
-(void)designOfTableView{
    tblView_Category.layer.borderWidth = 1;
    tblView_Category.layer.cornerRadius = 5;
    tblView_Category.clipsToBounds = YES;
    tblView_Category.layer.borderColor = [[UIColor colorWithRed:234.0/255.0 green:234.0/255.0 blue:234.0/255.0 alpha:1.0]CGColor];
    
    tblView_Category.allowsSelectionDuringEditing=YES;
    tblView_Category.tableFooterView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, tblView_Category.frame.size.width, 0.1f)];
    tblView_Category.hidden = YES;
}


-(void)registerKeyBoardShowHideNotification{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidShow:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidHide:)
                                                 name:UIKeyboardDidHideNotification
                                               object:nil];
}


-(NSMutableAttributedString*)boldtextOfString:(NSString*)string withRange:(NSRange)range{
    NSMutableAttributedString *strText = [[NSMutableAttributedString alloc] initWithString:string];
    [strText addAttribute:NSFontAttributeName
                    value:Calibri(22.0f)
                    range:range];
    return strText;
}


-(void)showCheckYourConnection{
    dispatch_async(dispatch_get_main_queue(), ^{
        [CUSTOMINDICATORMACRO stopLoadAnimation:self];
    });
    
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                    message:@"Please check your connection"
                                                   delegate:self
                                          cancelButtonTitle:@"Ok"
                                          otherButtonTitles:nil];
    [alert show];
}


- (void) dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];

}

-(BOOL)checkLocation{

    BOOL check;
    if ([CLLocationManager locationServicesEnabled]){
        
        if ([CLLocationManager authorizationStatus]==kCLAuthorizationStatusDenied){
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"App Permission Denied"
                                                            message:@"To re-enable, please go to Settings and turn on Location Service for this app."
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
             check=NO;
        }
        else
            check=YES;
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Enable Location Service"
                                                        message:@"Please go to settings and Enable Location Service."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        check=NO;
    }
    return check;
}



@end
