//
//  customSearchViewController.m
//  Spotlyte
//
//  Created by CIPL227-MOBILITY on 16/03/16.
//  Copyright © 2016 Naveen. All rights reserved.
//

#import "customSearchViewController.h"
#import "SWRevealViewController.h"
#import "RestListViewController.h"
#import "ProfileViewController.h"
#import "MyProfileViewController.h"
#import "spotlytePromotionViewController.h"
#import "InnerCell.h"
#import "CustomSearchCell.h"
#import "HeaderView.h"

@interface customSearchViewController ()
{
    NSString *searchString_text;
    NSString *strStatus;
}

@property (weak, nonatomic) IBOutlet UIButton *MenuTapBtn_lbl;
- (IBAction)MenuTapBtn:(id)sender;

@end

@implementation customSearchViewController

@synthesize customSearchBar, tblViewSearchOption;
@synthesize arrayLstSearch,inSearchTitle,CustomSearchView;

- (void)viewDidLoad {
    [super viewDidLoad];
    _tbl_SearchItems.delegate = self;
    _tbl_SearchItems.dataSource = self;
    _ArrItems = [[NSArray alloc]initWithObjects:@"Beer",@"Pepsi",@"Coke",@"Spite",@"Asdf",@"XYZ", nil];
    HeaderView *header=[[HeaderView alloc]initWithTitle:@"Advanced Search Filter" controller:self];
    [header addSubview:header.btnback];
    [self.view addSubview:header];

     mileString = @"";
    happyString = @"";
     timeString = @"";
     cityString = @"";
     statusCity = @"";

    _tbl_SearchItems.layer.borderWidth = 1;
    _tbl_SearchItems.layer.cornerRadius = 5;
    _tbl_SearchItems.clipsToBounds = YES;
   // _tbl_SearchItems.layer.borderColor = [[UIColor colorWithRed:234.0/255.0 green:234.0/255.0 blue:234.0/255.0 alpha:1.0]CGColor];
    _tbl_SearchItems.layer.borderColor = [[UIColor darkGrayColor]CGColor] ;
    _tbl_SearchItems.allowsSelectionDuringEditing=YES;
    _tbl_SearchItems.tableFooterView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, _tbl_SearchItems.frame.size.width, 0.1f)];
    _tbl_SearchItems.hidden = YES;
    
    _responseArray=APP_DELEGATE.cityArray;
    
    searchBar_City.hidden = YES;
    [[UITextField appearanceWhenContainedIn:[UISearchBar class], nil] setDefaultTextAttributes:
     @{NSFontAttributeName: [UIFont fontWithName:@"Helvetica" size:14],}];
    
    
    restModalObj = [[ResturantModal alloc]init];
    tempArray = [[NSMutableArray alloc]init];
    [self loadDictionaryValues];
    [self changeIconPostionOfSearchBar];

    
    
   
    tblViewSearchOption.allowsSelectionDuringEditing=YES;
    tblViewSearchOption.tableFooterView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, tblViewSearchOption.frame.size.width, 0.1f)];
    self.tblViewInnerSearchOption.allowsSelectionDuringEditing=YES;
    self.tblViewInnerSearchOption.tableFooterView = [[UIView alloc]initWithFrame:CGRectMake(0, 0,  self.tblViewInnerSearchOption.frame.size.width, 0.1f)];
    [self getCityListServiceHelper];
}
-(void)viewDidAppear:(BOOL)animated
{
    if (!_indicator) {
        self.indicator = [[JTSScrollIndicator alloc] initWithScrollView:_tbl_SearchItems];
        self.indicator.backgroundColor = [UIColor lightGrayColor];
        _tbl_SearchItems.contentOffset = CGPointMake(0.0, 1.0);
        [_indicator show:YES];
    }
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    _tbl_SearchItems.hidden = YES;
    _MenuTapBtn_lbl.hidden=YES;
    self.ActID.hidden = YES;
    if (CustomSearchView.text.length==0)
    {
        int width=self.view.frame.size.width;
        NSLog(@"width %d",width);
        if (width == 320)
        {
            CustomSearchView.font = [UIFont systemFontOfSize:10.2];
            _SearbarStar_lbl.font = [UIFont italicSystemFontOfSize:10];
        }
        else if (width == 414)
        {
            
            CustomSearchView.font = [UIFont systemFontOfSize:14.7];
            
            _SearbarStar_lbl.font = [UIFont italicSystemFontOfSize:13.7];
        }
        else
        {
            CustomSearchView.font = [UIFont systemFontOfSize:12.5];
            _SearbarStar_lbl.font = [UIFont italicSystemFontOfSize:12];
            
        }
        
        
        
        [CustomSearchView setText:@"Beer, Wine, Martini, Jameson, Tito's, Bloody...."];
        
        [CustomSearchView setTextColor:[UIColor lightGrayColor]];
        
        
        
    }
    
    appendTotalString = [[NSMutableString alloc]init];
    arrayAppend = [[NSMutableArray alloc]init];
    categoryString = @"";
}

- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
}


#pragma mark --- City Service Helper
-(void)GetItemList:(NSString*) str
{
    NSDictionary *dict = @{@"search":str};
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    //retrieveSpecialsHappyHours.json
    //https://spotlytespecials.com/beta/webservice/api/mobile1/userFavoritesList.json
    //[CUSTOMINDICATORMACRO startLoadAnimation:self];
    self.ActID.hidden = NO;
    self.cnstActID_Width.constant = 20;
    [ServiceHelper sendRequestToServerWithParameters:@"searchByItem.json" forhttpMethod:@"POST" withrequestBody:jsonString sync:YES completion:^(id obj) {
        if ([obj isKindOfClass:[NSDictionary class]]){
            self.ActID.hidden = YES;
            //[CUSTOMINDICATORMACRO stopLoadAnimation:self];
            ResturantModal *modal = [[ResturantModal alloc]init];
            [modal setResult:[obj valueForKey:@"result"]];
            [modal setMessage:[obj valueForKey:@"message"]];
            //daily_special
            //id data = [obj valueForKey:@"data"];
            [tempArray removeAllObjects];
            tempArray = [obj valueForKey:@"data"];
            dispatch_async(dispatch_get_main_queue(), ^{
                NSLog(@"Im on the main thread");
                self.ActID.hidden = YES;
                [self.ActID setHidden:YES];
                self.cnstActID_Width.constant = 0;
            });

            if(tempArray.count>0)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    NSLog(@"Im on the main thread");
                    _tbl_SearchItems.hidden = NO;
                    self.ActID.hidden = YES;
                    [self.ActID setHidden:YES];
                     self.cnstActID_Width.constant = 0;
                    //[self.ActID stopAnimating];
                    [_tbl_SearchItems reloadData];
                });
            }
            else
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    NSLog(@"Im on the main thread");
                    _tbl_SearchItems.hidden = YES;
                    [self.ActID setHidden:YES];
                     self.cnstActID_Width.constant = 0;
                    self.ActID.hidden = YES;
                  //  [self.ActID stopAnimating];
                });
            }
        }
    }];
}
-(void)getCityListServiceHelper{
    
  //  NSArray *array = [_responseArray valueForKey:@"city"];
      NSArray *array = [_responseArray valueForKey:@"city"];

    NSArray *array_Dup = [[NSSet setWithArray:array] allObjects];
    _cityArray = [array_Dup sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
}

- (void)changeIconPostionOfSearchBar
{
    // set border for the search bar
    customSearchBar.layer.borderWidth = 1;
    customSearchBar.layer.cornerRadius = 5;
    customSearchBar.clipsToBounds = YES;
    customSearchBar.layer.borderColor = [[UIColor colorWithRed:234.0/255.0 green:234.0/255.0 blue:234.0/255.0 alpha:1.0]CGColor];
    
    UIView *addTblTopBorder=[[UIView alloc]initWithFrame:CGRectMake(0, tblViewSearchOption.frame.origin.y-1, [UIScreen mainScreen].bounds.size.width, 1)];
   // addTblTopBorder.backgroundColor=[UIColor colorWithWhite:0.2f alpha:0.2f];
    //[searchVw addSubview:addTblTopBorder];
    
    // set border for the tableview
    /*//tblViewSearchOption.layer.borderWidth = 1;
    tblViewSearchOption.layer.cornerRadius = 5;
    tblViewSearchOption.clipsToBounds = YES;
    tblViewSearchOption.layer.borderColor = [[UIColor colorWithRed:170.0/255.0 green:170.0/255.0 blue:170.0/255.0 alpha:0.4]CGColor];*/
    arrayLstSearch = [[NSArray alloc]initWithObjects:@"DAY",@"CITY/AREA",@"MILE RADIUS",@"PRICE",@"TIME", nil];
}

#pragma mark - TableView Delegates
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if(tableView == _tbl_SearchItems)
        return tempArray.count;
    else if (tableView == tblViewSearchOption)
        return [arrayLstSearch count];
    else
        return [arrayValueForInnerTable count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == _tbl_SearchItems)
    {
        static NSString *cellIdentifier = @"identifier";
        UITableViewCell *cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
        if(cell == nil)
        {
            cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        }
        if(tempArray.count>0 )
        {
//            NSString *str = [tempArray objectAtIndex:indexPath.row];
//            NSString *correctString = [NSString stringWithCString:[str cStringUsingEncoding:NSISOLatin1StringEncoding] encoding:NSUTF8StringEncoding];
//            NSLog(@"%@",correctString);
            cell.textLabel.text = [tempArray objectAtIndex:indexPath.row];
        }
        tableView.backgroundColor = [UIColor whiteColor];
        cell.contentView.backgroundColor = [UIColor whiteColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    
    if (tableView == tblViewSearchOption) {
        
        
        static NSString *simpleTableIdentifier = @"SimpleTableCell";
        
        CustomSearchCell *cell = (CustomSearchCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle]loadNibNamed:@"CustomSearchCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
    
        cell.lbl_Name.text = [arrayLstSearch objectAtIndex:indexPath.row];
        if(indexPath.row == 1)
        {
            cell.lbl_Select.text = cityString ;
            cell.imgView.image=[UIImage imageNamed:@"City..area"];
        }
        else if(indexPath.row == 2)
        {
            cell.lbl_Select.text = mileString ;
            cell.imgView.image=[UIImage imageNamed:@"Miles-radius"];
        }
        else if(indexPath.row == 3)
        {
            cell.lbl_Select.text = happyString ;
            cell.imgView.image=[UIImage imageNamed:@"Price"];
        }
        else if(indexPath.row == 4)
        {
            cell.lbl_Select.text =timeString;
            cell.imgView.image=[UIImage imageNamed:@"Time"];
        }
        else
        {
            cell.lbl_Select.text =dayString ;
            cell.imgView.image=[UIImage imageNamed:@"day"];
        }
        
        return cell;
    }else{
        
        static NSString *cellIdentifier = @"MyTableCell";
        InnerCell *cell = (InnerCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle]loadNibNamed:@"InnerCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        if(arrayValueForInnerTable.count > 0){
            
            if(![categoryString isEqualToString:@"city"])
            {
                cell.textLbl.text = [arrayValueForInnerTable objectAtIndex:indexPath.row];
            }
            
            [cell.btnFullScreen addTarget:self action:@selector(pressRadioButton:) forControlEvents:UIControlEventTouchUpInside];
            cell.btnFullScreen.tag = indexPath.row;
            
            [cell.btnRadio addTarget:self action:@selector(pressRadioButton:) forControlEvents:UIControlEventTouchUpInside];
            cell.btnRadio.tag = indexPath.row;
            
            if(isCheckedLoadData == FALSE)//button action reload data
            {
                if([categoryString isEqualToString:@"radius"])
                {
                    if(selectedIndexRadius == indexPath.row)
                    {
                        if(isCheckedMileRadius == TRUE)
                        {
                            [cell.btnRadio setBackgroundImage:[UIImage imageNamed:@"radio_btn_check.png"]forState:UIControlStateNormal];
                        }
                        else
                        {
                            [cell.btnRadio setBackgroundImage:[UIImage imageNamed:@"radio_btn_uncheck.png"]forState:UIControlStateNormal];
                        }
                    }
                    else
                    {
                        [cell.btnRadio setBackgroundImage:[UIImage imageNamed:@"radio_btn_uncheck.png"]forState:UIControlStateNormal];
                    }
                }
                else if([categoryString isEqualToString:@"happyhour"])
                {
                    if(selectedIndexPrice == indexPath.row)
                    {
                        if(isCheckedPrice == TRUE)
                        {
                            [cell.btnRadio setBackgroundImage:[UIImage imageNamed:@"radio_btn_check.png"]forState:UIControlStateNormal];
                        }
                        else
                        {
                            [cell.btnRadio setBackgroundImage:[UIImage imageNamed:@"radio_btn_uncheck.png"]forState:UIControlStateNormal];
                        }
                    }
                }
                else if([categoryString isEqualToString:@"city"])
                {
                    RegisterModal *modal = [arrayValueForInnerTable objectAtIndex:indexPath.row];
                    cell.textLbl.text = modal.city;
                    if([selectedCityId isEqualToString:modal.cityID])
                    {
                        selectedIndexCity = indexPath.row;
                        if(isCheckedCity == TRUE)
                        {
                            [cell.btnRadio setBackgroundImage:[UIImage imageNamed:@"radio_btn_check.png"]forState:UIControlStateNormal];
                        }
                        else
                        {
                            [cell.btnRadio setBackgroundImage:[UIImage imageNamed:@"radio_btn_uncheck.png"]forState:UIControlStateNormal];
                        }
                    }
                }
                else if([categoryString isEqualToString:@"day"])
                {
                    if(selectedIndexDay == indexPath.row)
                    {
                        if(isCheckedDay == TRUE)
                        {
                            [cell.btnRadio setBackgroundImage:[UIImage imageNamed:@"radio_btn_check.png"]forState:UIControlStateNormal];
                        }
                        else
                        {
                            [cell.btnRadio setBackgroundImage:[UIImage imageNamed:@"radio_btn_uncheck.png"]forState:UIControlStateNormal];
                        }
                    }
                }
                
                else
                {
                    if(selectedIndexTime == indexPath.row)
                    {
                        if(isCheckedTime == TRUE)
                        {
                            [cell.btnRadio setBackgroundImage:[UIImage imageNamed:@"radio_btn_check.png"]forState:UIControlStateNormal];
                        }
                        else
                        {
                            [cell.btnRadio setBackgroundImage:[UIImage imageNamed:@"radio_btn_uncheck.png"]forState:UIControlStateNormal];
                        }
                    }
                }
            }
            else
            {
                if([categoryString isEqualToString:@"radius"])
                {
                    if(restModalObj.intMile == indexPath.row)
                    {
                        if(restModalObj.isCheckedModalMile == TRUE)
                        {
                            [cell.btnRadio setBackgroundImage:[UIImage imageNamed:@"radio_btn_check.png"]forState:UIControlStateNormal];
                        }
                        else
                        {
                            [cell.btnRadio setBackgroundImage:[UIImage imageNamed:@"radio_btn_uncheck.png"]forState:UIControlStateNormal];
                        }
                    }
                    else
                    {
                        [cell.btnRadio setBackgroundImage:[UIImage imageNamed:@"radio_btn_uncheck.png"]forState:UIControlStateNormal];
                    }
                }
                else if([categoryString isEqualToString:@"happyhour"])
                {
                    if(restModalObj.intPrice == indexPath.row)
                    {
                        if(restModalObj.isCheckedModalPrice == TRUE)
                        {
                            [cell.btnRadio setBackgroundImage:[UIImage imageNamed:@"radio_btn_check.png"]forState:UIControlStateNormal];
                        }
                        else
                        {
                            [cell.btnRadio setBackgroundImage:[UIImage imageNamed:@"radio_btn_uncheck.png"]forState:UIControlStateNormal];
                        }
                    }
                }
                else if([categoryString isEqualToString:@"day"])
                {
                    if(restModalObj.intDay == indexPath.row)
                    {
                        if(restModalObj.isCheckedModalDay == TRUE)
                        {
                            [cell.btnRadio setBackgroundImage:[UIImage imageNamed:@"radio_btn_check.png"]forState:UIControlStateNormal];
                        }
                        else
                        {
                            [cell.btnRadio setBackgroundImage:[UIImage imageNamed:@"radio_btn_uncheck.png"]forState:UIControlStateNormal];
                        }
                    }
                    
                }
                else if([categoryString isEqualToString:@"city"])
                {
                    RegisterModal *modal = [arrayValueForInnerTable objectAtIndex:indexPath.row];
                    cell.textLbl.text = modal.city;
                    if([selectedCityId isEqualToString:modal.cityID])
                    {
                        selectedIndexCity = indexPath.row;
                        if(restModalObj.isCheckedModalCity == TRUE)
                        {
                            [cell.btnRadio setBackgroundImage:[UIImage imageNamed:@"radio_btn_check.png"]forState:UIControlStateNormal];
                        }
                        else
                        {
                            [cell.btnRadio setBackgroundImage:[UIImage imageNamed:@"radio_btn_uncheck.png"]forState:UIControlStateNormal];
                        }
                    }
                }
                else
                {
                    if(restModalObj.intTime == indexPath.row)
                    {
                        if(restModalObj.isCheckedModalTime == TRUE)
                        {
                            [cell.btnRadio setBackgroundImage:[UIImage imageNamed:@"radio_btn_check.png"]forState:UIControlStateNormal];
                        }
                        else
                        {
                            [cell.btnRadio setBackgroundImage:[UIImage imageNamed:@"radio_btn_uncheck.png"]forState:UIControlStateNormal];
                        }
                    }
                }
            }
            
            
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        
        return cell;
        
        
    }
    

}




- (UIImage*)imageWithImage:(UIImage*)image scaledToSize:(CGSize)newSize;
{
    UIGraphicsBeginImageContext( newSize );
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == _tbl_SearchItems)
    {
        if (tempArray.count > 0)
        {
            NSString *  newString = [[tempArray objectAtIndex:indexPath.row] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            CustomSearchView.text = newString ; //[tempArray objectAtIndex:indexPath.row];
            NSLog(@"%@",newString);
            // [self pressSearch:nil];
            _tbl_SearchItems.hidden = YES;
            return;
        }
    }
    [customSearchBar resignFirstResponder];
    [searchBar_City resignFirstResponder];
    searchBar_City.hidden = YES;
    if(tableView == tblViewSearchOption)
    {
        if (_innerContentView == nil) {
            _innerContentView = [[[NSBundle mainBundle] loadNibNamed:@"InnerCustomSearchView" owner:self options:nil] firstObject];
            _innerContentView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
            [self.view addSubview:_innerContentView];
            [tblViewSearchOption deselectRowAtIndexPath:indexPath animated:YES];
        }
        
        CustomSearchCell *selectedCell=[tableView cellForRowAtIndexPath:indexPath];
        inSearchTitle.text = selectedCell.lbl_Name.text.uppercaseString;;
        if(indexPath.row == 1)
        {
            searchBar_City.hidden = NO;
            categoryString = @"city";
            arrayValueForInnerTable = _responseArray;
            [_tblViewInnerSearchOption reloadData];
            
            if(isCheckedCity == TRUE)
            {
                [_tblViewInnerSearchOption scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:scrollIndex inSection:0] atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
            }

            
        }else if(indexPath.row == 2)
        {
            
            
            categoryString = @"radius";
            arrayValueForInnerTable = [[NSArray alloc]initWithObjects:@"2 MILE",@"4 MILES",@"6 MILES",@"8 MILES",@"25 MILES",nil];
            [_tblViewInnerSearchOption reloadData];
            
            if(isCheckedMileRadius == TRUE)
            {
                [_tblViewInnerSearchOption scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:selectedIndexRadius inSection:0] atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
            }

            
                  }
        else if(indexPath.row == 3)
        {
            
            categoryString = @"happyhour";
            //arrayValueForInnerTable = [[NSArray alloc]initWithObjects:@"$0.01 - $0.99",@"$1",@"$2",@"$3",@"$4",@"$5",@"$1 - $5",@"$1 - $10",@"$11 - $20",@"$21+",@"1/2 OFF",@"FREE",@"OTHERS",nil];
            arrayValueForInnerTable = [[NSArray alloc]initWithObjects:@"$0 - $0.99",@"$1 - $5",@"$1 - $10",@"$1 - $20",@"$20+", nil];
            [_tblViewInnerSearchOption reloadData];
            
            if(isCheckedPrice == TRUE)
            {
                [_tblViewInnerSearchOption scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:selectedIndexPrice inSection:0] atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
            }
            return;

         }
        else if(indexPath.row == 4)
        {
            categoryString = @"time";
            NSMutableArray *array = [[NSMutableArray alloc]init];
            for(int i = 1;i<=12; i++)
            {
                NSString *str = [NSString stringWithFormat:@"%d AM",i];
                [array addObject:str];
            }
            for(int i = 1;i<=12; i++)
            {
                NSString *str = [NSString stringWithFormat:@"%d PM",i];
                [array addObject:str];
            }
            arrayValueForInnerTable = array;
            [_tblViewInnerSearchOption reloadData];
            if(isCheckedTime == TRUE)
            {
                [_tblViewInnerSearchOption scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:scrollIndexHappyHours inSection:0] atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
            }
        }
        else
        {
            categoryString = @"day";
            arrayValueForInnerTable = [[NSArray alloc]initWithObjects:@"View All",@"Sunday",@"Monday",@"Tuesday",@"Wednesday",@"Thursday",@"Friday",@"Saturday",nil];
            [_tblViewInnerSearchOption reloadData];
            
            if(isCheckedDay == TRUE)
            {
                [_tblViewInnerSearchOption scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:selectedIndexDay inSection:0] atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
            }
         }
    }
    else
    {
        UITableViewCell *selectedCell=[tableView cellForRowAtIndexPath:indexPath];
        _searchParameterStr = selectedCell.textLabel.text;
        
        [_tblViewInnerSearchOption reloadData];
    }
    
}

-(BOOL)firstimage:(UIImage *)image1 isEqualTo:(UIImage *)image2 {
    NSData *data1 = UIImagePNGRepresentation(image1);
    NSData *data2 = UIImagePNGRepresentation(image2);
    
    return [data1 isEqualToData:data2];
}

-(void)pressRadioButton : (id)sender
{
    UIButton *button = (UIButton *)sender;
    [searchBar_City resignFirstResponder];
    isCheckedLoadData = FALSE;
    if([categoryString isEqualToString:@"radius"])
    {
        if(isCheckedMileRadius == TRUE)
        {
            if( selectedIndexRadius == button.tag)
                isCheckedMileRadius = FALSE;
            else
                isCheckedMileRadius = TRUE;
        }
        else
        {
            isCheckedMileRadius = TRUE;
        }
        selectedIndexRadius = button.tag;
    }
    else if([categoryString isEqualToString:@"happyhour"])
    {
        if(isCheckedPrice == TRUE)
        {
            if( selectedIndexPrice == button.tag)
                isCheckedPrice = FALSE;
            else
                isCheckedPrice = TRUE;
        }
        else
        {
            isCheckedPrice = TRUE;
        }
        selectedIndexPrice = button.tag;
    }
    else if([categoryString isEqualToString:@"day"])
    {
        if(isCheckedDay == TRUE)
        {
            if( selectedIndexDay == button.tag)
                isCheckedDay = FALSE;
            else
                isCheckedDay = TRUE;
        }
        else
        {
            isCheckedDay = TRUE;
        }
        selectedIndexDay = button.tag;
    }
    else if([categoryString isEqualToString:@"city"])
    {
        _innerContentView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
        
        RegisterModal *modal = [arrayValueForInnerTable objectAtIndex:button.tag];
        selectedCityId = modal.cityID;
        if(isCheckedCity == TRUE)
        {
            if( selectedIndexCity == button.tag)
                isCheckedCity = FALSE;
            else
                isCheckedCity = TRUE;
        }
        else
        {
            isCheckedCity = TRUE;
        }
        selectedIndexCity = button.tag;
    }
    
    else
    {
        if(isCheckedTime == TRUE)
        {
            if( selectedIndexTime == button.tag)
                isCheckedTime = FALSE;
            else
                isCheckedTime = TRUE;
        }
        else
        {
            isCheckedTime = TRUE;
        }
        selectedIndexTime = button.tag;
    }
    
    [_tblViewInnerSearchOption reloadData];
}


-(void)loadDictionaryValues
{
    finalDictionary = [[NSMutableDictionary alloc]init];
    
    //Food and Drinks Catgory
    [finalDictionary setObject:@"food" forKey:@"Food"];
    [finalDictionary setObject:@"beer_and_wine" forKey:@"Drink"];
    [finalDictionary setObject:@"both" forKey:@"Both"];
    
    //Type of Food
    [finalDictionary setObject:@"tradamerican" forKey:@"American"];
    [finalDictionary setObject:@"argentine" forKey:@"Argentinian"];
    [finalDictionary setObject:@"asianfusion" forKey:@"Asian"];
    [finalDictionary setObject:@"austrian" forKey:@"Austrian"];
    [finalDictionary setObject:@"chinese" forKey:@"Chinese"];
    [finalDictionary setObject:@"cuban" forKey:@"Cuban"];
    [finalDictionary setObject:@"ethiopian" forKey:@"Ethiopian"];
    [finalDictionary setObject:@"modern_european" forKey:@"European"];
    [finalDictionary setObject:@"filipino" forKey:@"Filipino"];
    [finalDictionary setObject:@"french" forKey:@"French"];
    [finalDictionary setObject:@"german" forKey:@"German"];
    [finalDictionary setObject:@"greek" forKey:@"Greek"];
    [finalDictionary setObject:@"hawaiian" forKey:@"Hawaiian"];
    [finalDictionary setObject:@"indpak" forKey:@"Indian"];
    [finalDictionary setObject:@"italian" forKey:@"Italian"];
    [finalDictionary setObject:@"japanese" forKey:@"Japanese(Sushi)"];
    [finalDictionary setObject:@"korean" forKey:@"Korean"];
    [finalDictionary setObject:@"mediterranean" forKey:@"Mediterranean"];
    [finalDictionary setObject:@"mexican" forKey:@"Mexican"];
    [finalDictionary setObject:@"polish" forKey:@"Polish"];
    [finalDictionary setObject:@"russian" forKey:@"Russian"];
    [finalDictionary setObject:@"southern" forKey:@"Southern"];
    [finalDictionary setObject:@"spanish" forKey:@"Spanish(Tapas)"];
    [finalDictionary setObject:@"thai" forKey:@"Thai"];
    [finalDictionary setObject:@"vietnamese" forKey:@"Vietnamese"];
    
    //Entertainment
    [finalDictionary setObject:@"triviahosts" forKey:@"Trivia"];
    [finalDictionary setObject:@"karaoke" forKey:@"Karaoke"];
    [finalDictionary setObject:@"musicvenues" forKey:@"Live Music"];
    
    //Mile Radius
    [finalDictionary setObject:@"1" forKey:@"1 mile"];
    [finalDictionary setObject:@"2" forKey:@"2 miles"];
    [finalDictionary setObject:@"3" forKey:@"3 miles"];
    [finalDictionary setObject:@"5" forKey:@"5 miles"];
    [finalDictionary setObject:@"10" forKey:@"10 miles"];
    [finalDictionary setObject:@"20" forKey:@"20 miles"];
    [finalDictionary setObject:@"50" forKey:@"50 miles"];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static CGFloat cellDefaultHeight = 50;
    static CGFloat screenDefaultHeight =568;
    
    CGFloat factor = cellDefaultHeight/screenDefaultHeight;
    return factor * [UIScreen mainScreen].bounds.size.height;
}




#pragma mark -- Search Bar Delegate Methods

-(BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    
    
    if(customSearchBar == searchBar)
    {
        if(isCheckedSearchUserInteractio == TRUE)
        {
            [searchBar resignFirstResponder];
            return NO;
        }
        else
        {
            return YES;
        }
        
    }
    else
    {
        _innerContentView.frame = CGRectMake(0, -80, self.view.frame.size.width, self.view.frame.size.height);
        
        return YES;
    }
    
}

-(BOOL)searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if(customSearchBar == searchBar)
    {
        if(isCheckedSearchUserInteractio == TRUE)
        {
            return NO;
        }
        else
        {
            return YES;
        }
    }
    else
    {
        return YES;
    }
    
}
-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if(customSearchBar == searchBar)
    {
        if(isCheckedSearchUserInteractio == TRUE)//Close Button Called
        {
            mileString = @"";
            happyString = @"";
            timeString = @"";
            cityString = @"";//man
            dayString = @"";
            [arrayAppend removeAllObjects];
            isCheckedSearchUserInteractio = FALSE;
            return;
        }
    }
    else
    {
        [self searchByEvents_city:searchText];
    }
}
- (void)searchByEvents_city:(NSString *)searchStr
{
    if([searchStr length] > 0)
    {
        [tempArray removeAllObjects];
        for (RegisterModal *eventsData in _responseArray)
        {
            NSRange range=[eventsData.city rangeOfString:searchStr options:(NSCaseInsensitiveSearch)];
            
            if(range.location != NSNotFound)
                [tempArray addObject:eventsData];
        }
        arrayValueForInnerTable = tempArray;
        
    }else{
        
        [tempArray removeAllObjects];
        arrayValueForInnerTable = _responseArray;
    }
    
    [_tblViewInnerSearchOption reloadData];
    NSLog(@"data == %@",tempArray);
}

-(void)searchByEvents:(NSString *)searchStr
{
    [tempArray removeAllObjects];
    if([searchStr length] > 2)
    {
        [self GetItemList:searchStr];
    }
    else{
        [tempArray removeAllObjects];
        _tbl_SearchItems.hidden = YES;
    }
    
   [self scrollViewDidScroll:_tbl_SearchItems];
    
    NSLog(@"data == %@",tempArray);

    
    
    
    
    
    
//    if([searchStr length] > 0)
//    {
//        [tempArray removeAllObjects];
//        for (RegisterModal *eventsData in _responseArray)
//        {
//            NSRange range=[eventsData.city rangeOfString:searchStr options:(NSCaseInsensitiveSearch)];
//            if(range.location != NSNotFound)
//                [tempArray addObject:eventsData];
//        }
//        arrayValueForInnerTable = tempArray;
//    }else{
//        
//        [tempArray removeAllObjects];
//        arrayValueForInnerTable = _responseArray;
//    }
//    
//    [_tblViewInnerSearchOption reloadData];
//    NSLog(@"data == %@",tempArray);
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    _innerContentView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    
    [searchBar resignFirstResponder];
    
}
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
}

#pragma mark - IBAction Button Handler

-(IBAction)btnIsItemsClick:(id)sender
{
    _btnIsItem.selected = !_btnIsItem.selected;
    if (_btnIsItem.isSelected)
    {
        [_btnIsItem setImage:[UIImage imageNamed:@"check-blue"] forState:UIControlStateNormal];
    }
    else{
        [_btnIsItem setImage:[UIImage imageNamed:@"uncheck-blue"] forState:UIControlStateNormal];
    }
        //    spotlytePromotionViewController *spotLightVC = [self.storyboard instantiateViewControllerWithIdentifier:@"spotlight"];
//    spotLightVC.selectedController = @"search";
//    [self.navigationController pushViewController:spotLightVC animated:YES];
}
-(IBAction)pressSpotLight:(id)sender
{
    spotlytePromotionViewController *spotLightVC = [self.storyboard instantiateViewControllerWithIdentifier:@"spotlight"];
    spotLightVC.selectedController = @"search";
    [self.navigationController pushViewController:spotLightVC animated:YES];
}
- (IBAction)innerSearchOkButtonHandler:(id)sender
{
    isCheckedLoadData = TRUE;
    if([categoryString isEqualToString:@"radius"])
    {
        if(isCheckedMileRadius == TRUE)
        {
            [restModalObj setIsCheckedModalMile:isCheckedMileRadius];
            [restModalObj setIntMile:selectedIndexRadius];
            mileString = [arrayValueForInnerTable objectAtIndex:selectedIndexRadius];
        }
        else
        {
            [restModalObj setIsCheckedModalMile:isCheckedMileRadius];
            mileString = @"";
        }
    }
    else if([categoryString isEqualToString:@"happyhour"])
    {
        if(isCheckedPrice == TRUE)
        {
            scrollIndex = selectedIndexCity;
            [restModalObj setIsCheckedModalPrice:isCheckedPrice];
            [restModalObj setIntPrice:selectedIndexPrice];
            happyString = [arrayValueForInnerTable objectAtIndex:selectedIndexPrice];
        }
        else
        {
            [restModalObj setIsCheckedModalPrice:isCheckedPrice];
            happyString = @"";
        }
    }
    else if([categoryString isEqualToString:@"day"])
    {
        if(isCheckedDay == TRUE)
        {
            scrollIndex = selectedIndexDay;
            [restModalObj setIsCheckedModalDay:isCheckedDay];
            [restModalObj setIntDay:selectedIndexDay];
            dayString = [arrayValueForInnerTable objectAtIndex:selectedIndexDay];
        }
        else
        {
            [restModalObj setIsCheckedModalDay:isCheckedDay];
            dayString = nil;
        }
    }
    else if([categoryString isEqualToString:@"city"])
    {
        _innerContentView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
        
        if(isCheckedCity == TRUE)
        {
            NSArray *valArray = [_responseArray valueForKey:@"cityID"];
            scrollIndex = [valArray indexOfObject:selectedCityId];
            RegisterModal *modal = [arrayValueForInnerTable objectAtIndex:selectedIndexCity];
            selectedCityId = modal.cityID;
            [restModalObj setIsCheckedModalCity:isCheckedCity];
            [restModalObj setIntCity:selectedIndexCity];
            [restModalObj setCityId:modal.cityID];
            statusCity =  modal.statusCity;
            
            //NSArray *valArray2 = [_responseArray valueForKey:@"CityorArea"];
          //  scrollIndex = [valArray2 indexOfObject:selectedCityId];
            RegisterModal *modal2 = [arrayValueForInnerTable objectAtIndex:selectedIndexCity];
            [restModalObj setCityOrArea:modal2.CityorArea];
            
          //  NSString *strStatus = [statusArr indexOfObject:selectedCityId];
            
            if ([restModalObj.CityOrArea isEqualToString:@"Area"])
            {
                cityString = [NSString stringWithFormat:@"%@,Chicago",modal.city];
            }
            else{
                cityString = [NSString stringWithFormat:@"%@,IL",modal.city];
            }
            strStatus = restModalObj.CityOrArea;
        }
        else
        {
            [restModalObj setIsCheckedModalCity:isCheckedCity];
            cityString = @"";
            statusCity = @"";
        }
    }
    else
    {
        if(isCheckedTime == TRUE)
        {
            [restModalObj setIsCheckedModalTime:isCheckedTime];
            [restModalObj setIntTime:selectedIndexTime];
            timeString = [arrayValueForInnerTable objectAtIndex:selectedIndexTime];
            scrollIndexHappyHours = selectedIndexTime;
        }
        else
        {
            [restModalObj setIsCheckedModalTime:isCheckedTime];
            timeString = @"";
        }
    }
    [_innerContentView removeFromSuperview];
    [tblViewSearchOption reloadData];
    _innerContentView = nil;
}

- (IBAction)innerSearchCancelButtonHandler:(id)sender
{
    isCheckedLoadData = TRUE;
    
    if([categoryString isEqualToString:@"radius"])
    {
        isCheckedMileRadius = restModalObj.isCheckedModalMile;
        selectedIndexRadius = restModalObj.intMile;
        
    }
    else if([categoryString isEqualToString:@"happyhour"])
    {
        isCheckedPrice = restModalObj.isCheckedModalPrice;
        selectedIndexPrice = restModalObj.intPrice;
        
    }
    else if([categoryString isEqualToString:@"happyhour"])
    {
        isCheckedDay = restModalObj.isCheckedModalDay;
        selectedIndexDay = restModalObj.intDay;
        
    }
    else if([categoryString isEqualToString:@"city"])
    {
        _innerContentView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
        isCheckedCity = restModalObj.isCheckedModalCity;
        selectedIndexCity = restModalObj.intCity;
        selectedCityId = restModalObj.cityId;
    }
    else
    {
        isCheckedTime = restModalObj.isCheckedModalTime;
        selectedIndexTime = restModalObj.intTime;
    }
    [_innerContentView removeFromSuperview];
    _innerContentView = nil;
    [tblViewSearchOption reloadData];
}


- (IBAction)backButtobHandler:(id)sender{
    [[NSUserDefaults standardUserDefaults]setBool:FALSE forKey:@"ischeckedshowquickview"];
    [self.navigationController popViewControllerAnimated:YES];
}



-(IBAction)pressSearch:(id)sender
{
    BOOL dayStringAdded = ([dayString isEqualToString:@""] || dayString == nil) ? 0 : 1;
    BOOL mileStringAdded = ([mileString isEqualToString:@""] || mileString == nil) ? 0 : 1;
    BOOL happyStringAdded = ([happyString isEqualToString:@""] || happyString == nil) ? 0 : 1;
    BOOL timeStringAdded = ([timeString isEqualToString:@""] || timeString == nil) ? 0 : 1;
    BOOL cityStringAdded = ([cityString isEqualToString:@""] || cityString == nil) ? 0 : 1;
    BOOL statusCityAdded = ([statusCity isEqualToString:@""] || statusCity == nil) ? 0 : 1;
    BOOL searchStringAdded = ([CustomSearchView.text isEqualToString:@""] || CustomSearchView.text == nil|| [CustomSearchView.text isEqualToString:@"Beer, Wine, Martini, Jameson, Tito's, Bloody...."]) ? 0 : 1;
    
    if (!dayStringAdded && !mileStringAdded && !happyStringAdded && !timeStringAdded && !cityStringAdded && !statusCityAdded && !searchStringAdded) {
        [self showAlertTitle:@"" Message:@"Please select atleast one advanced option or enter text in search box."];
        return;
    }
    RestListViewController *restVC = [self.storyboard instantiateViewControllerWithIdentifier:@"restlist"];
    
    NSString *HappyString_Price = [happyString
                                     stringByReplacingOccurrencesOfString:@"$" withString:@""];
    restVC.mileString = mileString;
    restVC.happyHourString = HappyString_Price;
    restVC.timeString = timeString;
    restVC.cityString = cityString;
    restVC.status = statusCity;
    restVC.strStatus = strStatus;
    restVC.searchString =[CustomSearchView.text stringByReplacingOccurrencesOfString:@"Beer, Wine, Martini, Jameson, Tito's, Bloody...." withString:@""];
    if ([CustomSearchView.text isEqualToString: @"Beer, Wine, Martini, Jameson, Tito's, Bloody...."])
    {
        restVC.searchString = @"";
    }
    else
    {
        restVC.searchString = CustomSearchView.text;
    }
    restVC.HappHoursDay=dayString;
    [self.navigationController pushViewController:restVC animated:YES];
}

-(IBAction)pressProfileBtn:(id)sender
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [defaults objectForKey:@"logindetails"];
    RegisterModal *registerModalResponeValue = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    MyProfileViewController *profileVC = [self.storyboard instantiateViewControllerWithIdentifier:@"myprofile"];
    profileVC.selectedController = @"currentuser";
    profileVC.selectedTweet = 0;
    profileVC.user_IDString = registerModalResponeValue.userID;
    [self.navigationController pushViewController:profileVC animated:YES];
}

- (IBAction)menuButtonHandler:(id)sender
{
    _MenuTapBtn_lbl.hidden=NO;
    [self.view endEditing:YES];
    [self.revealViewController revealToggle:sender];
}


#pragma  mark --- Alert Controller Common Method

-(void)showAlertTitle:(NSString *)title Message:(NSString *)message
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [alertController addAction:okBtn];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

#pragma mark - UITextView Delegate Methods

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    
    
    if ([textView.text isEqualToString:@"Beer, Wine, Martini, Jameson, Tito's, Bloody...."])
    {
        [textView setText:@""];
        
        [textView setTextColor:[UIColor darkGrayColor]];
        
    }
    
    
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    if (CustomSearchView.text.length==0)
    {
        int width=self.view.frame.size.width;
        if (width == 320)
        {
            CustomSearchView.font = [UIFont systemFontOfSize:10.2];
            
        }
        else if (width == 414)
        {
            
            CustomSearchView.font = [UIFont systemFontOfSize:14.7];
            
            
        }
        else
        {
            CustomSearchView.font = [UIFont systemFontOfSize:12.5];
            
            
        }
        [textView setText:@"Beer, Wine, Martini, Jameson, Tito's, Bloody...."];
        [textView setTextColor:[UIColor lightGrayColor]];
        
    }
    
    
}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    CustomSearchView.font = [UIFont systemFontOfSize:15];
   
    if([text isEqualToString:@"\n"])
    {
        [textView resignFirstResponder];
        return NO;
    }
        if (text.length <3)
        {
            _tbl_SearchItems.hidden = true;
        }
    
    
    if([text isEqualToString:@""])
    {
        if (text.length > 0){
            searchString_text = [CustomSearchView.text substringToIndex:[searchString_text length]-1];
        }
        
    }
    else{
        searchString_text = [CustomSearchView.text stringByAppendingString:text];
    }

    [CustomSearchView setTextColor:[UIColor blackColor]];
    if(isCheckedSearchUserInteractio == TRUE)//Close Button Called
    {
        mileString = @"";
        happyString = @"";
        timeString = @"";
        cityString = @"";//man
        dayString = @"";
        
        [arrayAppend removeAllObjects];
        isCheckedSearchUserInteractio = FALSE;
    }
    else
    {
        if (![text isEqualToString:@""])
        {
        [self searchByEvents:searchString_text];
        }
    }
    
    
    return YES;
}
- (IBAction)MenuTapBtn:(id)sender
{
    _MenuTapBtn_lbl.hidden=YES;
    [self.view endEditing:YES];
    [self.revealViewController revealToggle:0];
    
    
}
#pragma mark -- ScrollView Methods
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [self.indicator scrollViewDidScroll:_tbl_SearchItems];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    [self.indicator scrollViewDidEndDragging:_tbl_SearchItems willDecelerate:decelerate];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    [self.indicator scrollViewDidEndDecelerating:_tbl_SearchItems];
}

- (void)scrollViewWillScrollToTop:(UIScrollView *)scrollView {
    [self.indicator scrollViewWillScrollToTop:_tbl_SearchItems];
}

- (void)scrollViewDidScrollToTop:(UIScrollView *)scrollView {
    [self.indicator scrollViewDidScrollToTop:_tbl_SearchItems];
}

@end
