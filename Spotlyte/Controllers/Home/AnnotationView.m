//
//  AnnotationView.h
//  Spotlyte
//
//  Created by Tamilarasan on 3/3/16.
//  Copyright © 2016 Naveen. All rights reserved.
//


#import "AnnotationView.h"
#import "MapAnnotation.h"


@implementation AnnotationView
@synthesize strPlaceType,lblCount;

- (id)initWithAnnotation:(id <MKAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithAnnotation:annotation reuseIdentifier:reuseIdentifier];
    if (self != nil)
    {
        //imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 40, 42)];
        //[self addSubview:imageView];
        
        
        self.centerOffset = CGPointMake(0, -35);
    }
    
    return self;
}

- (void)setAnnotation:(id <MKAnnotation>)inAnnotation
{
    [super setAnnotation:inAnnotation];
    MapAnnotation *mapAnnotation = (MapAnnotation*)inAnnotation;
    
    self.lblCount.layer.cornerRadius = self.lblCount.frame.size.width / 2;
    self.lblCount.clipsToBounds = YES;
    
    self.lblCount.text = [NSString stringWithFormat:@"%@",mapAnnotation.annotationCount];
    
    if ([mapAnnotation.annotationCount isEqualToString:@"0"]) {
        self.lblCount.hidden = NO;
        self.lblCount.text = @"0";
    } else {
        self.lblCount.hidden = NO;
    }
    
    if(mapAnnotation.rating.floatValue == 0)
    {
        if([mapAnnotation.annotationCount intValue] >= 0){
            self.annotatonImage.image = [UIImage imageNamed:@"grayBlank"];
        }
        else{
            self.annotatonImage.image = [UIImage imageNamed:@"gray"];
        }
    }
    else if(mapAnnotation.rating.floatValue<=2)
    {
        if([mapAnnotation.annotationCount intValue] >= 0){
            self.annotatonImage.image = [UIImage imageNamed:@"redBlank"];
        }
        else{
            self.annotatonImage.image = [UIImage imageNamed:@"red"];
        }
    }
    else if(mapAnnotation.rating.floatValue<4)
    {
        if([mapAnnotation.annotationCount intValue] >= 0){
            self.annotatonImage.image = [UIImage imageNamed:@"yellowBlank"];
        }
        else{
            self.annotatonImage.image = [UIImage imageNamed:@"yellow"];
        }
    }
    else
    {
        if([mapAnnotation.annotationCount intValue] >= 0){
            self.annotatonImage.image = [UIImage imageNamed:@"greenBlank"];
        }
        else{
            self.annotatonImage.image = [UIImage imageNamed:@"green"];
        }
    }
   
    /*
    if([mapAnnotation.placeType isEqualToString:@"restaurant"] || [mapAnnotation.placeType isEqualToString:@"hotel"]){
        if(mapAnnotation.rating.floatValue == 0)
        {
            self.annotatonImage.image = [UIImage imageNamed:@"RestGreyColor"];
        }
        else if(mapAnnotation.rating.floatValue<=2)
        {
            self.annotatonImage.image = [UIImage imageNamed:@"RestaurantRedIcon"];
        }
        else if(mapAnnotation.rating.floatValue<4)
        {
            self.annotatonImage.image = [UIImage imageNamed:@"RestaurantYellowIcon"];
        }
        else
        {
            self.annotatonImage.image = [UIImage imageNamed:@"RestaurantGreenIcon"];
        }
    }
    else if([mapAnnotation.placeType isEqualToString:@"bar"] || [mapAnnotation.placeType isEqualToString:@"night_club"]){
        if(mapAnnotation.rating.floatValue == 0){
            self.annotatonImage.image = [UIImage imageNamed:@"BarGreyColor"];
        }
        else if(mapAnnotation.rating.floatValue<=2){
            self.annotatonImage.image = [UIImage imageNamed:@"BarRedIcon"];
        }else if(mapAnnotation.rating.floatValue<4){
            self.annotatonImage.image = [UIImage imageNamed:@"BarYellowIcon"];
        }else{
            self.annotatonImage.image = [UIImage imageNamed:@"BarGreenIcon"];
        }
    }
    else if([mapAnnotation.placeType isEqualToString:@"concert"]){
        
        if(mapAnnotation.rating.floatValue == 0){
            self.annotatonImage.image = [UIImage imageNamed:@"ConcertGreyColor"];
        }
        else if(mapAnnotation.rating.floatValue<=2){
            self.annotatonImage.image = [UIImage imageNamed:@"ConcertRedIcon"];
        }else if(mapAnnotation.rating.floatValue<4){
            self.annotatonImage.image = [UIImage imageNamed:@"ConcertYellowIcon"];
        }else{
            self.annotatonImage.image = [UIImage imageNamed:@"ConcertGreenIcon"];
        }
    }
    else if([mapAnnotation.placeType isEqualToString:@"museum"]){
        
        if(mapAnnotation.rating.floatValue == 0){
            self.annotatonImage.image = [UIImage imageNamed:@"MuseumGreyColor"];
        }
        else if(mapAnnotation.rating.floatValue<=2){
            self.annotatonImage.image = [UIImage imageNamed:@"MuseumRedIcon"];
        }else if(mapAnnotation.rating.floatValue<4){
            self.annotatonImage.image = [UIImage imageNamed:@"MuseumYellowIcon"];
        }else{
            self.annotatonImage.image = [UIImage imageNamed:@"MuseumGreenIcon"];
        }
    }
    else if([mapAnnotation.placeType isEqualToString:@"movie_theater"]){
        
        if(mapAnnotation.rating.floatValue == 0){
            self.annotatonImage.image = [UIImage imageNamed:@"TheatreGreyColor"];
        }
        else if(mapAnnotation.rating.floatValue<=2){
            self.annotatonImage.image = [UIImage imageNamed:@"TheatreRedIcon"];
        }else if(mapAnnotation.rating.floatValue<4){
            self.annotatonImage.image = [UIImage imageNamed:@"TheatreYellowIcon"];
        }else{
            self.annotatonImage.image = [UIImage imageNamed:@"TheatreGreenIcon"];
        }
    }
    else{
        if(mapAnnotation.rating.floatValue == 0)
        {
            self.annotatonImage.image = [UIImage imageNamed:@"RestGreyColor"];
        }
        else if(mapAnnotation.rating.floatValue<=2)
        {
            self.annotatonImage.image = [UIImage imageNamed:@"RestaurantRedIcon"];
        }
        else if(mapAnnotation.rating.floatValue<4)
        {
            self.annotatonImage.image = [UIImage imageNamed:@"RestaurantYellowIcon"];
        }else
        {
            self.annotatonImage.image = [UIImage imageNamed:@"RestaurantGreenIcon"];
        }
    }
    */
}



@end
