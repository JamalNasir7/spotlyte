//
//  ViewController.h
//  Spotlyte
//
//  Created by Tamilarasan on 3/3/16.
//  Copyright © 2016 Naveen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AnnotationView.h"
#import "EDStarRating.h"
#import "ResturantModal.h"
#import "AppDelegate.h"
#import <MessageUI/MFMailComposeViewController.h>
#import "MapAnnotation.h"
#import "RegisterModal.h"
#import "HMSegmentedControl.h"

@interface HomeViewController : UIViewController<MKMapViewDelegate,EDStarRatingProtocol,UITextFieldDelegate,CLLocationManagerDelegate,UIGestureRecognizerDelegate,UITableViewDataSource,UITableViewDelegate,MFMailComposeViewControllerDelegate,UITabBarControllerDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UIScrollViewDelegate, UIGestureRecognizerDelegate>
{
    int i;
    __weak IBOutlet UIButton *btnHighlight;
    AnnotationView *annotationView;
    IBOutlet MKMapView *mapView;
    NSInteger restaurantTag;
    
    IBOutlet UIView *quick_touchableView;
    IBOutlet UIButton *btnClearFilter;
    IBOutlet UIImageView *pressBackBtn;
    UITapGestureRecognizer *tap;
    
    CLLocationManager *locationManager;
    CLLocation *currentLocation;
    
    NSString *stringLat;
    NSString *stringLong;
    
    NSString *stringLatForCurrentLocation;
    
    NSString *stringLongForCurrentLocation;
    
    NSString *user_IDRegister;
    RegisterModal *registerModalResponeValue;
    
    BOOL isShow;
    BOOL isCheckedHotel;
    IBOutlet UIButton       *btn_Emonji1;
    IBOutlet UIButton       *btn_Emonji2;
    __weak IBOutlet UIImageView *imageView_Like;
    __weak IBOutlet UIImageView *imageView_Unlike;
    IBOutlet UIButton       *btn_Emonji3;
    IBOutlet UIButton       *btn_Emonji4;
    IBOutlet UIScrollView   *scrollView;
    IBOutlet UIView         *view_PressTab;
    IBOutlet UILabel        *lbl_TopView;
    
    IBOutlet NSLayoutConstraint *mapView_bottomSpace;
    IBOutlet NSLayoutConstraint *quickView_bottomSpace;


    
    IBOutlet UIView *bottomView,*topView,*view_Main;
    IBOutlet UIButton *backButton,*searchButton,*homeButton;
    
    IBOutlet UITableView *tblView_Category;
    IBOutlet UITableView *tblView_DailySpecial;
    IBOutlet UITableView *tblView_HappyHours;
    IBOutlet UITableView *tblView_TopSpots;
    
    NSMutableArray *array_weekDays;
    
    NSMutableArray *array_SelectedDailySpecial;
    NSMutableArray *array_SelectedHappyHours;
    NSMutableArray *array_SelectedTopSpots;
    
    IBOutlet UIView *view_Btn;
    BOOL isShowQuickView;
    BOOL isCheckedShowQuickView;
    NSString *placeidSub;
  
    NSMutableArray *tempArray;
    
    IBOutlet NSLayoutConstraint *tblView_X;
    
    IBOutlet UIButton *btn_Filter;
    IBOutlet UIButton *btn_FilterReset;
    IBOutlet UIButton *btn_FilterDone;
    IBOutlet UIImageView *imgArrowDown;
    
    IBOutlet UIButton *btn_HappyHours;
    IBOutlet UIButton *btn_Daily;
    IBOutlet UIButton *btn_ViewMap;
    IBOutlet UILabel *lbl_Address;
    
    //Daily Special
    
    ResturantModal *restModal_DidSelectMap;

    BOOL isCheckedSelectMap;
    
    
    NSString *string_HappyHours;
    NSString *string_DailySpecials;
    
    BOOL isCheckedLiveMap;
    BOOL isCheckedHappyHours;
    BOOL isCheckedDailySpecials;
    BOOL isSelectedValueHappy;
    BOOL isSelectedValueSspecials;
    BOOL isSelectedValuesIsLive;
    BOOL isCheckedQuickView;
    
    NSString *checkWillAppear;
    
    //IBOutlet UIImageView *imageView_Indicator2;
    BOOL isCheckedLazyLoading;
    BOOL isCheckedWhichCoordinate;
    NSString *latStr ;
    NSString *longStr;
    BOOL mapViewsHidden;
    NSInteger startingValue ;
    NSInteger endValue ;
    
    
    IBOutlet UILabel *lbl_BtnBar;

    IBOutlet UIView *view_Location;
   
    
    BOOL isCheckedLocation;
    
    MapAnnotation *closedAnnot;
    
    IBOutlet UIView *view_CloseQuick;
    NSString *searchAreaPlaceString;
    
    BOOL isCheckedBackData_HappyDaily;
    
    ResturantModal *lastModal;
    UIView *quick_mainView;
    UIView *HighlightView;
    
    
    __weak IBOutlet UILabel *lblTotalResults;
    //outlets for the quick view elements
    IBOutlet UIView *viewMapQuickDetails;
    IBOutlet UILabel *lblResName;
    IBOutlet UILabel *lblResAddress;
    IBOutlet UILabel *lblResPhoneNo;
    IBOutlet UILabel *lblResHours;
    IBOutlet UILabel *lblResLike;
    IBOutlet UILabel *lblResDislike;
    IBOutlet UILabel *lblWebSite;
    IBOutlet UIImageView *imgViewResImage;
    IBOutlet UIButton *btnResLike;
    IBOutlet UIButton *btnResDislike;
    IBOutlet UIButton *btnResFavourite;
    IBOutlet UILabel *lblFilter;
    IBOutlet UILabel *lblListView;
    IBOutlet UIButton *btnClose_quickView;
    IBOutlet UIButton *btn_QuickViewHappy;
    IBOutlet UIButton *btn_quickViewDaily;
    IBOutlet UIView *view_Empty;
    IBOutlet UITableView *tableView_DailySpecial;
    IBOutlet UIButton *btn_ClickWebSite;
    IBOutlet UIButton *viewMoreButton;
    IBOutlet UIView *view_tableView;
    
    IBOutlet UIView *view_Filter;
    IBOutlet UIView *view_Filter_DailySpecial;
    IBOutlet UIView *view_Filter_HappyHours;
    IBOutlet UIView *view_Filter_TopSpots;
    
     IBOutlet UILabel *lblTotalResult;
    IBOutlet UIButton *btnSundayDaily;
    IBOutlet UIButton *btnMondayDaily;
    IBOutlet UIButton *btnTuesdayDaily;
    IBOutlet UIButton *btnWednesdayDaily;
    IBOutlet UIButton *btnThuesdayDaily;
    IBOutlet UIButton *btnFridayDaily;
    IBOutlet UIButton *btnSaturdayDaily;
    
    IBOutlet UIButton *btnSundayHappy;
    IBOutlet UIButton *btnMondayHappy;
    IBOutlet UIButton *btnTuesdayHappy;
    IBOutlet UIButton *btnWednesdayHappy;
    IBOutlet UIButton *btnThuesdayHappy;
    IBOutlet UIButton *btnFridayHappy;
    IBOutlet UIButton *btnSaturdayHappy;
    
}

@property (weak, nonatomic) IBOutlet UIView *info_View;
@property (nonatomic) BOOL currentLocationEnable;
@property (nonatomic) BOOL isCheckedCityStatus;
@property (nonatomic) BOOL firstTimeLoad;

@property (nonatomic,strong) NSMutableArray *categoryarray;
@property (nonatomic,strong) NSArray *cityArray;
@property (nonatomic,strong) NSString *strSW_Lat;
@property (nonatomic,strong) NSString *strSW_Long;
@property (nonatomic,strong) NSString *strNE_Lat;
@property (nonatomic,strong) NSString *strNE_Long;



@property (weak, nonatomic) IBOutlet UIButton *btnCurrentLocation;

@property (strong, nonatomic) ResturantModal *restModal_QuickView;
@property (strong,nonatomic) ResturantModal *rest_HighLight;
@property (strong, nonatomic) IBOutlet UITextField *txt_SearchBox;


@property (weak, nonatomic) IBOutlet EDStarRating *starRatingView;
@property (nonatomic, retain) ResturantModal *resturantModalObj;
@property (strong, nonatomic) ResturantModal *nearByRestModal;
@property (weak, nonatomic) IBOutlet UIImageView *imageView_Tutotial;

@property (nonatomic, retain) NSMutableArray *resturantArray;
@property (nonatomic, retain) NSMutableArray *resturantScrollArray;
@property (nonatomic, retain) NSArray *restListArray;

@property (nonatomic, retain) NSString *checkVC;
@property (nonatomic, retain) NSString *restVCString;
@property (nonatomic, retain) NSString *Place_id;

@property (nonatomic, retain) NSString *checkLoadService;
@property NSInteger selectedRestValue;


//Daily Special
@property (weak, nonatomic)  ResturantModal *modalOBJ;
@property (weak, nonatomic)  ResturantModal *promotionModalOBJ;

@property (strong, nonatomic) NSArray *arraySpecialforday;
@property (strong, nonatomic) NSMutableArray *array_TopSpots;

@property (weak, nonatomic) IBOutlet UIButton *HappHoursTable_lbl;

- (IBAction)HappHoursTable:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *DailySpecial_lbl;

@property (weak, nonatomic) IBOutlet UIButton *MenuTapBtn_lbl;

- (IBAction)MenuTapBtn:(id)sender;


- (IBAction)DailySpecial:(id)sender;

-(IBAction)pressProfileBtn:(id)sender;
-(IBAction)pressAdvancedSearch:(id)sender;
-(IBAction)pressSpotLight:(id)sender;
-(IBAction)pressViewMore:(id)sender;
-(IBAction)pressResLike:(id)sender;
-(IBAction)pressResDisLike:(id)sender;
-(IBAction)pressFavouriteBtn:(id)sender;
-(IBAction)pressBack:(id)sender;

- (IBAction)pressHappyHours:(id)sender;
- (IBAction)pressViewMap:(id)sender;
- (IBAction)pressDailySpecials:(id)sender;

- (IBAction)btnFilterClick:(id)sender;
- (IBAction)btnDoneFilterClick:(id)sender;
- (IBAction)btnResetFilterClick:(id)sender;

- (IBAction)pressNavigateSpecialHours:(id)sender;

- (IBAction)pressSpecialHappyHours:(id)sender;

-(IBAction)pressReportHereBtn : (id)sender;
-(IBAction)pressSpecialHappyClickHereBtn : (id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnYellow;

-(IBAction)pressCurrentLocation:(id)sender;
-(IBAction)pressRandomSearch:(id)sender;
-(IBAction)pressCloseBtnQuickView:(id)sender;

-(void)pressSearchBtn:(NSString*)city;
@property (weak, nonatomic) IBOutlet HMSegmentedControl *viewTool;

@property (strong, nonatomic) IBOutlet UILabel *lblDisplayLocations;
@property (strong, nonatomic) IBOutlet UILabel *lblRating;

//-(void)getHandler:(void(^)(BOOL success))callback;

// Highlight Popup
@property (weak, nonatomic) IBOutlet UICollectionView *Collection_Highlight;
@property (weak, nonatomic) IBOutlet NSArray *ArrHighLight;

@property (weak, nonatomic) IBOutlet UIButton *btnHighlightCancel;
@property (weak, nonatomic) IBOutlet UILabel *lblHighlightTitle;

/// QuickView Slider
@property (weak, nonatomic) IBOutlet UIScrollView *sliderMainScroller;


@end

