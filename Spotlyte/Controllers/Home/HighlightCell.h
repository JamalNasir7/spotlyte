//
//  HighlightCell.h
//  Spotlyte
//
//  Created by Tops on 8/29/17.
//  Copyright © 2017 Hemant. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HighlightCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgHighlights;
@property (weak, nonatomic) IBOutlet UILabel *lblHighlights;

@end
