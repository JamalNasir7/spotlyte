//
//  ViewController.m
//  Spotlyte
//
//  Created by Tamilarasan on 3/3/16.
//  Copyright © 2016 Naveen. All rights reserved.
//

#import "HomeViewController.h"
#import "SWRevealViewController.h"
#import "ProfileViewController.h"
#import "customSearchViewController.h"
#import "ProfileViewController.h"
#import "MyProfileViewController.h"
#import "spotlytePromotionViewController.h"
#import "PromotionsViewController.h"
#import "MapAnnotation.h"
#import "RestListViewController.h"
#import "NewHomeViewController.h"
#import "JTSScrollIndicator.h"
#import "HeaderView.h"
#import "filterCell.h"
#import "DailySpecialCell.h"
#import "HighlightCell.h"

BOOL MapListBoolean;
BOOL HappyHourseBool_lbl;
BOOL DailySpecialBool_lbl;


@interface HomeViewController ()
{
    NSArray *MapListArray;
    
    long HappyHoursSelected;
    
    long DailySpecialSelected;
    
    long CommanRow;
    
    float SelectedLat;
    
    float SelectedLog;
    
    BOOL manuallySearchLocation;
    BOOL zoomEnable;
    BOOL annotationSelected;
    BOOL SearchFixedPlace;
    
    int btnNo;
    double min_distance;
    
    NSTimer *mapMoveTimer;
    
    UIImage *selectedAnnnotationImage;
    
    MapAnnotation *selectedAnnotation;
    MKAnnotationView *selectedView;
    
    NSString *GlobalRadius;
    
    BOOL mapUpdate;
    BOOL isPinSelected;
    BOOL tableCellSelected;
    BOOL isKeepDraging;
    MKCoordinateRegion regionChange;
    int NumberOfRecordFound;
    NSMutableArray *arrPasstoRest;
    
    

}
@property (strong, nonatomic) NSString *strCityOrPlace;
@property (strong, nonatomic) JTSScrollIndicator *indicator;
@property (strong, nonatomic) IBOutlet UIView *vwHppyDailly;
@property (weak, nonatomic) IBOutlet UIButton *List_Btn;
@property (weak, nonatomic) IBOutlet UIButton *MenuTapBtn;
@property (nonatomic, retain) MKPointAnnotation *centerAnnotation;
- (IBAction)MenuTapButton:(id)sender;

@property (nonatomic, retain) CLLocation* initialLocation;
@end

@implementation HomeViewController
@synthesize starRatingView;
@synthesize resturantArray;
@synthesize resturantScrollArray;
@synthesize resturantModalObj;
@synthesize checkVC;
@synthesize nearByRestModal;
@synthesize restListArray;
@synthesize selectedRestValue;
@synthesize arraySpecialforday;
@synthesize array_TopSpots;
@synthesize promotionModalOBJ;
@synthesize restVCString,MenuTapBtn;
@synthesize List_Btn,info_View;
@synthesize HappHoursTable_lbl,DailySpecial_lbl;
@synthesize txt_SearchBox;
@synthesize ArrHighLight;

#pragma mark ViewController Methods


- (void)createSliderWithImagesWithAutoScroll {
    //    self.backgroundColor = [UIColor blackColor];
    //self.view.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
    
    /*
     lblResName.text = @"";
     lblResAddress.text = @"";
     lblResPhoneNo.text = @"";
     lblWebSite.text = @"";
     btn_Emonji1.enabled = NO;
     btn_Emonji2.enabled = NO;
     btn_Emonji3.enabled = NO;
     btn_Emonji4.enabled = NO;
     if([modal.resturantEMail isEqualToString:@""])
     {
     _btnYellow.hidden = YES;
     }
     else{
     _btnYellow.hidden = NO;
     }
     
     if ([modal.isHighlights isEqualToString:@"0"])
     {
     btnHighlight.hidden = YES;
     }
     else{
     btnHighlight.hidden = NO;
     }
     
     lblResName.text = modal.resturantName.uppercaseString;
     
     NSString *detailedAddress;
     if([checkVC isEqualToString:@"restlist"])
     {
     
     detailedAddress = [NSString stringWithFormat:@"%@, %@, %@ %@",modal.resturantAddress,modal.resturantCity,modal.resturantState,modal.resturantPostalCode];
     }
     else
     {
     detailedAddress = [NSString stringWithFormat:@"%@, %@, %@ %@",modal.resturantAddress,modal.resturantCity,modal.resturantState,modal.resturantPostalCode];
     }
     
     //[lblResAddress setText:detailedAddress.uppercaseString];
     [lblResAddress setText:detailedAddress];
     lblResPhoneNo.text = modal.resturantPhoneNo;
     
     NSString *openingHours = modal.openFrom;
     lblResHours.text = openingHours;
     imgViewResImage.layer.cornerRadius = 3.0;
     [imgViewResImage setBackgroundColor:[UIColor blackColor]];
     imgViewResImage.layer.masksToBounds = YES;
     if ([modal.placeRatings isEqualToString:@"0"]){
     _lblRating.text = @"N/A";
     }else{
     _lblRating.text = modal.placeRatings;
     }
     dispatch_async(dispatch_get_main_queue(), ^{
     NSURL *url = [[NSBundle mainBundle] URLForResource:@"loading123" withExtension:@"gif"];
     NSString *newUrl = [modal.placeImage stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
     [imgViewResImage sd_setImageWithURL:[NSURL URLWithString:newUrl]
     placeholderImage:[UIImage imageNamed:@"noimage.png"]];
     
     });
     lblResLike.text = modal.likecount;
     lblResDislike.text = modal.dislikeCount;
     lblWebSite.text = modal.resturantEMail;
     if([modal.likeORDislike isEqualToString:@"1"] || [modal.likeORDislike isEqualToString:@"2"])
     {
     if([modal.likeORDislike isEqualToString:@"1"])
     {
     btnResLike.enabled = YES;
     btnResDislike.enabled = YES;
     }
     else
     {
     btnResLike.enabled = YES;
     btnResDislike.enabled = YES;
     }
     }
     else
     {
     btnResLike.enabled = YES;
     btnResDislike.enabled = YES;
     }
     
     if(modal.isCheckedFavourite == NO)
     {
     [btnResFavourite setImage:[UIImage imageNamed:@"FavUnSelLIst"] forState:UIControlStateNormal];
     }
     else
     {
     [btnResFavourite setImage:[UIImage imageNamed:@"FavSelLIst"] forState:UIControlStateNormal];
     }
     
     if(IS_IPHONE_6PLUS)
     {
     if(modal.placeRatings.floatValue <= 2.0)
     {
     starRatingView.starHighlightedImage = [UIImage imageNamed:@"star-1_R.png"];
     }
     else if(modal.placeRatings.floatValue <= 3.9)
     {
     starRatingView.starHighlightedImage = [UIImage imageNamed:@"star-1_Y.png"];
     }
     else
     {
     starRatingView.starHighlightedImage = [UIImage imageNamed:@"star-1_G.png"];
     }
     starRatingView.starImage = [UIImage imageNamed:@"unstartemp.png"];
     }
     else if (IS_IPHONE_6)
     {
     if(modal.placeRatings.floatValue <= 2.0)
     {
     starRatingView.starHighlightedImage = [UIImage imageNamed:@"star-1_R.png"];
     }
     else if(modal.placeRatings.floatValue <= 3.9)
     {
     starRatingView.starHighlightedImage = [UIImage imageNamed:@"star-1_Y.png"];
     }
     else
     {
     starRatingView.starHighlightedImage = [UIImage imageNamed:@"star-1_G.png"];
     }
     starRatingView.starImage = [UIImage imageNamed:@"unstartemp.png"];
     
     }
     else
     {
     starRatingView.starImage = [UIImage imageNamed:@"UnStarIcon"];
     if(modal.placeRatings.floatValue <= 2.0)
     {
     starRatingView.starHighlightedImage = [UIImage imageNamed:@"RedStarIcon"];
     
     }
     else if(modal.placeRatings.floatValue <= 3.9)
     {
     starRatingView.starHighlightedImage = [UIImage imageNamed:@"YellowStarIcon"];
     }
     else
     {
     starRatingView.starHighlightedImage = [UIImage imageNamed:@"GreenStarIcon"];
     }
     }
     starRatingView.maxRating = 5.0;
     starRatingView.delegate = self;
     starRatingView.horizontalMargin = 12;
     starRatingView.editable = NO;
     starRatingView.rating = modal.placeRatings.floatValue;
     starRatingView.displayMode = EDStarRatingDisplayAccurate;
     
     [self setEmonjis:1 type:modal.emonjiID.intValue];
     [self setEmonjis:2 type:modal.emonjiID2.intValue];
     [self setEmonjis:3 type:modal.emonjiID3.intValue];
     [self setEmonjis:4 type:modal.emonjiID4.intValue];
     */
    resturantScrollArray = [[NSMutableArray alloc] init];
    int firstFindingIndex = 0;
    Boolean isFound = false;
    if(restModal_DidSelectMap != nil){
        [resturantScrollArray removeAllObjects];
        NSString *placeID =  restModal_DidSelectMap.placeID;
         for (int i=0; i < [resturantArray count]; i++) {
             ResturantModal  *modal = [resturantArray objectAtIndex:i];
             if(!isFound){
                 if([modal.placeID isEqualToString:placeID]){
                     firstFindingIndex = i;
                     isFound = true;
                     [resturantScrollArray addObject:modal];
                 }
             }else{
                  [resturantScrollArray addObject:modal];
             }
         }
        for (int i=0; i < firstFindingIndex; i++) {
             ResturantModal  *modal = [resturantArray objectAtIndex:i];
            [resturantScrollArray addObject:modal];
        }
    }
    _sliderMainScroller.pagingEnabled = YES;
    _sliderMainScroller.delegate = self;
   
    _sliderMainScroller.contentSize = CGSizeMake(([UIScreen mainScreen].bounds.size.width * [resturantArray count] * 3), _sliderMainScroller.frame.size.height);
    
    int mainCount = 0;
    for (int x = 0; x < 3; x++) {
        
        for (int i=0; i < [resturantScrollArray count]; i++) {
            ResturantModal  *modal = [resturantScrollArray objectAtIndex:i];
            UIView *mainView = [[UIView alloc] init];
            CGRect frameRectView;
            frameRectView.origin.y = 0.0f;
            frameRectView.size.width = [UIScreen mainScreen].bounds.size.width;
            frameRectView.size.height = _sliderMainScroller.frame.size.height;
            frameRectView.origin.x = (frameRectView.size.width* mainCount);
            mainView.frame =frameRectView;
            UIImageView *imageV = [[UIImageView alloc] initWithFrame:CGRectMake(([UIScreen mainScreen].bounds.size.width * 0.05), (_sliderMainScroller.frame.size.height * 0.05), [UIScreen mainScreen].bounds.size.width*0.90, (_sliderMainScroller.frame.size.height * 0.90))];
            UIView *overlayView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width*0.90, (_sliderMainScroller.frame.size.height * 0.90))];
            [overlayView setBackgroundColor:[UIColor blackColor]];
            overlayView.layer.opacity = 0.25f;
            overlayView.layer.cornerRadius = 10.0;
            overlayView.layer.masksToBounds = YES;
            
            ///// Ratings Label
            UILabel *rating = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, 50, 50)];
            [rating setBackgroundColor:[UIColor yellowColor]];
            [rating setText: @"4.0"];
            [rating setFont:CalibriBold(14.0)];
            [rating setTextColor:[UIColor blackColor]];
            [rating setTextAlignment:NSTextAlignmentFromCTTextAlignment(kCTTextAlignmentCenter)];
            ////// Address And Name labels
            NSString *detailedAddress;
            if([checkVC isEqualToString:@"restlist"])
            {
                detailedAddress = [NSString stringWithFormat:@"%@, %@, %@ %@",modal.resturantAddress,modal.resturantCity,modal.resturantState,modal.resturantPostalCode];
            }
            else
            {
                detailedAddress = [NSString stringWithFormat:@"%@, %@, %@ %@",modal.resturantAddress,modal.resturantCity,modal.resturantState,modal.resturantPostalCode];
            }
            
           
            UILabel *restAddress = [[UILabel alloc] initWithFrame:CGRectMake(20, (imageV.frame.size.height*0.70),(imageV.frame.size.width*0.45) , (imageV.frame.size.height*0.30))];
            [restAddress setFont:Calibri(18.0)];
            
            [restAddress setTextColor:[UIColor whiteColor]];
            [restAddress setTextAlignment:NSTextAlignmentFromCTTextAlignment(kCTTextAlignmentJustified)];
            restAddress.lineBreakMode = NSLineBreakByWordWrapping;
            restAddress.numberOfLines = 0;
            [restAddress setText: detailedAddress];
            
            UILabel *restName = [[UILabel alloc] initWithFrame:CGRectMake(20, (imageV.frame.size.height*0.60), (imageV.frame.size.width*0.45),(imageV.frame.size.height*0.10))];
            [restName setFont:CalibriBold(19.0)];
            [restName setTextColor:[UIColor whiteColor]];
            [restName setTextAlignment:NSTextAlignmentFromCTTextAlignment(kCTTextAlignmentJustified)];
            [restName setText:modal.resturantName];
            
            //////// UI Button left right arrow
            
            UIImage *btnleftImage = [UIImage imageNamed:@"ic_keyboard_arrow_left_white.png"];
            UIImage *btnrightImage = [UIImage imageNamed:@"ic_keyboard_arrow_right_white.png"];
            UIButton *leftBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, (imageV.frame.size.height-50)/2, 50, 50)];
            UIButton *rightBtn = [[UIButton alloc] initWithFrame:CGRectMake(imageV.frame.size.width-50, (imageV.frame.size.height-50)/2, 50, 50)];
            [leftBtn setImage:(btnleftImage) forState:(UIControlStateNormal)];
            [rightBtn setImage:(btnrightImage) forState:(UIControlStateNormal)];
            
            //
            
            /// Specials Label
            UILabel *specialsCountLbl = [[UILabel alloc] initWithFrame:CGRectMake((imageV.frame.size.width*0.50)+20, (imageV.frame.size.height*0.70), (imageV.frame.size.width*0.35), (imageV.frame.size.height*0.20))];
            [specialsCountLbl setFont:CalibriBold(19.0)];
            [specialsCountLbl setTextColor:[UIColor colorWithRed:43.0f/255.0f
                                                           green:183.0f/255.0f
                                                            blue:224.0f/255.0f
                                                           alpha:1.0f]];
            [specialsCountLbl setTextAlignment:NSTextAlignmentFromCTTextAlignment(kCTTextAlignmentJustified)];
            NSUInteger specialsCount = 0;
            if(i == 0){
                specialsCount = [_restModal_QuickView.arraySpecials count];
            }
            NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%lu Specials", (unsigned long)specialsCount]];
            [attributeString addAttribute:NSUnderlineStyleAttributeName
                                    value:[NSNumber numberWithInt:1]
                                    range:(NSRange){0,[attributeString length]}];
            [specialsCountLbl setAttributedText:(attributeString)];
            ////// Btn Favourite
            UIButton *favBtn = [[UIButton alloc] initWithFrame:CGRectMake((imageV.frame.size.width*0.95)-40,  (imageV.frame.size.height*0.05),40, 40)];
            UIImage *btnFavImage;
            if(modal.isCheckedFavourite == NO){
                btnFavImage = [UIImage imageNamed:@"fav_unselected.png"];
            }else{
                 btnFavImage = [UIImage imageNamed:@"fav_selected.png"];
            }
            [favBtn setImage:(btnFavImage) forState:(UIControlStateNormal)];
            [favBtn addTarget:self
                       action:@selector(btnFavPressedInCarousal:)
             forControlEvents:UIControlEventTouchUpInside];
            ///////// Btn specials
            UIButton *btnlblspecial = [[UIButton alloc] initWithFrame:CGRectMake((imageV.frame.size.width*0.50)+20, (imageV.frame.size.height*0.70), (imageV.frame.size.width*0.35), (imageV.frame.size.height*0.20))];
            [btnlblspecial setBackgroundColor:[UIColor clearColor]];
            [btnlblspecial addTarget:self
                       action:@selector(openPromotionsViewController:)
             forControlEvents:UIControlEventTouchUpInside];
            ///////// Btn open promotionView Controller
            UIButton *btnopenPromotionViewController = [[UIButton alloc] initWithFrame:CGRectMake((imageV.frame.size.width-50)*0.50, (imageV.frame.size.height*0.05), 50, 50)];
            UIImage *btnViewMoreImage;
            btnViewMoreImage = [UIImage imageNamed:@"ic_keyboard_arrow_up_white_36pt"];
            [btnopenPromotionViewController setImage:btnViewMoreImage forState:UIControlStateNormal];
            [btnopenPromotionViewController addTarget:self
                       action:@selector(openPromotionsViewController:)
             forControlEvents:UIControlEventTouchUpInside];
            /////////
            
            imageV.layer.cornerRadius = 10.0;
            [imageV setBackgroundColor:[UIColor blackColor]];
            imageV.layer.masksToBounds = YES;
            imageV.contentMode = UIViewContentModeScaleAspectFill;
            
            [mainView setBackgroundColor:[UIColor clearColor]];
            
           
            NSString *newUrl = [modal.placeImage stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
            [imageV sd_setImageWithURL:[NSURL URLWithString:newUrl]
                      placeholderImage:[UIImage imageNamed:@"noimage.png"]];
            
            [imageV addSubview:overlayView]; // Overlay at first
            [imageV addSubview:restAddress];
            [imageV addSubview:rating];
            [imageV addSubview:restName];
            [imageV addSubview:leftBtn];
            [imageV addSubview:rightBtn];
            [imageV addSubview:specialsCountLbl];
            [imageV addSubview:favBtn];
            [imageV addSubview:btnopenPromotionViewController];
            [imageV addSubview:btnlblspecial];
            
            UISwipeGestureRecognizer *recognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(openPromotionsViewController:)];
            recognizer.direction = UISwipeGestureRecognizerDirectionUp;
            recognizer.delegate = self;
            [imageV addGestureRecognizer:recognizer];
            
            
            
            [mainView addSubview:imageV];
           
            [_sliderMainScroller addSubview:mainView];
            imageV.clipsToBounds = YES;
            imageV.userInteractionEnabled = YES;
            mainCount++;
        }
        [_sliderMainScroller setBackgroundColor:[UIColor clearColor]];
    }
    
    CGFloat startX = (CGFloat)[resturantScrollArray count] * [UIScreen mainScreen].bounds.size.width;
    [_sliderMainScroller setContentOffset:CGPointMake(startX, 0) animated:NO];
}

#pragma mark - UIScrollView delegate

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    if(scrollView == _sliderMainScroller){
        CGFloat width = scrollView.frame.size.width;
        NSInteger page = (scrollView.contentOffset.x + (0.5f * width)) / width;
        
        NSInteger moveToPage = page;
        if (moveToPage == 0) {
            
            moveToPage = [resturantScrollArray count];
            CGFloat startX = (CGFloat)moveToPage * [UIScreen mainScreen].bounds.size.width;
            [scrollView setContentOffset:CGPointMake(startX, 0) animated:NO];
            
        } else if (moveToPage == (([resturantScrollArray count] * 3) - 1)) {
            
            moveToPage = [resturantScrollArray count] - 1;
            CGFloat startX = (CGFloat)moveToPage * [UIScreen mainScreen].bounds.size.width;
            [scrollView setContentOffset:CGPointMake(startX, 0) animated:NO];
            
        }
        
        if (moveToPage < [resturantScrollArray count]) {
           // pageIndicator.currentPage = moveToPage;
        } else {
            
            moveToPage = moveToPage % [resturantScrollArray count];
            
            restModal_DidSelectMap = [resturantScrollArray objectAtIndex:moveToPage];
            [self specialsServiceHelper];
           // pageIndicator.currentPage = moveToPage;
        }
    }else{
         [self.indicator scrollViewDidEndDecelerating:tblView_Category];
    }
}
- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView {
    if(scrollView == _sliderMainScroller){
        CGFloat width = scrollView.frame.size.width;
        NSInteger page = (scrollView.contentOffset.x + (0.5f * width)) / width;
        
        NSInteger moveToPage = page;
        if (moveToPage == 0) {
            
            moveToPage = [resturantScrollArray count];
            CGFloat startX = (CGFloat)moveToPage * [UIScreen mainScreen].bounds.size.width;
            [scrollView setContentOffset:CGPointMake(startX, 0) animated:NO];
            
        } else if (moveToPage == (([resturantScrollArray count] * 3) - 1)) {
            
            moveToPage = [resturantScrollArray count] - 1;
            CGFloat startX = (CGFloat)moveToPage * [UIScreen mainScreen].bounds.size.width;
            [scrollView setContentOffset:CGPointMake(startX, 0) animated:NO];
            
        }
        
        if (moveToPage < [resturantScrollArray count]) {
            //pageIndicator.currentPage = moveToPage;
        } else {
            
            moveToPage = moveToPage % [resturantScrollArray count];
            //pageIndicator.currentPage = moveToPage;
        }
    }
}

/////// End Scroll View

- (void)viewDidLoad
{
    [super viewDidLoad];
    btn_Filter.hidden = false;
    _strCityOrPlace = [[NSString alloc]init];
    arrPasstoRest = [[NSMutableArray alloc]init];
    lblTotalResult.hidden = true;

//    tableView_DailySpecial.rowHeight = UITableViewAutomaticDimension;
//    tableView_DailySpecial.estimatedRowHeight = 30.0;
    
  // [_collectionVw registerNib:[UINib nibWithNibName:@"BreweriesCollectionCell" bundle: nil] forCellWithReuseIdentifier:@"BreweriesCell"];
    if (appDelegate.IsFromNewHome == YES){
        appDelegate.IsFromNewHome = NO;
        if (![checkVC isEqualToString:@"restlist"])
        {
            [self pressSearchBtn:appDelegate.Modal_PlaceType];
        }

        _cityArray=[APP_DELEGATE.cityArray copy];
        _categoryarray=[APP_DELEGATE.categoryarray copy];
        _strCityOrPlace = APP_DELEGATE.Modal_CityOrArea;
    }
    else{
        lastModal.Tabs = @"";
        _strCityOrPlace = @"";
        GlobalRadius = @"1";
        
            [self searchFromCurrentLocation];
        
    }
    txt_SearchBox.returnKeyType = UIReturnKeyDone;
    btn_QuickViewHappy.tag=0;
    btn_quickViewDaily.tag=1;
    //APP_DELEGATE.tabView.delegate=self;
    quick_mainView.hidden=YES;
    SelectedLat=0.0000;
    SelectedLog =0.0000;
    
    HappyHoursSelected=0;
    DailySpecialSelected=0;
    
    CommanRow=0;
    tempArray   = [NSMutableArray new];
    
    if (!lastModal) {
        lastModal=[ResturantModal new];
    }
    
    [HappHoursTable_lbl setUserInteractionEnabled:NO];
    [DailySpecial_lbl setUserInteractionEnabled:NO];
    
    //HighlightView
    
    HighlightView = [[[NSBundle mainBundle] loadNibNamed:@"HighlightView" owner:self options:nil]firstObject];
    HighlightView.frame = CGRectMake(0, 0, self.view.bounds.size.width, 310);
    quick_mainView = [[[NSBundle mainBundle] loadNibNamed:@"QuickView" owner:self options:nil] firstObject];
    quick_mainView.frame=CGRectMake(0, self.view.bounds.size.height-310, self.view.bounds.size.width, 310);
    btnHighlight.layer.cornerRadius = btnHighlight.frame.size.width/2;
    _btnYellow.layer.cornerRadius = _btnYellow.frame.size.width/2;

    NSDictionary *underlineAttribute = @{NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)};
    btn_ClickWebSite.titleLabel.attributedText = [[NSAttributedString alloc] initWithString:@"Click For Website Specials Page"attributes:underlineAttribute];
    self.viewTool.sectionTitles = @[@"Happy Hours",@"Daily Specials"];
    self.viewTool.selectedSegmentIndex = 0;
    self.viewTool.backgroundColor = [UIColor whiteColor];
    self.viewTool.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor colorWithRed:153.0/255.0 green:153.0/255.0 blue:153.0/255.0 alpha:1.0],NSFontAttributeName: [UIFont systemFontOfSize:14.0]};
    self.viewTool.selectedTitleTextAttributes = @{NSForegroundColorAttributeName : [UIColor colorWithRed:0.0/255.0 green:158.0/255.0 blue:224.0/255.0 alpha:1.0],NSFontAttributeName: [UIFont systemFontOfSize:14.0]};
    self.viewTool.selectionIndicatorColor = [UIColor colorWithRed:0.0/255.0 green:158.0/255.0 blue:224.0/255.0 alpha:1.0];
    self.viewTool.selectionStyle = HMSegmentedControlSelectionStyleFullWidthStripe;
    self.viewTool.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    self.viewTool.selectionIndicatorHeight = 3.0;
    
    [self.viewTool setIndexChangeBlock:^(NSInteger index) {
        if (index == 0){
            if(_restModal_QuickView
               .arrayHappyHours.count>0)
            {
                arraySpecialforday = _restModal_QuickView.arrayHappyHours;
                info_View.hidden=NO;
                view_tableView.hidden = NO;
                view_Empty.hidden = YES;
            }
            else{
                info_View.hidden=YES;
                view_tableView.hidden = YES;
                view_Empty.hidden = NO;
            }
        }
        else{
            if(_restModal_QuickView.arraySpecials.count>0)
            {
                arraySpecialforday = _restModal_QuickView.arraySpecials;
                info_View.hidden=NO;
                view_tableView.hidden = NO;
                view_Empty.hidden = YES;
            }
            else
            {
                info_View.hidden=YES;
                view_tableView.hidden = YES;
                view_Empty.hidden = NO;
            }
        }
        [tableView_DailySpecial reloadData];

    }];

    //GlobalRadius=@"0";
    [[NSUserDefaults standardUserDefaults]setBool:FALSE forKey:@"ischeckedshowquickview"];
    
    [quick_mainView removeFromSuperview];
    [HighlightView removeFromSuperview];
    viewMapQuickDetails.hidden = YES;
    lbl_BtnBar.hidden = YES;
    view_CloseQuick.hidden = TRUE;
    viewMoreButton.hidden = YES;
    
    view_PressTab.hidden = YES;
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    tblView_DailySpecial.allowsSelectionDuringEditing=YES;
    tblView_DailySpecial.tableFooterView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, tblView_DailySpecial.frame.size.width, 0.1f)];
    
    tblView_HappyHours.allowsSelectionDuringEditing=YES;
    tblView_HappyHours.tableFooterView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, tblView_HappyHours.frame.size.width, 0.1f)];
    
    tblView_TopSpots.allowsSelectionDuringEditing=YES;
    tblView_TopSpots.tableFooterView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, tblView_TopSpots.frame.size.width, 0.1f)];
    
    BOOL isCheckedTutorial = [userDefaults boolForKey:@"tutorials"];
    if(isCheckedTutorial == FALSE)
    {
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(pressTutorialImage:)];
        tapGesture.numberOfTapsRequired = 1;
        tapGesture.delegate = self;
        [self.imageView_Tutotial addGestureRecognizer:tapGesture];
        self.imageView_Tutotial.hidden = NO;
        self.imageView_Tutotial.userInteractionEnabled = YES;
        [userDefaults setBool:TRUE forKey:@"tutorials"];
    }
    else
    {
        self.imageView_Tutotial.hidden = YES;
        
    }
    
    selectedView=[[MKAnnotationView alloc]init];
    
    _vwHppyDailly.layer.borderWidth = 1;
    _vwHppyDailly.clipsToBounds = YES;
    _vwHppyDailly.layer.borderColor = [[UIColor lightGrayColor]CGColor];
    
    
  //  _lblDisplayLocations.layer.borderWidth = 1;
    _lblDisplayLocations.clipsToBounds = YES;
   // _lblDisplayLocations.layer.borderColor = [[UIColor lightGrayColor]CGColor];
    
    tblView_Category.layer.borderWidth = 1;
    tblView_Category.layer.cornerRadius = 5;
    tblView_Category.clipsToBounds = YES;
    tblView_Category.layer.borderColor = [[UIColor colorWithRed:234.0/255.0 green:234.0/255.0 blue:234.0/255.0 alpha:1.0]CGColor];
    
    tblView_Category.allowsSelectionDuringEditing=YES;
    tblView_Category.tableFooterView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, tblView_Category.frame.size.width, 0.1f)];
    
    [quick_mainView removeFromSuperview];
    [HighlightView removeFromSuperview];
    viewMapQuickDetails.hidden = YES;
    lbl_BtnBar.hidden = YES;
    view_CloseQuick.hidden = TRUE;
    viewMoreButton.hidden = YES;
    view_PressTab.hidden = YES;
    tblView_Category.hidden = YES;
    
    resturantModalObj   =   [[ResturantModal alloc]init];
    resturantArray      =   [[NSMutableArray alloc]init];
    array_weekDays      =   [[NSMutableArray alloc]init];
    array_TopSpots      =   [[NSMutableArray alloc]init];
    
    array_SelectedDailySpecial  =   [[NSMutableArray alloc]init];
    array_SelectedHappyHours    =   [[NSMutableArray alloc]init];
    array_SelectedTopSpots      =   [[NSMutableArray alloc]init];
    
    registerModalResponeValue   =   [[RegisterModal alloc]init];
    
    NSData *data = [userDefaults objectForKey:@"logindetails"];
    registerModalResponeValue = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    user_IDRegister = registerModalResponeValue.userID;
    restModal_DidSelectMap = [[ResturantModal alloc]init];
    txt_SearchBox.text = @"";
    txt_SearchBox.clearButtonMode = UITextFieldViewModeWhileEditing;
    [mapView removeAnnotations:mapView.annotations];
    //[array_weekDays addObject:@"View All"];
    [array_weekDays addObject:@"Sunday"];
    [array_weekDays addObject:@"Monday"];
    [array_weekDays addObject:@"Tuesday"];
    [array_weekDays addObject:@"Wednesday"];
    [array_weekDays addObject:@"Thursday"];
    [array_weekDays addObject:@"Friday"];
    [array_weekDays addObject:@"Saturday"];
    btn_Daily.selected = NO;
    btn_HappyHours.selected = NO;
    btn_ViewMap.selected = NO;
    
    
    //Delete
    [quick_mainView removeFromSuperview];
    [HighlightView removeFromSuperview];
    viewMapQuickDetails.hidden = YES;
    lbl_BtnBar.hidden = YES;
    view_CloseQuick.hidden = TRUE;
    viewMoreButton.hidden = YES;
    view_PressTab.hidden = YES;
    tblView_Category.hidden = YES;
    
    MapListBoolean=true;
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if (!_indicator) {
        self.indicator = [[JTSScrollIndicator alloc] initWithScrollView:tblView_Category];
        self.indicator.backgroundColor = [UIColor lightGrayColor];
        tblView_Category.contentOffset = CGPointMake(0.0, 1.0);
        [_indicator show:YES];
    }
    [self addCityArray];
}
- (void)viewWillAppear:(BOOL)animated
{
    
    self.strNE_Lat = [[NSString alloc]init];
    self.strNE_Long = [[NSString alloc]init];
    self.strSW_Lat = [[NSString alloc]init];
    self.strSW_Long = [[NSString alloc]init];
    
    tableView_DailySpecial.rowHeight = UITableViewAutomaticDimension;
    tableView_DailySpecial.estimatedRowHeight = 50;
    //[CUSTOMINDICATORMACRO stopLoadAnimation:self];
    btnResLike.userInteractionEnabled = YES;
    btnResDislike.userInteractionEnabled = YES;
    isKeepDraging = YES;
    btn_Filter.hidden = false;
    if (appDelegate.IsFromNewHome == YES) {
        appDelegate.IsFromNewHome = NO;
       [self pressSearchBtn:appDelegate.Modal_PlaceType];
        _cityArray=[APP_DELEGATE.cityArray copy];
        _categoryarray=[APP_DELEGATE.categoryarray copy];
    }
    tblView_Category.rowHeight = UITableViewAutomaticDimension;
    tblView_Category.estimatedRowHeight = 20;
    btnClearFilter.hidden = true;
    [super viewWillAppear:YES];
    self.navigationController.navigationBar.hidden = true;
    
    MenuTapBtn.hidden=YES;
    
    view_Filter.hidden = true;
    
    btn_FilterDone.layer.cornerRadius = 5;
    btn_FilterReset.layer.cornerRadius = 5;
    
    HappyHourseBool_lbl=TRUE;
    DailySpecialBool_lbl =TRUE;
    
    //Hiren
    
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"ischeckedshowquickview"];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    [defaults setObject:@"" forKey:@"Classname"];
    
    [defaults synchronize];
    
    if (selectedAnnnotationImage!=nil) {
        [mapView deselectAnnotation:selectedAnnotation animated:NO];
    }
    
    isCheckedLazyLoading = FALSE;
    isCheckedWhichCoordinate = FALSE;
    
    [self getEventsCategory];
    
    if(checkWillAppear == nil || checkWillAppear.length == 0)
    {
        if([checkVC isEqualToString:@"listplaces"])
        {
            _firstTimeLoad=YES;
            List_Btn.hidden=YES;
            manuallySearchLocation=YES;
            view_Btn.hidden = YES;
            view_Location.hidden = YES;
            backButton.hidden = NO;
            [quick_mainView removeFromSuperview];
            [HighlightView removeFromSuperview];
            lbl_TopView.text = @"NEAR BY PLACES";
            lbl_TopView.hidden = NO;
            txt_SearchBox.hidden = YES;
            searchButton.hidden = YES;
            bottomView.hidden = YES;
            homeButton.hidden = YES;
            viewMoreButton.hidden = YES;
            mapView_bottomSpace.constant = 0;
            quickView_bottomSpace.constant = 0;
            lblFilter.hidden=YES;
            lblListView.hidden=YES;
            GlobalRadius=@"";
            
            [self loadMapValuesServiceHelper:nearByRestModal];
        }
        else if([checkVC isEqualToString:@"restlist"])
        {
            
            List_Btn.hidden=YES;
            
            view_Btn.hidden = YES;
            view_Location.hidden = YES;
            backButton.hidden = NO;
            
            [quick_mainView removeFromSuperview];
            [HighlightView removeFromSuperview];
            _lblDisplayLocations.text=@"   1 Location Displaying";
            lblFilter.hidden=YES;
            lblListView.hidden=YES;
            lbl_TopView.text = @"PLACES";
            lbl_TopView.hidden = NO;
            txt_SearchBox.hidden = YES;
            searchButton.hidden = YES;
            bottomView.hidden = YES;
            homeButton.hidden = YES;
            viewMoreButton.hidden = YES;
            mapView_bottomSpace.constant = 0;
            quickView_bottomSpace.constant = 0;
            viewMapQuickDetails.hidden = YES;
            lbl_BtnBar.hidden = YES;
            view_CloseQuick.hidden = TRUE;
            viewMoreButton.hidden = YES;
            view_Btn.hidden = YES;
            view_Location.hidden = YES;
            
            imageView_Like.hidden = NO;
            imageView_Unlike.hidden = NO;
            [resturantArray removeAllObjects];
            [resturantArray addObjectsFromArray:restListArray];
            [self loadLocationInMap];
            
            
            if([restVCString isEqualToString:@"viewmap"])
            {
                lbl_TopView.text = @"PLACES";
            }
            else if ([restVCString isEqualToString:@"singlemap"])
            {
                
                NSLog(@"%@",resturantArray);
                
                ResturantModal *modal = [resturantArray firstObject];
                
                NSLog(@"%@",modal.placeID);
                
                placeidSub=modal.placeID;
                
                [self addQuickView];
                viewMapQuickDetails.hidden = FALSE;
                
                
                lbl_BtnBar.hidden = NO;
                view_CloseQuick.hidden = NO;
                viewMoreButton.hidden = FALSE;
                isShowQuickView = TRUE;
                restModal_DidSelectMap = [resturantArray firstObject];
                [self specialsHappyHoursServiceHelper];
                [self loadDataForQuickViewInSort:restModal_DidSelectMap];
                lbl_TopView.text = restModal_DidSelectMap.resturantName.uppercaseString;
            }
            else
            {
                lbl_TopView.text = @"PLACES";
            }
        }
        else
        {
            [[NSUserDefaults standardUserDefaults]setBool:TRUE forKey:@"checkmap"];
            isCheckedQuickView = [[NSUserDefaults standardUserDefaults]boolForKey:@"ischeckedshowquickview"];
            BOOL isCheckedRandomSearch = [[NSUserDefaults standardUserDefaults]boolForKey:@"randomsearch"];
            
            if(isCheckedRandomSearch == TRUE)
            {
                if(isCheckedQuickView == TRUE)
                {
                    if(searchAreaPlaceString.length>0)
                    {
                        [mapView removeAnnotations:mapView.annotations];
                        [promotionModalOBJ setUserID:user_IDRegister];
                        [promotionModalOBJ setResturantLatitude:stringLat];
                        [promotionModalOBJ setResturantLongtitude:stringLong];
                        [promotionModalOBJ setSystemMessage:searchAreaPlaceString];
                        [self loadMapValuesServiceHelper:promotionModalOBJ];
                    }
                    else
                    {
                        [mapView removeAnnotations:mapView.annotations];
                        [promotionModalOBJ setUserID:user_IDRegister];
                        [promotionModalOBJ setResturantLatitude:stringLat];
                        [promotionModalOBJ setResturantLongtitude:stringLong];
                        [self loadMapValuesServiceHelper:promotionModalOBJ];
                    }
                    
                }
                
            }
            else
            {
                if(txt_SearchBox.text.length>0)
                {
                    if(isCheckedQuickView == TRUE)
                    {
                        [mapView removeAnnotations:mapView.annotations];
                        [promotionModalOBJ setUserID:user_IDRegister];
                        [promotionModalOBJ setSystemMessage:SearchFixedPlace?@"":[GlobalRadius isEqualToString:@""]?txt_SearchBox.text:@""];
                        [self loadMapValuesServiceHelper:promotionModalOBJ];
                    }
                }
                else
                {
                    if(isCheckedQuickView == TRUE)
                    {
                        [mapView removeAnnotations:mapView.annotations];
                        [self CurrentLocationIdentifier];
                    }
                    
                }
                
            }
            //[self addQuickView];
            backButton.hidden = YES;
            lbl_TopView.hidden = YES;
            if(IS_IPHONE_6PLUS)
            {
                mapView_bottomSpace.constant = 65;
                quickView_bottomSpace.constant = 65;
                
            }
            else if (IS_IPHONE_6)
            {
                mapView_bottomSpace.constant = 55;
                quickView_bottomSpace.constant = 55;
                
            }
            else
            {
                mapView_bottomSpace.constant = 45;
                quickView_bottomSpace.constant = 45;
                
            }
        }
        
        isShow = FALSE;
        tap = [[UITapGestureRecognizer alloc]
               initWithTarget:self
               action:@selector(dismissKeyboard)];
        
        [view_PressTab addGestureRecognizer:tap];
        tap.delegate = self;
        
    }
    else
    {
        checkWillAppear = @"";
    }
    
    //    btnSundayDaily.selected = false;
    //    btnMondayDaily.selected = false;
    //    btnTuesdayDaily.selected = false;
    //    btnWednesdayDaily.selected = false;
    //    btnThuesdayDaily.selected = false;
    //    btnFridayDaily.selected = false;
    //    btnSaturdayDaily.selected = false;
    //
    //    btnSundayHappy.selected = false;
    //    btnMondayHappy.selected = false;
    //    btnTuesdayHappy.selected = false;
    //    btnWednesdayHappy.selected = false;
    //    btnThuesdayHappy.selected = false;
    //    btnFridayHappy.selected = false;
    //    btnSaturdayHappy.selected = false;
    //
    //    [array_SelectedDailySpecial removeAllObjects];
    //    [array_SelectedHappyHours removeAllObjects];
    //    [array_SelectedTopSpots removeAllObjects];
    //
    //    [tblView_TopSpots reloadData];
}

#pragma mark Scroll View Methods
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [self.indicator scrollViewDidScroll:tblView_Category];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    [self.indicator scrollViewDidEndDragging:tblView_Category willDecelerate:decelerate];
}



- (void)scrollViewWillScrollToTop:(UIScrollView *)scrollView {
    [self.indicator scrollViewWillScrollToTop:tblView_Category];
}

- (void)scrollViewDidScrollToTop:(UIScrollView *)scrollView {
    [self.indicator scrollViewDidScrollToTop:tblView_Category];
}


-(void)pressTutorialImage :(UITapGestureRecognizer *)tapRecognizer
{
    self.imageView_Tutotial.hidden = YES;
}

#pragma mark - GetCity And Category List
-(void)addCityArray{
    
    BOOL check=[[AppDelegate shareAppdelegateInstance] checkReachability];
    
    if (check) {
        if (_cityArray.count==0) {
            [CUSTOMINDICATORMACRO startLoadAnimation:self];
            [self getCityListServiceHelper];
        }
    }
}
-(void)getCityListServiceHelper{
    BOOL check=[[AppDelegate shareAppdelegateInstance] checkReachability];
    if(check){
        [REGISTERMACRO listallCityServer:nil withCompletion:^(id obj1) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                if (obj1!=nil) {
                    if([obj1 isKindOfClass:[NSArray class]] || [obj1 isKindOfClass:[NSMutableArray class]]){
                        _cityArray = obj1;
                        [self getCategoryListServiceHelper];
                        // [self completeTask];
                        NSLog(@"cityList");
                    }
                    else
                        [self showCheckYourConnection];
                }
                else
                    [self showCheckYourConnection];
            });
        }];
    }
}

-(void)showCheckYourConnection{
    dispatch_async(dispatch_get_main_queue(), ^{
        [CUSTOMINDICATORMACRO stopLoadAnimation:self];
    });
    
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                    message:@"Please check your connection"
                                                   delegate:self
                                          cancelButtonTitle:@"Ok"
                                          otherButtonTitles:nil];
    [alert show];
}

-(void)getCategoryListServiceHelper
{
    BOOL check=[[AppDelegate shareAppdelegateInstance] checkReachability];
    if(check)
    {
        [REGISTERMACRO listallCategoryServer:nil withCompletion:^(id obj1) {
            if (obj1!=nil) {
                if([obj1 isKindOfClass:[NSArray class]] || [obj1 isKindOfClass:[NSMutableArray class]] || [obj1 isKindOfClass:[NSDictionary class]]){
                    _categoryarray=obj1;
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [CUSTOMINDICATORMACRO stopLoadAnimation:self];
                    });
                    //[self completeTask];
                    NSLog(@"categoryList");
                }
                else
                    [self showCheckYourConnection];
            }else
                [self showCheckYourConnection];
            
        }];
    }
    
}

#pragma mark - Getting Current Location

-(void)CurrentLocationIdentifier
{
    //---- For getting current gps location
    locationManager = [CLLocationManager new];
    locationManager.delegate = self;
    [locationManager requestAlwaysAuthorization];
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [locationManager startUpdatingLocation];
    mapView.showsUserLocation = YES;
    //------
}
#pragma mark - CollectionView Delegate Methods

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section;
{
    return ArrHighLight.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath;
{
    [_Collection_Highlight registerNib:[UINib nibWithNibName:@"HighlightCell" bundle:nil] forCellWithReuseIdentifier:@"HighlightCell"];

    HighlightCell *cell = [_Collection_Highlight dequeueReusableCellWithReuseIdentifier:@"HighlightCell" forIndexPath:indexPath];
    ResturantModal *modal = [ArrHighLight objectAtIndex:indexPath.item];
    
    cell.lblHighlights.text = modal.HighlightName;
    
    if ([modal.Highlight_Status isEqualToString:@"1"]) {
        cell.imgHighlights.image = [UIImage imageNamed:@"check-blue"];
    
    }
    else{
        cell.imgHighlights.image = [UIImage imageNamed:@"uncheck-blue"];
    }
    return cell;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    float cellWidth =screenRect.size.width/2;
    // float cellHeight = screenRect.size.height/3.5-10;
    float cellHeight=55;
    CGSize size = CGSizeMake(cellWidth,cellHeight);
    return size;
}

#pragma  mark - TableView Delegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView == tblView_DailySpecial || tableView == tblView_HappyHours)
    {
        return array_weekDays.count;
    }
    else if (tableView == tblView_TopSpots){
        return array_TopSpots.count;
    }
    else if(tableView == tableView_DailySpecial)
    {
        return arraySpecialforday.count;
    }
    else
    {
        return tempArray.count;
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == tblView_DailySpecial || tableView == tblView_HappyHours)
    {
        
        filterCell * cell = [tableView dequeueReusableCellWithIdentifier:@"filterCell"];
        if (!cell){
            NSArray * arrNib = [[NSBundle mainBundle ]loadNibNamed:@"filterCell" owner:self options:nil];
            cell = (filterCell *)arrNib[0];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        //        static NSString *cellIdentifier = @"identifier";
        //        UITableViewCell *cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        //        if(cell == nil)
        //        {
        //            cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        //        }
        if(array_weekDays.count>0)
        {
            if(tableView == tblView_DailySpecial){
                //[cell.checkbox addTarget:self action:@selector(checkboxDidChangeForDailySpecial:) forControlEvents:UIControlEventValueChanged];
            }
            else{
                //[cell.checkbox addTarget:self action:@selector(checkboxDidChangeForHappyHours:) forControlEvents:UIControlEventValueChanged];
            }
            //cell.checkbox.tag = indexPath.row;
            //cell.checkbox.checked = false;
            cell.lblText.text = [array_weekDays objectAtIndex:indexPath.row];
            
            //            if (CommanRow==indexPath.row)
            //            {
            //                [Cell.checkbox addTarget:self action:@selector(checkboxDidChange:) forControlEvents:UIControlEventValueChanged];
            //                Cell.checkbox.textLabel.text = @"Label text";
            //                cell.textLabel.text = [array_weekDays objectAtIndex:indexPath.row];
            //                cell.textLabel.textAlignment = NSTextAlignmentCenter;
            //                cell.textLabel.textColor = [UIColor whiteColor];
            //                cell.textLabel.font = [UIFont systemFontOfSize:14.0];
            //                cell.backgroundColor=[UIColor colorWithRed:0.0/255.0f green:204.0/255.0f blue:255.0/255.0f alpha:1];
            //
            //            }else
            //            {
            //
            //
            //                cell.textLabel.text = [array_weekDays objectAtIndex:indexPath.row];
            //                cell.textLabel.textAlignment = NSTextAlignmentCenter;
            //                cell.textLabel.textColor = [UIColor darkGrayColor];
            //                cell.textLabel.font = [UIFont systemFontOfSize:13.0];
            //                cell.backgroundColor=[UIColor whiteColor];
            //            }
            
            
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    else if (tableView == tableView_DailySpecial)
    {
        static NSString *cellIdentifier = @"sampleidentifier";
        DailySpecialCell *cell = (DailySpecialCell *)[tableView_DailySpecial dequeueReusableCellWithIdentifier:cellIdentifier];
        
        if(cell == nil)
        {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"DailySpecialCell" owner:self options:nil]objectAtIndex:0];
        }
        ResturantModal *modelDailySpecialDetails = [arraySpecialforday objectAtIndex:indexPath.row];
        
        NSString *mystr;
        
        if([modelDailySpecialDetails.specials_weekDay isEqualToString:@"monday"])
        {
            mystr=@"Monday";
        }
        else if ([modelDailySpecialDetails.specials_weekDay isEqualToString:@"tuesday"])
        {
            mystr=@"Tuesday";
        }
        else if ([modelDailySpecialDetails.specials_weekDay isEqualToString:@"wednesday"])
        {
            mystr=@"Wednesday";
        }
        else if ([modelDailySpecialDetails.specials_weekDay isEqualToString:@"thursday"])
        {
            mystr=@"Thursday";
        }
        else if ([modelDailySpecialDetails.specials_weekDay isEqualToString:@"friday"])
        {
            mystr=@"Friday";
        }
        else if ([modelDailySpecialDetails.specials_weekDay isEqualToString:@"saturday"])
        {
            mystr=@"Saturday";
        }
        else
        {
            mystr=@"Sunday";
        }
        cell.lblDescription.numberOfLines = 0;
        cell.lblDescription.lineBreakMode = NSLineBreakByWordWrapping;
        
        
        NSString *trimmedString = [modelDailySpecialDetails.title stringByTrimmingCharactersInSet:
                                   [NSCharacterSet whitespaceCharacterSet]];
        
        NSString *stringStartTime;
        
        NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
        NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
        [dateFormatter1 setLocale:locale];

        dateFormatter1.dateFormat = @"HH:mm:ss";
        
        NSDate *date1 = [dateFormatter1 dateFromString:modelDailySpecialDetails.startTime];
        
        
        
        dateFormatter1.dateFormat = @"hh:mm a";
        
        stringStartTime = [dateFormatter1 stringFromDate:date1];
        
        stringStartTime = [stringStartTime stringByReplacingOccurrencesOfString:@":00" withString:@""];
        
        
        int stringStartTimeInt=[stringStartTime intValue];
        
        if (stringStartTimeInt<10)
        {
            NSRange range = NSMakeRange(0,1);
            stringStartTime = [stringStartTime stringByReplacingCharactersInRange:range withString:@""];
        }
        else
        {
            stringStartTime = [stringStartTime stringByReplacingOccurrencesOfString:@":00" withString:@""];
        }
        
        
        NSString *stringCloseTime;
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        NSLocale *locale2 = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
        [dateFormatter setLocale:locale2];

        dateFormatter.dateFormat = @"HH:mm:ss";
        
        NSDate *date = [dateFormatter dateFromString:modelDailySpecialDetails.endTime];
        
        
        
        dateFormatter.dateFormat = @"hh:mm a";
        
        stringCloseTime = [dateFormatter stringFromDate:date];
        
        stringCloseTime = [stringCloseTime stringByReplacingOccurrencesOfString:@":00" withString:@""];
        
        
        int stringCloseTimeInt=[stringCloseTime intValue];
        
        if (stringCloseTimeInt<10)
        {
            NSRange range = NSMakeRange(0,1);
            
            stringCloseTime = [stringCloseTime stringByReplacingCharactersInRange:range withString:@""];
        }
        else
        {
            stringCloseTime = [stringCloseTime stringByReplacingOccurrencesOfString:@":00" withString:@""];
        }
        
        
        if ([modelDailySpecialDetails.price isEqualToString:@"N/A"] || [modelDailySpecialDetails.price isEqualToString:@"1/2 Off"] || [modelDailySpecialDetails.price isEqualToString:@"Other"] || [modelDailySpecialDetails.price isEqualToString:@"FREE"])
        {
            
            cell.lblDays.text=mystr;
            NSString *completeString = [NSString stringWithFormat:@"%@ %@ (%@-%@)",modelDailySpecialDetails.price,trimmedString,stringStartTime,stringCloseTime];
            NSMutableAttributedString *AttributedString = [[NSMutableAttributedString alloc] initWithString:completeString];
            NSString *boldString =modelDailySpecialDetails.price;
            NSRange boldRange = [completeString rangeOfString:boldString];
            [AttributedString addAttribute: NSFontAttributeName value:CalibriBold(11.0f) range:boldRange];
            [cell.lblDescription setAttributedText: AttributedString];
        }
        else
        {
            cell.lblDays.text=mystr;
            NSString *price;
            if ([modelDailySpecialDetails.price rangeOfString:@"$"].location == NSNotFound) {
                price = [NSString stringWithFormat:@"$%@",modelDailySpecialDetails.price];
            }else
            {
                price = [NSString stringWithFormat:@"%@",modelDailySpecialDetails.price];
            }
            
            NSString *completeString = [NSString stringWithFormat:@"%@ %@ (%@-%@)",price, trimmedString,stringStartTime,stringCloseTime];
            NSMutableAttributedString *AttributedString = [[NSMutableAttributedString alloc] initWithString:completeString];
            [cell.lblDescription setAttributedText: AttributedString];
        }
        
        if(modelDailySpecialDetails.isCheckedFavSpecial == NO)
        {
            cell.btnFavSpecial.selected = false;
            //cell.btnFavWidthCons.constant = 0;
        }
        else{
            cell.btnFavSpecial.selected = true;
            //cell.btnFavWidthCons.constant = 0;
        }
        [cell.btnFavSpecial addTarget:self action:@selector(btnFavSpecialClick:) forControlEvents:UIControlEventTouchUpInside];
        cell.btnFavSpecial.tag = indexPath.row;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return cell;
    }
    
    else if (tableView == tblView_TopSpots){
        filterCell * cell = [tableView dequeueReusableCellWithIdentifier:@"filterCell"];
        if (!cell){
            NSArray * arrNib = [[NSBundle mainBundle ]loadNibNamed:@"filterCell" owner:self options:nil];
            cell = (filterCell *)arrNib[0];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        
        if(array_TopSpots.count>0)
        {
            EventsModel *modal = [array_TopSpots objectAtIndex:indexPath.row];
            
            //            [cell.checkbox addTarget:self action:@selector(checkboxDidChangeForTopSpot:) forControlEvents:UIControlEventValueChanged];
            //            cell.checkbox.tag = indexPath.row;
            //            cell.checkbox.checked = false;
            cell.lblText.text = modal.categoryName;
            cell.lblText.textColor = [UIColor colorWithRed:73.0/255.0 green:73.0/255.0 blue:73.0/255.0 alpha:1.0];
            
            if (![array_SelectedTopSpots containsObject:modal.categoryID]) {
                [cell.imgCheckBox setHighlighted:false];
                cell.btnCell.selected = false;
            }
            else{
                [cell.imgCheckBox setHighlighted:true];
                cell.btnCell.selected = true;
            }
            
            cell.imgCheckBox.backgroundColor = [UIColor greenColor];
            cell.btnCell.tag = indexPath.row;
            [cell.btnCell addTarget:self action:@selector(btnCkeckBoxClick:) forControlEvents:UIControlEventTouchUpInside];
        }
        
        return cell;
    }
    else
    {
        static NSString *cellIdentifier = @"identifier";
        UITableViewCell *cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
        if(cell == nil)
        {
            cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        }
        if(_categoryarray.count>0 || _cityArray.count>0 )
        {
            RegisterModal *modal = [tempArray objectAtIndex:indexPath.row];
            cell.textLabel.text = modal.placeType;
            cell.detailTextLabel.numberOfLines = 1;
            if ([modal.CityorArea isEqualToString:@"City"] || [modal.CityorArea isEqualToString:@"Area"])
            {
                // cell.detailTextLabel.text= [NSString stringWithFormat:@"%@",modal.address1];
            }else{
                 cell.detailTextLabel.text= [NSString stringWithFormat:@"%@, %@, IL",modal.address1,modal.city];
            }
           //modal.address1;
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == tblView_DailySpecial || tableView == tblView_HappyHours || tableView == tblView_TopSpots)
    {
        return 30;
    }
    else if (tableView == tableView_DailySpecial)
    {
        ResturantModal *modelDailySpecialDetails = [arraySpecialforday objectAtIndex:indexPath.row];
        int intDescHeight = [self calculateDailySpecialDescHeight : modelDailySpecialDetails];
         return UITableViewAutomaticDimension;
       // return UITableViewAutomaticDimension;
    }
    
    else
    {
        RegisterModal *modal = [tempArray objectAtIndex:indexPath.row];

        if ([modal.CityorArea isEqualToString:@"City"] || [modal.CityorArea isEqualToString:@"Area"])
        {
            return 44;
        }
        else
        {
            return 44;
        }
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == tblView_DailySpecial)
    {
        
    }
    else if (tableView == tblView_HappyHours){
        
    }
    else if (tableView == tblView_TopSpots){
        filterCell *cell = [tblView_TopSpots cellForRowAtIndexPath:indexPath];
        
    }
    else
    {
        tblView_Category.hidden = YES;
        RegisterModal *modal = [tempArray objectAtIndex:indexPath.row];
        NSRange range=[modal.placeType rangeOfString:@", IL" options:(NSCaseInsensitiveSearch)];
        NSRange range1=[modal.placeType rangeOfString:@", Chicago" options:(NSCaseInsensitiveSearch)];
        
        GlobalRadius=@"";
        
        if(range.location != NSNotFound || range1.location != NSNotFound)
        {
            _isCheckedCityStatus = TRUE;
        }
        else if([modal.statusCity isEqualToString:@"City"])
        {
            _isCheckedCityStatus = TRUE;
        }
        else
        {
            _isCheckedCityStatus = FALSE;
            placeidSub=modal.placeID;
        }
        self.strSW_Lat = @"";
        tableCellSelected=YES;
        _strCityOrPlace = modal.CityorArea;
        txt_SearchBox.text = modal.placeType;
        [txt_SearchBox resignFirstResponder];
        
        [self textFieldShouldReturn: txt_SearchBox];
    }
    //    if(tableView == tblView_DailySpecial)
    //    {
    //        tblView_DailySpecial.hidden = YES;
    //        if(isSelectedValueHappy == TRUE)
    //        {
    //            string_HappyHours = [array_weekDays objectAtIndex:indexPath.row];
    //            isSelectedValueHappy = FALSE;
    //
    //            HappyHoursSelected=indexPath.row;
    //
    //        }
    //        if(isSelectedValueSspecials == TRUE)
    //        {
    //            string_DailySpecials = [array_weekDays objectAtIndex:indexPath.row];
    //            isSelectedValueSspecials = FALSE;
    //            DailySpecialSelected=indexPath.row;
    //        }
    //
    //        if(HappyHourseBool_lbl == FALSE)
    //        {
    //            string_HappyHours = [array_weekDays objectAtIndex:indexPath.row];
    //            HappyHoursSelected=indexPath.row;
    //            HappyHourseBool_lbl=TRUE;
    //        }
    //
    //        if(DailySpecialBool_lbl == FALSE)
    //        {
    //            string_DailySpecials = [array_weekDays objectAtIndex:indexPath.row];
    //
    //            DailySpecialSelected=indexPath.row;
    //
    //            DailySpecialBool_lbl=TRUE;
    //        }
    //
    //
    //
    //        ResturantModal *modal = [[ResturantModal alloc]init];
    //        [modal setUserID:user_IDRegister];
    //        [modal setResturantLatitude:stringLat];
    //        [modal setResturantLongtitude:stringLong];
    //        [modal setSystemMessage:SearchFixedPlace?@"":[GlobalRadius isEqualToString:@"0"]?txt_SearchBox.text:@""];
    //
    //        [self loadMapValuesServiceHelper:modal];
    //
    //    }
    //    else if (tableView == tableView_DailySpecial)
    //    {
    //
    //    }
}

- (void)btnCkeckBoxClick:(id)sender {
    UIButton *button = (UIButton*)sender;
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:button.tag inSection:0];
    filterCell *cell = (filterCell *)[tblView_TopSpots cellForRowAtIndexPath:indexPath];
    
    EventsModel *modal = [array_TopSpots objectAtIndex:indexPath.row];
    
    if(!button.selected){
        button.selected = true;
        if (![array_SelectedTopSpots containsObject:modal.categoryID]) {
            [array_SelectedTopSpots addObject:modal.categoryID];
            [cell.imgCheckBox setHighlighted:true];
        }
    }
    else{
        button.selected = false;
        if ([array_SelectedTopSpots containsObject:modal.categoryID]) {
            [array_SelectedTopSpots removeObject:modal.categoryID ];
            [cell.imgCheckBox setHighlighted:false];
        }
    }
}

-(IBAction)btnDailySpecialClick:(id)sender
{
    UIButton *button = (UIButton*)sender;
    if(button.isSelected == true){
        button.selected = false;
        if ([array_SelectedDailySpecial containsObject:button.titleLabel.text]) {
            [array_SelectedDailySpecial removeObject:button.titleLabel.text];
        }
    }
    else{
        button.selected = true;
        if (![array_SelectedDailySpecial containsObject:button.titleLabel.text]) {
            [array_SelectedDailySpecial addObject:button.titleLabel.text];
        }
    }
}

-(IBAction)btnHappyHoursClick:(id)sender
{
    UIButton *button = (UIButton*)sender;
    if(button.isSelected == true){
        button.selected = false;
        if ([array_SelectedHappyHours containsObject:button.titleLabel.text]) {
            [array_SelectedHappyHours removeObject:button.titleLabel.text];
        }
    }
    else{
        button.selected = true;
        if (![array_SelectedHappyHours containsObject:button.titleLabel.text]) {
            [array_SelectedHappyHours addObject:button.titleLabel.text];
        }
    }
}

/*
 - (void)checkboxDidChangeForDailySpecial:(CTCheckbox *)checkbox
 {
 NSIndexPath *indexPath = [NSIndexPath indexPathForRow:checkbox.tag inSection:0];
 filterCell *cell = (filterCell *)[tblView_DailySpecial cellForRowAtIndexPath:indexPath];
 
 if(checkbox.checked){
 if (![array_SelectedDailySpecial containsObject:cell.lblText.text]) {
 [array_SelectedDailySpecial addObject:cell.lblText.text];
 }
 }
 else{
 if ([array_SelectedDailySpecial containsObject:cell.lblText.text]) {
 [array_SelectedDailySpecial removeObject:cell.lblText.text];
 }
 }
 }
 
 - (void)checkboxDidChangeForHappyHours:(CTCheckbox *)checkbox
 {
 NSIndexPath *indexPath = [NSIndexPath indexPathForRow:checkbox.tag inSection:0];
 filterCell *cell = (filterCell *)[tblView_HappyHours cellForRowAtIndexPath:indexPath];
 
 if(checkbox.checked){
 if (![array_SelectedHappyHours containsObject:cell.lblText.text]) {
 [array_SelectedHappyHours addObject:cell.lblText.text];
 }
 }
 else{
 if ([array_SelectedHappyHours containsObject:cell.lblText.text]) {
 [array_SelectedHappyHours removeObject:cell.lblText.text];
 }
 }
 }
 
 - (void)checkboxDidChangeForTopSpot:(CTCheckbox *)checkbox
 {
 NSIndexPath *indexPath = [NSIndexPath indexPathForRow:checkbox.tag inSection:0];
 filterCell *cell = (filterCell *)[tblView_TopSpots cellForRowAtIndexPath:indexPath];
 
 EventsModel *modal = [array_TopSpots objectAtIndex:indexPath.row];
 
 if(checkbox.checked){
 if (![array_SelectedTopSpots containsObject:modal.categoryID]) {
 [array_SelectedTopSpots addObject:modal.categoryID];
 }
 }
 else{
 if ([array_SelectedTopSpots containsObject:modal.categoryID]) {
 [array_SelectedTopSpots removeObject:modal.categoryID ];
 }
 }
 }
 */

#pragma mark -- Calculate Daily Special Cell Height
-(int) calculateDailySpecialDescHeight : (ResturantModal *) modelDailySpecialDetails
{
    NSString *trimmedString = [modelDailySpecialDetails.title stringByTrimmingCharactersInSet:
                               [NSCharacterSet whitespaceCharacterSet]];
    
    NSString *stringStartTime;
    
    NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [dateFormatter1 setLocale:locale];

    dateFormatter1.dateFormat = @"HH:mm:ss";
    
    NSDate *date1 = [dateFormatter1 dateFromString:modelDailySpecialDetails.startTime];
    
    
    
    dateFormatter1.dateFormat = @"hh:mm a";
    
    stringStartTime = [dateFormatter1 stringFromDate:date1];
    
    stringStartTime = [stringStartTime stringByReplacingOccurrencesOfString:@":00" withString:@""];
    
    
    int stringStartTimeInt=[stringStartTime intValue];
    
    if (stringStartTimeInt<10)
    {
        NSRange range = NSMakeRange(0,1);
        stringStartTime = [stringStartTime stringByReplacingCharactersInRange:range withString:@""];
    }
    else
    {
        stringStartTime = [stringStartTime stringByReplacingOccurrencesOfString:@":00" withString:@""];
    }
    
    
    NSString *stringCloseTime;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSLocale *locale2 = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [dateFormatter setLocale:locale2];

    dateFormatter.dateFormat = @"HH:mm:ss";
    
    NSDate *date = [dateFormatter dateFromString:modelDailySpecialDetails.endTime];
    
    dateFormatter.dateFormat = @"hh:mm a";
    
    stringCloseTime = [dateFormatter stringFromDate:date];
    
    stringCloseTime = [stringCloseTime stringByReplacingOccurrencesOfString:@":00" withString:@""];
    
    
    
    int stringCloseTimeInt=[stringCloseTime intValue];
    
    if (stringCloseTimeInt<10)
    {
        NSRange range = NSMakeRange(0,1);
        
        stringCloseTime = [stringCloseTime stringByReplacingCharactersInRange:range withString:@""];
    }
    else
    {
        stringCloseTime = [stringCloseTime stringByReplacingOccurrencesOfString:@":00" withString:@""];
    }
    
    
    if ([modelDailySpecialDetails.price isEqualToString:@"N/A"] || [modelDailySpecialDetails.price isEqualToString:@"1/2 Off"] || [modelDailySpecialDetails.price isEqualToString:@"Other"] || [modelDailySpecialDetails.price isEqualToString:@"FREE"])
    {
        NSString *completeString = [NSString stringWithFormat:@"%@ %@ (%@-%@)",modelDailySpecialDetails.price,trimmedString,stringStartTime,stringCloseTime];
        NSMutableAttributedString *AttributedString = [[NSMutableAttributedString alloc] initWithString:completeString];
        [AttributedString addAttribute:NSFontAttributeName value:Calibri(12) range:NSMakeRange(0, completeString.length-1)];
        [AttributedString addAttribute: NSFontAttributeName value:CalibriBold(11.0f) range:[completeString rangeOfString:modelDailySpecialDetails.price]];
        CGRect rectsize = [AttributedString boundingRectWithSize:CGSizeMake(tableView_DailySpecial.frame.size.width-16, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin context:nil];
        return rectsize.size.height;
    }
    else
    {
        NSString *price;
        if ([modelDailySpecialDetails.price rangeOfString:@"$"].location == NSNotFound) {
            price = [NSString stringWithFormat:@"$%@",modelDailySpecialDetails.price];
        }else
        {
            price = [NSString stringWithFormat:@"%@",modelDailySpecialDetails.price];
        }
        
        NSString *completeString = [NSString stringWithFormat:@"%@ %@ (%@-%@)",price, trimmedString,stringStartTime,stringCloseTime];
        NSMutableAttributedString *AttributedString = [[NSMutableAttributedString alloc] initWithString:completeString];
        [AttributedString addAttribute:NSFontAttributeName value:Calibri(12) range:NSMakeRange(0, completeString.length-1)];
        CGRect rectsize = [AttributedString boundingRectWithSize:CGSizeMake(tableView_DailySpecial.frame.size.width-16, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin context:nil];
        return rectsize.size.height;
    }
}
#pragma mark -- HighlightHelper
-(void) GetHighlightsOfPlaces
{
    BOOL check=[[AppDelegate shareAppdelegateInstance] checkReachability];
    if(check)
    {
        [CUSTOMINDICATORMACRO startLoadAnimation:self];
        [restModal_DidSelectMap setUserID:user_IDRegister];
        
        [RestaurantMacro GetHighlightsOfPlace:restModal_DidSelectMap withCompletion:^(id obj1) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                if([obj1 isKindOfClass:[ResturantModal class]])
                {
                    _rest_HighLight  = obj1;
                    
                    if (_rest_HighLight.arrHighlight > 0)
                    {
                        ArrHighLight = _rest_HighLight.arrHighlight;
                       [quick_mainView addSubview:HighlightView];
                        [_Collection_Highlight registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"HighlightCell"];

                    }
                    else{
                        
                    }
                }
                else
                {
                  //  info_View.hidden=YES;
                   // view_tableView.hidden = YES;
                   // view_Empty.hidden = NO;
                    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Please check your Internet Connection." preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction *okBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        [self dismissViewControllerAnimated:YES completion:nil];
                    }];
                    [alertController addAction:okBtn];
                    [self presentViewController:alertController animated:YES completion:nil];
                    
                }
                
                [CUSTOMINDICATORMACRO stopLoadAnimation:self];
                
            });
        }];
    }
}

#pragma mark --Daily Specials Service Helper
-(void)specialsServiceHelper
{
    
    BOOL check=[[AppDelegate shareAppdelegateInstance] checkReachability];
    if(check)
    {
        [CUSTOMINDICATORMACRO startLoadAnimation:self];
        [restModal_DidSelectMap setUserID:user_IDRegister];
        
        [RestaurantMacro getSpecialsHappyHoursToServer:restModal_DidSelectMap withCompletion:^(id obj1) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                if([obj1 isKindOfClass:[ResturantModal class]])
                {
                    _restModal_QuickView  = obj1;
                    [self createSliderWithImagesWithAutoScroll];
                    
                    
                    
                }
                else
                {
                    info_View.hidden=YES;
                    view_tableView.hidden = YES;
                    view_Empty.hidden = NO;
                    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Please check your Internet Connection." preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction *okBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        [self dismissViewControllerAnimated:YES completion:nil];
                    }];
                    [alertController addAction:okBtn];
                    [self presentViewController:alertController animated:YES completion:nil];
                    
                }
                quick_touchableView.hidden=YES;
                
                [CUSTOMINDICATORMACRO stopLoadAnimation:self];
                
            });
        }];
    }
    
}

#pragma mark -- Happy Hours and Daily Specials Service Helper
-(void)specialsHappyHoursServiceHelper
{
    
    BOOL check=[[AppDelegate shareAppdelegateInstance] checkReachability];
    if(check)
    {
        [CUSTOMINDICATORMACRO startLoadAnimation:self];
        [restModal_DidSelectMap setUserID:user_IDRegister];
        
        [RestaurantMacro getSpecialsHappyHoursToServer:restModal_DidSelectMap withCompletion:^(id obj1) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                if([obj1 isKindOfClass:[ResturantModal class]])
                {
                    _restModal_QuickView  = obj1;
                    
                     [self createSliderWithImagesWithAutoScroll];
                    if(isCheckedBackData_HappyDaily == FALSE)
                    {
                        
                        if(_restModal_QuickView.arrayHappyHours.count>0)
                        {
                            arraySpecialforday = _restModal_QuickView.arrayHappyHours;
                            
                            info_View.hidden=NO;
                            view_tableView.hidden = NO;
                            view_Empty.hidden = YES;
                            [tableView_DailySpecial reloadData];
                            
                        }
                        else
                        {
                            info_View.hidden=YES;
                            view_tableView.hidden = YES;
                            view_Empty.hidden = NO;
                            
                        }
                    }
                    else
                    {
                        
                        if(_restModal_QuickView.arraySpecials.count>0)
                        {
                            arraySpecialforday = _restModal_QuickView.arraySpecials;
                            
                            info_View.hidden=NO;
                            view_tableView.hidden = NO;
                            view_Empty.hidden = YES;
                            
                            
                            
                            [tableView_DailySpecial reloadData];
                            
                            
                        }
                        else
                        {
                            info_View.hidden=YES;
                            view_tableView.hidden = YES;
                            view_Empty.hidden = NO;
                            
                        }
                    }
                    
                }
                else
                {
                    info_View.hidden=YES;
                    view_tableView.hidden = YES;
                    view_Empty.hidden = NO;
                    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Please check your Internet Connection." preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction *okBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        [self dismissViewControllerAnimated:YES completion:nil];
                    }];
                    [alertController addAction:okBtn];
                    [self presentViewController:alertController animated:YES completion:nil];
                    
                }
                quick_touchableView.hidden=YES;
                
                [CUSTOMINDICATORMACRO stopLoadAnimation:self];
                
            });
        }];
    }
    
}


#pragma  mark - Daily Specials For Each Resturant

-(void)dailySpecialServiceHelper
{
    BOOL check=[[AppDelegate shareAppdelegateInstance] checkReachability];
    if(check)
    {
        [CUSTOMINDICATORMACRO startLoadAnimation:self];
        [RestaurantMacro listAllDailySpecialsFromServer:restModal_DidSelectMap withCompletion:^(id obj1) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if([obj1 isKindOfClass:[NSArray class]] || [obj1 isKindOfClass:[NSMutableArray class]])
                {
                    NSArray *response  = obj1;
                    if(response.count>0)
                    {
                        arraySpecialforday = obj1;
                        info_View.hidden=NO;
                        view_tableView.hidden = NO;
                        view_Empty.hidden = YES;
                        
                        [tableView_DailySpecial reloadData];
                    }
                    else
                    {
                        info_View.hidden=YES;
                        view_tableView.hidden = YES;
                        view_Empty.hidden = NO;
                    }
                }
                else
                {
                    info_View.hidden=YES;
                    view_tableView.hidden = YES;
                    view_Empty.hidden = NO;
                    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Please check your Internet Connection." preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction *okBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        [self dismissViewControllerAnimated:YES completion:nil];
                    }];
                    [alertController addAction:okBtn];
                    [self presentViewController:alertController animated:YES completion:nil];
                }
                [CUSTOMINDICATORMACRO stopLoadAnimation:self];
            });
        }];
    }
    
}

#pragma mark --- Load All Map Data Service Helper

-(void)loadMapValuesServiceHelper : (ResturantModal *)modal{
    NSString *strDailySpecial,*strHappyHours,*strTopSpots;
    if(array_SelectedDailySpecial.count > 0){
        strDailySpecial = [array_SelectedDailySpecial componentsJoinedByString:@","];
    }
    else{
        strDailySpecial = @"";
    }
    
    if(array_SelectedHappyHours.count > 0){
        strHappyHours = [array_SelectedHappyHours componentsJoinedByString:@","];
    }
    else{
        strHappyHours = @"";
    }
    
    if(array_SelectedTopSpots.count > 0){
        strTopSpots = [array_SelectedTopSpots componentsJoinedByString:@","];
    }
    else{
        strTopSpots = @"";
    }
    
    [modal setResturantLatitude:modal.resturantLatitude];//customlat
    [modal setResturantLongtitude:modal.resturantLongtitude];//customlat
    
    NSString *strReplace = modal.systemMessage;
    modal.systemMessage = strReplace;
    
    
    if (user_IDRegister==nil) {
        NSData *data = [Default objectForKey:@"logindetails"];
        registerModalResponeValue = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        user_IDRegister = registerModalResponeValue.userID;
    }
    
    [modal setResturantRadius:GlobalRadius];
    [modal setUserID:user_IDRegister];
    
    modal.resturantLatitude     =   (modal.resturantLatitude == nil) ? @"" : modal.resturantLatitude;
    modal.resturantLongtitude   =   (modal.resturantLongtitude == nil) ? @"" : modal.resturantLongtitude;
    modal.systemMessage         =   (modal.systemMessage == nil) ? @"" : modal.systemMessage;
    modal.is_LiveMap            =   @"0"; //(isSelectedValuesIsLive == TRUE) ? @"0" : @"1";
    modal.is_HappyHours         =   array_SelectedHappyHours.count > 0 ? @"1" : @"0"; //(isCheckedHappyHours == TRUE)
    modal.is_DailySpecials      =   array_SelectedDailySpecial.count > 0 ? @"1" : @"0"; //(isCheckedDailySpecials == TRUE)
    modal.happHoursDay          =   strHappyHours;//(string_HappyHours == nil || [string_HappyHours isEqualToString:@"View All"]) ? @"" : string_HappyHours;
    modal.dailySpecialsDay  =   strDailySpecial;//(string_DailySpecials == nil) || [string_DailySpecials isEqualToString:@"View All"] ? @"" : string_DailySpecials;
    modal.top_spot_id = strTopSpots;
    modal.CityOrArea = _strCityOrPlace;
    modal.strOffset = @"0";
    modal.strSW_Lat = @"";
    modal.strSW_Long = @"";
    modal.strNE_Lat = @"";
    modal.strNE_Long = @"";

    if (!lastModal) {
        lastModal=[ResturantModal new];
    }
    lastModal=modal;
    
    [resturantArray removeAllObjects];
    
    BOOL check=[[AppDelegate shareAppdelegateInstance] checkReachability];
    if(check){
        [CUSTOMINDICATORMACRO startLoadAnimation:self];
        
        [RestaurantMacro listAllMapValuesToServer:modal withCompletion:^(id obj1)
         {
             
             dispatch_async(dispatch_get_main_queue(), ^{
                 if([obj1 isKindOfClass:[NSArray class]] || [obj1 isKindOfClass:[NSMutableArray class]])
                 {
                     isKeepDraging = NO;
                     NSArray *arry=obj1;
                     if (arry.count>0) {
                         NumberOfRecordFound = [obj1[2] intValue];
                         min_distance =[obj1[0] doubleValue];
                         resturantArray = obj1[1];
                     }
                     if(resturantArray.count>0)
                     {
                         dispatch_async(dispatch_get_main_queue(), ^{
                             //My change
                           //  _lblDisplayLocations.text=NumberOfRecordFound==1?@"   1 Location Displaying": [NSString stringWithFormat:@"   %ld Locations Displaying",(unsigned long)NumberOfRecordFound];
                         });
                         
                         isCheckedQuickView = [[NSUserDefaults standardUserDefaults]boolForKey:@"ischeckedshowquickview"];
                         if(isCheckedQuickView == TRUE)
                         {
                             [[NSUserDefaults standardUserDefaults]setBool:FALSE forKey:@"ischeckedshowquickview"];
                             
                             [self addQuickView];
                             viewMapQuickDetails.hidden = FALSE;
                             lbl_BtnBar.hidden = NO;
                             view_CloseQuick.hidden = NO;
                             viewMoreButton.hidden = FALSE;
                             NSArray *filteredArray = [resturantArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"placeID ==%@",placeidSub]];
                             if(filteredArray>0)
                             {
                                 ResturantModal *modal = [filteredArray firstObject];
                                 [self setImageOnSelectedAnnotation:selectedView withResturant:modal];
                                 [self specialsHappyHoursServiceHelper];
                                 [self loadDataForQuickViewInSort:modal];
                             }
                             mapUpdate=NO;
                             [self loadLocationInMap];
                         }
                         else
                         {
                             [mapView removeAnnotations:mapView.annotations];
                             NSArray *filteredArray = [resturantArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"placeID ==%@",placeidSub]];
                             if(filteredArray>0)
                             {
                                 ResturantModal *modal = [filteredArray firstObject];
                                 [self loadDataForQuickViewInSort:modal];
                             }
                             mapUpdate=NO;
                             [self loadLocationInMap];
                         }
                         
                     }
                     else
                     {
                         [mapView removeAnnotations:mapView.annotations];
                         mapView.showsUserLocation = YES;
                         
                         CLLocationCoordinate2D cord= CLLocationCoordinate2DMake([stringLat doubleValue],[stringLong doubleValue]);
                         
                         
                         MKCoordinateRegion region;
                         region.center=cord;
                         //change Span
                         region.span.latitudeDelta=0.010;
                         region.span.longitudeDelta=0.010;
                         [mapView setRegion:region animated:YES];
                         [mapView regionThatFits:region];
                         
                         dispatch_async(dispatch_get_main_queue(), ^{
                             _lblDisplayLocations.text=@"   0 Location Displaying";
                             manuallySearchLocation=NO;
                         });
                         
                         [CUSTOMINDICATORMACRO stopLoadAnimation:self];
                     }
                 }
                 else
                 {
                     [CUSTOMINDICATORMACRO stopLoadAnimation:self];
                     mapView.showsUserLocation = YES;
                     [mapView removeAnnotations:mapView.annotations];
                     UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Please check your Internet Connection." preferredStyle:UIAlertControllerStyleAlert];
                     
                     UIAlertAction *okBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                         [self dismissViewControllerAnimated:YES completion:nil];
                     }];
                     manuallySearchLocation=NO;
                     [alertController addAction:okBtn];
                     [self presentViewController:alertController animated:YES completion:nil];
                 }
             });
         }];
    }
}

-(void)getEventsCategory{
    //[CUSTOMINDICATORMACRO startLoadAnimation:self];
    
    [ServiceHelper getRequest:@"getEventCategories.json" callback:^(id data, NSError* error, BOOL success){
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if (success) {
                if (data!=nil && ![data isKindOfClass:[NSNull class]]) {
                    if ([data isKindOfClass:[NSArray class]]) {
                        if (array_TopSpots.count>0) {
                            [array_TopSpots  removeAllObjects];
                        }
                        
                        EventsModel *model;
                        for (NSDictionary *dict in data) {
                            model = [EventsModel new];
                            [model setCategoryID:dict[@"Category_id"]];
                            [model setCategoryName:dict[@"Category_name"]];
                            [model setCategoryImage:dict[@"Category_image"]];
                            [array_TopSpots addObject:model];
                            model=nil;
                        }
                        //[CUSTOMINDICATORMACRO stopLoadAnimation:self];
                        [tblView_TopSpots reloadData];
                    }
                    else
                    {
                        [CUSTOMINDICATORMACRO stopLoadAnimation:self];
                    }
                }else
                {
                    [CUSTOMINDICATORMACRO stopLoadAnimation:self];
                    
                }
            }else
            {
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Please check your Internet Connection." preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *okBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    [self dismissViewControllerAnimated:YES completion:nil];
                }];
                [alertController addAction:okBtn];
                [self presentViewController:alertController animated:YES completion:nil];
                [CUSTOMINDICATORMACRO stopLoadAnimation:self];
            }
        });
    }];
    
    
}

///<-------------------------------------------------
#pragma  mark --- Get coordinate according to CGRect
///<-------------------------------------------------


// SW Coordinates
-(CLLocationCoordinate2D)getSWCoordinate:(MKMapRect)mRect{
    return [self getCoordinateFromMapRectanglePoint:mRect.origin.x y:MKMapRectGetMaxY(mRect)];
}
-(CLLocationCoordinate2D)getCoordinateFromMapRectanglePoint:(double)x y:(double)y{
    MKMapPoint swMapPoint = MKMapPointMake(x, y);
    return MKCoordinateForMapPoint(swMapPoint);
}

//NW
-(CLLocationCoordinate2D)getNWCoordinate:(MKMapRect)mRect{
    return [self getCoordinateFromMapRectanglePoint:MKMapRectGetMinX(mRect) y:mRect.origin.y];
}
//NE Coordiantes
-(CLLocationCoordinate2D)getNECoordinate:(MKMapRect)mRect{
    return [self getCoordinateFromMapRectanglePoint:MKMapRectGetMaxX(mRect) y:mRect.origin.y];
}
- (CLLocationCoordinate2D)getTopCenterCoordinate{
    CLLocationCoordinate2D topCenterCoor = [mapView convertPoint:CGPointMake(mapView.frame.size.width/2,0) toCoordinateFromView:mapView];
    return topCenterCoor;
}

- (CLLocationCoordinate2D)getTopLeftCenterCoordinate{
    CLLocationCoordinate2D topCenterCoor = [mapView convertPoint:CGPointMake(50,50) toCoordinateFromView:mapView];
    return topCenterCoor;
}

- (CLLocationCoordinate2D)getBottomRightCenterCoordinate{
    CLLocationCoordinate2D topCenterCoor = [mapView convertPoint:CGPointMake(mapView.frame.size.width-50,mapView.frame.size.height+220) toCoordinateFromView:mapView];
    return topCenterCoor;
}

- (CLLocationCoordinate2D)getTopRightCenterCoordinate{
    CLLocationCoordinate2D topCenterCoor = [mapView convertPoint:CGPointMake(20,mapView.frame.size.width-20) toCoordinateFromView:mapView];
    return topCenterCoor;
}

- (CLLocationCoordinate2D)getBottomLeftCenterCoordinate{
    CLLocationCoordinate2D topCenterCoor = [mapView convertPoint:CGPointMake(20,mapView.frame.size.height-30) toCoordinateFromView:mapView];
    return topCenterCoor;
}

///<---------------------------------------
#pragma  mark --- Location Manager Delegate
///<---------------------------------------

- (void)mapView:(MKMapView *)mapView1 regionWillChangeAnimated:(BOOL)animate
{
    [mapMoveTimer invalidate];
    if(isCheckedLocation == FALSE){
        latStr = [NSString stringWithFormat:@"%.8f", mapView1.centerCoordinate.latitude];
        longStr = [NSString stringWithFormat:@"%.8f", mapView1.centerCoordinate.longitude];
    }
}

-(void)mapView:(MKMapView *)mapViewVw regionDidChangeAnimated:(BOOL)animated{
    stringLat=[NSString stringWithFormat:@"%f",mapViewVw.centerCoordinate.latitude];
    stringLong=[NSString stringWithFormat:@"%f",mapViewVw.centerCoordinate.longitude];
    if (_currentLocationEnable == YES) {
        lblTotalResult.hidden = true;
    }
    if (isKeepDraging == YES)
    {
        if (![checkVC isEqualToString:@"restlist"] && !manuallySearchLocation && !annotationSelected && !_currentLocationEnable)
        {
            if (![txt_SearchBox.text isEqualToString: @""]){
                btnClearFilter.hidden = true;
            }
            mapUpdate=NO;
            zoomEnable=YES;
            SearchFixedPlace=NO;
            lblTotalResult.hidden = true;
            txt_SearchBox.text = @"";
            // Please add "distance"  PARAMETRE in api
            CLLocationCoordinate2D centerCoor = mapViewVw.centerCoordinate;
            CLLocation *centerLocation = [[CLLocation alloc] initWithLatitude:centerCoor.latitude longitude:centerCoor.longitude];
            
            CLLocationCoordinate2D topCenterCoor = [self getTopCenterCoordinate];
            CLLocation *topCenterLocation = [[CLLocation alloc] initWithLatitude:topCenterCoor.latitude longitude:topCenterCoor.longitude];
            
            CLLocationDistance radius = [centerLocation distanceFromLocation:topCenterLocation];
           // GlobalRadius= [ NSString stringWithFormat:@"%f",(radius+0.5)/1000];
            MKMapRect mRect = mapView.visibleMapRect;

            CLLocationCoordinate2D topRightCoor = [self getNWCoordinate:mRect];//[self getTopRightCenterCoordinate];
            self.strNE_Long = [NSString stringWithFormat:@"%f",topRightCoor.longitude];
            self.strNE_Lat = [NSString stringWithFormat:@"%f",topRightCoor.latitude];
            
            
            CLLocationCoordinate2D BottomRightCoor = [self getSWCoordinate:mRect]; // [self getBottomLeftCenterCoordinate];
            self.strSW_Long = [NSString stringWithFormat:@"%f",BottomRightCoor.longitude];
            self.strSW_Lat = [NSString stringWithFormat:@"%f",BottomRightCoor.latitude];
            
            
            
            
            GlobalRadius= @"1";
            if (!mapMoveTimer) {
                mapMoveTimer=[[NSTimer alloc]init];
            }
            mapMoveTimer=[NSTimer scheduledTimerWithTimeInterval:2 repeats:NO block:^(NSTimer *timer){
                [self getLocationsFromRadius:radius mapView:mapViewVw];
            }];
        }
    }
    isKeepDraging = YES;
}

-(void)getLocationsFromRadius:(CLLocationDistance)radius mapView:(MKMapView *)mapViewVw{
    
    NSString *strDailySpecial,*strHappyHours,*strTopSpots;
    
    if(array_SelectedDailySpecial.count > 0){
        strDailySpecial = [array_SelectedDailySpecial componentsJoinedByString:@","];
    }
    else{
        strDailySpecial = @"";
    }
    
    if(array_SelectedHappyHours.count > 0){
        strHappyHours = [array_SelectedHappyHours componentsJoinedByString:@","];
    }
    else{
        strHappyHours = @"";
    }
    
    if(array_SelectedTopSpots.count > 0){
        strTopSpots = [array_SelectedTopSpots componentsJoinedByString:@","];
    }
    else{
        strTopSpots = @"";
    }
    
    ResturantModal *modal = [[ResturantModal alloc]init];
    [modal setResturantLatitude:[ NSString stringWithFormat:@"%f",mapViewVw.centerCoordinate.latitude ]];//customlat
    [modal setResturantLongtitude:[ NSString stringWithFormat:@"%f",mapViewVw.centerCoordinate.longitude ]];//customlat
    //[modal setResturantRadius:[ NSString stringWithFormat:@"%f",radius/1000]];
    [modal setResturantRadius:@"0.8"];
    [modal setSystemMessage:txt_SearchBox.text];
    [modal setUserID:user_IDRegister];
    [modal setCityOrArea:_strCityOrPlace];
    [modal setCityOrArea:@""];
    [modal setTabs:@""];
    modal.is_LiveMap            = @"0";
    modal.is_HappyHours         =   array_SelectedHappyHours.count > 0 ? @"1" : @"0"; //(isCheckedHappyHours == TRUE)
    modal.is_DailySpecials      =   array_SelectedDailySpecial.count > 0 ? @"1" : @"0"; //(isCheckedDailySpecials == TRUE)
    modal.happHoursDay          =   strHappyHours; //(string_HappyHours == nil || [string_HappyHours isEqualToString:@"View All"]) ? @"" : string_HappyHours;
    modal.dailySpecialsDay  =   strDailySpecial; //(string_DailySpecials == nil) || [string_DailySpecials isEqualToString:@"View All"] ? @"" : string_DailySpecials;
    modal.systemMessage         =   (modal.systemMessage == nil) ? @"" : modal.systemMessage;
    modal.top_spot_id = strTopSpots;
    modal.strOffset = @"0";
    modal.strSW_Lat = self.strSW_Lat;
    modal.strSW_Long = self.strSW_Long;
    modal.strNE_Lat = self.strNE_Lat;
    modal.strNE_Long = self.strNE_Long;
    
    
    
    lastModal=modal;
    
    BOOL check=[[AppDelegate shareAppdelegateInstance] checkReachability];
    if(check)
    {
        if (!restListArray) {
            resturantArray = [[NSMutableArray alloc]init];
        }
        [CUSTOMINDICATORMACRO startLoadAnimation:self];
        
        //service
        [RestaurantMacro listAllMapValuesToServer:modal withCompletion:^(id obj1) {
            //NSLog(@"View Account ===  %@",obj1);
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if([obj1 isKindOfClass:[NSArray class]] || [obj1 isKindOfClass:[NSMutableArray class]])
                {
                    
                    NSArray *arry=obj1;
                    if (arry.count>0) {
                        NumberOfRecordFound = [obj1[2] intValue];
                        min_distance =[obj1[0] doubleValue];
                        resturantArray = obj1[1];
                    }
                    
                    //_lblDisplayLocations.text=[NSString stringWithFormat:@"   %ld Locations Displaying",(unsigned long)NumberOfRecordFound];
                    if(resturantArray.count>0)
                    {
                        
                        isCheckedQuickView = [[NSUserDefaults standardUserDefaults]boolForKey:@"ischeckedshowquickview"];
                        if(isCheckedQuickView == TRUE)
                        {
                            [[NSUserDefaults standardUserDefaults]setBool:FALSE forKey:@"ischeckedshowquickview"];
                            [self addQuickView];
                            viewMapQuickDetails.hidden = FALSE;
                            lbl_BtnBar.hidden = NO;
                            view_CloseQuick.hidden = NO;
                            viewMoreButton.hidden = FALSE;
                            NSArray *filteredArray = [resturantArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"placeID ==%@",placeidSub]];
                            if(filteredArray>0)
                            {
                                ResturantModal *modal = [filteredArray firstObject];
                                
                                [self loadDataForQuickViewInSort:modal];
                            }
                            mapUpdate=NO;
                            [self loadLocationInMap];
                        }
                        else{
                            [mapView removeAnnotations:mapView.annotations];
                            NSArray *filteredArray = [resturantArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"placeID ==%@",placeidSub]];
                            if(filteredArray>0)
                            {
                                ResturantModal *modal = [filteredArray firstObject];
                                
                                [self loadDataForQuickViewInSort:modal];
                            }
                            mapUpdate=NO;
                            [self loadLocationInMap];
                        }
                    }
                    else
                    {
                        [mapView removeAnnotations:mapView.annotations];
                        dispatch_async(dispatch_get_main_queue(), ^{
                            _lblDisplayLocations.text=@"   0 Location Displaying";
                        });
                        
                        
                        
                        /* _lblDisplayLocations.text=@"   0 Location Displaying";
                         [self showAlertTitle:@"" Message:PLACENOTFOUND_ALERT];*/
                        [CUSTOMINDICATORMACRO stopLoadAnimation:self];
                    }
                }
                else
                {
                    [mapView removeAnnotations:mapView.annotations];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        _lblDisplayLocations.text=@"   0 Location Displaying";
                    });
                    
                    
                    /* _lblDisplayLocations.text=@"   0 Location Displaying";
                     [self showAlertTitle:@"" Message:PLACENOTFOUND_ALERT];*/
                    [CUSTOMINDICATORMACRO stopLoadAnimation:self];
                }
                
            });
            
        }];
        
    }
    [[NSUserDefaults standardUserDefaults]setBool:FALSE forKey:@"checkmap"];
    
}

-(void)getAddress:(CLLocation*)centreLocation{
    
    __block NSString *strAdd = nil;
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    [geocoder reverseGeocodeLocation:centreLocation completionHandler:^(NSArray *placemarks, NSError *error)
     {
         NSLog(@"Found placemarks: %@, error: %@", placemarks, error);
         if (error == nil && [placemarks count] > 0)
         {
             CLPlacemark *  placemark = [placemarks lastObject];
             // strAdd -> take bydefault value nil
             // NSString *strAdd = nil;
             
             if ([placemark.subThoroughfare length] != 0)
                 strAdd = placemark.subThoroughfare;
             
             if ([placemark.thoroughfare length] != 0)
             {
                 // strAdd -> store value of current location
                 if ([strAdd length] != 0)
                     strAdd = [NSString stringWithFormat:@"%@, %@",strAdd,[placemark thoroughfare]];
                 else
                 {
                     // strAdd -> store only this value,which is not null
                     strAdd = placemark.thoroughfare;
                 }
             }
             NSLog(@"1 == %@",strAdd);
             
             if ([placemark.locality length] != 0)
             {
                 if ([strAdd length] != 0)
                     strAdd = [NSString stringWithFormat:@"%@, %@",strAdd,[placemark locality]];
                 else
                     strAdd = placemark.locality;
             }
             NSLog(@"3 == %@",strAdd);
             
             if ([placemark.administrativeArea length] != 0)
             {
                 if ([strAdd length] != 0)
                     strAdd = [NSString stringWithFormat:@"%@, %@",strAdd,[placemark administrativeArea]];
                 else
                     strAdd = placemark.administrativeArea;
             }
             NSLog(@"4 == %@",strAdd);
             
             if ([placemark.country length] != 0)
             {
                 if ([strAdd length] != 0)
                     strAdd = [NSString stringWithFormat:@"%@, %@",strAdd,[placemark country]];
                 else
                     strAdd = placemark.country;
             }
             NSLog(@"5 == %@",strAdd);
             
             dispatch_async(dispatch_get_main_queue(), ^{
                 txt_SearchBox.text=strAdd;
                 [txt_SearchBox resignFirstResponder];
             });
             placemark=nil;
         }
     }];
    
    geocoder=nil;
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    CLLocation *location = [locations lastObject];
    //AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.userCurrentLocation = location;
    
    [[NSUserDefaults standardUserDefaults]setBool:FALSE forKey:@"randomsearch"];
    
    if (location != nil)
    {
        stringLong =  [NSString stringWithFormat:@"%.8f", location.coordinate.longitude];
        stringLat  =  [NSString stringWithFormat:@"%.8f", location.coordinate.latitude];
        
        ResturantModal *modal = [[ResturantModal alloc]init];
        NSLog(@"lat == %@ long == %@",stringLat,stringLong);
        
        
        stringLatForCurrentLocation=stringLat;
        stringLongForCurrentLocation=stringLong;
        
        NSString *strDailySpecial,*strHappyHours,*strTopSpots;
        
        if(array_SelectedDailySpecial.count > 0){
            strDailySpecial = [array_SelectedDailySpecial componentsJoinedByString:@","];
        }
        else{
            strDailySpecial = @"";
        }
        
        if(array_SelectedHappyHours.count > 0){
            strHappyHours = [array_SelectedHappyHours componentsJoinedByString:@","];
        }
        else{
            strHappyHours = @"";
        }
        
        if(array_SelectedTopSpots.count > 0){
            strTopSpots = [array_SelectedTopSpots componentsJoinedByString:@","];
        }
        else{
            strTopSpots = @"";
        }
        
        if(![stringLong isEqualToString:@""] && ![stringLong isEqualToString:@"0"])
        {
            BOOL isCheckedMap = [[NSUserDefaults standardUserDefaults]boolForKey:@"checkmap"];
            if(isCheckedMap == TRUE)
            {
                [modal setResturantLatitude:stringLat];//customlat
                [modal setResturantLongtitude:stringLong];//customlat
                [modal setUserID:user_IDRegister];
                [modal setResturantRadius:@"1"];
                GlobalRadius=@"";
                
                modal.is_LiveMap            = @"0";
                modal.is_HappyHours         =   array_SelectedHappyHours.count > 0 ? @"1" : @"0"; //(isCheckedHappyHours == TRUE)
                modal.is_DailySpecials      =   array_SelectedDailySpecial.count > 0 ? @"1" : @"0"; //(isCheckedDailySpecials == TRUE)
                modal.happHoursDay          =   strHappyHours; //(string_HappyHours == nil || [string_HappyHours isEqualToString:@"View All"]) ? @"" : string_HappyHours;
                modal.dailySpecialsDay  =   strDailySpecial; //(string_DailySpecials == nil) || [string_DailySpecials isEqualToString:@"View All"] ? @"" : string_DailySpecials;
                modal.systemMessage         =   (modal.systemMessage == nil) ? @"" : modal.systemMessage;
                modal.top_spot_id = strTopSpots;
                modal.strOffset = @"0";
                modal.strSW_Lat = @"";
                modal.strSW_Long = @"";
                modal.strNE_Lat = @"";
                modal.strNE_Long = @"";

                if (!lastModal) {
                    lastModal=[ResturantModal new];
                }
                lastModal=modal;
                BOOL check=[[AppDelegate shareAppdelegateInstance] checkReachability];
                if(check)
                {
                    locationManager=nil;
                    resturantArray = [[NSMutableArray alloc]init];
                    [CUSTOMINDICATORMACRO startLoadAnimation:self];
                    [RestaurantMacro listAllMapValuesToServer:modal withCompletion:^(id obj1) {
                        NSLog(@"View Account ===  %@",obj1);
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            if([obj1 isKindOfClass:[NSArray class]] || [obj1 isKindOfClass:[NSMutableArray class]])
                            {
                                NSArray *arry=obj1;
                                if (arry.count>0) {
                                    NumberOfRecordFound = [obj1[2] intValue];
                                    min_distance=[obj1[0] doubleValue];
                                    resturantArray = obj1[1];
                                }
                                
                                
                                dispatch_async(dispatch_get_main_queue(), ^{
                                   // _lblDisplayLocations.text=NumberOfRecordFound==1?@"   1 Location Displaying": [NSString stringWithFormat:@"   %ld Locations Displaying",(unsigned long)NumberOfRecordFound];
                                });
                                
                                if(resturantArray.count>0)
                                {
                                    
                                    isCheckedQuickView = [[NSUserDefaults standardUserDefaults]boolForKey:@"ischeckedshowquickview"];
                                    if(isCheckedQuickView == TRUE)
                                    {
                                        [[NSUserDefaults standardUserDefaults]setBool:FALSE forKey:@"ischeckedshowquickview"];
                                        [self addQuickView];
                                        viewMapQuickDetails.hidden = FALSE;
                                        lbl_BtnBar.hidden = NO;
                                        view_CloseQuick.hidden = NO;
                                        viewMoreButton.hidden = FALSE;
                                        NSArray *filteredArray = [resturantArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"placeID ==%@",placeidSub]];
                                        if(filteredArray>0)
                                        {
                                            ResturantModal *modal = [filteredArray firstObject];
                                            
                                            [self loadDataForQuickViewInSort:modal];
                                        }
                                        mapUpdate=NO;
                                        [self loadLocationInMap];
                                        _currentLocationEnable=NO;
                                        _firstTimeLoad=NO;
                                    }
                                    else{
                                        [mapView removeAnnotations:mapView.annotations];
                                        NSArray *filteredArray = [resturantArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"placeID ==%@",placeidSub]];
                                        if(filteredArray>0)
                                        {
                                            ResturantModal *modal = [filteredArray firstObject];
                                            [self loadDataForQuickViewInSort:modal];
                                            
                                        }
                                        mapUpdate=NO;
                                        [self loadLocationInMap];
                                        _currentLocationEnable=NO;
                                        _firstTimeLoad=NO;
                                    }
                                }
                                else
                                {
                                    [mapView removeAnnotations:mapView.annotations];
                                    CLLocationCoordinate2D cord= CLLocationCoordinate2DMake(stringLat.floatValue,stringLong.floatValue);
                                    
                                    MKCoordinateRegion region;
                                    region.center=cord;
                                    //change span
                                    region.span.latitudeDelta=0.010;
                                    region.span.longitudeDelta=0.010;
                                    ;
                                    [mapView setRegion:region animated:YES];
                                    [mapView regionThatFits:region];
                                    
                                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 3 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                                        _lblDisplayLocations.text=@"   0 Location Displaying";
                                        _currentLocationEnable=NO;
                                        _firstTimeLoad=NO;
                                        manuallySearchLocation=NO;
                                        [CUSTOMINDICATORMACRO stopLoadAnimation:self];
                                    });
                                }
                            }
                            else
                            {
                                [mapView removeAnnotations:mapView.annotations];
                                
                                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 3 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                                    _lblDisplayLocations.text=@"   0 Location Displaying";
                                    _currentLocationEnable=NO;
                                    _firstTimeLoad=NO;
                                    manuallySearchLocation=NO;
                                    [CUSTOMINDICATORMACRO stopLoadAnimation:self];
                                });
                            }
                            
                        });
                        
                    }];
                    
                }
                [[NSUserDefaults standardUserDefaults]setBool:FALSE forKey:@"checkmap"];
                
            }
        }
    }
    [manager stopUpdatingLocation];
}

-(void)zoomToFitMapAnnotations
{
    if([mapView.annotations count] == 0)
        return;
    
    CLLocationCoordinate2D topLeftCoord;
    topLeftCoord.latitude = -90;
    topLeftCoord.longitude = 180;
    
    CLLocationCoordinate2D bottomRightCoord;
    bottomRightCoord.latitude = 90;
    bottomRightCoord.longitude = -180;
    
    for(MKPointAnnotation* annotation in mapView.annotations)
    {
        topLeftCoord.longitude = fmin(topLeftCoord.longitude, annotation.coordinate.longitude);
        topLeftCoord.latitude = fmax(topLeftCoord.latitude, annotation.coordinate.latitude);
        
        bottomRightCoord.longitude = fmax(bottomRightCoord.longitude, annotation.coordinate.longitude);
        bottomRightCoord.latitude = fmin(bottomRightCoord.latitude, annotation.coordinate.latitude);
    }
    
    MKCoordinateRegion region;
    region.center.latitude = topLeftCoord.latitude - (topLeftCoord.latitude - bottomRightCoord.latitude) * 0.5;
    region.center.longitude = topLeftCoord.longitude + (bottomRightCoord.longitude - topLeftCoord.longitude) * 0.5;
    region.span.latitudeDelta = fabs(topLeftCoord.latitude - bottomRightCoord.latitude) * 1.1; // Add a little extra space on the sides
    region.span.longitudeDelta = fabs(bottomRightCoord.longitude - topLeftCoord.longitude) * 1.1; // Add a little extra space on the sides
    region = [mapView regionThatFits:region];
    [mapView setRegion:region animated:YES];
}
- (void)mapViewDidFinishRenderingMap:(MKMapView *)mapView1 fullyRendered:(BOOL)fullyRendered
{
    /*if (mapUpdate) {
     [self loadLocationInMap];
     mapUpdate=NO;
     }*/
}
-(void)loadLocationInMap
{
    i = 0;
    int j = 0;
    
    NSMutableArray *sortedArray = [[NSMutableArray alloc]init];
    for (ResturantModal *modal in resturantArray)
    {
        [modal setPlaceType:modal.placeType];
        [modal setRatingType:modal.placeRatings];
        [sortedArray addObject:modal];
    }
    
    if([checkVC isEqualToString:@"listplaces"])
        [sortedArray addObjectsFromArray:resturantArray];
    else if ([checkVC isEqualToString:@"restlist"])
        [sortedArray addObjectsFromArray:resturantArray];
    
    else
        [sortedArray addObjectsFromArray:resturantArray];
    
    float lat = 0.0 ;
    float longt = 0.0;
    
    if (!zoomEnable || _firstTimeLoad) {
        
        if(txt_SearchBox.text.length>0){
            
            if(_isCheckedCityStatus == TRUE){
                if (SelectedLat == 0.0000){
                    if (min_distance>1) {
                        for (ResturantModal *modal in sortedArray){
                            NSLog(@"min_distance_modal == %f",[modal.minDistance doubleValue]);
                            if (min_distance == [modal.minDistance doubleValue]) {
                                //change span
                                lat = modal.resturantLatitude.floatValue;
                                longt = modal.resturantLongtitude.floatValue;
                                [self setCentreAnnotationWithLattitute:lat andLongitute:longt andWithSpan:0.010];
                                break;}
                        }
                    }else
                        [self setCentreAnnotationWithLattitute:[stringLat doubleValue] andLongitute:[stringLong doubleValue] andWithSpan:0.010];
                }
                else
                    [self setCentreAnnotationWithLattitute:SelectedLat andLongitute:SelectedLog andWithSpan:0.010];
            }
            else{
                if (SelectedLat == 0.0000){
                    
                    mapUpdate=YES;
                    NSArray *filteredArray;
                    if (_isCheckedCityStatus) {
                        NSPredicate *pedi=[NSPredicate predicateWithFormat:@"resturantName ==[c] %@",[ NSString stringWithFormat:@"%@",txt_SearchBox.text ]];
                        filteredArray = [resturantArray filteredArrayUsingPredicate:pedi];
                    }else
                    {
                        NSPredicate *pedi=[NSPredicate predicateWithFormat:@"resturantName ==[c] %@ AND placeID ==%@",txt_SearchBox.text,placeidSub];
                        filteredArray = [resturantArray filteredArrayUsingPredicate:pedi];
                    }
                    
                    
                    if(filteredArray.count>0)
                    {
                        for (ResturantModal *modal in filteredArray){
                            if ([modal.resturantLatitude doubleValue]!=0 && [modal.resturantLongtitude doubleValue]!=0) {
                                lat = modal.resturantLatitude.floatValue;
                                longt = modal.resturantLongtitude.floatValue;
                                [self setCentreAnnotationWithLattitute:lat andLongitute:longt andWithSpan:0.010];
                                break;
                            }
                        }
                    }else
                    {
                        for (ResturantModal *modal in sortedArray){
                            if ([modal.resturantLatitude doubleValue]!=0 && [modal.resturantLongtitude doubleValue]!=0) {
                                lat = modal.resturantLatitude.floatValue;
                                longt = modal.resturantLongtitude.floatValue;
                                [self setCentreAnnotationWithLattitute:lat andLongitute:longt andWithSpan:0.010];
                                break;
                            }
                        }
                    }
                }
                else
                    [self setCentreAnnotationWithLattitute:SelectedLat andLongitute:SelectedLog andWithSpan:0.010];
            }
        }
        //// main else
        else{
            if (SelectedLat == 0.0000){
                ResturantModal *modal = [sortedArray firstObject];
                
                // current location
                lat = modal.resturantLatitude.floatValue;
                longt = modal.resturantLongtitude.floatValue;
                
                if(![checkVC isEqualToString:@"listplaces"])
                    manuallySearchLocation=_firstTimeLoad?YES:NO;
                
                [self setCentreAnnotationWithLattitute:lat andLongitute:longt andWithSpan:0.010];
                _firstTimeLoad=NO;
                
            }
            else
                [self setCentreAnnotationWithLattitute:SelectedLat andLongitute:SelectedLog andWithSpan:0.010];
        }
    }
    
    MKMapRect mRect = mapView.visibleMapRect;

    CLLocationCoordinate2D coordinate1 = [self getNECoordinate:mRect];
    CLLocationCoordinate2D coordinate2 = [self getSWCoordinate:mRect];
    NSLog(@"%@",self.strNE_Long);
    NSLog(@"%@",self.strNE_Lat);
    MKMapPoint p1 = MKMapPointForCoordinate (coordinate1);
    MKMapPoint p2 = MKMapPointForCoordinate (coordinate2);
    
    MKMapRect visibleRect = MKMapRectMake(fmin(p1.x,p2.x), fmin(p1.y,p2.y), fabs(p1.x-p2.x), fabs(p1.y-p2.y));
    
    int countLocation=0;
    
    int visibleLocation_count = 0;
    [arrPasstoRest removeAllObjects];
    
    for(ResturantModal *modal in resturantArray)
    {
        lat = modal.resturantLatitude.floatValue;
        longt = modal.resturantLongtitude.floatValue;
        
        CLLocationCoordinate2D cord= CLLocationCoordinate2DMake(lat, longt);
         if(MKMapRectContainsPoint(visibleRect, MKMapPointForCoordinate(cord)))
         {
             [arrPasstoRest addObject:modal];
         }

    }
    for (ResturantModal *modal in sortedArray){
        lat = modal.resturantLatitude.floatValue;
        longt = modal.resturantLongtitude.floatValue;
        
        CLLocationCoordinate2D cord= CLLocationCoordinate2DMake(lat, longt);
        
        
        
        
        if(MKMapRectContainsPoint(visibleRect, MKMapPointForCoordinate(cord))) {
            
            if([checkVC isEqualToString:@"listplaces"]){
                if(![nearByRestModal.placeID isEqualToString:modal.placeID]){
                    MapAnnotation *newAnnotation = [[MapAnnotation alloc]initWithTitle:modal.resturantName andCoordinate:cord andPlaceType:modal.placeType andRating:modal.ratingType andPlaceID:modal.placeID andAnnotatonCount:modal.place_specials_count];
                    //[[MapAnnotation alloc]initWithTitle:modal.resturantName andCoordinate:cord andPlaceType:modal.placeType andRating:modal.ratingType andPlaceID:modal.placeID ];
                    [mapView addAnnotation:newAnnotation];
                    countLocation=countLocation+1;
                }
            }
            else{
                
                int specialCount=[modal.happy_hour_count intValue]+[modal.place_specials_count intValue];
                NSString *AnnotationCount = [NSString stringWithFormat:@"%d",specialCount];
                MapAnnotation *newAnnotation = [[MapAnnotation alloc]initWithTitle:modal.resturantName andCoordinate:cord andPlaceType:modal.placeType andRating:modal.ratingType andPlaceID:modal.placeID andAnnotatonCount:AnnotationCount];
                
                [mapView addAnnotation:newAnnotation];
            }
            visibleLocation_count = visibleLocation_count + 1;
       }
        
        i   = i+1;
        
        if(j == 2)
        {
            j = 0;
        }
        j   = j +1;
        
    }
    
    NSLog(@"%lu",(unsigned long)arrPasstoRest.count);
    dispatch_async(dispatch_get_main_queue(), ^{
        
        if (![self.strSW_Lat isEqualToString:@""]){
           // _lblDisplayLocations.text=visibleLocation_count==2?@"   1 Location Displaying": [NSString stringWithFormat:@"   %ld Locations Displaying",(unsigned long)visibleLocation_count/2];
            lblTotalResult.hidden = true;
            _lblDisplayLocations.text = [NSString stringWithFormat:@"   %ld Locations Displaying",(unsigned long)visibleLocation_count/2];

        }
        else{
           // _lblDisplayLocations.text=visibleLocation_count==2?@"   1 Location Displaying": [NSString stringWithFormat:@"   %ld Locations Displaying",(unsigned long)NumberOfRecordFound];
            lblTotalResult.hidden = false;
            _lblDisplayLocations.text = [NSString stringWithFormat:@"   %ld Locations Displaying",(unsigned long)visibleLocation_count/2];
            lblTotalResult.text = [NSString stringWithFormat:@"   %ld Search Results",(unsigned long)NumberOfRecordFound];


        }
        //_lblDisplayLocations.text=visibleLocation_count==2?@"   1 Location Displaying": [NSString stringWithFormat:@"   %ld Locations Displaying",(unsigned long)resturantArray.count];
       // _lblDisplayLocations.text= [NSString stringWithFormat:@"    %ld Locations Displaying",(unsigned long)NumberOfRecordFound];

        //[mapView setVisibleMapRect:mapView.visibleMapRect edgePadding:UIEdgeInsetsMake(100, 0, 0, 0) animated:NO];
        
    });
    
    if ([checkVC isEqualToString:@"listplaces"]) {
        dispatch_async(dispatch_get_main_queue(), ^{
           // _lblDisplayLocations.text=countLocation==2?@"   1 Location Displaying": [NSString stringWithFormat:@"   %ld Locations Displaying",(unsigned long)countLocation/2];
         //   _lblDisplayLocations.text=NumberOfRecordFound==2?@"   1 Location Displaying": [NSString stringWithFormat:@"   %ld Locations Displaying",(unsigned long)NumberOfRecordFound];
        });
    }
    
    if(![checkVC isEqualToString:@"listplaces"])
        manuallySearchLocation=_firstTimeLoad?YES:NO;
     //   manuallySearchLocation = YES;
    
    mapUpdate=YES;
    NSLog(@"manoj");
    [CUSTOMINDICATORMACRO stopLoadAnimation:self];
}


-(void)setCentreAnnotationWithLattitute:(double)lattitute andLongitute:(double)longitute andWithSpan:(double)span1{
    
    CLLocationCoordinate2D cord1 = CLLocationCoordinate2DMake(lattitute,longitute);
    MKCoordinateRegion region;
    region.center=cord1;
    region.span.latitudeDelta=span1;
    region.span.longitudeDelta=span1;
    regionChange=region;
    [mapView setRegion:region animated:NO];
    [mapView regionThatFits:region];
    
}

- (MKAnnotationView *)mapView:(MKMapView *)mapViewLocal viewForAnnotation:(id <MKAnnotation>)annotation
{
    annotationView=[[AnnotationView alloc]init];
    //annotationView = (AnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:@"MapDetails"];
    
    annotationView = [[[NSBundle mainBundle] loadNibNamed:@"AnnotationView" owner:self options:nil] objectAtIndex:0];
    
    if (annotation == mapView.userLocation){
        return nil; //default to blue dot
    }
    if (annotationView == nil){
        annotationView = [[[NSBundle mainBundle] loadNibNamed:@"AnnotationLayout" owner:self options:nil] objectAtIndex:0];
        //annotationView = [[AnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"MapDetails"];
        

    }
    
    annotationView.annotation = annotation;
    return annotationView;
}

- (void)mapView:(MKMapView *)mapView1 didSelectAnnotationView:(MKAnnotationView *)view
{
   // view.image = [UIImage imageNamed:@"map_listing_map_pin"];
    if([view isKindOfClass:[AnnotationView class]])
    {
        [txt_SearchBox resignFirstResponder];
        tblView_Category.hidden = YES;
        if(isCheckedShowQuickView == FALSE)
        {
            isCheckedShowQuickView = TRUE;
        }
        
        selectedView=view;
        selectedAnnotation=view.annotation;
        self.viewTool.selectedSegmentIndex = 0;
        MapAnnotation *annot = view.annotation;
        closedAnnot = view.annotation;
        placeidSub = annot.placeid;
        
        NSArray *filteredArray = [resturantArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"placeID = %@",placeidSub]];
        if(filteredArray>0)
        {
            restModal_DidSelectMap = [filteredArray firstObject];
            
            
            SelectedLat=[restModal_DidSelectMap.resturantLatitude floatValue];
            SelectedLog=[restModal_DidSelectMap.resturantLongtitude floatValue];
            
           // [self specialsHappyHoursServiceHelper];
            [self specialsServiceHelper];
            
            [self loadDataForQuickViewInSort:restModal_DidSelectMap];
        }
        
        //[self colorChangeOnPressTab:@[btn_QuickViewHappy,btn_quickViewDaily]];
        
        selectedAnnnotationImage=view.image;
        
        view.image=nil;
        isPinSelected = YES;
        [self setImageOnSelectedAnnotation:view withResturant:restModal_DidSelectMap];
        restaurantTag = view.tag;
        view.transform = CGAffineTransformMakeScale(1.2, 1.2);
        viewMapQuickDetails.hidden = FALSE;
        lbl_BtnBar.hidden = NO;
        view_CloseQuick.hidden = NO;
        viewMoreButton.hidden = FALSE;
        isShowQuickView = TRUE;
        
        annotationSelected=YES;
        
        CLLocationCoordinate2D cord1 = CLLocationCoordinate2DMake(view.annotation.coordinate.latitude, view.annotation.coordinate.longitude);
        
        MKCoordinateRegion region ;
      //  region.center = cord1;
        region = MKCoordinateRegionMakeWithDistance(cord1, 900.0, 900.0);
        region.span=mapView.region.span;
        [mapView setRegion:region animated:YES];
        [mapView regionThatFits:region];
        [self addQuickView];
        
        
       
    }
}

-(void)setImageOnSelectedAnnotation:(MKAnnotationView *)view withResturant:(ResturantModal *)resturant{
    
    int specialCount=[resturant.happy_hour_count intValue]+[resturant.place_specials_count intValue];

    ((AnnotationView *)view ).lblCount.text = [NSString stringWithFormat:@"%d",specialCount];
    //NSLog(@"resturant.place_specials_count : %@",resturant.place_specials_count);
    
    if(resturant.placeRatings.floatValue == 0)
    {
        ((AnnotationView *)view ).annotatonImage.image = [UIImage imageNamed:@"grayBlank"];
       // view.image = [UIImage imageNamed:@"grayBlank"];
    }
    else if(resturant.placeRatings.floatValue<=2)
    {
        ((AnnotationView *)view ).annotatonImage.image = [UIImage imageNamed:@"redBlank"];
      //  view.image = [UIImage imageNamed:@"redBlank"];
    }
    else if(resturant.placeRatings.floatValue<4)
    {
        ((AnnotationView *)view ).annotatonImage.image = [UIImage imageNamed:@"yellowBlank"];

        //view.image = [UIImage imageNamed:@"yellowBlank"];
    }else
    {
        ((AnnotationView *)view ).annotatonImage.image = [UIImage imageNamed:@"greenBlank"];

       // view.image = [UIImage imageNamed:@"greenBlank"];
    }

    if (isPinSelected == YES)
    {
        ((AnnotationView *)view ).annotatonImage.image = [UIImage imageNamed:@"map_listing_map_pin"];
    }
    
    /*
     if([resturant.placeType isEqualToString:@"restaurant"] || [resturant.placeType isEqualToString:@"hotel"])
     {
     if(resturant.placeRatings.floatValue == 0)
     {
     view.image = [UIImage imageNamed:@"selectedRestaurantGreyColor"];
     }
     else if(resturant.placeRatings.floatValue<=2)
     {
     view.image = [UIImage imageNamed:@"selectedRestaurantRedColor"];
     }
     else if(resturant.placeRatings.floatValue<4)
     {
     view.image = [UIImage imageNamed:@"selectedRestaurantYellowColor"];
     }
     else
     {
     view.image = [UIImage imageNamed:@"selectedRestaurantGreenColor"];
     }
     }
     else if([resturant.placeType isEqualToString:@"bar"] || [resturant.placeType isEqualToString:@"night_club"]){
     if(resturant.placeRatings.floatValue == 0){
     view.image = [UIImage imageNamed:@"selectedBarGreyColor"];
     }
     else if(resturant.placeRatings.floatValue<=2){
     view.image = [UIImage imageNamed:@"selectedBarRedColor"];
     }else if(resturant.placeRatings.floatValue<4){
     view.image = [UIImage imageNamed:@"selectedBarYellowColor"];
     }else{
     view.image = [UIImage imageNamed:@"selectedBarGreenColor"];
     }
     }
     
     else if([resturant.placeType isEqualToString:@"concert"]){
     
     if(resturant.placeRatings.floatValue == 0){
     view.image = [UIImage imageNamed:@"selectedConcertGreyColor"];
     }
     else if(resturant.placeRatings.floatValue<=2){
     view.image = [UIImage imageNamed:@"selectedConcertRedColor"];
     }else if(resturant.placeRatings.floatValue<4){
     view.image = [UIImage imageNamed:@"selectedConcertYellowColor"];
     }else{
     view.image = [UIImage imageNamed:@"selectedConcertGreenColor"];
     }
     }
     
     else if([resturant.placeType isEqualToString:@"museum"])
     {
     if(resturant.placeRatings.floatValue == 0){
     view.image = [UIImage imageNamed:@"selectedMuseumGreyColor"];
     }
     else if(resturant.placeRatings.floatValue<=2){
     view.image = [UIImage imageNamed:@"selectedMuseumRedColor"];
     }else if(resturant.placeRatings.floatValue<4){
     view.image = [UIImage imageNamed:@"selectedMuseumYellowColor"];
     }else{
     view.image = [UIImage imageNamed:@"selectedMuseumGreenColor"];
     }
     }
     else if([resturant.placeType isEqualToString:@"movie_theater"]){
     
     if(resturant.placeRatings.floatValue == 0){
     view.image = [UIImage imageNamed:@"selectedTheatreGreyColor"];
     }
     else if(resturant.placeRatings.floatValue<=2){
     view.image = [UIImage imageNamed:@"selectedTheatreRedColor"];
     }else if(resturant.placeRatings.floatValue<4){
     view.image = [UIImage imageNamed:@"selectedTheatreYellowColor"];
     }else{
     view.image = [UIImage imageNamed:@"selectedTheatreGreenColor"];
     }
     }
     else
     {
     if(resturant.placeRatings.floatValue == 0)
     {
     view.image = [UIImage imageNamed:@"selectedRestaurantGreyColor"];
     }
     else if(resturant.placeRatings.floatValue<=2)
     {
     view.image = [UIImage imageNamed:@"selectedRestaurantRedColor"];
     }
     else if(resturant.placeRatings.floatValue<4)
     {
     view.image = [UIImage imageNamed:@"selectedRestaurantYellowColor"];
     }else
     {
     view.image = [UIImage imageNamed:@"selectedRestaurantGreenColor"];
     }
     }
     */
}

- (void)mapView:(MKMapView *)mapView didDeselectAnnotationView:(MKAnnotationView *)view
{
    if([view isKindOfClass:[AnnotationView class]])
    {
        view.transform = CGAffineTransformMakeScale(1.0, 1.0);
        viewMapQuickDetails.hidden = TRUE;
        lbl_BtnBar.hidden = TRUE;
        view_CloseQuick.hidden = TRUE;
        viewMoreButton.hidden = TRUE;
        
        dispatch_async(dispatch_get_main_queue(), ^{
            view.image=selectedAnnnotationImage;
        });
        isShowQuickView = FALSE;
        annotationSelected=NO;
        isPinSelected = NO;
        
        [self setImageOnSelectedAnnotation:view withResturant:restModal_DidSelectMap];
        [HighlightView removeFromSuperview];
        [quick_mainView removeFromSuperview];
    }
}

-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer
shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}


- (void)dismissKeyboard {
    if(view_PressTab.hidden == FALSE)
    {
        view_PressTab.hidden = TRUE;
        [self.view endEditing:YES];
        [self.revealViewController revealToggle:0];
    }
}


- (void)loadDataForQuickViewInSort :(ResturantModal *)modal
{
    lblResName.text = @"";
    lblResAddress.text = @"";
    lblResPhoneNo.text = @"";
    lblWebSite.text = @"";
    btn_Emonji1.enabled = NO;
    btn_Emonji2.enabled = NO;
    btn_Emonji3.enabled = NO;
    btn_Emonji4.enabled = NO;
    if([modal.resturantEMail isEqualToString:@""])
    {
        _btnYellow.hidden = YES;
    }
    else{
        _btnYellow.hidden = NO;
    }
    
    if ([modal.isHighlights isEqualToString:@"0"])
    {
        btnHighlight.hidden = YES;
    }
    else{
        btnHighlight.hidden = NO;
    }
    
    lblResName.text = modal.resturantName.uppercaseString;
    
    NSString *detailedAddress;
    if([checkVC isEqualToString:@"restlist"])
    {
        
        detailedAddress = [NSString stringWithFormat:@"%@, %@, %@ %@",modal.resturantAddress,modal.resturantCity,modal.resturantState,modal.resturantPostalCode];
    }
    else
    {
        detailedAddress = [NSString stringWithFormat:@"%@, %@, %@ %@",modal.resturantAddress,modal.resturantCity,modal.resturantState,modal.resturantPostalCode];
    }
    
    //[lblResAddress setText:detailedAddress.uppercaseString];
    [lblResAddress setText:detailedAddress];
    lblResPhoneNo.text = modal.resturantPhoneNo;
    
    NSString *openingHours = modal.openFrom;
    lblResHours.text = openingHours;
    imgViewResImage.layer.cornerRadius = 3.0;
    [imgViewResImage setBackgroundColor:[UIColor blackColor]];
    imgViewResImage.layer.masksToBounds = YES;
    if ([modal.placeRatings isEqualToString:@"0"]){
        _lblRating.text = @"N/A";
    }else{
        _lblRating.text = modal.placeRatings;
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        NSURL *url = [[NSBundle mainBundle] URLForResource:@"loading123" withExtension:@"gif"];
        NSString *newUrl = [modal.placeImage stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
        [imgViewResImage sd_setImageWithURL:[NSURL URLWithString:newUrl]
                           placeholderImage:[UIImage imageNamed:@"noimage.png"]];
        
    });
    lblResLike.text = modal.likecount;
    lblResDislike.text = modal.dislikeCount;
    lblWebSite.text = modal.resturantEMail;
    if([modal.likeORDislike isEqualToString:@"1"] || [modal.likeORDislike isEqualToString:@"2"])
    {
        if([modal.likeORDislike isEqualToString:@"1"])
        {
            btnResLike.enabled = YES;
            btnResDislike.enabled = YES;
        }
        else
        {
            btnResLike.enabled = YES;
            btnResDislike.enabled = YES;
        }
    }
    else
    {
        btnResLike.enabled = YES;
        btnResDislike.enabled = YES;
    }
    
    if(modal.isCheckedFavourite == NO)
    {
        [btnResFavourite setImage:[UIImage imageNamed:@"FavUnSelLIst"] forState:UIControlStateNormal];
    }
    else
    {
        [btnResFavourite setImage:[UIImage imageNamed:@"FavSelLIst"] forState:UIControlStateNormal];
    }
    
    if(IS_IPHONE_6PLUS)
    {
        if(modal.placeRatings.floatValue <= 2.0)
        {
            starRatingView.starHighlightedImage = [UIImage imageNamed:@"star-1_R.png"];
        }
        else if(modal.placeRatings.floatValue <= 3.9)
        {
            starRatingView.starHighlightedImage = [UIImage imageNamed:@"star-1_Y.png"];
        }
        else
        {
            starRatingView.starHighlightedImage = [UIImage imageNamed:@"star-1_G.png"];
        }
        starRatingView.starImage = [UIImage imageNamed:@"unstartemp.png"];
    }
    else if (IS_IPHONE_6)
    {
        if(modal.placeRatings.floatValue <= 2.0)
        {
            starRatingView.starHighlightedImage = [UIImage imageNamed:@"star-1_R.png"];
        }
        else if(modal.placeRatings.floatValue <= 3.9)
        {
            starRatingView.starHighlightedImage = [UIImage imageNamed:@"star-1_Y.png"];
        }
        else
        {
            starRatingView.starHighlightedImage = [UIImage imageNamed:@"star-1_G.png"];
        }
        starRatingView.starImage = [UIImage imageNamed:@"unstartemp.png"];
        
    }
    else
    {
        starRatingView.starImage = [UIImage imageNamed:@"UnStarIcon"];
        if(modal.placeRatings.floatValue <= 2.0)
        {
            starRatingView.starHighlightedImage = [UIImage imageNamed:@"RedStarIcon"];
            
        }
        else if(modal.placeRatings.floatValue <= 3.9)
        {
            starRatingView.starHighlightedImage = [UIImage imageNamed:@"YellowStarIcon"];
        }
        else
        {
            starRatingView.starHighlightedImage = [UIImage imageNamed:@"GreenStarIcon"];
        }
    }
    starRatingView.maxRating = 5.0;
    starRatingView.delegate = self;
    starRatingView.horizontalMargin = 12;
    starRatingView.editable = NO;
    starRatingView.rating = modal.placeRatings.floatValue;
    starRatingView.displayMode = EDStarRatingDisplayAccurate;
    
    [self setEmonjis:1 type:modal.emonjiID.intValue];
    [self setEmonjis:2 type:modal.emonjiID2.intValue];
    [self setEmonjis:3 type:modal.emonjiID3.intValue];
    [self setEmonjis:4 type:modal.emonjiID4.intValue];
}
- (void) loadDataForQuickView
{
    lblResName.text    = @"";
    lblResAddress.text = @"";
    lblResPhoneNo.text = @"";
    lblWebSite.text    = @"";
    btn_Emonji1.enabled = NO;
    btn_Emonji2.enabled = NO;
    btn_Emonji3.enabled = NO;
    btn_Emonji4.enabled = NO;
    
    
    ResturantModal *modal =[resturantArray objectAtIndex:restaurantTag];
    lblResName.text = modal.resturantName.uppercaseString;
    
    
    NSString *detailedAddress;
    if([checkVC isEqualToString:@"restlist"])
    {
        detailedAddress = [NSString stringWithFormat:@"%@,\n%@,%@ %@",modal.resturantAddress,modal.resturantCity,modal.resturantState,modal.resturantPostalCode];
    }
    else
    {
        detailedAddress = [NSString stringWithFormat:@"%@,\n%@,%@ %@",modal.resturantAddress,modal.resturantCity,modal.resturantState,modal.resturantPostalCode];
    }
    
    [lblResAddress setText:detailedAddress];
    lblResPhoneNo.text = modal.resturantPhoneNo;
    
    NSString *openingHours = modal.openFrom;
    lblResHours.text = openingHours;
    if ([modal.placeRatings isEqualToString:@"0"]) {
        _lblRating.text = @"N/A";
    }else{
        _lblRating.text = modal.placeRatings;
    }
    imgViewResImage.layer.cornerRadius = 3.0;
    [imgViewResImage setBackgroundColor:[UIColor blackColor]];
    imgViewResImage.layer.masksToBounds = YES;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        NSURL *url = [[NSBundle mainBundle] URLForResource:@"loading123" withExtension:@"gif"];
        NSString *newUrl = [modal.placeImage stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
        [imgViewResImage sd_setImageWithURL:[NSURL URLWithString:newUrl]
                           placeholderImage:[UIImage imageNamed:@"noimage.png"]];
        
    });
    lblResLike.text = modal.likecount;
    lblResDislike.text = modal.dislikeCount;
    lblWebSite.text = modal.resturantEMail;
    if([modal.likeORDislike isEqualToString:@"1"] || [modal.likeORDislike isEqualToString:@"2"])
    {
        if([modal.likeORDislike isEqualToString:@"1"])
        {
            btnResLike.enabled = YES;
            btnResDislike.enabled = YES;
        }
        else
        {
            btnResLike.enabled = YES;
            btnResDislike.enabled = YES;
        }
    }
    else
    {
        btnResLike.enabled = YES;
        btnResDislike.enabled = YES;
    }
    
    if(modal.isCheckedFavourite == NO)
    {
        btnResFavourite.selected = NO;
    }
    else
    {
        btnResFavourite.selected = YES;
    }
    
    if(IS_IPHONE_6PLUS)
    {
        starRatingView.starImage = [UIImage imageNamed:@"unstartemp.png"];
        starRatingView.starHighlightedImage = [UIImage imageNamed:@"star-1.png"];
    }
    else if (IS_IPHONE_6)
    {
        starRatingView.starImage = [UIImage imageNamed:@"unstartemp.png"];
        starRatingView.starHighlightedImage = [UIImage imageNamed:@"star-1.png"];
        
    }
    else
    {
        starRatingView.starImage = [UIImage imageNamed:@"UnStarIcon"];
        starRatingView.starHighlightedImage = [UIImage imageNamed:@"StarIcon"];
    }
    
    starRatingView.maxRating = 5.0;
    starRatingView.delegate = self;
    starRatingView.horizontalMargin = 12;
    starRatingView.editable = YES;
    starRatingView.rating = modal.placeRatings.floatValue;
    starRatingView.displayMode = EDStarRatingDisplayAccurate;
    
    [self setEmonjis:1 type:modal.emonjiID.intValue];
    [self setEmonjis:2 type:modal.emonjiID2.intValue];
    [self setEmonjis:3 type:modal.emonjiID3.intValue];
    [self setEmonjis:4 type:modal.emonjiID4.intValue];
}


#pragma mark --- TextField Delegate Method

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    viewMapQuickDetails.hidden = TRUE;
    lbl_BtnBar.hidden = YES;
    view_CloseQuick.hidden = TRUE;
    viewMoreButton.hidden = TRUE;
    _isCheckedCityStatus=YES;
    [mapView deselectAnnotation:closedAnnot animated:YES];
    [HighlightView removeFromSuperview];
    [quick_mainView removeFromSuperview];
    tableCellSelected=NO;
}

-(BOOL)textFieldShouldClear:(UITextField *)textField{
    textField.text = @"";
    [textField resignFirstResponder];
    tblView_Category.hidden = YES;
    return NO;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    SelectedLat=0.0000;
    SelectedLog =0.0000;
    [self btnResetFilterClick:nil];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"ischeckedshowquickview"];
    
    if (HappyHoursSelected == 0)
        tblView_DailySpecial.hidden =YES;
    if (DailySpecialSelected == 0)
        tblView_DailySpecial.hidden =YES;
    
    zoomEnable=NO;
    if (txt_SearchBox.text.length>0) {
        manuallySearchLocation=NO;
    }else{
        manuallySearchLocation=NO;

    }
    GlobalRadius=@"";
    SearchFixedPlace=NO;
    
    
    if (!tableCellSelected) {
        //txt_SearchBox.text = @"";
        [txt_SearchBox resignFirstResponder];
        tblView_Category.hidden = YES;
        return true;
        
        NSMutableArray *localArry = [NSMutableArray new];
        
        for (RegisterModal *eventsData in _cityArray)
        {
            NSRange range=[eventsData.placeType rangeOfString:txt_SearchBox.text options:(NSCaseInsensitiveSearch)];
            
            if(range.location != NSNotFound)
                [localArry addObject:eventsData];
        }
        
        
        for (RegisterModal *eventsData in _categoryarray)
        {
            NSRange range=[eventsData.placeType rangeOfString:txt_SearchBox.text options:(NSCaseInsensitiveSearch)];
            
            if(range.location != NSNotFound)
                [localArry addObject:eventsData];
        }
        
        if (localArry.count>0) {
            RegisterModal *modal = [localArry objectAtIndex:0];
            NSRange range=[modal.placeType rangeOfString:@", IL" options:(NSCaseInsensitiveSearch)];
            NSRange range1=[modal.placeType rangeOfString:@", Chicago" options:(NSCaseInsensitiveSearch)];
            
            if(range.location != NSNotFound || range1.location != NSNotFound)
            {
                _isCheckedCityStatus = TRUE;
            }
            else if([modal.statusCity isEqualToString:@"City"])
            {
                _isCheckedCityStatus = TRUE;
            }
            else
            {
                _isCheckedCityStatus = FALSE;
                
                placeidSub=tableCellSelected?placeidSub:modal.placeID;
                //placeidSub=modal.placeID;
            }
            
        }else
            _isCheckedCityStatus = TRUE;
        
    }
    
    if(textField.text.length>0){
        [[NSUserDefaults standardUserDefaults]setBool:FALSE forKey:@"randomsearch"];
        
        if(_isCheckedCityStatus == TRUE)
            [self getAddressLatLong:textField.text];
        else
        {
            ResturantModal *modal = [[ResturantModal alloc]init];
            [modal setUserID:user_IDRegister];
            [modal setResturantLatitude:stringLat];
            [modal setResturantLongtitude:stringLong];
            NSString *strReplace = [textField.text stringByReplacingOccurrencesOfString:@"," withString:@""];
            strReplace = [strReplace stringByReplacingOccurrencesOfString:@"IL" withString:@""];
            [modal setSystemMessage:strReplace];
            [modal setTabs:@"firsttab"];
            [self loadMapValuesServiceHelper:modal];
        }
    }
    tblView_Category.hidden = YES;
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField;
{
    if (textField == txt_SearchBox)
    {
        //tableCellSelected = YES;
        return true;
    }
    return true;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(txt_SearchBox.text.length == 1 && [string isEqualToString:@""]){
        tblView_Category.hidden = YES;
        txt_SearchBox.text = @"";
        [textField resignFirstResponder];}
    else{
        tblView_Category.hidden = NO;
        NSString *substring = [NSString stringWithString:textField.text];
        substring = [substring stringByReplacingCharactersInRange:range withString:string];
        
        [self searchByEvents:substring];
    }
    return YES;
}


- (void)searchByEvents:(NSString *)searchStr
{
    NSArray *TempNSArray = [[NSArray alloc]init];
    [tempArray removeAllObjects];
    
    if([searchStr length] > 0)
    {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"placeType beginswith[c] %@",  searchStr];
        TempNSArray = [_cityArray filteredArrayUsingPredicate:predicate];
        if (TempNSArray.count>0)
        {
            [tempArray addObjectsFromArray:TempNSArray];
        }
        TempNSArray = [_categoryarray filteredArrayUsingPredicate:predicate];
        
        if (TempNSArray.count>0)
        {
            [tempArray addObjectsFromArray:TempNSArray];
        }
    }
    else{
        
        [tempArray removeAllObjects];
    }
    
    if(tempArray.count>0)
    {
        tblView_Category.hidden = NO;
        [tblView_Category reloadData];
    }
    else
    {
        tblView_Category.hidden = YES;
    }
    
    [self scrollViewDidScroll:tblView_Category];
    
    NSLog(@"data == %@",tempArray);
}

#pragma mark --  MFMailComposeViewControllerDelegate Methods

-(void)reportAction
{
    NSString *emailTitle = restModal_DidSelectMap.resturantName.uppercaseString;
    // Email Content
    //NSString *messageBody = resturantModalObj.resturantDescription;
    NSString *messageBody = @"";
    // To address
    //contact@spotlytespecials.com
    NSArray *toRecipents = [NSArray arrayWithObject:@"contact@spotlytespecials.com"];
    
    MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
    if ([MFMailComposeViewController canSendMail]) {
        mc.mailComposeDelegate = self;
        [mc setSubject:emailTitle];
        [mc setMessageBody:messageBody isHTML:NO];
        [mc setToRecipients:toRecipents];
        
        // Present mail view controller on screen
        [self presentViewController:mc animated:YES completion:NULL];
    }
}



- (void)mailComposeController:(MFMailComposeViewController*)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError*)error;
{
    checkWillAppear = @"Dismiss";
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            [self dismissViewControllerAnimated:YES completion:NULL];
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            [self dismissViewControllerAnimated:YES completion:NULL];
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            [self dismissViewControllerAnimated:YES completion:NULL];
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            [self dismissViewControllerAnimated:YES completion:NULL];
            break;
        default:
            break;
    }
    // Close the Mail Interface
}

#pragma mark -- Action Methods
- (IBAction)btnYellow_click:(id)sender {
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:restModal_DidSelectMap.resturantName message:@"Specials and Hours are subject to change. To ensure the most up-to-date specials you can view this locations website specials here" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *YesBtn = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                             {
                                 
                                   // [selectedResturant setWebSite_Specials:modal.webSite_Specials];
                                     if(restModal_DidSelectMap.webSite_Specials.length>0)
                                     {
                                         TermsViewController *termsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"termsvc"];
                                         termsVC.checkVC = @"promotions";
                                         termsVC.checkBack = @"homevc";
                                         termsVC.CheckUrlType=@"SpecialLink";
                                         termsVC.restModal_TermsVC = restModal_DidSelectMap;
                                         [self.navigationController pushViewController:termsVC animated:YES];
                                     }
                                     else
                                     {
                                         
                                         TermsViewController *termsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"termsvc"];
                                         termsVC.checkVC = @"promotions";
                                         termsVC.checkBack = @"homevc";
                                         termsVC.CheckUrlType=@"ResturantEMail";
                                         termsVC.restModal_TermsVC =restModal_DidSelectMap;
                                         [self.navigationController pushViewController:termsVC animated:YES];
                                     }
                                     
                                 
                                 
                             }];
    
    UIAlertAction *NoBtn = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                            {
                                [self dismissViewControllerAnimated:YES completion:nil];
                            }];
    
    [alertController addAction:YesBtn];
    [alertController addAction:NoBtn];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

- (IBAction)btnHighlight_Click:(id)sender {
   // [self.view addSubview:HighlightView];

    [self GetHighlightsOfPlaces];
}
- (IBAction)btnHighLightCancel_Click:(id)sender {
    [HighlightView removeFromSuperview];
}


- (void)btnFavSpecialClick:(id)sender {
    UIButton *btn = (UIButton*)sender;
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:btn.tag inSection:0];
    DailySpecialCell *cell = (DailySpecialCell *)[tableView_DailySpecial cellForRowAtIndexPath:indexPath];
        ResturantModal *modelDailySpecialDetails  = [arraySpecialforday objectAtIndex:btn.tag];
    NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:@"logindetails"];
    RegisterModal *registerModalResponeValue = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    NSString *strType;
    if(self.viewTool.selectedSegmentIndex == 0){
        strType = @"h";
    }
    else{
        strType = @"s";
    }
    
    NSString *strFav;
    if(btn.selected == true){
        strFav = @"2";
    }
    else{
        strFav = @"1";
    }
    
    BOOL check=[[AppDelegate shareAppdelegateInstance] checkReachability];
    if(check)
    {
        [modelDailySpecialDetails setUserID:registerModalResponeValue.userID];
        [CUSTOMINDICATORMACRO startLoadAnimation:self];
        [RestaurantMacro favUnfavSpecialToServer:modelDailySpecialDetails :strFav :strType withCompletion:^(id obj1) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [CUSTOMINDICATORMACRO stopLoadAnimation:self];

                ResturantModal *responseModal = nil;
                if([obj1 isKindOfClass:[ResturantModal class]])
                {
                    responseModal = obj1;
                    if([responseModal.result isEqualToString:@"success"]){
                        if(btn.selected == true){
                            [modelDailySpecialDetails setIsCheckedFavSpecial:NO];
                        }
                        else{
                            [modelDailySpecialDetails setIsCheckedFavSpecial:YES];
                        }
                    
                        if(btn.selected == true){
                            cell.btnFavSpecial.selected = false;
                            [cell.btnFavSpecial setImage:[UIImage imageNamed:@"unFavPin"] forState:UIControlStateNormal];
                           // [cell.btnFavSpecial setSelected:false];
                        }
                        else{
                            cell.btnFavSpecial.selected = true;
                            [cell.btnFavSpecial setImage:[UIImage imageNamed:@"favPin"] forState:UIControlStateNormal];
                            //[cell.btnFavSpecial setSelected:true];
                        }
                       // [tableView_DailySpecial reloadData];
                    }
                }
                else
                   // [self serverError];
                
                [CUSTOMINDICATORMACRO stopLoadAnimation:self];
            });
        }];
    }
}
-(IBAction)pressCloseBtnQuickView:(id)sender
{
    [quick_mainView removeFromSuperview];
    [HighlightView removeFromSuperview];
    
    // self.tabBarController.tabBar.hidden=NO;
    SelectedLat=0.0000;
    
    SelectedLog =0.0000;
    
    [mapView deselectAnnotation:closedAnnot animated:YES];
    
    viewMapQuickDetails.hidden = TRUE;
    lbl_BtnBar.hidden = TRUE;
    view_CloseQuick.hidden = TRUE;
    viewMoreButton.hidden = TRUE;
    isShowQuickView = FALSE;
}


-(IBAction)pressCurrentLocation:(id)sender
{
    [self searchFromCurrentLocation];
}

-(void)searchFromCurrentLocation{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        locationManager=nil;
        if (!locationManager) {
            [self CurrentLocationIdentifier];
        }
        
        SearchFixedPlace=YES;
        _currentLocationEnable=YES;
        
        [[NSUserDefaults standardUserDefaults]setBool:TRUE forKey:@"checkmap"];
        SelectedLat=0.0000;
        SelectedLog =0.0000;
        mapView.showsUserLocation = YES;
        isCheckedLocation = TRUE;
        txt_SearchBox.text = @"";
        viewMapQuickDetails.hidden = YES;
        lbl_BtnBar.hidden = YES;
        view_CloseQuick.hidden = TRUE;
        [quick_mainView removeFromSuperview];
        [HighlightView removeFromSuperview];
        viewMoreButton.hidden = YES;
        isShowQuickView = FALSE;
        GlobalRadius = @"1";
        [mapView removeAnnotations:mapView.annotations];
        [locationManager startUpdatingLocation];
        
        // [self getAddress:locationManager.location];
        
        CLLocationCoordinate2D cord= CLLocationCoordinate2DMake(locationManager.location.coordinate.latitude,locationManager.location.coordinate.longitude);
        
        MKCoordinateRegion region;
        region.center=cord;
        region.span.latitudeDelta=0.010;
        region.span.longitudeDelta=0.010;
        [mapView setRegion:region animated:YES];
        [mapView regionThatFits:region];
        //[locationManager stopUpdatingLocation];
    });
}

-(IBAction)pressRandomSearch:(id)sender
{
    SelectedLat=0.0000;
    SelectedLog =0.0000;
    
    isCheckedLocation = FALSE;
    [[NSUserDefaults standardUserDefaults]setBool:TRUE forKey:@"randomsearch"];
    txt_SearchBox.text = @"";
    viewMapQuickDetails.hidden = YES;
    lbl_BtnBar.hidden = YES;
    view_CloseQuick.hidden = TRUE;
    viewMoreButton.hidden = YES;
    [quick_mainView removeFromSuperview];
    [HighlightView removeFromSuperview];
    [mapView removeAnnotations:mapView.annotations];
    stringLong =  longStr;
    stringLat  =  latStr;
    NSLog(@"lat == %@ long == %@",stringLat,stringLong);
    isCheckedLazyLoading = TRUE;
    
    
    CLLocationCoordinate2D centre = [mapView centerCoordinate];
    longStr=[NSString stringWithFormat:@"%f", centre.longitude];
    latStr=[NSString stringWithFormat:@"%f", centre.latitude];
    
    CLLocation *searchLocation = [[CLLocation alloc] initWithLatitude:latStr.floatValue longitude:longStr.floatValue];
    [self getLatLongBasedOnRefresh:searchLocation];
}

-(IBAction)pressReportHereBtn : (id)sender
{
    ResturantModal *modal = [arraySpecialforday objectAtIndex:0];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:restModal_DidSelectMap.resturantName message:@"Specials and Hours are subject to change. To ensure the most up-to-date specials you can view this locations website specials here" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *YesBtn = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                             {
                                 
                                     [restModal_DidSelectMap setWebSite_Specials:modal.webSite_Specials];
                                     if(modal.webSite_Specials.length>0)
                                     {
                                         TermsViewController *termsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"termsvc"];
                                         termsVC.checkVC = @"promotions";
                                         termsVC.checkBack = @"homevc";
                                         termsVC.CheckUrlType=@"SpecialLink";
                                         termsVC.restModal_TermsVC = restModal_DidSelectMap;
                                         [self.navigationController pushViewController:termsVC animated:YES];
                                     }
                                     else
                                     {
                                         
                                         TermsViewController *termsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"termsvc"];
                                         termsVC.checkVC = @"promotions";
                                         termsVC.checkBack = @"homevc";
                                         termsVC.CheckUrlType=@"ResturantEMail";
                                         termsVC.restModal_TermsVC = restModal_DidSelectMap;
                                         [self.navigationController pushViewController:termsVC animated:YES];
                                     }
                                     
                                
                                 
                             }];
    
    UIAlertAction *NoBtn = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                            {
                                [self dismissViewControllerAnimated:YES completion:nil];
                            }];
    
    [alertController addAction:YesBtn];
    [alertController addAction:NoBtn];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

-(IBAction)pressSpecialHappyClickHereBtn : (id)sender
{
    [self reportAction];
}
- (IBAction)btnClearFilter_Click:(id)sender
{
    btnClearFilter.hidden = true;
    txt_SearchBox.text = @"";
   // [self pressSearchBtn:nil];
   // [self loadMapValuesServiceHelper:lastModal];
}

- (IBAction)btnFilterClick:(id)sender
{
    //    btnSundayDaily.selected = false;
    //    btnMondayDaily.selected = false;
    //    btnTuesdayDaily.selected = false;
    //    btnWednesdayDaily.selected = false;
    //    btnThuesdayDaily.selected = false;
    //    btnFridayDaily.selected = false;
    //    btnSaturdayDaily.selected = false;
    //
    //    btnSundayHappy.selected = false;
    //    btnMondayHappy.selected = false;
    //    btnTuesdayHappy.selected = false;
    //    btnWednesdayHappy.selected = false;
    //    btnThuesdayHappy.selected = false;
    //    btnFridayHappy.selected = false;
    //    btnSaturdayHappy.selected = false;
    //
    //    [array_SelectedDailySpecial removeAllObjects];
    //    [array_SelectedHappyHours removeAllObjects];
    //    [array_SelectedTopSpots removeAllObjects];
    //
    //    [tblView_TopSpots reloadData];
    
    view_Filter.hidden = false;
}

- (IBAction)btnDoneFilterClick:(id)sender
{
    self.strNE_Long = @"";
    view_Filter.hidden = true;
    //btn_Filter.selected = NO;
    //imgArrowDown.transform = CGAffineTransformMakeRotation(0);
   
    
    ResturantModal *modal = [[ResturantModal alloc]init];
    [modal setUserID:user_IDRegister];
    [modal setResturantLatitude:stringLat];
    [modal setResturantLongtitude:stringLong];
   // [modal setSystemMessage:SearchFixedPlace?@"":[GlobalRadius isEqualToString:@""]?txt_SearchBox.text:@""];
    if (txt_SearchBox.text.length > 0)
    {
        GlobalRadius = @"";
        [modal setCityOrArea:_strCityOrPlace];
        [modal setTabs:@"firsttab"];
        [modal setSystemMessage:txt_SearchBox.text];
    }
    else{
        GlobalRadius = @"";
        _strCityOrPlace = @"";
        [modal setCityOrArea:@""];
        [modal setTabs:@""];
    }
    isKeepDraging = NO;
     manuallySearchLocation = YES;
    
    if(array_SelectedDailySpecial.count > 0 || array_SelectedHappyHours.count > 0 || array_SelectedTopSpots.count > 0){
        [btn_Filter setImage:[UIImage imageNamed:@"filter_yellow"] forState:UIControlStateNormal];
    }
    else
    {
        [btn_Filter setImage:[UIImage imageNamed:@"filter"] forState:UIControlStateNormal];
        if (txt_SearchBox.text.length > 0)
        {
            self.strSW_Lat = @"";
            self.strNE_Lat = @"";
            self.strNE_Long = @"";
            self.strSW_Long = @"";
        }
    }
    
   

    
    [self loadMapValuesServiceHelper:modal];
}

- (IBAction)btnDismissFilterClick:(id)sender
{
    view_Filter.hidden = true;
}

- (IBAction)btnResetFilterClick:(id)sender
{
    //view_Filter.hidden = true;
    //btn_Filter.selected = NO;
    //imgArrowDown.transform = CGAffineTransformMakeRotation(0);
    
    btnSundayDaily.selected = false;
    btnMondayDaily.selected = false;
    btnTuesdayDaily.selected = false;
    btnWednesdayDaily.selected = false;
    btnThuesdayDaily.selected = false;
    btnFridayDaily.selected = false;
    btnSaturdayDaily.selected = false;
    
    btnSundayHappy.selected = false;
    btnMondayHappy.selected = false;
    btnTuesdayHappy.selected = false;
    btnWednesdayHappy.selected = false;
    btnThuesdayHappy.selected = false;
    btnFridayHappy.selected = false;
    btnSaturdayHappy.selected = false;
    
    [array_SelectedDailySpecial removeAllObjects];
    [array_SelectedHappyHours removeAllObjects];
    [array_SelectedTopSpots removeAllObjects];
    
    [btn_Filter setImage:[UIImage imageNamed:@"filter"] forState:UIControlStateNormal];

        [tblView_TopSpots reloadData];
}

- (IBAction)pressHappyHours:(id)sender
{
    //    CommanRow=HappyHoursSelected;
    //
    //    SelectedLat=0.0000;
    //
    //    SelectedLog =0.0000;
    //
    //    if(btn_HappyHours.isSelected == YES)
    //    {
    //        [HappHoursTable_lbl setUserInteractionEnabled:NO];
    //
    //        btn_HappyHours.selected = NO;
    //        tblView_DailySpecial.hidden = YES;
    //        isCheckedHappyHours = FALSE;
    //        isSelectedValueHappy = FALSE;
    //        string_HappyHours = @"";
    //
    //        ResturantModal *modal = [[ResturantModal alloc]init];
    //        [modal setUserID:user_IDRegister];
    //        [modal setResturantLatitude:stringLat];
    //        [modal setResturantLongtitude:stringLong];
    //        [modal setSystemMessage:SearchFixedPlace?@"":[GlobalRadius isEqualToString:@"0"]?txt_SearchBox.text:@""];
    //        [self loadMapValuesServiceHelper:modal];
    //    }
    //    else
    //    {
    //        CommanRow=0;
    //        [HappHoursTable_lbl setUserInteractionEnabled:YES];
    //        btn_HappyHours.selected = YES;
    //        tblView_X.constant = 30;
    //        tblView_DailySpecial.hidden = NO;
    //        isCheckedHappyHours = TRUE;
    //        isSelectedValueHappy = TRUE;
    //
    //    }
    //    [tblView_DailySpecial reloadData];
}
- (IBAction)pressViewMap:(id)sender
{
    //    if(btn_ViewMap.isSelected == YES)
    //    {
    //        btn_ViewMap.selected = NO;
    //        isSelectedValuesIsLive = FALSE;
    //        ResturantModal *modal = [[ResturantModal alloc]init];
    //        [modal setUserID:user_IDRegister];
    //        [modal setResturantLatitude:stringLat];
    //        [modal setResturantLongtitude:stringLong];
    //        [self loadMapValuesServiceHelper:modal];
    //    }
    //    else
    //    {
    //        btn_ViewMap.selected = YES;
    //        isSelectedValuesIsLive = TRUE;
    //    }
    //    ResturantModal *modal = [[ResturantModal alloc]init];
    //    [modal setUserID:user_IDRegister];
    //    [modal setResturantLatitude:stringLat];
    //    [modal setResturantLongtitude:stringLong];
    //    [self loadMapValuesServiceHelper:modal];
    //    tblView_DailySpecial.hidden = YES;
}

- (IBAction)pressDailySpecials:(id)sender
{
    //    SelectedLat=0.0000;
    //    SelectedLog =0.0000;
    //
    //    CommanRow=DailySpecialSelected;
    //
    //    if(btn_Daily.isSelected == YES)
    //    {
    //
    //        [DailySpecial_lbl setUserInteractionEnabled:NO];
    //
    //        btn_Daily.selected = NO;
    //        tblView_DailySpecial.hidden = YES;
    //        isCheckedDailySpecials = FALSE;
    //        isSelectedValueSspecials = FALSE;
    //        string_DailySpecials = @"";
    //        ResturantModal *modal = [[ResturantModal alloc]init];
    //        [modal setUserID:user_IDRegister];
    //        [modal setResturantLatitude:stringLat];
    //        [modal setResturantLongtitude:stringLong];
    //        [modal setSystemMessage:SearchFixedPlace?@"":[GlobalRadius isEqualToString:@"0"]?txt_SearchBox.text:@""];
    //        [self loadMapValuesServiceHelper:modal];
    //
    //    }
    //    else
    //    {
    //        CommanRow=0;
    //        [DailySpecial_lbl setUserInteractionEnabled:YES];
    //        btn_Daily.selected = YES;
    //        tblView_DailySpecial.hidden = NO;
    //        tblView_X.constant = 0;
    //        isCheckedDailySpecials = TRUE;
    //        isSelectedValueSspecials = TRUE;
    //    }
    //
    //    [tblView_DailySpecial reloadData];
}

- (IBAction)pressback:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


- (IBAction)HappHoursTable:(id)sender
{
    
    if (HappyHourseBool_lbl==TRUE)
    {
        
        
        CommanRow=HappyHoursSelected;
        
        tblView_DailySpecial.hidden=NO;
        [tblView_DailySpecial reloadData];
        
        HappyHourseBool_lbl=FALSE;
        
    }else
    {
        
        CommanRow=HappyHoursSelected;
        
        tblView_DailySpecial.hidden=YES;
        [tblView_DailySpecial reloadData];
        
        HappyHourseBool_lbl=TRUE;
    }
}

- (IBAction)MenuTapBtn:(id)sender{
    MenuTapBtn.hidden=YES;
    [self.revealViewController revealToggle:sender];
}

- (IBAction)DailySpecial:(id)sender{
    if (DailySpecialBool_lbl==TRUE)
    {
        CommanRow=DailySpecialSelected;
        
        tblView_DailySpecial.hidden=NO;
        [tblView_DailySpecial reloadData];
        
        DailySpecialBool_lbl=FALSE;
        
    }else
    {
        
        CommanRow=DailySpecialSelected;
        
        tblView_DailySpecial.hidden=YES;
        [tblView_DailySpecial reloadData];
        
        DailySpecialBool_lbl=TRUE;
    }
}

-(IBAction)pressProfileBtn:(id)sender
{
    MyProfileViewController *profileVC = [self.storyboard instantiateViewControllerWithIdentifier:@"myprofile"];
    profileVC.selectedController = @"currentuser";
    profileVC.selectedTweet = 0;
    profileVC.user_IDString = registerModalResponeValue.userID;
    [self.navigationController pushViewController:profileVC animated:YES];
    
}

-(IBAction)pressAdvancedSearch:(id)sender
{
    customSearchViewController *customVC = [self.storyboard instantiateViewControllerWithIdentifier:@"advancedsearch"];
    customVC.responseArray=_cityArray;
    [self.navigationController pushViewController:customVC animated:YES];
}

- (IBAction)pressNavigateSpecialHours:(id)sender
{
    [quick_mainView removeFromSuperview];
    [HighlightView removeFromSuperview];
    PromotionsViewController *promtionVC = [self.storyboard instantiateViewControllerWithIdentifier:@"promtions"];
    promtionVC.selectedValue = restaurantTag;
    promtionVC.placeIDString = restModal_DidSelectMap.placeID;
    promtionVC.promtionsArray = resturantArray;
    promtionVC.selectedWhichController = @"specialshappyhours";
    
    BOOL isCheckedRandomSearch = [[NSUserDefaults standardUserDefaults]boolForKey:@"randomsearch"];
    if(isCheckedRandomSearch == TRUE)
    {
        promtionVC.checkTextOrSearch = @"customsearch";
    }
    else
    {
        promtionVC.checkTextOrSearch = @"nosearch";
        [[NSUserDefaults standardUserDefaults]setBool:FALSE forKey:@"randomsearch"];
    }
    [self.navigationController pushViewController:promtionVC animated:YES];
    
    promtionVC.selectHomeVC=^(ResturantModal *restModel){
        
        promotionModalOBJ = restModel;
    };
    
}

-(void) openPromotionsViewController:(id)sender{
    
    
        CATransition *transition = [CATransition animation];
        transition.duration = 0.5;
        transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromTop;
    
        PromotionsViewController *promtionVC = [self.storyboard instantiateViewControllerWithIdentifier:@"promtions"];
        promtionVC.selectedValue = restaurantTag;
        promtionVC.placeIDString = restModal_DidSelectMap.placeID;
        promtionVC.promtionsArray = resturantArray;
        
        BOOL isCheckedRandomSearch = [[NSUserDefaults standardUserDefaults]boolForKey:@"randomsearch"];
        if(isCheckedRandomSearch == TRUE)
        {
            promtionVC.checkTextOrSearch = @"customsearch";
        }
        else
        {
            promtionVC.checkTextOrSearch = @"nosearch";
            [[NSUserDefaults standardUserDefaults]setBool:FALSE forKey:@"randomsearch"];
        }
        promtionVC.selectedWhichController = @"eventSection";
        [self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
        [self.navigationController pushViewController:promtionVC animated:NO];
        
        promtionVC.selectHomeVC=^(ResturantModal *restModel){
            promotionModalOBJ = restModel;
        };
}


-(IBAction)pressViewMore:(id)sender
{
    PromotionsViewController *promtionVC = [self.storyboard instantiateViewControllerWithIdentifier:@"promtions"];
    promtionVC.selectedValue = restaurantTag;
    promtionVC.placeIDString = restModal_DidSelectMap.placeID;
    promtionVC.promtionsArray = resturantArray;
    
    BOOL isCheckedRandomSearch = [[NSUserDefaults standardUserDefaults]boolForKey:@"randomsearch"];
    if(isCheckedRandomSearch == TRUE)
    {
        promtionVC.checkTextOrSearch = @"customsearch";
    }
    else
    {
        promtionVC.checkTextOrSearch = @"nosearch";
        [[NSUserDefaults standardUserDefaults]setBool:FALSE forKey:@"randomsearch"];
    }
    promtionVC.selectedWhichController = @"eventSection";
    [self.navigationController pushViewController:promtionVC animated:YES];
    
    promtionVC.selectHomeVC=^(ResturantModal *restModel){
        
        promotionModalOBJ = restModel;
    };
}

-(IBAction)pressSpotLight:(id)sender
{
    spotlytePromotionViewController *spotLightVC = [self.storyboard instantiateViewControllerWithIdentifier:@"spotlight"];
    [self.navigationController pushViewController:spotLightVC animated:YES];
}
-(IBAction)pressResLike:(id)sender
{
    NSInteger  strLike = [lblResLike.text integerValue];
    NSInteger  strDisLike = [lblResDislike.text integerValue];
    ResturantModal *modal = [[ResturantModal alloc]init];
    
    NSArray *filteredArray = [resturantArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"placeID ==%@",placeidSub]];
    if(filteredArray>0)
    {
        modal = [filteredArray firstObject];
    }
    
    if(![modal.likeORDislike isEqualToString:@"1"])
    {
        [modal setUserID:registerModalResponeValue.userID];
        NSString *checkcount  = modal.likeORDislike;
        [modal setLikeORDislike:@"1"];
        
        BOOL check=[[AppDelegate shareAppdelegateInstance] checkReachability];
        if(check)
        {
            [CUSTOMINDICATORMACRO startLoadAnimation:self];
            [RestaurantMacro likeOrDislikeToServer:modal withCompletion:^(id obj1) {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    ResturantModal *responseModal = nil;
                    
                    if([obj1 isKindOfClass:[ResturantModal class]])
                    {
                        responseModal = obj1;
                        if([responseModal.result isEqualToString:@"success"])
                        {
                            lblResLike.text = [NSString stringWithFormat:@"%ld",(long)strLike+1];
                            btnResLike.enabled = YES;
                            btnResDislike.enabled = YES;
                        }
                        if([checkcount isEqualToString:@"2"])
                        {
                            lblResDislike.text = [NSString stringWithFormat:@"%ld",(long)strDisLike-1];
                        }
                    }
                    else
                    {
                        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Please check your Internet Connection." preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction *okBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                            [self dismissViewControllerAnimated:YES completion:nil];
                        }];
                        
                        [alertController addAction:okBtn];
                        [self presentViewController:alertController animated:YES completion:nil];
                    }
                    [CUSTOMINDICATORMACRO stopLoadAnimation:self];
                });
            }];
            
        }
        
    }else
    {
        NSLog(@"error occure here");
    }
    
    
    
}


-(IBAction)pressResDisLike:(id)sender
{
    NSInteger  strDisLike = [lblResDislike.text integerValue];
    NSInteger  strLike = [lblResLike.text integerValue];
    ResturantModal *modal = [[ResturantModal alloc]init];
    
    NSArray *filteredArray = [resturantArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"placeID ==%@",placeidSub]];
    if(filteredArray>0)
    {
        modal = [filteredArray firstObject];
    }
    if(![modal.likeORDislike isEqualToString:@"2"])
    {
        [modal setUserID:registerModalResponeValue.userID];
        NSString *checkcount  = modal.likeORDislike;
        [modal setLikeORDislike:@"2"];
        
        BOOL check=[[AppDelegate shareAppdelegateInstance] checkReachability];
        if(check)
        {
            [CUSTOMINDICATORMACRO startLoadAnimation:self];
            [RestaurantMacro likeOrDislikeToServer:modal withCompletion:^(id obj1) {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    ResturantModal *responseModal = nil;
                    
                    if([obj1 isKindOfClass:[ResturantModal class]])
                    {
                        responseModal = obj1;
                        if([responseModal.result isEqualToString:@"success"])
                        {
                            lblResDislike.text = [NSString stringWithFormat:@"%ld",(long)strDisLike+1];
                            btnResLike.enabled = YES;
                            btnResDislike.enabled = YES;
                        }
                        
                        if([checkcount isEqualToString:@"1"])
                        {
                            if (strLike >= 0)
                            {
                                lblResLike.text = [NSString stringWithFormat:@"%ld",(long)strLike-1];
                            }
                        }
                    }
                    else
                    {
                        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Please check your Internet Connection." preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction *okBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                            [self dismissViewControllerAnimated:YES completion:nil];
                        }];
                        
                        [alertController addAction:okBtn];
                        [self presentViewController:alertController animated:YES completion:nil];
                    }
                    [CUSTOMINDICATORMACRO stopLoadAnimation:self];
                });
            }];
        }
        
    }
    
}
-(void)setEmonjis :(int)selectedEmonjis type:(int)value
{
    switch (selectedEmonjis) {
        case e_EmonjiType_Cover:
        {
            if(value == 1)
            {
                btn_Emonji1.enabled = YES;
            }
            else
            {
                btn_Emonji1.enabled = NO;
            }
            
            break;
        }
        case e_EmonjiType_Wall:
        {
            if(value == 1)
            {
                btn_Emonji2.enabled = YES;
            }
            else
            {
                btn_Emonji2.enabled = NO;
            }
            
            break;
        }
        case e_EmonjiType_Shirt:
        {
            if(value == 1)
            {
                btn_Emonji3.enabled = YES;
            }
            else
            {
                btn_Emonji3.enabled = NO;
            }
            
            break;
        }
        case e_EmonjiType_Music:
        {
            if(value == 1)
            {
                btn_Emonji4.enabled = YES;
            }
            else
            {
                btn_Emonji4.enabled = NO;
            }
            break;
        }
        default:
            break;
    }
}

-(void)pressSearchBtn:(NSString*)city
{
    dispatch_async(dispatch_get_main_queue(), ^{
        txt_SearchBox.text=city;
    });
    self.strSW_Lat = @"";
    GlobalRadius=@"";
    mapView.delegate=self;
    [self btnResetFilterClick:nil];
    //---- For getting current gps location
    if (locationManager) {
        locationManager=nil;
    }
    locationManager = [CLLocationManager new];
    locationManager.delegate = self;
    [locationManager requestWhenInUseAuthorization];
    [locationManager requestAlwaysAuthorization];
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [locationManager startUpdatingLocation];
    [locationManager requestAlwaysAuthorization];
    mapView.showsUserLocation = YES;
    [btn_Filter setImage:[UIImage imageNamed:@"filter"] forState:UIControlStateNormal];

    APP_DELEGATE.userCurrentLocation=locationManager.location;
    stringLat=[NSString stringWithFormat:@"%f",locationManager.location.coordinate.latitude ];
    stringLong=[NSString stringWithFormat:@"%f",locationManager.location.coordinate.longitude];
    self.strSW_Lat = @"";
    [locationManager stopUpdatingLocation];
    locationManager=nil;
    
    SelectedLat=0.0000;
    SelectedLog =0.0000;
    
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"ischeckedshowquickview"];
    
    if (HappyHoursSelected == 0)
        tblView_DailySpecial.hidden =YES;
    if (DailySpecialSelected == 0)
        tblView_DailySpecial.hidden =YES;
    
    zoomEnable=NO;
    if (txt_SearchBox.text.length > 0){
        manuallySearchLocation=YES;
    }
    else{
        manuallySearchLocation=NO;
    }
    
    NSMutableArray *localArry=[NSMutableArray new];
    
    for (RegisterModal *eventsData in _cityArray)
    {
        NSRange range=[eventsData.placeType rangeOfString:city options:(NSCaseInsensitiveSearch)];
        
        if(range.location != NSNotFound)
            [localArry addObject:eventsData];
    }
    
    for (RegisterModal *eventsData in _categoryarray)
    {
        NSRange range=[eventsData.placeType rangeOfString:city options:(NSCaseInsensitiveSearch)];
        
        if(range.location != NSNotFound)
            [localArry addObject:eventsData];
    }
    
    
    
    if (localArry.count>0) {
        RegisterModal *modal = [localArry objectAtIndex:0];
        NSRange range=[modal.placeType rangeOfString:@", IL" options:(NSCaseInsensitiveSearch)];
        NSRange range1=[modal.placeType rangeOfString:@", Chicago" options:(NSCaseInsensitiveSearch)];
        
        if(range.location != NSNotFound || range1.location != NSNotFound)
        {
            _isCheckedCityStatus = TRUE;
        }
        else if([modal.statusCity isEqualToString:@"City"])
        {
            _isCheckedCityStatus = TRUE;
        }
        else
        {
            _isCheckedCityStatus = FALSE;
        }
        
    }else
        _isCheckedCityStatus = TRUE;
    
    
    if(city.length>0){
        [[NSUserDefaults standardUserDefaults]setBool:FALSE forKey:@"randomsearch"];
        
        if(_isCheckedCityStatus == TRUE)
            [self getAddressLatLong:city];
        else
        {
            ResturantModal *modal = [[ResturantModal alloc]init];
            [modal setUserID:user_IDRegister];
            [modal setResturantLatitude:stringLat];
            [modal setResturantLongtitude:stringLong];
            NSString *strReplace = [city stringByReplacingOccurrencesOfString:@"," withString:@""];
            strReplace = [strReplace stringByReplacingOccurrencesOfString:@"IL" withString:@""];
            [modal setSystemMessage:strReplace];
            [self loadMapValuesServiceHelper:modal];
        }
    }
    tblView_Category.hidden = YES;
    [txt_SearchBox resignFirstResponder];
    
}
-(void)getAddressLatLongWithoutService:(NSString *)string
{
    if(string.length>0)
    {
        CLGeocoder *geocoder = [[CLGeocoder alloc] init];
        
        [geocoder geocodeAddressString:string completionHandler:^(NSArray *placemarks, NSError *error) {
            if([placemarks count]) {
                CLPlacemark *placemark = [placemarks objectAtIndex:0];
                CLLocation *location = placemark.location;
                CLLocationCoordinate2D coordinate = location.coordinate;
                NSLog(@"coordinate = (%f, %f)", coordinate.latitude, coordinate.longitude);
                ResturantModal *moadal = [[ResturantModal alloc]init];
                [moadal setResturantLatitude:[NSString stringWithFormat:@"%f",coordinate.latitude]];
                [moadal setResturantLongtitude:[NSString stringWithFormat:@"%f",coordinate.longitude]];
                [moadal setUserID:registerModalResponeValue.userID];
                
                [moadal setCityOrArea:_strCityOrPlace];
                moadal.is_LiveMap        = @"0";//(isSelectedValuesIsLive == TRUE) ? @"0" : @"1";
                moadal.is_HappyHours     =   (isCheckedHappyHours == TRUE) ? @"1" : @"0";
                moadal.is_DailySpecials  =   (isCheckedDailySpecials == TRUE) ? @"1" : @"0";
                moadal.happHoursDay      =   (string_HappyHours == nil || [string_HappyHours isEqualToString:@"View All"]) ? @"" : string_HappyHours;
                moadal.dailySpecialsDay  =   (string_DailySpecials == nil) || [string_DailySpecials isEqualToString:@"View All"] ? @"" : string_DailySpecials;
                moadal.strOffset = @"0";
                moadal.strSW_Lat = self.strSW_Lat;
                moadal.strSW_Long = self.strSW_Long;
                moadal.strNE_Lat = self.strNE_Lat;
                moadal.strNE_Long = self.strNE_Long;

                [moadal setCityOrArea:_strCityOrPlace];
                [resturantArray removeAllObjects];
                [mapView removeAnnotations:mapView.annotations];
                
                BOOL check=[[AppDelegate shareAppdelegateInstance] checkReachability];
                if(check)
                {
                    [CUSTOMINDICATORMACRO startLoadAnimation:self];
                    [RestaurantMacro listAllMapValuesToServer:moadal withCompletion:^(id obj1) {
                        NSLog(@"View Account ===  %@",obj1);
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            if([obj1 isKindOfClass:[NSArray class]] || [obj1 isKindOfClass:[NSMutableArray class]])
                            {
                                
                                NSArray *arry=obj1;
                                if (arry.count>0) {
                                    NumberOfRecordFound = [obj1[2]intValue];
                                    min_distance=[obj1[0] doubleValue];
                                    resturantArray = obj1[1];
                                }
                                
                                
                                if(resturantArray.count>0)
                                {
                                    isCheckedQuickView = [[NSUserDefaults standardUserDefaults]boolForKey:@"ischeckedshowquickview"];
                                    if(isCheckedQuickView == TRUE)
                                    {
                                        [[NSUserDefaults standardUserDefaults]setBool:FALSE forKey:@"ischeckedshowquickview"];
                                        [self addQuickView];
                                        
                                        viewMapQuickDetails.hidden = FALSE;
                                        lbl_BtnBar.hidden = NO;
                                        view_CloseQuick.hidden = NO;
                                        viewMoreButton.hidden = FALSE;
                                        NSArray *filteredArray = [resturantArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"placeID ==%@",placeidSub]];
                                        if(filteredArray>0)
                                        {
                                            ResturantModal *modal = [filteredArray firstObject];
                                            [self loadDataForQuickViewInSort:modal];
                                        }
                                    }
                                    else{
                                        [mapView removeAnnotations:mapView.annotations];
                                        NSArray *filteredArray = [resturantArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"placeID ==%@",placeidSub]];
                                        if(filteredArray>0)
                                        {
                                            ResturantModal *modal = [filteredArray firstObject];
                                            [self loadDataForQuickViewInSort:modal];
                                        }
                                        mapUpdate=NO;
                                        [self loadLocationInMap];
                                    }
                                    
                                    
                                }
                                else
                                {
                                    [mapView removeAnnotations:mapView.annotations];
                                    _lblDisplayLocations.text=@"   0 Location Displaying";
                                    /* _lblDisplayLocations.text=@"   0 Location Displaying";
                                     [self showAlertTitle:@"" Message:PLACENOTFOUND_ALERT];*/
                                    [CUSTOMINDICATORMACRO stopLoadAnimation:self];
                                }
                                
                            }
                            else
                            {
                                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Please check your Internet Connection." preferredStyle:UIAlertControllerStyleAlert];
                                
                                UIAlertAction *okBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                    [self dismissViewControllerAnimated:YES completion:nil];
                                }];
                                
                                [alertController addAction:okBtn];
                                [self presentViewController:alertController animated:YES completion:nil];
                                [CUSTOMINDICATORMACRO stopLoadAnimation:self];
                            }
                            
                        });
                        
                    }];
                    
                }
            }
        }];
        
    }
    
}


-(void)getLatLongBasedOnRefresh :(CLLocation *)location
{
    
    if(location != nil)
    {
        CLGeocoder *geocoder = [[CLGeocoder alloc] init];
        [geocoder reverseGeocodeLocation:location
                       completionHandler:^(NSArray *placemarks, NSError *error) {
                           CLPlacemark *placemark = [placemarks objectAtIndex:0];
                           if (placemark) {
                               
                               
                               ResturantModal *modal = [[ResturantModal alloc]init];
                               
                               [modal setSystemMessage:@""];
                               
                               searchAreaPlaceString = placemark.locality;
                               [modal setUserID:user_IDRegister];
                               [modal setResturantLatitude:latStr];
                               [modal setResturantLongtitude:longStr];
                               [self loadMapValuesServiceHelper:modal];
                           }
                           else {
                               NSLog(@"Could not locate");
                           }
                       }
         ];
    }
}




-(void)getAddressLatLong:(NSString *)string
{
    manuallySearchLocation=YES;
    if(string.length>0)
    {
        CLGeocoder *geocoder = [[CLGeocoder alloc] init];
        [CUSTOMINDICATORMACRO startLoadAnimation:self];
        [geocoder geocodeAddressString:string completionHandler:^(NSArray *placemarks, NSError *error) {
            if([placemarks count]) {
                CLPlacemark *placemark = [placemarks objectAtIndex:0];
                CLLocation *location = placemark.location;
                CLLocationCoordinate2D coordinate = location.coordinate;
                NSLog(@"coordinate = (%f, %f)", coordinate.latitude, coordinate.longitude);
                stringLat =  [NSString stringWithFormat:@"%f",coordinate.latitude];
                stringLong = [NSString stringWithFormat:@"%f",coordinate.longitude];
                
                ResturantModal *modal = [[ResturantModal alloc]init];
                [modal setUserID:user_IDRegister];
                [modal setResturantLatitude:stringLat];
                [modal setResturantLongtitude:stringLong];
                [modal setStatusCityOrAreas:_strCityOrPlace];
                [modal setSystemMessage:txt_SearchBox.text];
                
                [self loadMapValuesServiceHelper:modal];
            }
            else
            {
                /*[mapView removeAnnotations:mapView.annotations];
                 mapView.showsUserLocation = YES;
                 
                 CLLocationCoordinate2D cord= CLLocationCoordinate2DMake([stringLat doubleValue],[stringLong doubleValue]);
                 
                 
                 MKCoordinateRegion region;
                 region.center=cord;
                 region.span.latitudeDelta=0.020;
                 region.span.longitudeDelta=0.020;
                 [mapView setRegion:region animated:YES];
                 [mapView regionThatFits:region];
                 
                 
                 _lblDisplayLocations.text=@"   0 Location Displaying";
                 manuallySearchLocation=NO;
                 [CUSTOMINDICATORMACRO stopLoadAnimation:self];*/
                
                
                
                ResturantModal *modal = [[ResturantModal alloc]init];
                [modal setUserID:user_IDRegister];
                [modal setResturantLatitude:stringLat];
                [modal setResturantLongtitude:stringLong];
                NSString *strReplace = [txt_SearchBox.text stringByReplacingOccurrencesOfString:@"," withString:@""];
                strReplace = [strReplace stringByReplacingOccurrencesOfString:@"IL" withString:@""];
                [modal setSystemMessage:strReplace];
                [self loadMapValuesServiceHelper:modal];
                
                
                
                /* manuallySearchLocation=NO;
                 [CUSTOMINDICATORMACRO stopLoadAnimation:self];*/
            }
        }];
    }
}


-(void)loadSearchServiceHelper:(NSString *)string
{
    if(string.length>0)
    {
        ResturantModal *moadal = [[ResturantModal alloc]init];
        [moadal setUserID:registerModalResponeValue.userID];
        [moadal setResturantName:string];
        
        BOOL check=[[AppDelegate shareAppdelegateInstance] checkReachability];
        if(check)
        {
            [CUSTOMINDICATORMACRO startLoadAnimation:self];
            [RestaurantMacro listAllSearchData:moadal withCompletion:^(id obj1) {
                NSLog(@"View Account ===  %@",obj1);
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [resturantArray removeAllObjects];
                    if([obj1 isKindOfClass:[NSArray class]] || [obj1 isKindOfClass:[NSMutableArray class]])
                    {
                        [mapView removeAnnotations:mapView.annotations];
                        resturantArray = obj1;
                        if(resturantArray.count>0)
                        {
                            [self loadLocationInMap];
                            [self zoomToFitMapAnnotations];
                        }
                        else
                        {
                            [mapView removeAnnotations:mapView.annotations];
                            //ERRORMESSAGE_ALERT(@"No places found.");
                            [CUSTOMINDICATORMACRO stopLoadAnimation:self];
                        }
                    }
                    else
                    {
                        [mapView removeAnnotations:mapView.annotations];
                        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Please check your Internet Connection." preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction *okBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                            [self dismissViewControllerAnimated:YES completion:nil];
                        }];
                        
                        [alertController addAction:okBtn];
                        [self presentViewController:alertController animated:YES completion:nil];
                        [CUSTOMINDICATORMACRO stopLoadAnimation:self];
                    }
                    
                    
                });
                
            }];
        }
    }
    
}
-(void) btnFavPressedInCarousal:(id)sender{
   
    UIButton *btnfavpressed = (UIButton*) sender;
    ResturantModal *modal = restModal_DidSelectMap;
    
    if(modal.isCheckedFavourite == NO)
    {
        BOOL check=[[AppDelegate shareAppdelegateInstance] checkReachability];
        if(check)
        {
            [modal setUserID:registerModalResponeValue.userID];
            [CUSTOMINDICATORMACRO startLoadAnimation:self];
            [RestaurantMacro favouriteToServer:modal withCompletion:^(id obj1) {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    ResturantModal *responseModal = nil;
                    if([obj1 isKindOfClass:[ResturantModal class]])
                    {
                        responseModal = obj1;
                        if([responseModal.result isEqualToString:@"success"])
                        {
                            //btnResFavourite.selected = YES;
                            [modal setIsCheckedFavourite:YES];
                            [btnfavpressed setImage:[UIImage imageNamed:@"fav_selected.png"] forState:UIControlStateNormal];
                            
                        }
                        
                    }
                    else
                    {
                        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Please check your Internet Connection." preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction *okBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                            [self dismissViewControllerAnimated:YES completion:nil];
                        }];
                        [alertController addAction:okBtn];
                        [self presentViewController:alertController animated:YES completion:nil];
                    }
                    [CUSTOMINDICATORMACRO stopLoadAnimation:self];
                });
            }];
        }
    }
    else
    {
        [modal setUserID:registerModalResponeValue.userID];
        
        BOOL check=[[AppDelegate shareAppdelegateInstance] checkReachability];
        if(check)
        {
            [CUSTOMINDICATORMACRO startLoadAnimation:self];
            [RestaurantMacro unFavouriteToServer:modal withCompletion:^(id obj1) {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    ResturantModal *responseModal =  nil;
                    if([obj1 isKindOfClass:[ResturantModal class]])
                    {
                        responseModal = obj1;
                        if([responseModal.result isEqualToString:@"success"])
                        {
                            // btnResFavourite.selected = NO;
                            [btnfavpressed setImage:[UIImage imageNamed:@"fav_unselected.png"] forState:UIControlStateNormal];
                            
                            [modal setIsCheckedFavourite:NO];
                        }
                    }
                    else
                    {
                        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Please check your Internet Connection." preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction *okBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                            [self dismissViewControllerAnimated:YES completion:nil];
                        }];
                        [alertController addAction:okBtn];
                        [self presentViewController:alertController animated:YES completion:nil];
                        
                    }
                    [CUSTOMINDICATORMACRO stopLoadAnimation:self];
                    
                });
            }];
        }
        
    }
}

-(IBAction)pressFavouriteBtn:(id)sender
{
    ResturantModal *modal = [[ResturantModal alloc]init];
    
    NSArray *filteredArray = [resturantArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"placeID ==%@",placeidSub]];
    if(filteredArray>0)
    {
        modal = [filteredArray firstObject];
    }
    
    if(modal.isCheckedFavourite == NO)
    {
        BOOL check=[[AppDelegate shareAppdelegateInstance] checkReachability];
        if(check)
        {
            [modal setUserID:registerModalResponeValue.userID];
            [CUSTOMINDICATORMACRO startLoadAnimation:self];
            [RestaurantMacro favouriteToServer:modal withCompletion:^(id obj1) {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    ResturantModal *responseModal = nil;
                    if([obj1 isKindOfClass:[ResturantModal class]])
                    {
                        responseModal = obj1;
                        if([responseModal.result isEqualToString:@"success"])
                        {
                           //btnResFavourite.selected = YES;
                            [modal setIsCheckedFavourite:YES];
                            [btnResFavourite setImage:[UIImage imageNamed:@"FavSelLIst"] forState:UIControlStateNormal];
                            
                        }
                        
                    }
                    else
                    {
                        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Please check your Internet Connection." preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction *okBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                            [self dismissViewControllerAnimated:YES completion:nil];
                        }];
                        [alertController addAction:okBtn];
                        [self presentViewController:alertController animated:YES completion:nil];
                    }
                    [CUSTOMINDICATORMACRO stopLoadAnimation:self];
                });
            }];
        }
    }
    else
    {
        [modal setUserID:registerModalResponeValue.userID];
        
        BOOL check=[[AppDelegate shareAppdelegateInstance] checkReachability];
        if(check)
        {
            [CUSTOMINDICATORMACRO startLoadAnimation:self];
            [RestaurantMacro unFavouriteToServer:modal withCompletion:^(id obj1) {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    ResturantModal *responseModal =  nil;
                    if([obj1 isKindOfClass:[ResturantModal class]])
                    {
                        responseModal = obj1;
                        if([responseModal.result isEqualToString:@"success"])
                        {
                           // btnResFavourite.selected = NO;
                            [btnResFavourite setImage:[UIImage imageNamed:@"FavUnSelLIst"] forState:UIControlStateNormal];

                            [modal setIsCheckedFavourite:NO];
                        }
                    }
                    else
                    {
                        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Please check your Internet Connection." preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction *okBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                            [self dismissViewControllerAnimated:YES completion:nil];
                        }];
                        [alertController addAction:okBtn];
                        [self presentViewController:alertController animated:YES completion:nil];
                        
                    }
                    [CUSTOMINDICATORMACRO stopLoadAnimation:self];
                    
                });
            }];
        }
        
    }
    
    
}

- (IBAction)pressSpecialHappyHours:(id)sender
{
    UIButton *button = (UIButton *)sender;
    if(button.tag == 0){
        [self colorChangeOnPressTab:@[btn_QuickViewHappy,btn_quickViewDaily]];
        
        if(_restModal_QuickView
           .arrayHappyHours.count>0)
        {
            arraySpecialforday = _restModal_QuickView.arrayHappyHours;
            
            info_View.hidden=NO;
            view_tableView.hidden = NO;
            view_Empty.hidden = YES;
            
        }
        else{
            
            info_View.hidden=YES;
            view_tableView.hidden = YES;
            view_Empty.hidden = NO;
        }
    }
    else{
        [self colorChangeOnPressTab:@[btn_quickViewDaily,btn_QuickViewHappy]];
        if(_restModal_QuickView.arraySpecials.count>0)
        {
            arraySpecialforday = _restModal_QuickView.arraySpecials;
            
            info_View.hidden=NO;
            view_tableView.hidden = NO;
            view_Empty.hidden = YES;
        }
        else
        {
            info_View.hidden=YES;
            view_tableView.hidden = YES;
            view_Empty.hidden = NO;
        }
    }
    [tableView_DailySpecial reloadData];
}


-(IBAction)pressBack:(id)sender{
    [[NSUserDefaults standardUserDefaults]setBool:FALSE forKey:@"ischeckedshowquickview"];
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)showAlertTitle:(NSString *)title Message:(NSString *)message
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [alertController addAction:okBtn];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

- (IBAction)MapListView:(id)sender
{
    [self NavigateToRestListViewController];
}


-(void)NavigateToRestListViewController{
    
    if (resturantArray.count !=0)
    {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:@"RestListViewController" forKey:@"Classname"];
        [defaults synchronize];
        
        RestListViewController *restVC = [self.storyboard instantiateViewControllerWithIdentifier:@"restlist"];
       // restVC.newarray=resturantArray;
        if ([self.strSW_Lat isEqualToString:@""]) {
            restVC.newarray=resturantArray;
            restVC.NumberOfRecordFound = NumberOfRecordFound;

        }else{
            restVC.newarray=arrPasstoRest;
            restVC.NumberOfRecordFound = arrPasstoRest.count;

        }
        [restVC getResturentModel:lastModal];
        [[NSUserDefaults standardUserDefaults]setBool:FALSE forKey:@"ischeckedshowquickview"];
        [self.navigationController pushViewController:restVC animated:YES];
    }
    else
    {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Places Not Found." preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                                {
                                    [self dismissViewControllerAnimated:YES completion:nil];
                                }];
        
        [alertController addAction:okBtn];
        [self presentViewController:alertController animated:YES completion:nil];
        
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)MenuButtonClicked:(id)sender
{
    [self.view endEditing:YES];
    MenuTapBtn.hidden=NO;
    [self.revealViewController revealToggle:0];
}

- (IBAction)MenuTapButton:(id)sender
{
    MenuTapBtn.hidden=YES;
    [self.revealViewController revealToggle:0];
}


-(void)colorChangeOnPressTab:(NSArray*)arry{
    dispatch_async(dispatch_get_main_queue(), ^{
        for (btnNo=0; btnNo<arry.count; btnNo++) {
            if (btnNo==0) {
                UIButton *button =(UIButton*)arry[0];
                button.backgroundColor=[UIColor colorWithRed:0.0f/255.0f green:204.0f/255.0f blue:255.0f/255.0f alpha:1];
                [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            }else
            {
                UIButton *button =(UIButton*)arry[btnNo];
                button.backgroundColor=[UIColor whiteColor];
                [button setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
            }
        }
    });
    
}


-(void)addQuickView{
    
    quick_touchableView.hidden=NO;
    [self.tabBarController.view addSubview:quick_mainView];
//    btnResLike.enabled = YES;
//    btnResDislike.enabled = YES;
    btnResDislike.userInteractionEnabled = true;
    btnResLike.userInteractionEnabled = true;
    
}


-(void)viewWillDisappear:(BOOL)animated{
    [HighlightView removeFromSuperview];
    [quick_mainView removeFromSuperview];
}



@end

