//
//  NewHomeViewController.h
//  Spotlyte
//
//  Created by Admin on 17/08/16.
//  Copyright © 2016 Naveen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewHomeViewController : UIViewController<UITextViewDelegate,UITableViewDelegate,UITableViewDataSource,UIScrollViewDelegate,UITabBarDelegate,CLLocationManagerDelegate>
{
   RegisterModal *registerModalResponeValue;
}

@property (strong, nonatomic) IBOutlet UIImageView *logo_Spotlyte;
@property (strong, nonatomic) IBOutlet UITextField *txtField;
@property (strong, nonatomic) IBOutlet UITableView *tblView_Category;

@property (strong, nonatomic) IBOutlet UIView *viewOfTextField;
@property (strong, nonatomic) IBOutlet UIView *contentScrView;

@property (strong, nonatomic) IBOutlet UIScrollView *scrollVw;

@property (strong, nonatomic) IBOutlet UIButton *menuBtn;

@property (strong, nonatomic) IBOutlet UILabel *lblFind;
@property (strong, nonatomic) IBOutlet UILabel *lblNever;

@property (strong, nonatomic) IBOutlet UIView *main_view;


@property (strong, nonatomic) IBOutlet NSLayoutConstraint *textField_Y;



@end
