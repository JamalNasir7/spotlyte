//
//  FollowersViewController.h
//  Spotlyte
//
//  Created by Admin on 20/09/16.
//  Copyright © 2016 Naveen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RegisterModal.h"

@interface FollowersViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
{
     IBOutlet UIView *Empty_View;
    IBOutlet UITableView *tableView_Followers;
    IBOutlet UILabel *lbl_Header;
    NSArray *responseArray;
    RegisterModal *registertModalObj;
    
    RegisterModal *registerModalResponeValue1;
}

@property (strong, nonatomic) NSString *checkString;
@property (strong, nonatomic) NSString *userIdString;

@property (strong, nonatomic)NSString *selectedController;

- (IBAction)pressBack:(id)sender;

@end
