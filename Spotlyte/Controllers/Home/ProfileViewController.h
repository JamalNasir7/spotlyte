//
//  ProfileViewController.h
//  Spotlyte
//
//  Created by Admin on 17/03/16.
//  Copyright © 2016 Naveen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RegisterModal.h"
#import "ResturantModal.h"
#import "DashBoardViewController.h"
#import "ProgressViewController.h"
#import "SettingsViewController.h"
#import "HMSegmentedControl.h"

@interface ProfileViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    IBOutlet UITableView *tweetsTableView;
    IBOutlet UIImageView *profileImage;
    IBOutlet UIImageView  *previousUserImage;
    IBOutlet UIImageView *nextUserImage;
    IBOutlet UILabel *profileName;
    IBOutlet UIView * profileView;
    IBOutlet UILabel *lblTweetId;
    IBOutlet UILabel *backButtonProfileName;
    IBOutlet UILabel *following_Lbl,*followerLbl;
    IBOutlet UIPageControl *tweetUserPageControl;
    NSString *strNameAndId;
    NSArray *arrayTweetMessages;
    NSArray *arrayTweetTiming;
    UISwipeGestureRecognizer *rightGestureRecognizer;
    UISwipeGestureRecognizer *leftGestureRecognizer;

    IBOutlet NSLayoutConstraint *tweetsTableViewCons;
    
    IBOutlet UIButton *btn_Fav;
    IBOutlet UIButton *btn_Follow;
    IBOutlet UIButton *btn_add;
    IBOutlet UILabel *lblCommentsCount;
    IBOutlet UIButton *btn_dashBoard;
    IBOutlet UIImageView *asynch_MainProfileImage;
    IBOutlet UIButton *btn_back;

    int i;
    UITabBarController *tabBarController;
    RegisterModal *registerModalResponeValue1;

    IBOutlet NSLayoutConstraint *tweeterView_Y;
    IBOutlet UILabel    *lbl_NoFollowingComments;
}
@property (weak, nonatomic) IBOutlet HMSegmentedControl *viewTool;

@property (strong, nonatomic)NSMutableArray *personalTweetArray ;
@property (strong, nonatomic)NSMutableArray *imageArray ;

@property (strong, nonatomic)NSString *selectedController;
@property (strong, nonatomic)NSString *user_IDString;
@property (strong, nonatomic)RegisterModal *registerModalOBJ ;

@property (strong, nonatomic) NSString *controllerName;

@property (nonatomic) NSInteger selectedTweet ;


-(IBAction)pressBack:(id)sender;
-(IBAction)pressFavourite:(id)sender;
-(IBAction)pressFollower:(id)sender;
-(IBAction)pressDashBoard:(id)sender;
-(IBAction)pressAddFollowerBtn:(id)sender;

@end
