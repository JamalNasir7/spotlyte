//
//  ProfileViewController.m
//  Spotlyte
//
//  Created by Admin on 17/03/16.
//  Copyright © 2016 Naveen. All rights reserved.
//

#import "ProfileViewController.h"
#import "TweetsCell.h"
#import "FavouriteListViewController.h"
#import "LiveFeedModal.h"
#import "FollowersViewController.h"

@interface ProfileViewController ()
@end

@implementation ProfileViewController
@synthesize personalTweetArray,selectedTweet;
@synthesize selectedController;
@synthesize user_IDString;
@synthesize registerModalOBJ;
@synthesize imageArray;

- (void)viewDidLoad {
    [super viewDidLoad];
    tweetsTableView.rowHeight = UITableViewAutomaticDimension;
    tweetsTableView.estimatedRowHeight = 80.0;
    [self.view setFrame:[UIScreen mainScreen].bounds];
    tweetUserPageControl.numberOfPages = personalTweetArray.count;
    tweetUserPageControl.currentPage = selectedTweet;
    
//    [btn_Follow.layer setBorderWidth:1.0];
//    [btn_Follow.layer setBorderColor:[[UIColor lightGrayColor] CGColor]];
    
    self.navigationController.navigationBar.hidden = true;
    
    self.viewTool.sectionTitles = @[@"Timeline"];
    self.viewTool.selectedSegmentIndex = 0;
    self.viewTool.backgroundColor = [UIColor whiteColor];
    self.viewTool.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor blackColor],NSFontAttributeName: [UIFont systemFontOfSize:14.0]};
    self.viewTool.selectedTitleTextAttributes = @{NSForegroundColorAttributeName : [UIColor colorWithRed:0.0/255.0 green:158.0/255.0 blue:224.0/255.0 alpha:1.0],NSFontAttributeName: [UIFont systemFontOfSize:14.0]};
    self.viewTool.selectionIndicatorColor = [UIColor colorWithRed:0.0/255.0 green:158.0/255.0 blue:224.0/255.0 alpha:1.0];
    self.viewTool.selectionStyle = HMSegmentedControlSelectionStyleFullWidthStripe;
    self.viewTool.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    self.viewTool.selectionIndicatorHeight = 2.0;
    
    [self.viewTool setIndexChangeBlock:^(NSInteger index) {
        if (index == 0){
            
        }else{
            
        }
    }];
    
    profileImage.layer.cornerRadius = profileImage.frame.size.width / 2;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    lbl_NoFollowingComments.hidden = true;
    registerModalOBJ = [[RegisterModal alloc]init];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [defaults objectForKey:@"logindetails"];
    registerModalResponeValue1 = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
     if (![_controllerName isEqualToString:@"view_feed"])
            user_IDString=registerModalResponeValue1.userID;
    
    NSString *userID1 = [NSString stringWithFormat:@"%@",user_IDString];
    NSString *userID2 = [NSString stringWithFormat:@"%@",registerModalResponeValue1.userID];
    
    BOOL isCheckedUser =  FALSE;
    if([selectedController isEqualToString:@"otheruser"])
    {
        isCheckedUser = TRUE;
    }
    
    if([userID1 isEqualToString:userID2])
    {
        selectedController = @"currentuser";
    }

    if([selectedController isEqualToString:@"currentuser"])
    {
        btn_add.hidden              =   YES;
        btn_Fav.hidden              =   NO;
        btn_Follow.hidden           =   YES;
        previousUserImage.hidden    =   YES;
        nextUserImage.hidden        =   YES;
        btn_dashBoard.hidden        =   NO;
        tweeterView_Y.constant=0;
        if(isCheckedUser == TRUE)
        {
            rightGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(rightSwipeHandler:)];
            [rightGestureRecognizer setDirection:UISwipeGestureRecognizerDirectionRight];
            //[profileView addGestureRecognizer:rightGestureRecognizer];
            
            leftGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(leftSwipeHandler:)];
            [leftGestureRecognizer setDirection:UISwipeGestureRecognizerDirectionLeft];
            //[profileView addGestureRecognizer:leftGestureRecognizer];
        }
    }
    else
    {
        btn_add.hidden              =   YES;
        btn_Fav.hidden              =   YES;
        btn_Follow.hidden           =   NO;
        previousUserImage.hidden    =   NO;
        nextUserImage.hidden        =   NO;
        btn_dashBoard.hidden        =   YES;
         tweeterView_Y.constant=40;
        
        rightGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(rightSwipeHandler:)];
        [rightGestureRecognizer setDirection:UISwipeGestureRecognizerDirectionRight];
        //[profileView addGestureRecognizer:rightGestureRecognizer];
        
        leftGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(leftSwipeHandler:)];
        [leftGestureRecognizer setDirection:UISwipeGestureRecognizerDirectionLeft];
        //[profileView addGestureRecognizer:leftGestureRecognizer];
    }
    [self loadServiceGetProfile];
}


#pragma mark -- Service Helper
-(void)addFollowerServiceHelper
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [defaults objectForKey:@"logindetails"];
    RegisterModal * registerModalResponeValue = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    [registerModalResponeValue setFollowerID:user_IDString];
    
    BOOL check=[[AppDelegate shareAppdelegateInstance] checkReachability];
    if(check)
    {
        [CUSTOMINDICATORMACRO startLoadAnimation:self];
        [REGISTERMACRO addFollowerToServer:registerModalResponeValue withCompletion:^(id obj1) {
            NSLog(@"View Account ===  %@",obj1);
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [CUSTOMINDICATORMACRO stopLoadAnimation:self];
                if([obj1 isKindOfClass:[RegisterModal class]])
                {
                    registerModalOBJ = obj1;
                    if([registerModalOBJ.result isEqualToString:@"success"])
                    {
                        //[btn_Follow setImage:[UIImage imageNamed:@"UnFollow"] forState:UIControlStateNormal];
                        [btn_Follow setTitle:@"Unfollow" forState:UIControlStateNormal];
                        [self loadServiceGetProfile];
                    }
                    else
                    {
                        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:registerModalOBJ.result message:registerModalOBJ.message preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction *okBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                            [self dismissViewControllerAnimated:YES completion:nil];
                        }];
                        
                        [alertController addAction:okBtn];
                        [self presentViewController:alertController animated:YES completion:nil];
                    }
                }
                else
                {
                    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Please check your Internet Connection." preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction *okBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        [self dismissViewControllerAnimated:YES completion:nil];
                    }];
                    [alertController addAction:okBtn];
                    [self presentViewController:alertController animated:YES completion:nil];
                }
            });
        }];
    }
}


-(void)addUnFollowerServiceHelper
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [defaults objectForKey:@"logindetails"];
    RegisterModal * registerModalResponeValue = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    [registerModalResponeValue setFollowerID:user_IDString];
    
    BOOL check=[[AppDelegate shareAppdelegateInstance] checkReachability];
    if(check)
    {
        [CUSTOMINDICATORMACRO startLoadAnimation:self];
        [REGISTERMACRO addUnFollowerToServer:registerModalResponeValue withCompletion:^(id obj1) {
            NSLog(@"View Account ===  %@",obj1);
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [CUSTOMINDICATORMACRO stopLoadAnimation:self];
                if([obj1 isKindOfClass:[RegisterModal class]])
                {
                    registerModalOBJ = obj1;
                    if([registerModalOBJ.result isEqualToString:@"success"])
                    {
                        
                        //[btn_Follow setImage:[UIImage imageNamed:@"userTwitFollow"] forState:UIControlStateNormal];
                        [btn_Follow setTitle:@"Follow" forState:UIControlStateNormal];
                        [self loadServiceGetProfile];
                    }
                    else
                    {
                        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:registerModalOBJ.result message:registerModalOBJ.message preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction *okBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                            [self dismissViewControllerAnimated:YES completion:nil];
                        }];
                        
                        [alertController addAction:okBtn];
                        [self presentViewController:alertController animated:YES completion:nil];
                    }
                    
                }
                else
                {
                    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Please check your Internet Connection." preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction *okBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        [self dismissViewControllerAnimated:YES completion:nil];
                    }];
                    [alertController addAction:okBtn];
                    [self presentViewController:alertController animated:YES completion:nil];
                }
                
            });
        }];
    }
    
}

-(void)loadServiceGetProfile
{
    BOOL check=[[AppDelegate shareAppdelegateInstance] checkReachability];
    if(check)
    {
        [CUSTOMINDICATORMACRO startLoadAnimation:self];
        [REGISTERMACRO viewAccountToServer:user_IDString withCompletion:^(id obj1) {
            NSLog(@"View Account ===  %@",obj1);
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [CUSTOMINDICATORMACRO stopLoadAnimation:self];
                
                if([obj1 isKindOfClass:[RegisterModal class]])
                {
                    registerModalOBJ = obj1;
                    if([registerModalOBJ.result isEqualToString:@"success"])
                    {
                        backButtonProfileName.text = [NSString stringWithFormat:@"%@ %@",registerModalOBJ.firstName,registerModalOBJ.lastName];
                        profileName.text = [NSString stringWithFormat:@"%@ %@",registerModalOBJ.firstName,registerModalOBJ.lastName];
                        lblTweetId.text = [NSString stringWithFormat:@"@%@",registerModalOBJ.userName];
                        personalTweetArray = [registerModalOBJ.profileCommentsArray mutableCopy];
                        lblCommentsCount.text = [NSString stringWithFormat:@"%lu",(unsigned long)personalTweetArray.count];
//                        followerLbl.text = registerModalOBJ.followerCount;
//                        following_Lbl.text = registerModalOBJ.followingCount;
                        
                        followerLbl.text = [NSString stringWithFormat:@"%@ Followers",registerModalOBJ.followerCount];
                        following_Lbl.text = [NSString stringWithFormat:@"%@ Following",registerModalOBJ.followingCount];
                        
                        profileImage.layer.cornerRadius = profileImage.frame.size.width / 2;
                        [profileImage setBackgroundColor:[UIColor blackColor]];
                        profileImage.layer.masksToBounds = YES;

                        if(![selectedController isEqualToString:@"currentuser"])
                        {
                            if(registerModalOBJ.isCheckedFollower == FALSE)
                            {
                                 [btn_Follow setTitle:@"Follow" forState:UIControlStateNormal];
                            }
                            else
                            {
                                 [btn_Follow setTitle:@"Unfollow" forState:UIControlStateNormal];
                            }
                        }

                        profileImage.layer.cornerRadius = profileImage.frame.size.width / 2;
                        [profileImage setBackgroundColor:[UIColor blackColor]];
                        profileImage.layer.masksToBounds = YES;

                        dispatch_async(dispatch_get_main_queue(), ^{
                            NSURL *url = [[NSBundle mainBundle] URLForResource:@"loading-1" withExtension:@"gif"];
                            NSString *newUrl = [registerModalOBJ.profileImage stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
                            [profileImage sd_setImageWithURL:[NSURL URLWithString:newUrl]placeholderImage:[UIImage imageNamed:@"imgplaceHolder"]];
                          
                            
                        });
                        
                        [tweetsTableView reloadData];
                    }
                    
                }
                else
                {
                    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Please check your Internet Connection." preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction *okBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        [self dismissViewControllerAnimated:YES completion:nil];
                    }];
                    [alertController addAction:okBtn];
                    [self presentViewController:alertController animated:YES completion:nil];
                }
                
            });
        }];
    }
}

#pragma  mark - Swipe Gesture Handler
-(void)rightSwipeHandler:(UISwipeGestureRecognizer *)recognizer
{
    selectedTweet--;
    if(selectedTweet == -1){
        selectedTweet = imageArray.count-1;
    }
    [self updateTheUserProfile];
}

-(void)leftSwipeHandler:(UISwipeGestureRecognizer *)recognizer
{
    selectedTweet ++;
    if(selectedTweet > imageArray.count-1){
        selectedTweet = 0;
    }
    [self updateTheUserProfile];
}

- (void)updateTheUserProfile
{
    if(imageArray.count>1)
    {
        if (selectedTweet == 0) {
            
            ResturantModal *modalLive1 = [imageArray objectAtIndex:imageArray.count-1];
            previousUserImage.layer.cornerRadius = 3.0;
            [previousUserImage setBackgroundColor:[UIColor blackColor]];
            previousUserImage.layer.masksToBounds = YES;
            dispatch_async(dispatch_get_main_queue(), ^{
                NSURL *url = [[NSBundle mainBundle] URLForResource:@"loading-1" withExtension:@"gif"];
                NSString *newUrl = [modalLive1.profileImage stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
                [previousUserImage sd_setImageWithURL:[NSURL URLWithString:newUrl]
                                     placeholderImage:[UIImage imageNamed:@"noimage.png"]];
                
            });
            user_IDString = modalLive1.userID;
            
        }else{
            ResturantModal *modalLive1 = [imageArray objectAtIndex:selectedTweet-1];
            previousUserImage.layer.cornerRadius = 3.0;
            [previousUserImage setBackgroundColor:[UIColor blackColor]];
            previousUserImage.layer.masksToBounds = YES;
            dispatch_async(dispatch_get_main_queue(), ^{
                NSURL *url = [[NSBundle mainBundle] URLForResource:@"loading-1" withExtension:@"gif"];
                NSString *newUrl = [modalLive1.profileImage stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
                [previousUserImage sd_setImageWithURL:[NSURL URLWithString:newUrl]
                                     placeholderImage:[UIImage imageNamed:@"noimage.png"]];
                
            });
            user_IDString = modalLive1.userID;
        }
        if(selectedTweet == imageArray.count-1){
            ResturantModal *modalLive2 = [imageArray objectAtIndex:0];
            nextUserImage.layer.cornerRadius = 3.0;
            [nextUserImage setBackgroundColor:[UIColor blackColor]];
            nextUserImage.layer.masksToBounds = YES;
            dispatch_async(dispatch_get_main_queue(), ^{
                NSURL *url = [[NSBundle mainBundle] URLForResource:@"loading-1" withExtension:@"gif"];
                NSString *newUrl = [modalLive2.profileImage stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
                [nextUserImage sd_setImageWithURL:[NSURL URLWithString:newUrl]
                                 placeholderImage:[UIImage imageNamed:@"noimage.png"]];
                
            });
            user_IDString = modalLive2.userID;
            
        }else{
            ResturantModal *modalLive2 = [imageArray objectAtIndex:selectedTweet+1];
            nextUserImage.layer.cornerRadius = 3.0;
            [nextUserImage setBackgroundColor:[UIColor blackColor]];
            nextUserImage.layer.masksToBounds = YES;
            dispatch_async(dispatch_get_main_queue(), ^{
                NSURL *url = [[NSBundle mainBundle] URLForResource:@"loading-1" withExtension:@"gif"];
                NSString *newUrl = [modalLive2.profileImage stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
                [nextUserImage sd_setImageWithURL:[NSURL URLWithString:newUrl]
                                 placeholderImage:[UIImage imageNamed:@"noimage.png"]];
                
            });
            user_IDString = modalLive2.userID;
        }
    }
    
    if([user_IDString isEqualToString:registerModalResponeValue1.userID])
    {
        btn_add.hidden              =   YES;
        btn_Fav.hidden              =   NO;
        btn_Follow.hidden           =   YES;
        previousUserImage.hidden    =   YES;
        nextUserImage.hidden        =   YES;
        btn_dashBoard.hidden        =   NO;
    }
    else
    {
        btn_add.hidden              =   YES;
        btn_Fav.hidden              =   YES;
        btn_Follow.hidden           =   NO;
        previousUserImage.hidden    =   NO;
        nextUserImage.hidden        =   NO;
        btn_dashBoard.hidden        =   YES;
    }
    
    [self loadServiceGetProfile];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    if(personalTweetArray.count == 0){
        lbl_NoFollowingComments.hidden = false;
        tweetsTableView.hidden = true;
    }
    else{
        lbl_NoFollowingComments.hidden = true;
        tweetsTableView.hidden = false;
    }
    return personalTweetArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier=@"tweetCell";
    TweetsCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[TweetsCell alloc]
                initWithStyle:UITableViewCellStyleDefault
                reuseIdentifier:cellIdentifier];
    }
    if(personalTweetArray.count>0)
    {
        RegisterModal *modal = [personalTweetArray objectAtIndex:indexPath.row];
       /* if(registerModalOBJ.userName != nil && registerModalOBJ.userName.length != 0 )
        {
            NSString *combinedStr = [NSString stringWithFormat:@"%@ %@ @%@",registerModalOBJ.firstName,registerModalOBJ.lastName,registerModalOBJ.userName];
            [cell.userNameLabel setAttributedText:[self getAttrStrfromTheStr:combinedStr]];
            
        }*/
        cell.user_name.text = modal.placeName;
        cell.userNameLabel.text = modal.CommentedUser_name;
        
        
        cell.asynch_ProfileImage.layer.cornerRadius = cell.asynch_ProfileImage.frame.size.width / 2;
       // cell.asynch_ProfileImage.layer.cornerRadius = 3.0;
        [cell.asynch_ProfileImage setBackgroundColor:[UIColor blackColor]];
        cell.asynch_ProfileImage.layer.masksToBounds = YES;
        dispatch_async(dispatch_get_main_queue(), ^{
            NSURL *url = [[NSBundle mainBundle] URLForResource:@"loading-1" withExtension:@"gif"];
            NSString *newUrl = [modal.CommentedUser_Image stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
            [cell.asynch_ProfileImage sd_setImageWithURL:[NSURL URLWithString:newUrl]
                                        placeholderImage:[UIImage imageNamed:@"imgplaceHolder"]];
        });
        cell.lblTweetMessage.text = modal.comments;
        //cell.lblTweetTiming.text = modal.createdOn;
        cell.lblRating.hidden = true;
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
        [dateFormat setLocale:locale];

        [dateFormat setDateFormat:@"MM-dd-yyyy hh:mm a"];
        
        NSString *strCreatedDate = modal.createdOn;
        NSDate *dateCreate = [dateFormat dateFromString:strCreatedDate];
        [dateFormat setDateFormat:@"MM-dd-yyyy"];
        
        NSString *strStartDate = [dateFormat stringFromDate:dateCreate];
        cell.lblDate.text = strStartDate;
        
        cell.lblRating.hidden = true;
        cell.lblRating.text = modal.rating;
        [dateFormat setDateFormat:@"hh:mm a"];
        NSString *strTime = [dateFormat stringFromDate:dateCreate];
        cell.lblTweetTiming.text = strTime;
    }
    
    return cell;
}

-(NSMutableAttributedString*)getAttrStrfromTheStr:(NSString*)str
{
    NSMutableAttributedString *attString=[[NSMutableAttributedString alloc] initWithString:str];
    
    UIFont *font1=[UIFont fontWithName:@"HelveticaNeue" size:12.0];
    UIFont *font2=[UIFont fontWithName:@"HelveticaNeue" size:10.0];
    UIColor *textColor1=[UIColor colorWithRed:39/255.0 green:31/255.0 blue:135/255.0 alpha:1.0];
    UIColor *textColor2=[UIColor colorWithRed:49/255.0 green:151/255.0 blue:204/255.0 alpha:1.0];
    
    NSRange range=[str rangeOfString:@"@"];
    [attString addAttribute:NSFontAttributeName value:font1 range:NSMakeRange(0, range.location+1)];
    [attString addAttribute:NSForegroundColorAttributeName value:textColor1 range:NSMakeRange(0, range.location+1)];
    [attString addAttribute:NSFontAttributeName value:font2 range:NSMakeRange(range.location, str.length-range.location)];
    [attString addAttribute:NSForegroundColorAttributeName value:textColor2 range:NSMakeRange(range.location, str.length-range.location)];
    return attString;
}


-(IBAction)pressFavourite:(id)sender
{
    FavouriteListViewController *favouriteVC  = [self.storyboard instantiateViewControllerWithIdentifier:@"favouritelist"];
    [self.navigationController pushViewController:favouriteVC animated:YES];
}

-(IBAction)pressFollower:(id)sender
{
    UIButton *button = (UIButton *)sender;
    FollowersViewController *followersVC  = [self.storyboard instantiateViewControllerWithIdentifier:@"followersvc"];
    followersVC.selectedController = @"otheruser";
    followersVC.userIdString = user_IDString;
    if(button.tag == 2)
    {
        followersVC.checkString = @"followers";
        [self.navigationController pushViewController:followersVC animated:YES];
    }
    else if (button.tag == 1)
    {
        followersVC.checkString = @"following";
        [self.navigationController pushViewController:followersVC animated:YES];
    }
    
    
}

-(IBAction)pressAddFollowerBtn:(id)sender
{
    if(registerModalOBJ.isCheckedFollower == FALSE)
    {
        //[btn_Follow setImage:[UIImage imageNamed:@"userTwitFollow"] forState:UIControlStateNormal];
        [btn_Follow setTitle:@"Follow" forState:UIControlStateNormal];
        [self addFollowerServiceHelper];
    }
    else
    {
        //[btn_Follow setImage:[UIImage imageNamed:@"UnFollow"] forState:UIControlStateNormal];
        [btn_Follow setTitle:@"Unfollow" forState:UIControlStateNormal];
        [self addUnFollowerServiceHelper];
    }
}

-(IBAction)pressDashBoard:(id)sender{
    [self pushTabBarController];
}

-(void)pushTabBarController{
    [self.tabBarController setSelectedIndex:3];
}

-(IBAction)pressBack:(id)sender{
     if ([_controllerName isEqualToString:@"view_feed"])
         [self.navigationController popViewControllerAnimated:YES];
     else
         [self.tabBarController setSelectedIndex:0];
  }

@end
