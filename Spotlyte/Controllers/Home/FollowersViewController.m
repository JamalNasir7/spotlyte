//
//  FollowersViewController.m
//  Spotlyte
//
//  Created by Admin on 20/09/16.
//  Copyright © 2016 Naveen. All rights reserved.
//

#import "FollowersViewController.h"
#import "FollowersCell.h"
#import "ProfileViewController.h"
#import "HeaderView.h"

@interface FollowersViewController ()

@end

@implementation FollowersViewController
@synthesize checkString,userIdString,selectedController;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    HeaderView *header=[[HeaderView alloc]initWithTitle:[checkString isEqualToString:@"followers"]?@"FOLLOWERS":@"FOLLOWINGS" controller:self];
    [header addSubview:header.btnback];
    [self.view addSubview:header];
    
    
    tableView_Followers.allowsSelectionDuringEditing=YES;
    tableView_Followers.tableFooterView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, tableView_Followers.frame.size.width, 0.1f)];
    registertModalObj = [[RegisterModal alloc]init];
    [self followersAndFollowingsServiceHelper];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [super viewWillAppear:YES];
    registertModalObj = [[RegisterModal alloc]init];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [defaults objectForKey:@"logindetails"];
    registerModalResponeValue1 = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    NSString *userID1 = [NSString stringWithFormat:@"%@",userIdString];
    NSString *userID2 = [NSString stringWithFormat:@"%@",registerModalResponeValue1.userID];
    BOOL isCheckedUser =  FALSE;
    if([selectedController isEqualToString:@"otheruser"])
    {
        isCheckedUser = TRUE;
    
    }
    if([userID1 isEqualToString:userID2])
    {
        selectedController = @"currentuser";
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark -- Followers And Followings Service Helper


-(void)followersAndFollowingsServiceHelper
{
    BOOL check=[[AppDelegate shareAppdelegateInstance] checkReachability];
    if(check)
    {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [CUSTOMINDICATORMACRO startLoadAnimation:self];
        });

        RegisterModal *modal = [[RegisterModal alloc]init];
        [modal setUserID:userIdString];
        [REGISTERMACRO listallFollowersAndFollowingsServer:userIdString withCompletion:^(id obj1) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [CUSTOMINDICATORMACRO stopLoadAnimation:self];
                if([obj1 isKindOfClass:[RegisterModal class]])
                {
                    registertModalObj = obj1;
                    if([checkString isEqualToString:@"followers"])
                    {
                        responseArray = registertModalObj.followersArray;
                    }
                    else{
                        responseArray = registertModalObj.followingsArray;
                    }
                    if(responseArray.count>0)
                    {
                        tableView_Followers.hidden = NO;
                        Empty_View.hidden = YES;
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [tableView_Followers reloadData];
                        });
                    }
                    else
                    {
                        Empty_View.hidden = NO;
                        tableView_Followers.hidden = YES;
                    }
                    
                }
                else
                {
                    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Please check your Internet Connection." preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction *okBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        [self dismissViewControllerAnimated:YES completion:nil];
                    }];
                    [alertController addAction:okBtn];
                    [self presentViewController:alertController animated:YES completion:nil];
                }
                
            });
        }];
    }
    
}

#pragma mark -- TableView Delegate Method

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return responseArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    FollowersCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CellIdentifier"];
    
    RegisterModal *modal = [responseArray objectAtIndex:indexPath.row];
    if(cell == nil)
    {
        NSArray *topLevelObjects = [[NSBundle mainBundle]loadNibNamed:@"FollowersCell" owner:self options:nil];
        
        cell = [topLevelObjects objectAtIndex:0];
    }
    cell.lbl_ProfileName.text = [NSString stringWithFormat:@"@%@",modal.userName];
    cell.lbl_FirstLastName.text = [NSString stringWithFormat:@"%@ %@",modal.firstName,modal.lastName];
    dispatch_async(dispatch_get_main_queue(), ^{
        NSString *newUrl = [modal.profileImage stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
        [cell.image_Profile sd_setImageWithURL:[NSURL URLWithString:newUrl]placeholderImage:[UIImage imageNamed:@"imgplaceHolderFollower"]];
    });
    
    cell.image_Profile.layer.cornerRadius = cell.image_Profile.frame.size.width / 2;
    cell.image_Profile.layer.masksToBounds = true;
    
    [cell.btn_Follow.layer setBorderWidth:1.0];
    cell.btn_Follow.layer.cornerRadius = 5.0;
    cell.btn_Follow.tag = indexPath.row;
    [cell.btn_Follow addTarget:self action:@selector(pressAddFollowerBtn:) forControlEvents:UIControlEventTouchUpInside];
    if([selectedController isEqualToString:@"currentuser"])
    {
        if(modal.isCheckedFollower == FALSE)
        {
            [cell.btn_Follow setTitle:@"Follow" forState:UIControlStateNormal];
            [cell.btn_Follow.layer setBorderColor:[[UIColor colorWithRed:18.0f/255.0f green:166.0f/255.0f blue:236.0f/255.0f alpha:1] CGColor]];
        }
        else
        {
            [cell.btn_Follow setTitle:@"Unfollow" forState:UIControlStateNormal];
            [cell.btn_Follow.layer setBorderColor:[[UIColor colorWithRed:18.0f/255.0f green:166.0f/255.0f blue:236.0f/255.0f alpha:1] CGColor]];
        }
    }
    else{
        cell.btnFollowWidthConst.constant = 0;
        
    }
    if([checkString isEqualToString:@"followers"])
    {
       // cell.btnFollowWidthConst.constant = 0;
    }
    else{
        //cell.btnFollowWidthConst.constant = 0;
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ProfileViewController *profileVC = [self.storyboard instantiateViewControllerWithIdentifier:@"profile"];
    profileVC.selectedController = @"otheruser";
    profileVC.selectedTweet = indexPath.row;
    profileVC.imageArray = [responseArray mutableCopy];
    RegisterModal *modal = [responseArray objectAtIndex:indexPath.row];
    profileVC.controllerName=@"view_feed";
    profileVC.user_IDString = modal.friendsUserId;
    [self.navigationController pushViewController:profileVC animated:YES];
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

- (IBAction)pressBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)pressAddFollowerBtn:(UIButton*)sender
{
    UIButton *btnFollow = (UIButton *)sender;
    RegisterModal *modal = [responseArray objectAtIndex:sender.tag];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:btnFollow.tag inSection:0];
    FollowersCell *cell = [tableView_Followers cellForRowAtIndexPath:indexPath];
    
    if(modal.isCheckedFollower == NO)
    {
        //[btn_Follow setImage:[UIImage imageNamed:@"userTwitFollow"] forState:UIControlStateNormal];
        //[cell.btn_Follow setTitle:@"Unfollow" forState:UIControlStateNormal];
        [modal setIsCheckedFollower:YES];
        [self addFollowerServiceHelper:cell];
    }
    else
    {
        //[btn_Follow setImage:[UIImage imageNamed:@"UnFollow"] forState:UIControlStateNormal];
        //Follow
        [modal setIsCheckedFollower:NO];

       // [cell.btn_Follow setTitle:@"Follow" forState:UIControlStateNormal];
        [self addUnFollowerServiceHelper:cell];
    }
}

#pragma mark -- Service Helper
-(void)addFollowerServiceHelper:(FollowersCell *)cell
{
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    NSData *data = [defaults objectForKey:@"logindetails"];
//    RegisterModal * registerModalResponeValue = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
//    [registerModalResponeValue setFollowerID:userIdString];
    
    NSIndexPath *indexPath = [tableView_Followers indexPathForCell:cell];
    RegisterModal * registerModalResponeValue = [responseArray objectAtIndex:indexPath.row];
    [registerModalResponeValue setUserID:userIdString];
    [registerModalResponeValue setFollowerID:registerModalResponeValue.friendsUserId];

    BOOL check=[[AppDelegate shareAppdelegateInstance] checkReachability];
    if(check)
    {
        [CUSTOMINDICATORMACRO startLoadAnimation:self];
        [REGISTERMACRO addFollowerToServer:registerModalResponeValue withCompletion:^(id obj1) {
            NSLog(@"View Account ===  %@",obj1);
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [CUSTOMINDICATORMACRO stopLoadAnimation:self];
                if([obj1 isKindOfClass:[RegisterModal class]])
                {
                    registertModalObj = obj1;
                    if([registertModalObj.result isEqualToString:@"success"])
                    {
                        [cell.btn_Follow setTitle:@"Unfollow" forState:UIControlStateNormal];
                    }
                    else
                    {
                        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:registertModalObj.result message:registertModalObj.message preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction *okBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                            [self dismissViewControllerAnimated:YES completion:nil];
                        }];
                        
                        [alertController addAction:okBtn];
                        [self presentViewController:alertController animated:YES completion:nil];
                    }
                }
                else
                {
                    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Please check your Internet Connection." preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction *okBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        [self dismissViewControllerAnimated:YES completion:nil];
                    }];
                    [alertController addAction:okBtn];
                    [self presentViewController:alertController animated:YES completion:nil];
                }
            });
        }];
    }
}

-(void)addUnFollowerServiceHelper:(FollowersCell *)cell
{
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    NSData *data = [defaults objectForKey:@"logindetails"];
//    RegisterModal * registerModalResponeValue = [NSKeyedUnarchiver unarchiveObjectWithData:data];
//    [registerModalResponeValue setFollowerID:userIdString];
    
    NSIndexPath *indexPath = [tableView_Followers indexPathForCell:cell];
    RegisterModal * registerModalResponeValue = [responseArray objectAtIndex:indexPath.row];
    [registerModalResponeValue setUserID:userIdString];
    [registerModalResponeValue setFollowerID:registerModalResponeValue.friendsUserId];
    
    BOOL check=[[AppDelegate shareAppdelegateInstance] checkReachability];
    if(check)
    {
        [CUSTOMINDICATORMACRO startLoadAnimation:self];
        [REGISTERMACRO addUnFollowerToServer:registerModalResponeValue withCompletion:^(id obj1) {
            NSLog(@"View Account ===  %@",obj1);
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [CUSTOMINDICATORMACRO stopLoadAnimation:self];
                if([obj1 isKindOfClass:[RegisterModal class]])
                {
                    registertModalObj = obj1;
                    if([registertModalObj.result isEqualToString:@"success"])
                    {
                        
                        [cell.btn_Follow setTitle:@"Follow" forState:UIControlStateNormal];
                    }
                    else
                    {
                        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:registertModalObj.result message:registertModalObj.message preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction *okBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                            [self dismissViewControllerAnimated:YES completion:nil];
                        }];
                        
                        [alertController addAction:okBtn];
                        [self presentViewController:alertController animated:YES completion:nil];
                    }
                    
                }
                else
                {
                    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Please check your Internet Connection." preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction *okBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        [self dismissViewControllerAnimated:YES completion:nil];
                    }];
                    [alertController addAction:okBtn];
                    [self presentViewController:alertController animated:YES completion:nil];
                }
                
            });
        }];
    }
    
}
@end
