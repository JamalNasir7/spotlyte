//
//  MapAnnotation.m
//  Map View in iOS
//
//  Created by Pravin Kumar on 13/04/15.
//  Copyright (c) 2015 pravin. All rights reserved.
//

#import "MapAnnotation.h"

@implementation MapAnnotation

-(id)initWithTitle:(NSString *)title andCoordinate:(CLLocationCoordinate2D)coordinate2d andPlaceType:(NSString*)placeType andRating:(NSString *)Rating andPlaceID:(NSString *)placeID andAnnotatonCount:(NSString *)count;{
    
    self.title = title;
    self.coordinate = coordinate2d;
    self.placeType = placeType;
    self.rating = Rating;
    self.placeid = placeID;
    self.annotationCount = count;
    
    return self;
}

@end
