//
//  MapAnnotation.h
//  Map View in iOS
//
//  Created by Pravin Kumar on 13/04/15.
//  Copyright (c) 2015 pravin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>


@interface MapAnnotation : NSObject<MKAnnotation>

@property (nonatomic, copy) NSString *title;
@property (nonatomic,strong)NSString *placeType;
@property (nonatomic, strong)NSString *rating;
@property (nonatomic, strong)NSString *placeid;
@property (nonatomic, strong)NSString *annotationCount;

@property (nonatomic, readwrite) CLLocationCoordinate2D coordinate;

-(id)initWithTitle:(NSString *)title andCoordinate:(CLLocationCoordinate2D)coordinate2d andPlaceType:(NSString*)placeType andRating:(NSString *)Rating andPlaceID:(NSString *)placeID andAnnotatonCount:(NSString *)count;



@end
