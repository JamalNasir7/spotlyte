//
//  filterCell.h
//  Spotlyte
//
//  Created by Tops on 7/19/17.
//  Copyright © 2017 Hemant. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface filterCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblText;

@property (weak, nonatomic) IBOutlet UIImageView *imgCheckBox;
@property (weak, nonatomic) IBOutlet UIButton *btnCell;

@end
