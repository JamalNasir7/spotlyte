//
//  customSearchViewController.h
//  Spotlyte
//
//  Created by CIPL227-MOBILITY on 16/03/16.
//  Copyright © 2016 Naveen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "ResturantModal.h"
#import "JTSScrollIndicator.h"

@interface customSearchViewController : UIViewController<UISearchBarDelegate, UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate,UITextViewDelegate>{
   
    NSArray * arrayValueForInnerTable;
    NSMutableDictionary *finalDictionary;
    NSMutableDictionary *selectedDictionary;
    NSString *categoryString;
    NSString *mileString;
    NSString *happyString;
    NSString *timeString;
    NSString *selectedString;
    NSString *cityString;
    NSMutableString *appendTotalString;
    NSInteger positionViewAppendString;
    NSMutableArray *arrayAppend;
    BOOL isCheckedSearchUserInteractio;
     NSString *dayString;
    
    BOOL isCheckedPrice;
    BOOL isCheckedMileRadius;
    BOOL isCheckedTime;
    BOOL isCheckedCity;
    BOOL isCheckedDay;
    BOOL isCheckedRadioButton;
    
    NSInteger indexPrice;
    NSInteger indexMileRadius;
    NSInteger indexTime;
    NSInteger indexCity;
    NSInteger selectedIndexDay;
    NSInteger selectedIndexPrice;
    NSInteger selectedIndexRadius;
    NSInteger selectedIndexTime;
    NSInteger selectedIndexCity;
    
    ResturantModal *restModalObj;
    BOOL isCheckedLoadData;
    NSString *selectedCityId;
    IBOutlet UISearchBar *searchBar_City;
    NSMutableArray *tempArray;
    NSString *statusCity;
    NSIndexPath *indexPath1;
    
    NSInteger scrollIndex;
    NSInteger scrollIndexHappyHours;
    
    IBOutlet UIView *searchVw;
    
}
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *ActID;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cnstActID_Width;

@property (nonatomic,strong) NSArray *responseArray;
@property (nonatomic,strong) NSArray *cityArray;
@property (nonatomic,strong) NSArray *ArrItems;
@property (weak, nonatomic) IBOutlet UISearchBar *customSearchBar;
@property (weak, nonatomic) IBOutlet UILabel *SearbarStar_lbl;
@property (weak, nonatomic) IBOutlet UIButton *btnIsItem;
@property (weak, nonatomic) IBOutlet UITableView *tblViewSearchOption;
@property (weak, nonatomic) IBOutlet UITableView *tbl_SearchItems;
@property (weak, nonatomic) IBOutlet UITableView *tblViewInnerSearchOption;
@property (strong, nonatomic) JTSScrollIndicator *indicator;

@property (strong, nonatomic) NSArray * arrayLstSearch;

@property (strong, nonatomic) NSString *searchParameterStr;

@property (strong, nonatomic) IBOutlet UIView *innerContentView;

@property (strong, nonatomic) IBOutlet UILabel *inSearchTitle;

@property (strong, nonatomic) IBOutlet UIButton *inSearchOkBtn;

@property (strong, nonatomic) IBOutlet UIButton *inSearchCancelBtn;

@property (weak, nonatomic) IBOutlet UITextView *CustomSearchView;


- (IBAction)backButtobHandler:(id)sender;

- (IBAction)menuButtonHandler:(id)sender;

- (IBAction)innerSearchOkButtonHandler:(id)sender;

- (IBAction)innerSearchCancelButtonHandler:(id)sender;

-(IBAction)pressSearch:(id)sender;
-(IBAction)pressSpotLight:(id)sender;


@end
