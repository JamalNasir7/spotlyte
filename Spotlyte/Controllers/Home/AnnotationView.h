//
//  AnnotationView.h
//  Spotlyte
//
//  Created by Tamilarasan on 3/3/16.
//  Copyright © 2016 Naveen. All rights reserved.
//

#import <MapKit/MapKit.h>

@interface AnnotationView : MKAnnotationView
{
    /*UIImageView *imageView;
    UILabel *lblCount;*/
   
}

@property (weak, nonatomic) IBOutlet UIImageView *annotatonImage;
@property (weak, nonatomic) IBOutlet UILabel *lblCount;

@property (strong, nonatomic) NSString *strPlaceType;

@end
