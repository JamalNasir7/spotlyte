//
//  TabBarController.m
//  Spotlyte
//
//  Created by Hemant on 03/03/17.
//  Copyright © 2017 Hemant. All rights reserved.
//

#import "TabBarController.h"

@interface TabBarController ()

@end

@implementation TabBarController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.delegate=self;
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone){
        if ([[UITabBar appearance] respondsToSelector:@selector(setUnselectedItemTintColor:)]) {
            [[UITabBar appearance] setUnselectedItemTintColor:[UIColor blackColor]];
        }
       //[[UITabBar appearance] setUnselectedItemTintColor:[UIColor blackColor]];
    }
    
    CALayer *topBorder = [CALayer layer];
    topBorder.frame = CGRectMake(0.0f, 0.0f, self.view.frame.size.width, 2.0f);
    topBorder.backgroundColor = [[UIColor colorWithRed:0.0f/255.0f green:204.0f/255.0f blue:255.0f/255.0f alpha:1] CGColor];
    [self.tabBar.layer addSublayer:topBorder];
}

  
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


- (void)tabBarController:(UITabBarController *)theTabBarController didSelectViewController:(UIViewController *)viewController {
    NSUInteger indexOfTab = [theTabBarController.viewControllers indexOfObject:viewController];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"tabSelected"];

    for (id nav in theTabBarController.viewControllers) {
        if ([nav isKindOfClass:[UINavigationController class]]) {
            [nav popToRootViewControllerAnimated:NO];
        }
    }
    
    for (UITabBarItem *item in theTabBarController.tabBar.items) {
            [item setImageInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
        
         switch (item.tag) {
                case 0:
                    [item setImageInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
                    break;
                case 1:
                    [item setImageInsets:UIEdgeInsetsMake(0, 1, 0, 0)];
                    break;
                case 2:
                    [item setImageInsets:UIEdgeInsetsMake(3, 2, 0, 1)];
                    break;
                case 3:
                    [item setImageInsets:UIEdgeInsetsMake(1, 0, 0, 1)];
                    break;
                case 4:
                    [item setImageInsets:UIEdgeInsetsMake(5, 3, 0, 2)];
                    break;
                default:
                    break;
            }
        }
    if (indexOfTab==0) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"MenuButtonEnable"
         object:self userInfo:nil];
        [self.revealViewController revealToggle:nil];
    }
}


@end
