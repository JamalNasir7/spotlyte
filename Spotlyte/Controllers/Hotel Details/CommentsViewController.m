//
//  CommentsViewController.m
//  Spotlyte
//
//  Created by Admin on 22/03/16.
//  Copyright © 2016 Naveen. All rights reserved.
//

#import "CommentsViewController.h"
#import "TweetsCell.h"
#import "ProfileViewController.h"
#import "LiveFeedModal.h"

@interface CommentsViewController ()

@end

@implementation CommentsViewController

-(void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view setFrame:[UIScreen mainScreen].bounds];
    
    //appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate]; //Jigar
    
    UIView *tweetTableFooterView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, tableView_Comments.frame.size.width, 40)];
    UIButton *viewMoreButton=[UIButton buttonWithType:UIButtonTypeCustom];
    [viewMoreButton setTitle:@"View More Tweets" forState:UIControlStateNormal];
    viewMoreButton.frame = CGRectMake(20, 0, 160.0, 40.0);
    [viewMoreButton setTitleColor:[UIColor colorWithRed:82/255.0 green:82/255.0 blue:82/255.0 alpha:1.0] forState:UIControlStateNormal];
    [viewMoreButton.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:15.0]];
    
    UIButton *viewMoreRightArrowBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    [viewMoreRightArrowBtn setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [viewMoreRightArrowBtn setFrame:CGRectMake(tweetTableFooterView.frame.size.width-60, 0, 40, 40)];
    [tweetTableFooterView addSubview:viewMoreRightArrowBtn];
    [tweetTableFooterView addSubview:viewMoreButton];
    [tableView_Comments setTableFooterView:tweetTableFooterView];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    return appDelegate.liveFeedArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier=@"tweetCell";
    TweetsCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[TweetsCell alloc]
                initWithStyle:UITableViewCellStyleDefault
                reuseIdentifier:cellIdentifier];
    }
    LiveFeedModal *modalLive = [appDelegate.liveFeedArray objectAtIndex:indexPath.row];
    
    
    
    [cell.userNameLabel setAttributedText:[self getAttrStrfromTheStr:[NSString stringWithFormat:@"%@ %@",modalLive.strTweetname,modalLive.strTweetId]]];
    cell.imageProfile.image =[UIImage imageNamed:modalLive.strTweetImage];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ProfileViewController *profileVC = [self.storyboard instantiateViewControllerWithIdentifier:@"profile"];
    profileVC.selectedTweet = 0;
    profileVC.selectedController = @"otheruser";
    AppDelegate *appDelegate1 = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    profileVC.personalTweetArray = appDelegate1.liveFeedArray;
    [self.navigationController pushViewController:profileVC animated:YES];
}

-(NSMutableAttributedString*)getAttrStrfromTheStr:(NSString*)str
{
    NSMutableAttributedString *attString=[[NSMutableAttributedString alloc] initWithString:str];
    
    UIFont *font1=[UIFont fontWithName:@"HelveticaNeue" size:12.0];
    UIFont *font2=[UIFont fontWithName:@"HelveticaNeue" size:10.0];
    UIColor *textColor1=[UIColor colorWithRed:39/255.0 green:31/255.0 blue:135/255.0 alpha:1.0];
    UIColor *textColor2=[UIColor colorWithRed:49/255.0 green:151/255.0 blue:204/255.0 alpha:1.0];
    
    NSRange range=[str rangeOfString:@"@"];
    [attString addAttribute:NSFontAttributeName value:font1 range:NSMakeRange(0, range.location+1)];
    [attString addAttribute:NSForegroundColorAttributeName value:textColor1 range:NSMakeRange(0, range.location+1)];
    [attString addAttribute:NSFontAttributeName value:font2 range:NSMakeRange(range.location, str.length-range.location)];
    [attString addAttribute:NSForegroundColorAttributeName value:textColor2 range:NSMakeRange(range.location, str.length-range.location)];
    return attString;
}


-(IBAction)pressBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
