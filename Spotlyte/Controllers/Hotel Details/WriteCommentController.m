//
//  WriteCommentController.m
//  Spotlyte
//
//  Created by Hemant on 23/02/17.
//  Copyright © 2017 Hemant. All rights reserved.
//

#import "WriteCommentController.h"
@interface WriteCommentController ()
@end

@implementation WriteCommentController
@synthesize GiveStarRating;
- (void)viewDidLoad {
    [super viewDidLoad];
    //[_txtView becomeFirstResponder];
    _strGivenRating = [[NSString alloc]init];
    _strGivenRating = @"0";
     [self displayStar];
  }

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


-(void)recievedData:(void (^)(BOOL success))Callback{
    _AlertHandler = Callback;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    _txtView.userInteractionEnabled = false;
    /*dispatch_async(dispatch_get_main_queue(), ^{

       _lblName.text=[NSString stringWithFormat:@"%@ %@",[Default valueForKey:@"first_name"],[Default valueForKey:@"last_name"]];
     
    });*/
    
    dispatch_async(dispatch_get_main_queue(), ^{
        _imgView.image=_img;
    });

    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [defaults objectForKey:@"logindetails"];
   RegisterModal *registerModalResponeValue = [NSKeyedUnarchiver unarchiveObjectWithData:data];
      
    [REGISTERMACRO viewAccountToServer:registerModalResponeValue.userID withCompletion:^(id obj1) {
       
        
        RegisterModal *registerModalOBJ;
        if([obj1 isKindOfClass:[RegisterModal class]])
        {
            registerModalOBJ = obj1;
            if([registerModalOBJ.result isEqualToString:@"success"])
            {
                 dispatch_async(dispatch_get_main_queue(), ^{
                    _lblName.text=[NSString stringWithFormat:@"%@ %@",registerModalOBJ.firstName,registerModalOBJ.lastName];
                     _txtView.userInteractionEnabled = true;
                     [_txtView becomeFirstResponder];
                });
            }
        }
        else
        {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Please check your Internet Connection." preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self dismissViewControllerAnimated:YES completion:nil];
            }];
            [alertController addAction:okBtn];
            [self presentViewController:alertController animated:YES completion:nil];
        }
        
    }];
}

-(IBAction)btnMethods:(UIButton*)sender{
    
    NSInteger btnTag =sender.tag;
    switch (btnTag) {
        case 0:
            [self.navigationController popViewControllerAnimated:YES];
            break;
            
        case 1:{
            if((_txtView.text.length != 0 ) || (![_strGivenRating isEqualToString:@"0"]))
            {
                ResturantModal *modal = [[ResturantModal alloc]init];
                [modal setCommentsImage:@""];
                [modal setComments:_txtView.text];
                [modal setPlaceID:_restModalOBJ1.placeID];
                [modal setIs_Emoji:@"1"];
                [modal setUserID:_registerModalResponeValue1.userID];
                [modal setSystemMessage:@"1"];
                [modal setCmdNumber:@"1"];
                [modal setUserRating_comment:_strGivenRating];
                [self createCommentServiceHelper:modal];
            }
            else{
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Please enter rating or comment." preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *okBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    [self dismissViewControllerAnimated:YES completion:nil];
                }];
                [alertController addAction:okBtn];
                [self presentViewController:alertController animated:YES completion:nil];
            }
         }
        default:
            break;
    }
}
    
    
-(void)createCommentServiceHelper :(ResturantModal *)restModalOBJ1
    {
        BOOL check=[[AppDelegate shareAppdelegateInstance] checkReachability];
        if(check)
        {
            [CUSTOMINDICATORMACRO startLoadAnimation:self];
            [RestaurantMacro postCommentsToServer:restModalOBJ1 withCompletion:^(id obj1) {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [CUSTOMINDICATORMACRO stopLoadAnimation:self];
                    ResturantModal *responseModal = nil;
                    [CUSTOMINDICATORMACRO stopLoadAnimation:self];
                    if([obj1 isKindOfClass:[ResturantModal class]])
                    {
                        responseModal = obj1;
                         [_txtView resignFirstResponder];
                        if([responseModal.result isEqualToString:@"success"])
                        {
                            _AlertHandler(YES);
                            restModalOBJ1.totalReview=[NSString stringWithFormat:@"%d",[restModalOBJ1.totalReview intValue] +1];
                            [self.navigationController popViewControllerAnimated:YES];
                        }
                        _txtView.text = @"";
                    }
                    else
                    {
                        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Please check your Internet Connection." preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction *okBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                            [self dismissViewControllerAnimated:YES completion:nil];
                        }];
                        [alertController addAction:okBtn];
                        [self presentViewController:alertController animated:YES completion:nil];
                    }
                    
                });
            }];
        }
    }
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if (![text isEqualToString:@""])
    {
        _lblPlaceHolder.hidden = true;
    }
    return  true;
}
#pragma mark --- Star Rating Delegate Method

-(void)starsSelectionChanged:(EDStarRating*)control rating:(float)rating
{
    int ratingValue = (int) rating;
    _strGivenRating = [NSString stringWithFormat:@"%d", ratingValue];
    [self displayStar];
}

- (void)displayStar
{
    
        if(_strGivenRating.floatValue <= 2.0)
        {
            GiveStarRating.starHighlightedImage = [UIImage imageNamed:@"star-1_R.png"];
        }
        else if(_strGivenRating.floatValue <= 3.9)
        {
            GiveStarRating.starHighlightedImage = [UIImage imageNamed:@"star-1_Y.png"];
        }
        else
        {
            GiveStarRating.starHighlightedImage = [UIImage imageNamed:@"star-1_G.png"];
        }
        GiveStarRating.starImage = [UIImage imageNamed:@"unstartemp.png"];
        GiveStarRating.maxRating = 5.0;
        GiveStarRating.delegate = self;
        GiveStarRating.horizontalMargin = 12;
        GiveStarRating.editable = YES;
        GiveStarRating.rating = _strGivenRating.floatValue;
        GiveStarRating.displayMode = EDStarRatingDisplayAccurate;
    
}

    

@end
