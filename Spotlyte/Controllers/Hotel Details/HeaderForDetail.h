//
//  HeaderForDetail.h
//  Spotlyte
//
//  Created by Tops on 8/31/17.
//  Copyright © 2017 Hemant. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HeaderForDetail : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblHeaderTitle;
@property (weak, nonatomic) IBOutlet UIView *VwSep;

@end
