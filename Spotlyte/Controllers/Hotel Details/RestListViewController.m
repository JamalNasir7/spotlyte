//
//  RestListViewController.m
//  Spotlyte
//
//  Created by Admin on 17/03/16.
//  Copyright © 2016 Naveen. All rights reserved.
//

#import "RestListViewController.h"
#import "HomeViewController.h"
#import "NearByViewController.h"
#import "HeaderView.h"
#import "DailySpecialCell.h"
#import "FavouriteCell.h"
#import "PromotionsViewController.h"

@interface RestListViewController ()
{
    NSUserDefaults *defaults;
    
    NSString* BackToHomeView;
    
    NSInteger selectedRow;
    
    NSArray *arrySpecials;

    BOOL tapped;
    
    int btnNo;
    int IntOffset;

    NSString *strStatus;
    
    ResturantModal *restModal_QuickView;
    
    ResturantModal *selectedResturant;

    IBOutlet UIView *alertVw;
    BOOL restlist_success;
    BOOL isDataLoaded;
    NSString *CheckViewController;
  
}
@end

@implementation RestListViewController
@synthesize strStatus;
@synthesize stringLat,stringLongt;
@synthesize hotelNameArray;
@synthesize hotelImageArray,loadingArray;
@synthesize responseData,JSON1;
@synthesize selectedString,selectedCategory;
@synthesize happyHourString,timeString,mileString;
@synthesize searchString,cityString,status;
@synthesize NumberOfRecordFound;

- (void)viewDidLoad {
    [super viewDidLoad];
    CheckViewController = [[NSString alloc]init];
    
    tableView_RestList.rowHeight = UITableViewAutomaticDimension;
    tableView_RestList.estimatedRowHeight = 30.0;

    _tblPopup.rowHeight  = UITableViewAutomaticDimension;
    _tblPopup.estimatedRowHeight = 44.0;

    HeaderView *header=[[HeaderView alloc]initWithTitle:@"Places" controller:self];
    header.BackButtonHandler=^(BOOL success){
    
     [[NSUserDefaults standardUserDefaults]setBool:FALSE forKey:@"ischeckedshowquickview"];
         [self.navigationController popViewControllerAnimated:YES];
    };
    [header addSubview:header.btnback];
    [self.view addSubview:header];
    
    
    tableView_RestList.allowsSelectionDuringEditing=YES;
    isCheckedLoadMap = FALSE;
    
    defaults= [NSUserDefaults standardUserDefaults];
    
    NSString *check=[defaults objectForKey:@"Classname"];
    CheckViewController = check;
    _popupViewBG.hidden = YES;
    _popupView.hidden=YES;
    _btnCross.hidden=YES;
    _emptyView.hidden=YES;
    
    _popupView.layer.cornerRadius = 5;
    _popupView.layer.borderWidth = 1;
    _popupView.clipsToBounds = YES;
    _popupView.layer.borderColor = [[UIColor colorWithRed:153.0f/255.0f green:153.0f/255.0f blue:153.0f/255.0f alpha:1]CGColor];
    
    
    _btnExpand.layer.cornerRadius = 5;
    _btnExpand.layer.borderWidth = 1;
    _btnExpand.clipsToBounds = YES;
    _btnExpand.layer.borderColor = [[UIColor colorWithRed:18.0f/255.0f green:166.0f/255.0f blue:233.0f/255.0f alpha:1]CGColor];
    
    if ([check isEqualToString:@"RestListViewController"] && ![check isEqualToString:@""])
    {
        loadingArray=[NSMutableArray arrayWithArray:_newarray];
        lblRestCount.text = [NSString stringWithFormat:@"%lu Places Available",(unsigned long)NumberOfRecordFound];
        restlist_success=YES;
        [defaults setObject:@"" forKey:@"Classname"];
        [defaults synchronize];
        
        BackToHomeView=@"HomeView";
    }
    else
    {
        BackToHomeView=@"PresentView";
        [self initializeLocationManager];
        [defaults setObject:@"" forKey:@"Classname"];
        [defaults synchronize];
     }
    
    self.viewTool.sectionTitles = @[@"Happy Hours",@"Daily Specials"];
    self.viewTool.selectedSegmentIndex = 0;
    self.viewTool.backgroundColor = [UIColor whiteColor];
    self.viewTool.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor colorWithRed:153.0/255.0 green:153.0/255.0 blue:153.0/255.0 alpha:1.0],NSFontAttributeName: [UIFont systemFontOfSize:14.0]};
    self.viewTool.selectedTitleTextAttributes = @{NSForegroundColorAttributeName : [UIColor colorWithRed:0.0/255.0 green:158.0/255.0 blue:224.0/255.0 alpha:1.0],NSFontAttributeName: [UIFont systemFontOfSize:14.0]};
    self.viewTool.selectionIndicatorColor = [UIColor colorWithRed:0.0/255.0 green:158.0/255.0 blue:224.0/255.0 alpha:1.0];
    self.viewTool.selectionStyle = HMSegmentedControlSelectionStyleFullWidthStripe;
    self.viewTool.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    self.viewTool.selectionIndicatorHeight = 2.0;
    
    [self.viewTool setIndexChangeBlock:^(NSInteger index) {
        if (index == 0){
            arrySpecials = restModal_QuickView.arrayHappyHours;
            [self showHidePopup:arrySpecials.count];
        }else{
            arrySpecials = restModal_QuickView.arraySpecials;
            [self showHidePopup:arrySpecials.count];
        }
    }];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    IntOffset = 0;
    isDataLoaded = true;
    [tableView_RestList reloadData];
}

-(void)getResturentModel:(ResturantModal *)model{
    _restModal_Specials=model;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark --- Alert Method

-(void)showAlertTitle:(NSString *)title Message:(NSString *)message
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [alertController addAction:okBtn];
    
    [self presentViewController:alertController animated:YES completion:nil];
}


#pragma  mark --- Core Location
- (void)initializeLocationManager {
    loadingArray = [[NSMutableArray alloc]init];
    
    // Check to ensure location services are enabled
    if(![CLLocationManager locationServicesEnabled]) {
        
        return;
    }
    
    if([CLLocationManager locationServicesEnabled]) {
        
        _locationManager = [[CLLocationManager alloc] init];
        _locationManager.delegate = self;
        [_locationManager requestAlwaysAuthorization];
        _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        
        
        _locationManager.distanceFilter = kCLDistanceFilterNone;
        _locationManager.activityType = CLActivityTypeAutomotiveNavigation;
        
        [_locationManager startUpdatingLocation];
    }

}


- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    CLLocation *location = [locations lastObject];
    //AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.userCurrentLocation = location;
    
      if (location != nil)
    {
        if(![stringLongt isEqualToString:@""] && ![stringLongt isEqualToString:@"0"])
        {
            if(isCheckedLoadMap == FALSE)
            {
                isCheckedLoadMap = TRUE;
                if([status isEqualToString:@"city"])
                {
                    [self getAddressLatLong:cityString];
                }
                else if ([status isEqualToString:@"Chicago_area"])
                {
                    NSString *formatString = [NSString stringWithFormat:@"%@,Chicago",cityString];
                    [self getAddressLatLong:formatString];
                }
                else
                {
                    stringLat =  [NSString stringWithFormat:@"%.8f", location.coordinate.latitude];
                    stringLongt  =  [NSString stringWithFormat:@"%.8f", location.coordinate.longitude];
                    [_locationManager stopUpdatingLocation];
                    [self loadAdvancedFilterServiceHelper];
                    
                }
            }
        }
    }
}

-(void)getAddressLatLong:(NSString *)string
{
    if(string.length>0)
    {
        CLGeocoder *geocoder = [[CLGeocoder alloc] init];
        [CUSTOMINDICATORMACRO startLoadAnimation:self];
        [geocoder geocodeAddressString:string completionHandler:^(NSArray *placemarks, NSError *error) {
            if([placemarks count]) {
                CLPlacemark *placemark = [placemarks objectAtIndex:0];
                CLLocation *location = placemark.location;
                CLLocationCoordinate2D coordinate = location.coordinate;
                NSLog(@"coordinate = (%f, %f)", coordinate.latitude, coordinate.longitude);
                stringLat =  [NSString stringWithFormat:@"%.8f", location.coordinate.latitude];
                stringLongt  =  [NSString stringWithFormat:@"%.8f", location.coordinate.longitude];
                
                [self loadAdvancedFilterServiceHelper];
            }
            else{
                [CUSTOMINDICATORMACRO stopLoadAnimation:self];
            }
        }];
    }
}
-(void)loadMapValuesServiceHelper : (ResturantModal *)modal{
    
    BOOL check=[[AppDelegate shareAppdelegateInstance] checkReachability];
    if(check){
        [CUSTOMINDICATORMACRO startLoadAnimation:self];
        
        [RestaurantMacro listAllMapValuesToServer:modal withCompletion:^(id obj1)
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 if([obj1 isKindOfClass:[NSArray class]] || [obj1 isKindOfClass:[NSMutableArray class]])
                 {
                     [CUSTOMINDICATORMACRO stopLoadAnimation:self];

                     NSArray *arry=obj1;
                     NSMutableArray *arrPagination = [[NSMutableArray alloc]init];
                     if (arry.count>0) {
                       //  min_distance =[obj1[0] doubleValue];
                         NumberOfRecordFound = [obj1[2] intValue];

                         arrPagination = obj1[1];
                         
                         //[arrPagination addObjectsFromArray:arry];
                          [loadingArray addObjectsFromArray:arrPagination];
                     }
                     if(loadingArray.count>0)
                     {
                         dispatch_async(dispatch_get_main_queue(), ^{
                             lblRestCount.text=loadingArray.count==1?@"   1 Places Available": [NSString stringWithFormat:@"   %ld Locations Displaying",(unsigned long)NumberOfRecordFound];
                         });
                       }
                     if (arrPagination.count > 0){
                         isDataLoaded = true;
                         [tableView_RestList reloadData];
                     }
                 }
                 else
                 {
                     [CUSTOMINDICATORMACRO stopLoadAnimation:self];
                   //  mapView.showsUserLocation = YES;
                   //  [mapView removeAnnotations:mapView.annotations];
                     UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Please check your Internet Connection." preferredStyle:UIAlertControllerStyleAlert];
                     
                     UIAlertAction *okBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                         [self dismissViewControllerAnimated:YES completion:nil];
                     }];
                     //manuallySearchLocation=NO;
                     [alertController addAction:okBtn];
                     [self presentViewController:alertController animated:YES completion:nil];
                 }
             });
         }];
    }
}
-(void)loadAdvancedFilterServiceHelper
{
    [_locationManager startUpdatingLocation];
    ResturantModal *modal = [ResturantModal new];
    NSData *data = [defaults objectForKey:@"logindetails"];
    RegisterModal *registerModalResponeValue = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    NSString *str_mile = (mileString == nil) ? @"" : [NSString stringWithString:mileString];
    NSString *str_Time = (timeString == nil) ? @"" : [NSString stringWithString:timeString];
    NSString *miles = [str_mile stringByReplacingOccurrencesOfString:@" KM" withString:@""];
    
    double milesDouble = [miles doubleValue];
   // double km = milesDouble *1.6;
    double km = milesDouble;
    NSString *myString = [[NSString alloc]initWithFormat:@"%.2f", km];
    modal.mile         = (myString.doubleValue > 0) ? myString : @"";
    
    if ([str_Time isEqualToString:@""]) {
        modal.time = @"";
    }
    else{
        modal.time = [str_Time stringByReplacingOccurrencesOfString:@" " withString:@""];
    }
    if ([happyHourString isEqualToString:@""])
    {
        modal.happyHours = @"";
    }
    else{
        modal.happyHours    =   (happyHourString == nil) ? @"" : happyHourString;
    }
    modal.systemMessage = (searchString == nil) ? @"" : searchString;
    modal.resturantCity = (cityString == nil) ? @"" : cityString;
    modal.Day_name=(_HappHoursDay == nil) ? @"" :_HappHoursDay;
    modal.CityOrArea = strStatus;
    
    [modal setResturantLatitude:stringLat];
    [modal setResturantLongtitude:stringLongt];
    if (modal.systemMessage == nil)
    {
        modal.systemMessage = @"";
    }
    [modal setUserID:registerModalResponeValue.userID];
    NSString *offset = [NSString stringWithFormat:@"%d",IntOffset];
    modal.strOffset = offset;
    [_restModal_Specials setStrOffset:offset];

    [_locationManager stopUpdatingLocation];
    BOOL check=[[AppDelegate shareAppdelegateInstance] checkReachability];
    if(check)
    {
        [CUSTOMINDICATORMACRO startLoadAnimation:self];
        [RestaurantMacro listAllAdvancefilterData:modal withCompletion:^(id obj1) {
            NSLog(@"View Account ===  %@",obj1);
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [CUSTOMINDICATORMACRO stopLoadAnimation:self];
                
                if([obj1 isKindOfClass:[NSArray class]] || [obj1 isKindOfClass:[NSMutableArray class]])
                {
                    NSMutableArray *arrPagination = [[NSMutableArray alloc]init];
                    isDataLoaded = true;

                    NSArray *temp = obj1;
                    if (temp.count>0){
                        arrPagination = obj1[1];
                        if (arrPagination.count>0) {
                            NumberOfRecordFound = [obj1[0] intValue];
                        }
                        [loadingArray addObjectsFromArray:arrPagination
                         ];
                    }
                    
                     if(arrPagination.count>0){
                        lblRestCount.text = [NSString stringWithFormat:@"%lu Places Available",(unsigned long)NumberOfRecordFound];
                        [tableView_RestList reloadData];
                     }
                    else{
                        [self showAlertTitle:@"" Message:@"No Places Available"];
                    }
                }
                else{
                    [self showAlertTitle:@"" Message:@"Please check your Internet Connection."];
                }
            });
         }];
    }
}
#pragma mark -- Scrollview Delegate Method
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (tableView_RestList.contentOffset.y >= (tableView_RestList.contentSize.height - tableView_RestList.frame.size.height))
        
    {

        if ([CheckViewController isEqualToString:@"RestListViewController"] && ![CheckViewController isEqualToString:@""])
        {
            if (isDataLoaded == true && NumberOfRecordFound > 350) {
                isDataLoaded = false;
                IntOffset = IntOffset + 350;
                NSString *offset = [NSString stringWithFormat:@"%d",IntOffset];
                [_restModal_Specials setStrOffset:offset];
                [_restModal_Specials setStrNE_Lat:@""];
                [_restModal_Specials setStrSW_Lat:@""];
                [_restModal_Specials setStrNE_Long:@""];
                [_restModal_Specials setStrSW_Lat:@""];
                [self loadMapValuesServiceHelper:_restModal_Specials];
            }
        }
        else
        {
            if (isDataLoaded == true && NumberOfRecordFound > 350){
                isDataLoaded = false;
                IntOffset = IntOffset + 350;
                [self loadAdvancedFilterServiceHelper];
            }
        }

    }
}

#pragma mark -- TableView Delegate and Datasource Method

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([tableView isEqual:_tblPopup]) {
        return arrySpecials.count;
    }else
      return loadingArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == _tblPopup)
    {
        static NSString *cellIdentifier = @"sampleidentifier";
        DailySpecialCell *cell = (DailySpecialCell *)[_tblPopup dequeueReusableCellWithIdentifier:cellIdentifier];
        
        if(cell == nil)
        {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"DailySpecialCell" owner:self options:nil]objectAtIndex:0];
        }
        if (arrySpecials.count>0) {
            ResturantModal *modelDailySpecialDetails = [arrySpecials objectAtIndex:indexPath.row];
            
            NSString *mystr;
            
           // _btnExpand.tag=indexPath.row;
           // _btnReportHere.tag=indexPath.row;
            
            
            if([modelDailySpecialDetails.specials_weekDay isEqualToString:@"monday"])
            {
                mystr=@"Monday";
            }
            else if ([modelDailySpecialDetails.specials_weekDay isEqualToString:@"tuesday"])
            {
                mystr=@"Tuesday";
            }
            else if ([modelDailySpecialDetails.specials_weekDay isEqualToString:@"wednesday"])
            {
                mystr=@"Wednesday";
            }
            else if ([modelDailySpecialDetails.specials_weekDay isEqualToString:@"thursday"])
            {
                mystr=@"Thursday";
            }
            else if ([modelDailySpecialDetails.specials_weekDay isEqualToString:@"friday"])
            {
                mystr=@"Friday";
            }
            else if ([modelDailySpecialDetails.specials_weekDay isEqualToString:@"saturday"])
            {
                mystr=@"Saturday";
            }
            else
            {
                mystr=@"Sunday";
            }
            
            NSString *trimmedString = [modelDailySpecialDetails.title stringByTrimmingCharactersInSet:
                                       [NSCharacterSet whitespaceCharacterSet]];
            
            NSString *stringStartTime;
            
            NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
            [dateFormatter1 setLocale:locale];

            dateFormatter1.dateFormat = @"HH:mm:ss";
            
            NSDate *date1 = [dateFormatter1 dateFromString:modelDailySpecialDetails.startTime];
            
            dateFormatter1.dateFormat = @"hh:mm a";
            
            stringStartTime = [dateFormatter1 stringFromDate:date1];
            
            stringStartTime = [stringStartTime stringByReplacingOccurrencesOfString:@":00" withString:@""];
            
            
            int stringStartTimeInt=[stringStartTime intValue];
            
            if (stringStartTimeInt<10)
            {
                
                
                NSRange range = NSMakeRange(0,1);
                
                stringStartTime = [stringStartTime stringByReplacingCharactersInRange:range withString:@""];
            }
            else
            {
                stringStartTime = [stringStartTime stringByReplacingOccurrencesOfString:@":00" withString:@""];
            }
            
            
            NSString *stringCloseTime;
            
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            NSLocale *locale2 = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
            [dateFormatter setLocale:locale2];

            dateFormatter.dateFormat = @"HH:mm:ss";
            
            NSDate *date = [dateFormatter dateFromString:modelDailySpecialDetails.endTime];

            dateFormatter.dateFormat = @"hh:mm a";
            
            stringCloseTime = [dateFormatter stringFromDate:date];
            
            stringCloseTime = [stringCloseTime stringByReplacingOccurrencesOfString:@":00" withString:@""];

            int stringCloseTimeInt=[stringCloseTime intValue];
            
            if (stringCloseTimeInt<10)
            {
                NSRange range = NSMakeRange(0,1);
                
                stringCloseTime = [stringCloseTime stringByReplacingCharactersInRange:range withString:@""];
            }
            else
            {
                stringCloseTime = [stringCloseTime stringByReplacingOccurrencesOfString:@":00" withString:@""];
            }

            if ([modelDailySpecialDetails.price isEqualToString:@"N/A"] || [modelDailySpecialDetails.price isEqualToString:@"1/2 Off"] || [modelDailySpecialDetails.price isEqualToString:@"Other"] || [modelDailySpecialDetails.price isEqualToString:@"FREE"])
            {
                cell.lblDays.text=mystr;
                NSString *completeString = [NSString stringWithFormat:@"%@ %@ (%@-%@)",modelDailySpecialDetails.price,trimmedString,stringStartTime,stringCloseTime];
                NSMutableAttributedString *AttributedString = [[NSMutableAttributedString alloc] initWithString:completeString];
                NSString *boldString =modelDailySpecialDetails.price;
                NSRange boldRange = [completeString rangeOfString:boldString];
                [AttributedString addAttribute: NSFontAttributeName value:CalibriBold(11.0f) range:boldRange];
                [cell.lblDescription setAttributedText: AttributedString];
            }
            else
            {
                cell.lblDays.text=mystr;
                NSString *price;
                if ([modelDailySpecialDetails.price rangeOfString:@"$"].location == NSNotFound) {
                    price = [NSString stringWithFormat:@"$%@",modelDailySpecialDetails.price];
                }else
                {
                    price = [NSString stringWithFormat:@"%@",modelDailySpecialDetails.price];
                }
                
                cell.lblDescription.text=[NSString stringWithFormat:@"%@ %@ (%@-%@)",price, trimmedString,stringStartTime,stringCloseTime];
            }
            
            if(modelDailySpecialDetails.isCheckedFavSpecial == NO)
            {
                cell.btnFavSpecial.selected = false;
            }
            else{
                cell.btnFavSpecial.selected = true;
            }
            
            [cell.btnFavSpecial addTarget:self action:@selector(btnFavSpecialClick:) forControlEvents:UIControlEventTouchUpInside];
            cell.btnFavSpecial.tag = indexPath.row;
            
            cell.textLabel.numberOfLines = 2;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.viewSeperator.backgroundColor = [UIColor colorWithRed:153.0f/255.0f green:153.0f/255.0f blue:153.0f/255.0f alpha:1];
        }
        return cell;
    }
    else{
        static NSString *cellIdentifier = @"restlistcell";
        FavouriteCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
        if(cell == nil)
        {
            cell = [[FavouriteCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        }
            
        if([loadingArray count] > 0)
        {
            ResturantModal *restObj = [loadingArray objectAtIndex:indexPath.row];
            [cell.imgView setBackgroundColor:[UIColor clearColor]];
            cell.imgView.layer.masksToBounds = YES;
            cell.imgView.layer.cornerRadius = 5;
            dispatch_async(dispatch_get_main_queue(), ^{
                NSURL *url = [[NSBundle mainBundle] URLForResource:@"loading-1" withExtension:@"gif"];
                NSString *newUrl = [restObj.placeImage stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
                [cell.imgView sd_setImageWithURL:[NSURL URLWithString:newUrl]
                                  placeholderImage:[UIImage imageNamed:@"noimage.png"]];
                
            });
            
            cell.lblResName.text = restObj.resturantName;
            
            NSString *trimmedString = [restObj.resturantCity stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            NSString *city=[NSString stringWithFormat:@"%@,",trimmedString];
            cell.lblAddress.text = [NSString stringWithFormat:@"%@ \n%@ %@ %@",restObj.resturantAddress,city,restObj.resturantState,restObj.resturantPostalCode];
            
            starRatingView = cell.EDStar_userRating;
            
            [self displayStar:restObj.placeRatings];
            
            cell.lblMiles.text=[NSString stringWithFormat:@"%.1f mi",[restObj.mile floatValue]]; //miles 
            
            cell.lblReviews.text=[restObj.totalReview isEqualToString:@"1"]?@"1 review":[restObj.totalReview isEqualToString:@""]?@"0 reviews":[NSString stringWithFormat:@"%@ reviews",restObj.totalReview];
            
            cell.lblLikes.text=[restObj.likecount isEqualToString:@"1"]?@"1 Like":[restObj.likecount isEqualToString:@""]?@"0 Likes":[NSString stringWithFormat:@"%@ Likes",restObj.likecount];
            cell.btnLike.tag = indexPath.row;
            if([restObj.likeORDislike isEqualToString:@"2"] || [restObj.likeORDislike isEqualToString:@""]){
                
                [cell.btnLike setTitle:@"Like" forState:UIControlStateNormal];
                [cell.btnLike setImage:[UIImage imageNamed:@"like"] forState:UIControlStateNormal];
            }
            else{
                [cell.btnLike setTitle:@"Dislike" forState:UIControlStateNormal];
                [cell.btnLike setImage:[UIImage imageNamed:@"disLike"] forState:UIControlStateNormal];
            }
            [cell.btnLike addTarget:self action:@selector(tapOnLike:) forControlEvents:UIControlEventTouchUpInside];
            if ([restObj.placeRatings isEqualToString:@"0"]){
                cell.lblOverallRating.text=@"N/A";
            }
            else{
                cell.lblOverallRating.text=restObj.placeRatings;
            }
            cell.lblOverallRating.layer.cornerRadius = 5;
            cell.lblOverallRating.layer.masksToBounds = YES;
            
            cell.btnFavourite.tag=indexPath.row;
            [cell.btnFavourite setImage:[UIImage imageNamed:restObj.isCheckedFavourite?@"FavSelLIst":@"FavUnSelLIst"] forState:UIControlStateNormal];
            [cell.btnReport addTarget:self action:@selector(pressReportHereBtn_cell:) forControlEvents:UIControlEventTouchUpInside];
            cell.btnReport.tag = indexPath.row;
            int specialCount=[restObj.happy_hour_count intValue]+[restObj.place_specials_count intValue];
          
            [cell.btnSpecials setTitle:[NSString stringWithFormat:@"%d specials",specialCount] forState:UIControlStateNormal];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                //cell.btnSpecials.imageView.hidden=specialCount==0?YES:NO;
                cell.btnSpecials.userInteractionEnabled=specialCount==0?NO:YES;
            });
            [cell.btnSpecials addTarget:self action:@selector(tapOnSpecial:) forControlEvents:UIControlEventTouchUpInside];
    //        cell.btnSpecials.layer.cornerRadius = 10;
    //        cell.btnSpecials.layer.borderWidth = 1;
    //        cell.btnSpecials.clipsToBounds = YES;
    //        cell.btnSpecials.layer.borderColor = [[UIColor colorWithRed:65.0f/255.0f green:113.0f/255.0f blue:156.0f/255.0f alpha:1]CGColor];
            cell.btnSpecials.tag=indexPath.row;
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return cell;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    tapped=YES;
//    HomeViewController *homeVC = [self.storyboard instantiateViewControllerWithIdentifier:@"homescreen"];
//    
//    NSMutableArray *array = [[NSMutableArray alloc]init];
//    ResturantModal *modal = [loadingArray objectAtIndex:indexPath.row];
//    
//    [array addObject:modal];
//    homeVC.restListArray = array;
//    homeVC.selectedRestValue = indexPath.row;
//    homeVC.checkVC = @"restlist";
//  //  homeVC.restVCString = @"singlemap";
//    [self.navigationController pushViewController:homeVC animated:YES];
    
    selectedResturant = [loadingArray objectAtIndex:indexPath.row];
    PromotionsViewController *promtionVC = [self.storyboard instantiateViewControllerWithIdentifier:@"promtions"];
    promtionVC.selectedValue = indexPath.row;
    promtionVC.placeIDString = selectedResturant.placeID;
    promtionVC.promtionsArray = loadingArray;
    promtionVC.selectedWhichController = @"";
    
    BOOL isCheckedRandomSearch = [[NSUserDefaults standardUserDefaults]boolForKey:@"randomsearch"];
    if(isCheckedRandomSearch == TRUE)
    {
        promtionVC.checkTextOrSearch = @"customsearch";
    }
    else
    {
        promtionVC.checkTextOrSearch = @"nosearch";
        [[NSUserDefaults standardUserDefaults]setBool:FALSE forKey:@"randomsearch"];
        
    }
    promtionVC.selectHomeVC=^(ResturantModal *restModel){
        HomeViewController *homeVC = [self.storyboard instantiateViewControllerWithIdentifier:@"homescreen"];
        homeVC.promotionModalOBJ = restModel;
    };
    [self.navigationController pushViewController:promtionVC animated:YES];

    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    if ([tableView isEqual:_tblPopup])
    {
        return UITableViewAutomaticDimension;
    }
    return UITableViewAutomaticDimension;
}
- (void)btnFavSpecialClick:(id)sender {
    UIButton *btn = (UIButton*)sender;
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:btn.tag inSection:0];
    
    DailySpecialCell *cell = (DailySpecialCell *)[_tblPopup cellForRowAtIndexPath:indexPath];
    
    ResturantModal *modelDailySpecialDetails = [arrySpecials objectAtIndex:btn.tag];

    NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:@"logindetails"];
    RegisterModal *registerModalResponeValue = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    NSString *strType;
    if(self.viewTool.selectedSegmentIndex == 0){
        strType = @"h";
    }
    else{
        strType = @"s";
    }
    
    NSString *strFav;
    if(btn.selected == true){
        strFav = @"2";
    }
    else{
        strFav = @"1";
    }

    BOOL check=[[AppDelegate shareAppdelegateInstance] checkReachability];
    if(check)
    {
        [modelDailySpecialDetails setUserID:registerModalResponeValue.userID];
        [CUSTOMINDICATORMACRO startLoadAnimation:self];
        [RestaurantMacro favUnfavSpecialToServer:modelDailySpecialDetails :strFav :strType withCompletion:^(id obj1) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                ResturantModal *responseModal = nil;
                if([obj1 isKindOfClass:[ResturantModal class]])
                {
                    responseModal = obj1;
                    if([responseModal.result isEqualToString:@"success"]){
                        if(btn.selected == true){
                            [modelDailySpecialDetails setIsCheckedFavSpecial:NO];
                        }
                        else{
                            [modelDailySpecialDetails setIsCheckedFavSpecial:YES];
                        }
                        
//                        [_tblPopup beginUpdates];
//                        [_tblPopup reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:false];
//                        [_tblPopup endUpdates];
                        if(btn.selected == true){
                            [cell.btnFavSpecial setSelected:false];
                        }
                        else{
                            [cell.btnFavSpecial setSelected:true];
                        }
                    }
                }
                else
                    [self serverError];
                
                [CUSTOMINDICATORMACRO stopLoadAnimation:self];
            });
        }];
    }
}

- (void)displayStar :(NSString *)ratingVal
{
    if(ratingVal.floatValue <= 2.0)
    {
        starRatingView.starHighlightedImage = [UIImage imageNamed:@"RedStarIcon"];
    }
    else if(ratingVal.floatValue <= 3.9)
    {
        starRatingView.starHighlightedImage = [UIImage imageNamed:@"YellowStarIcon"];
    }
    else
    {
        starRatingView.starHighlightedImage = [UIImage imageNamed:@"GreenStarIcon"];
    }
    
    starRatingView.starImage = [UIImage imageNamed:@"UnStarIcon"];
    starRatingView.maxRating = 5.0;
    starRatingView.delegate = self;
    starRatingView.horizontalMargin = 10;
    starRatingView.editable = NO;
    starRatingView.rating = ratingVal.floatValue;
    starRatingView.displayMode = EDStarRatingDisplayAccurate;
}


#pragma mark -- Action Methods

- (IBAction)tapOnCrossBtn:(id)sender {
    _popupViewBG.hidden = YES;
    _popupView.hidden=YES;
    _btnCross.hidden=YES;
    alertVw.hidden=YES;
}


- (IBAction)reportAction:(id)sender {
    NSString *emailTitle = selectedResturant.resturantName.uppercaseString;
    // Email Content
   NSString *messageBody = @"";
    // NSString *messageBody = selectedResturant.resturantDescription;
    // To address
    NSArray *toRecipents = [NSArray arrayWithObject:@"contact@spotlytespecials.com"];
    
    MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
    if ([MFMailComposeViewController canSendMail]) {
        mc.mailComposeDelegate = self;
        [mc setSubject:emailTitle];
        [mc setMessageBody:messageBody isHTML:NO];
        [mc setToRecipients:toRecipents];
        
        // Present mail view controller on screen
        [self presentViewController:mc animated:YES completion:NULL];
    }

}

- (void)mailComposeController:(MFMailComposeViewController*)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError*)error;
{

    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            [self dismissViewControllerAnimated:YES completion:NULL];
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            [self dismissViewControllerAnimated:YES completion:NULL];
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            [self dismissViewControllerAnimated:YES completion:NULL];
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            [self dismissViewControllerAnimated:YES completion:NULL];
            break;
        default:
            break;
    }
    // Close the Mail Interface
}


-(IBAction)pressViewMap:(id)sender
{
    if ([BackToHomeView isEqualToString:@"HomeView"])
            [self.navigationController popViewControllerAnimated:YES];
    else
    {
        if(loadingArray.count>0)
        {
            HomeViewController *homeVC = [self.storyboard instantiateViewControllerWithIdentifier:@"homescreen"];
            homeVC.restListArray = loadingArray;
            homeVC.checkVC = @"restlist";
            homeVC.restVCString = @"viewmap";
            [self.navigationController pushViewController:homeVC animated:YES];
        }
        else
        {
            [self showAlertTitle:@"" Message:@"No places found."];
        }
    }
}

-(NSMutableArray *)sortRatingArray :(NSArray *)array
{
    NSArray *filteredArray;
    NSString *firstString;
    NSString *secondString;
    
    NSMutableArray *filteredResultArray = [NSMutableArray new];
    
    
    if ([selectedString rangeOfString:@"-"].location == NSNotFound)
    {
        NSLog(@"string does not contain");
        filteredArray = [array filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"placeRatings ==%ld",selectedString.intValue]];
    }
    else
    {
        NSLog(@"string contains!");
        NSArray *subStringArray = [selectedString componentsSeparatedByString:@"-"];
        firstString = [subStringArray firstObject];
        secondString = [subStringArray lastObject];
        
        filteredArray = [array filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"placeRatings <=%ld || placeRatings == %ld",secondString.intValue,firstString.intValue]];
    }
    NSSortDescriptor *sortDescriptor;
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"placeRatings"
                                                 ascending:NO];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    NSArray *sortedArray = [filteredArray sortedArrayUsingDescriptors:sortDescriptors];
    
    [filteredResultArray addObjectsFromArray:sortedArray];
    
    
    return filteredResultArray;
}


-(IBAction)pressFavouriteBtn:(UIButton*)sender
{
    UIButton *btnFavourite=sender;
    
    ResturantModal *modal = [loadingArray objectAtIndex:sender.tag];;
    NSArray *filteredArray = [loadingArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"placeID ==%@",modal.placeID]];
    if(filteredArray>0)
        modal = [filteredArray firstObject];
   
    
    
    NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:@"logindetails"];
    RegisterModal *registerModalResponeValue = [NSKeyedUnarchiver unarchiveObjectWithData:data];

    if(modal.isCheckedFavourite == NO)
    {
        BOOL check=[[AppDelegate shareAppdelegateInstance] checkReachability];
        if(check)
        {
            [modal setUserID:registerModalResponeValue.userID];
            [CUSTOMINDICATORMACRO startLoadAnimation:self];
            [RestaurantMacro favouriteToServer:modal withCompletion:^(id obj1) {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    ResturantModal *responseModal = nil;
                    if([obj1 isKindOfClass:[ResturantModal class]]){
                        responseModal = obj1;
                        if([responseModal.result isEqualToString:@"success"]){
                            [btnFavourite setImage:[UIImage imageNamed:@"FavSelLIst" ] forState:UIControlStateNormal];
                            [modal setIsCheckedFavourite:YES];}
                    }
                    else
                        [self serverError];

                    [CUSTOMINDICATORMACRO stopLoadAnimation:self];
                });
            }];
        }
    }
    else
    {
        [modal setUserID:registerModalResponeValue.userID];
        
        BOOL check=[[AppDelegate shareAppdelegateInstance] checkReachability];
        if(check)
        {
            [CUSTOMINDICATORMACRO startLoadAnimation:self];
            [RestaurantMacro unFavouriteToServer:modal withCompletion:^(id obj1) {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [CUSTOMINDICATORMACRO stopLoadAnimation:self];
                    ResturantModal *responseModal =  nil;
                    if([obj1 isKindOfClass:[ResturantModal class]])
                    {
                        responseModal = obj1;
                        if([responseModal.result isEqualToString:@"success"])
                        {
                            [btnFavourite setImage:[UIImage imageNamed:@"FavUnSelLIst" ] forState:UIControlStateNormal];
                            [modal setIsCheckedFavourite:NO];
                        }
                    }
                    else
                        [self serverError];
                 });
            }];
        }
    }
}

-(void)specialsHappyHoursServiceHelper
{
    BOOL check=[[AppDelegate shareAppdelegateInstance] checkReachability];
    if(check){
        [CUSTOMINDICATORMACRO startLoadAnimation:self];
        [RestaurantMacro getSpecialsHappyHoursToServer:selectedResturant withCompletion:^(id obj1) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if([obj1 isKindOfClass:[ResturantModal class]]){
                    restModal_QuickView  = obj1;
                    if (restModal_QuickView.arrayHappyHours.count>0 || restModal_QuickView.arraySpecials.count>0) {
                        
                        [_tblPopup setContentOffset:CGPointZero animated:NO];
                        [self tapOnHappyHours:nil];
                        _popupViewBG.hidden = NO;
                        _popupView.hidden=NO;
                        _btnCross.hidden=NO;
                         alertVw.hidden=NO;
                    }
                }
                else{
                    _popupViewBG.hidden = NO;
                    _popupView.hidden =NO;
                    _btnCross.hidden  =NO;
                    [self serverError];
                }
                [CUSTOMINDICATORMACRO stopLoadAnimation:self];
            });
        }];
    }
}


- (IBAction)pressNavigateSpecialHours:(UIButton*)sender
{
    PromotionsViewController *promtionVC = [self.storyboard instantiateViewControllerWithIdentifier:@"promtions"];
    promtionVC.selectedValue = sender.tag;
    promtionVC.placeIDString = selectedResturant.placeID;
    promtionVC.promtionsArray = loadingArray;
    promtionVC.selectedWhichController = @"specialshappyhours";
    
    BOOL isCheckedRandomSearch = [[NSUserDefaults standardUserDefaults]boolForKey:@"randomsearch"];
    if(isCheckedRandomSearch == TRUE)
    {
        promtionVC.checkTextOrSearch = @"customsearch";
    }
    else
    {
        promtionVC.checkTextOrSearch = @"nosearch";
        [[NSUserDefaults standardUserDefaults]setBool:FALSE forKey:@"randomsearch"];
        
    }
    promtionVC.selectHomeVC=^(ResturantModal *restModel){
        HomeViewController *homeVC = [self.storyboard instantiateViewControllerWithIdentifier:@"homescreen"];
        homeVC.promotionModalOBJ = restModel;
        homeVC=nil;
     };
    [self.navigationController pushViewController:promtionVC animated:YES];
    
    
}
-(IBAction)pressReportHereBtn_cell : (UIButton*)sender
{
    selectedResturant = [loadingArray objectAtIndex:sender.tag];
    //ResturantModal *modal = [loadingArray objectAtIndex:sender.tag];
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:selectedResturant.resturantName message:@"Specials and Hours are subject to change. To ensure the most up-to-date specials you can view this locations website specials here" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *YesBtn = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                             {
                                 
                                 if(selectedResturant.webSite_Specials.length>0)
                                 {
                                     TermsViewController *termsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"termsvc"];
                                     termsVC.checkVC = @"promotions";
                                     termsVC.checkBack = @"homevc";
                                     termsVC.CheckUrlType=@"SpecialLink";
                                     termsVC.restModal_TermsVC = selectedResturant;
                                     [self.navigationController pushViewController:termsVC animated:YES];
                                 }
                                 else
                                 {
                                     
                                     TermsViewController *termsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"termsvc"];
                                     termsVC.checkVC = @"promotions";
                                     termsVC.checkBack = @"homevc";
                                     termsVC.CheckUrlType=@"ResturantEMail";
                                     termsVC.restModal_TermsVC = selectedResturant;
                                     [self.navigationController pushViewController:termsVC animated:YES];
                                 }
                                 
                                 
                                 
                             }];
    
    UIAlertAction *NoBtn = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                            {
                                [self dismissViewControllerAnimated:YES completion:nil];
                            }];
    
    [alertController addAction:YesBtn];
    [alertController addAction:NoBtn];
    
    [self presentViewController:alertController animated:YES completion:nil];
    
}
-(IBAction)pressReportHereBtn : (UIButton*)sender
{
    
    //ResturantModal *modal = [loadingArray objectAtIndex:sender.tag];
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:selectedResturant.resturantName message:@"Specials and Hours are subject to change. To ensure the most up-to-date specials you can view this locations website specials here" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *YesBtn = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                             {
                                
                                     if(selectedResturant.webSite_Specials.length>0)
                                     {
                                         TermsViewController *termsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"termsvc"];
                                         termsVC.checkVC = @"promotions";
                                         termsVC.checkBack = @"homevc";
                                         termsVC.CheckUrlType=@"SpecialLink";
                                         termsVC.restModal_TermsVC = selectedResturant;
                                         [self.navigationController pushViewController:termsVC animated:YES];
                                     }
                                     else
                                     {
                                         
                                         TermsViewController *termsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"termsvc"];
                                         termsVC.checkVC = @"promotions";
                                         termsVC.checkBack = @"homevc";
                                         termsVC.CheckUrlType=@"ResturantEMail";
                                         termsVC.restModal_TermsVC = selectedResturant;
                                         [self.navigationController pushViewController:termsVC animated:YES];
                                     }
                                     
                                 
                                 
                             }];
    
    UIAlertAction *NoBtn = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action)
                            {
                                [self dismissViewControllerAnimated:YES completion:nil];
                            }];
    
    [alertController addAction:YesBtn];
    [alertController addAction:NoBtn];
    
    [self presentViewController:alertController animated:YES completion:nil];
    
}


-(void)tapOnLike:(UIButton*)sender{
    
    ResturantModal *modal = [[ResturantModal alloc]init];
    
    modal = [loadingArray objectAtIndex:sender.tag];
    NSInteger  strLike = [modal.likecount integerValue];
    if ([modal.likeORDislike isEqualToString:@"2"] || [modal.likeORDislike isEqualToString:@""]) {
        [modal.likeORDislike isEqualToString:@"1"];
        [modal setLikeORDislike:@"1"];
        modal.likecount = [NSString stringWithFormat:@"%ld",(long)strLike+1];
    }
    else{
        [modal.likeORDislike isEqualToString:@"2"];
        [modal setLikeORDislike:@"2"];
        if (strLike > 0)
        {
            modal.likecount = [NSString stringWithFormat:@"%ld",(long)strLike-1];
        }
    }
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [defaults objectForKey:@"logindetails"];
    RegisterModal *registerModalResponeValue = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    NSString * user_IDRegister = registerModalResponeValue.userID;
    [modal setUserID:user_IDRegister];
    NSString *checkcount  = modal.likeORDislike;
    BOOL check=[[AppDelegate shareAppdelegateInstance] checkReachability];
    if(check)
    {
        [CUSTOMINDICATORMACRO startLoadAnimation:self];
        [RestaurantMacro likeOrDislikeToServer:modal withCompletion:^(id obj1) {
            dispatch_async(dispatch_get_main_queue(), ^{
                ResturantModal *responseModal = nil;
                
                if([obj1 isKindOfClass:[ResturantModal class]])
                {
                    responseModal = obj1;
                    if([responseModal.result isEqualToString:@"success"])
                    {
                        
                        [tableView_RestList reloadData];
                    }
                    if([checkcount isEqualToString:@"2"])
                    {
                    }
                }
                else
                {
                    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Please check your Internet Connection." preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction *okBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        [self dismissViewControllerAnimated:YES completion:nil];
                    }];
                    
                    [alertController addAction:okBtn];
                    [self presentViewController:alertController animated:YES completion:nil];
                }
                [CUSTOMINDICATORMACRO stopLoadAnimation:self];
            });
        }];
        
    }
}-(void)tapOnSpecial:(UIButton*)sender{
    
    NSIndexPath *indexpath=[NSIndexPath indexPathForRow:sender.tag inSection:0];
    CGRect myRect = [tableView_RestList rectForRowAtIndexPath:indexpath];
    CGRect rectInSuperview = [tableView_RestList convertRect:myRect toView:[tableView_RestList superview]];
    self.viewTool.selectedSegmentIndex = 0;
    [self changePositionOfPopupView:rectInSuperview];
    
    selectedResturant = [loadingArray objectAtIndex:sender.tag];
    _lblpopupName.text=selectedResturant.resturantName;

    
    if ([selectedResturant.happy_hour_count intValue]+[selectedResturant.place_specials_count intValue]!=0) {
        [self specialsHappyHoursServiceHelper];
        selectedRow=sender.tag;
    }
 }

-(void)changePositionOfPopupView:(CGRect)rect{
    int x_position=rect.origin.y+rect.size.height-80;
    
    int popup_Y=self.view.frame.size.height-(self.tabBarController.tabBar.bounds.size.height+_popupView.frame.size.height+40);
    
    if (x_position>popup_Y)
        X_axis.constant=self.view.frame.size.height-(self.tabBarController.tabBar.bounds.size.height+_popupView.frame.size.height+40);
    else
        X_axis.constant=x_position;
}

- (IBAction)tapOnHappyHours:(id)sender {
    [self colorChangeOnPressTab:@[_btnHppyHours,_btnDailySpecials]];
    arrySpecials = restModal_QuickView.arrayHappyHours;
    [self showHidePopup:arrySpecials.count];
}

- (IBAction)tapOnDailySpecials:(id)sender {
    [self colorChangeOnPressTab:@[_btnDailySpecials,_btnHppyHours]];
    arrySpecials = restModal_QuickView.arraySpecials;
    [self showHidePopup:arrySpecials.count];
}


-(void)showHidePopup:(NSInteger)count{
    if (count==0) {
        _emptyView.hidden=NO;
        _btnReportHere.hidden=YES;
        _btnExpand.hidden=YES;
        _tblPopup.hidden=YES;
 
    }else{
        _tblPopup.hidden=NO;
        _btnReportHere.hidden=NO;
        _btnExpand.hidden=NO;
        _emptyView.hidden=YES;
       [_tblPopup reloadData];
    }
}



-(void)colorChangeOnPressTab:(NSArray*)arry { //:(NSArray*)arryView
    dispatch_async(dispatch_get_main_queue(), ^{
    
        for (btnNo=0; btnNo<arry.count; btnNo++) {
            if (btnNo==0) {
                UIButton *button =(UIButton*)arry[0];
                [button setTitleColor:[UIColor colorWithRed:153.0f/255.0f green:153.0f/255.0f blue:153.0f/255.0f alpha:1] forState:UIControlStateNormal];
            }else
            {
                UIButton *button =(UIButton*)arry[btnNo];
                [button setTitleColor:[UIColor colorWithRed:54.0f/255.0f green:69.0f/255.0f blue:79.0f/255.0f alpha:1] forState:UIControlStateNormal];
            }
        }
        
//        for (btnNo=0; btnNo<arryView.count; btnNo++) {
//            if (btnNo==0) {
//                UIView *button =(UIView*)arryView[0];
//                button.backgroundColor=[UIColor colorWithRed:0.0f/255.0f green:204.0f/255.0f blue:255.0f/255.0f alpha:1];
//            }else
//            {
//                UIView *button =(UIView*)arryView[btnNo];
//                button.backgroundColor=[UIColor whiteColor];
//            }
//        }
    });
}


-(void)serverError{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Please check your Internet Connection." preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
    [alertController addAction:okBtn];
    [self presentViewController:alertController animated:YES completion:nil];
}

@end
