//
//  PromtionDetailsViewController.m
//  Spotlyte
//
//  Created by Admin on 18/03/16.
//  Copyright © 2016 Naveen. All rights reserved.
//

#import "PromtionDetailsViewController.h"
#import "HomeViewController.h"
#import "CommentsViewController.h"
#import "NearByViewController.h"
#import "HeaderView.h"
#import "PromotionsViewController.h"

@interface PromtionDetailsViewController ()

@end

@implementation PromtionDetailsViewController
@synthesize promtionModalObj;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    HeaderView *header=[[HeaderView alloc]initWithTitle:@"SPOTLYTE PROMOTIONS" controller:self];
    [header addSubview:header.btnback];
    [self.view addSubview:header];

    
    _btnViewLocation.layer.borderColor=[UIColor darkGrayColor].CGColor;
    _btnViewLocation.layer.borderWidth=2.0f;
    _btnViewLocation.layer.cornerRadius=10.0f;
    
    asynchHotelImage.layer.cornerRadius = 3;
    [asynchHotelImage setBackgroundColor:[UIColor clearColor]];
    asynchHotelImage.layer.masksToBounds = YES;
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    dispatch_async(dispatch_get_main_queue(), ^{
        NSString *newUrl = [promtionModalObj.promotionImage stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
        [asynchHotelImage sd_setImageWithURL:[NSURL URLWithString:newUrl] placeholderImage:[UIImage imageNamed:@"noimage.png"]];
        [self displayAllData];
    });
    
}


-(void)displayAllData{
    lblName.text=_resturantModalObj.resturantName;
    _lblPromotionTitle.text=promtionModalObj.Promotion_Type;
    lblmiles.text=[NSString stringWithFormat:@"%.1f miles",[_resturantModalObj.mile floatValue]];
    lblReviews.text=[_resturantModalObj.totalReview isEqualToString:@"1"]?@"1 review":[_resturantModalObj.totalReview isEqualToString:@"0"]?@"0 review":[NSString stringWithFormat:@"%@ reviews",_resturantModalObj.totalReview];
    
    NSString *city=[NSString stringWithFormat:@"%@,",_resturantModalObj.resturantCity ];
    NSString *state=_resturantModalObj.resturantState;
    NSString *zipcode=_resturantModalObj.resturantPostalCode;

    lblAddress.text=[NSString stringWithFormat:@"%@\n%@ %@ %@",_resturantModalObj.resturantAddress,[city isEqualToString:@""]?@"":city,[state isEqualToString:@""]?@"":state,[zipcode isEqualToString:@""]?@"":zipcode];
    
    lblOverallRating.text=_resturantModalObj.placeRatings;
    
    [_btnFavourite addTarget:self action:@selector(pressFavouriteBtn:) forControlEvents:UIControlEventTouchUpInside];
    [_btnFavourite setImage:[UIImage imageNamed:[promtionModalObj.promotionFavStatus isEqualToString:@"No"]?@"FavUnSel35":@"FavSel35"] forState:UIControlStateNormal];

    [self displayStar:_resturantModalObj.placeRatings];
     txt_desc.text       =   promtionModalObj.promotionDesc;
    [txt_desc setContentOffset:CGPointZero animated:YES];

    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [dateFormat setLocale:locale];

    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    
    NSString *strDateStart = promtionModalObj.promotionStartDate;
    NSDate *dateStart = [dateFormat dateFromString:strDateStart];
    
    NSString *strDateEnd = promtionModalObj.promotionExpiredDate;
    NSDate *dateEnd = [dateFormat dateFromString:strDateEnd];
    
    [dateFormat setDateFormat:@"MMM dd,yyy"];
    NSString *strStartDate = [dateFormat stringFromDate:dateStart];
    lblStartDate.text = strStartDate;
        
    NSString *strEndDate = [dateFormat stringFromDate:dateEnd];
    lblEndDate.text = strEndDate;
}

- (void)displayStar :(NSString *)ratingVal
{
   /*if(IS_IPHONE_6PLUS)
    {
        if(_resturantModalObj.placeRatings.floatValue <= 2.0)
        {
            starRating.starHighlightedImage = [UIImage imageNamed:@"star-1_R.png"];
        }
        else if(_resturantModalObj.placeRatings.floatValue <= 3.9)
        {
            starRating.starHighlightedImage = [UIImage imageNamed:@"star-1_Y.png"];
        }
        else
        {
            starRating.starHighlightedImage = [UIImage imageNamed:@"star-1_G.png"];
        }
        starRating.starImage = [UIImage imageNamed:@"unstartemp.png"];
    }
    else if (IS_IPHONE_6)
    {
        if(_resturantModalObj.placeRatings.floatValue <= 2.0)
        {
            starRating.starHighlightedImage = [UIImage imageNamed:@"star-1_R.png"];
        }
        else if(_resturantModalObj.placeRatings.floatValue <= 3.9)
        {
            starRating.starHighlightedImage = [UIImage imageNamed:@"star-1_Y.png"];
        }
        else
        {
            starRating.starHighlightedImage = [UIImage imageNamed:@"star-1_G.png"];
        }
        starRating.starImage = [UIImage imageNamed:@"unstartemp.png"];
        
    }
    else
    {
        starRating.starImage = [UIImage imageNamed:@"UnStarIcon"];
        if(_resturantModalObj.placeRatings.floatValue <= 2.0)
        {
            starRating.starHighlightedImage = [UIImage imageNamed:@"RedStarIcon"];
            
        }
        else if(_resturantModalObj.placeRatings.floatValue <= 3.9)
        {
            starRating.starHighlightedImage = [UIImage imageNamed:@"YellowStarIcon"];
        }
        else
        {
            starRating.starHighlightedImage = [UIImage imageNamed:@"GreenStarIcon"];
        }
    }*/
    
    if(_resturantModalObj.placeRatings.floatValue <= 2.0)
    {
        starRating.starHighlightedImage = [UIImage imageNamed:@"star-1_R.png"];
    }
    else if(_resturantModalObj.placeRatings.floatValue <= 3.9)
    {
        starRating.starHighlightedImage = [UIImage imageNamed:@"star-1_Y.png"];
    }
    else
    {
        starRating.starHighlightedImage = [UIImage imageNamed:@"star-1_G.png"];
    }
    starRating.starImage = [UIImage imageNamed:@"unstartemp.png"];
  
    /* starRatingView.maxRating = 5.0;
    starRatingView.delegate = self;
    starRatingView.horizontalMargin = 12;
    starRatingView.editable = YES;
    starRatingView.rating = responseModalForStarRating.placeRatings.floatValue;
    starRatingView.displayMode = EDStarRatingDisplayAccurate;
    
    lblRating_Details.text = responseModalForStarRating.placeRatings;*/

    
    starRating.maxRating = 5.0;
    starRating.delegate = self;
    starRating.horizontalMargin = 12;
    starRating.editable = NO;
    starRating.rating = _resturantModalObj.placeRatings.floatValue;
    starRating.displayMode = EDStarRatingDisplayAccurate;

}


#pragma mark - Selector Button Action Methods


-(IBAction)pressFavouriteBtn :(id)sender
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [defaults objectForKey:@"logindetails"];
    RegisterModal *registerModalResponeValue = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    NSString * user_IDRegister = registerModalResponeValue.userID;
    [promtionModalObj setUser_ID:user_IDRegister];
    if([promtionModalObj.promotionFavStatus isEqualToString:@"No"])
        [self favouriteServiceHelper];
    else
        [self unFavouriteServiceHelper];
}

-(void)favouriteServiceHelper
{
    BOOL check=[[AppDelegate shareAppdelegateInstance] checkReachability];
    if(check)
    {
        [CUSTOMINDICATORMACRO startLoadAnimation:self];
        [PromotionMacro addFavouriteSpotlyteToServer:promtionModalObj withCompletion:^(id obj1) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [CUSTOMINDICATORMACRO stopLoadAnimation:self];
                if([obj1 isKindOfClass:[ResturantModal class]])
                {
                    ResturantModal *responseModal = nil;
                    responseModal = obj1;
                    if([responseModal.result isEqualToString:@"success"])
                    {
                        [_btnFavourite setImage:[UIImage imageNamed:@"FavSel35"] forState:UIControlStateNormal];
                        promtionModalObj.promotionFavStatus=@"Yes";
                    }
                }
                else
                    [self serverError];
            });
        }];
    }
}

-(void)unFavouriteServiceHelper
{
    BOOL check=[[AppDelegate shareAppdelegateInstance] checkReachability];
    if(check)
    {
        [CUSTOMINDICATORMACRO startLoadAnimation:self];
        [PromotionMacro unFavouriteSpotlyteToServer:promtionModalObj withCompletion:^(id obj1) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [CUSTOMINDICATORMACRO stopLoadAnimation:self];
                if([obj1 isKindOfClass:[ResturantModal class]])
                {
                    ResturantModal *responseModal;
                    responseModal = obj1;
                    if([responseModal.result isEqualToString:@"success"])
                    {
                        [_btnFavourite setImage:[UIImage imageNamed:@"FavUnSel35"] forState:UIControlStateNormal];
                        promtionModalObj.promotionFavStatus=@"No";
                    }
                }
                else
                    [self serverError];
            });
        }];
    }
    
}


- (IBAction)tapOnViewPromotions:(id)sender {
    
    HomeViewController *homeVC = [self.storyboard instantiateViewControllerWithIdentifier:@"homescreen"];
    NSMutableArray *array = [[NSMutableArray alloc]init];
    [array addObject:_resturantModalObj];
    homeVC.restListArray = array;
    homeVC.checkVC = @"restlist";
    appDelegate.IsFromNewHome = YES;
    [self.navigationController pushViewController:homeVC animated:YES];
}
    
//    PromotionsViewController *promtionVC = [self.storyboard instantiateViewControllerWithIdentifier:@"promtions"];
//    promtionVC.placeIDString = [NSString stringWithFormat:@"%@",_resturantModalObj.placeID];
//    promtionVC.promtionsArray = [_promtions copy] ;
//    promtionVC.selectedWhichController = @"eventSection";
//    promtionVC.checkTextOrSearch = @"customsearch";
//    [self.navigationController pushViewController:promtionVC animated:YES];}


-(void)serverError{
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Please check your Internet Connection." preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
    [alertController addAction:okBtn];
    [self presentViewController:alertController animated:YES completion:nil];
    
}

@end
