//
//  RestListViewController.h
//  Spotlyte
//
//  Created by Admin on 17/03/16.
//  Copyright © 2016 Naveen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OAuthConsumer.h"
//#import "NSObject+YAJL.h"
#import "ResturantModal.h"
#import "HMSegmentedControl.h"

@interface RestListViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,EDStarRatingProtocol,CLLocationManagerDelegate, MKMapViewDelegate,MFMailComposeViewControllerDelegate>//,NSURLConnectionDelegate,NSURLConnectionDataDelegate>
{
    IBOutlet UITableView    *tableView_RestList;
     EDStarRating    *starRatingView;
    IBOutlet UILabel *lblRestCount;

    IBOutlet NSLayoutConstraint *X_axis;
    CLLocationManager *_locationManager;
    NSString *stringLat;
    NSString *stringLongt;
    BOOL isCheckedLoadMap;
}
@property (nonatomic) int NumberOfRecordFound;
@property (strong, nonatomic) NSString *stringLat;
@property (strong, nonatomic) NSString *stringLongt;
@property (strong, nonatomic) NSArray   *hotelNameArray;
@property (strong, nonatomic) NSArray   *hotelImageArray;
@property (strong, nonatomic) NSMutableArray   *loadingArray;
@property (nonatomic, strong) NSMutableData *responseData;
@property (nonatomic, strong) NSMutableString *JSON1;
@property (nonatomic, strong) NSString *selectedString;
@property (nonatomic, strong) NSString *selectedCategory;
@property (nonatomic, strong) NSString *mileString;
@property (nonatomic, strong) NSString *happyHourString;
@property (nonatomic, strong) NSString *timeString;
@property (nonatomic, strong) NSString *cityString;
@property (nonatomic, strong) NSString *status;
@property (nonatomic, strong) NSString *HappHoursDay;

@property (nonatomic, strong) NSString *searchString;

@property (nonatomic, strong) NSArray *newarray;

@property (nonatomic, strong) NSString *strStatus;
@property (nonatomic, strong) ResturantModal *restModal_Specials;
@property (nonatomic, strong) ResturantModal *NearByModel;

////Pagination

@property (nonatomic,strong) NSMutableArray *ArrPagination;

//////////////////// popup view
@property (strong, nonatomic) IBOutlet UIView *popupViewBG;

@property (strong, nonatomic) IBOutlet UIView *popupView;
@property (strong, nonatomic) IBOutlet UILabel *lblpopupName;
@property (strong, nonatomic) IBOutlet UIButton *btnHppyHours;
@property (strong, nonatomic) IBOutlet UIButton *lblDailySpecial;

@property (strong, nonatomic) IBOutlet UITableView *tblPopup;
@property (strong, nonatomic) IBOutlet UIButton *btnCross;

@property (strong, nonatomic) IBOutlet UIButton *btnHappyHours;
@property (strong, nonatomic) IBOutlet UIButton *btnDailySpecials;
@property (strong, nonatomic) IBOutlet UIButton *btnReportHere;
@property (strong, nonatomic) IBOutlet UIButton *btnExpand;


-(void)getResturentModel:(ResturantModal *)model;

@property (strong, nonatomic) IBOutlet UIView *emptyView;

@property (weak, nonatomic) IBOutlet HMSegmentedControl *viewTool;

@end
