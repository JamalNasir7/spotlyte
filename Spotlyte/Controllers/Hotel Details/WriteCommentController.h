//
//  WriteCommentController.h
//  Spotlyte
//
//  Created by Hemant on 23/02/17.
//  Copyright © 2017 Hemant. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WriteCommentController : UIViewController<EDStarRatingProtocol,UITextViewDelegate>

@property(copy,nonatomic)void (^AlertHandler)(BOOL success);
@property (strong, nonatomic) IBOutlet UILabel *lblPlaceHolder;


@property (strong, nonatomic) IBOutlet EDStarRating *GiveStarRating;
@property (strong, nonatomic) IBOutlet UIImageView *imgView;
@property (strong, nonatomic) IBOutlet UILabel *lblName;
@property (strong, nonatomic) IBOutlet UITextView *txtView;
@property (strong, nonatomic) IBOutlet UIButton *btnCancel;
@property (strong, nonatomic) IBOutlet UIButton *btnPost;
@property (strong, nonatomic) RegisterModal *registerModalResponeValue1;
@property (strong, nonatomic)  ResturantModal *restModalOBJ1;
@property (strong, nonatomic)  NSString *strGivenRating;

-(void)recievedData:(void (^)(BOOL success))Callback;
@property (strong, nonatomic)  UIImage *img;
@end
