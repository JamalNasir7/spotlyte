//
//  HappyHoursCell.h
//  Spotlyte
//
//  Created by Admin on 28/09/16.
//  Copyright © 2016 Naveen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HappyHoursCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblAllItems;

@property (strong, nonatomic) IBOutlet UILabel *lbl_Day;
@property (strong, nonatomic) IBOutlet UILabel *lbl_Price;
@property (strong, nonatomic) IBOutlet UILabel *lbl_Time;
@property (strong, nonatomic) IBOutlet UILabel *lbl_ItemName;

@property (strong, nonatomic) IBOutlet UIButton *btnFavSpecial;

@end
