//
//  DailySpecialViewController.h
//  Spotlyte
//
//  Created by CIPL227-MOBILITY on 16/03/16.
//  Copyright © 2016 Naveen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ResturantModal.h"
#import <MessageUI/MFMailComposeViewController.h>


@interface DailySpecialViewController : UIViewController<UITableViewDelegate, UITableViewDataSource,MFMailComposeViewControllerDelegate>
{
    IBOutlet UITableView    *tableView_DailySpecial;
    IBOutlet UIView         *view_NoSpecial;
}

@property (weak, nonatomic) IBOutlet UIButton *btnClickHere;
@property (weak, nonatomic) IBOutlet UILabel *lbl_TopView;;

@property (weak, nonatomic)  ResturantModal *modalOBJ;


@property (strong, nonatomic) NSDictionary *dictWeeks;

@property (strong, nonatomic) NSArray *arraySpecialforday;

-(IBAction)pressClickHereBtn:(id)sender;
@end
