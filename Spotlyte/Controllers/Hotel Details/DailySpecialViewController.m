//
//  DailySpecialViewController.m
//  Spotlyte
//
//  Created by CIPL227-MOBILITY on 16/03/16.
//  Copyright © 2016 Naveen. All rights reserved.
//

#import "DailySpecialViewController.h"
#import <CoreText/CoreText.h>

@interface DailySpecialViewController ()

@end

@implementation DailySpecialViewController
@synthesize arraySpecialforday,dictWeeks;
@synthesize btnClickHere;
@synthesize modalOBJ;
@synthesize lbl_TopView;

- (void)viewDidLoad {
    [super viewDidLoad];
    NSString *tem = self.btnClickHere.titleLabel.text;
    if (tem != nil && ![tem isEqualToString:@""]) {
        NSMutableAttributedString *temString=[[NSMutableAttributedString alloc]initWithString:tem];
        [temString addAttribute:NSUnderlineStyleAttributeName
                          value:[NSNumber numberWithInt:1]
                          range:(NSRange){0,[temString length]}];
        
        self.btnClickHere.titleLabel.attributedText = temString;
        
        dictWeeks = @{@"SUNDAY" : @[@"No Specials"],
                      @"MONDAY" : @[@"$5 SKY Vodka Bloody Marys & Screwdrivers", @"$3 Miller Lite & Bud Light Drafts"],
                      @"TUESDAY" : @[@"$7 Burger w/2 Toppings and a Domestic Pint"],
                      @"WEDNESDAY" : @[@"1 off Domestic Beers"],
                      @"THURSDAY" : @[@"Trivia Night", @"$5 Burgers"],
                      @"FRIDAY" : @[@"$5 Bombs"],
                      @"SATURDAY" : @[@"$5 Bombs"]};
        arraySpecialforday = [[NSArray alloc]initWithObjects:@"SUNDAY",@"MONDAY",@"TUESDAY",@"WEDNESDAY",@"THURSDAY",@"FRIDAY",@"SATURDAY", nil];
    }
    lbl_TopView.text = modalOBJ.resturantName;
    if([modalOBJ.resturantName isEqualToString:@"Vessey Museum"])
    {
        view_NoSpecial.hidden = NO;
        tableView_DailySpecial.hidden = YES;
    }
    else
    {
        view_NoSpecial.hidden = YES;
        tableView_DailySpecial.hidden = NO;
    }
    
    
}



#pragma mark - TableView Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [arraySpecialforday count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [arraySpecialforday objectAtIndex:section];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSString *sectionTitle = [arraySpecialforday objectAtIndex:section];
    NSArray *sectionDish = [dictWeeks objectForKey:sectionTitle];
    return [sectionDish count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"identifierDailySpecial" forIndexPath:indexPath];
    
    NSString *sectionTitle = [arraySpecialforday objectAtIndex:indexPath.section];
    NSArray *sectionDish = [dictWeeks objectForKey:sectionTitle];
    NSString *strSplDish = [sectionDish objectAtIndex:indexPath.row];
    cell.textLabel.text = strSplDish;
    cell.textLabel.text = [NSString stringWithFormat:@"     %@", strSplDish];
    [cell.textLabel setTextColor:[UIColor colorWithRed:95/255.0 green:95.0/255.0 blue:95.0/255.0 alpha:1.0]];
    [cell.textLabel setFont:[UIFont boldSystemFontOfSize:12]];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 30;
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    UIImageView *logo = [[UIImageView  alloc]initWithFrame:CGRectMake(20, 10, 11, 11)];
    logo.image = [UIImage imageNamed:@"Dots"];
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 30.0f)];
    [view setBackgroundColor:[UIColor clearColor]];
    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(40, 5, 150, 20)];
    NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:[arraySpecialforday objectAtIndex: section]];
    [attString addAttribute:(NSString*)kCTUnderlineStyleAttributeName
                      value:[NSNumber numberWithInt:kCTUnderlineStyleSingle]
                      range:(NSRange){0,[attString length]}];
    [lbl setFont:[UIFont boldSystemFontOfSize:15]];
    
    [lbl setTextColor:[UIColor colorWithRed:0/255.0 green:113.0/255.0 blue:201.0/255.0 alpha:1.0]];
    [view addSubview:logo];
    [view addSubview:lbl];
    lbl.attributedText = attString;
    
    return view;
}

-(IBAction)pressback:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)pressClickHereBtn:(id)sender
{
    [self reportAction];
}

-(void)reportAction
{
    NSString *emailTitle = @"BarName";
    // Email Content
    NSString *messageBody = @"test mail";
    // To address
    NSArray *toRecipents = [NSArray arrayWithObject:@"barname@spotlyte.com"];
    
    MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
    mc.mailComposeDelegate = self;
    [mc setSubject:emailTitle];
    [mc setMessageBody:messageBody isHTML:NO];
    [mc setToRecipients:toRecipents];
    
    // Present mail view controller on screen
    [self presentViewController:mc animated:YES completion:NULL];
}

- (void)mailComposeController:(MFMailComposeViewController*)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError*)error;
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            [self dismissViewControllerAnimated:YES completion:NULL];
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            [self dismissViewControllerAnimated:YES completion:NULL];
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            [self dismissViewControllerAnimated:YES completion:NULL];
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            [self dismissViewControllerAnimated:YES completion:NULL];
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
