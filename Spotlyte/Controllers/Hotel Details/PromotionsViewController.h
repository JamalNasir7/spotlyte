//
//  PromotionsViewController.h
//  Spotlyte
//
//  Created by CIPL242-MOBILITY on 15/03/16.
//  Copyright © 2016 Naveen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EDStarRating.h"
#import "ResturantModal.h"
#import <CoreText/CoreText.h>
#import "Base64.h"
#import "HMSegmentedControl.h"

#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import "SBSliderView.h"
#import "HotelDetailTableView.h"

typedef void(^homeVCSelection) (ResturantModal *);

@interface PromotionsViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,EDStarRatingProtocol,UITextViewDelegate,UITextFieldDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UICollectionViewDataSource,UICollectionViewDelegate,CLLocationManagerDelegate, MKMapViewDelegate,UIGestureRecognizerDelegate,MFMailComposeViewControllerDelegate,UIScrollViewDelegate, UIGestureRecognizerDelegate>
{
    IBOutlet UIView *lblBelowRating;
    IBOutlet UITableView    *tableView_Promotions;
    IBOutlet HotelDetailTableView    *tableView_Promt;
    IBOutlet UIView         *view_imageText;
    IBOutlet UIView         *view_Promotions;
    IBOutlet UIView         *view_PromotionsDetails;
    IBOutlet UIView         *view_main;
    IBOutlet UIView         *view_Tab;
    IBOutlet UIView         *view_Details;
    
    IBOutlet UIView         *view_HotalDetail;
    IBOutlet UIView         *view_Dailyspecials;

    IBOutlet UIView         *view_LiveFeed;
    IBOutlet UIView         *view_TopBar;
    IBOutlet UIView         *view_TextView;
    
    __weak IBOutlet NSLayoutConstraint *tableHeight;
    IBOutlet UIView *view_CommentPhotos;
    
    __weak IBOutlet UIScrollView *mainscrollView;
    
    __weak IBOutlet UIScrollView *innerScrollView;
    
    //IBOutlet UIView         *view_BottomView;
    NSInteger                selectedIndex;
    BOOL                    isCheckedSelectedIndexPath;
    IBOutlet  UITableView    *tableView_LiveFeed;
    IBOutlet UITableViewCell *tableViewCell_LiveFeed;
    IBOutlet UITableView        *tableView_DailySpecial;
     IBOutlet UIView         *view_NoSpecial;
     IBOutlet UIView         *view_Special;
    IBOutlet EDStarRating    *starRatingView;
    IBOutlet EDStarRating    *starRating_LiveFeed;
    IBOutlet EDStarRating    *starRating_userGiven;

    
    IBOutlet UIButton       *btn_LiveFeed;
    IBOutlet UIButton       *btn_Details;
    IBOutlet UIButton       *btn_Promtions;
    IBOutlet UIButton       *btn_FavSelected;
    
    
    IBOutlet UIButton       *btn_Emonji1;
    IBOutlet UIButton       *btn_Emonji2;
    IBOutlet UIButton       *btn_Emonji3;
    IBOutlet UIButton       *btn_Emonji4;
    IBOutlet UILabel        *lbl_EmoniTime;

    
    IBOutlet UIButton       *comments_Btn;
    IBOutlet UIButton       *photos_Btn;
    IBOutlet UIView         *photosView;
    IBOutlet UICollectionView *photos_CollectionView;
    
    IBOutlet UILabel    *lbl_HeaderName;
    IBOutlet UILabel    *lblRestName_Details;
    IBOutlet UILabel    *lblAddress_Details;
    IBOutlet UITextView *lblDesc_Details;
    IBOutlet UILabel    *lblEmail_Details;
    IBOutlet UILabel    *lblPhoneNo_Details;
    IBOutlet UILabel    *lbHours_Details;
    IBOutlet UILabel    *lblRating;
    IBOutlet UILabel    *lblRating_Details;
    IBOutlet UILabel    *lbl_Review;
    
    IBOutlet UIView     *view_NoComments;
    IBOutlet UILabel    *lbl_NoComments;


    IBOutlet UILabel *lblNoDescription;
    IBOutlet UIImageView *restImage_Details;
    IBOutlet UIButton *btn_CheckIn;
    
    IBOutlet UIScrollView *sliderMainScroller;
    IBOutlet UIPageControl *pageIndicator;
    
    
    IBOutlet UIView *view_NoPromtions;
    
    UIImage *commentsImage;
    IBOutlet UITextField *comment_LiveFeed;
    NSMutableArray * liveFeedArray;
    int selectedView;
    BOOL isShowKeyBoard;
    BOOL isCheckedCheckIn;
    IBOutlet UIImageView *profileImg;
    
    __weak IBOutlet UILabel *lbl_phonenumber;
    
    
    __weak IBOutlet UIView *view_Rating;
    BOOL isCheckedDailySpecials;
    BOOL isCheckedPromotions;
    BOOL isCheckedHotel;
    
    IBOutlet MKMapView *mapview;

    
    int i;
    int j;
    int m;
    NSInteger emonji_X;
    NSString *user_IDRegister;
    RegisterModal *registerModalResponeValue;
    IBOutlet NSLayoutConstraint *lblTime_X;

    PromotionsViewController *viewController;
    
    CLLocationManager *_locationManager;
    float totalDistance;
    ResturantModal *restModalOBJ;
    
    IBOutlet UIView *view_HappyHours;
     ResturantModal *restModal_QuickView;
    
    IBOutlet UIButton *backBtn;
    IBOutlet UIButton *btn_HappyHours;
    IBOutlet UIButton *btn_DailySpecials;
    IBOutlet UIView *view_Empty;

    IBOutlet NSLayoutConstraint *lblHeader_X;
    
    IBOutlet NSLayoutConstraint *lblDesc_Y;
    IBOutlet UILabel *descriptionAboveLabel;
    
    IBOutlet NSLayoutConstraint *TextViewHeight_Y;
    
    
    IBOutlet UIButton *btn_comment;
    IBOutlet NSLayoutConstraint *viewCommentHeight_Y;
    IBOutlet UIView *view_clickHere;
    
    
    
    IBOutlet UIButton *btnYelp;
    IBOutlet UIButton *btnOpenTable;
    IBOutlet UIButton *btnGrubHub;
    IBOutlet UIButton *btnBearMenus;
    IBOutlet UIButton *btnWebSite;
    IBOutlet UIButton *btnCalling;
    
}

@property (nonatomic, retain) ResturantModal *restModalObj_Promtions;
@property (nonatomic, retain) NSMutableArray *promtionsArray;
@property (nonatomic, retain) NSMutableArray *photosArray;
@property (nonatomic,retain) NSString *strEventImage;
@property  NSInteger selectedValue;

@property (strong, nonatomic) NSArray *hotelNameArray;
@property (strong, nonatomic) NSArray *hotelImageArray;

@property (weak, nonatomic) IBOutlet UIButton *btnClickHere;
@property (weak, nonatomic) IBOutlet UILabel *lbl_TopView;;

@property (weak, nonatomic)  ResturantModal *modalOBJ;


@property (strong, nonatomic) NSDictionary *dictWeeks;

@property (strong, nonatomic) NSArray *arraySpecialforday;
@property (nonatomic, retain) NSMutableArray *hotelPromtionsArray;
@property (strong, nonatomic) NSString *placeIDString;
@property (strong, nonatomic) NSString *selectedWhichController;
@property (strong, nonatomic) NSString *checkTextOrSearch;

@property (nonatomic,copy) homeVCSelection selectHomeVC;

@property (nonatomic, retain) NSString *dummyvalue;

@property (nonatomic, strong) NSMutableDictionary *imageDownloadsInProgress;

//@property (weak, nonatomic) IBOutlet HMSegmentedControl *viewTool;

-(IBAction)pressCommentsOrPhotosTab:(id)sender;


-(IBAction)btnleftslide:(id)sender;
-(IBAction)btnrightslide:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btn_AddReview;


-(IBAction)clickDashBoard:(id)sender;
-(IBAction)pressBack:(id)sender;
-(IBAction)pressDailySpecial:(id)sender;
-(IBAction)pressTabButton:(id)sender;
-(IBAction)clickFavourite:(id)sender;
-(IBAction)pressDirections:(id)sender;
-(IBAction)pressSubmit:(id)sender;
-(IBAction)pressEmonjis:(id)sender;
- (IBAction)pressSpecialHappyHours:(id)sender;

-(IBAction)checkInButtonHandler:(id)sender;
-(IBAction)pressReportHereBtn : (id)sender;
-(IBAction)pressCallNumber: (id)sender;
-(IBAction)pressWebsite:(id)sender;
- (IBAction)readAllReviews:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIButton *btn_readAllreviews;

@property (weak, nonatomic) IBOutlet UILabel *lbl_secondRating;

@property (weak, nonatomic) IBOutlet HMSegmentedControl *viewTool;

@property (weak, nonatomic) IBOutlet UIButton *btn_addPhotos;
- (IBAction)onPressAddPhotos:(id)sender;



@end
