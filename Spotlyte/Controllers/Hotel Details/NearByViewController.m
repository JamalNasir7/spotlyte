//
//  NearByViewController.m
//  Spotlyte
//
//  Created by Admin on 25/03/16.
//  Copyright © 2016 Naveen. All rights reserved.
//

#import "NearByViewController.h"
#import "MapAnnotation.h"
#define kBaseUrl @"http://maps.googleapis.com/maps/api/directions/json?"
@interface NearByViewController ()

@end

@implementation NearByViewController
@synthesize restModal,promotionModal;

- (void)viewDidLoad {
    [super viewDidLoad];
    if(restModal.resturantLongtitude !=nil && restModal.resturantLatitude.length != 0)
    {
        [self initializeLocationManager];
    }
    else
    {
        [self showAlertTitle:@"" Message:@"No location found"];
    }
}

#pragma  mark --- Alert Controller Common Method

-(void)showAlertTitle:(NSString *)title Message:(NSString *)message
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self.navigationController popViewControllerAnimated:YES];
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [alertController addAction:okBtn];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)initializeLocationManager {
    // Check to ensure location services are enabled
    if(![CLLocationManager locationServicesEnabled]) {
        
        return;
    }
    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    [locationManager requestAlwaysAuthorization];
    locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
    
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.activityType = CLActivityTypeAutomotiveNavigation;
    
    [locationManager startUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    CLLocation *location = [locations lastObject];
    //AppDelegate *appDelegate = (AppDelegate *[)[[UIApplication sharedApplication] delegate];
    appDelegate.userCurrentLocation = location;
    
    if (location != nil)
    {
        stringLong =  [NSString stringWithFormat:@"%.8f", location.coordinate.longitude];//customlat
        stringLat  =  [NSString stringWithFormat:@"%.8f", location.coordinate.latitude];//customlat
        NSLog(@"lat == %@ long == %@",stringLat,stringLong);
        
        if(![stringLong isEqualToString:@""] && ![stringLong isEqualToString:@"0"])
        {
            [self loadLocationInMap];
        }
        
        
    }
    [manager stopUpdatingLocation];
}


-(void)loadLocationInMap
{
    
    float Long =  stringLong.floatValue;
    float Lat = stringLat.floatValue;
    
    float lat = [restModal.resturantLatitude floatValue];
    float longt = [restModal.resturantLongtitude floatValue];
    
    mapViewSelectLoc.delegate = self;
    CLLocationCoordinate2D destination= CLLocationCoordinate2DMake(lat,longt);
    MapAnnotation *newAnnotation1 = [[MapAnnotation alloc]initWithTitle:restModal.resturantName andCoordinate:destination andPlaceType:restModal.placeType andRating:restModal.placeRatings andPlaceID:@"" andAnnotatonCount:restModal.place_specials_count];
    
    CLLocationCoordinate2D userLocation= CLLocationCoordinate2DMake(Lat, Long);
    MapAnnotation *newAnnotation = [[MapAnnotation alloc]initWithTitle:@"UserLocation" andCoordinate:userLocation andPlaceType:@"1" andRating:@"0" andPlaceID:@"" andAnnotatonCount:@"0"];
    [mapViewSelectLoc addAnnotation:newAnnotation];
    [mapViewSelectLoc addAnnotation:newAnnotation1];
    [mapViewSelectLoc setCenterCoordinate:userLocation animated:YES];
    [self getPathDirections:userLocation withDestination:destination];
    annotationView.transform = CGAffineTransformMakeScale(1.0, 1.0);
    MKCoordinateRegion region;
    region.center=userLocation;
    region.span.latitudeDelta=0.14;
    region.span.longitudeDelta=0.14;
    [mapViewSelectLoc setRegion:region animated:YES];
    [mapViewSelectLoc regionThatFits:region];
}

- (MKAnnotationView *)mapView:(MKMapView *)mapViewLocal viewForAnnotation:(id <MKAnnotation>)annotation
{
    annotationView = (AnnotationView *)[mapViewSelectLoc dequeueReusableAnnotationViewWithIdentifier:@"MapDetails"];
    if (annotationView == nil)
    {
        annotationView = [[AnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"MapDetails"] ;
    }
    annotationView.annotation = annotation;
    return annotationView;
}

-(void)zoomToFitMapAnnotations
{
    if([mapViewSelectLoc.annotations count] == 0)
        return;
    
    CLLocationCoordinate2D topLeftCoord;
    topLeftCoord.latitude = -90;
    topLeftCoord.longitude = 180;
    
    CLLocationCoordinate2D bottomRightCoord;
    bottomRightCoord.latitude = 90;
    bottomRightCoord.longitude = -180;
    
    for(MKPointAnnotation* annotation in mapViewSelectLoc.annotations)
    {
        topLeftCoord.longitude = fmin(topLeftCoord.longitude, annotation.coordinate.longitude);
        topLeftCoord.latitude = fmax(topLeftCoord.latitude, annotation.coordinate.latitude);
        
        bottomRightCoord.longitude = fmax(bottomRightCoord.longitude, annotation.coordinate.longitude);
        bottomRightCoord.latitude = fmin(bottomRightCoord.latitude, annotation.coordinate.latitude);
    }
    
    MKCoordinateRegion region;
    region.center.latitude = topLeftCoord.latitude - (topLeftCoord.latitude - bottomRightCoord.latitude) * 0.5;
    region.center.longitude = topLeftCoord.longitude + (bottomRightCoord.longitude - topLeftCoord.longitude) * 0.5;
    region.span.latitudeDelta = fabs(topLeftCoord.latitude - bottomRightCoord.latitude) * 1.1; // Add a little extra space on the sides
    region.span.longitudeDelta = fabs(bottomRightCoord.longitude - topLeftCoord.longitude) * 1.1; // Add a little extra space on the sides
    
    region = [mapViewSelectLoc regionThatFits:region];
    [mapViewSelectLoc setRegion:region animated:YES];
}



-(void)getPathDirections:(CLLocationCoordinate2D)source withDestination:(CLLocationCoordinate2D)destination{
    
    
    NSString *strUrl;
    
    _startPoint = [NSString stringWithFormat:@"%f,%f",source.latitude,source.longitude];
    _endPoint = [NSString stringWithFormat:@"%f,%f",destination.latitude,destination.longitude];
    
    strUrl=[NSString stringWithFormat:@"%@origin=%@&destination=%@&sensor=true&alternatives=true",kBaseUrl,_startPoint,_endPoint];
    
    
    NSLog(@"%@",strUrl);
    strUrl=[strUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSData *data =[NSData dataWithContentsOfURL:[NSURL URLWithString:strUrl]];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [self fetchedData:data];
    });
    
    
}
#pragma mark - map overlay

-(MKOverlayRenderer *)mapView:(MKMapView *)mapView
           rendererForOverlay:(id<MKOverlay>)overlay {
    
    MKPolylineRenderer *renderer = [[MKPolylineRenderer alloc] initWithOverlay:overlay];
    renderer.lineWidth = 3;
    renderer.strokeColor = [UIColor purpleColor];
    renderer.fillColor = [[UIColor purpleColor] colorWithAlphaComponent:0.1f];
    
    return renderer;
}

- (void)fetchedData :(NSData *)responseData {
    NSError* error;
    
    NSDictionary* json = [NSJSONSerialization
                          JSONObjectWithData:responseData //1
                          
                          options:kNilOptions
                          error:&error];
    NSArray *arrRouts=[json objectForKey:@"routes"];
    if ([arrRouts isKindOfClass:[NSArray class]]&&arrRouts.count==0) {
        UIAlertView *alrt=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"didn't find direction" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
        [alrt show];
        return;
    }
    
    NSArray* arrpolyline = [[[json valueForKeyPath:@"routes.legs.steps.polyline.points"] objectAtIndex:0] objectAtIndex:0];
    NSMutableArray *polyLinesArray =[[NSMutableArray alloc]initWithCapacity:0];
    
    for (int i = 0; i < [arrpolyline count]; i++)
    {
        NSString* encodedPoints = [arrpolyline objectAtIndex:i] ;
        MKPolyline *route = [self polylineWithEncodedString:encodedPoints];
        [polyLinesArray addObject:route];
    }
    [mapViewSelectLoc addOverlays:polyLinesArray];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    
    
}

#pragma mark - decode map polyline

- (MKPolyline *)polylineWithEncodedString:(NSString *)encodedString {
    const char *bytes = [encodedString UTF8String];
    NSUInteger length = [encodedString lengthOfBytesUsingEncoding:NSUTF8StringEncoding];
    NSUInteger idx = 0;
    
    NSUInteger count = length / 4;
    CLLocationCoordinate2D *coords = calloc(count, sizeof(CLLocationCoordinate2D));
    NSUInteger coordIdx = 0;
    
    float latitude = 0;
    float longitude = 0;
    while (idx < length) {
        char byte = 0;
        int res = 0;
        char shift = 0;
        
        do {
            byte = bytes[idx++] - 63;
            res |= (byte & 0x1F) << shift;
            shift += 5;
        } while (byte >= 0x20);
        
        float deltaLat = ((res & 1) ? ~(res >> 1) : (res >> 1));
        latitude += deltaLat;
        
        shift = 0;
        res = 0;
        
        do {
            byte = bytes[idx++] - 0x3F;
            res |= (byte & 0x1F) << shift;
            shift += 5;
        } while (byte >= 0x20);
        
        float deltaLon = ((res & 1) ? ~(res >> 1) : (res >> 1));
        longitude += deltaLon;
        
        float finalLat = latitude * 1E-5;
        float finalLon = longitude * 1E-5;
        
        CLLocationCoordinate2D coord = CLLocationCoordinate2DMake(finalLat, finalLon);
        coords[coordIdx++] = coord;
        
        if (coordIdx == count) {
            NSUInteger newCount = count + 10;
            coords = realloc(coords, newCount * sizeof(CLLocationCoordinate2D));
            count = newCount;
        }
    }
    
    MKPolyline *polyline = [MKPolyline polylineWithCoordinates:coords count:coordIdx];
    free(coords);
    
    return polyline;
}



-(IBAction)pressBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
