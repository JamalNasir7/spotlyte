//
//  HotelDetailTableView.m
//  Spotlyte
//
//  Created by Anwaar Malik on 31/03/2018.
//  Copyright © 2018 Hemant. All rights reserved.
//

#import "HotelDetailTableView.h"

@implementation HotelDetailTableView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (CGSize)intrinsicContentSize {
    [self layoutIfNeeded]; // force my contentSize to be updated immediately
    return CGSizeMake(UIViewNoIntrinsicMetric, self.contentSize.height);
}

@end
