//
//  NearByViewController.h
//  Spotlyte
//
//  Created by Admin on 25/03/16.
//  Copyright © 2016 Naveen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NearByViewController : UIViewController<MKMapViewDelegate,CLLocationManagerDelegate>
{
 IBOutlet MKMapView *mapViewSelectLoc;
    AnnotationView *annotationView;
    CLLocationManager *locationManager;
    CLLocation *currentLocation;
    NSString *_startPoint;
    NSString *_endPoint;
    NSString *stringLat;
    NSString *stringLong;
}
@property (strong, nonatomic)ResturantModal *restModal;
@property (strong, nonatomic)PromtionsModal *promotionModal;

@end
