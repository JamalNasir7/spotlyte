//
//  PromotionsViewController.m
//  Spotlyte
//
//  Created by CIPL242-MOBILITY on 15/03/16.
//  Copyright © 2016 Naveen. All rights reserved.
//

#import "PromotionsViewController.h"
#import "DashBoardViewController.h"
#import "LiveFeedCell.h"
#import "DailySpecialViewController.h"
#import "SWRevealViewController.h"
#import "spotlytePromotionViewController.h"
#import "ProfileViewController.h"
#import "MyProfileViewController.h"
#import "HomeViewController.h"
#import "CommentsViewController.h"
#import "HomeViewController.h"
#import "NearByViewController.h"
#import "LiveFeedModal.h"
#import "PhotosCollectionViewCell.h"
#import "PromotionsCell.h"
#import <TwitterKit/TwitterKit.h>
#import <Fabric/Fabric.h>
#import "HappyHoursCell.h"
#import "HappyoursHeaderCell.h"
#import "HeaderView.h"
#import "WriteCommentController.h"
#import "EXPhotoViewer.h"

#import "FSBasicImage.h"
#import "FSBasicImageSource.h"
#import "FSImageViewerViewController.h"
#import "HeaderForDetail.h"
#import "IconDownloader.h"
#import "TOActionSheet.h"

#define _AUTO_SCROLL_ENABLED 0

NSString* OldRating;

@interface PromotionsViewController ()
{
    ResturantModal* responseModalForStarRating;
     int btnNo ;
    NSMutableArray *images;
    UIButton *btnGalleryClose;
    FSImageViewerViewController *imageViewController;
    NSMutableArray *ArrDays;
    
    NSMutableDictionary *rows;
    NSArray *sections;
    NSMutableArray *imagesArray;
    int scrollPositionY;
    HeaderView *header;
}



@property (weak, nonatomic) IBOutlet UIButton *MenuTapBtn_lbl;
- (IBAction)MenuTapBtn:(id)sender;
@end

@implementation PromotionsViewController
@synthesize restModalObj_Promtions;
@synthesize promtionsArray;
@synthesize hotelImageArray,hotelNameArray;
@synthesize arraySpecialforday,dictWeeks;
@synthesize btnClickHere;
@synthesize modalOBJ;
@synthesize lbl_TopView,placeIDString;
@synthesize hotelPromtionsArray,photosArray;
@synthesize selectedWhichController;
@synthesize checkTextOrSearch;
@synthesize strEventImage;

- (void)createSliderWithImagesWithAutoScroll {
    //    self.backgroundColor = [UIColor blackColor];
    //self.view.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
    
    
    sliderMainScroller.pagingEnabled = YES;
    sliderMainScroller.delegate = self;
    pageIndicator.numberOfPages = [imagesArray count];
    sliderMainScroller.contentSize = CGSizeMake(([UIScreen mainScreen].bounds.size.width * [imagesArray count] * 3), sliderMainScroller.frame.size.height);
    
    int mainCount = 0;
    for (int x = 0; x < 3; x++) {
        
        for (int i=0; i < [imagesArray count]; i++) {
            
            UIImageView *imageV = [[UIImageView alloc] init];
            CGRect frameRect;
            frameRect.origin.y = 0.0f;
            frameRect.size.width = [UIScreen mainScreen].bounds.size.width;
            frameRect.size.height = sliderMainScroller.frame.size.height;
            frameRect.origin.x = (frameRect.size.width * mainCount);
            imageV.frame = frameRect;
            imageV.layer.cornerRadius = 3.0;
            [imageV setBackgroundColor:[UIColor blackColor]];
            imageV.layer.masksToBounds = YES;
            imageV.contentMode = UIViewContentModeScaleAspectFill;
            [imageV sd_setImageWithURL:[NSURL URLWithString:[imagesArray objectAtIndex:i]]
              placeholderImage:[UIImage imageNamed:@"noimage.png"]];
            [sliderMainScroller addSubview:imageV];
            imageV.clipsToBounds = YES;
            imageV.userInteractionEnabled = YES;
            mainCount++;
        }
        
    }
    
    CGFloat startX = (CGFloat)[imagesArray count] * [UIScreen mainScreen].bounds.size.width;
    [sliderMainScroller setContentOffset:CGPointMake(startX, 0) animated:NO];
}

#pragma mark - UIScrollView delegate

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    if(scrollView == sliderMainScroller){
        CGFloat width = scrollView.frame.size.width;
        NSInteger page = (scrollView.contentOffset.x + (0.5f * width)) / width;
        
        NSInteger moveToPage = page;
        if (moveToPage == 0) {
            
            moveToPage = [imagesArray count];
            CGFloat startX = (CGFloat)moveToPage * [UIScreen mainScreen].bounds.size.width;
            [scrollView setContentOffset:CGPointMake(startX, 0) animated:NO];
            
        } else if (moveToPage == (([imagesArray count] * 3) - 1)) {
            
            moveToPage = [imagesArray count] - 1;
            CGFloat startX = (CGFloat)moveToPage * [UIScreen mainScreen].bounds.size.width;
            [scrollView setContentOffset:CGPointMake(startX, 0) animated:NO];
            
        }
        
        if (moveToPage < [imagesArray count]) {
            pageIndicator.currentPage = moveToPage;
        } else {
            
            moveToPage = moveToPage % [imagesArray count];
            pageIndicator.currentPage = moveToPage;
        }
    }
}
- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView {
    if(scrollView == sliderMainScroller){
        CGFloat width = scrollView.frame.size.width;
        NSInteger page = (scrollView.contentOffset.x + (0.5f * width)) / width;
        
        NSInteger moveToPage = page;
        if (moveToPage == 0) {
            
            moveToPage = [imagesArray count];
            CGFloat startX = (CGFloat)moveToPage * [UIScreen mainScreen].bounds.size.width;
            [scrollView setContentOffset:CGPointMake(startX, 0) animated:NO];
            
        } else if (moveToPage == (([imagesArray count] * 3) - 1)) {
            
            moveToPage = [imagesArray count] - 1;
            CGFloat startX = (CGFloat)moveToPage * [UIScreen mainScreen].bounds.size.width;
            [scrollView setContentOffset:CGPointMake(startX, 0) animated:NO];
            
        }
        
        if (moveToPage < [imagesArray count]) {
            pageIndicator.currentPage = moveToPage;
        } else {
            
            moveToPage = moveToPage % [imagesArray count];
            pageIndicator.currentPage = moveToPage;
        }
    }
}

#pragma mark end -

- (void)slideImageRight {
    
    CGFloat startX = 0.0f;
    CGFloat width = sliderMainScroller.frame.size.width;
    NSInteger page = (sliderMainScroller.contentOffset.x + (0.5f * width)) / width;
    NSInteger nextPage = page + 1;
    startX = (CGFloat)nextPage * width;
    //    [_sliderMainScroller scrollRectToVisible:CGRectMake(startX, 0, width, _sliderMainScroller.frame.size.height) animated:YES];
    [sliderMainScroller setContentOffset:CGPointMake(startX, 0) animated:YES];
}

#pragma mark end -

- (void)slideImageLeft {
    
    CGFloat startX = 0.0f;
    CGFloat width = sliderMainScroller.frame.size.width;
    NSInteger page = (sliderMainScroller.contentOffset.x + (0.5f * width)) / width;
    NSInteger nextPage = page - 1;
    startX = (CGFloat)nextPage * width;
    //    [_sliderMainScroller scrollRectToVisible:CGRectMake(startX, 0, width, _sliderMainScroller.frame.size.height) animated:YES];
    [sliderMainScroller setContentOffset:CGPointMake(startX, 0) animated:YES];
}

- (void)scrollViewDidScroll:(UIScrollView *)scroll_View {
    if(scroll_View == mainscrollView){
        
        if(scroll_View.contentOffset.y>scrollPositionY){
            
            [header setBackgroundColor:[UIColor whiteColor]];
             header.imgViewBack.hidden = YES;
             header.lblViewName.text = restModalOBJ.resturantName;
             [header.btnAddPhotos setTitleColor:[UIColor redColor] forState:(UIControlStateNormal)];
            [header.lblSaprator setBackgroundColor:[UIColor colorWithRed:206.0f/255.0f green:206.0f/255.0f blue:206.0f/255.0f alpha:0.8]];
        } else {
            [header setBackgroundColor:[UIColor clearColor]];
            header.imgViewBack.hidden = NO;
            header.lblViewName.text = @"";
            [header.btnAddPhotos setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
            [header.lblSaprator setBackgroundColor:[UIColor clearColor]];
        }
    }
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    innerScrollView.delegate = self;
    innerScrollView.bounces = false;
    scrollPositionY = 100;
   
    imagesArray = [[NSMutableArray alloc] init];
    
    
    //Sections
    ArrDays = [[NSMutableArray alloc]init];
    sections = [[NSArray alloc]init];
    rows = [[NSMutableDictionary alloc]init];
    //tableView_Promt.estimatedSectionHeaderHeight = 100;
    //tableView_Promt.sectionHeaderHeight = 50.0;
    
    

    [CUSTOMINDICATORMACRO startLoadAnimation:self];
    UICollectionViewFlowLayout *flow=[[UICollectionViewFlowLayout alloc]init];
    flow.sectionInset=UIEdgeInsetsMake(5, 5, 0, 2);
    
    btn_Promtions.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    btn_Promtions.titleLabel.numberOfLines = 2;
    
    restModalOBJ = [[ResturantModal alloc]init];
    NSArray *filteredArray = [promtionsArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"placeID ==%@",placeIDString]];
    if(filteredArray>0)
    {
        restModalOBJ = [filteredArray firstObject];
        OldRating=restModalOBJ.userRatings;
    }
    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *data = [defaults objectForKey:@"logindetails"];
    registerModalResponeValue = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    user_IDRegister = registerModalResponeValue.userID;
    
    isCheckedDailySpecials = FALSE;
    isCheckedHotel = FALSE;
    view_NoComments.hidden = YES;
    view_imageText.layer.cornerRadius = 10;
    view_imageText.layer.masksToBounds = YES;
    
    view_HotalDetail = [[[NSBundle mainBundle] loadNibNamed:@"HotelDetailViewController" owner:self options:nil] firstObject];
    view_LiveFeed = [[[NSBundle mainBundle] loadNibNamed:@"LiveFeed" owner:self options:nil] firstObject];
    
    view_Dailyspecials = [[[NSBundle mainBundle] loadNibNamed:@"DailySpecial" owner:self options:nil] firstObject];
    
    view_Promotions = [[[NSBundle mainBundle] loadNibNamed:@"Promotions" owner:self options:nil] firstObject];
    
    view_HappyHours = [[[NSBundle mainBundle] loadNibNamed:@"HappyHours" owner:self options:nil] firstObject];
    
    hotelNameArray = [[NSArray alloc]initWithObjects:@"Intourist Promo",@"Promenade Promo",@"Cardose Promo",@"The View Point Inn Promo",@"Royal Hotel Promo",@"Ostro Promo",@"BarCode Promo",@"Liquor Shop Promo",@"Bar One Promo",@"Safari Bar Promo", nil];
    
    hotelImageArray = [[NSArray alloc]initWithObjects:@"promo1.jpeg",@"promo2.jpeg",@"promo3.jpeg",@"promo4.jpeg",@"promo5.jpeg",@"promo6.jpeg",@"promo7.jpeg",@"promo8.jpeg",@"promo9.jpeg",@"promo10.jpeg", nil];
    
    
    tableView_Promotions.layer.cornerRadius = 5;
    tableView_Promotions.layer.masksToBounds = YES;
    tableView_Promotions.allowsSelectionDuringEditing=YES;
    [self initializeLocationManager];
    
    photosArray = [NSMutableArray new];
    [photos_CollectionView registerNib:[UINib nibWithNibName:@"PhotosCollectionViewCell" bundle:nil]
            forCellWithReuseIdentifier:@"cell123"];
    
    if ([restModalOBJ.yelp_url isEqualToString:@""]) {
        btnYelp.enabled=NO;
        btnYelp.alpha = 0.4;
        
    }
    
    if ([restModalOBJ.open_table isEqualToString:@""]) {
        btnOpenTable.enabled=NO;
        btnOpenTable.alpha = 0.4;
        
    }
    
    if ([restModalOBJ.grub_hub isEqualToString:@""]) {
        btnGrubHub.enabled=NO;
        btnGrubHub.alpha = 0.4;
        
    }
    
    if ([restModalOBJ.beer_menus isEqualToString:@""]) {
        btnBearMenus.enabled=NO;
        btnBearMenus.alpha = 0.4;
    }
    
    if ([restModalOBJ.strWebsite isEqualToString:@""]) {
        btnWebSite.enabled=NO;
        btnWebSite.alpha = 0.4;
    }
    
    if ([restModalOBJ.strWebsite isEqualToString:@""]) {
        btnWebSite.enabled=NO;
        btnWebSite.alpha = 0.4;
    }
    
    
    if(restModalOBJ.resturantPhoneNo.length>0)
    {
        NSString *phNo =  [NSString  stringWithFormat:@"%@",restModalOBJ.resturantPhoneNo];
        phNo = [phNo stringByReplacingOccurrencesOfString:@"(" withString:@""];
        phNo = [phNo stringByReplacingOccurrencesOfString:@")" withString:@""];
        phNo = [phNo stringByReplacingOccurrencesOfString:@"-" withString:@""];
        phNo = [phNo stringByReplacingOccurrencesOfString:@" " withString:@""];
        [lbl_phonenumber setText:phNo];
        if (phNo.length==0) {
            btnCalling.enabled=NO;
            btnCalling.alpha = 0.4;
        }
    }
    
    NSLog(@"PhoneNumber == %@",restModalOBJ.resturantPhoneNo);
   
    

        selectedView = 1;
        //self.viewTool.selectedSegmentIndex = 1;
        //[self colorChangeOnPressTab:@[btn_Details,btn_LiveFeed,btn_Promtions]];
        
        [view_HappyHours removeFromSuperview];
        [view_LiveFeed removeFromSuperview];
        [view_Promotions removeFromSuperview];
    
       view_HotalDetail.frame = CGRectMake(0, -20, self.view.frame.size.width, self.view.frame.size.height-50);
        [self.view addSubview:view_HotalDetail];
    
    
    
        [self displayStar];
        
        if(restModalOBJ.isCheckedFavourite == NO){
            btn_FavSelected.selected = NO;
            [btn_FavSelected setImage:[UIImage imageNamed:@"fav_unselected"] forState:UIControlStateNormal];
            [btn_FavSelected setTitleColor:[UIColor blackColor]forState:UIControlStateNormal];}
        else{
            btn_FavSelected.selected = YES;
            [btn_FavSelected setImage:[UIImage imageNamed:@"fav_selected"] forState:UIControlStateNormal];
            [btn_FavSelected setTitleColor:[UIColor blackColor] forState:UIControlStateNormal]; }

    
    NSString *tem = self.btnClickHere.titleLabel.text;
    if (tem != nil && ![tem isEqualToString:@""]) {
        NSMutableAttributedString *temString=[[NSMutableAttributedString alloc]initWithString:tem];
        [temString addAttribute:NSUnderlineStyleAttributeName
                          value:[NSNumber numberWithInt:1]
                          range:(NSRange){0,[temString length]}];
        
        self.btnClickHere.titleLabel.attributedText = temString;
        [self setDetailsValue];
    }
    
    _MenuTapBtn_lbl.hidden=YES;
    
    [[NSUserDefaults standardUserDefaults]setBool:TRUE forKey:@"ischeckedshowquickview"];
    registerModalResponeValue = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    user_IDRegister = registerModalResponeValue.userID;
    
    [self setImageOnCommentProfileImage];
    [self colorChangeOnPressTab:@[comments_Btn,photos_Btn]];
    
    [[NSUserDefaults standardUserDefaults]setBool:TRUE forKey:@"ischeckedshowquickview"];
    
    
    header = [[HeaderView alloc] initWithTitleAndWhiteBack:@"" controller:self];
    [header setBackgroundColor:[UIColor clearColor]];
    [header addSubview:header.btnback];
    header.BackButtonHandler = ^(BOOL success){
        
        if(isCheckedDailySpecials == TRUE)
        {
            view_Special.hidden = YES;
            view_NoSpecial.hidden = YES;
            
            isCheckedDailySpecials = FALSE;
        }
        else if(isCheckedPromotions == TRUE)
        {
            isCheckedPromotions = FALSE;
            isCheckedSelectedIndexPath = FALSE;
            [tableView_Promt reloadData];
        }
        else
        {
            NSString *returnString = ([checkTextOrSearch isEqualToString:@"customsearch"]) ? @"true" : @"false";
            [restModalOBJ setHomeVCStatus:returnString];
            if (_selectHomeVC) {
                _selectHomeVC(restModalOBJ);
            }
            [self.navigationController popViewControllerAnimated:YES];
        }
    };
    [self.view addSubview:header];
    
    
    
    self.viewTool.sectionTitles = @[@"Happy Hours",@"Daily Specials"];
    self.viewTool.selectedSegmentIndex = 0;
    self.viewTool.backgroundColor = [UIColor whiteColor];
    self.viewTool.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor colorWithRed:153.0/255.0 green:153.0/255.0 blue:153.0/255.0 alpha:1.0],NSFontAttributeName: [UIFont systemFontOfSize:14.0]};
    self.viewTool.selectedTitleTextAttributes = @{NSForegroundColorAttributeName : [UIColor colorWithRed:0.0/255.0 green:158.0/255.0 blue:224.0/255.0 alpha:1.0],NSFontAttributeName: [UIFont systemFontOfSize:14.0]};
    self.viewTool.selectionIndicatorColor = [UIColor colorWithRed:0.0/255.0 green:158.0/255.0 blue:224.0/255.0 alpha:1.0];
    self.viewTool.selectionStyle = HMSegmentedControlSelectionStyleFullWidthStripe;
    self.viewTool.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    self.viewTool.selectionIndicatorHeight = 3.0;
    
    [self.viewTool setIndexChangeBlock:^(NSInteger index) {
        if (index == 0){
            [UIView animateWithDuration:0.3
                                  delay:0.1
                                options: UIViewAnimationOptionCurveEaseInOut
                             animations:^
             {
                 isCheckedDailySpecials = false;
                 [innerScrollView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
                 
                 if(restModal_QuickView.arrayHappyHours.count>0)
                 {
                     arraySpecialforday = restModal_QuickView.arrayHappyHours;
                     [self happyHoursOrSpecial];
                 }
                 else{
                     view_Empty.frame = CGRectMake(0, 0, 320,100);
                     [innerScrollView addSubview:view_Empty];
                     view_Empty.hidden=NO;
                 }
             }
                             completion:^(BOOL finished)
             {
                 
             }];
        }
        else{
            [UIView animateWithDuration:0.3
                                  delay:0.1
                                options: UIViewAnimationOptionCurveEaseInOut
                             animations:^
             {
                 isCheckedDailySpecials = true;
                  [innerScrollView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
                 if(restModal_QuickView.arraySpecials.count > 0)
                 {
                     arraySpecialforday = restModal_QuickView.arraySpecials;
                     [self happyHoursOrSpecial];
                 }
                 else
                 {
                       view_Empty.hidden=NO;
                 }
                 
             }
                             completion:^(BOOL finished)
             {
                 
             }];
        }
        
    }];
    
    mainscrollView.delegate = self;
    mainscrollView.contentSize = CGSizeMake(self.view.frame.size.width, mainscrollView.frame.size.height);
    
    _btn_AddReview.layer.cornerRadius = 5; // this value vary as per your desire
    
    lblRating.layer.cornerRadius = 5;
    _lbl_secondRating.layer.cornerRadius = 5;
    _lbl_secondRating.clipsToBounds = YES;
    lblRating.clipsToBounds = YES;
    _btn_AddReview.clipsToBounds = YES;
    
    _btn_addPhotos.layer.cornerRadius = 5;
    _btn_addPhotos.clipsToBounds = YES;
}

-(void) happyHoursOrSpecial{
    NSString *SortDay;
    if (arraySpecialforday.count > 0) {
        NSMutableDictionary *extractedData = [NSMutableDictionary new];
        for (ResturantModal *dict in arraySpecialforday) {
            NSString *Days = dict.specials_weekDay;
            
            if ([Days isEqualToString:@"monday"]) {
                SortDay = [NSString stringWithFormat:@"1%@",Days];//@"0_monday";
            }
            else if ([Days isEqualToString:@"tuesday"]) {
                SortDay = [NSString stringWithFormat:@"2%@",Days];
                // SortDay = @"1_tuesday";
            }
            else if ([Days isEqualToString:@"wednesday"]) {
                SortDay = [NSString stringWithFormat:@"3%@",Days];
                // SortDay = @"2_wednesday";
            }
            else if ([Days isEqualToString:@"thursday"]) {
                //SortDay = @"3_thursday";
                SortDay = [NSString stringWithFormat:@"4%@",Days];
            }
            
            else if ([Days isEqualToString:@"friday"]) {
                //SortDay = @"4_friday";
                SortDay = [NSString stringWithFormat:@"5%@",Days];
            }
            
            else if ([Days isEqualToString:@"saturday"]) {
                // SortDay = @"5_saturday";
                SortDay = [NSString stringWithFormat:@"6%@",Days];
            }
            
            else if ([Days isEqualToString:@"sunday"]) {
                // SortDay = @"6_sunday";
                SortDay = [NSString stringWithFormat:@"7%@",Days];
            }
            NSString *firstLetter = [SortDay substringWithRange:NSMakeRange(0, 1)];
            NSMutableArray *rowArray = [extractedData objectForKey:firstLetter];//extractedData[firstLetter];
            NSLog(@"%@",extractedData);
            if (rowArray == nil){
                rowArray = [NSMutableArray new];
                [extractedData setObject:rowArray forKey:firstLetter];
                // extractedData[firstLetter] = rowArray;
            }
            [rowArray addObject:dict];
        }
        [ArrDays removeAllObjects];
        [ArrDays addObject:extractedData];
        
        [rows removeAllObjects];
        rows = [ArrDays objectAtIndex:0];
    }
    NSArray *keys = [rows allKeys];
    keys = [keys sortedArrayUsingComparator:^(id a, id b) {
        return [a compare:b options:NSNumericSearch];
    }];
    
    
    sections = keys;
    int yAxis = 0;
    int count = 0;
    for(int i=0;i<sections.count ;i++){
        [innerScrollView addSubview: header_view(0, yAxis,self.view.frame.size.width,i,sections[i])];
        yAxis = yAxis+35;
        count++;
        for(int j=0;j<[rows[sections[i]] count]; j++){
            [innerScrollView addSubview: cell_view(0, yAxis,self.view.frame.size.width,rows[sections[i]],j,i,self)];
            yAxis = yAxis+35;
            count++;
        }
    }
    innerScrollView.contentSize = CGSizeMake(self.view.frame.size.width, 35*count);
}

UIView * cell_view (NSInteger x, NSInteger y, NSInteger w, NSArray *RowArr, NSInteger indexpathRow,NSInteger indexpathSection,PromotionsViewController *self) {
    
    NSString *strPrice;
    NSString *strItemName;
    NSString *strTime;
    HappyHoursCell *cell;
    UIView *shelf = [[UIView alloc] initWithFrame:CGRectMake(x, y, w, 197)];
    cell = [[[NSBundle mainBundle]loadNibNamed:@"HappyHoursCell" owner:self options:nil]objectAtIndex:0];
        ResturantModal *modelDailySpecialDetails = RowArr[indexpathRow];
        
        NSString *trimmedString = [modelDailySpecialDetails.title stringByTrimmingCharactersInSet:
                                   [NSCharacterSet whitespaceCharacterSet]];
        if ([modelDailySpecialDetails.price isEqualToString:@"N/A"] || [modelDailySpecialDetails.price isEqualToString:@"1/2 Off"] || [modelDailySpecialDetails.price isEqualToString:@"Other"] || [modelDailySpecialDetails.price isEqualToString:@"FREE"])
        {
            strPrice = modelDailySpecialDetails.price;
        }
        else
        {
            if ([modelDailySpecialDetails.price rangeOfString:@"$"].location == NSNotFound) {
                strPrice = [NSString stringWithFormat:@"$%@",modelDailySpecialDetails.price];
            }else
            {
                strPrice = [NSString stringWithFormat:@"%@",modelDailySpecialDetails.price];
            }
            
        }
        
        strItemName = trimmedString.uppercaseString;
        
        if(modelDailySpecialDetails.isCheckedFavSpecial == NO)
        {
            cell.btnFavSpecial.selected = false;
            
        }
        else{
            cell.btnFavSpecial.selected = true;
        }
        [cell.btnFavSpecial addTarget:self action:@selector(btnFavSpecialClickNew:) forControlEvents:UIControlEventTouchUpInside];
        cell.btnFavSpecial.titleLabel.tag = indexpathSection;
        cell.btnFavSpecial.tag = indexpathRow;
        
        NSString *stringStartTime;
        
        NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
        NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
        [dateFormatter1 setLocale:locale];
        
        dateFormatter1.dateFormat = @"HH:mm:ss";
        
        NSDate *date1 = [dateFormatter1 dateFromString:modelDailySpecialDetails.startTime];
        
        dateFormatter1.dateFormat = @"hh:mm a";
        
        stringStartTime = [dateFormatter1 stringFromDate:date1];
        
        stringStartTime = [stringStartTime stringByReplacingOccurrencesOfString:@":00" withString:@""];
        
        
        int stringStartTimeInt=[stringStartTime intValue];
        
        if (stringStartTimeInt<10)
        {
            NSRange range = NSMakeRange(0,1);
            stringStartTime = [stringStartTime stringByReplacingCharactersInRange:range withString:@""];
        }
        else
        {
            stringStartTime = [stringStartTime stringByReplacingOccurrencesOfString:@":00" withString:@""];
        }
        
        
        NSString *stringCloseTime;
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        NSLocale *locale2 = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
        [dateFormatter setLocale:locale2];
        
        dateFormatter.dateFormat = @"HH:mm:ss";
        
        NSDate *date = [dateFormatter dateFromString:modelDailySpecialDetails.endTime];
        
        
        
        dateFormatter.dateFormat = @"hh:mm a";
        
        stringCloseTime = [dateFormatter stringFromDate:date];
        
        stringCloseTime = [stringCloseTime stringByReplacingOccurrencesOfString:@":00" withString:@""];
        
        
        
        int stringCloseTimeInt=[stringCloseTime intValue];
        
        if (stringCloseTimeInt<10)
        {
            
            
            NSRange range = NSMakeRange(0,1);
            
            stringCloseTime = [stringCloseTime stringByReplacingCharactersInRange:range withString:@""];
            
            
        }
        else
        {
            stringCloseTime = [stringCloseTime stringByReplacingOccurrencesOfString:@":00" withString:@""];
        }
        strTime = [NSString stringWithFormat:@"(%@ - %@)",stringStartTime,stringCloseTime];
        NSString *AllItems = [NSString stringWithFormat:@"%@ %@ %@",strPrice,strItemName,strTime];
        cell.lblAllItems.text = AllItems;
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.frame = CGRectMake(cell.frame.origin.x, cell.frame.origin.y, w, cell.frame.size.height);
        [shelf addSubview:cell];
    return shelf;
}

UIView * header_view (NSInteger x, NSInteger y, NSInteger w ,NSInteger section, NSString *sectionStr) {
    
    HeaderForDetail *cell;
    UIView *shelf = [[UIView alloc] initWithFrame:CGRectMake(x, y, w, 197)];
    
    if(cell == nil)
    {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"HeaderForDetail" owner:shelf options:nil]objectAtIndex:0];
    }
    if (section == 0){
        cell.VwSep.hidden = YES;
    }
    else{
        cell.VwSep.hidden = NO;
    }
    NSString *mystr;
    
    if([sectionStr isEqualToString:@"1"])
    {
        mystr= @"Monday"; //[modelDailySpecialDetails.specials_weekDay substringToIndex:1];
    }
    else if ([sectionStr isEqualToString:@"2"])
    {
        mystr= @"Tuesday"; //[modelDailySpecialDetails.specials_weekDay substringToIndex:2];
    }
    else if ([sectionStr  isEqualToString:@"3"])
    {
        mystr= @"Wednesday"; //[modelDailySpecialDetails.specials_weekDay substringToIndex:1];
    }
    else if ([sectionStr isEqualToString:@"4"])
    {
        mystr= @"Thursday"; //[modelDailySpecialDetails.specials_weekDay substringToIndex:2];
    }
    else if ([sectionStr  isEqualToString:@"5"])
    {
        mystr= @"Friday"; //[modelDailySpecialDetails.specials_weekDay substringToIndex:1];
    }
    else if ([sectionStr  isEqualToString:@"6"])
    {
        mystr= @"Saturday"; //[modelDailySpecialDetails.specials_weekDay substringToIndex:2];
    }
    else if ([sectionStr  isEqualToString:@"7"])
    {
        mystr= @"Sunday"; //[modelDailySpecialDetails.specials_weekDay substringToIndex:2];
    }
    cell.lblHeaderTitle.text = mystr;
    cell.frame = CGRectMake(cell.frame.origin.x, cell.frame.origin.y, w, cell.frame.size.height);
    [shelf addSubview:cell];
    return shelf;
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

//    tableView_LiveFeed.estimatedRowHeight = 80;
//    tableView_LiveFeed.rowHeight = UITableViewAutomaticDimension;

}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self tapOnComments:YES];

}


- (IBAction)tapOnCommentButton:(id)sender {
    WriteCommentController *writeVC = [[WriteCommentController alloc]init];
    writeVC.lblName.text=registerModalResponeValue.userName;
    writeVC.registerModalResponeValue1=registerModalResponeValue;
    writeVC.restModalOBJ1=restModalOBJ;
    writeVC.img=profileImg.image;
    [writeVC recievedData:^(BOOL success){
        if (success) {
           // [self commentsServiceHelper];
        }
    }];
    [self.navigationController pushViewController:writeVC animated:YES];
}

-(void)setImageOnCommentProfileImage{

    dispatch_async(dispatch_get_main_queue(), ^{
        NSURL *url = [[NSBundle mainBundle] URLForResource:@"loading-1" withExtension:@"gif"];
        NSString *newUrl = [registerModalResponeValue.profileImage stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
        [profileImg sd_setImageWithURL:[NSURL URLWithString:newUrl]
                             placeholderImage:[UIImage imageNamed:@"noimage.png"]];
        
    });


}

- (CGFloat)widthOfString:(NSString *)string withFont:(UIFont *)font {
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:font, NSFontAttributeName, nil];
    return [[[NSAttributedString alloc] initWithString:string attributes:attributes] size].width;
}


#pragma  mark --- Core Location
- (void)initializeLocationManager {
    if(![CLLocationManager locationServicesEnabled]) {
        
        return;
    }
    
    _locationManager = [[CLLocationManager alloc] init];
    _locationManager.delegate = self;
    [_locationManager requestAlwaysAuthorization];
    _locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
    _locationManager.distanceFilter = kCLDistanceFilterNone;
    _locationManager.activityType = CLActivityTypeAutomotiveNavigation;
    [_locationManager startUpdatingLocation];
}


- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    CLLocation *locA = [[CLLocation alloc] initWithLatitude:newLocation.coordinate.latitude longitude:newLocation.coordinate.longitude];//customlat
    CLLocation *locB = [[CLLocation alloc] initWithLatitude:restModalOBJ.resturantLatitude.floatValue longitude:restModalOBJ.resturantLongtitude.floatValue];
    
    CLLocationDistance distance = [locA distanceFromLocation:locB];
    
    NSString *distanceString = [[NSString alloc] initWithFormat: @"%f", distance];
    
    totalDistance = [distanceString floatValue];
    NSLog(@"Distance=== %f",totalDistance);
    
    
    
    if(totalDistance<=150)//5000--5km//100000--100Km
    {
        isCheckedHotel = TRUE;
    }else{
        isCheckedHotel = FALSE;
    }
    
    
    /*if(totalDistance<=150)//5000--5km//100000--100Km
    {
        isCheckedHotel = TRUE;
        [comment_LiveFeed setUserInteractionEnabled:YES];
        [btn_comment setHidden:NO];
    }
    else
    {
         isCheckedHotel = FALSE;
         [btn_comment setHidden:YES];
         [comment_LiveFeed setUserInteractionEnabled:NO];
    }*/
    [_locationManager stopUpdatingLocation];
}

#pragma mark -- FaceBook

-(void)faceBookMethod
{
    FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
    content.contentURL = [NSURL
                          URLWithString:@"https://www.facebook.com/FacebookDevelopers"];
    FBSDKShareButton *shareButton = [[FBSDKShareButton alloc] init];
    shareButton.shareContent = content;
    shareButton.center = self.view.center;
    [self.view addSubview:shareButton];
}

#pragma mark -- Service Helper

#pragma mark -- Happy Hours and Daily Specials Service Helper

-(void)specialsHappyHoursServiceHelper
{
    BOOL check=[[AppDelegate shareAppdelegateInstance] checkReachability];
    if(check)
    {
        
        [CUSTOMINDICATORMACRO startLoadAnimation:self];
        [restModalOBJ setUserID:user_IDRegister];
        [RestaurantMacro getSpecialsHappyHoursToServer:restModalOBJ withCompletion:^(id obj1) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
              
                if([obj1 isKindOfClass:[ResturantModal class]])
                {
                    restModal_QuickView  = obj1;
                    [CUSTOMINDICATORMACRO stopLoadAnimation:self];
                    if(restModal_QuickView.arrayHappyHours.count>0)
                    {
                        arraySpecialforday = restModal_QuickView.arrayHappyHours;
                        [self happyHoursOrSpecial];
                    }
                    else
                    {
                        view_Empty.hidden=NO;
                    }
                }
                else
                {
                    [CUSTOMINDICATORMACRO stopLoadAnimation:self];
                    view_Empty.hidden=NO;
                    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Please check your Internet Connection." preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction *okBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        [self dismissViewControllerAnimated:YES completion:nil];
                    }];
                    [alertController addAction:okBtn];
                    [self presentViewController:alertController animated:YES completion:nil];
                }
                
            });
        }];
    }
    
}


#pragma  mark - Daily Specials For Each Resturant

-(void)dailySpecialServiceHelper
{
    
    BOOL check=[[AppDelegate shareAppdelegateInstance] checkReachability];
    if(check)
    {
        //[CUSTOMINDICATORMACRO startLoadAnimation:self];
        [RestaurantMacro listAllDailySpecialsFromServer:restModalOBJ withCompletion:^(id obj1) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [CUSTOMINDICATORMACRO stopLoadAnimation:self];
                if([obj1 isKindOfClass:[NSArray class]] || [obj1 isKindOfClass:[NSMutableArray class]])
                {
                    NSArray *response  = obj1;
                    if(response.count>0)
                    {
                        view_NoSpecial.hidden = YES;
                        view_Special.hidden = NO;
                        arraySpecialforday = obj1;
                        [tableView_DailySpecial reloadData];
                    }
                    else{
                        view_NoSpecial.hidden = NO;
                        view_Special.hidden = YES;
                    }
                    
                }
                else
                {
                    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Please check your Internet Connection." preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction *okBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        [self dismissViewControllerAnimated:YES completion:nil];
                    }];
                    [alertController addAction:okBtn];
                    [self presentViewController:alertController animated:YES completion:nil];
                }
                
            });
        }];
    }
    
}


-(void)getEmonjiServiceHelper :(ResturantModal *)modal{
    
    
    BOOL check=[[AppDelegate shareAppdelegateInstance] checkReachability];
    if(check)
    {
        //[CUSTOMINDICATORMACRO startLoadAnimation:self];
        [RestaurantMacro getEmonjiTimeToServer:modal withCompletion:^(id obj1) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [CUSTOMINDICATORMACRO stopLoadAnimation:self];
                ResturantModal *responseModal = nil;
                
                if([obj1 isKindOfClass:[ResturantModal class]])
                {
                    responseModal = obj1;
                    
                    if([responseModal.result isEqualToString:@"success"])
                    {
                        if([responseModal.emonjiTime isEqualToString:@""] || [responseModal.emonjiTime isEqualToString:@""])
                        {
                            lbl_EmoniTime.text = @"Just now";
                            
                        }
                        else
                        {
                            lbl_EmoniTime.text = responseModal.emonjiTime;
                            
                        }
                        NSLog(@"%@======",responseModal.emonjiTime);
                        lblTime_X.constant= emonji_X;
                    }
                }
                else
                {
                    [self showAlertTitle:@"" Message:@"Please check your Internet Connection."];
                }
            });
        }];
    }
}


-(void)setEmonjiServiceHelper :(ResturantModal *)modal{
    
    
    BOOL check=[[AppDelegate shareAppdelegateInstance] checkReachability];
    if(check)
    {
        //[CUSTOMINDICATORMACRO startLoadAnimation:self];
        [RestaurantMacro setEmonjiToServer:modal withCompletion:^(id obj1) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [CUSTOMINDICATORMACRO stopLoadAnimation:self];
                ResturantModal *responseModal = nil;
                
                if([obj1 isKindOfClass:[ResturantModal class]])
                {
                    responseModal = obj1;
                    
                    if([responseModal.result isEqualToString:@"success"])
                    {
                        [self getEmonjiServiceHelper:modal];
                        [self setEmonjis:modal.emonjiID.intValue type:1];
                        [self setEmonjiTimeLabel:modal.emonjiID.intValue];
                    }
                }
                else
                {
                    [self showAlertTitle:@"" Message:@"Please check your Internet Connection."];
                }
                
            });
        }];
    }
}

-(void)setCheckInServiceHelper{
    ResturantModal *modal = [[ResturantModal alloc]init];
    
    [modal setPlaceID:restModalOBJ.placeID];
    [modal setUserID:registerModalResponeValue.userID];
    [modal setCheckInStr:@"1"];
    
    
    BOOL check=[[AppDelegate shareAppdelegateInstance] checkReachability];
    if(check)
    {
////[CUSTOMINDICATORMACRO startLoadAnimation:self];
        [RestaurantMacro checkInToServer:modal withCompletion:^(id obj1) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [CUSTOMINDICATORMACRO stopLoadAnimation:self];
                ResturantModal *responseModal = nil;
                
                if([obj1 isKindOfClass:[ResturantModal class]])
                {
                    responseModal = obj1;
                    
                    if([responseModal.result isEqualToString:@"success"])
                    {
                        [btn_CheckIn setSelected: YES];
                        restModalOBJ.isCheckedCheckIn=YES;
                        
                        ResturantModal *modal = [[ResturantModal alloc]init];
                        
                        [modal setCommentsImage:@""];
                        [modal setComments:@"You CHECKED IN at"];
                        [modal setPlaceID:restModalOBJ.placeID];
                        [modal setIs_Emoji:@"1"];
                        [modal setUserID:registerModalResponeValue.userID];
                        [modal setSystemMessage:@"0"];
                        [modal setCmdNumber:@"0"];
                        [modal setComment_for_checkin:@"1"];
                        [modal setUserGivenRating:@"0"];
                        
                        [self createCommentServiceHelper:modal];
                        
                    }
                }
                else
                {
                    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Please check your Internet Connection." preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction *okBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        [self dismissViewControllerAnimated:YES completion:nil];
                    }];
                    [alertController addAction:okBtn];
                    [self presentViewController:alertController animated:YES completion:nil];
                }
                
            });
        }];
    }
}

-(void)createImageServiceHelper :(ResturantModal *)restModalOBJ1
{
    BOOL check=[[AppDelegate shareAppdelegateInstance] checkReachability];
    if(check)
    {
        [CUSTOMINDICATORMACRO startLoadAnimation:self];
        [RestaurantMacro postImagesToServer:restModalOBJ1 withCompletion:^(id obj1) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                ResturantModal *responseModal = nil;
                
                if([obj1 isKindOfClass:[ResturantModal class]])
                {
                    responseModal = obj1;
                    
                    [comment_LiveFeed resignFirstResponder];
                    
                    if([responseModal.result isEqualToString:@"success"])
                    {
                       [self commentsImageServiceHelper];
                      //  [self commentsServiceHelper];
                        
                    }
                    comment_LiveFeed.text = @"";
                }
                else
                {
                    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Please check your Internet Connection." preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction *okBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        [self dismissViewControllerAnimated:YES completion:nil];
                    }];
                    [alertController addAction:okBtn];
                    [self presentViewController:alertController animated:YES completion:nil];
                }
                
            });
        }];
    }
}
-(void)createCommentServiceHelper :(ResturantModal *)restModalOBJ1
{
    BOOL check=[[AppDelegate shareAppdelegateInstance] checkReachability];
    if(check)
    {
        [CUSTOMINDICATORMACRO startLoadAnimation:self];
        [RestaurantMacro postCommentsToServer:restModalOBJ1 withCompletion:^(id obj1) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [CUSTOMINDICATORMACRO stopLoadAnimation:self];
                ResturantModal *responseModal = nil;
                
                if([obj1 isKindOfClass:[ResturantModal class]])
                {
                    responseModal = obj1;
                    
                    [comment_LiveFeed resignFirstResponder];
                    
                    if([responseModal.result isEqualToString:@"success"])
                    {
                        [self commentsServiceHelper];
                    }
                    comment_LiveFeed.text = @"";
                }
                else
                {
                    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Please check your Internet Connection." preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction *okBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        [self dismissViewControllerAnimated:YES completion:nil];
                    }];
                    [alertController addAction:okBtn];
                    [self presentViewController:alertController animated:YES completion:nil];
                }
                
            });
        }];
    }
}
-(void)commentsImageServiceHelper
{
    
    BOOL check=[[AppDelegate shareAppdelegateInstance] checkReachability];
    if(check)
    {
        [CUSTOMINDICATORMACRO startLoadAnimation:self];
        
        [imagesArray removeAllObjects];
        dispatch_async(dispatch_get_main_queue(), ^{
            NSString *newUrl;
            
            if ([restModalOBJ.DetailViewPlaceImage isEqualToString:@""]){
                newUrl = [restModalOBJ.placeImage stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
            }
            else{
                newUrl = [restModalOBJ.DetailViewPlaceImage stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
            }
            [imagesArray addObject:(newUrl)];
        });
       
        
        [RestaurantMacro listAllHotelcommentsImageToServer:restModalOBJ withCompletion:^(id obj1) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if([obj1 isKindOfClass:[NSArray class]] || [obj1 isKindOfClass:[NSMutableArray class]])
                {
                    
                    photosArray = obj1;
                    if(photosArray.count>0)
                    {
                        if (images.count>0) {
                            [images removeAllObjects];
                        }
                        view_NoComments.hidden = YES;
                        photos_CollectionView.hidden  =NO;
                        photosView.hidden = NO;
                        view_Rating.hidden = YES;
                        lblBelowRating.hidden=YES;
                       // view_TextView.hidden = YES;
                        [photos_CollectionView reloadData];
                    }
                    else
                    {
                        view_NoComments.hidden = NO;
                        tableView_LiveFeed.hidden = YES;
                        lbl_NoComments.text = @"No Photos Available At the Moment";
                        photos_CollectionView.hidden  =YES;
                        view_Rating.hidden = YES;
                        lblBelowRating.hidden=YES;

                      //  view_TextView.hidden = YES;
                        
                    }
                    
                     [CUSTOMINDICATORMACRO stopLoadAnimation:self];
                    
                    
                        for (ResturantModal *model in photosArray) {
                           NSString *newUrl = [model.commentsImage stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
                            [imagesArray addObject:newUrl];
                        }
                     [self createSliderWithImagesWithAutoScroll];
                     [self specialsHappyHoursServiceHelper];
                }
                else
                {
                    [CUSTOMINDICATORMACRO stopLoadAnimation:self];
                    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Please check your Internet Connection." preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction *okBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        [self dismissViewControllerAnimated:YES completion:nil];
                    }];
                    [alertController addAction:okBtn];
                    [self presentViewController:alertController animated:YES completion:nil];
                }
            });
        }];
    }
}



-(void)hotelPromotionsServiceHelper
{
    hotelPromtionsArray = [NSMutableArray new];
    
    BOOL check=[[AppDelegate shareAppdelegateInstance] checkReachability];
    if(check)
    {
        ////[CUSTOMINDICATORMACRO startLoadAnimation:self];
        [restModalOBJ setPlaceID:restModalOBJ.placeID];
        [restModalOBJ setUserID:user_IDRegister];
        
        [PromotionMacro hotelPromotionsListValuesToServer:restModalOBJ withCompletion:^(id obj1) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [CUSTOMINDICATORMACRO stopLoadAnimation:self];
                [self.view addSubview:view_Promotions];
                if([obj1 isKindOfClass:[NSArray class]] || [obj1 isKindOfClass:[NSMutableArray class]])
                {
                    NSArray *response = obj1;
                    if(response.count>0)
                    {
                        view_NoPromtions.hidden = YES;
                        tableView_Promt.hidden = NO;
                        hotelPromtionsArray = obj1;
                        [tableView_Promt reloadData];
                    }
                    else
                    {
                        view_NoPromtions.hidden = NO;
                        tableView_Promt.hidden = YES;
                    }
                }
                else
                {
                    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Please check your Internet Connection." preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction *okBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        [self dismissViewControllerAnimated:YES completion:nil];
                    }];
                    [alertController addAction:okBtn];
                    [self presentViewController:alertController animated:YES completion:nil];
                }
                
            });
        }];
    }
}

-(void)commentsServiceHelper
{
    liveFeedArray = [NSMutableArray new];
    [restModalOBJ setUserID:registerModalResponeValue.userID];
    [restModalOBJ setPlaceID:placeIDString];
    BOOL check=[[AppDelegate shareAppdelegateInstance] checkReachability];
    tableView_LiveFeed.estimatedRowHeight = 20;
    tableView_LiveFeed.rowHeight = UITableViewAutomaticDimension;

    if(check)
    {
        ////[CUSTOMINDICATORMACRO startLoadAnimation:self];
       /*  UIActivityIndicatorView *activityIndicator;
        if (!activityIndicator) {
            activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
            activityIndicator.frame = CGRectMake(tableView_LiveFeed.bounds.size.width/2-15, tableView_LiveFeed.bounds.size.height/2-15, 30, 30);
        }
        [activityIndicator startAnimating];
        [tableView_LiveFeed addSubview:activityIndicator];*/
        
        [RestaurantMacro listAllCommentsValuesToServer:restModalOBJ withCompletion:^(id obj1) {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if([obj1 isKindOfClass:[NSArray class]] || [obj1 isKindOfClass:[NSMutableArray class]])
                {
                    NSArray *response = obj1;
                    if(response.count>0)
                    {
                        
                        restModalOBJ.totalReview=[NSString stringWithFormat:@"%lu",(unsigned long)response.count];
                        
                        [_btn_readAllreviews setTitle:[restModalOBJ.totalReview isEqualToString:@"1"]?@"Read all reviews (1)":[restModalOBJ.totalReview isEqualToString:@""]?@"No reviews yet.":[NSString stringWithFormat:@"Read all reviews (%@) ",restModalOBJ.totalReview] forState:UIControlStateNormal];
                        lbl_Review.text =[restModalOBJ.totalReview isEqualToString:@"1"]?@"Based on 1 review":[restModalOBJ.totalReview isEqualToString:@""]?@"Based on 0 reviews":[NSString stringWithFormat:@"Based on %@ reviews",restModalOBJ.totalReview];
                        
                        
                        liveFeedArray = obj1;
                        view_NoComments.hidden = YES;
                        tableView_LiveFeed.hidden = NO;
                        [tableView_LiveFeed reloadData];
                        [CUSTOMINDICATORMACRO stopLoadAnimation:self];
                    }
                    else
                    {
                        view_NoComments.hidden = NO;
                        lbl_NoComments.text = @"No Comments Available At the Moment";
                        tableView_LiveFeed.hidden  =YES;
                        [CUSTOMINDICATORMACRO stopLoadAnimation:self];
                    }
                    
                   /* [activityIndicator stopAnimating];
                    [activityIndicator removeFromSuperview];*/
                }
                else
                {
                    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Please check your Internet Connection." preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction *okBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        [self dismissViewControllerAnimated:YES completion:nil];
                    }];
                    [alertController addAction:okBtn];
                    [self presentViewController:alertController animated:YES completion:nil];
                    [CUSTOMINDICATORMACRO stopLoadAnimation:self];

                  /*  [activityIndicator stopAnimating];
                    [activityIndicator removeFromSuperview];*/

                }
                
            });
        }];
    }
    
}
- (void)userGivenRating :(NSString *)starValue
{
    
    if(starValue.floatValue <= 2.0)
    {
        starRating_userGiven.starHighlightedImage = [UIImage imageNamed:@"star-1_Rcopy.png"];
    }
    else if(starValue.floatValue <= 3.9)
    {
        starRating_userGiven.starHighlightedImage = [UIImage imageNamed:@"star-1_Ycopy.png"];
    }
    else
    {
        starRating_userGiven.starHighlightedImage = [UIImage imageNamed:@"star-1_Gcopy.png"];
    }
    starRating_userGiven.starImage = [UIImage imageNamed:@"unstartempcopy.png"];
    starRating_userGiven.maxRating = 5.0;
    starRating_userGiven.delegate = self;
    starRating_userGiven.horizontalMargin = 4;
    starRating_userGiven.editable = YES;
    starRating_userGiven.rating = starValue.floatValue;
    starRating_userGiven.displayMode = EDStarRatingDisplayAccurate;
}

#pragma mark --- Star Rating Delegate Method

-(void)starsSelectionChanged:(EDStarRating*)control rating:(float)rating
{
    [restModalOBJ setUserID:registerModalResponeValue.userID];
    [restModalOBJ setUserRatings:[NSString stringWithFormat:@"%f",rating]];
    
    BOOL check=[[AppDelegate shareAppdelegateInstance] checkReachability];
    if(check)
    {
        ////[CUSTOMINDICATORMACRO startLoadAnimation:self];
        [RestaurantMacro ratingGivenToServer:restModalOBJ withCompletion:^(id obj1) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [CUSTOMINDICATORMACRO stopLoadAnimation:self];
                ResturantModal *responseModal = nil;
                
                if([obj1 isKindOfClass:[ResturantModal class]])
                {
                    responseModal = obj1;
                    
                    if([responseModal.result isEqualToString:@"success"])
                    {
                        responseModalForStarRating = obj1;
                        restModalOBJ.placeRatings=[NSString stringWithFormat:@"%.1f",[responseModal.placeRatings floatValue] ];
                        restModalOBJ.userRatings=[NSString stringWithFormat:@"%.1f",[responseModal.userRatings floatValue] ];
                        OldRating=responseModal.placeRatings;
                        
                        [self userGivenRating:[NSString stringWithFormat:@"%f",rating]];
                        [self liveFeedstarRating:responseModal.placeRatings];
                        if ([responseModal.placeRatings isEqualToString:@"0"])
                        {
                            lblRating.text = @"N/A";
                        }
                        else
                        {
                            lblRating.text = [NSString stringWithFormat:@"%@",restModalOBJ.placeRatings];
                        }
                        //SpotLyte Rating /
                    }
                    else
                    {
                        [self userGivenRating:OldRating];
                        [self showAlertTitle:@"Rating" Message:responseModal.message];
                    }
                }
                else
                {
                    [self showAlertTitle:@"" Message:@"Please check your Internet Connection."];
                }
            });
        }];
        
        
    }
    else
    {
        [self userGivenRating:restModalOBJ.userRatings];
        [self showAlertTitle:@"" Message:@"SpotLyte Encourages Real Time Reviews - In order to Check In you must be located near this location."];
        
    }
    
    
}

- (void)displayStar
{
    if (responseModalForStarRating !=nil)
    {
        
        if(responseModalForStarRating.placeRatings.floatValue <= 2.0)
        {
            starRatingView.starHighlightedImage = [UIImage imageNamed:@"star-1_R.png"];
        }
        else if(responseModalForStarRating.placeRatings.floatValue <= 3.9)
        {
            starRatingView.starHighlightedImage = [UIImage imageNamed:@"star-1_Y.png"];
        }
        else
        {
            starRatingView.starHighlightedImage = [UIImage imageNamed:@"star-1_G.png"];
        }
        starRatingView.starImage = [UIImage imageNamed:@"unstartemp.png"];
        starRatingView.maxRating = 5.0;
        starRatingView.delegate = self;
        starRatingView.horizontalMargin = 12;
        starRatingView.editable = YES;
        starRatingView.rating = responseModalForStarRating.placeRatings.floatValue;
        starRatingView.displayMode = EDStarRatingDisplayAccurate;
        
       // lblRating_Details.text = responseModalForStarRating.placeRatings;

        
        
    }else
    {
        if(restModalOBJ.placeRatings.floatValue <= 2.0)
        {
            starRatingView.starHighlightedImage = [UIImage imageNamed:@"star-1_R.png"];
        }
        else if(restModalOBJ.placeRatings.floatValue <= 3.9)
        {
            starRatingView.starHighlightedImage = [UIImage imageNamed:@"star-1_Y.png"];
        }
        else
        {
            starRatingView.starHighlightedImage = [UIImage imageNamed:@"star-1_G.png"];
        }
        starRatingView.starImage = [UIImage imageNamed:@"unstartemp.png"];
        starRatingView.maxRating = 5.0;
        starRatingView.delegate = self;
        starRatingView.horizontalMargin = 12;
        starRatingView.editable = YES;
        starRatingView.rating = restModalOBJ.placeRatings.floatValue;
        starRatingView.displayMode = EDStarRatingDisplayAccurate;
     
        //lblRating_Details.text = restModalOBJ.placeRatings;
    }
}


-(void)liveFeedstarRating :(NSString *)ratingVal
{
    
    if(ratingVal.floatValue <= 2.0)
    {
        starRating_LiveFeed.starHighlightedImage = [UIImage imageNamed:@"star-1_R.png"];
    }
    else if(ratingVal.floatValue <= 3.9)
    {
        starRating_LiveFeed.starHighlightedImage = [UIImage imageNamed:@"star-1_Y.png"];
    }
    else
    {
        starRating_LiveFeed.starHighlightedImage = [UIImage imageNamed:@"star-1_G.png"];
    }
    starRating_LiveFeed.starImage = [UIImage imageNamed:@"unstartemp.png"];
    starRating_LiveFeed.maxRating = 5.0;
    starRating_LiveFeed.delegate = self;
    starRating_LiveFeed.horizontalMargin = 12;
    starRating_LiveFeed.editable = YES;
    starRating_LiveFeed.rating = ratingVal.floatValue;
    starRating_LiveFeed.displayMode = EDStarRatingDisplayAccurate;
}

-(void)setDetailsValue
{
    NSString *openingHours = [NSString stringWithFormat:@"%@",restModalOBJ.openFrom];
    lbHours_Details.text = openingHours;
    NSString * detailedAddress = [NSString stringWithFormat:@"%@\n%@, %@ %@",restModalOBJ.resturantAddress,restModalOBJ.resturantCity,restModalOBJ.resturantState,restModalOBJ.resturantPostalCode];
    
    lblAddress_Details.text = detailedAddress;
    
 
    if (restModalOBJ.resturantDescription.length>0) {
        lblDesc_Details.text = restModalOBJ.resturantDescription;
        lblNoDescription.hidden=YES;}
    else{
        lblNoDescription.hidden=NO;
        lblDesc_Details.hidden=YES;
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [lblDesc_Details setContentOffset:CGPointZero animated:YES];
    });
    
    lblEmail_Details.text = restModalOBJ.strWebsite;
    
    NSDictionary *underlineAttribute = @{NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)};
    lblPhoneNo_Details.attributedText = [[NSAttributedString alloc] initWithString:restModalOBJ.resturantPhoneNo
                                                                        attributes:underlineAttribute];
    
    lblRestName_Details.text = restModalOBJ.resturantName;
    
    if ([restModalOBJ.placeRatings isEqualToString:@"0"])
    {
        lblRating.text = @"N/A";
        _lbl_secondRating.text = @"N/A";
    }else{
        lblRating.text = [NSString stringWithFormat:@"%@",restModalOBJ.placeRatings];
        _lbl_secondRating.text = [NSString stringWithFormat:@"%@",restModalOBJ.placeRatings];
        
    }
    [_btn_readAllreviews setTitle:[restModalOBJ.totalReview isEqualToString:@"1"]?@"Read all reviews (1)":[restModalOBJ.totalReview isEqualToString:@""]?@"No reviews yet.":[NSString stringWithFormat:@"Read all reviews (%@) ",restModalOBJ.totalReview] forState:UIControlStateNormal];
    lbl_Review.text =[restModalOBJ.totalReview isEqualToString:@"1"]?@"Based on 1 review":[restModalOBJ.totalReview isEqualToString:@""]?@"Based on 0 reviews":[NSString stringWithFormat:@"Based on %@ reviews",restModalOBJ.totalReview];

    lblRating_Details.text = restModalOBJ.placeRatings;
    //restImage_Details.layer.cornerRadius = 3.0;
    //[restImage_Details setBackgroundColor:[UIColor blackColor]];
    //restImage_Details.layer.masksToBounds = YES;
    if(restModalOBJ.isCheckedCheckIn == TRUE)
    {
        [btn_CheckIn setSelected: YES];
    }
    else
    {
        [btn_CheckIn setSelected: NO];
    }
    
    [self setEmonjis:1 type:restModalOBJ.emonjiID.intValue];
    [self setEmonjis:2 type:restModalOBJ.emonjiID2.intValue];
    [self setEmonjis:3 type:restModalOBJ.emonjiID3.intValue];
    [self setEmonjis:4 type:restModalOBJ.emonjiID4.intValue];
    [self setEmonjiTimeLabel:restModalOBJ.latestEmonji.intValue];
    
    lbl_EmoniTime.text = restModalOBJ.emonjiTime;
    lblTime_X.constant= emonji_X;
    
    lbl_HeaderName.text = @"Hello";//restModalOBJ.resturantName;
    float sizeOfWidth = [self widthOfString:lbl_HeaderName.text withFont:lbl_HeaderName.font];
    NSLog(@"font width == %f",sizeOfWidth);
    lblHeader_X.constant = (sizeOfWidth>270) ? 70:0;

    [self liveFeedstarRating:restModalOBJ.placeRatings];
    [self userGivenRating:restModalOBJ.userRatings];
    [self commentsImageServiceHelper];
}


///---------------------------
#pragma mark TextField Method
///---------------------------

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    if(textField == comment_LiveFeed)
    {
        
        [self keyboardDidShow:nil];
    }
    
    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    
    [self keyboardDidHide:nil];
    [textField resignFirstResponder];
    return YES;
}


- (void)keyboardDidShow:(NSNotification *)notification
{
    /*CGRect frame;
    if(IS_IPHONE_6PLUS)
    {
        frame = CGRectMake(0,view_TextView.frame.origin.y-170,self.view.frame.size.width,view_TextView.frame.size.height);
        
    }
    else if (IS_IPHONE_6)
    {
        frame = CGRectMake(0,view_TextView.frame.origin.y-160,self.view.frame.size.width,view_TextView.frame.size.height);
    }
    else
    {
        frame = CGRectMake(0,view_TextView.frame.origin.y-170,self.view.frame.size.width,view_TextView.frame.size.height);
    }
    
    [UIView beginAnimations:@"" context:@""];
    [UIView animateWithDuration:0.1 animations:^{
        [view_TextView setFrame:frame];
        
    }];
    [UIView commitAnimations];*/
    
    
    
}

-(void)keyboardDidHide:(NSNotification *)notification
{
    
   /* CGRect frame;
    if(IS_IPHONE_6PLUS)
    {
        frame = CGRectMake(0,view_TextView.frame.origin.y+170,self.view.frame.size.width,view_TextView.frame.size.height);
        
    }
    else if (IS_IPHONE_6)
    {
        frame = CGRectMake(0,view_TextView.frame.origin.y+160,self.view.frame.size.width,view_TextView.frame.size.height);
    }
    else
    {
        frame = CGRectMake(0,view_TextView.frame.origin.y+170,self.view.frame.size.width,view_TextView.frame.size.height);
    }
    
    [UIView beginAnimations:@"" context:@""];
    [UIView animateWithDuration:0.1 animations:^{
        [view_TextView setFrame:frame];
        
    }];
    [UIView commitAnimations];*/
    
}

-(BOOL)textView:(UITextView*)textView shouldChangeCharactersInRange:(NSRange)range replacementText:(NSString*)text {
    if ([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    else
        return YES;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


#pragma mark -- Collection View Delegate Method

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
    
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return photosArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    PhotosCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell123" forIndexPath:indexPath];
    
    if(photosArray.count>0)
    {
        ResturantModal *modal = [photosArray objectAtIndex:indexPath.row];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            cell.asynchImage.layer.cornerRadius = 3.0;
            [cell.asynchImage setBackgroundColor:[UIColor grayColor]];
            cell.asynchImage.layer.masksToBounds = YES;
            NSURL *url = [[NSBundle mainBundle] URLForResource:@"loading-1" withExtension:@"gif"];
            NSString *newUrl = [modal.commentsImage stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
            [cell.asynchImage sd_setImageWithURL:[NSURL URLWithString:newUrl]
                                placeholderImage:[UIImage imageNamed:@"noimage.png"]];

            if (!images) {
                images=[NSMutableArray new];
            }
            FSBasicImage *image = [[FSBasicImage alloc] initWithImageURL:[NSURL URLWithString:modal.commentsImage] name:[NSString stringWithFormat:@"Photo %ld",(long)indexPath.row]];
            [images addObject:image];
        });
    }
    return cell;
}


-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{

    FSBasicImageSource *photoSource = [[FSBasicImageSource alloc] initWithImages:images];
    imageViewController = [[FSImageViewerViewController alloc] initWithImageSource:photoSource imageIndex:indexPath.item];
    
    imageViewController.view.frame = CGRectMake(0, (view_Tab.frame.origin.y+view_Tab.frame.size.height), self.view.frame.size.width, self.tabBarController.tabBar.frame.origin.y - (view_Tab.frame.origin.y+view_Tab.frame.size.height));
    imageViewController.view.backgroundColor=[UIColor grayColor];
    [self.view addSubview:imageViewController.view];
    
    
    btnGalleryClose=[[UIButton alloc]initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width-35, 5, 25,25)];
    [btnGalleryClose setImage:[UIImage imageNamed:@"close.png"] forState:UIControlStateNormal];
    [btnGalleryClose addTarget:self action:@selector(closeGallery) forControlEvents:UIControlEventTouchUpInside];
    [imageViewController.view addSubview:btnGalleryClose];
}

#pragma  mark TableView Datasource and Delegate Method

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if(tableView == tableView_Promt)
    {
           NSString *SortDay;
        if (arraySpecialforday.count > 0) {
            NSMutableDictionary *extractedData = [NSMutableDictionary new];
            for (ResturantModal *dict in arraySpecialforday) {
                NSString *Days = dict.specials_weekDay;
                
                if ([Days isEqualToString:@"monday"]) {
                    SortDay = [NSString stringWithFormat:@"1%@",Days];//@"0_monday";
                }
                else if ([Days isEqualToString:@"tuesday"]) {
                    SortDay = [NSString stringWithFormat:@"2%@",Days];
                    // SortDay = @"1_tuesday";
                }
                else if ([Days isEqualToString:@"wednesday"]) {
                    SortDay = [NSString stringWithFormat:@"3%@",Days];
                    // SortDay = @"2_wednesday";
                }
                else if ([Days isEqualToString:@"thursday"]) {
                    //SortDay = @"3_thursday";
                    SortDay = [NSString stringWithFormat:@"4%@",Days];
                }
                
                else if ([Days isEqualToString:@"friday"]) {
                    //SortDay = @"4_friday";
                    SortDay = [NSString stringWithFormat:@"5%@",Days];
                }
                
                else if ([Days isEqualToString:@"saturday"]) {
                    // SortDay = @"5_saturday";
                    SortDay = [NSString stringWithFormat:@"6%@",Days];
                }
                
                else if ([Days isEqualToString:@"sunday"]) {
                    // SortDay = @"6_sunday";
                    SortDay = [NSString stringWithFormat:@"7%@",Days];
                }
                NSString *firstLetter = [SortDay substringWithRange:NSMakeRange(0, 1)];
                NSMutableArray *rowArray = [extractedData objectForKey:firstLetter];//extractedData[firstLetter];
                NSLog(@"%@",extractedData);
                if (rowArray == nil){
                    rowArray = [NSMutableArray new];
                    [extractedData setObject:rowArray forKey:firstLetter];
                   // extractedData[firstLetter] = rowArray;
                }
                [rowArray addObject:dict];
            }
            [ArrDays removeAllObjects];
            [ArrDays addObject:extractedData];
            
            [rows removeAllObjects];
            rows = [ArrDays objectAtIndex:0];
    }
        NSArray *keys = [rows allKeys];
        keys = [keys sortedArrayUsingComparator:^(id a, id b) {
            return [a compare:b options:NSNumericSearch];
        }];
        
        NSLog(@"%@",keys);
        sections = keys;
        NSLog(@"%@",sections);
        return sections.count;
 }
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView == tableView_LiveFeed){
        if(liveFeedArray.count>0)
            return liveFeedArray.count;
        else
            return 1;
    }
    else if (tableView == tableView_Promt)
    {
      //  return arraySpecialforday.count;
        return [rows[sections[section]] count];
    }
    else
    {
        return 1;
    }
    return 10;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if(tableView == tableView_Promt)
    {
        NSString *strPrice;
        NSString *strItemName;
        NSString *strTime;
        static NSString *cellIdentifier = @"sampleidentifier";
        HappyHoursCell *cell = (HappyHoursCell *)[tableView_DailySpecial dequeueReusableCellWithIdentifier:cellIdentifier];
        
        if(cell == nil)
        {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"HappyHoursCell" owner:self options:nil]objectAtIndex:0];
        }
        
        if (sections.count>0)
        {
            NSArray *RowArr = rows[sections[indexPath.section]];
            ResturantModal *modelDailySpecialDetails = RowArr[indexPath.row];
            
            NSString *trimmedString = [modelDailySpecialDetails.title stringByTrimmingCharactersInSet:
                                       [NSCharacterSet whitespaceCharacterSet]];
            if ([modelDailySpecialDetails.price isEqualToString:@"N/A"] || [modelDailySpecialDetails.price isEqualToString:@"1/2 Off"] || [modelDailySpecialDetails.price isEqualToString:@"Other"] || [modelDailySpecialDetails.price isEqualToString:@"FREE"])
            {
                strPrice = modelDailySpecialDetails.price;
            }
            else
            {
                if ([modelDailySpecialDetails.price rangeOfString:@"$"].location == NSNotFound) {
                    strPrice = [NSString stringWithFormat:@"$%@",modelDailySpecialDetails.price];
                }else
                {
                    strPrice = [NSString stringWithFormat:@"%@",modelDailySpecialDetails.price];
                }
                
            }
            
            strItemName = trimmedString.uppercaseString;
            
            if(modelDailySpecialDetails.isCheckedFavSpecial == NO)
            {
                cell.btnFavSpecial.selected = false;
                
            }
            else{
                cell.btnFavSpecial.selected = true;
            }
            [cell.btnFavSpecial addTarget:self action:@selector(btnFavSpecialClick:) forControlEvents:UIControlEventTouchUpInside];
            cell.btnFavSpecial.tag = indexPath.row;
            
            NSString *stringStartTime;
            
            NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
            NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
            [dateFormatter1 setLocale:locale];

            dateFormatter1.dateFormat = @"HH:mm:ss";
            
            NSDate *date1 = [dateFormatter1 dateFromString:modelDailySpecialDetails.startTime];
            
            dateFormatter1.dateFormat = @"hh:mm a";
            
            stringStartTime = [dateFormatter1 stringFromDate:date1];
            
            stringStartTime = [stringStartTime stringByReplacingOccurrencesOfString:@":00" withString:@""];
            
            
            int stringStartTimeInt=[stringStartTime intValue];
            
            if (stringStartTimeInt<10)
            {
                NSRange range = NSMakeRange(0,1);
                stringStartTime = [stringStartTime stringByReplacingCharactersInRange:range withString:@""];
            }
            else
            {
                stringStartTime = [stringStartTime stringByReplacingOccurrencesOfString:@":00" withString:@""];
            }
            
            
            NSString *stringCloseTime;
            
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            NSLocale *locale2 = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
            [dateFormatter setLocale:locale2];

            dateFormatter.dateFormat = @"HH:mm:ss";
            
            NSDate *date = [dateFormatter dateFromString:modelDailySpecialDetails.endTime];
            
            
            
            dateFormatter.dateFormat = @"hh:mm a";
            
            stringCloseTime = [dateFormatter stringFromDate:date];
            
            stringCloseTime = [stringCloseTime stringByReplacingOccurrencesOfString:@":00" withString:@""];
            
            
            
            int stringCloseTimeInt=[stringCloseTime intValue];
            
            if (stringCloseTimeInt<10)
            {
                
                
                NSRange range = NSMakeRange(0,1);
                
                stringCloseTime = [stringCloseTime stringByReplacingCharactersInRange:range withString:@""];
                
                
            }
            else
            {
                stringCloseTime = [stringCloseTime stringByReplacingOccurrencesOfString:@":00" withString:@""];
            }
            strTime = [NSString stringWithFormat:@"(%@ - %@)",stringStartTime,stringCloseTime];
            NSString *AllItems = [NSString stringWithFormat:@"%@ %@ %@",strPrice,strItemName,strTime];
            cell.lblAllItems.text = AllItems;

            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        
        return cell;
    }
    else if (tableView == tableView_DailySpecial)
    {
        
        static NSString *cellIdentifier = @"sampleidentifier";
        HappyHoursCell *cell = (HappyHoursCell *)[tableView_DailySpecial dequeueReusableCellWithIdentifier:cellIdentifier];
        
        if(cell == nil)
        {
            cell = [[[NSBundle mainBundle]loadNibNamed:@"HappyHoursCell" owner:self options:nil]objectAtIndex:0];
        }
        return cell;
        
    }
    else
    {
        static NSString *cellIdentifier = @"MyTableCell";
        UILabel *emptyLabel = [[UILabel alloc]init];
        LiveFeedCell *cell = (LiveFeedCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle]loadNibNamed:@"LiveFeedCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        if(liveFeedArray.count>0)
        {
            emptyLabel.hidden = YES;
            [emptyLabel removeFromSuperview];
            ResturantModal *modelLiveFeed = [liveFeedArray objectAtIndex:indexPath.row];
            
            if ([modelLiveFeed.isRated isEqualToString:@"1"]){
                starRating_userGiven.userInteractionEnabled = NO;
            }
            else {
                starRating_userGiven.userInteractionEnabled = YES;
            }
            
            if ([modelLiveFeed.isCommentAdded isEqualToString:@"1"]) {
                comment_LiveFeed.userInteractionEnabled = NO;
                btn_comment.userInteractionEnabled = NO;
            }
            else {
                comment_LiveFeed.userInteractionEnabled = YES;
                btn_comment.userInteractionEnabled = YES;
            }
            
            cell.asynchProfileImage.layer.cornerRadius = 3.0;
            [cell.asynchProfileImage setBackgroundColor:[UIColor clearColor]];
            cell.asynchProfileImage.layer.masksToBounds = YES;
            dispatch_async(dispatch_get_main_queue(), ^{
                NSURL *url = [[NSBundle mainBundle] URLForResource:@"loading-1" withExtension:@"gif"];
                NSString *newUrl = [modelLiveFeed.profileImage stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
                [cell.asynchProfileImage sd_setImageWithURL:[NSURL URLWithString:newUrl]placeholderImage:[UIImage imageNamed:@"imgplaceHolder"]];
            });
            
            NSString *combinedStr = [NSString stringWithFormat:@"%@ %@",modelLiveFeed.firstName,modelLiveFeed.lastName];
            NSString *username = [NSString stringWithFormat:@"@%@",modelLiveFeed.userName];
            cell.lblUsername.text = username;
            cell.lbltweetName.text = combinedStr;
            cell.lbltweetMessage.text = modelLiveFeed.comments;
            
            cell.lbltweetTime.text = modelLiveFeed.createdOn;
            if((![modelLiveFeed.userGivenRating isEqualToString:@"0.0"]) && (![modelLiveFeed.userGivenRating isEqualToString:@"0"]))
            {
                [cell liveFeedstarRating :modelLiveFeed.userGivenRating];
                 cell.starRating_LiveFeed.hidden = false;
            }
            else{
               // [cell liveFeedstarRating :@"3.5"];
                cell.starRating_LiveFeed.hidden = true;
            }
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        
        return cell;
    }
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(selectedIndex == indexPath.row)
    {
        
    }
    else
    {
        
    }
    selectedIndex = indexPath.row;
    if(tableView ==  tableView_Promt)
    {
        if(isCheckedSelectedIndexPath == FALSE)
        {
            isCheckedSelectedIndexPath = TRUE;
            isCheckedPromotions = TRUE;
        }
        else
        {
            isCheckedSelectedIndexPath = FALSE;
            isCheckedPromotions = FALSE;
        }
        [tableView_Promt reloadData];
        
    }
    else if (tableView == tableView_LiveFeed)
    {
        if(liveFeedArray.count)
        {
            ProfileViewController *profileVC = [self.storyboard instantiateViewControllerWithIdentifier:@"profile"];
            profileVC.selectedController = @"otheruser";
            profileVC.selectedTweet = indexPath.row;
            profileVC.imageArray = liveFeedArray;
            profileVC.controllerName=@"view_feed";
            ResturantModal *modal = [liveFeedArray objectAtIndex:indexPath.row];
            profileVC.user_IDString = modal.userID;
            [self.navigationController pushViewController:profileVC animated:YES];
        }
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == tableView_LiveFeed)
    {
        return  UITableViewAutomaticDimension;
    }
    else if (tableView == tableView_Promt)
    {
        return 35;
    }
    else if (tableView == tableView_DailySpecial)
    {
        return 44;
    }
    
    return 90;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (tableView == tableView_Promt)
    {
        return 20;
    }
    return 0;
}
-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    static NSString *cellIdentifier = @"sampleidentifier";
    HeaderForDetail *cell = (HeaderForDetail *)[tableView_DailySpecial dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if(cell == nil)
    {
        cell = [[[NSBundle mainBundle]loadNibNamed:@"HeaderForDetail" owner:self options:nil]objectAtIndex:0];
    }
    if (section == 0){
        cell.VwSep.hidden = YES;
    }
    else{
        cell.VwSep.hidden = NO;
    }
    NSString *mystr;
    
    if([sections[section] isEqualToString:@"1"])
    {
        mystr= @"Monday"; //[modelDailySpecialDetails.specials_weekDay substringToIndex:1];
    }
    else if ([sections[section]  isEqualToString:@"2"])
    {
        mystr= @"Tuesday"; //[modelDailySpecialDetails.specials_weekDay substringToIndex:2];
    }
    else if ([sections[section]  isEqualToString:@"3"])
    {
        mystr= @"Wednesday"; //[modelDailySpecialDetails.specials_weekDay substringToIndex:1];
    }
    else if ([sections[section]  isEqualToString:@"4"])
    {
        mystr= @"Thursday"; //[modelDailySpecialDetails.specials_weekDay substringToIndex:2];
    }
    else if ([sections[section]  isEqualToString:@"5"])
    {
        mystr= @"Friday"; //[modelDailySpecialDetails.specials_weekDay substringToIndex:1];
    }
    else if ([sections[section]  isEqualToString:@"6"])
    {
        mystr= @"Saturday"; //[modelDailySpecialDetails.specials_weekDay substringToIndex:2];
    }
    else if ([sections[section]  isEqualToString:@"7"])
    {
        mystr= @"Sunday"; //[modelDailySpecialDetails.specials_weekDay substringToIndex:2];
    }
    cell.lblHeaderTitle.text = mystr;
    return cell;
}
///-------------------------
#pragma mark Button Method
///-------------------------
- (void) btnFavSpecialClickNew:(id) sender  {
    UIButton *btn = (UIButton*)sender;
    NSArray *RowArr = rows[sections[btn.titleLabel.tag]];
    ResturantModal *modelDailySpecialDetails = RowArr[btn.tag];
    
    NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:@"logindetails"];
    RegisterModal *registerModalResponeValue = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    NSString *strType;
    if(isCheckedDailySpecials == FALSE){
        strType = @"h";
    }
    else{
        strType = @"s";
    }
    
    NSString *strFav;
    if(btn.selected == true){
        strFav = @"2";
    }
    else{
        strFav = @"1";
    }
    
    BOOL check=[[AppDelegate shareAppdelegateInstance] checkReachability];
    if(check)
    {
        [modelDailySpecialDetails setUserID:registerModalResponeValue.userID];
        [CUSTOMINDICATORMACRO startLoadAnimation:self];
        [RestaurantMacro favUnfavSpecialToServer:modelDailySpecialDetails :strFav :strType withCompletion:^(id obj1) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [CUSTOMINDICATORMACRO stopLoadAnimation:self];
                ResturantModal *responseModal = nil;
                if([obj1 isKindOfClass:[ResturantModal class]])
                {
                    responseModal = obj1;
                    if([responseModal.result isEqualToString:@"success"]){
                        if(btn.selected == true){
                            [modelDailySpecialDetails setIsCheckedFavSpecial:NO];
                        }
                        else{
                            [modelDailySpecialDetails setIsCheckedFavSpecial:YES];
                        }
                        
                        if(btn.selected == true){
                            [btn setSelected:false];
                        }
                        else{
                            [btn setSelected:true];
                        }
                    }
                }
                else
                    //[self serverError];
                    
                    [CUSTOMINDICATORMACRO stopLoadAnimation:self];
            });
        }];
    }
}
- (void)btnFavSpecialClick:(id)sender {
    UIButton *btn = (UIButton*)sender;
    
    
    UIView *parentCell = btn.superview;
    
    while (![parentCell isKindOfClass:[UITableViewCell class]]) {   // iOS 7 onwards the table cell hierachy has changed.
        parentCell = parentCell.superview;
    }
    
    UIView *parentView = parentCell.superview;
    
    while (![parentView isKindOfClass:[UITableView class]]) {   // iOS 7 onwards the table cell hierachy has changed.
        parentView = parentView.superview;
    }
    
    UITableView *tableView = (UITableView *)parentView;
    NSIndexPath *indexPath = [tableView indexPathForCell:(UITableViewCell *)parentCell];
    
    NSLog(@"indexPath = %@", indexPath);
    
    
    
    CGPoint buttonOrigin = [sender frame].origin;
    CGPoint originInTableView = [tableView_Promt convertPoint:buttonOrigin fromView:[btn superview]];
                                 
  //  NSIndexPath *rowIndexPath = [tableView_Promt indexPathForRowAtPoint:originInTableView];
                                 

    HappyHoursCell *cell = (HappyHoursCell *)[tableView_Promt cellForRowAtIndexPath:indexPath];
    NSArray *RowArr = rows[sections[indexPath.section]];
    ResturantModal *modelDailySpecialDetails = RowArr[indexPath.row];

    NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:@"logindetails"];
    RegisterModal *registerModalResponeValue = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    NSString *strType;
    if(isCheckedDailySpecials == FALSE){
        strType = @"h";
    }
    else{
        strType = @"s";
    }
    
    NSString *strFav;
    if(btn.selected == true){
        strFav = @"2";
    }
    else{
        strFav = @"1";
    }
    
    BOOL check=[[AppDelegate shareAppdelegateInstance] checkReachability];
    if(check)
    {
        [modelDailySpecialDetails setUserID:registerModalResponeValue.userID];
        [CUSTOMINDICATORMACRO startLoadAnimation:self];
        [RestaurantMacro favUnfavSpecialToServer:modelDailySpecialDetails :strFav :strType withCompletion:^(id obj1) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [CUSTOMINDICATORMACRO stopLoadAnimation:self];
                ResturantModal *responseModal = nil;
                if([obj1 isKindOfClass:[ResturantModal class]])
                {
                    responseModal = obj1;
                    if([responseModal.result isEqualToString:@"success"]){
                        if(btn.selected == true){
                            [modelDailySpecialDetails setIsCheckedFavSpecial:NO];
                        }
                        else{
                            [modelDailySpecialDetails setIsCheckedFavSpecial:YES];
                        }
                        
                        if(btn.selected == true){
                            [cell.btnFavSpecial setSelected:false];
                        }
                        else{
                            [cell.btnFavSpecial setSelected:true];
                        }
                    }
                }
                else
                    //[self serverError];
                
                [CUSTOMINDICATORMACRO stopLoadAnimation:self];
            });
        }];
    }
}
-(IBAction)clickShareButton :(UIButton *)sender
{
    UIAlertController * view=   [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    
    
    UIAlertAction *faceBook = [UIAlertAction actionWithTitle:@"Share Facebook" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
        [view dismissViewControllerAnimated:NO completion:nil];
        
        [self shareFacebook:sender];
        
    }];
    
    UIAlertAction* twitter = [UIAlertAction actionWithTitle:@"Share Twitter" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
        [view dismissViewControllerAnimated:NO completion:nil];
        [self shareTwitter:sender];
        
    }];
    
    UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * action){
        [view dismissViewControllerAnimated:NO completion:nil];
    }];
    
    
    
    [view addAction:faceBook];
    [view addAction:twitter];
    [view addAction:cancel];
    
    [self.navigationController presentViewController:view animated:YES completion:nil];
}

-(void)shareFacebook:(id)sender
{
    NSString *webURL = restModalOBJ.strWebsite;
    NSString *contentString = [NSString stringWithFormat:@"%@,\n%@,%@\n%@,%@ - %@,%@",restModalOBJ.resturantName,restModalOBJ.resturantAddress,restModalOBJ.resturantAddress2, restModalOBJ.resturantCity,restModalOBJ.resturantCountry,restModalOBJ.resturantPostalCode,webURL];
    
    NSURL *imageURL = [NSURL URLWithString:restModalOBJ.DetailViewPlaceImage];
    
    FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
    content.contentTitle = @"SpotLyte";
    content.contentDescription = contentString;
    content.imageURL = imageURL;
    
    
    FBSDKShareDialog *dialog = [[FBSDKShareDialog alloc] init];
    dialog.fromViewController = viewController;
    dialog.shareContent = content;
    dialog.mode = FBSDKShareDialogModeFeedBrowser;
    [dialog show];
}


-(void)tweet
{
    TWTRComposer *composer = [[TWTRComposer alloc] init];
    NSString *contentString = [NSString stringWithFormat:@"%@,%@,%@ - %@,%@",restModalOBJ.resturantName,restModalOBJ.resturantCity,restModalOBJ.resturantCountry,restModalOBJ.resturantPostalCode,restModalOBJ.strWebsite];
    NSURL *imageURL;
    if ([restModalOBJ.DetailViewPlaceImage isEqualToString:@""]){
        imageURL = [NSURL URLWithString:restModalOBJ.placeImage];
    }
    else{
        
        imageURL = [NSURL URLWithString:restModalOBJ.DetailViewPlaceImage];
    }

    NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
    UIImage *image = [UIImage imageWithData:imageData];
    [composer setText:contentString];
    [composer setImage:image];
    
    [composer showFromViewController:self completion:^(TWTRComposerResult result)
     {
         if (result == TWTRComposerResultCancelled)
         {
             NSLog(@"Tweet composition cancelled");
         }
         else
         {
             NSLog(@"Sending Tweet!");
             
         }
     }];
    
}
-(void)shareTwitter :(id)sender
{
    BOOL isCheckedTwitter = [[NSUserDefaults standardUserDefaults]boolForKey:@"twitter"];
    
    if (isCheckedTwitter == TRUE)
    {
        
        [self tweet];
        
    }
    else
    {
        [[Twitter sharedInstance] logInWithCompletion:^(TWTRSession *session, NSError *error)
         {
             if (session)
             {
                 dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                     
                     NSLog(@"signed in as %@", [session userName]);
                     
                     dispatch_async(dispatch_get_main_queue(), ^{
                         
                         
                         [self tweet];
                     });
                 });
             
             } else {
                 NSLog(@"error: %@", [error localizedDescription]);
             }
         }];
    }
}

-(void)clickComments:(UIButton *)sender
{
    CommentsViewController *commentsVC =[self.storyboard instantiateViewControllerWithIdentifier:@"commentsscreen"];
    [self.navigationController pushViewController:commentsVC animated:YES];
}


-(void)clickMap:(UIButton *)sender
{
    HomeViewController *homeVC =[self.storyboard instantiateViewControllerWithIdentifier:@"homescreen"];
    [self.navigationController pushViewController:homeVC animated:YES];
}

-(IBAction)clickDashBoard:(id)sender
{
    DashBoardViewController *dash =[self.storyboard instantiateViewControllerWithIdentifier:@"dashboard"];
    [self.navigationController pushViewController:dash animated:YES];
}
-(IBAction)pressBack:(id)sender
{
    if(isCheckedDailySpecials == TRUE)
    {
        view_Special.hidden = YES;
        view_NoSpecial.hidden = YES;
        [self.view addSubview:view_HotalDetail];
        isCheckedDailySpecials = FALSE;
    }
    else if(isCheckedPromotions == TRUE)
    {
        isCheckedPromotions = FALSE;
        isCheckedSelectedIndexPath = FALSE;
        [tableView_Promt reloadData];
    }
    else
    {
        NSString *returnString = ([checkTextOrSearch isEqualToString:@"customsearch"]) ? @"true" : @"false";
        [restModalOBJ setHomeVCStatus:returnString];
        if (_selectHomeVC) {
            _selectHomeVC(restModalOBJ);
        }
        [self.navigationController popViewControllerAnimated:YES];
    }
}

-(IBAction)btnleftslide:(id)sender{
    [self slideImageLeft];
}

-(IBAction)btnrightslide:(id)sender{
    [self slideImageRight];
}


-(IBAction)clickHomeButton:(id)sender
{
    HomeViewController *homeVC =[self.storyboard instantiateViewControllerWithIdentifier:@"homescreen"];
    [self.navigationController pushViewController:homeVC animated:YES];
}
-(IBAction)pressDirections:(id)sender
{
    NearByViewController *nearbyVC =[self.storyboard instantiateViewControllerWithIdentifier:@"nearbyvc"];
    nearbyVC.restModal = restModalOBJ;
    [self.navigationController pushViewController:nearbyVC animated:YES];
}

-(IBAction)pressNearByBtn:(id)sender
{
    HomeViewController *homeVC = [self.storyboard instantiateViewControllerWithIdentifier:@"homescreen"];
    homeVC.checkVC = @"listplaces";
    homeVC.nearByRestModal = restModalOBJ;
    [self.navigationController pushViewController:homeVC animated:YES];
}

-(IBAction)pressTabButton:(id)sender
{
    UIButton *button = (UIButton *)sender;
    
    if(button.tag == 2){
       
        selectedWhichController=@"specialshappyhours";
         selectedView = 2;
         //[self colorChangeOnPressTab:@[btn_Promtions,btn_Details,btn_LiveFeed]];
         [self colorChangeOnPressTab:@[btn_HappyHours,btn_DailySpecials]];
    
         view_HappyHours.frame = CGRectMake(0, (view_Tab.frame.origin.y+view_Tab.frame.size.height), self.view.frame.size.width, self.tabBarController.tabBar.frame.origin.y - (view_Tab.frame.origin.y+view_Tab.frame.size.height));
        [self.view addSubview:view_HappyHours];
        
        
        [view_HotalDetail removeFromSuperview];
        [view_LiveFeed removeFromSuperview];
        [self specialsHappyHoursServiceHelper];
    }
    else if (button.tag == 1){
        [self setDetailsValue];
       selectedWhichController=@"eventSection";
        selectedView = 1;
        //[self colorChangeOnPressTab:@[btn_Details,btn_LiveFeed,btn_Promtions]];
        
       view_HotalDetail.frame = CGRectMake(0, (view_Tab.frame.origin.y+view_Tab.frame.size.height), [UIScreen mainScreen].bounds.size.width, self.tabBarController.tabBar.frame.origin.y - (view_Tab.frame.origin.y+view_Tab.frame.size.height));
        
        [self.view addSubview:view_HotalDetail];
        [view_Promotions removeFromSuperview];
        [view_LiveFeed removeFromSuperview];
        
        [self displayStar];
        
        if(restModalOBJ.isCheckedFavourite == NO){
            btn_FavSelected.selected = NO;
            [btn_FavSelected setImage:[UIImage imageNamed:@"Heart_unselect"] forState:UIControlStateNormal];
            [btn_FavSelected setTitleColor:[UIColor blackColor]forState:UIControlStateNormal];}
        else{
            btn_FavSelected.selected = YES;
            [btn_FavSelected setImage:[UIImage imageNamed:@"Heart_select"] forState:UIControlStateNormal];
            [btn_FavSelected setTitleColor:[UIColor blackColor] forState:UIControlStateNormal]; }
        
    }
    else{
        
        selectedWhichController=@"";
        selectedView = 0;
        
        //[self colorChangeOnPressTab:@[btn_LiveFeed,btn_Details,btn_Promtions]];
        [self colorChangeOnPressTab:@[comments_Btn,photos_Btn]];
        [self tapOnComments:YES];
        
        view_LiveFeed.frame = CGRectMake(0, (view_Tab.frame.origin.y+view_Tab.frame.size.height), self.view.frame.size.width, self.tabBarController.tabBar.frame.origin.y - (view_Tab.frame.origin.y+view_Tab.frame.size.height));
        [self.view addSubview:view_LiveFeed];
        
        [view_Promotions removeFromSuperview];
        [view_HotalDetail removeFromSuperview];
        [view_HappyHours removeFromSuperview];
    }
}


-(IBAction)clickFavourite:(id)sender
{
    
    if(restModalOBJ.isCheckedFavourite == NO)
    {
        [restModalOBJ setUserID:registerModalResponeValue.userID];
        
        BOOL check=[[AppDelegate shareAppdelegateInstance] checkReachability];
        if(check)
        {
            //[CUSTOMINDICATORMACRO startLoadAnimation:self];
            [RestaurantMacro favouriteToServer:restModalOBJ withCompletion:^(id obj1) {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [CUSTOMINDICATORMACRO stopLoadAnimation:self];
                    ResturantModal *responseModal = nil;
                    if([obj1 isKindOfClass:[ResturantModal class]])
                    {
                        responseModal = obj1;
                        if([responseModal.result isEqualToString:@"success"])
                        {
                            btn_FavSelected.selected = YES;
                            [btn_FavSelected setImage:[UIImage imageNamed:@"fav_selected"] forState:UIControlStateNormal];
                            [btn_FavSelected setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                            [restModalOBJ setIsCheckedFavourite:YES];
                        }
                        
                    }
                    else
                    {
                        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Please check your Internet Connection." preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction *okBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                            [self dismissViewControllerAnimated:YES completion:nil];
                        }];
                        [alertController addAction:okBtn];
                        [self presentViewController:alertController animated:YES completion:nil];
                    }
                    
                });
            }];
            
        }
        
    }
    else
    {
        [restModalOBJ setUserID:registerModalResponeValue.userID];
        
        BOOL check=[[AppDelegate shareAppdelegateInstance] checkReachability];
        if(check)
        {
            //[CUSTOMINDICATORMACRO startLoadAnimation:self];
            [RestaurantMacro unFavouriteToServer:restModalOBJ withCompletion:^(id obj1) {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [CUSTOMINDICATORMACRO stopLoadAnimation:self];
                    ResturantModal *responseModal = nil;
                    if([obj1 isKindOfClass:[ResturantModal class]])
                    {
                        responseModal = obj1;
                        if([responseModal.result isEqualToString:@"success"])
                        {
                            btn_FavSelected.selected = NO;
                            [btn_FavSelected setImage:[UIImage imageNamed:@"fav_unselected"] forState:UIControlStateNormal];
                            [btn_FavSelected setTitleColor:[UIColor blackColor]forState:UIControlStateNormal];
                            [restModalOBJ setIsCheckedFavourite:NO];
                        }
                    }
                    else
                    {
                        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:@"Please check your Internet Connection." preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction *okBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                            [self dismissViewControllerAnimated:YES completion:nil];
                        }];
                        [alertController addAction:okBtn];
                        [self presentViewController:alertController animated:YES completion:nil];
                    }
                    
                });
            }];
        }
        
    }
}


-(IBAction)pressDailySpecial:(id)sender
{
    lbl_TopView.text = modalOBJ.resturantName;
    
    [self.view addSubview:view_HappyHours];
    isCheckedDailySpecials = TRUE;
    view_HappyHours.frame = CGRectMake(0, (view_Tab.frame.origin.y+view_Tab.frame.size.height), self.view.frame.size.width, self.tabBarController.tabBar.frame.origin.y - (view_Tab.frame.origin.y+view_Tab.frame.size.height));
}

- (IBAction)MenuButtonClicked:(id)sender
{
    
    _MenuTapBtn_lbl.hidden=NO;
    _MenuTapBtn_lbl.layer.zPosition = 1;
    
    [_MenuTapBtn_lbl layoutIfNeeded];
    
    [view_LiveFeed setUserInteractionEnabled:NO];
    
    [view_HotalDetail setUserInteractionEnabled:NO];
    
    [view_Promotions setUserInteractionEnabled:NO];
    
    [view_HappyHours setUserInteractionEnabled:NO];
    
    
    [self.view endEditing:YES];
    [self.revealViewController revealToggle:sender];
}

-(IBAction)pressProfileBtn:(id)sender
{
    MyProfileViewController *profileVC = [self.storyboard instantiateViewControllerWithIdentifier:@"myprofile"];
    profileVC.selectedController = @"currentuser";
    profileVC.selectedTweet = 0;
    profileVC.user_IDString = registerModalResponeValue.userID;
    [self.navigationController pushViewController:profileVC animated:YES];
}

-(IBAction)pressSpotLight:(id)sender
{
    spotlytePromotionViewController *spotLightVC = [self.storyboard instantiateViewControllerWithIdentifier:@"spotlight"];
    [self.navigationController pushViewController:spotLightVC animated:YES];
}



- (void)contentForLiveFeed
{
    liveFeedArray = [NSMutableArray new];
    // 1
    LiveFeedModal *liveFeed1 = [LiveFeedModal new];
    liveFeed1.strTweetname = @"ChristyJason";
    liveFeed1.strTweetMessaged = @"The hotel is as Grand and Historical as you can find in Helsinki with wonder luxurious interior and an exceptional fine bar by any standards. The service is first rate and the staff very welcoming. Good place to stay on business or with the family in an interesting city. Central location a plus for strolling around town.";
    liveFeed1.strTweetImage = @"img_08.jpeg";
    liveFeed1.strTweetTime = @"10.30 am";
    liveFeed1.strTweetId = @"@Christyjason1890";
    liveFeed1.arrayOfMessages = [[NSArray alloc]initWithObjects:@"The Kämp hotel is in a class of its own. Service is first class as are the rooms. This kind of luxury is many times more expensive in just nearby Stockholm. Kämp's bar and restaurant are very good and food and drinks are super. Try the hamburger. It is a classic!",@"Everything was very good: personal, comfort, breakfast, location. Second time in this hotel and it is still as good as before. The only thing that disappointed me - the restaurant of pan-asian cuisine was closed.", nil];
    liveFeed1.arrayOfMessagesTiming = [[NSArray alloc]initWithObjects:@"12.30 pm",@"1.25 am",nil];
    
    [liveFeedArray addObject:liveFeed1];
    
    //2
    LiveFeedModal *liveFeed2 = [LiveFeedModal new];
    liveFeed2.strTweetname = @"Christopher";
    liveFeed2.strTweetMessaged = @"Previously my go-to hotel when on business in Helsinki. The rooms are really sumptuous and the beds are among the most comfortable I've experienced. Considering its location in central Helsinki it's also pretty quiet. That being said, since they stopped being an SPG hotel I've transferred my stays to another hotel. Shame! Was great whilst it lasted";
    liveFeed2.strTweetImage = @"img_01.jpeg";
    liveFeed2.strTweetTime = @"10.55 am";
    liveFeed2.strTweetId = @"@Christopher18";
    liveFeed2.arrayOfMessages = [[NSArray alloc]initWithObjects:@"Stayed at Vivanta for 3 nights as a base for visiting Mahabalipuram and Pondicherry. Very welcoming, excellent service, spacious grounds, lovely seafood open air beachside restaurant, huge pool with bar and access to the beach. You need a driver as about 30 minutes north of Mahabalipuram but otherwise it was a perfect location.",@"I had no issues right from the booking to check out. Everything was as informed. They had only one room when i called for reservation on the same night. It was delux room and expensive compared to other hotels in the area with similiar or better rooms. Excellent place for couples.. good privacy.",@"Superb Location. Great Hospitality, Best Food, Friendly Staff, Luxurious Rooms and lot more We were on our Business Trip and found this hotel the best for Business as well as Family Gateway Trip. Great Hotel", nil];
    liveFeed2.arrayOfMessagesTiming = [[NSArray alloc]initWithObjects:@"1 am",@"5.05 am",@"9.25 pm",nil];
    
    [liveFeedArray addObject:liveFeed2];
    
    //3
    LiveFeedModal *liveFeed3 = [LiveFeedModal new];
    liveFeed3.strTweetname = @"Maria";
    liveFeed3.strTweetMessaged = @"A beautifully furnished room, spacious with a lovely bathroom (with all the amenities). Exceptionally well placed in the the centre of the shopping district( which is closed to traffic). The port as well as a covered food market is within 12 minutes walking distance. Very good breakfast and a lovely terrace restaurant.";
    liveFeed3.strTweetImage = @"img_02.jpeg";
    liveFeed3.strTweetTime = @"9.00 pm";
    liveFeed3.strTweetId = @"@Maria9242";
    liveFeed3.arrayOfMessages = [[NSArray alloc]initWithObjects:@"This is one of the better Leela properties and has a very good location. The room was very good and comfortable with a sea view. The staff was the highlight - they never said no for anything and went out of their way to make us comfortable.",@"Everything was very good: personal, comfort, breakfast, location. Second time in this hotel and it is still as good as before. The only thing that disappointed me - the restaurant of pan-asian cuisine was closed.",@"Although not as well-known as its counterparts in Bangalore and Udaipur, The Leela Palace at Adyar in Chennai is a truly wonderful hotel - superb service, awesome views of the estuary and the sea, good spread at breakfast, spacious rooms, smiling staff....highly recommended!",@"I stayed in a room with a view of the river. It's amazing how this area survived a devastating flood only months ago. Regardless, their breakfast buffet is the best across India's 5 star restaurants in terms of its variety. It's also the only place I was able to get green juice in the morning.",@"I had come to stay for a long period from 23rd Feb to 8th March. The ambience is very good. Hotel staffs are very nice. I didn't feel like I was away from my home. Special thanks to Utkarsh, Payal, Rekha, Mayuri, Anu, Momita, Murli, Pushkar, Pujari, Ranjan & Phillip for making my stay wonderful.", nil];
    liveFeed3.arrayOfMessagesTiming = [[NSArray alloc]initWithObjects:@"3.20 pm",@"3.25 pm",@"4 am",@"10 am",@"9.31 pm",nil];
    
    [liveFeedArray addObject:liveFeed3];
    
    //4
    LiveFeedModal *liveFeed4 = [LiveFeedModal new];
    liveFeed4.strTweetname = @"LEELA";
    liveFeed4.strTweetMessaged = @"Many will be surprised by the general level of service in restaurants, cafés and hotels in Finland. Kämp is world class, they know what they're doing. It's a hotel where local business and upper class gather at the bar after work and the very hushhush upper floor lounge is perfect for a living room like business meeting";
    liveFeed4.strTweetImage = @"img_03.jpeg";
    liveFeed4.strTweetTime = @"10.20 am";
    liveFeed4.strTweetId = @"@LEELA1234";
    liveFeed4.arrayOfMessages = [[NSArray alloc]initWithObjects:@"Currently staying in The Lee Palace, Chennai, the property lives up to the brand Lee, plus the Front office team with an excellent service and their pro activeness for helping the guests, welcome was great .. had my rooms ready before hand, Room service was good, food is awesome, great Hospitality and A royal property",@"Stayed for two nights at this property with friends.. The property lives up the the name of the Leela group.. From the checkin to ushering us to the rooms, from the food to the room service everything was perfect.. ",@"Dear Guest Greetings from The Leela Palace. Thank you for taking the time to write such extensive and positive comments on. I am delighted to note that you have enjoyed your stay with us and the overall experience was very positive is very encouraging and a great source of motivation for all of us.",@"I stayed in this hotel for two days during a business travel. Fabulous hotel , great service. great service , great people I have ever met. They will look in to everything you need in instant , Of course its a palace , you get a royal treatment !!!!!!!!!!",nil];
    liveFeed4.arrayOfMessagesTiming = [[NSArray alloc]initWithObjects:@"12.30 pm",@"1.25 am",@"3.20 pm",@"5.25 am",nil];
    
    [liveFeedArray addObject:liveFeed4];
    
    //5
    LiveFeedModal *liveFeed5 = [LiveFeedModal new];
    liveFeed5.strTweetname = @"Alex";
    liveFeed5.strTweetMessaged = @"Amazingly central location. Large room sizes with modern technology. The service - dining, check in, check out, concierge, bell desk, shoe shine, room service - was all very prompt. It has good restaurants. I wish the gym were slightly better equipped. High time it is renovated.";
    liveFeed5.strTweetImage = @"img_04.jpeg";
    liveFeed5.strTweetTime = @"12 am";
    liveFeed5.strTweetId = @"@alex_alex343";
    liveFeed5.arrayOfMessages = [[NSArray alloc]initWithObjects:@"Location was very Nice. We enjoyed the trip with our team is joyful. Event coordination was Good. Food was also very Good. The beach resort was good space enough to have a get together for corporate meetings. Best place to rock and roll.",@"Had been for family weekend outing. Most of the food items was good except Fish which was not as good as other items. Rooms are good and air-con is good as well. Beach view from restaurant is good and worth spending peaceful time during weekends.", nil];
    liveFeed5.arrayOfMessagesTiming = [[NSArray alloc]initWithObjects:@"12.00 am",@"1.13 am",nil];
    
    [liveFeedArray addObject:liveFeed5];
    
    //6
    
    LiveFeedModal *liveFeed6 = [LiveFeedModal new];
    liveFeed6.strTweetname = @"Marc H";
    liveFeed6.strTweetMessaged = @"Best hotel in the locality. If u want a value for money hotel then this pick should be on your top of the list. No other hotel in this locality can match its level of cleanliness and hospitality. It's is not only good, it is above the expectations.";
    liveFeed6.strTweetImage = @"img_05.jpeg";
    liveFeed6.strTweetTime = @"3 pm";
    liveFeed6.strTweetId = @"@marc_h9023";
    liveFeed6.arrayOfMessages = [[NSArray alloc]initWithObjects:@"An amazing resort with good facilities, Beach side view with awesome food, what more do you want to enjoy winters with your friends. The rooms are very decent and at amazing price. Its a must recommended hotel for travellers and holiday lovers.",@"My recent visit to mgm resort was awesome... Ambience is too good... Loved the food... And service is very fast... Everyone must visit it once.. I will plan my next visit very soon... I invite my family and friends to plan a visit @ mgm",@"The location was amazing and fabolous in other words it simply marvelous.The room was so cleaned that it looking very fresh,it reaaly awesome. The view from hotel is too good,specally the sun set . The stay was awesome", @"We had a excellent time in MGM beach resort during my 2nd Daughter 4th Birthday.Food both Veg and Non Veg was excellent and especially Non Veg variety was awesome and my family really enjoyed the Food and Service. We plan to stay for 1 night for my 1st Daughter Preethika Sree Birthday on 20th Jul this year.",@"It was a great outing celebrating the success of our product and Jai from event management team made sure that the event was entertaining and fun. Food and ambience was great. Overall kudos to MGM and their event management team.",@"It was a company outing ; almost 100 people went there ; For large pax it's worth going ; Food was very good ; Near to beach ; Games were good ; easy access to beach ; Good scenery views ; overall it was a good experience",@"This is a beach resort , ask for sea view rooms. Food is amazing. Facilities- pool, spa, beach, volleyball, hammock Ideal for family outings. Priced expensive but worth it! Dinner buffet is exquisite! Very good resort for photography.",nil];
    liveFeed6.arrayOfMessagesTiming = [[NSArray alloc]initWithObjects:@"4.30 pm",@"2.25 am",@"2.30 am",@"1 pm",@"5.01 pm",@"7.20 pm",@"8.15 pm",nil];
    
    [liveFeedArray addObject:liveFeed6];
    
    //7
    
    LiveFeedModal *liveFeed7 = [LiveFeedModal new];
    liveFeed7.strTweetname = @"Kirsten E";
    liveFeed7.strTweetMessaged = @"The Kämp hotel is in a class of its own. Service is first class as are the rooms. This kind of luxury is many times more expensive in just nearby Stockholm. Kämp's bar and restaurant are very good and food and drinks are super. Try the hamburger. It is a classic!";
    liveFeed7.strTweetImage = @"img_06.jpeg";
    liveFeed7.strTweetTime = @"10.05 am";
    liveFeed7.strTweetId = @"@kristen";
    liveFeed7.arrayOfMessages = [[NSArray alloc]initWithObjects:@"The Kämp hotel is in a class of its own. Service is first class as are the rooms. This kind of luxury is many times more expensive in just nearby Stockholm. Kämp's bar and restaurant are very good and food and drinks are super. Try the hamburger. It is a classic!",@"Everything was very good: personal, comfort, breakfast, location. Second time in this hotel and it is still as good as before. The only thing that disappointed me - the restaurant of pan-asian cuisine was closed.", nil];
    liveFeed7.arrayOfMessagesTiming = [[NSArray alloc]initWithObjects:@"2.30 pm",@"6 am",nil];
    [liveFeedArray addObject:liveFeed7];
    
    //8
    
    LiveFeedModal *liveFeed8 = [LiveFeedModal new];
    liveFeed8.strTweetname = @"Chales";
    liveFeed8.strTweetMessaged = @"Everything was very good: personal, comfort, breakfast, location. Second time in this hotel and it is still as good as before. The only thing that disappointed me - the restaurant of pan-asian cuisine was closed.";
    liveFeed8.strTweetImage = @"img_07.jpeg";
    liveFeed8.strTweetTime = @"5.08 pm";
    liveFeed8.strTweetId = @"@Chales_pp87";
    liveFeed8.arrayOfMessages = [[NSArray alloc]initWithObjects:@"I really enjoying to stay in hotel sabri classic.In sabri classic all the departments are very responsible for their work.The house keeping deparment are very responsible.I remember the name(Kaushik,Krisna). both the guy is very cooperative and are cleaning the room very well also they are very co-operative.Also the other department are very well they are listening the guest very well.",@"i had a wonderful time through out my stay in this noble hotel the gokulam park. everything about the park is wonderful. the rooms are of international standard. the meal is inter-continental. all the staffs of the parks are accomodating and caring especially shri krishna i.t.", nil];
    liveFeed8.arrayOfMessagesTiming = [[NSArray alloc]initWithObjects:@"1.30 pm",@"9.20 pm",nil];
    [liveFeedArray addObject:liveFeed8];
    
}



-(void)setEmonjis :(int)selectedEmonjis type:(int)value
{
    switch (selectedEmonjis) {
        case e_EmonjiType_Cover:
        {
            if(value == 1)
            {
                [btn_Emonji1 setImage:[UIImage imageNamed:@"coversmall1.png"] forState:UIControlStateNormal];
            }
            else
            {
                [btn_Emonji1 setImage:[UIImage imageNamed:@"cover_pad.png"] forState:UIControlStateNormal];
            }
            break;
        }
        case e_EmonjiType_Wall:
        {
            if(value == 1)
            {
                [btn_Emonji2 setImage:[UIImage imageNamed:@"livesmall2.png"] forState:UIControlStateNormal];
            }
            else
            {
                [btn_Emonji2 setImage:[UIImage imageNamed:@"live_pad.png"] forState:UIControlStateNormal];
            }
            break;
        }
        case e_EmonjiType_Shirt:
        {
            if(value == 1)
            {
                [btn_Emonji3 setImage:[UIImage imageNamed:@"dresscodesmall.png"] forState:UIControlStateNormal];
                
            }
            else
            {
                [btn_Emonji3 setImage:[UIImage imageNamed:@"dresscode_pad.png"] forState:UIControlStateNormal];
            }
            break;
        }
        case e_EmonjiType_Music:
        {
            if(value == 1)
            {
                [btn_Emonji4 setImage:[UIImage imageNamed:@"live-musicsmall2.png"] forState:UIControlStateNormal];
            }
            else
            {
                [btn_Emonji4 setImage:[UIImage imageNamed:@"music_pad.png"] forState:UIControlStateNormal];
            }
            break;
        }
        default:
            break;
    }
}

-(void)setEmonjiTimeLabel :(int)value
{
    switch (value) {
        case e_EmonjiType_Cover:
        {
            emonji_X = btn_Emonji1.frame.origin.x;
            break;
        }
        case e_EmonjiType_Wall:
        {
            emonji_X = btn_Emonji2.frame.origin.x;
            break;
        }
        case e_EmonjiType_Shirt:
        {
            emonji_X = btn_Emonji3.frame.origin.x;
            break;
        }
        case e_EmonjiType_Music:
        {
            emonji_X = btn_Emonji4.frame.origin.x;
            break;
        }
        default:
        {
            break;
        }
    }
}


-(IBAction)pressEmonjis:(id)sender
{
    
    //if(isCheckedHotel == TRUE)
    //{
        UIButton *button = (UIButton *)sender;
        int buttonTag =  (int)button.tag;
        
        NSString *emonjiIdStr = [NSString stringWithFormat:@"%d",buttonTag];
        
        
        
        ResturantModal *modal = [[ResturantModal alloc]init];
        
        [modal setPlaceID:restModalOBJ.placeID];
        [modal setUserID:registerModalResponeValue.userID];
        [modal setEmonjiID:emonjiIdStr];
        
        [self setEmonjiServiceHelper:modal];
   /* }
    else
    {
        [self showAlertTitle:@"" Message:@"SpotLyte Encourages Real Time Reviews - You Are Not Currently Located Near This Location."];
    }*/
    
}

-(void)openCamera:(id)sender
{
    __block UIImagePickerController *picker;
    
    TOActionSheet *actionSheet = [[TOActionSheet alloc] init];
    actionSheet.title = @"";
    actionSheet.style = TOActionSheetStyleLight;
    
    [actionSheet addButtonWithTitle:@"Take Photo" tappedBlock:^{
        @try  {
            picker = [[UIImagePickerController alloc] init];
            picker.delegate = self;
            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
            [self presentViewController:picker animated:YES completion:NULL];
        }
        @catch (NSException *exception) {
            [self showAlertTitle:@"" Message:@"Camera is not available."];
        }
    }];
    [actionSheet addButtonWithTitle:@"Choose Photo" tappedBlock:^{
        picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:picker animated:YES completion:NULL];
    }];
    [actionSheet showFromView:self.view inView:self.view];
}

- (IBAction)pressSpecialHappyHours:(id)sender
{
    UIButton *button = (UIButton *)sender;
    
    
    
    if(button.tag == 0)
    {
         [self colorChangeOnPressTab:@[btn_HappyHours,btn_DailySpecials]];
        
        [UIView animateWithDuration:0.3
                              delay:0.1
                            options: UIViewAnimationOptionCurveEaseInOut
                         animations:^
         {
             isCheckedDailySpecials = false;
             if(restModal_QuickView.arrayHappyHours.count>0)
             {
                 arraySpecialforday = restModal_QuickView.arrayHappyHours;
                 tableView_Promt.hidden = NO;
                 view_Empty.hidden = YES;
                 
             }
             else{
                 tableView_Promt.hidden = YES;
                 view_Empty.hidden = NO;
             }
             
         }
                         completion:^(BOOL finished)
         {
             
         }];
        
        
        
    }
    else
    {
         [self colorChangeOnPressTab:@[btn_DailySpecials,btn_HappyHours]];
        [UIView animateWithDuration:0.3
                              delay:0.1
                            options: UIViewAnimationOptionCurveEaseInOut
                         animations:^
         {
             isCheckedDailySpecials = true;
           
             if(restModal_QuickView.arraySpecials.count>0)
             {
                 arraySpecialforday = restModal_QuickView.arraySpecials;
                 tableView_Promt.hidden = NO;
                 view_Empty.hidden = YES;
             }
             else
             {
                 tableView_Promt.hidden = YES;
                 view_Empty.hidden = NO;
             }
             
         }
                         completion:^(BOOL finished)
         {
             
         }];
        
        
    }
    [tableView_Promt reloadData];
}

-(IBAction)pressCallNumber: (id)sender
{
    if(restModalOBJ.resturantPhoneNo.length>0)
    {
        NSString *phNo =  [NSString  stringWithFormat:@"%@",restModalOBJ.resturantPhoneNo];
        phNo = [phNo stringByReplacingOccurrencesOfString:@"(" withString:@""];
        phNo = [phNo stringByReplacingOccurrencesOfString:@")" withString:@""];
        phNo = [phNo stringByReplacingOccurrencesOfString:@"-" withString:@""];
        phNo = [phNo stringByReplacingOccurrencesOfString:@" " withString:@""];
        NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",phNo]];
        
        if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]) {
            [[UIApplication sharedApplication] openURL:phoneUrl];
        } else
        {
            [self showAlertTitle:@"" Message:@"Call facility is not available!!!"];
        }
    }
}

-(IBAction)pressWebsite:(id)sender
{
    if(restModalOBJ.strWebsite.length>0)
    {
        TermsViewController *termsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"termsvc"];
        termsVC.isFromDetail = @"true";
        termsVC.CheckUrlType=@"ResturantEMail";
        termsVC.restModal_TermsVC = restModalOBJ;
        [self.navigationController pushViewController:termsVC animated:YES];
    }
    
}

- (IBAction)readAllReviews:(UIButton *)sender {
    
    self.viewTool.selectedSegmentIndex = 0;
    [self colorChangeOnPressTab:@[btn_HappyHours,btn_DailySpecials]];
    //[self colorChangeOnPressTab:@[btn_LiveFeed,btn_Promtions,btn_Details]];
    [self tapOnComments:YES];
    [view_Promotions removeFromSuperview];
    [view_HotalDetail removeFromSuperview];
    [view_HappyHours removeFromSuperview];
    
    view_LiveFeed.frame = CGRectMake(0, 60, self.view.frame.size.width, self.tabBarController.tabBar.frame.origin.y - (view_Tab.frame.origin.y+view_Tab.frame.size.height));
    [self.view addSubview:view_LiveFeed];
    
}

#pragma mark --  MFMailComposeViewControllerDelegate Methods

-(void)reportAction
{
   NSString *emailTitle = restModalOBJ.resturantName;
    // Email Content
  
    NSString *messageBody = @"";
    //  NSString *messageBody = (restModalOBJ.resturantDescription==nil || [restModalOBJ.resturantDescription isEqualToString:@""]) ? @"Test Mail":restModalOBJ.resturantDescription;
    // To address
    //NSArray *toRecipents = [NSArray arrayWithObject:@"spotlytegroup@gmail.com"];
    NSArray *toRecipents = [NSArray arrayWithObject:@"contact@spotlytespecials.com"];
    
    
    MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
    if ([MFMailComposeViewController canSendMail]) {
      mc.mailComposeDelegate = self;
    [mc setSubject:emailTitle];
    [mc setMessageBody:messageBody isHTML:NO];
    [mc setToRecipients:toRecipents];
    
    // Present mail view controller on screen
    
       [self presentViewController:mc animated:YES completion:nil];
    }
}

- (void)mailComposeController:(MFMailComposeViewController*)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError*)error;
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            
            NSLog(@"Mail cancelled");
            [self dismissViewControllerAnimated:YES completion:NULL];
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            [self dismissViewControllerAnimated:YES completion:NULL];
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            [self dismissViewControllerAnimated:YES completion:NULL];
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            [self dismissViewControllerAnimated:YES completion:NULL];
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    
}


-(IBAction)pressReportHereBtn : (id)sender
{
    [self reportAction];
}

#pragma  mark - Method to handle Base64 Image Encoding
- (NSString *)encodeToBase64String:(UIImage *)image
{
    return [UIImagePNGRepresentation(image) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
}

-(IBAction)pressSubmit:(id)sender
{
    //if(isCheckedHotel == TRUE)
   // {
        
        UIImage *resizeImage = [AppDelegate scaleAndRotateImage:commentsImage];
        
       /* if(comment_LiveFeed.text.length != 0 )
        {*/
            ResturantModal *modal = [[ResturantModal alloc]init];
            if(resizeImage == nil)
            {
                [modal setCommentsImage:@""];
                
            }
            else
            {
                
                [modal setCommentsImage:@""];
                
            }
            [modal setComments:comment_LiveFeed.text];
            [modal setPlaceID:restModalOBJ.placeID];
            [modal setIs_Emoji:@"1"];
            [modal setUserID:registerModalResponeValue.userID];
            [modal setSystemMessage:@"1"];
            [modal setCmdNumber:@"1"];
            [self createCommentServiceHelper:modal];
            
        /*}
        else
        {
            [self showAlertTitle:@"" Message:@"Please enter comment"];
        }*/
        
   /* }
    else
    {
        [self showAlertTitle:@"" Message:@"SpotLyte Encourages Real Time Reviews - You Are Not Currently Located Near This Location."];
    }*/
}

#pragma  mark --- Alert Controller Common Method

-(void)showAlertTitle:(NSString *)title Message:(NSString *)message
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [alertController addAction:okBtn];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

-(IBAction)pressCommentsOrPhotosTab:(id)sender
{
    UIButton *button = (UIButton *)sender;
    if(button.tag == 0){
        [self colorChangeOnPressTab:@[comments_Btn,photos_Btn]];
        [self tapOnComments:YES];}
    else{
        [self colorChangeOnPressTab:@[photos_Btn,comments_Btn]];
        [self tapOnComments:NO];
      }
}

-(void)tapOnComments:(BOOL)comment{
    if (comment) {
        TextViewHeight_Y.constant=60;  //177
        view_TextView.hidden=NO;
        
        viewCommentHeight_Y.constant=100;   //180
        viewCommentHeight_Y.priority=997;
         [self commentsServiceHelper];
      /*  if(liveFeedArray.count>0){
            view_NoComments.hidden = YES;
            tableView_LiveFeed.hidden = NO;
        }
        else{
            view_NoComments.hidden = NO;
            lbl_NoComments.text = @"No Comments Available At the Moment";
            tableView_LiveFeed.hidden  =YES;
        }
        */
        view_Rating.hidden = NO;
        lblBelowRating.hidden=NO;
        photos_CollectionView.hidden = YES;
        photosView.hidden = YES;
       

        
    }else{
        TextViewHeight_Y.constant=0;
        view_TextView.hidden=YES;
        
        viewCommentHeight_Y.constant=10;
        viewCommentHeight_Y.priority=999;
        
        photos_CollectionView.hidden = NO;
        view_Rating.hidden = YES;
        lblBelowRating.hidden=YES;
        tableView_LiveFeed.hidden = YES;
        [self commentsImageServiceHelper];
    }
}


#pragma mark - UIImagePicker Delegate Method

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    commentsImage = info[UIImagePickerControllerOriginalImage];
    
    UIImage *resizeImage = [AppDelegate scaleAndRotateImage:commentsImage];
    ResturantModal *modal = [[ResturantModal alloc]init];
    [modal setCommentsImage:[self encodeToBase64String:resizeImage]];
    [modal setComments:@""];
    [modal setPlaceID:restModalOBJ.placeID];
    [modal setIs_Emoji:@"1"];
    [modal setUserID:registerModalResponeValue.userID];
    [modal setSystemMessage:@"1"];
    [modal setCmdNumber:@"0"];
    [self createImageServiceHelper:modal];
    
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

-(IBAction)checkInButtonHandler:(id)sender
{
    if(isCheckedHotel == TRUE){
        if([btn_CheckIn isSelected] == YES){
            ERRORMESSAGE_ALERT(@"You have already check in.");}
        else{
            [self setCheckInServiceHelper];}}
    else{
        [self showAlertTitle:@"" Message:@"SpotLyte Encourages Real Time Reviews - In order to Check In you must be located near this location."];
    }
}

- (IBAction)MenuTapBtn:(id)sender
{
    
    _MenuTapBtn_lbl.hidden=YES;
    
    [view_LiveFeed setUserInteractionEnabled:YES];
    
    [view_HotalDetail setUserInteractionEnabled:YES];
    
    [view_Promotions setUserInteractionEnabled:YES];
    [view_HappyHours setUserInteractionEnabled:YES];
    
    
    [self.view endEditing:YES];
    [self.revealViewController revealToggle:sender];
}



-(void)colorChangeOnPressTab:(NSArray*)arry{

    dispatch_async(dispatch_get_main_queue(), ^{
        for (btnNo=0; btnNo<arry.count; btnNo++) {
            if (btnNo==0) {
                
                UIButton *button =(UIButton*)arry[0];
                button.backgroundColor=[UIColor colorWithRed:0.0f/255.0f green:204.0f/255.0f blue:255.0f/255.0f alpha:1];
                [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            }else
            {
                UIButton *button =(UIButton*)arry[btnNo];
                button.backgroundColor=[UIColor whiteColor];
                [button setTitleColor:[UIColor colorWithRed:54.0f/255.0f green:69.0f/255.0f blue:79.0f/255.0f alpha:1] forState:UIControlStateNormal];
            }
        }

    });
}



-(IBAction)btnMethods:(UIButton*)sender{

    NSInteger btnTag=sender.tag;

    switch (btnTag) {
        case 11:
        {
            if (![restModalOBJ.yelp_url isEqualToString:@""]) {
                TermsViewController *termsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"termsvc"];
                termsVC.checkVC = @"promotions";
                termsVC.CheckUrlType=@"yelp";
                termsVC.restModal_TermsVC = restModalOBJ;
                [self.navigationController pushViewController:termsVC animated:YES];
            }
        }
            break;
            
        case 22:
        {
             if (![restModalOBJ.open_table isEqualToString:@""]) {
            TermsViewController *termsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"termsvc"];
            termsVC.checkVC = @"promotions";
            termsVC.CheckUrlType=@"openTable";
            termsVC.restModal_TermsVC = restModalOBJ;
            [self.navigationController pushViewController:termsVC animated:YES];
             }
            
        }
            break;
            
        case 33:
        {
             if (![restModalOBJ.grub_hub isEqualToString:@""]) {
            TermsViewController *termsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"termsvc"];
            termsVC.checkVC = @"promotions";
            termsVC.CheckUrlType=@"GrubHub";
            termsVC.restModal_TermsVC = restModalOBJ;
            [self.navigationController pushViewController:termsVC animated:YES];
             }
        }
            break;
            
        case 44:
        {
             if (![restModalOBJ.beer_menus isEqualToString:@""]) {
            TermsViewController *termsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"termsvc"];
            termsVC.checkVC = @"promotions";
            termsVC.CheckUrlType=@"beerMenus";
            termsVC.restModal_TermsVC = restModalOBJ;
            [self.navigationController pushViewController:termsVC animated:YES];
             }
        }
            break;
            
        default:
            break;
    }

}



- (CGSize)getSizeForText:(NSString *)strHeaderText{
    CGSize constraint = CGSizeMake(self.view.bounds.size.width-80, 9999);
    CGSize size;
    
    NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
    CGSize boundingBox = [strHeaderText boundingRectWithSize:constraint
                                                     options:NSStringDrawingUsesLineFragmentOrigin
                                                  attributes:@{NSFontAttributeName:Calibri(13.0f)}
                                                     context:context].size;
    size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
    
    context=nil;
    return size;
}
-(void)closeGallery{
      [imageViewController.view removeFromSuperview];
}

-(void)dealloc{
   /* for (UIView *view in self.view.subviews) {
        [view removeFromSuperview];
    }
    
   [promtionsArray removeAllObjects];
   [photosArray removeAllObjects];
    hotelNameArray=nil;
   hotelImageArray=nil;
    
   dictWeeks=nil;
    
    arraySpecialforday;
    @property (nonatomic, retain) NSMutableArray *hotelPromtionsArray;
    @property (nonatomic, strong) NSMutableDictionary *imageDownloadsInProgress;
    
    */
}



- (IBAction)onPressAddPhotos:(id)sender {
    __block UIImagePickerController *picker;
    
    TOActionSheet *actionSheet = [[TOActionSheet alloc] init];
    actionSheet.title = @"";
    actionSheet.style = TOActionSheetStyleLight;
    
    [actionSheet addButtonWithTitle:@"Take Photo" tappedBlock:^{
        @try  {
            picker = [[UIImagePickerController alloc] init];
            picker.delegate = self;
            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
            [self presentViewController:picker animated:YES completion:NULL];
        }
        @catch (NSException *exception) {
            [self showAlertTitle:@"" Message:@"Camera is not available."];
        }
    }];
    [actionSheet addButtonWithTitle:@"Choose Photo" tappedBlock:^{
        picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:picker animated:YES completion:NULL];
    }];
    [actionSheet showFromView:self.view inView:self.view];
}
@end
