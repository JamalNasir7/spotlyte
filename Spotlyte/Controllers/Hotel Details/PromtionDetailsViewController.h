//
//  PromtionDetailsViewController.h
//  Spotlyte
//
//  Created by Admin on 18/03/16.
//  Copyright © 2016 Naveen. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface PromtionDetailsViewController : UIViewController<EDStarRatingProtocol>
{
    IBOutlet UIImageView *asynchHotelImage;
    IBOutlet UITextView  *txt_desc;
    IBOutlet UILabel     *lblName;
    IBOutlet UILabel     *lblmiles;
    IBOutlet UILabel     *lblAddress;
    IBOutlet UILabel     *lblOverallRating;
    IBOutlet EDStarRating *starRating;
    IBOutlet UILabel     *lblReviews;
    IBOutlet UILabel     *lblStartDate;
    IBOutlet UILabel     *lblEndDate;
}

@property (nonatomic, retain) PromtionsModal *promtionModalObj;
@property (nonatomic, retain) ResturantModal *resturantModalObj;

@property (nonatomic, retain) NSArray *promtions;
@property (strong, nonatomic) IBOutlet UIButton *btnFavourite;

@property (strong, nonatomic) IBOutlet UIButton *btnViewLocation;
@property (strong, nonatomic) IBOutlet UILabel *lblPromotionTitle;

@end
