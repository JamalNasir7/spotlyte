//
//  Global.h
//
//


//App Name.
#define kAppName        @"Vyaapaar Samachar"
#define kAppStoreUrl    @""

//Live Url
#define kwebserviceUsername @"admin"
#define kwebservicePassword @"1234"

//#define baseURLPath @"http://192.168.0.47/vyapar_samachar/ws/"   //Local server
//#define baseURLPath @"https://www.vyaapaarsamachar.com/development/ws/"

//#define baseURLPath @"https://www.vyaapaarsamachar.com/development/ws/"
//#define baseURLShareNews @"https://www.vyaapaarsamachar.com/development/share"
//
//#define ShareBaseURLPath @""
//
//#define BaseURLImagePath @"vyaapaarsamachar/uploads/users/portfolio_user/filename/"
//#define BaseURLImagePathDevelopment @"https://www.vyaapaarsamachar.com/development/uploads/users/portfolio_users/"

#define baseURLPath @"https://www.vyaapaarsamachar.com/ws/"  // Live server
#define baseURLShareNews @"https://www.vyaapaarsamachar.com/share"

#define ShareBaseURLPath @""

#define BaseURLImagePath @"vyaapaarsamachar/uploads/users/portfolio_user/filename/"
#define BaseURLImagePathDevelopment @"https://www.vyaapaarsamachar.com/uploads/users/portfolio_users/"

//Device Compatibility

#define g_IS_IPHONE             ( [[[UIDevice currentDevice] model] isEqualToString:@"iPhone"] )
#define g_IS_IPOD               ( [[[UIDevice currentDevice] model] isEqualToString:@"iPod touch"] )
#define g_IS_IPAD               ( [[[UIDevice currentDevice] model] isEqualToString:@"iPad"] || [[[UIDevice currentDevice] model] isEqualToString:@"iPad Simulator"])


#define font_roboto_Thin  @"Roboto-Thin"
#define font_roboto_Condesed  @"Roboto-Condensed"
#define font_roboto_Italic   @"Roboto-Italic"
#define font_roboto_BlackItalic   @"Roboto-BlackItalic"
#define font_roboto_Light   @"Roboto-Light"
#define font_roboto_BoldI   @"Roboto-BoldItalic"
#define font_roboto_BoldCond   @"Roboto-BoldCondensed"
#define font_roboto_BoldCondBlack   @"Roboto-Black"
#define font_roboto_BoldCondI   @"Roboto-CondensedItalic"
#define font_roboto_LithIta   @"Roboto-LightItalic"
#define font_roboto_ThinIta   @"Roboto-ThinItalic"
#define font_roboto_Bold   @"Roboto-Bold"
#define font_roboto_Regular   @"Roboto-Regular"
#define font_roboto_Medium  @"Roboto-Medium"
#define font_roboto_BoldCondIta  @"Roboto-BoldCondensedItalic"
#define font_roboto_MediumIta  @"Roboto-MediumItalic"


#define k_userD_RefreshRate @"RefreshTime"
#define k_userD_Notification @"is_Notificaton"


#define k_NotificationMutualFundFileterSelection  @"mutualfund_selectvalues"
#define k_NotificationCommodities  @"Comodities_selectvalues"
#define k_NotificationCommoFilter  @"Comodities_FilterValues"
#define k_NotificationCommodityFilter = @"CommodityFilterfromDetail"
#define k_NotificationDerivatives  @"Derivatives_FilterValues"
#define k_Notificationvolume       @"Detail_Volume"

#define k_Noti_NewMutual  @"NewmutualEquityfund_selectvalues"
#define k_NotificationMutualFundEquityFileterSelection  @"mutualEquityfund_selectvalues"

#define k_NotificationStockTopGainer  @"TopGainerStock_FilterValues"

#define k_PushNotificationArticale @"pushnotification_Article"
#define k_PushNotificationArticaleRefresh @"pushnotification_Article_Refresh"

#define k_NotificationPortfolio  @"Portfolio_selectvalues"

//Display Size Compatibility
#define g_IS_IPHONE_4_SCREEN    [[UIScreen mainScreen] bounds].size.height >= 480.0f && [[UIScreen mainScreen] bounds].size.height < 568.0f
#define g_IS_IPHONE_5_SCREEN    [[UIScreen mainScreen] bounds].size.height >= 568.0f && [[UIScreen mainScreen] bounds].size.height < 667.0f
#define g_IS_IPHONE_6_SCREEN    [[UIScreen mainScreen] bounds].size.height >= 667.0f && [[UIScreen mainScreen] bounds].size.height < 736.0f
#define g_IS_IPHONE_6P_SCREEN    [[UIScreen mainScreen] bounds].size.height >= 736.0f && [[UIScreen mainScreen] bounds].size.height < 1024.0f


//IOS Version Compatibility
#define g_IS_IOS_10 ([[[UIDevice currentDevice] systemVersion] floatValue] >= 10.0)
#define g_IS_IOS_9 ([[[UIDevice currentDevice] systemVersion] floatValue] >= 9.0)
#define g_IS_IOS_8 ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
#define g_IS_IOS_7 ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)
#define g_IS_IOS_6 ([[[UIDevice currentDevice] systemVersion] floatValue] < 7.0)

//screen width height
#define screenWidth appDelegate.window.bounds.size.width
#define screenHeight appDelegate.window.bounds.size.height

#define screenWidthself self.view.frame.size.width
#define screenHeightself self.view.frame.size.height

//hexadecimal color to RGB Color
#define RGBA(r,g,b,a) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:a]
#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

////appdelegate
//#define appDelegate ((AppDelegate *)[[UIApplication sharedApplication]delegate])

//singleton
#define singleton [Singleton sharedSingleton]

//common define functions
#define removeNull(strData) [StaticClass removeNull:strData]
#define removeNulltoDash(strData) [StaticClass removeNullToDash:strData]
#define removeNulltoUndefined(strData) [StaticClass removeNullToUndefined:strData]
#define removeNullNo(strData) [StaticClass removeNullNumerical:strData]
#define removeNullFArry(strData) [StaticClass removeNullFromArray:strData]
#define removeNullFDict(strData) [StaticClass removeNullFromDictionary:strData]

//Defalt LOGO url
#define k_applogoURL @""
#define googleTrackingID @""
#define googleTrackingKey @""

#define TRIM(string) [string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]

//Filter table Screen Flag
#define keyFilterMarketForex 1

//USER INFO
#define KEY_EMAILADDRESS     [StaticClass retrieveFromUserDefaults:@"email"]
#define KEY_ID               [StaticClass retrieveFromUserDefaults:@"id"]
#define KEY_PHONE            [StaticClass retrieveFromUserDefaults:@"phone"]
#define KEY_MOBILE_NUMBER           [StaticClass retrieveFromUserDefaults:@"mobile_no"]
#define KEY_TOKEN            [StaticClass retrieveFromUserDefaults:@"token"]
#define KEY_FIRST_NAME          [StaticClass retrieveFromUserDefaults:@"first_name"]
#define KEY_LAST_NAME          [StaticClass retrieveFromUserDefaults:@"last_name"]
#define KEY_PROFILE_PIC          [StaticClass retrieveFromUserDefaults:@"profile_pic"]
#define KEY_DATE_OF_BIRTH          [StaticClass retrieveFromUserDefaults:@"date_of_birth"]
#define KEY_PASSWORD         [StaticClass retrieveFromUserDefaults:@"password"]
#define KEY_GOOGLE_ID         [StaticClass retrieveFromUserDefaults:@"google_id"]
#define KEY_FACEBOOK_ID         [StaticClass retrieveFromUserDefaults:@"facebook_id"]
#define KEY_LINKEDIN_ID         [StaticClass retrieveFromUserDefaults:@"linkedin_id"]
#define KEY_TWITTER_ID         [StaticClass retrieveFromUserDefaults:@"twitter_id"]
#define KEY_PHONE        [StaticClass retrieveFromUserDefaults:@"phone"]
#define IS_LOGIN          [StaticClass retrieveFromUserDefaults:@"is_login"]

