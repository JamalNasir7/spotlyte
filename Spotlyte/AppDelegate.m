//
//  AppDelegate.m
//  Spotlyte
//
//  Created by Tamilarasan on 3/3/16.
//  Copyright © 2016 Naveen. All rights reserved.
//

#import "AppDelegate.h"
#import "LiveFeedModal.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import <TwitterKit/TwitterKit.h>

@interface AppDelegate ()
@end

@implementation AppDelegate
@synthesize liveFeedArray,userCurrentLocation;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    [Fabric with:@[[Twitter class]]];
    [Fabric with:@[[Crashlytics class]]];
    _Modal_PlaceType = [[NSString alloc]init];
    _Modal_CityOrArea = [[NSString alloc]init];
    [self contentForLiveFeed];
    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"firstTimeLogin"];

    BOOL isChecked = [[NSUserDefaults standardUserDefaults]boolForKey:@"login"];
    if(isChecked == TRUE)
    {
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"firstTimeLogin"];
        self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
        self.navigationController = [[UINavigationController alloc]init];
        self.window.rootViewController = self.navigationController;
        self.navigationController.navigationBarHidden=YES;
        //[self pushHomeView];
       [self setCustomeTabbar];
    }
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    
    return YES;
}
- (void)pushHomeView
{
    SWRevealViewController *obj_RevealVC = [[UIStoryboard storyboardWithName:@"Main" bundle: nil] instantiateViewControllerWithIdentifier:@"SWRevealViewStoryboard"];
    [self.navigationController pushViewController:obj_RevealVC animated:YES];
}


-(void) setCustomeTabbar {
    //======================= set custome tabbar =============================
    self.window =[[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    [self setTabBar];
    self.navObj =[[UINavigationController alloc] initWithRootViewController:self.objCustomTabBar];
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    MenuViewController *slideObj = [mainStoryboard instantiateViewControllerWithIdentifier:@"MenuViewController"];
    
    UINavigationController *navSlider = [[UINavigationController alloc] initWithRootViewController:slideObj];
    navSlider.navigationBarHidden = true;
    //searchObj = [[SearchVC alloc] initWithNibName:@"SearchVC" bundle:nil];
    SWRevealViewController *mainRevealController =
    [[SWRevealViewController alloc] initWithRearViewController:navSlider frontViewController:self.navObj];
    mainRevealController.rearViewRevealWidth = ([UIScreen mainScreen].bounds.size.width * 80)/100;
    mainRevealController.rearViewRevealOverdraw = 120;
    //mainRevealController.shouldUseFrontViewOverlay = true;
    mainRevealController.bounceBackOnOverdraw = NO;
    mainRevealController.stableDragOnOverdraw = YES;
    mainRevealController.delegate = self;
    
    self.window.rootViewController = mainRevealController;
    self.navObj.navigationBarHidden=YES;
    [self.window makeKeyAndVisible];
    //=====================================================================
    
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    
    if ([url.absoluteString hasPrefix:@"fb"])
        return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                              openURL:url sourceApplication:sourceApplication annotation:annotation];
    else
        return [[GIDSignIn sharedInstance] handleURL:url
                                   sourceApplication:sourceApplication
                                          annotation:annotation];
}

/*- (BOOL)application:(UIApplication *)application
 openURL:(NSURL *)url
 sourceApplication:(NSString *)sourceApplication
 annotation:(id)annotation {
 return [[FBSDKApplicationDelegate sharedInstance] application:application
 openURL:url
 sourceApplication:sourceApplication
 annotation:annotation];
 }*/



- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    
    [FBSDKAppEvents activateApp];
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}



#pragma mark Check Reachability

-(BOOL)checkReachability
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    
    if(networkStatus == NotReachable)
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Internet is not Available", nil) message:NSLocalizedString(@"Please connect to Internet", nil) delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
        
        return NO;
    }
    else
    {
        return YES;
    }
}

+(AppDelegate *)shareAppdelegateInstance;
{
    return (AppDelegate*)[[UIApplication sharedApplication] delegate];
}

-(UINavigationController *)getNavigationController
{
    UIStoryboard *storyboardObj = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UINavigationController *navigationControllerObj = [storyboardObj instantiateViewControllerWithIdentifier:@"gopher_navigation"];
    
    
    return navigationControllerObj;
    
    
}

-(UIStoryboard *)getStoryBoardObj
{
    UIStoryboard *storyboardObj = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    return storyboardObj;
    
}

- (void)contentForLiveFeed
{
    liveFeedArray = [NSMutableArray new];
    // 1
    LiveFeedModal *liveFeed1 = [LiveFeedModal new];
    liveFeed1.strTweetname = @"ChristyJason";
    liveFeed1.strTweetMessaged = @"The hotel is as Grand and Historical as you can find in Helsinki with wonder luxurious interior and an exceptional fine bar by any standards. The service is first rate and the staff very welcoming. Good place to stay on business or with the family in an interesting city. Central location a plus for strolling around town.";
    liveFeed1.strTweetImage = @"img_08.jpeg";
    liveFeed1.strTweetTime = @"10.30 am";
    liveFeed1.strTweetId = @"@Christyjason1890";
    liveFeed1.arrayOfMessages = [[NSArray alloc]initWithObjects:@"The Kämp hotel is in a class of its own. Service is first class as are the rooms. This kind of luxury is many times more expensive in just nearby Stockholm. Kämp's bar and restaurant are very good and food and drinks are super. Try the hamburger. It is a classic!",@"Everything was very good: personal, comfort, breakfast, location. Second time in this hotel and it is still as good as before. The only thing that disappointed me - the restaurant of pan-asian cuisine was closed.", nil];
    liveFeed1.arrayOfMessagesTiming = [[NSArray alloc]initWithObjects:@"12.30 pm",@"1.25 am",nil];
    
    [liveFeedArray addObject:liveFeed1];
    
    //2
    LiveFeedModal *liveFeed2 = [LiveFeedModal new];
    liveFeed2.strTweetname = @"Christopher";
    liveFeed2.strTweetMessaged = @"Previously my go-to hotel when on business in Helsinki. The rooms are really sumptuous and the beds are among the most comfortable I've experienced. Considering its location in central Helsinki it's also pretty quiet. That being said, since they stopped being an SPG hotel I've transferred my stays to another hotel. Shame! Was great whilst it lasted";
    liveFeed2.strTweetImage = @"img_01.jpeg";
    liveFeed2.strTweetTime = @"10.55 am";
    liveFeed2.strTweetId = @"@Christopher18";
    liveFeed2.arrayOfMessages = [[NSArray alloc]initWithObjects:@"Stayed at Vivanta for 3 nights as a base for visiting Mahabalipuram and Pondicherry. Very welcoming, excellent service, spacious grounds, lovely seafood open air beachside restaurant, huge pool with bar and access to the beach. You need a driver as about 30 minutes north of Mahabalipuram but otherwise it was a perfect location.",@"I had no issues right from the booking to check out. Everything was as informed. They had only one room when i called for reservation on the same night. It was delux room and expensive compared to other hotels in the area with similiar or better rooms. Excellent place for couples.. good privacy.",@"Superb Location. Great Hospitality, Best Food, Friendly Staff, Luxurious Rooms and lot more We were on our Business Trip and found this hotel the best for Business as well as Family Gateway Trip. Great Hotel", nil];
    liveFeed2.arrayOfMessagesTiming = [[NSArray alloc]initWithObjects:@"1 am",@"5.05 am",@"9.25 pm",nil];
    
    [liveFeedArray addObject:liveFeed2];
    
    //3
    LiveFeedModal *liveFeed3 = [LiveFeedModal new];
    liveFeed3.strTweetname = @"Maria";
    liveFeed3.strTweetMessaged = @"A beautifully furnished room, spacious with a lovely bathroom (with all the amenities). Exceptionally well placed in the the centre of the shopping district( which is closed to traffic). The port as well as a covered food market is within 12 minutes walking distance. Very good breakfast and a lovely terrace restaurant.";
    liveFeed3.strTweetImage = @"img_02.jpeg";
    liveFeed3.strTweetTime = @"9.00 pm";
    liveFeed3.strTweetId = @"@Maria9242";
    liveFeed3.arrayOfMessages = [[NSArray alloc]initWithObjects:@"This is one of the better Leela properties and has a very good location. The room was very good and comfortable with a sea view. The staff was the highlight - they never said no for anything and went out of their way to make us comfortable.",@"Everything was very good: personal, comfort, breakfast, location. Second time in this hotel and it is still as good as before. The only thing that disappointed me - the restaurant of pan-asian cuisine was closed.",@"Although not as well-known as its counterparts in Bangalore and Udaipur, The Leela Palace at Adyar in Chennai is a truly wonderful hotel - superb service, awesome views of the estuary and the sea, good spread at breakfast, spacious rooms, smiling staff....highly recommended!",@"I stayed in a room with a view of the river. It's amazing how this area survived a devastating flood only months ago. Regardless, their breakfast buffet is the best across India's 5 star restaurants in terms of its variety. It's also the only place I was able to get green juice in the morning.",@"I had come to stay for a long period from 23rd Feb to 8th March. The ambience is very good. Hotel staffs are very nice. I didn't feel like I was away from my home. Special thanks to Utkarsh, Payal, Rekha, Mayuri, Anu, Momita, Murli, Pushkar, Pujari, Ranjan & Phillip for making my stay wonderful.", nil];
    liveFeed3.arrayOfMessagesTiming = [[NSArray alloc]initWithObjects:@"3.20 pm",@"3.25 pm",@"4 am",@"10 am",@"9.31 pm",nil];
    
    [liveFeedArray addObject:liveFeed3];
    
    //4
    LiveFeedModal *liveFeed4 = [LiveFeedModal new];
    liveFeed4.strTweetname = @"LEELA";
    liveFeed4.strTweetMessaged = @"Many will be surprised by the general level of service in restaurants, cafés and hotels in Finland. Kämp is world class, they know what they're doing. It's a hotel where local business and upper class gather at the bar after work and the very hushhush upper floor lounge is perfect for a living room like business meeting";
    liveFeed4.strTweetImage = @"img_03.jpeg";
    liveFeed4.strTweetTime = @"10.20 am";
    liveFeed4.strTweetId = @"@LEELA1234";
    liveFeed4.arrayOfMessages = [[NSArray alloc]initWithObjects:@"Currently staying in The Lee Palace, Chennai, the property lives up to the brand Lee, plus the Front office team with an excellent service and their pro activeness for helping the guests, welcome was great .. had my rooms ready before hand, Room service was good, food is awesome, great Hospitality and A royal property",@"Stayed for two nights at this property with friends.. The property lives up the the name of the Leela group.. From the checkin to ushering us to the rooms, from the food to the room service everything was perfect.. ",@"Dear Guest Greetings from The Leela Palace. Thank you for taking the time to write such extensive and positive comments on. I am delighted to note that you have enjoyed your stay with us and the overall experience was very positive is very encouraging and a great source of motivation for all of us.",@"I stayed in this hotel for two days during a business travel. Fabulous hotel , great service. great service , great people I have ever met. They will look in to everything you need in instant , Of course its a palace , you get a royal treatment !!!!!!!!!!",nil];
    liveFeed4.arrayOfMessagesTiming = [[NSArray alloc]initWithObjects:@"12.30 pm",@"1.25 am",@"3.20 pm",@"5.25 am",nil];
    
    [liveFeedArray addObject:liveFeed4];
    
    //5
    LiveFeedModal *liveFeed5 = [LiveFeedModal new];
    liveFeed5.strTweetname = @"Alex";
    liveFeed5.strTweetMessaged = @"Amazingly central location. Large room sizes with modern technology. The service - dining, check in, check out, concierge, bell desk, shoe shine, room service - was all very prompt. It has good restaurants. I wish the gym were slightly better equipped. High time it is renovated.";
    liveFeed5.strTweetImage = @"img_04.jpeg";
    liveFeed5.strTweetTime = @"12 am";
    liveFeed5.strTweetId = @"@alex_alex343";
    liveFeed5.arrayOfMessages = [[NSArray alloc]initWithObjects:@"Location was very Nice. We enjoyed the trip with our team is joyful. Event coordination was Good. Food was also very Good. The beach resort was good space enough to have a get together for corporate meetings. Best place to rock and roll.",@"Had been for family weekend outing. Most of the food items was good except Fish which was not as good as other items. Rooms are good and air-con is good as well. Beach view from restaurant is good and worth spending peaceful time during weekends.", nil];
    liveFeed5.arrayOfMessagesTiming = [[NSArray alloc]initWithObjects:@"12.00 am",@"1.13 am",nil];
    
    [liveFeedArray addObject:liveFeed5];
    
    //6
    
    LiveFeedModal *liveFeed6 = [LiveFeedModal new];
    liveFeed6.strTweetname = @"Marc H";
    liveFeed6.strTweetMessaged = @"Best hotel in the locality. If u want a value for money hotel then this pick should be on your top of the list. No other hotel in this locality can match its level of cleanliness and hospitality. It's is not only good, it is above the expectations.";
    liveFeed6.strTweetImage = @"img_05.jpeg";
    liveFeed6.strTweetTime = @"3 pm";
    liveFeed6.strTweetId = @"@marc_h9023";
    liveFeed6.arrayOfMessages = [[NSArray alloc]initWithObjects:@"An amazing resort with good facilities, Beach side view with awesome food, what more do you want to enjoy winters with your friends. The rooms are very decent and at amazing price. Its a must recommended hotel for travellers and holiday lovers.",@"My recent visit to mgm resort was awesome... Ambience is too good... Loved the food... And service is very fast... Everyone must visit it once.. I will plan my next visit very soon... I invite my family and friends to plan a visit @ mgm",@"The location was amazing and fabolous in other words it simply marvelous.The room was so cleaned that it looking very fresh,it reaaly awesome. The view from hotel is too good,specally the sun set . The stay was awesome", @"We had a excellent time in MGM beach resort during my 2nd Daughter 4th Birthday.Food both Veg and Non Veg was excellent and especially Non Veg variety was awesome and my family really enjoyed the Food and Service. We plan to stay for 1 night for my 1st Daughter Preethika Sree Birthday on 20th Jul this year.",@"It was a great outing celebrating the success of our product and Jai from event management team made sure that the event was entertaining and fun. Food and ambience was great. Overall kudos to MGM and their event management team.",@"It was a company outing ; almost 100 people went there ; For large pax it's worth going ; Food was very good ; Near to beach ; Games were good ; easy access to beach ; Good scenery views ; overall it was a good experience",@"This is a beach resort , ask for sea view rooms. Food is amazing. Facilities- pool, spa, beach, volleyball, hammock Ideal for family outings. Priced expensive but worth it! Dinner buffet is exquisite! Very good resort for photography.",nil];
    liveFeed6.arrayOfMessagesTiming = [[NSArray alloc]initWithObjects:@"4.30 pm",@"2.25 am",@"2.30 am",@"1 pm",@"5.01 pm",@"7.20 pm",@"8.15 pm",nil];
    
    [liveFeedArray addObject:liveFeed6];
    
    //7
    
    LiveFeedModal *liveFeed7 = [LiveFeedModal new];
    liveFeed7.strTweetname = @"Kirsten E";
    liveFeed7.strTweetMessaged = @"The Kämp hotel is in a class of its own. Service is first class as are the rooms. This kind of luxury is many times more expensive in just nearby Stockholm. Kämp's bar and restaurant are very good and food and drinks are super. Try the hamburger. It is a classic!";
    liveFeed7.strTweetImage = @"img_06.jpeg";
    liveFeed7.strTweetTime = @"10.05 am";
    liveFeed7.strTweetId = @"@kristen";
    liveFeed7.arrayOfMessages = [[NSArray alloc]initWithObjects:@"The Kämp hotel is in a class of its own. Service is first class as are the rooms. This kind of luxury is many times more expensive in just nearby Stockholm. Kämp's bar and restaurant are very good and food and drinks are super. Try the hamburger. It is a classic!",@"Everything was very good: personal, comfort, breakfast, location. Second time in this hotel and it is still as good as before. The only thing that disappointed me - the restaurant of pan-asian cuisine was closed.", nil];
    liveFeed7.arrayOfMessagesTiming = [[NSArray alloc]initWithObjects:@"2.30 pm",@"6 am",nil];
    [liveFeedArray addObject:liveFeed7];
    
    //8
    
    LiveFeedModal *liveFeed8 = [LiveFeedModal new];
    liveFeed8.strTweetname = @"Chales";
    liveFeed8.strTweetMessaged = @"Everything was very good: personal, comfort, breakfast, location. Second time in this hotel and it is still as good as before. The only thing that disappointed me - the restaurant of pan-asian cuisine was closed.";
    liveFeed8.strTweetImage = @"img_07.jpeg";
    liveFeed8.strTweetTime = @"5.08 pm";
    liveFeed8.strTweetId = @"@Chales_pp87";
    liveFeed8.arrayOfMessages = [[NSArray alloc]initWithObjects:@"I really enjoying to stay in hotel sabri classic.In sabri classic all the departments are very responsible for their work.The house keeping deparment are very responsible.I remember the name(Kaushik,Krisna). both the guy is very cooperative and are cleaning the room very well also they are very co-operative.Also the other department are very well they are listening the guest very well.",@"i had a wonderful time through out my stay in this noble hotel the gokulam park. everything about the park is wonderful. the rooms are of international standard. the meal is inter-continental. all the staffs of the parks are accomodating and caring especially shri krishna i.t.", nil];
    liveFeed8.arrayOfMessagesTiming = [[NSArray alloc]initWithObjects:@"1.30 pm",@"9.20 pm",nil];
    [liveFeedArray addObject:liveFeed8];
    
}

#pragma mark - Rotate Image

+ (UIImage *)scaleAndRotateImage:(UIImage *)image {
    
    int kMaxResolution = 800; // Or whatever
    
    CGImageRef imgRef = image.CGImage;
    
    CGFloat width = CGImageGetWidth(imgRef);
    CGFloat height = CGImageGetHeight(imgRef);
    
    
    CGAffineTransform transform = CGAffineTransformIdentity;
    CGRect bounds = CGRectMake(0, 0, width, height);
    if (width > kMaxResolution || height > kMaxResolution) {
        CGFloat ratio = width/height;
        if (ratio > 1) {
            bounds.size.width = kMaxResolution;
            bounds.size.height = roundf(bounds.size.width / ratio);
        }
        else {
            bounds.size.height = kMaxResolution;
            bounds.size.width = roundf(bounds.size.height * ratio);
        }
    }
    
    CGFloat scaleRatio = bounds.size.width / width;
    CGSize imageSize = CGSizeMake(CGImageGetWidth(imgRef), CGImageGetHeight(imgRef));
    CGFloat boundHeight;
    UIImageOrientation orient = image.imageOrientation;
    switch(orient) {
            
        case UIImageOrientationUp: //EXIF = 1
            transform = CGAffineTransformIdentity;
            break;
            
        case UIImageOrientationUpMirrored: //EXIF = 2
            transform = CGAffineTransformMakeTranslation(imageSize.width, 0.0);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            break;
            
        case UIImageOrientationDown: //EXIF = 3
            transform = CGAffineTransformMakeTranslation(imageSize.width, imageSize.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationDownMirrored: //EXIF = 4
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.height);
            transform = CGAffineTransformScale(transform, 1.0, -1.0);
            break;
            
        case UIImageOrientationLeftMirrored: //EXIF = 5
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, imageSize.width);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationLeft: //EXIF = 6
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.width);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationRightMirrored: //EXIF = 7
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeScale(-1.0, 1.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        case UIImageOrientationRight: //EXIF = 8
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, 0.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        default:
            [NSException raise:NSInternalInconsistencyException format:@"Invalid image orientation"];
            
    }
    
    UIGraphicsBeginImageContext(bounds.size);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    if (orient == UIImageOrientationRight || orient == UIImageOrientationLeft) {
        CGContextScaleCTM(context, -scaleRatio, scaleRatio);
        CGContextTranslateCTM(context, -height, 0);
    }
    else {
        CGContextScaleCTM(context, scaleRatio, -scaleRatio);
        CGContextTranslateCTM(context, 0, -height);
    }
    
    CGContextConcatCTM(context, transform);
    
    CGContextDrawImage(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, width, height), imgRef);
    UIImage *imageCopy = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return imageCopy;
}


-(void)setTabBar
{
    MenuViewController *sideMenuVC;
   
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    
    self.objCustomTabBar=[[UITabBarCustom alloc]init];
    
    // first
    homeVC = (NewHomeViewController *)[mainStoryboard
                                       instantiateViewControllerWithIdentifier:@"newhomevc"];
    UINavigationController *navhomeVC = [[UINavigationController alloc] initWithRootViewController:homeVC];
    
    //second
//    BOOL check=[self checkLocation];
//    if (check) {
//        HomeViewController *homeCon = (HomeViewController *)[mainStoryboard
//                                           instantiateViewControllerWithIdentifier:@"homescreen"];
//        homeCon.cityArray=APP_DELEGATE.cityArray;
//        homeCon.categoryarray=[APP_DELEGATE.categoryarray copy];
//        //homeCon.firstTimeLoad=YES;
//        homeCon.currentLocationEnable = true;
//        [homeCon pressCurrentLocation:nil];
//        [self.navigationController pushViewController:homeCon animated:YES];
//    }
    HomeViewController *mapVC = (HomeViewController *)[mainStoryboard
                                       instantiateViewControllerWithIdentifier:@"homescreen"];
    //Second
    UINavigationController *navMapVC = [[UINavigationController alloc]initWithRootViewController:mapVC];
    
    //Third
    topSpotVC = (EventCategoryViewController *)[mainStoryboard
                instantiateViewControllerWithIdentifier:@"eventscategoryvc"];
    UINavigationController *navtopSpotVC = [[UINavigationController alloc] initWithRootViewController:topSpotVC];
    
    //forth
    promotionVC = (spotlytePromotionViewController *)[mainStoryboard
                instantiateViewControllerWithIdentifier:@"spotlight"];
    UINavigationController *navpromotionVC = [[UINavigationController alloc] initWithRootViewController:promotionVC];
    
    //fifth
    dashboardVC = (DashBoardViewController *)[mainStoryboard
                                          instantiateViewControllerWithIdentifier:@"dashboard"];
    UINavigationController *navdashboardVC = [[UINavigationController alloc] initWithRootViewController:dashboardVC];
    
    
    //Sixth
    profileVC = (MyProfileViewController *)[mainStoryboard
                                      instantiateViewControllerWithIdentifier:@"myprofile"];
    profileVC.selectedController = @"currentuser";
    UINavigationController *navprofileVC = [[UINavigationController alloc] initWithRootViewController:profileVC];
    
    self.objCustomTabBar.viewControllers =@[navhomeVC,navMapVC,navtopSpotVC,navpromotionVC,navdashboardVC,navprofileVC,];
    [_objCustomTabBar selectTab:1];
    [navhomeVC setNavigationBarHidden:TRUE];
    [navMapVC setNavigationBarHidden:TRUE];
    [navtopSpotVC setNavigationBarHidden:TRUE];
    [navpromotionVC setNavigationBarHidden:TRUE];
    [navdashboardVC setNavigationBarHidden:TRUE];
    [navprofileVC setNavigationBarHidden:TRUE];
}

-(BOOL)checkLocation{
    
    BOOL check;
    if ([CLLocationManager locationServicesEnabled]){
        
        if ([CLLocationManager authorizationStatus]==kCLAuthorizationStatusDenied){
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"App Permission Denied"
            message:@"To re-enable, please go to Settings and turn on Location Service for this app."
                delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            check=NO;
        }
        else
            check=YES;
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Enable Location Service"
            message:@"Please go to settings and Enable Location Service."
                delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        check=NO;
    }
    return check;
}

#pragma mark
#pragma mark:- SWREVEAL SLIDEBAR DELEGATE METHDOS
- (NSString*)stringFromFrontViewPosition:(FrontViewPosition)position
{
    NSString *str = nil;
    if ( position == FrontViewPositionLeft ) str = @"FrontViewPositionLeft";
    if ( position == FrontViewPositionRight ) str = @"FrontViewPositionRight";
    if ( position == FrontViewPositionRightMost ) str = @"FrontViewPositionRightMost";
    if ( position == FrontViewPositionRightMostRemoved ) str = @"FrontViewPositionRightMostRemoved";
    return str;
}

- (void)revealController:(SWRevealViewController *)revealController willMoveToPosition:(FrontViewPosition)position
{
    NSLog( @"%@: %@", NSStringFromSelector(_cmd), [self stringFromFrontViewPosition:position]);
}

- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position
{
    NSLog( @"%@: %@", NSStringFromSelector(_cmd), [self stringFromFrontViewPosition:position]);
}

- (void)revealController:(SWRevealViewController *)revealController willRevealRearViewController:(UIViewController *)viewController
{
    NSLog( @"%@: %@", NSStringFromSelector(_cmd), viewController);
}

- (void)revealController:(SWRevealViewController *)revealController didRevealRearViewController:(UIViewController *)viewController
{
    NSLog( @"%@: %@", NSStringFromSelector(_cmd), viewController);
}

- (void)revealController:(SWRevealViewController *)revealController willHideRearViewController:(UIViewController *)viewController
{
    NSLog( @"%@: %@", NSStringFromSelector(_cmd), viewController);
}

- (void)revealController:(SWRevealViewController *)revealController didHideRearViewController:(UIViewController *)viewController
{
    NSLog( @"%@: %@", NSStringFromSelector(_cmd), viewController);
}

- (void)revealController:(SWRevealViewController *)revealController willShowFrontViewController:(UIViewController *)viewController
{
    NSLog( @"%@: %@", NSStringFromSelector(_cmd), viewController);
}

- (void)revealController:(SWRevealViewController *)revealController didShowFrontViewController:(UIViewController *)viewController
{
    NSLog( @"%@: %@", NSStringFromSelector(_cmd), viewController);
}

- (void)revealController:(SWRevealViewController *)revealController willHideFrontViewController:(UIViewController *)viewController
{
    NSLog( @"%@: %@", NSStringFromSelector(_cmd), viewController);
}

- (void)revealController:(SWRevealViewController *)revealController didHideFrontViewController:(UIViewController *)viewController
{
    NSLog( @"%@: %@", NSStringFromSelector(_cmd), viewController);
}

@end
